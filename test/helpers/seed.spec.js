/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

describe('Seed Helper', () => {
    describe('getAdditionalPackageData', () => {
        const { getAdditionalPackageData } = require('../../helpers/seed/packages');
        const sandbox = sinon.createSandbox();

        afterEach(() => {
            sandbox.restore()
        })

        xit('should return the correct package data query, select query, and join query', async () => {
            let sourceAlias = 'sourceAlias'

            let expectedPackageDataQuery = `
                moistureContent AS (
                    SELECT
                        trait.package_id,
                        trait.data_value AS data_value
                    FROM
                        germplasm.package_data trait
                        JOIN master.variable variable ON 
                            variable.id = trait.variable_id AND variable.is_void = FALSE
                        JOIN baseQuery 
                            ON baseQuery."packageDbId" = trait.package_id
                    WHERE
                        variable.abbrev = 'MC_CONT' AND trait.is_void = FALSE
                ),
                mcCleanSeed AS (
                    SELECT
                        trait.package_id,
                        trait.data_value AS data_value
                    FROM
                        germplasm.package_data trait
                        JOIN master.variable variable ON
                            variable.id = trait.variable_id AND variable.is_void = FALSE
                        JOIN baseQuery
                            ON baseQuery."packageDbId" = trait.package_id
                    WHERE
                        variable.abbrev = 'MC_CLEANSEED' AND trait.is_void = FALSE
                ),
                germplasmMta AS (
                    SELECT
                        germplasm_mta.seed_id,
                        germplasm_mta.germplasm_id,
                        STRING_AGG( DISTINCT germplasm_mta.mta_status::text, ',') AS mta_status,
                        germplasm_mta.mls_ancestors,
                        germplasm_mta.genetic_stock
                    FROM
                        germplasm.germplasm_mta germplasm_mta
                        JOIN baseQuery 
                            ON baseQuery."seedDbId" = germplasm_mta.seed_id 
                            AND baseQuery."germplasmDbId" = germplasm_mta.germplasm_id
                    WHERE
                        germplasm_mta.is_void = FALSE
                    GROUP BY germplasm_mta.seed_id, germplasm_mta.germplasm_id, germplasm_mta.mls_ancestors, germplasm_mta.genetic_stock
                ),
                rshtReferenceNo AS (
                        SELECT
                            trait.seed_id,
                            STRING_AGG( DISTINCT trait.data_value::text, ',') AS data_value
                        FROM
                            germplasm.seed_data trait
                            JOIN master.variable variable ON 
                                variable.id = trait.variable_id AND variable.is_void = FALSE
                            JOIN baseQuery ON baseQuery."seedDbId" = trait.seed_id
                        WHERE
                            variable.abbrev = 'RSHT_NO' AND trait.is_void = FALSE
                        GROUP BY trait.seed_id
                )
            `

            const expectedPackageDataSelectQuery = `
                moistureContent.data_value AS "moistureContent",
                mcCleanSeed.data_value AS "mcCleanSeed",
                rshtReferenceNo.data_value AS "rshtReferenceNo",
                germplasmMta.mta_status AS "mtaStatus",
                germplasmMta.mls_ancestors AS "mlsAncestors",
                germplasmMta.genetic_stock AS "geneticStock"
            `

            const expectedPackageDataJoinQuery = `
                LEFT JOIN moistureContent moistureContent 
                    ON moistureContent.package_id = sourceAlias."packageDbId"
                LEFT JOIN mcCleanSeed mcCleanSeed
                    ON mcCleanSeed.package_id = sourceAlias."packageDbId"
                LEFT JOIN germplasmMta germplasmMta
                ON germplasmMta.germplasm_id = t."germplasmDbId" 
                AND germplasmMta.seed_id = t."seedDbId"
                LEFT JOIN rshtReferenceNo rshtReferenceNo
                    ON rshtReferenceNo.seed_id = sourceAlias."seedDbId" 
            `

            const result = await getAdditionalPackageData(sourceAlias)

            expect(result.packageDataQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataQuery.replace(/\s+/g, ' ').trim())
            expect(result.packageDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.packageDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataJoinQuery.replace(/\s+/g, ' ').trim())
        })

        xit('should return correct queries for a different sourceAlias value', async () => {
            const sourceAlias = 'differentAlias'

            const expectedPackageDataQuery = `
                moistureContent AS (
                    SELECT
                        trait.package_id,
                        trait.data_value AS data_value
                    FROM
                        germplasm.package_data trait
                        JOIN master.variable variable ON 
                            variable.id = trait.variable_id AND variable.is_void = FALSE
                        JOIN baseQuery 
                            ON baseQuery."packageDbId" = trait.package_id
                    WHERE
                        variable.abbrev = 'MC_CONT' AND trait.is_void = FALSE
                ),
                mcCleanSeed AS (
                    SELECT
                        trait.package_id,
                        trait.data_value AS data_value
                    FROM
                        germplasm.package_data trait
                        JOIN master.variable variable ON
                            variable.id = trait.variable_id AND variable.is_void = FALSE
                        JOIN baseQuery
                            ON baseQuery."packageDbId" = trait.package_id
                    WHERE
                        variable.abbrev = 'MC_CLEANSEED' AND trait.is_void = FALSE
                ),
                germplasmMta AS (
                    SELECT
                        germplasm_mta.seed_id,
                        germplasm_mta.germplasm_id,
                        STRING_AGG( DISTINCT germplasm_mta.mta_status::text, ',') AS mta_status,
                        germplasm_mta.mls_ancestors,
                        germplasm_mta.genetic_stock
                    FROM
                        germplasm.germplasm_mta germplasm_mta
                        JOIN baseQuery 
                            ON baseQuery."seedDbId" = germplasm_mta.seed_id 
                            AND baseQuery."germplasmDbId" = germplasm_mta.germplasm_id
                    WHERE
                        germplasm_mta.is_void = FALSE
                    GROUP BY germplasm_mta.seed_id, germplasm_mta.germplasm_id, germplasm_mta.mls_ancestors, germplasm_mta.genetic_stock
                ),
                rshtReferenceNo AS (
                        SELECT
                            trait.seed_id,
                            STRING_AGG( DISTINCT trait.data_value::text, ',') AS data_value
                        FROM
                            germplasm.seed_data trait
                            JOIN master.variable variable ON 
                                variable.id = trait.variable_id AND variable.is_void = FALSE
                            JOIN baseQuery ON baseQuery."seedDbId" = trait.seed_id
                        WHERE
                            variable.abbrev = 'RSHT_NO' AND trait.is_void = FALSE
                        GROUP BY trait.seed_id
                )
            `;

            const expectedPackageDataSelectQuery = `
                moistureContent.data_value AS "moistureContent",
                mcCleanSeed.data_value AS "mcCleanSeed",
                rshtReferenceNo.data_value AS "rshtReferenceNo",
                germplasmMta.mta_status AS "mtaStatus",
                germplasmMta.mls_ancestors AS "mlsAncestors",
                germplasmMta.genetic_stock AS "geneticStock"
            `

            const expectedPackageDataJoinQuery = `
                LEFT JOIN moistureContent moistureContent 
                    ON moistureContent.package_id = differentAlias."packageDbId"
                LEFT JOIN mcCleanSeed mcCleanSeed
                    ON mcCleanSeed.package_id = differentAlias."packageDbId"
                LEFT JOIN germplasmMta germplasmMta
                    ON germplasmMta.germplasm_id = t."germplasmDbId" 
                    AND germplasmMta.seed_id = t."seedDbId"
                LEFT JOIN rshtReferenceNo rshtReferenceNo
                    ON rshtReferenceNo.seed_id = differentAlias."seedDbId" 
            `

            const result = await getAdditionalPackageData(sourceAlias);

            expect(result.packageDataQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataQuery.replace(/\s+/g, ' ').trim())
            expect(result.packageDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.packageDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataJoinQuery.replace(/\s+/g, ' ').trim())
        })
    })
    describe('getSeedData', () => {
        const { getSeedData } = require('../../helpers/seed/packages');
        const sandbox = sinon.createSandbox()

        afterEach(() => {
            sandbox.restore();
        });

        it('should return default select and join queries when no filters are provided and dataLevel is not "seed"', async () => {
            let isSource = false
            let dataFilter = []
            let dataLevel = 'defaultLevel'

            let expectedSeedDataSelectQuery = `
                seed.seed_name AS "seedName",
                seed.seed_code AS "seedCode",
                seed.source_experiment_id AS "experimentDbId",
                seed.source_occurrence_id AS "occurrenceDbId",
                seed.source_location_id AS "locationDbId",
                seed.source_entry_id AS "entryDbId",
                seed.cross_id AS "crossDbId",
                seed.harvest_source AS "harvestSource",`

            let expectedSeedDataJoinQuery = `
                JOIN germplasm.seed seed 
                    ON seed.germplasm_id = germplasm.id 
                    AND seed.is_void = FALSE 
            `

            let result = await getSeedData(isSource, dataFilter, dataLevel)

            expect(result.seedDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.seedDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataJoinQuery.replace(/\s+/g, ' ').trim())
        })

        it('should include germplasm data in select query when dataLevel is "seed"', async () => {
            let isSource = false
            let dataFilter = []
            let dataLevel = 'seed'

            let expectedSeedDataSelectQuery = `
                seed.seed_name AS "seedName",
                seed.seed_code AS "seedCode",
                seed.source_experiment_id AS "experimentDbId",
                seed.source_occurrence_id AS "occurrenceDbId",
                seed.source_location_id AS "locationDbId",
                seed.source_entry_id AS "entryDbId",
                seed.cross_id AS "crossDbId",
                seed.harvest_source AS "harvestSource",
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                germplasm.designation AS "designation",`

            let expectedSeedDataJoinQuery = `
                germplasm.seed seed 
                JOIN germplasm.germplasm germplasm 
                    ON germplasm.id = seed.germplasm_id 
                    AND germplasm.is_void = FALSE 
            `

            let result = await getSeedData(isSource, dataFilter, dataLevel)

            expect(result.seedDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.seedDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataJoinQuery.replace(/\s+/g, ' ').trim())
        })

        it('should return join queries when dataLevel is not "all" or "seed"', async () => {
            let isSource = false
            let dataFilter = []
            let dataLevel = 'defaultLevel'

            let expectedSeedDataSelectQuery = `
                seed.seed_name AS "seedName",
                seed.seed_code AS "seedCode",
                seed.source_experiment_id AS "experimentDbId",
                seed.source_occurrence_id AS "occurrenceDbId",
                seed.source_location_id AS "locationDbId",
                seed.source_entry_id AS "entryDbId",
                seed.cross_id AS "crossDbId",
                seed.harvest_source AS "harvestSource",`

            let expectedSeedDataJoinQuery = `
                JOIN germplasm.seed seed 
                    ON seed.germplasm_id = germplasm.id 
                    AND seed.is_void = FALSE 
            `

            let result = await getSeedData(isSource, dataFilter, dataLevel);

            expect(result.seedDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.seedDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataJoinQuery.replace(/\s+/g, ' ').trim())
        })

        it('should return filtered select and join queries when filters are provided and isSource is true', async () => {
            let isSource = true
            let dataFilter = { seedNames: 'seedName1', seedCodes: '' }
            let dataLevel = 'defaultLevel'

            let expectedSeedDataSelectQuery = `
                seed.seed_name AS "seedName",
                seed.seed_code AS "seedCode",
                seed.source_experiment_id AS "experimentDbId",
                seed.source_occurrence_id AS "occurrenceDbId",
                seed.source_location_id AS "locationDbId",
                seed.source_entry_id AS "entryDbId",
                seed.cross_id AS "crossDbId",
                seed.harvest_source AS "harvestSource",`

            let expectedSeedDataJoinQuery = `
                (
                    SELECT
                        seed.id,
                        seed.seed_name,
                        seed.seed_code,
                        seed.source_experiment_id,
                        seed.source_occurrence_id,
                        seed.source_location_id,
                        seed.source_entry_id,
                        seed.source_plot_id,
                        seed.harvest_date,
                        seed.cross_id,
                        seed.harvest_source,
                        seed.germplasm_id,
                        seed.program_id
                    FROM
                        germplasm.seed seed
                    WHERE
                        seed.is_void = FALSE
                        seedName1
                    GROUP BY
                        seed.id
                ) AS seed
                JOIN germplasm.germplasm germplasm 
                    ON seed.germplasm_id = germplasm.id 
                    AND germplasm.is_void = FALSE
            `

            let result = await getSeedData(isSource, dataFilter, dataLevel)

            expect(result.seedDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.seedDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedSeedDataJoinQuery.replace(/\s+/g, ' ').trim())
        })
    })

    describe('getPackageData', () => {
        const { getPackageData } = require('../../helpers/seed/packages')
        const sandbox = sinon.createSandbox()

        afterEach(() => {
            sandbox.restore()
        })

        it('should return default select and join queries when no filters are provided', async () => {
            let isSource = false
            let dataFilter = []
            let dataLevel = 'defaultLevel'

            let expectedPackageDataSelectQuery = `
                seed.program_id AS "seedProgramDbId",  
                package.package_code AS "packageCode",
                package.package_label AS "label",
                package.package_quantity AS "quantity",
                package.package_unit AS "unit",
                package.package_document AS "packageDocument",
                package.package_status AS "packageStatus",
                package.package_reserved AS "packageReserved",
            `

            let expectedPackageDataJoinQuery = ''

            let result = await getPackageData(isSource, dataFilter, dataLevel)

            expect(result.packageDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.packageDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataJoinQuery.replace(/\s+/g, ' ').trim())
        })

        it('should return filtered select and join queries when filters are provided', async () => {
            let isSource = true
            let dataFilter = { labels: 'label1', packageCodes: '' }
            let dataLevel = 'defaultLevel'

            let expectedPackageDataSelectQuery = `
                seed.program_id AS "seedProgramDbId",  
                package.package_code AS "packageCode",
                package.package_label AS "label",
                package.package_quantity AS "quantity",
                package.package_unit AS "unit",
                package.package_document AS "packageDocument",
                package.package_status AS "packageStatus",
                package.package_reserved AS "packageReserved",
                package."package_id" AS "packageDbId", 
            `

            let expectedPackageDataJoinQuery = `
                (
                    SELECT
                        package.id as "package_id",
                        package.package_code,
                        package.package_label,
                        package.package_quantity,
                        package.package_unit,
                        package.package_document,
                        package.package_status,
                        package.package_reserved,
                        package.seed_id,
                        package.program_id,
                        package.facility_id,
                        package.creator_id,
                        package.modifier_id,
                        package.is_void,
                        package.id
                    FROM
                        germplasm.package package
                    WHERE
                        package.is_void = FALSE
                        label1
                    GROUP BY
                        package.id
                ) AS package
                JOIN germplasm.seed seed 
                    ON seed.id = package.seed_id 
                    AND seed.is_void = FALSE
                JOIN germplasm.germplasm germplasm 
                    ON seed.germplasm_id = germplasm.id 
                    AND seed.is_void = FALSE
            `

            let result = await getPackageData(isSource, dataFilter, dataLevel);

            expect(result.packageDataSelectQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataSelectQuery.replace(/\s+/g, ' ').trim())
            expect(result.packageDataJoinQuery.replace(/\s+/g, ' ').trim()).to.equal(expectedPackageDataJoinQuery.replace(/\s+/g, ' ').trim())
        })
    })

    describe('toCamelCase', () => {
        const { toCamelCase } = require('../../helpers/seed/packages');
        it('should convert text to camel case', () => {
            let inputText = "this is a test string"
            let expectedOutput = "thisIsATestString"

            let result = toCamelCase(inputText)

            expect(result).to.equal(expectedOutput)
        })

        it('should handle empty string', () => {
            let inputText = ""
            let expectedOutput = ""

            let result = toCamelCase(inputText)

            expect(result).to.equal(expectedOutput)
        })

        it('should handle a single word', () => {
            let inputText = "hello"
            let expectedOutput = "hello"

            let result = toCamelCase(inputText)

            expect(result).to.equal(expectedOutput)
        })
    })
})