/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

describe('Seedlot Helper', () => {
    describe('encloseDesignationInParentheses', () => {
        const { encloseDesignationInParentheses } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should enclose designation in parentheses '
            + 'if there are two parents', async () => {
            let germplasmRecord = {
                germplasmDbId: 123,
                designation: "ABC/123",
                generation: "inbred_line"
            }

            queryStub.onFirstCall().resolves(['parent1','parent2'])

            const result = await encloseDesignationInParentheses(germplasmRecord)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == '(ABC/123)').to.be.true('the designation must be enclosed in parentheses')
        })

        it('should enclose designation in parentheses '
            + 'if generation is F1', async () => {
            let germplasmRecord = {
                germplasmDbId: 123,
                designation: "ABC/123",
                generation: "F1"
            }

            queryStub.onFirstCall().resolves(['parent1'])

            const result = await encloseDesignationInParentheses(germplasmRecord)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == '(ABC/123)').to.be.true('the designation must be enclosed in parentheses')
        })

        it('should NOT enclose designation in parentheses '
            + 'if parent count is NOT 2'
            + 'and if generation is NOT F1', async () => {
            let germplasmRecord = {
                germplasmDbId: 123,
                designation: "(ABC/123)-B",
                generation: "inbred_line"
            }

            queryStub.onFirstCall().resolves(['parent1'])

            const result = await encloseDesignationInParentheses(germplasmRecord)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == '(ABC/123)-B').to.be.true('the designation must NOT be enclosed in parentheses')
        })
    })

    describe('retrieveChildGermplasm', () => {
        const { retrieveChildGermplasm } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should succefully retrieve the germplasm children', async () => {
            let parentGermplasmId

            queryStub.onFirstCall().resolves([
                {
                    childGermplasmDbId:123
                }
            ])

            let result = await retrieveChildGermplasm(parentGermplasmId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 123).to.be.true('the child gerplasm id should be 123')
        })

        it('should succefully retrieve the germplasm children '
            + 'with child conditions provided', async () => {
            let parentGermplasmId
            let childConditions = [ 'param1=456' ]

            queryStub.onFirstCall().resolves([
                {
                    childGermplasmDbId:123,
                    param1:456
                }
            ])

            let result = await retrieveChildGermplasm(parentGermplasmId, childConditions)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 123).to.be.true('the child gerplasm id should be 123')
        })

        it('should return null when there is no child', async () => {
            let parentGermplasmId
            let childConditions = []

            queryStub.onFirstCall().resolves([])

            let result = await retrieveChildGermplasm(parentGermplasmId, childConditions)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('the child gerplasm id should be null')
        })
    })

    describe('retrieveCommonChildGermplasm', () => {
        const { retrieveCommonChildGermplasm } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should succefully retrieve the germplasm children', async () => {
            let femaleGermplasmId = 1
            let maleGermplasmId = 2

            queryStub.onFirstCall().resolves([
                {
                    childGermplasmDbId:123
                }
            ])

            let result = await retrieveCommonChildGermplasm(femaleGermplasmId, maleGermplasmId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 123).to.be.true('the child gerplasm id should be 123')
        })

        it('should succefully retrieve the germplasm children '
            + 'with child conditions provided', async () => {
            let femaleGermplasmId = 1
            let maleGermplasmId = 2
            let childConditions = [ 'param1=456' ]

            queryStub.onFirstCall().resolves([
                {
                    childGermplasmDbId:123,
                    param1:456
                }
            ])

            let result = await retrieveCommonChildGermplasm(femaleGermplasmId, maleGermplasmId, childConditions)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 123).to.be.true('the child gerplasm id should be 123')
        })

        it('should return null when there is no child', async () => {
            let femaleGermplasmId = 1
            let maleGermplasmId = 2
            let childConditions = []

            queryStub.onFirstCall().resolves([])

            let result = await retrieveCommonChildGermplasm(femaleGermplasmId, maleGermplasmId, childConditions)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('the child gerplasm id should be null')
        })
    })

    describe('getNextInSequence', () => {
        const { getNextInSequence } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve last in sequence value', async () => {

            let schema = 'germplasm'
            let sequenceName = 'germplasm'

            queryStub.onFirstCall().resolves([{'nextVal':1}])

            let result = await getNextInSequence(schema, sequenceName)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 1).to.be.true('the last sequence value is 1')
        })

        it('should return null if no last sequence', async () => {

            let schema = 'germplasm'
            let sequenceName = 'germplasm'

            queryStub.onFirstCall().resolves([])

            let result = await getNextInSequence(schema, sequenceName)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('the last sequence value is null')
        })

        it('should return null if query fails', async () => {

                let schema = 'germplasm'
                let sequenceName = 'germplasm'

                queryStub.onFirstCall().rejects

                let result = await getNextInSequence(schema, sequenceName)

                expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
                expect(result == null).to.be.true('the last sequence value is null')
        })

        it('should return null or undefined if response format is unexpected', async () => {
            let schema = 'germplasm';
            let sequenceName = 'germplasm';
            let invalidKey = 'unexpected_format';

            queryStub.onFirstCall().resolves([{ 'invalidKey': invalidKey }]);

            let result = await getNextInSequence(schema, sequenceName);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result === null || result === undefined).to.be.true('the result should be null or undefined for unexpected response format');
        })
    })

    describe('getPlotInformation', () => {
        const { getPlotInformation } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully return plot information', async () =>{

            let plotDbId = 1;
            let plotInfo = [{
                'plotDbId':1,
                'locationDbId':123,
                'experimentDbId':123,
                'experimentName':'Sample Experiment',
                'experimentYear':2022,
                'experimentType':'Breeding Trial',
                'cropDbId':1,
                'cropName':'Sample Crop',
                'cropCode':'CROP CODE',
                'experimentSeason':'Wet',
                'stage':'F1',
                'entryDbId':123,
                'plotNumber':1,
                'plotCode':'1',
                'occurrenceDbId':1,
                'occurrenceCode':'OCCURRENCE CODE',
                'occurrenceName':'Sample Occurrence',
                'nurserySiteCode':'',
                'experimentSeasonCode':'WS',
                'experimentYearYY':22,
                'experimentYear':2022,
                'harvest_method':'Bulk',
                'germplasmDbId':123,
                'germplasmState':'not_fixed',
                'germplasmType':'F1',
                'germplasmGeneration':'F1',
                'germplasmDesignation':'Sample Designation',
                'germplasmParentage':'Sample Parentage',
                'germplasmTaxonomyId':1,
                'germplasmNameType':'F1',
                'seedDbId':123,
                'seedProgramDbId':2,
                'programCode':'SAMPLE_PROGRAM',
                'crossDbId':123,
                'crossMethod':'single cross',
                'originSiteCode':'',
                'fieldOriginSiteCode':'',
                'crossParentCount':2
            }]

            queryStub.onFirstCall().resolves(plotInfo)

            let result = await getPlotInformation(plotDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('plot information is not empty')
        })

        it('should return null when no retrieved plot information', async ()=>{
            let plotDbId = 1

            queryStub.onFirstCall().resolves([])

            let result = await getPlotInformation(plotDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('plot information is empty')
        })

        it('should return null when query fails', async ()=>{
            let plotDbId = 1

            queryStub.onFirstCall().rejects

            let result = await getPlotInformation(plotDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('plot information is empty')
        })
    })

    describe('getCrossRecord', () => {
        const { getCrossRecord } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve cross record information', async () =>{
            let plotDbId = 1;
            let crossParentsInfo = [{
                'plotDbId':1,
                'experimentDbId':123,
                'cropName':'Sample Crop',
                'crossDbId':123,
                'crossMethod':'single cross',
                'parent1Role':'Parent Role 1',
                'parent1EntryDbId':123,
                'parent1GermplasmDbId':123,
                'parent1GermplasmState':'not_fixed',
                'parent1GermplasmType':'F1',
                'parent1Generation':'F1',
                'parent1Designation':'Parent 1 Designation',
                'parent2Role':'Parent Role 2',
                'parent2EntryDbId':456,
                'parent2GermplasmDbId':456,
                'parent2Designation':'Parent 2 Designation',
                'parent2GermplasmState':'not_fixed',
                'parent2GermplasmType':'F1',
                'parent2Generation':'F1'
            }]

            queryStub.onFirstCall().resolves(crossParentsInfo)

            let result = await getCrossRecord(plotDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('cross information is not empty')
        })

        it('should return empty array when no record is retrieved', async () => {
            let plotDbId = 1

            queryStub.onFirstCall().resolves([])

            let result = await getCrossRecord(plotDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result.length == 0).to.be.true('cross information is empty')
        })

        it('should return null when query fails', async () => {
            let plotDbId = 1

            queryStub.onFirstCall().rejects

            let result = await getCrossRecord(plotDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('cross information is null')
        })
    })

    describe('getCrossInformation', () => {
        const { getCrossInformation } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve cross information', async () => {
            let crossDbId = 1
            let crossInformation = [{
                'crossDbId':123,
                'crossName':'Sample Cross Name',
                'crossMethod':'single cross',
                'cropDbId':123,
                'cropName':'Sample Crop',
                'cropCode':'SAMPLE_CROP_CODE',
                'programDbId':1,
                'programCode':'PROGRAM_CODE',
                'taxonomyDbId':1,
                'experimentDbId':1,
                'experimentName':'Sample Experiment',
                'experimentYear':2022,
                'experimentType':'Breeding Trial',
                'experimentYearYY':22,
                'experimentSeason':'Wet',
                'experimentSeasonCode':'WS',
                'occurrenceDbId':123,
                'occurrenceCode':'OCCURRENCE_CODE',
                'locationDbId':123,
                'geospatialObjectDbId':123,
                'originSiteCode':'',
                'fieldOriginSiteCode':'',
                'germplasmDbId':123,
                'germplasmState':'not_fixed',
                'germplasmType':'F1',
                'germplasmGeneration':'F1',
                'germplasmDesignation':'Sample',
                'germplasmParentage':'parentage',
                'germplasmTaxonomyId':123,
                'germplasmNameType':'F1',
                'seedDbId':123,
                'seedCode':'SEED_CODE',
                'seedName':'Seed Name',
                'nurserySiteCode':'',
                'harvestMethod':'Bulk',
                'plotDbId':123,
                'crossParentCount':2
            }]

            queryStub.onFirstCall().resolves(crossInformation)

            let result = await getCrossInformation(crossDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('plot information is not empty')
        })

        it('should return empty if no record is found', async () => {
            let crossDbId = 1

            queryStub.onFirstCall().resolves([])

            let result = await getCrossInformation(crossDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == undefined).to.be.true('plot information is empty')
        })

        it('should return null if query fails', async () => {
            let crossDbId = 1

            queryStub.onFirstCall().rejects

            let result = await getCrossInformation(crossDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('plot information is null')
        })

        it('should return null if both cross ID and occurrence ID are both provided but no records found', async () => {
            let crossDbId = 1;
            let occurrenceDbId = 2;

            queryStub.onFirstCall().resolves([]);

            let result = await getCrossInformation(crossDbId, occurrenceDbId);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result === undefined).to.be.true('cross information is null');
        })

        it('should return null if cross ID provided is null', async () => {
            let crossDbId = null;

            let result = await getCrossInformation(crossDbId);

            expect(queryStub.calledOnce).to.be.true('query should be called');
            expect(result === null).to.be.true('cross information is null');
        })
    })

    describe('getSeedInformation', () => {
        const { getSeedInformation } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve seed information', async () => {
            let seedDbId = 1
            let seedInformation = [{
                'seedDbId':123,
                'seedName':'Sample Seed Name'
            }]

            queryStub.onFirstCall().resolves(seedInformation)

            let result = await getSeedInformation(seedDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('seed information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getSeedInformation([{}])

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('seed information is empty')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().rejects

            let result = await getSeedInformation(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })
    })

    describe('getPackageInformation', () => {
        const { getPackageInformation } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve package information', async () => {
            let packageDbId = 1
            let packageInformation = [{
                'packageDbId':123,
                'packageLabel':'Sample Seed Name'
            }]

            queryStub.onFirstCall().resolves(packageInformation)

            let result = await getPackageInformation(packageDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('package information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getPackageInformation(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('package information is empty')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().resolves([{}])

            let result = await getPackageInformation(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })
    })

    describe('getPlotHarvestData', () => {
        const { getPlotHarvestData } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        }) 

        it('should successfully retrieve package data', async () => {
            let plotHarvestData = [{
                'plot_id':1,
                'harvest_date':'2022-01-01',
                'harvest_method':'Bulk',
                'numeric_variable':''
            }]

            queryStub.onFirstCall().resolves(plotHarvestData)

            let result = await getPlotHarvestData(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('plot harvest information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getPlotHarvestData(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('query has failed')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().resolves([{}])

            let result = await getPlotHarvestData(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })
    })

    describe('getCrossHarvestData', () => {
        const { getCrossHarvestData } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        }) 

        it('should successfully retrieve cross harvest data', async () => {
            let crossHarvestData = [{
                'cross_id':1,
                'crossing_date':'2021-12-30',
                'harvest_date':'2022-01-01',
                'harvest_method':'Bulk',
                'numeric_variable':''
            }]

            queryStub.onFirstCall().resolves(crossHarvestData)

            let result = await getCrossHarvestData(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('cross harvest information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getCrossHarvestData(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('query has failed')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().resolves([{}])

            let result = await getCrossHarvestData(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })
    })

    describe('getGermplasmParents', () => {
        const { getGermplasmParents } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        }) 

        it('should successfully retrieve germplasm parent information', async () => {
            let germplasmParentsData = [{
                'germplasmRelationDbId':1,
                'childGermplasmDbId':2,
                'parentGermplasmDbId':3
            }]

            queryStub.onFirstCall().resolves(germplasmParentsData)

            let result = await getGermplasmParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('germplasm parent information is not empty')
        })

        it('should successfully return empty if no record is found', async () => {
            queryStub.onFirstCall().resolves([{}])

            let result = await getGermplasmParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(Object.keys(result[0]).length == 0).to.be.true('no record found')
        })

        it('should return null query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getGermplasmParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === undefined).to.be.true('query failed')
        })

        it('should return empty if child germplasm ID is invalid', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await getGermplasmParents(-1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result.length === 0).to.be.true('should return empty if child germplasm ID is invalid')
        })

        it('should successfully retrieve multiple germplasm parent records', async () => {
            let germplasmParentsData = [
                {
                    'germplasmRelationDbId': 1,
                    'childGermplasmDbId': 2,
                    'parentGermplasmDbId': 3
                },
                {
                    'germplasmRelationDbId': 2,
                    'childGermplasmDbId': 2,
                    'parentGermplasmDbId': 4
                }
            ]

            queryStub.onFirstCall().resolves(germplasmParentsData)

            let result = await getGermplasmParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result.length > 1).to.be.true('should retrieve multiple germplasm parent records')
        })
    })

    describe('getCrossParents', () => {
        const { getCrossParents } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        }) 

        it('should successfully retrieve cross parents information', async () =>{
            let germplasmParentsData = [{
                'femaleParent' : {
                    'crossParentDbId':1,
                    'crossDbId':2,
                    'parentRole':'female-male',
                    'germplasmDbId':123,
                    'seedDbId':123,
                    'sourceExperimentDbId':12,
                    'sourceExperimentName':'Source Experiment',
                    'entryDbId':12,
                    'entryNumber':12,
                    'programDbId':1,
                    'programCode':'PROGRAM_CODE'
                },
                'maleParent': {
                    'crossParentDbId':1,
                    'crossDbId':2,
                    'parentRole':'female-male',
                    'germplasmDbId':123,
                    'seedDbId':123,
                    'sourceExperimentDbId':12,
                    'sourceExperimentName':'Source Experiment',
                    'entryDbId':12,
                    'entryNumber':12,
                    'programDbId':1,
                    'programCode':'PROGRAM_CODE'
                }
            }]

            queryStub.onFirstCall().resolves(germplasmParentsData)

            let result = await getCrossParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('cross parent information is not empty')
        })

        it('should return null male and female parent records if no records retrieved', async () => {
            queryStub.onFirstCall().resolves({'femaleParent':null,'maleParent':null})

            let result = await getCrossParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('no cross parent information retrieved')
        }) 

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getCrossParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('query failed')
        })

        it('should return null if cross ID is not provided', async () => {
            let result = await getCrossParents()

            expect(queryStub.calledOnce).to.be.true('query should be called')
            expect(result === null).to.be.true('return null')
        })

        it('should return null if no cross parents are found', async () => {
            queryStub.onFirstCall().resolves({'femaleParent':null,'maleParent':null})

            let result = await getCrossParents(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result.femaleParent == null).to.be.true('no female parent record found')
            expect(result.maleParent == null).to.be.true('no male parent record found')
        })
    })

    describe('getGermplasmInfo', () => {
        const { getGermplasmInfo } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve germplasm information', async () => {
            let germplasmInfo = {
                'germplasmDbId':1,
                'designation':'Designation',
                'parentage':'Parentage',
                'generation':'F1',
                'germplasmState':'not_fixed',
                'germplasmType':'F1',
                'cropDbId':12,
                'taxonomyDbId':12
            }

            queryStub.onFirstCall().resolves(germplasmInfo)

            let result = await getGermplasmInfo(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('germplasm information is not empty')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().resolves([{}])

            let result = await getGermplasmInfo(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(Object.keys(result).length == 0).to.be.true('germplasm information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getGermplasmInfo(2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('query failed')
        })

        it('should handle null germplasm ID and return null without querying', async () => {
            let germplasmDbId = null;

            let result = await getGermplasmInfo(germplasmDbId);

            expect(queryStub.calledOnce).to.be.true('query should be called');
            expect(result === null).to.be.true('germplasm information is null');
        })

        it('should handle invalid germplasm ID and return null', async () => {
            let germplasmDbId = 'invalidID';

            queryStub.onFirstCall().resolves();

            let result = await getGermplasmInfo(germplasmDbId);

            expect(queryStub.calledOnce).to.be.true('query should be called');
            expect(result === null).to.be.true('germplasm information is null');
        })
    })

    describe('getChildGermplasmId', () => {
        const { getChildGermplasmId } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve child germplasm id', async () => {
            queryStub.onFirstCall().resolves([{'child_germplasm_id':1}])

            let result = await getChildGermplasmId(1,2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 1).to.be.true('germplasm information is not empty')
        })

        it('should return 0 if no child germplasm record is found', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await getChildGermplasmId(1,2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 0).to.be.true('germplasm information is not empty')
        })

        it('should return 0 if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getChildGermplasmId(1,2)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 0).to.be.true('germplasm information is not empty')
        })

        it('should successfully retrieve child germplasm id when ordered is false', async () => {
            queryStub.onFirstCall().resolves([{ 'child_germplasm_id': 3 }])

            let result = await getChildGermplasmId(1, 2, false)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 3).to.be.true('germplasm information is not empty')
        })

        it('should return null if getCompleteInfo is true and no record is found', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await getChildGermplasmId(1, 2, true, false, [], false, true)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('germplasm information is null')
        })

        it('should successfully retrieve complete info if getCompleteInfo is true', async () => {
            const completeInfo = { 'child_germplasm_id': 1, 'some_other_field': 'some_value' }
            queryStub.onFirstCall().resolves([completeInfo])

            let result = await getChildGermplasmId(1, 2, true, false, [], false, true)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == completeInfo).to.be.true('complete germplasm information is returned')
        })

        it('should include child info and return appropriate result', async () => {
            const childInfo = { 'child_germplasm_id': 4 }
            queryStub.onFirstCall().resolves([childInfo])

            let result = await getChildGermplasmId(1, 2, true, true, ['child.is_active = true'], false)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 4 ).to.be.true('germplasm information with child info is returned')
        })

        it('should include cross info and return appropriate result', async () => {
            const crossInfo = { 'child_germplasm_id': 5 }
            queryStub.onFirstCall().resolves([crossInfo])

            let result = await getChildGermplasmId(1, 2, true, true, ['child.is_active = true'], true)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == 5).to.be.true('germplasm information with cross info is returned')
        })
    })

    describe('checkExistingCross', () => {
        const { checkExistingCross } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve existing cross information', async () => {
            queryStub.onFirstCall().resolves([{'count':1}])

            let result = await checkExistingCross(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result[0].count == 1).to.be.true('germplasm has existing cross record')
        })

        it('should return 0 if no cross record is found', async () => {
            queryStub.onFirstCall().resolves([{'count':0}])

            let result = await checkExistingCross(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result[0].count == 0).to.be.true('germplasm has no existing cross record')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await checkExistingCross(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('query failed')
        })

        it('should successfully retrieve existing cross information with a specified cross method', async () => {
            queryStub.onFirstCall().resolves([{'count': 2}])

            let result = await checkExistingCross(1, 'specificMethod')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result[0].count == 2).to.be.true('germplasm has existing cross record with specified method')
        })

        it('should return 0 if no cross record is found with a specified cross method', async () => {
            queryStub.onFirstCall().resolves([{'count': 0}])

            let result = await checkExistingCross(1, 'specificMethod')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result[0].count == 0).to.be.true('germplasm has no existing cross record with specified method')
        })

        it('should handle invalid germplasmId gracefully', async () => {
            queryStub.onFirstCall().resolves([{'count': 0}])

            let result = await checkExistingCross('invalidId')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result[0].count == 0).to.be.true('invalid germplasmId should result in 0 count')
        })

        it('should handle large germplasmId values correctly', async () => {
            queryStub.onFirstCall().resolves([{'count': 5}])

            let result = await checkExistingCross(999999999)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result[0].count == 5).to.be.true('large germplasmId should be handled correctly')
        })
    })

    describe('normalizeText', () => {
        const { normalizeText } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully normalize the given germplasm name', async () => {
            queryStub.onFirstCall().resolves([{'normalizedText':'IRRI123'}])

            let result = await normalizeText('irri 123')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === 'IRRI123').to.be.true('germplasm name has been normalized')
        })

        it('should return null when query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await normalizeText('irri 123')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('query has failed')
        })
    })

    describe('generateSeedCode', () => {
        const { generateSeedCode } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully generate seed code for given input', async () => {
            queryStub.onFirstCall().resolves([{'seedcode':'SEED000000000001'}])

            let result = await generateSeedCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === 'SEED000000000001').to.be.true('new seed code has been generated')
            expect(result).to.match(/^[SEED]+(\d{12})+$/)
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await generateSeedCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('query failed')
        })

        it('should return null of no seed code is generated', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await generateSeedCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('empty return')
        })

        it('should successfully generate seed code with custom prefix', async () => {
            const customPrefix = 'CUSTOM';

            queryStub.onFirstCall().resolves([{ 'seedcode': 'CUSTOM000000000001' }]);

            let result = await generateSeedCode(customPrefix);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result === 'CUSTOM000000000001').to.be.true('new seed code with custom prefix has been generated');
        })

        it('should successfully generate seed code with EMPTY custom prefix', async () => {
            const customPrefix = '';

            queryStub.onFirstCall().resolves([{ 'seedcode': '000000000001' }]);

            let result = await generateSeedCode(customPrefix);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result === '000000000001').to.be.true('new seed code with EMPTY custom prefix has been generated');
        })

    })

    describe('generatePackageCode', () => {
        const { generatePackageCode } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully generate package code for given input', async () => {
            queryStub.onFirstCall().resolves([{'packagecode':'PKG000000000001'}])

            let result = await generatePackageCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === 'PKG000000000001').to.be.true('new package code has been generated')
            expect(result).to.match(/^[PKG]+(\d{12})+$/)
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await generatePackageCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('query failed')
        })

        it('should return null of no package code is generated', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await generatePackageCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('empty return')
        })

        it('should successfully generate package code with custom prefix', async () => {
            const customPrefix = 'CUSTOM';

            queryStub.onFirstCall().resolves([{ 'packagecode': 'CUSTOM000000000001' }]);

            let result = await generatePackageCode(customPrefix);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result === 'CUSTOM000000000001').to.be.true('new package code with custom prefix has been generated');
        });

        it('should successfully generate package code with EMPTY custom prefix', async () => {
            const customPrefix = '';

            queryStub.onFirstCall().resolves([{ 'packagecode': '000000000001' }]);

            let result = await generatePackageCode(customPrefix);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result === '000000000001').to.be.true('new package code with EMPTY custom prefix has been generated');
        });
    })

    describe('germplasmTypeProcess', () => {
        const { germplasmTypeProcess } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should generate F1F germplasm type if both germplasm states of the parents are fixed', 
            async () => {
                let femaleParentGermplasm = { 'germplasmState' : 'fixed' }
                let maleParentGermplasm = { 'germplasmState' : 'fixed' }
                let childGermplasmType = 'F1F'

                let result = await germplasmTypeProcess(femaleParentGermplasm, maleParentGermplasm)

                expect(result === childGermplasmType).to.be.true('germplasm type determined')
        })

        it('should generate F1 germplasm type if germplasm state of either parents is not_fixed', 
            async () => {
                let femaleParentGermplasm = { 'germplasmState' : 'fixed' }
                let maleParentGermplasm = { 'germplasmState' : 'not_fixed' }
                let childGermplasmType = 'F1'

                let result1 = await germplasmTypeProcess(femaleParentGermplasm, maleParentGermplasm)
                let result2 = await germplasmTypeProcess(maleParentGermplasm, femaleParentGermplasm)

                expect(result1 === childGermplasmType).to.be.true('germplasm type determined')
                expect(result2 === childGermplasmType).to.be.true('germplasm type determined')
        })

        it('should return null if at least 1 parent germplasm is not provided', 
            async () => {
                let maleParentGermplasm = { 'germplasmState' : 'fixed' }
                let childGermplasmType = null

                let result1 = await germplasmTypeProcess(null, maleParentGermplasm)
                let result2 = await germplasmTypeProcess(maleParentGermplasm, null)
                let result3 = await germplasmTypeProcess(null, null)

                expect(result1 === childGermplasmType).to.be.true('null value returned')
                expect(result2 === childGermplasmType).to.be.true('null value returned')
                expect(result3 === childGermplasmType).to.be.true('null value returned')
        })
    })

    describe('taxonomyProcess', () => {
        const { taxonomyProcess } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully determine the taxonomy id of a child germplasm given matching taxonomy id of parents', 
            async () => {
                let femaleParentGermplasm = { 'taxonomyDbId' : 5 }
                let maleParentGermplasm = { 'taxonomyDbId' : 5 }
                let childTaxonomyId = 5

                let result = await taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)

                expect(result === childTaxonomyId).to.be.true('taxonomy id determined')
        })

        it('should successfully determine the taxonomy id of a child germplasm for maternal haploid induction process', 
            async () => {
                let femaleParentGermplasm = { 'taxonomyDbId' : 5 }
                let maleParentGermplasm = { 'taxonomyDbId' : 5 }
                let childTaxonomyId = 5

                let result = await taxonomyProcess(femaleParentGermplasm, maleParentGermplasm, true)

                expect(result === childTaxonomyId).to.be.true('taxonomy id determined')
        })

        it('should successfully determine the taxonomy id of a child germplasm for given different taxonomy id of parents', 
            async () => {
                let femaleParentGermplasm = { 'taxonomyDbId' : 5 }
                let maleParentGermplasm = { 'taxonomyDbId' : 3 }
                let childTaxonomyId = 4

                queryStub.resolves([{'taxonomyDbId':4}])

                let result = await taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)

                expect(queryStub.calledOnce).to.be.true('query has been called exactlyy once')
                expect(result === childTaxonomyId).to.be.true('taxonomy id determined')
        })

        it('should return false if query fails', async () => {
            let femaleParentGermplasm = { 'taxonomyDbId' : 5 }
            let maleParentGermplasm = { 'taxonomyDbId' : 3 }

            queryStub.rejects

            let result = await taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)

            expect(queryStub.calledOnce).to.be.true('query has been called exactlyy once')
            expect(result === false).to.be.true('query failed')
        })

        it('should return null if no taxonomy record is found', async () => {
            let femaleParentGermplasm = { 'taxonomyDbId' : 5 }
            let maleParentGermplasm = { 'taxonomyDbId' : 3 }

            queryStub.resolves([])

            let result = await taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)

            expect(queryStub.calledOnce).to.be.true('query has been called exactlyy once')
            expect(result === null).to.be.true('no taxonomy record found')
        })
    })

    describe('createSeedName', () => {
        const { createSeedName  } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let createSeedNameSpy

        beforeEach(function() {
            createSeedNameSpy = sinon.spy(createSeedName);
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully generate seed name', async () => {
            let seedPattern = {
                bulk: [{ type: 'field', plotInfoField: 'field1' }, { type: 'delimeter', value: '-' }],
                individual_ear: [{ type: 'field', plotInfoField: 'field2' }, { type: 'counter' }],
                single_plant: [{ type: 'field', plotInfoField: 'field3' }]
            }
            let plotRecord = {
                harvestMethod: 'bulk',
                field1: 'abc',
                field2: 'def',
                field3: 'ghi'
            }
            let seedCounter = 0
            let occurrenceCount = 1

            let result = await createSeedNameSpy(seedPattern, plotRecord, seedCounter, occurrenceCount)

            expect(createSeedNameSpy.calledOnce).to.be.true('function must be called once')
            expect(result !== null || result !== undefined).to.be.true('result should not be empty')
        })

        it('should return empty string if all variables are not provided', async () => {
            let result = await createSeedNameSpy()

            expect(createSeedNameSpy.calledOnce).to.be.true('function must be called once')
            expect(result === '').to.be.true('return empty string')
        })
    })

    describe('createPackageName', () => {
        const { createPackageName  } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let createPackageNameSpy

        beforeEach(function() {
            createPackageNameSpy = sinon.spy(createPackageName);
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully generate package name', async () => {
            let packagePattern = {
                pattern: [
                    { type: 'seedField', seedInfoField: 'seedField1' },
                    { type: 'plotField', plotInfoField: 'plotField1' },
                    { type: 'delimeter', value: '-' },
                    { type: 'free-text', value: 'pkg' },
                    { type: 'counter' }
                ]
            }
            let seedRecord = { seedField1: 'seedValue' }
            let plotRecord = { plotField1: 'plotValue' }

            let result = await createPackageNameSpy(packagePattern, seedRecord, plotRecord)

            expect(createPackageNameSpy.calledOnce).to.be.true('function must be called once')
            expect(result !== null && result !== undefined).to.be.true('result should not be empty')
        })

        it('should return empty string if only packagePattern is provided', async () => {
            let packagePattern = {'pattern' : []}
            let seedRecord = null
            let plotRecord = null

            let result = await createPackageNameSpy(packagePattern, seedRecord, plotRecord)

            expect(createPackageNameSpy.calledOnce).to.be.true('function must be called once')
            expect(result === '').to.be.true('return empty string')
        })
    })
    describe('getExperimentOccurrenceCount', () => {
        const { getExperimentOccurrenceCount } = require('../../helpers/seedlot/index')
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function () {
            queryStub = sandbox.stub(sequelize, 'query');
        })

        afterEach(function () {
            sandbox.restore()
        })

        it('should retrieve experiment occurrence count from the database', async () => {
            let experimentDbId = 1
            let expectedCount = 10

            queryStub.onFirstCall().resolves([{ 'count': expectedCount }])

            let result = await getExperimentOccurrenceCount(experimentDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == expectedCount).to.be.true('result should be equal to the expected experimentOccurenceCount')
        })

        it('should return null if the expected count is invalid', async () => {
            let experimentDbId = 1
            let invalidExpectedCount = NaN // Not a Number 

            queryStub.onFirstCall().resolves([{ 'count(1)': invalidExpectedCount }])

            let result = await getExperimentOccurrenceCount(experimentDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('result should be null')
        })

        it('should return null if no experiment occurrence found', async () => {
            let experimentDbId = 1

            queryStub.onFirstCall().resolves([])

            let result = await getExperimentOccurrenceCount(experimentDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('the result should be null if no experiment occurrence found')
        })

        it('should handle database query error', async () => {
            let experimentDbId = 1

            queryStub.onFirstCall().rejects()

            let result = await getExperimentOccurrenceCount(experimentDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('the result should be null if there was a database query error')
        })
    })

    describe('getSeed', () => {
        const { getSeed } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should retrieve seed from the database', async () => {
            let seedName = 'example_seed'
            let germplasmDbId = 1
            let sourceExperimentDbId = 2
            let sourceEntryDbId = 3
            let sourcePlotDbId = 4
            let sourceOccurrenceDbId = 5
            let sourceLocationDbId = 6
            let expectedSeed = { 'id': 123 }

            queryStub.onFirstCall().resolves([expectedSeed])

            let result = await getSeed(seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == expectedSeed).to.be.true('retrieve seed from database')
        })

        it('should return null if expectedSeed is null', async () => {
            let seedName = 'example_seed'
            let germplasmDbId = 1
            let sourceExperimentDbId = 2
            let sourceEntryDbId = 3
            let sourcePlotDbId = 4
            let sourceOccurrenceDbId = 5
            let sourceLocationDbId = 6
            let expectedSeed = null

            queryStub.onFirstCall().resolves([expectedSeed])

            let result = await getSeed(seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('should return null')
        })

        it('should return undefined if seed is not found', async () => {
            let seedName = undefined
            let germplasmDbId = 1
            let sourceExperimentDbId = 2
            let sourceEntryDbId = 3
            let sourcePlotDbId = 4
            let sourceOccurrenceDbId = 5
            let sourceLocationDbId = 6

            queryStub.onFirstCall().resolves([])

            let result = await getSeed(seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == undefined).to.be.true('the result should be undefined if seed is not found')
        })

        it('should handle database query error', async () => {
            let seedName = 'example_seed'
            let germplasmDbId = 1
            let sourceExperimentDbId = 2
            let sourceEntryDbId = 3
            let sourcePlotDbId = 4
            let sourceOccurrenceDbId = 5
            let sourceLocationDbId = 6

            queryStub.onFirstCall().rejects()

            let result = await getSeed(seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('the result should be null if there was a database query error')
        });
    })

    describe('generateGermplasmCode', () => {
        const { generateGermplasmCode } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully generate germplasm code', async () => {
            let expectedGermplasmCode = 'GE123456'
            queryStub.resolves([{ germplasmcode: expectedGermplasmCode }])

            let result = await generateGermplasmCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(queryStub.firstCall.args[0]).to.include('germplasm.generate_code')
            expect(queryStub.firstCall.args[1].type).to.equal(sequelize.QueryTypes.SELECT)
            expect(result == expectedGermplasmCode).to.be.true('successfully generate germplasm code')
        })

        it('should return null if an error occurs', async () => {
            queryStub.rejects(new Error('Database error'))

            let result = await generateGermplasmCode()

            expect(queryStub.calledOnce).to.be.true('must be called exactly once')
            expect(result == null).to.be.true('should return null')
        })
    })

    describe('getConfigValue', () => {
        const { getConfigValue } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getConfigValueSpy

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
            getConfigValueSpy = sinon.spy(getConfigValue)
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should succesfully retreive configuration value', async () => {
            let config = {
                key1: 'value1',
                key2: 'value2',
                default: 'defaultValue'
            }
            let key = 'key1'

            let result = await getConfigValueSpy(config, key)

            expect(getConfigValueSpy.calledOnce).to.be.true('function must be called once')
            expect(result == 'value1').to.be.true('should return config value')
        })

        it('should return null if key is not defined', async () => {
            let config = {}
            let key = undefined

            let result = await getConfigValueSpy(config, key)

            expect(getConfigValueSpy.calledOnce).to.be.true('function must be called once')
            expect(result == null).to.be.true('should return config value')
        })
    })

    describe('generateFamilyCode', () => {
        const { generateFamilyCode } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully generate family code with default prefix', async () => {
            queryStub.onFirstCall().resolves([{ 'familycode': 'FAM000000000001' }])

            let result = await generateFamilyCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === 'FAM000000000001').to.be.true('new family code has been generated')
        })

        it('should successfully generate family code with custom prefix', async () => {
            queryStub.onFirstCall().resolves([{ 'familycode': 'CUSTOM000000000001' }])

            let result = await generateFamilyCode('CUSTOM');

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === 'CUSTOM000000000001').to.be.true('new family code has been generated with custom prefix')
        })

        it('should successfully generate family code with EMPTY custom prefix', async () => {
            queryStub.onFirstCall().resolves([{ 'familycode': '000000000001' }])

            let result = await generateFamilyCode('');

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === '000000000001').to.be.true('new family code has been generated with EMPTY custom prefix')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects()

            let result = await generateFamilyCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('query failed')
        })

        it('should return null if no family code is generated', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await generateFamilyCode()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('empty return')
        })
    })

    describe('getCounterValue', () => {
        const { getCounterValue } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()

        beforeEach(function() {

        })

        afterEach(function() {
          sandbox.restore()
        })

        it('should return 1 when name is null', async () => {
            let result = await getCounterValue(null, '-');

            expect(result).to.equal(1)
        })

        it('should return NaN when name does not contain a number', async () => {
            let result = await getCounterValue('IR-A-B', '-');

            expect(isNaN(result)).to.be.true
        })

        it('should return the correct counter value with custom prefix', async () => {
            let result = await getCounterValue('CUSTOM-12345', '-', { prefix: 'CUSTOM-' });

            expect(result).to.equal(12346)
        })

        it('should return the correct counter value with custom suffix', async () => {
            let result = await getCounterValue('12345-CUSTOM-2', '-', { suffix: '-CUSTOM' })

            expect(result).to.equal(3)
        })
    })

    describe('getLastInNumberedSequence', () => {
        const { getLastInNumberedSequence } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should return the last value in the sequence with default options', async () => {
            queryStub.onFirstCall().resolves([{ 'field': 'IR-12345' }])

            let result = await getLastInNumberedSequence('schema', 'table', 'field', 'IR', '\\d+')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result).to.equal('IR-12345')
        })

        it('should return the last value in the sequence with custom delimiter', async () => {
            queryStub.onFirstCall().resolves([{ 'field': 'IR_12345' }])

            let result = await getLastInNumberedSequence('schema', 'table', 'field', 'IR', '\\d+', { delimiter: '_' })

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result).to.equal('IR_12345')
        })

        it('should return the last value in the sequence with custom prefix', async () => {
            queryStub.onFirstCall().resolves([{ 'field': 'PREFIX-12345' }])

            let result = await getLastInNumberedSequence('schema', 'table', 'field', 'PREFIX', '\\d+', { prefix: 'PREFIX-' })

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result).to.equal('PREFIX-12345')
        })

        it('should return the last value in the sequence with custom suffix', async () => {
            queryStub.onFirstCall().resolves([{ 'field': '12345-SUFFIX' }])

            let result = await getLastInNumberedSequence('schema', 'table', 'field', '', '\\d+', { suffix: '-SUFFIX' })

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result).to.equal('12345-SUFFIX')
        })

        it('should return the last value in the sequence with strict ending', async () => {
            queryStub.onFirstCall().resolves([{ 'field': 'IR-12345' }])

            let result = await getLastInNumberedSequence('schema', 'table', 'field', 'IR', '\\d+', {}, true)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result).to.equal('IR-12345')
        });

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects()

            const result = await getLastInNumberedSequence('schema', 'table', 'field', 'IR', '\\d+')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result).to.be.null
        });
    });

    describe('getConfigGeneric', () => {
        const { getConfigGeneric } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should return the config value for the crop program', async () => {
            queryStub.onFirstCall().resolves([{ configValue: 'crop_program_config' }])

            let result = await getConfigGeneric('CONFIG_BASE', 'CROP', 'PROGRAM')

            expect(queryStub.calledOnce).to.be.true('getHarvestConfig must be called exactly once')
            expect(result).to.equal('crop_program_config')
        })

        it('should return the default config value if crop program config is not found', async () => {
            queryStub.onFirstCall().resolves([])
            queryStub.onSecondCall().resolves([{ configValue: 'default_config' }])

            let result = await getConfigGeneric('CONFIG_BASE', 'CROP', 'PROGRAM')

            expect(queryStub.calledTwice).to.be.true('getHarvestConfig must be called twice')
            // expect(queryStub.firstCall.calledWith('CONFIG_BASE_CROP_PROGRAM')).to.be.true('correct config abbreviation')
            // expect(queryStub.secondCall.calledWith('CONFIG_BASE_CROP_DEFAULT')).to.be.true('correct default config abbreviation')
            expect(result).to.equal('default_config')
        });
    })

    describe('generateRecurrentRegex', () => {
        const { generateRecurrentRegex } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
        });

        afterEach(() => {
            sandbox.restore();
        });


        it('should handle patterns with only field types', async () => {
            const recurrentStr = 'test';
            const config = {
                delimiter_backcross_number: '-',
                pattern: [
                    [
                        { type: 'field' },
                        { type: 'field' },
                        { type: 'field' }
                    ],
                    [
                        { type: 'field' }
                    ]
                ]
            };

            const result = await generateRecurrentRegex(recurrentStr, config);

            const expectedLeftRegex = /^testtesttest$/g;
            expect(result.leftRegex.toString()).to.be.equal(expectedLeftRegex.toString());

            const expectedRightRegex = /^test$/g;
            expect(result.rightRegex.toString()).to.be.equal(expectedRightRegex.toString());
        });

        it('should handle empty patterns', async () => {
            const recurrentStr = 'test';
            const config = {
                delimiter_backcross_number: '-',
                pattern: [
                    [],
                    []
                ]
            };

            const result = await generateRecurrentRegex(recurrentStr, config);

            // Assert leftRegex
            const expectedLeftRegex = /^$/g;
            expect(result.leftRegex.toString()).to.be.equal(expectedLeftRegex.toString());

            // Assert rightRegex
            const expectedRightRegex = /^$/g;
            expect(result.rightRegex.toString()).to.be.equal(expectedRightRegex.toString());
        });
    });

    describe('getOneRecord', () => {
        const { getOneRecord } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should return one record', async () => {
            let schema = "sample_schema"
            let table = "sample_table"

            queryStub.resolves([schema, table, whereClause = ''])

            let result = await getOneRecord(schema, table, whereClause = '')

            expect(queryStub.calledOnce).to.be.true('query has been called exactlyy once')
            expect(result !== null).to.be.true('result has one')
        })

        it('should return empty if no record is found', async () => {
            let schema = ""
            let table = ""

            queryStub.resolves([schema, table, whereClause = ''])

            let result = await getOneRecord(schema, table, whereClause = '')

            expect(queryStub.calledOnce).to.be.true('query has been called exactlyy once')
            expect(result.length <= 0).to.be.true('no record found')
        })
    })

    describe('getFamilyInfo', () => {
        const { getFamilyInfo } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve family information', async () => {
            let familyDbId = 1
            let familyInformation = [{
                'familyDbId': 1,
                'familyCode': 'F123',
                'familyName': 'sampleFamily',
                'crossDbId': 2
            }]

            queryStub.onFirstCall().resolves(familyInformation)

            let result = await getFamilyInfo(familyDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == familyInformation[0]).to.be.true('retrieve family information')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getFamilyInfo()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('family information is null')
        })

        it('should return null if no record is found', async () => {
            let familyDbId
            let familyInformation

            queryStub.onFirstCall().resolves([familyInformation])

            let result = await getFamilyInfo(familyDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('family information is null')
        })

        it('should return undefined if query resolves to undefined', async () => {
            queryStub.onFirstCall().resolves(undefined)

            let result = await getFamilyInfo(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result).to.be.null('family information is null')
        })
    })

    describe('getGermplasmNameByType', () => {
        const { getGermplasmNameByType } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilder

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve germplasm name by type', async () => {
            const germplasmNameInfo = {
                germplasmDbId: 1,
                germplasmNameDbId: 10,
                nameValue: 'Sample Name',
                germplasmNameType: 'type1',
                germplasmNameStatus: 'active',
                germplasmNormalizedName: 'sample_name'
            }
            queryStub.onFirstCall().resolves([germplasmNameInfo])

            let result = await getGermplasmNameByType(1, 'type1')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == germplasmNameInfo).to.be.true('germplasm name information is correct')
        })

        it('should return undefined if no germplasm name record is found', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await getGermplasmNameByType(1, 'type1')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == undefined).to.be.true('no germplasm name record found')
        })

        it('should return germplasm name for a different name type', async () => {
            const germplasmNameInfo = {
                germplasmDbId: 2,
                germplasmNameDbId: 20,
                nameValue: 'Another Name',
                germplasmNameType: 'type2',
                germplasmNameStatus: 'standard',
                germplasmNormalizedName: 'another_name'
            }
            queryStub.onFirstCall().resolves([germplasmNameInfo])

            let result = await getGermplasmNameByType(2, 'type2')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == germplasmNameInfo).to.be.true('germplasm name information is correct for type2')
        })

        it('should handle multiple name statuses correctly', async () => {
            const germplasmNameInfo = {
                germplasmDbId: 3,
                germplasmNameDbId: 30,
                nameValue: 'Third Name',
                germplasmNameType: 'type3',
                germplasmNameStatus: 'standard',
                germplasmNormalizedName: 'third_name'
            }
            queryStub.onFirstCall().resolves([germplasmNameInfo])

            let result = await getGermplasmNameByType(3, 'type3')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == germplasmNameInfo).to.be.true('germplasm name information is correct for type3 with standard status')
        })
    })
})

describe('Seedlot Helper: Validation', () => {
    describe('updatePlotAdvancingHarvestStatus', () => {
        const { updatePlotAdvancingHarvestStatus } = require('../../helpers/seedlot/validation');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub

        beforeEach(function() {
            // 'resolves' returns the expected values of the stubbed functions
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully plot harvest status', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                }
            }
            let plotIdList = [ 1449323 ];
            let transaction = [
                {
                    "datasetDbId":"7170",
                    "transactionDbId":"229",
                    "variableDbId":"141",
                    "value":"2022-03-14",
                    "status":"new",
                    "isSuppressed":false,
                    "suppressRemarks":null,
                    "isComputed":false,
                    "entity":"plot_data",
                    "entityDbId":"1449323",
                    "dataUnit":"plot",
                    "remarks":null,
                    "notes":null,
                    "isVoid":false,
                    "collectionTimestamp":null,
                    "creationTimestamp":"2022-03-10T01:53:43.564Z",
                    "creatorDbId":1,
                    "creator":"Bims, Irri",
                    "modificationTimestamp":"2022-03-10T01:53:43.631Z",
                    "modifierDbId":null,
                    "modifier":null
                }
            ]

            queryStub.onFirstCall().resolves(['success'])

            const result = await updatePlotAdvancingHarvestStatus(req, res, plotIdList, transaction)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })

        it('should generate InternalError when the updateQuery fails', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                }
            }
            let plotIdList = [ 1449323 ];
            let transaction = [
                {
                    "datasetDbId":"7170",
                    "transactionDbId":"229",
                    "variableDbId":"141",
                    "value":"2022-03-14",
                    "status":"new",
                    "isSuppressed":false,
                    "suppressRemarks":null,
                    "isComputed":false,
                    "entity":"plot_data",
                    "entityDbId":"1449323",
                    "dataUnit":"plot",
                    "remarks":null,
                    "notes":null,
                    "isVoid":false,
                    "collectionTimestamp":null,
                    "creationTimestamp":"2022-03-10T01:53:43.564Z",
                    "creatorDbId":1,
                    "creator":"Bims, Irri",
                    "modificationTimestamp":"2022-03-10T01:53:43.631Z",
                    "modifierDbId":null,
                    "modifier":null
                }
            ]

            queryStub.onFirstCall().rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await updatePlotAdvancingHarvestStatus(req, res, plotIdList, transaction)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })

    describe('updateCrossHarvestStatus', () => {
        const { updateCrossHarvestStatus } = require('../../helpers/seedlot/validation');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub

        beforeEach(function() {
            // 'resolves' returns the expected values of the stubbed functions
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully plot harvest status', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                }
            }
            let transactionFileDbId = 123;
            let transaction = [
                {
                    "datasetDbId":"7170",
                    "transactionDbId":"229",
                    "variableDbId":"141",
                    "value":"2022-03-14",
                    "status":"new",
                    "isSuppressed":false,
                    "suppressRemarks":null,
                    "isComputed":false,
                    "entity":"cross_data",
                    "entityDbId":"1449323",
                    "dataUnit":"cross",
                    "remarks":null,
                    "notes":null,
                    "isVoid":false,
                    "collectionTimestamp":null,
                    "creationTimestamp":"2022-03-10T01:53:43.564Z",
                    "creatorDbId":1,
                    "creator":"Bims, Irri",
                    "modificationTimestamp":"2022-03-10T01:53:43.631Z",
                    "modifierDbId":null,
                    "modifier":null
                }
            ]

            queryStub.onFirstCall().resolves(['success'])

            const result = await updateCrossHarvestStatus(req, res, transactionFileDbId, transaction)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })

        it('should generate InternalError when the updateQuery fails', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                }
            }
            let transactionFileDbId = 123;
            let transaction = [
                {
                    "datasetDbId":"7170",
                    "transactionDbId":"229",
                    "variableDbId":"141",
                    "value":"2022-03-14",
                    "status":"new",
                    "isSuppressed":false,
                    "suppressRemarks":null,
                    "isComputed":false,
                    "entity":"cross_data",
                    "entityDbId":"1449323",
                    "dataUnit":"cross",
                    "remarks":null,
                    "notes":null,
                    "isVoid":false,
                    "collectionTimestamp":null,
                    "creationTimestamp":"2022-03-10T01:53:43.564Z",
                    "creatorDbId":1,
                    "creator":"Bims, Irri",
                    "modificationTimestamp":"2022-03-10T01:53:43.631Z",
                    "modifierDbId":null,
                    "modifier":null
                }
            ]

            queryStub.onFirstCall().rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await updateCrossHarvestStatus(req, res, transactionFileDbId, transaction)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })

    describe('getHarvestConfig', () => {
        const { getHarvestConfigValue } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox();
        let queryStub;
        let errorBuilderStub;

        beforeEach(function () {
            queryStub = sandbox.stub(sequelize, 'query');
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        });

        afterEach(function () {
            sandbox.restore();
        });

        it('should retrieve harvest config from the database', async () => {
            let configAbbrev = 'example_abbrev'; 

            queryStub.onFirstCall().resolves([{ 'configValue': 'mockValue' }]);

            let result = await getHarvestConfigValue(configAbbrev);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result).to.deep.equal({ 'configValue': 'mockValue' });
        });

        it('should return undefined if config abbreviation does not exist', async () => {
            let configAbbrev = 'non_existent_abbrev'; 

            queryStub.onFirstCall().resolves([]);

            let result = await getHarvestConfigValue(configAbbrev);

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once');
            expect(result).to.be.undefined('the result should be undefined if config abbreviation does not exist');
        });

        // TODO: Function being tested needs to be updated first
        xit('should handle database query error', async () => {
            let configAbbrev = 'example_abbrev'; 

            // Simulate a query error
            queryStub.onFirstCall().rejects(); 
            errorBuilderStub.resolves('database query error');

            let result = await getHarvestConfigValue(configAbbrev);

            expect(queryStub.calledOnce).to.be.false('query must NOT be called');
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once');
            expect(result).to.be.undefined('the result should be null if there was a database query error');
        });
    });

    describe('getScaleValueByAbbrev', () => {
        const { getScaleValueByAbbrev } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve scale value', async () => {
            let abbrev = 'scalevalue'
            let scalevalueinfo = [{'value': 123}]

            queryStub.onFirstCall().resolves(scalevalueinfo)

            let result = await getScaleValueByAbbrev( abbrev)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('germplasm name information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getScaleValueByAbbrev()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('query failed')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().rejects

            let result = await getScaleValueByAbbrev(1)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('query failed')
        })
    })

    describe('getGermplasmNameInfo', () => {
        const { getGermplasmNameInfo } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve germplasm name info', async () => {
            let germplasmDbId = 1
            let germplasmNameInfo = {
                'germplasmDbId':1,
                'germplasmNameDbId':2,
                'nameValue':'sampleValue',
                'germplasmNameType':'nameType',
                'germplasmNameStatus':'nameStatus',
                'germplasmNormalizedName':'normalizedName',
            }

            queryStub.onFirstCall().resolves(germplasmNameInfo)

            let result = await getGermplasmNameInfo(germplasmDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('germplasm name information is not empty')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().resolves([{}])

            let result = await getGermplasmNameInfo()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(Object.keys(result).length == 0).to.be.true('germplasm name info is not empty')
        })

        // TODO: Function being tested needs to be updated first
        xit('should return null if query fails', async () => {
            let germplasmDbId = 1

            queryStub.onFirstCall().rejects(new Error('error'))

            let result = await getGermplasmNameInfo([{}])

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result === null).to.be.true('query failed')
        })
    })

    describe('addModifierFields', () => {
        const { addModifierFields } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox();
        let queryStub;

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query');
        });

        afterEach(function() {
            sandbox.restore();
        });

        it('should successfully add modifier fields', async () => {
            let values = [];
            let userId = 1;

            queryStub.resolves([{}]);

            let result = await addModifierFields(values, userId);

            expect(queryStub.calledOnce).to.be.true;
            expect(result).to.deep.equal([
                "modification_timestamp = NOW()",
                `modifier_id = ${userId}`
            ]);
        });

        it('should return empty if no record is found', async () => {
            let values = [];
            let userId = 1;

            queryStub.resolves([]); 

            let result = await addModifierFields(values, userId);

            expect(queryStub.calledOnce).to.be.true;
            expect(result).to.be.an('array').that.is.empty;
        });

        it('should return null if userId is invalid', async () => {
            let values = [];
            let userId = null; 

            let result = await addModifierFields(values, userId);

            expect(result).to.be.null;
        });
    });

    describe('updateRecordById', () => {
        const { updateRecordById } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox();
        let queryStub, transactionStub
        let errorBuilderStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction').callsFake(() => ({
                commit: sinon.stub().resolves(),
                rollback: sinon.stub().resolves()
            }))
        })

        afterEach(() => {
            sandbox.restore()
        });

        let dbId = 1
        let schema = 'sample_schema'
        let table = 'sample_table'
        let values = ['field1 = value1', 'field2 = value2']
        let userId = 123

        it('should update record by id and return true', async () => {
            const expectedQuery = `
                UPDATE
                    sample_schema.sample_table
                SET
                    field1 = value1, field2 = value2, modification_timestamp = NOW(), modifier_id = 123
                WHERE
                    id = 1
            `.replace(/\s+/g, ' ').trim()

            queryStub.resolves(true)

            let result = await updateRecordById(dbId, schema, table, values, userId)

            expect(queryStub.calledOnce).to.be.true
            expect(transactionStub.calledOnce).to.be.true
            expect(result == true).to.be.true("return true")
        })

        it('should return false when an error occurs', async () => {
            queryStub.rejects(errorBuilderStub('Query failed'))

            let result = await updateRecordById(dbId, schema, table, values, userId)

            expect(queryStub.calledOnce).to.be.true
            expect(transactionStub.calledOnce).to.be.true
            expect(result == false).to.be.true("return false")
        })
    })

    describe('insertRecord', () => {
        const { insertRecord } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub, transactionStub, commitStub, rollbackStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction').callsFake(() => {
                return {
                    commit: commitStub = sandbox.stub().resolves(),
                    rollback: rollbackStub = sandbox.stub().resolves(),
                    finished: null
                }
            })
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should return one record', async () => {
            let schema = "sample_schema"
            let table = "sample_table"
            let columnValueArray = { 'name': 'jc' }
            let userId = 123

            queryStub.resolves([{ id: 123 }])

            const result = await insertRecord(schema, table, columnValueArray, userId);

            expect(queryStub.calledOnce).to.be.true
            expect(transactionStub.calledOnce).to.be.true
            expect(commitStub.calledOnce).to.be.true
            expect(result).to.deep.equal({ id: 123 })
        })

        it('should return null if an error occurs', async () => {
            let schema = "sample_schema"
            let table = "sample_table"
            let columnValueArray = { 'name': 'jc' }
            let userId = 123

            queryStub.rejects(new Error('Insert failed'))

            const result = await insertRecord(schema, table, columnValueArray, userId)

            expect(queryStub.calledOnce).to.be.true
            expect(transactionStub.calledOnce).to.be.true
            expect(rollbackStub.calledOnce).to.be.true
            expect(result).to.be.null
        })
    })

    describe('getScaleValueInfoByAbbrev', () => {
        const { getScaleValueInfoByAbbrev } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve scale value info', async () => {
            let abbrev = 'GERMPLASM_TYPE'
            let scalevalueinfo = [{
                'scaleValueDbId':123,
                'abbrev':456,
                'value':789,
                'displayName':'name',
                'description':'description'
            }]

            queryStub.onFirstCall().resolves(scalevalueinfo)

            let result = await getScaleValueInfoByAbbrev( abbrev)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('scale value info is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getScaleValueInfoByAbbrev()

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('scale value info is empty')
        })

        it('should return empty if no record is found', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await getScaleValueInfoByAbbrev(1)

            expect(result == null).to.be.true('scale value info is empty')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })
    })

    describe('getGermplasmAttribute', () => {
        const { getGermplasmAttribute } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve germplasm attribute', async () => {
            let germplasmDbId = 1
            let germplasmAttribute = [{
                'variableDbId':123,
                'dataValue':456,
                'dataQcCode':789
            }]

            queryStub.onFirstCall().resolves(germplasmAttribute)

            let result = await getGermplasmAttribute( germplasmDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('scale value info is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getGermplasmAttribute([{}])

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('scale value info is empty')
        })

        it('should return empty if no germplasm attribute found', async () => {
            let germplasmDbId = 1

            queryStub.onFirstCall().resolves([])

            let result = await getGermplasmAttribute(germplasmDbId)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == undefined).to.be.true('germplasm attribute is empty')
        })
    })

    describe('getVariableByAbbrev', () => {
        const { getVariableByAbbrev } = require('../../helpers/seedlot/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve scale value', async () => {
            let abbrev = 1
            let variableByAbbrev = [{
                'variableDbI':123,
                'variableAbbrev':456,
                'variableLabel':'sampleLabel',
                'variableName':'sampleName',
                'variableType':'sampleType',
                'variableStatus':'sampleStatus',
                'variableDisplayName':'sampleDisplayName'
            }]

            queryStub.onFirstCall().resolves(variableByAbbrev)

            let result = await getVariableByAbbrev( abbrev)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('scale value is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getVariableByAbbrev([{}])

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('scale value is empty')
        })

        it('should return empty if no record is found', async () => {
            let abbrev = 1

            queryStub.onFirstCall().resolves([])

            let result = await getVariableByAbbrev(abbrev)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == undefined).to.be.true('plot information is empty')
        })
    })
})