/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
let { knex } = require('../../config/knex')
const errorBuilder = require('../../helpers/error-builder')
const { iteratee } = require('lodash')
const logger = require('../../helpers/logger')
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

describe('List Members Helper', () => {
    describe('getMembersQuery', () => {
        const { getMembersQuery } = require('../../helpers/listsMembers/index');
        const sandbox = sinon.createSandbox()
        let knexStub

        beforeEach(function() {
            knexStub = sandbox.stub(knex, 'column')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should return only variable and list data if list is of type designation',
            async ()=> {
                let listDbId = 1234
                let type = 'designation'
                let fields = ''

                const result = await getMembersQuery(listDbId,type,fields)

                expect(result !== null).to.be.true('successfully extracted the list member search query')
        })

        it('should not return null values if list type is empty',
            async ()=> {
                let listDbId = 1234
                let type = ''
                let fields = ''

                const result = await getMembersQuery(listDbId,type,fields)

                expect(result !== null).to.be.true('successfully extracted list member search query')
        })

        it('should not return null values if fields is null',
            async ()=> {
                let listDbId = 1234
                let type = 'trait'
                let fields = null

                const result = await getMembersQuery(listDbId,type,fields)

                expect(result !== null).to.be.true('successfully extracted list member search query')
        })

        it('should return only variable and list data if list is of type trait',
            async ()=> {
                let listDbId = 1234
                let type = 'trait'
                let fields = ''

                const result = await getMembersQuery(listDbId,type,fields)

                expect(result !== null).to.be.true('successfully extracted the list member search query')
        })
    })

    describe('reorder', () => {
        const { reorder } = require('../../helpers/listsMembers/index');
        const sandbox = sinon.createSandbox()
        let transactionStub
        let errorBuilderStub
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorBuilderStub = sandbox.stub(errorBuilder,'getError')
            loggerMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should generate an InternalError if update of order_number fails',
            async ()=> {
                let req = {
                    headers:{
                        host: 'testHost'
                    }
                }
                let listDbId = 1234
                let addNullsCond = 'ASC'
                let userDbId = 1

                transactionStub.resolves({
                    rollback: sinon.stub()
                })
                queryStub.rejects()
                transactionStub.throws(new Error)
                errorBuilderStub.resolves('InternalError')
                loggerMessageStub.resolves('error')

                const result = await reorder(req,res,listDbId,addNullsCond,userDbId)

                expect(errorBuilderStub.called).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })
})