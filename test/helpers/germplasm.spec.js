/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
let { knex } = require('../../config/knex')
const errorBuilder = require('../../helpers/error-builder')
const { iteratee } = require('lodash')

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

describe('Germplasm Helper', () => {
    const { 
        normalizeText, normalizeTextComplex, 
        germplasmIdHasNoValue, germplasmIdHasInvalidFormat,
        getGermplasmRecord, getAncestryRelationRecordsQuery
    } = require('../../helpers/germplasm/index');
        
    describe('normalizeText', ()=>{
        const sandbox = sinon.createSandbox()
        let knexStub

        beforeEach(function() {
            knexStub = sandbox.stub(knex, 'column')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully normalize input designation value', async () => {
                let result = await normalizeText('irri 123')

                expect(result === 'IRRI123').to.be.true('successfully normalized the designation')
        })

        it('should return empty if no input value is provided', async () => {
            let result = await normalizeText()

            expect(result === '').to.be.true('successfully returned value')
        })

        it('should return empty if input value is blank', async () => {
            let result = await normalizeText('')

            expect(result === '').to.be.true('successfully returned value')
        })
    })

    describe('normalizeTextComplex', ()=>{
        const sandbox = sinon.createSandbox()
        let knexStub

        beforeEach(function() {
            knexStub = sandbox.stub(knex, 'column')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully normalize input designation value using the old complex process', 
            async () => {
                let result1 = await normalizeTextComplex(' ir64(bph) ')
                let result2 = await normalizeTextComplex('MALI105')
                let result3 = await normalizeTextComplex('KHAO-DAWK-MALI  105')
                let result4 = await normalizeTextComplex('ir-64(bph)')
                let result5 = await normalizeTextComplex('IR64(bph) 001')
                let result6 = await normalizeTextComplex(' ir(bph) ')
                let result7 = await normalizeTextComplex('IR64(5A)')
                let result8 = await normalizeTextComplex('IR64(BPH)')
                let result9 = await normalizeTextComplex('B 533A-1')
                let result0 = await normalizeTextComplex('IR64A( BPH )')

                expect(result1 === 'IR 64 (BPH)').to.be.true('successfully normalized the designation1')
                expect(result2 === 'MALI 105').to.be.true('successfully normalized the designation2')
                expect(result3 === 'KHAO DAWK MALI 105').to.be.true('successfully normalized the designation3')
                expect(result4 === 'IR-64 (BPH)').to.be.true('successfully normalized the designation4')
                expect(result5 === 'IR 64 (BPH) 1').to.be.true('successfully normalized the designation5')
                expect(result6 === 'IR (BPH)').to.be.true('successfully normalized the designation6')
                expect(result7 === 'IR 64 (5 A)').to.be.true('successfully normalized the designation7')
                expect(result8 === 'IR 64 (BPH)').to.be.true('successfully normalized the designation8')
                expect(result9 === 'B 533 A-1').to.be.true('successfully normalized the designation9')
                expect(result0 === 'IR 64 A (BPH)').to.be.true('successfully normalized the designation0')
        })

        it('should return empty if input value is blank', async () => {
            let result = await normalizeTextComplex('')

            expect(result === '').to.be.true('successfully returned value')
        })

        it('should return empty if no input value is provided', async () => {
            let result = await normalizeText()

            expect(result === '').to.be.true('successfully return value')
        })
    })

    describe('germplasmIdHasNoValue', ()=>{
        const sandbox = sinon.createSandbox()
        let knexStub

        beforeEach(function() {
            knexStub = sandbox.stub(knex, 'column')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should return TRUE if germplasm Id value is empty or null or undefined', async () => {
            let result = await germplasmIdHasNoValue(undefined)

            expect(result === true).to.be.true('successfully checked germplasm ID value')
        })

        it('should return FALSE if germplasm Id value is defined', async () => {
            let result = await germplasmIdHasNoValue('1')

            expect(result === false).to.be.true('successfully checked germplasm ID value')
        })
    })

    describe('germplasmIdHasInvalidFormat', ()=>{
        const sandbox = sinon.createSandbox()
        let knexStub

        beforeEach(function() {
            knexStub = sandbox.stub(knex, 'column')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should return TRUE if germplasm Id value is not an integer', async () => {
            let result = await germplasmIdHasInvalidFormat('integer')

            expect(result === true).to.be.true('successfully checked germplasm ID value format')
        })

        it('should return FALSE if germplasm Id value is an integer', async () => {
            let result = await germplasmIdHasInvalidFormat('1')

            expect(result === false).to.be.true('successfully checked germplasm ID value format')
        })
    })

    describe('getAncestryRelationRecordsQuery', ()=>{
        const sandbox = sinon.createSandbox()
        let knexStub

        beforeEach(function() {
            knexStub = sandbox.stub(knex, 'column')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should return empty if germplasm Id value is empty or null or undefined', async () => {
            let result = await getAncestryRelationRecordsQuery(undefined)

            expect(result === '').to.be.true('successfully returned empty query')
        })

        it('should return empty if germplasm Id value is not an integer', async () => {
            let result = await getAncestryRelationRecordsQuery('integer')

            expect(result === '').to.be.true('successfully returned empty query')
        })

        it('should return a query if germplasm ID is valid', async () => {
            let result = await getAncestryRelationRecordsQuery('1364510')

            expect(result != '').to.be.true('successfully returned non-empty query')
        })
    })
})