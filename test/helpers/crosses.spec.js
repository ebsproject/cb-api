/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const patternHelper = require('../../helpers/patternGenerator');

  describe('Crosses Helper', () => {
    describe('getGermplasmInfo', () => {
        const { getGermplasmInfo } = require('../../helpers/crosses/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve germplasm information', async () => {
            let germplasmName = 'name'
            let germplasmInfo = [{
                'germplasmDbId':123,
                'parentage':'Parentage',
                'generation':'Generation',
                'germplasmState':'GermplasmState'
            }]

            queryStub.onFirstCall().resolves(germplasmInfo)

            let result = await getGermplasmInfo(germplasmName)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('germplasm information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getGermplasmInfo('')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('germplasm information is empty')
        })

        it('should return empty if no record is found', async () => {
            let germplasmName = 'name'

            queryStub.onFirstCall().resolves([])

            let result = await getGermplasmInfo(germplasmName)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == undefined).to.be.true('germplasm information is empty')
        })
    })

    describe('generateCode', () => {
        const { generateCode } = require('../../helpers/crosses/index'); 
        const sandbox = sinon.createSandbox();
        let getEntityValueStub, getCounterStub, getPaddedNumberStub;

        beforeEach(function() {
            getEntityValueStub = sandbox.stub(patternHelper, 'getEntityValue');
            getCounterStub = sandbox.stub(patternHelper, 'getCounter');
            getPaddedNumberStub = sandbox.stub(patternHelper, 'getPaddedNumber');
        });

        afterEach(function() {
            sandbox.restore();
        });

        it('should generate code with YEAR, SEASON_CODE, and SITE_CODE', async () => {
            const pattern = [
                { is_variable: true, value: 'YEAR' },
                { is_variable: true, value: 'SEASON_CODE' },
                { is_variable: true, value: 'SITE_CODE' },
            ];
            const experimentYear = 2024;
            const seasonDbId = 1;
            const siteDbId = 2;

            getEntityValueStub.withArgs('tenant', 'season', 'season_code', seasonDbId).resolves({ season_code: 'S1' });
            getEntityValueStub.withArgs('place', 'geospatial_object', 'geospatial_object_code', siteDbId)
            .resolves({ geospatial_object_code: 'SITE2' });

            const result = await generateCode(pattern, experimentYear, seasonDbId, siteDbId);

            expect(getEntityValueStub.calledTwice).to.be.true;
            expect(result).to.equal('2024S1SITE2');
        });

        it('should handle mixed static and dynamic parts correctly', async () => {
            const pattern = [
                { is_variable: true, value: 'YEAR' },
                { is_variable: false, value: '-' },
                { is_variable: true, value: 'SEASON_CODE' },
                { is_variable: false, value: '-' },
                { is_variable: false, value: 'COUNTER', max_value: 'YEAR|SEASON', zero_padding: true, digits: 3 }
            ];
            const experimentYear = 2024;
            const seasonDbId = 1;
            const siteDbId = 2;
            const startingCount = 10;

            getEntityValueStub.withArgs('tenant', 'season', 'season_code', seasonDbId).resolves({ season_code: 'S1' });
            getCounterStub.resolves(5);
            getPaddedNumberStub.withArgs(5, true, 3).resolves('005');

            const result = await generateCode(pattern, experimentYear, seasonDbId, siteDbId, startingCount);

            expect(getEntityValueStub.calledOnce).to.be.true;
            expect(getCounterStub.calledOnce).to.be.true;
            expect(getPaddedNumberStub.calledOnce).to.be.true;
            expect(result).to.equal('2024-S1-005');
        });

        it('should return an empty string if pattern is empty', async () => {
            const pattern = [];
            const experimentYear = 2024;
            const seasonDbId = 1;
            const siteDbId = 2;
            const startingCount = 10;

            const result = await generateCode(pattern, experimentYear, seasonDbId, siteDbId, startingCount);

            expect(result).to.equal('');
        });
    });


    describe('checkBreedingExperiment', () => {
        const { checkBreedingExperiment } = require('../../helpers/crosses/index');
        const sandbox = sinon.createSandbox()
        let queryStu

        beforeEach(function () {
            queryStub = sandbox.stub(sequelize, 'query');
        });

        afterEach(function () {
            sandbox.restore()
        });

        it('should successfully check breeding experiment for a given experiment ID', async () => {
            let experimentData = {
                'cropCode': 'RICE',
                'isBreeding': true,
                'isHybrid': false
            };

            queryStub.onFirstCall().resolves([experimentData])

            let result = await checkBreedingExperiment(1)

            expect(queryStub.calledOnce).to.be.true
            expect(result).to.deep.equal(experimentData)
        });

        it('should return null if no experiment is found', async () => {
            queryStub.onFirstCall().resolves([])

            let result = await checkBreedingExperiment(1)

            expect(queryStub.calledOnce).to.be.true
            expect(result).to.be.null
        });

        it('should handle null experiment ID and return null without querying', async () => {
            let experimentDbId = null

            let result = await checkBreedingExperiment(experimentDbId)

            expect(queryStub.notCalled).to.be.true
            expect(result).to.be.null
        });

        it('should handle invalid experiment ID and return null', async () => {
            let experimentDbId = 'invalidID'

            let result = await checkBreedingExperiment(experimentDbId)

            expect(queryStub.notCalled).to.be.true
            expect(result).to.be.null
        })
    })
})