/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const seedlotHelper = require('../../helpers/seedlot/index')

// Mock the transaction invoked after querying the database
const transactionMock = {
    finished: "",
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    },
    // Call this function in afterEach() to reset the value
    // of the property "finished"
    reset: function() {
        this.finished = ''
    }
}

describe('Package Helper', () => {
    describe('Create Package Log', () => {
        const { createPackageLog } = require('../../helpers/package/index')
        const sandbox = sinon.createSandbox()
        let queryStub
        let transactionStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize,'query')
            transactionStub = sandbox.stub(sequelize,'transaction')
        })

        afterEach(() => {
            sandbox.restore()
            transactionMock.reset()
        })

        it('should return null if the insertion fails', async () => {
            // mock input
            let packageDbId = 1234
            let packageQuantity = 5
            let packageUnit = 'g'
            let creatorDbId = 99
            let packageTransactionType = 'deposit'

            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).rejects()

            let result = await createPackageLog(packageDbId,packageQuantity,packageUnit,creatorDbId,packageTransactionType)

            expect(result).to.be.null()
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called exactly once')
            expect(queryStub.callCount).to.equal(0, 'query count must be 0')
        })

        it('should successfully return the id when the package log has been inserted', async () => {
            // mock input
            let packageDbId = 1234
            let packageQuantity = 5
            let packageUnit = 'g'
            let creatorDbId = 99
            let mockId = 2745

            transactionStub.onCall(0).resolves([{id:mockId}])
            queryStub.onCall(0).resolves([{id:mockId}])

            let result = await createPackageLog(packageDbId,packageQuantity,packageUnit,creatorDbId)

            expect(result).to.be.eq(mockId)
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called exactly once')
            expect(queryStub.callCount).to.equal(0, 'query call count must be 0')
        })

        it('should return null when transaction creation fails', async () => {
            // mock input
            let packageDbId = 1234
            let packageQuantity = 5
            let packageUnit = 'g'
            let creatorDbId = 99

            transactionStub.onCall(0).rejects()

            let result = await createPackageLog(packageDbId,packageQuantity,packageUnit,creatorDbId)

            expect(result).to.be.null()
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called exactly once')
            expect(queryStub.callCount).to.equal(0, 'query must NEVER be called')
        })
    })

    describe('Get Package Log Transaction Type', () => {
        const { getPackageLogTransactionType } = require('../../helpers/package/index')
        const sandbox = sinon.createSandbox()
        let getScaleValueByAbbrevStub

        beforeEach(() => {
            getScaleValueByAbbrevStub = sandbox.stub(seedlotHelper,'getScaleValueByAbbrev')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should return null if the current quantity is equal to the new quantity', async () => {
            let currentQuantity = 5
            let newQuantity = 5

            let result = await getPackageLogTransactionType(currentQuantity, newQuantity)

            expect(result).to.be.null('result should be null')
            expect(getScaleValueByAbbrevStub.callCount).to.be.equal(0, 'getScaleValueByAbbrev must NEVER be called')
        })

        it('should return transaction type = "deposit" and pqValue = newQuantity - currentQuantity if the currentQuantity is less than the newQuantity', async () => {
            let currentQuantity = 5
            let newQuantity = 13

            getScaleValueByAbbrevStub.onCall(0).resolves('deposit')

            let result = await getPackageLogTransactionType(currentQuantity, newQuantity)

            expect(result === null).to.be.false('result must NOT be null')
            expect(result.transactionType).to.be.equal('deposit', 'the transaction type should be deposit')
            expect(result.pqValue).to.be.equal(8, 'the pqValue type should be equal to 8')
            expect(getScaleValueByAbbrevStub.callCount).to.be.equal(1, 'getScaleValueByAbbrev must be called exactly once')
        })

        it('should return transaction type = "withdraw" and pqValue = currentQuantity - newQuantity if the currentQuantityy is greater than the newQuantity', async () => {
            let currentQuantity = 15
            let newQuantity = 4

            getScaleValueByAbbrevStub.onCall(0).resolves('withdraw')

            let result = await getPackageLogTransactionType(currentQuantity, newQuantity)

            expect(result === null).to.be.false('result must NOT be null')
            expect(result.transactionType).to.be.equal('withdraw', 'the transaction type should be withdraw')
            expect(result.pqValue).to.be.equal(11, 'the pqValue type should be equal to 11')
            expect(getScaleValueByAbbrevStub.callCount).to.be.equal(1, 'getScaleValueByAbbrev must be called exactly once')
        })
    })
})