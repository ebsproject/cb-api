/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')
const userValidator = require('../../helpers/person/validator')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}
const transactionMock = {
    finished: "",
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    }
}

describe('Seed Attributes', () => {
    describe('/POST seed-attributes', () => {
        const { post } = require('../../routes/v3/seed-attributes/index');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the request body'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when records'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: []
                }
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequest when records'
            + ' is undefined', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: { }
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequest')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a InternalError when transaction creation'
            + ' fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should generate a BadRequestError when required parameters'
            + ' are missing', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when seedDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "abc",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when validation fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:0}])
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should generate a BadRequestError when dataQcCode'
            + ' is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1",
                            dataQcCode: "INVALID"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate InternalError when variable info retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate BadRequestError when dataValue'
            + ' is not included in the scale values (if any)', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).resolves([{
                variableDbId: 1001,
                scaleValues: [ 'x', 'y', 'z' ]
            }])

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a InternalError when insertion fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).resolves([{
                variableDbId: 1001,
                scaleValues: [ 'val1', 'val2', 'val3' ]
            }])
            queryStub.onCall(2).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully insert germplasm attribute record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).resolves([{
                variableDbId: 1001,
                scaleValues: [ 'val1', 'val2', 'val3' ]
            }])
            queryStub.onCall(2).resolves([[{id:2003}]])

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })
    })
})