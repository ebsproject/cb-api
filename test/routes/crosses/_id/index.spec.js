/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize.js')
const errors = require('restify-errors')
const errorBuilder = require('../../../../helpers/error-builder.js')
const tokenHelper = require('../../../../helpers/auth/token.js')
const logger = require('../../../../helpers/logger/index.js')
const validator = require('validator')
const userValidator = require('../../../../helpers/person/validator.js')
const { expect } = require('chai')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Crosses', () => {
    describe('GET crosses/:id', () => {
        const { get } = require('../../../../routes/v3/crosses/_id/index.js');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let isIntStub
        let errorBuilderStub
        let queryStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            isIntStub = sandbox.stub(validator, 'isInt')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (400019) when crossDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': 'notInt'
            }

            //mock
            isIntStub.returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await get(req, res, next)

            //assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400019)
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await get(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate InternalError when the query for retrieving ' +
        'the cross records fails', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            //act
            const result = await get(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            
        })

        it('should throw a NotFoundError (404004) when retrieving cross record ' +
        'from database returns empty', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            queryStub.resolves([])
            errorBuilderStub.resolves('NotFoundError')

            //act
            const result = await get(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404004)
        })

        it('should successfully retrieve a cross record', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            queryStub.resolves([{ 'crossDbId': '123' }])

            //act
            const result = await get(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })
    })

    describe('PUT crosses/:id', () => {
        const { put } = require('../../../../routes/v3/crosses/_id/index.js');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let transactionStub
        let isIntStub
        let isAdminStub
        let errorStub
        let errorBuilderStub
        let queryStub
        let logMessageStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            isIntStub = sandbox.stub(validator, 'isInt')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            logMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (400019) when crossDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': 'notInt'
            }

            //mock
            isIntStub.returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            //assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400019)
        })

        it('should throw a BadRequestError (400009) when the request body is not set', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            //assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            req.params = {
                'id': '123'
            }
            req.body = {}

            isIntStub.returns(true)
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate InternalError when the query for retrieving ' +
        'the cross records fails', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')
            //act
            const result = await put(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw a NotFoundError (404004) when retrieving cross record ' +
        'from database returns empty', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([])
            errorBuilderStub.resolves('NotFoundError')

            //act
            const result = await put(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404004)
        })

        it('should throw a BadRequestError when invalid value for validateStatus', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'validateStatus': 'notBoolean'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            errorStub.resolves('error')

            //act
            const result = await put(req, res, next)

            //assert
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should throw a BadRequestError (400238) when germplasmDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'germplasmDbId': 'notInt'
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            isIntStub.onCall(1).returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            expect(isIntStub.callCount).to.equal(2,'isInt must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400238)
        })

        it('should throw a BadRequestError (400236) when seedDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'seedDbId': 'notInt',
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            isIntStub.onCall(1).returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            expect(isIntStub.callCount).to.equal(2,'isInt must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400236)
        })

        it('should throw a BadRequestError (400020) when entryDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'entryDbId': 'notInt',
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            isIntStub.onCall(1).returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            expect(isIntStub.callCount).to.equal(2,'isInt must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400020)
        })

        it('should throw a BadRequestError when invalid value for harvestStatus', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'harvestStatus': 'test harvest'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            queryStub.onCall(1).resolves([[{'scaleValue': 'sample_value'}]])
            errorStub.resolves('error')

            //act
            const result = await put(req, res, next)

            //assert
            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should throw a BadRequestError (400021) when entryListDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'entryListDbId': 'notInt',
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            isIntStub.onCall(1).returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            expect(isIntStub.callCount).to.equal(2,'isInt must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400021)
        })

        it('should throw a BadRequestError (400229) when occurrenceDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'occurrenceDbId': 'notInt',
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            isIntStub.onCall(1).returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            expect(isIntStub.callCount).to.equal(2,'isInt must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400229)
        })

        it('should throw a BadRequestError (400015) when inputCount doesn\'t match with validateCount', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross'
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            queryStub.onCall(1).resolves([{'count': 5}])
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            expect(isIntStub.callCount).to.equal(1,'isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should successfully update a cross record', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross',
                'germplasmDbId': '123',
                'seedDbId': '123',
                'entryDbId': '123',
                'entryListDbId': '123',
                'occurrenceDbId': '123',
                'remarks': 'string'
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'experimentDbId': '123',
                'crossName': 'sample',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO_HARVEST',
                'germplasmDbId': '456',
                'seedDbId': '456',
                'entryDbId': '456',
                'entryListDbId': '456',
                'occurrenceDbId': '456'
            }])
            isIntStub.onCall(1).returns(true)
            isIntStub.onCall(2).returns(true)
            isIntStub.onCall(3).returns(true)
            isIntStub.onCall(4).returns(true)
            isIntStub.onCall(5).returns(true)
            queryStub.onCall(1).resolves([{'count': 6}])
            transactionStub.resolves([{'success': true}])

            //act
            const result = await put(req, res, next)

            expect(transactionStub.calledOnce).to.be.true('transaction should be called once')          
        })

        it('should throw an InternalError (500003) if other errors were encountered', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross'
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'crossDbId': '123',
                'crossMethod': 'selfing',
                'harvestStatus': 'NO HARVEST',
                'experimentDbId': '123',
                'entryListDbId': '123',
                'germplasmDbId': '123'
            }])
            queryStub.onCall(1).resolves([{'count': 1}])            
            transactionStub.throws(new Error('dummy error'))
            logMessageStub.resolves('Dummy error')
            errorBuilderStub.resolves('InternalError')
            
            //act
            const result = await put(req, res, next)

            expect(transactionStub.calledOnce).to.be.true('transaction should be called once')
            expect(logMessageStub.calledOnce).to.be.true('logMessage must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500003)        
        })
    })

    describe('DELETE crosses/:id', () => {
        const { delete:del } = require('../../../../routes/v3/crosses/_id/index.js');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let transactionStub
        let isIntStub
        let isAdminStub
        let errorBuilderStub
        let queryStub
        let logMessageStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            isIntStub = sandbox.stub(validator, 'isInt')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            logMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (400019) when crossDbId is not an integer', async () => {
            //arrange
            req.params = {
                'id': 'notInt'
            }

            //mock
            isIntStub.returns(false)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await del(req, res, next)

            //assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400019)
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            req.params = {
                'id': '123'
            }
            req.body = {}

            isIntStub.returns(true)
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            const result = await del(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate InternalError when the query for retrieving ' +
        'the cross records fails', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            queryStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')
            //act
            const result = await del(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw a NotFoundError (404005) when retrieving cross record ' +
        'from database returns empty', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            queryStub.onCall(0).resolves([])
            errorBuilderStub.resolves('NotFoundError')

            //act
            const result = await del(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404005)
        })

        it('should throw a UnauthorizedError (401025) when retrieving person record ' +
        'and user is not a program team member', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'creatorDbId': '123',
                'experimentDbId': '123'
            }])
            queryStub.onCall(1).resolves([{}])
            errorBuilderStub.resolves('UnauthorizedError')

            //act
            const result = await del(req, res, next)

            //assert
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401025)
        })

        it('should throw a UnauthorizedError (401026) when retrieving person record ' +
        'and user is not a program team member or a producer', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'creatorDbId': '123',
                'experimentDbId': '123'
            }])
            queryStub.onCall(1).resolves([{
                'id': '456',
                'role': 'test'
            }])
            errorBuilderStub.resolves('UnauthorizedError')

            //act
            const result = await del(req, res, next)

            //assert
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401026)
        })

        it('should successfully delete a cross record', async () => {
            //arrange
            req.params = {
                'id': '123'
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'creatorDbId': '1',
                'experimentDbId': '123'
            }])
            queryStub.onCall(1).resolves([{
                'id': '1', 
                'role': 'collaborator'
            }])
            transactionStub.resolves([{'success': true}])

            //act
            const result = await del(req, res, next)

            expect(transactionStub.calledOnce).to.be.true('transaction should be called once')          
        })

        it('should throw an InternalError (500003) if other errors were encountered', async () => {
            //arrange
            req.params = {
                'id': '123'
            }
            req.body = {
                'crossName': 'crossName',
                'crossMethod': 'single cross'
            }

            //mock
            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            queryStub.onCall(0).resolves([{
                'creatorDbId': '1',
                'experimentDbId': '123'
            }])
            queryStub.onCall(1).resolves([{
                'id': '1', 
                'role': 'collaborator'
            }])      
            transactionStub.throws(new Error('dummy error'))
            logMessageStub.resolves('Dummy error')
            errorBuilderStub.resolves('InternalError')
            
            //act
            const result = await del(req, res, next)

            expect(transactionStub.calledOnce).to.be.true('transaction should be called once')
            expect(logMessageStub.calledOnce).to.be.true('logMessage must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)        
        })
    })
})