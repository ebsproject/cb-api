/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const errors = require('restify-errors')
const tokenHelper = require('../../helpers/auth/token')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}
const transactionMock = {
    finished: '',
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    }
}

describe('Seed Transfers', () => {
    const { get, put } = require('../../routes/v3/seed-transfers/_id/index');
    const st = require('../../routes/v3/seed-transfers/_id/index');

    describe('POST /seed-transfers', () => {
        const { post } = require('../../routes/v3/seed-transfers/index');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        /*** PRELIMINARY CHECKS ***/

        it('should generate a BadRequestError(401002) when userDbId is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }
            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError(400009) when request body does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError(400005) when records data does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: { }
            }
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError(400005) when records data is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: []
                }
            }
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
            expect(queryStub.called).to.be.false('query must not be called')
        })


        /*** REQUEST BODY REQUIRED FIELDS VALIDATIONS ***/

        it('should generate a BadRequest with custom message when seedTransferStatus is missing', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            // seedTransferStatus: "draft"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Required parameter is missing. Ensure that seedTransferStatus field is not empty.')
        })

        it('should generate a BadRequestError(400015) when seedTransferStatus is an invalid scale value', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedTransferStatus: "invalid_status"
                        }
                    ]
                }
            }
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:0}])   // validation count if successful should be 1

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })

        /*** REQUEST BODY OPTIONAL FIELDS VALIDATIONS ***/

        it('should generate a BadRequest with custom message when senderProgramDbId is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "senderProgramDbId": "string"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, sender program ID must be an integer.')
        })

        it('should generate a BadRequest with custom message when receiverProgramDbId is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "receiverProgramDbId": "string"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, receiver program ID must be an integer.')
        })

        it('should generate a BadRequest with custom message when sourceListDbId is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "sourceListDbId": "string"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, source list ID must be an integer.')
        })

        it('should generate a BadRequest with custom message when stagingListDbId is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "stagingListDbId": "string"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, staging list ID must be an integer.')
        })

        it('should generate a BadRequest with custom message when destinationListDbId is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "destinationListDbId": "string"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, destination list ID must be an integer.')
        })

        it('should generate a BadRequest with custom message when packageQuantity is not a number', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "packageQuantity": "string"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, package quantity must be a float.')
        })

        it('should generate a BadRequest with custom message when packageQuantity is negative', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "packageQuantity": "-100.25"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid number, negative values are not allowed for the package quantity.')
        })

        it('should generate a BadRequestError(400015) when packageUnit is an invalid scale value', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            seedTransferStatus: "draft",
                            packageUnit: "invalid_unit"
                        }
                    ]
                }
            }
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])   // validation count if successful should be 2

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called')
        })

        it('should generate a BadRequest with custom message when isSendWholePackage is not a boolean', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "isSendWholePackage": "partial"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, isSendWholePackage must be boolean.')
        })


        /*** DATABASE TRANSACTION ERRORS ***/

        it('should generate an InternalError(500004) if INSERT failed', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])   // validation query
            queryStub.onCall(1).rejects()               // insert query
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError(500001) if some other transaction error occurs', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "seedTransferStatus": "draft",
                            "isSendWholePackage": true  // non-string boolean value results in Error 500
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })


        /*** HAPPY PATHS ***/

        it('should successfully create a new seed transfer record when required fields are valid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            senderProgramDbId: "101",
                            receiverProgramDbId: "102",
                            sourceListDbId: "1700",
                            stagingListDbId: "1701",
                            destinationListDbId: "1702",
                            seedTransferStatus: "draft",
                            packageQuantity: "0",
                            packageUnit: "g",
                            isSendWholePackage: "false"
                        }
                    ]
                }
            }
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:7}])   // validation query
            queryStub.onCall(1).resolves([[{'seedTransferDbId':"13"}]])

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must not be called')
        })
    })

    describe('/GET seed-transfers/{id}', () => {
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate an InternalError when no id is provided', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {}
            }

            errorBuilderStub.onCall(0).resolves("InternalError")

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if id provided is not an integer', 
            async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    params: {
                        id: "abc"
                    }
                }

                errorBuilderStub.onCall(0).resolves("BadRequestError")

                const result = await get(req, res,next)

                expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError if the query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "5"
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves("InternalError")

            const result = await get(req, res,next)

            expect(queryStub.callCount).to.be.equal(1, 'query is called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if no record is retrieved', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "5"
                }
            }

            queryStub.onCall(0).resolves([])
            errorBuilderStub.onCall(0).resolves("BadRequestError")

            const result = await get(req, res,next)

            expect(queryStub.callCount).to.be.equal(1, 'query is called once')
        })

        it('should successfully retrieve seed transfer record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "5"
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'seedTransferDbId':'5'
                }
            ])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equal(1, 'query is called once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder is not called')
        })
    })

    describe('/PUT seed-transfers/{id}', () => {
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            transactionMock.finished = ""
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            tokenHelperStub.onCall(0).resolves(null)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the seedTransferDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the req.body'
            + ' is not specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a InternalError when the seed transfer retrieval '
            + ' fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {

                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the seed transfer'
            + ' does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {

                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when all values are not equal to the current values', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    senderProgramDbId: '102',
                    senderDbId: '2',
                    receiverProgramDbId: '103',
                    receiverDbId: '3',
                    sourceListDbId: '4',
                    stagingListDbId: '5',
                    destinationListDbId: '6',
                    seedTransferStatus: 'created',
                    packageQuantity: '2',
                    packageUnit: 'kg',
                    isSendWholePackage: 'false',
                    errorLog: 'some new error here',
                    remarks: 'new remarks'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:7}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when all values are equal to the current values', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    senderProgramDbId: '101',
                    senderDbId: '1',
                    receiverProgramDbId: '102',
                    receiverDbId: '2',
                    sourceListDbId: '1',
                    stagingListDbId: '2',
                    destinationListDbId: '3',
                    seedTransferStatus: 'draft',
                    packageQuantity: '1',
                    packageUnit: 'g',
                    isSendWholePackage: 'true',
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:7}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when no values are specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: { }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when senderProgramDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    senderProgramDbId: 'not an integer'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when senderProgramDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    senderProgramDbId: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when senderDbId'
        + ' is not an integer', async () => {
        req = {
            headers: {
                host: 'testHost'
            },
            params: {
                id: "456"
            },
            body: {
                senderDbId: 'not an integer'
            }
        }

        seedTransfer = [
            {
                senderProgramDbId: 101,
                senderDbId: 1,
                receiverProgramDbId: 102,
                receiverDbId: 2,
                sourceListDbId: 1,
                stagingListDbId: 2,
                destinationListDbId: 3,
                seedTransferStatus: 'draft',
                packageQuantity: 1,
                packageUnit: 'g',
                isSendWholePackage: true,
                errorLog: 'error here',
                remarks: 'remarks'
            }
        ]

        tokenHelperStub.onCall(0).resolves(123)
        queryStub.onCall(0).resolves(seedTransfer)
        errorBuilderStub.onCall(0).resolves('BadRequestError')
        transactionStub.onCall(0).resolves(transactionMock)

        const result = await put(req, res, next)

        expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
        expect(queryStub.callCount).to.equal(1, 'query must be called once')
        expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
        expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when senderDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    senderDbId: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when receiverProgramDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    receiverProgramDbId: 'not an integer'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when receiverProgramDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    receiverProgramDbId: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when receiverDbId'
        + ' is not an integer', async () => {
        req = {
            headers: {
                host: 'testHost'
            },
            params: {
                id: "456"
            },
            body: {
                receiverDbId: 'not an integer'
            }
        }

        seedTransfer = [
            {
                senderProgramDbId: 101,
                senderDbId: 1,
                receiverProgramDbId: 102,
                receiverDbId: 2,
                sourceListDbId: 1,
                stagingListDbId: 2,
                destinationListDbId: 3,
                seedTransferStatus: 'draft',
                packageQuantity: 1,
                packageUnit: 'g',
                isSendWholePackage: true,
                errorLog: 'error here',
                remarks: 'remarks'
            }
        ]

        tokenHelperStub.onCall(0).resolves(123)
        queryStub.onCall(0).resolves(seedTransfer)
        errorBuilderStub.onCall(0).resolves('BadRequestError')
        transactionStub.onCall(0).resolves(transactionMock)

        const result = await put(req, res, next)

        expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
        expect(queryStub.callCount).to.equal(1, 'query must be called once')
        expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
        expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when receiverDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    receiverDbId: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when sourceListDbId'
        + ' is not an integer', async () => {
        req = {
            headers: {
                host: 'testHost'
            },
            params: {
                id: "456"
            },
            body: {
                sourceListDbId: 'not an integer'
            }
        }

        seedTransfer = [
            {
                senderProgramDbId: 101,
                senderDbId: 1,
                receiverProgramDbId: 102,
                receiverDbId: 2,
                sourceListDbId: 1,
                stagingListDbId: 2,
                destinationListDbId: 3,
                seedTransferStatus: 'draft',
                packageQuantity: 1,
                packageUnit: 'g',
                isSendWholePackage: true,
                errorLog: 'error here',
                remarks: 'remarks'
            }
        ]

        tokenHelperStub.onCall(0).resolves(123)
        queryStub.onCall(0).resolves(seedTransfer)
        transactionStub.onCall(0).resolves(transactionMock)

        const result = await put(req, res, next)

        expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
        expect(queryStub.callCount).to.equal(1, 'query must be called once')
        expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when sourceListDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    sourceListDbId: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when stagingListDbId'
        + ' is not an integer', async () => {
        req = {
            headers: {
                host: 'testHost'
            },
            params: {
                id: "456"
            },
            body: {
                stagingListDbId: 'not an integer'
            }
        }

        seedTransfer = [
            {
                senderProgramDbId: 101,
                senderDbId: 1,
                receiverProgramDbId: 102,
                receiverDbId: 2,
                sourceListDbId: 1,
                stagingListDbId: 2,
                destinationListDbId: 3,
                seedTransferStatus: 'draft',
                packageQuantity: 1,
                packageUnit: 'g',
                isSendWholePackage: true,
                errorLog: 'error here',
                remarks: 'remarks'
            }
        ]

        tokenHelperStub.onCall(0).resolves(123)
        queryStub.onCall(0).resolves(seedTransfer)
        transactionStub.onCall(0).resolves(transactionMock)

        const result = await put(req, res, next)

        expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
        expect(queryStub.callCount).to.equal(1, 'query must be called once')
        expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when stagingListDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    stagingListDbId: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when destinationListDbId'
        + ' is not an integer', async () => {
        req = {
            headers: {
                host: 'testHost'
            },
            params: {
                id: "456"
            },
            body: {
                destinationListDbId: 'not an integer'
            }
        }

        seedTransfer = [
            {
                senderProgramDbId: 101,
                senderDbId: 1,
                receiverProgramDbId: 102,
                receiverDbId: 2,
                sourceListDbId: 1,
                stagingListDbId: 2,
                destinationListDbId: 3,
                seedTransferStatus: 'draft',
                packageQuantity: 1,
                packageUnit: 'g',
                isSendWholePackage: true,
                errorLog: 'error here',
                remarks: 'remarks'
            }
        ]

        tokenHelperStub.onCall(0).resolves(123)
        queryStub.onCall(0).resolves(seedTransfer)
        transactionStub.onCall(0).resolves(transactionMock)

        const result = await put(req, res, next)

        expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
        expect(queryStub.callCount).to.equal(1, 'query must be called once')
        expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when destinationListDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    destinationListDbId: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when seedTransferStatus'
            + ' is not a valid status', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    seedTransferStatus: 'invalid status'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when seedTransferStatus is valid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    seedTransferStatus: 'created'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(seedTransfer)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when packageQuantity'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    packageQuantity: 'not an integer'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when packageQuantity is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    packageQuantity: '12'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(seedTransfer)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when packageUnit'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    packageUnit: ''
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when packageUnit is not empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    packageUnit: 'kg'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(seedTransfer)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when isSendWholePackage'
            + ' is not a boolean', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    isSendWholePackage: 'not a boolean'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when isSendWholePackage is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    isSendWholePackage: 'false'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(seedTransfer)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when errorLog is specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    errorLog: 'new error log'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(seedTransfer)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the seed transfer record'
            + ' when remarks is specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    remarks: 'new remarks'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(seedTransfer)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when validation fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    senderProgramDbId: '0'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(seedTransfer)
            queryStub.onCall(2).resolves([{count:0}])
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should generate an InternalError when an error occurrs', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    programDbId: '103'
                }
            }

            seedTransfer = [
                {
                    senderProgramDbId: 101,
                    senderDbId: 1,
                    receiverProgramDbId: 102,
                    receiverDbId: 2,
                    sourceListDbId: 1,
                    stagingListDbId: 2,
                    destinationListDbId: 3,
                    seedTransferStatus: 'draft',
                    packageQuantity: 1,
                    packageUnit: 'g',
                    isSendWholePackage: true,
                    errorLog: 'error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves("InternalError")

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

    })

    describe('/DELETE seed-transfers/{id}', () => {
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            transactionMock.finished = ""
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: 1234
                }
            }

            tokenHelperStub.onCall(0).resolves(null)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await st.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the seedTransferDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)

            const result = await st.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError when the seed transfer retrieval '
            + ' fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await st.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the seed transfer'
            + ' does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([])

            const result = await st.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })

        it('should successfully delete the seed transfer record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            seedTransfer = [
                {
                    seedTransferDbId: 456
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves('success')

            const result = await st.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate an InternalError when deletion fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            seedTransfer = [
                {
                    seedTransferDbId: 456
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await st.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })

        it('should generate an InternalError when transaction creation fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            seedTransfer = [
                {
                    seedTransferDbId: 456
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(seedTransfer)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await st.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })
    })
})

