/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const processQueryHelper = require('../../helpers/processQuery')
const errorBuilder = require('../../helpers/error-builder')

const { expect } = require('chai')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

const next = {};

describe('Package Logs Search', () => {
    const { post } = require("../../routes/v3/package-logs-search/index")

    describe('/POST package-logs-search', () => {
        const sandbox = sinon.createSandbox()
    
        let errorBuilderStub
        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let getTotalCountQuery
        let queryStub

        beforeEach(function() {
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilterWithInfo');
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString');
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery');
            getTotalCountQuery = sandbox.stub(processQueryHelper, 'getTotalCountQuery');
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully search package logs records without parameters', async () => {
            // Arrange
            req = {
                headers: {
                    host:"testHost"
                },
                query: {
                    sort: null
                },
                paginate: {
                    limit: 1,
                    offset: 1
                }
            }

            // Mock
            // Mock getFinalSqlQuery
            getFinalSqlQueryStub.resolves('query');
            queryStub.onCall(0).resolves(['packageLog1', 'packageLog2', 'packageLog3'])

            // Mock getTotalCountQuery
            getTotalCountQuery.resolves('query');
            queryStub.onCall(1).resolves([{ count: 3 }])

            // Act
            await post(req, res, next)

            // Assert
            expect(getFilterStub.calledOnce).to.be.false()
            expect(getOrderStringStub.calledOnce).to.be.false()
            expect(getFinalSqlQueryStub.calledOnce).to.be.true()
            expect(getTotalCountQuery.calledOnce).to.be.true()
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
        })

        it('should throw a BadRequestError (400022) when filter contains an invalid parameter', async () => {
            // Arrange
            req = {
                headers: {
                    host:"testHost"
                },
                query: {},
                body: {
                    'attr1': 'invalid',
                },
                paginate: {
                    limit: 1,
                    offset: 1
                }
            }

            // Mock
            getFilterStub.resolves({ mainQuery: 'invalid' })
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await post(req, res, next)

            // Assert
            expect(getFilterStub.calledOnce).to.be.true('getFilterWithInfo must be called exactly once')
            // POST has terminated at this point
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should throw a BadRequestError (400057) when sort contains an invalid parameter', async () => {
            // Arrange
            req = {
                headers: {
                    host:"testHost"
                },
                query: {
                    sort: 'invalidSortParam'
                },
                paginate: {
                    limit: 1,
                    offset: 1
                }
            }

            // Mock
            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await post(req, res, next)

            // Assert
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called exactly once')
            // POST has terminated at this point
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400057)
        })
    })
})
