/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const { knex } = require('../../config/knex')
const processQueryHelper = require('../../helpers/processQuery')
const test = require('../../helpers/test')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const {post} = require("../../routes/v3/persons-search");
const req = {
    headers: {},
    query: {},
    body: {
        "personDbId": "1"
    },
    paginate: {
        limit: 1,
        offset: 1
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
};
const next = {};


describe('Persons', () => {
    describe('/POST persons-search', () => {
        const { post } = require('../../routes/v3/persons-search');
        const sandbox = sinon.createSandbox()
        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let retrieveDataStub

        beforeEach(function() {
            // 'resolves' returns the expected values of the stubbed functions
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter').resolves([])
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString').resolves([])
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery').resolves([])
            retrieveDataStub = sandbox.stub(test, 'retrieveData').resolves([])
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully search and filter accessible person records',
            async () => {
                const result = await post(req, res, next)

                expect(getFilterStub.calledOnce).to.be.true()
                expect(getOrderStringStub.calledOnce).to.be.false()
                expect(getFinalSqlQueryStub.calledOnce).to.be.true()
                expect(retrieveDataStub.calledOnce).to.be.true()
        })

        it('should fail if filter is invalid', async () => {
            getFilterStub.resolves('You have provided invalid values for filter.')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true()
            // POST has terminated at this point
            expect(getOrderStringStub.calledOnce).to.be.false()
            expect(getFinalSqlQueryStub.calledOnce).to.be.false()
            expect(retrieveDataStub.calledOnce).to.be.false()
        })
    })
})