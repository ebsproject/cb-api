/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const errors = require('restify-errors');

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

describe('Plots', () => {
    describe('/POST plots', () => {
        const { post } = require('../../routes/v3/plots/index');
        const tokenHelper = require('../../helpers/auth/token.js');
        const sandbox = sinon.createSandbox();
        let queryStub
        let req, res, next;

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            req = {
                body: {},
                headers: {
                    host: 'example.com'
                }
            };
            res = {
                send: sandbox.stub(),
                status: sandbox.stub().returnsThis()
            };
            next = sandbox.stub();
        });

        afterEach(() => {
            sandbox.restore();
        });

        it('should successfully create a new plot record', async () => {
            sandbox.stub(tokenHelper, 'getUserId').resolves(123);
            sandbox.stub(sequelize, 'transaction').resolves({
                rollback: sandbox.stub().resolves()
            });
        
            req.body = {
                records: [
                    {
                        plotType: 'Type1',
                        rep: '1',
                        plotStatus: 'Status1'
                        // Add other required fields as needed
                    }
                ]
            };
        
            await post(req, res, next);
        
            expect(res.status.calledWith(200)).to.be.true;
            expect(res.send.calledWith(sinon.match({ rows: sinon.match.array }))).to.be.true;
        });

        it('should handle missing request body', async () => {
            await post(req, res, next);
        
            expect(res.send.calledWith(sinon.match.instanceOf(errors.BadRequestError))).to.be.true;
        });
    })

    describe('/PUT/:id plots', () => {
        const { put } = require('../../routes/v3/plots/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update a specific and existing plot record ' +
            'given a plot ID', async () => {

        })
    })

    describe('/PUT/:id plots', () => {
        const { put } = require('../../routes/v3/plots/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update a specific and existing plot record ' +
            'given a plot ID', async () => {

        })
    })

    describe('/DELETE/:id plots', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete a specific and existing plot record ' +
            'given a plot ID', async () => {

        })
    })

    describe('/POST/:id/data-search plots', () => {
        const { post } = require('../../routes/v3/plots/_id/data-search');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully search and filter accessible plot data ' +
            'records in a specific and existing plot given a plot ID',
            async () => {

        })
    })

    describe('/POST/:id/seeds-search plots', () => {
        const { post } = require('../../routes/v3/plots/_id/seeds-search');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully search and filter accessible seed records ' +
            'in a specific and existing plot given a plot ID', async () => {

        })
    })

    describe('/POST/:id/seeds-search-lite plots', () => {
        const { post } = require('../../routes/v3/plots/_id/seeds-search-lite');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully search and limited filter of accessible ' +
            'seed records in a specific and existing plot given a plot ID',
            async () => {

        })
    })
})