/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize.js')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder.js')
const tokenHelper = require('../../helpers/auth/token.js')
const userValidator = require('../../helpers/person/validator.js')
const logger = require('../../helpers/logger/index.js')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Crops', () => {
    describe('/POST crops', () => {
        const { post } = require('../../routes/v3/crops/index.js');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let isAdminStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let logMessageStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            logMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError (401028) when a user is not an Admin', async () => {
            // Arrange
            getUserIdStub.resolves(1)
            isAdminStub.resolves(false)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(isAdminStub.calledOnce).to.be.true('isAdmin must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401028)
        })

        it('should throw a BadRequestError (400009) when the request body is not set', async () => {
            // Arrange
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(isAdminStub.calledOnce).to.be.true('isAdmin must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw a BadRequestError (400005) when the records array is empty', async () => {
            // Arrange
            req.body = {}

            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(isAdminStub.calledOnce).to.be.true('isAdmin must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        const attributes = {
            'cropCode': { 'cropName': 'Sample crop' },
            'cropName': { 'cropCode': 'SAMPLE' },
            'both': {},
        }

        for (const attribute of Object.entries(attributes)) {
            it(`should return a BadRequestError if ${attribute[0]} parameter(s) are missing`, async () => {
                // Arrange
                req.body = {
                    records: [attribute[1]]
                }
    
                getUserIdStub.resolves(1)
                isAdminStub.resolves(true)
                errorStub.resolves('Required parameters are missing.')
    
                // Act
                await post(req, res, next)
    
                // Assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(isAdminStub.calledOnce).to.be.true('isAdmin must be called exactly once')
                expect(errorStub.calledOnce).to.be.true('BadRequestError should be called once')
            })
        }

        it('should successfully insert a valid record with cropCode and cropName ' +
            'without description', async () => {
            // Arrange
            req.body = {
                records: [{
                    'cropCode': 'CODE',
                    'cropName': 'Name',
                }]
            }

            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            transactionStub.resolves([[{ id: 1 }]])

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(isAdminStub.calledOnce).to.be.true('isAdmin must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })


        it('should successfully insert a valid record with cropCode and cropName ' +
            'with description', async () => {
            // Arrange
            req.body = {
                records: [{
                    'cropCode': 'CODE',
                    'cropName': 'Name',
                    'description': 'description'
                }]
            }

            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            transactionStub.resolves([[{ id: 1 }]])

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(isAdminStub.calledOnce).to.be.true('isAdmin must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should throw an InternalError (500001) if other errors were encountered', async () => {
            // Arrange
            req.body = {
                records: [{
                    'cropCode': 'CODE',
                    'cropName': 'Name',
                }]
            }

            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            transactionStub.throws(new Error('Dummy error'))
            logMessageStub.resolves('Dummy error')
            errorBuilderStub.resolves('InternalError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(isAdminStub.calledOnce).to.be.true('isAdmin must be called exactly once')
            expect(transactionStub).to.throw(Error)
            expect(logMessageStub.calledOnce).to.be.true('logMessage must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })
})
