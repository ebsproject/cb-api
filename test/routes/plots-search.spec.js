/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const { knex } = require('../../config/knex')
const processQueryHelper = require('../../helpers/processQuery/index')
const test = require('../../helpers/test')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const {post} = require("../../routes/v3/plots-search/index");
const req = {
    headers: {},
    query: {},
    body: {
        "plotDbId": "1054"
    },
    paginate: {
        limit: 1,
        offset: 1
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Plots', () => {
    describe('/POST plots-search', () => {
        const { post } = require('../../routes/v3/plots-search/index')
        const sandbox = sinon.createSandbox()
        let getFilterStub1;
        let getOrderString1;
        let getFinalSQLQuery1;

        beforeEach(function() {
            getFilterStub1 = sandbox.stub(processQueryHelper, 'getFilterWithInfo').resolves([]);
            getOrderString1 = sandbox.stub(processQueryHelper, 'getOrderString').resolves([]);
            getFinalSQLQuery1 = sandbox.stub(processQueryHelper, 'getFinalSqlQuery').resolves([]);
            // retrieveData1 = sandbox.stub(test, 'retrieveData').resolves([]);
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully search and filter accessible plot records',
            async () => {
                const result = await post(req, res, next)

                expect(getFilterStub1.calledOnce).to.be.true();
                expect(getFinalSQLQuery1.calledOnce).to.be.true();
                expect(getOrderString1.calledOnce).to.be.false();
                // expect(retrieveData1.calledOnce).to.be.true();
            })

        xit('should fail if sort and filter are wrong', async () => {

        })
    })
})

// dito ilalagay yung kapag nagfeed tayo ng maling input for sort, filter, so dapat mali din yung result (500)

// kay getOrderString() function
// retrieveData() - no unit test because inaaccess yung db
// getOrderString() function



