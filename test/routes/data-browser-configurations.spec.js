/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize.js')
const errorBuilder = require('../../helpers/error-builder.js')
const tokenHelper = require('../../helpers/auth/token.js')
const userValidator = require('../../helpers/person/validator.js')
const logger = require('../../helpers/logger/index.js')
const errors = require('restify-errors')

let req = {
    headers: {
        host:'testHost'
    }
}
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Data Browser Configurations', () => {
    describe('/POST data-browser-configurations', () => {
        const { post } = require('../../routes/v3/data-browser-configurations/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let queryStub
        let logFailingQueryStub
        let logMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            queryStub = sandbox.stub(sequelize, 'query')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
            req = {
                headers: {
                    host:'testHost'
                }
            }
        })

        it('should throw a BadRequestError(401002) if user is not found', async () => {
            // MOCK
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should throw a BadRequestError(400009) if request body does is not set', async () => {
            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should throw a BadRequestError(400005) if input data is empty', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                records: []
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should throw a BadRequestError if the required parameters are missing', async () => {
            // ARRANGE
            req.body = {
                records: [{}]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        xit('should throw an InternalError(500001) if other errors were encountered', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        name: "test",
                        data: "{}",
                        dataBrowserDbId: "test",
                        type: "test"
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            transactionStub.onCall().throws(new Error)

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should create a new record if all parameters are valid', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        name: "test",
                        data: "{}",
                        dataBrowserDbId: "test",
                        type: "test"
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            transactionStub.onCall().resolves()

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')
        })
    })

    describe('/PUT data-browser-configurations/:id', () => {
        const { put } = require('../../routes/v3/data-browser-configurations/_id/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let userValidatorStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let errorNotFoundStub
        let queryStub
        let logFailingQueryStub
        let logMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            errorNotFoundStub = sandbox.stub(errors, 'NotFoundError')
            queryStub = sandbox.stub(sequelize, 'query')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
            req = {
                headers: {
                    host:'testHost'
                }
            }
        })

        it('should throw a BadRequestError if configDbId is not an integer', async () => {
            // MOCK
            req.params = {id: 'notAnInteger'}
            req.boody = {}
            errorStub.resolves('error')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should throw a BadRequestError(401002) if user is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should throw a BadRequestError if configuration is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorNotFoundStub.resolves('NotFoundError')
            userValidatorStub.resolves(true)
            queryStub.resolves([])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorNotFoundStub.callCount).to.equal(1,'errorBuilder must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })

        it('should throw a BadRequestError(400009) if request body does is not set', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            queryStub.resolves([{
                name : 'config name',
                configsData : '{}',
                configsDataBrowserDbId : '1',
                configsRemarks : 'remark'
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        xit('should throw an InternalError(500003) if other errors were encountered', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                name: "new name",
                data: "{}",
                dataBrowserDbId: "new dataBrowserDbId",
                type: "new type"
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            queryStub.resolves([{
                name : 'config name',
                configsData : '{}',
                configsDataBrowserDbId : '1',
                configsRemarks : 'remark'
            }])
            transactionStub.onCall().throws(new Error)

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should update a record if all parameters are valid', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                name: "new name",
                data: "{}",
                dataBrowserDbId: "new dataBrowserDbId",
                remarks: "new remark"
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            queryStub.resolves([{
                name : 'config name',
                configsData : '{}',
                configsDataBrowserDbId : '1',
                configsRemarks : 'remark'
            }])
            transactionStub.onCall().resolves()

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')
        })
    })

    describe('/DELETE data-browser-configurations/:id', () => {
        const { delete:del } = require('../../routes/v3/data-browser-configurations/_id/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let userValidatorStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let errorNotFoundStub
        let queryStub
        let logFailingQueryStub
        let logMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            errorNotFoundStub = sandbox.stub(errors, 'NotFoundError')
            queryStub = sandbox.stub(sequelize, 'query')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
            req = {
                headers: {
                    host:'testHost'
                }
            }
        })

        it('should throw a BadRequestError(401002) if user is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should throw a BadRequestError if configDbId is not an integer', async () => {
            // MOCK
            req.params = {id: 'notAnInteger'}
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should throw a BadRequestError if configuration is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorNotFoundStub.resolves('NotFoundError')
            userValidatorStub.resolves(true)
            queryStub.resolves([])

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorNotFoundStub.callCount).to.equal(1,'errorBuilder must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })

        xit('should throw an InternalError(500002) if other errors were encountered', async () => {
            // ARRANGE
            req.params = {id: '1'}

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            queryStub.resolves([{
                name : 'config name',
                configsData : '{}',
                configsDataBrowserDbId : '1',
                configsRemarks : 'remark'
            }])
            transactionStub.onCall().throws(new Error)

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should delete a record if all parameters are valid', async () => {
            // ARRANGE
            req.params = {id: '1'}

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            queryStub.resolves([{
                name : 'config name',
                configsData : '{}',
                configsDataBrowserDbId : '1',
                configsRemarks : 'remark'
            }])
            transactionStub.onCall().resolves()

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')
        })
    })
})