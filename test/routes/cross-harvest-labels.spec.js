/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let processQueryHelper = require('../../helpers/processQuery/index.js')
let errorBuilder = require('../../helpers/error-builder')
const { expect } = require('chai')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Cross Harvest Labels', () => {
    describe('/GET cross-harvest-labels', () => {

        const { get } = require('../../routes/v3/cross-harvest-labels/index');
        const sandbox = sinon.createSandbox()

        let getFilterStub
        let getOrderStringStub
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve cross data to be used for harvest labels',
            async () => {
                req = {
                    headers: {},
                    query: {
                        occurrenceDbId: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves(['occurrence'])
                getFilterStub.resolves('')
                queryStub.onSecondCall().resolves(['cross1', 'cross2'])
                queryStub.onThirdCall().resolves([{ count: 2 }])

                const result = await get(req, res, next)

                expect(getFilterStub.calledOnce).to.be.true('getFilter must be called exactly 1 time')
                expect(queryStub.calledThrice).to.be.true('query must be called exactly 3 times')
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should throw a InternalError if no occurrence id is provided',
            async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {},
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                errorBuilderStub.resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
                expect(queryStub.called).to.be.false('query must not be called')

        })

        it('should throw a BadRequestError if invalid format of occurrence id is provided',
            async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        occurrenceDbId:'testOcc'
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                errorBuilderStub.resolves('BadRequestError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub,'testHost',400241)
                expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should throw an InternalError if retrieving occurrence data fails',
            async () => {
                req = {
                    headers: {
                        host:'testHost'
                    },
                    query: {
                        occurrenceDbId: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().rejects()
                errorBuilderStub.resolves('InternalError')

                const result = await get(req, res, next)

                expect(queryStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })

        it('should throw a NotFoundError if no occurrence data is retrieved',
            async () => {
                req = {
                    headers: {},
                    query: {
                        occurrenceDbId: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves([])

                const result = await get(req, res, next)

                expect(queryStub.calledOnce).to.be.true('query must be called exactly 1 time')
        })

        it('should throw a BadRequestError for invalid condition string',
            async () => {
                req = {
                    headers: {
                        host:'testHost'
                    },
                    query: {
                        occurrenceDbId: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves(['occurrence'])
                getFilterStub.onFirstCall().resolves('invalid')
                errorBuilderStub.resolves('BadRequestError')

                const result = await get(req, res, next)

                expect(queryStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(getFilterStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub,'testHost',400022)
        })

        it('should throw a BadRequestError for invalid order string',
            async () => {
                req = {
                    headers: {
                        host:'testHost'
                    },
                    query: {
                        occurrenceDbId: "1",
                        sort: 'sort'
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves(['occurrence'])
                getFilterStub.onFirstCall().resolves('')
                getOrderStringStub.onFirstCall().resolves('invalid')
                errorBuilderStub.resolves('BadRequestError')

                const result = await get(req, res, next)

                expect(queryStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(getFilterStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(getOrderStringStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub,'testHost',400004)
        })

        it('should throw an InternalError if retrieving cross data fails',
            async () => {
                req = {
                    headers: {
                        host:'testHost'
                    },
                    query: {
                        occurrenceDbId: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves(['occurrence'])
                getFilterStub.onFirstCall().resolves('')
                queryStub.onSecondCall().rejects()
                errorBuilderStub.resolves('InternalError')

                const result = await get(req, res, next)

                expect(queryStub.calledTwice).to.be.true('query must be called exactly 2 times')
                expect(getFilterStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called')
                sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })

        it('should send result if no cross data is found',
            async () => {
                req = {
                    headers: {},
                    query: {
                        occurrenceDbId: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves(['occurrence'])
                getFilterStub.onFirstCall().resolves('')
                queryStub.onSecondCall().resolves([])

                const result = await get(req, res, next)

                expect(queryStub.calledTwice).to.be.true('query must be called exactly 2 times')
                expect(getFilterStub.calledOnce).to.be.true('query must be called exactly 1 time')
        })

        it('should throw an InternalError if retrieving cross data count fails',
            async () => {
                req = {
                    headers: {
                        host:'testHost'
                    },
                    query: {
                        occurrenceDbId: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves(['occurrence'])
                getFilterStub.onFirstCall().resolves('')
                queryStub.onSecondCall().resolves(['cross1','cross2','cross3'])
                queryStub.onThirdCall().rejects()
                errorBuilderStub.resolves('InternalError')

                const result = await get(req, res, next)

                expect(queryStub.calledThrice).to.be.true('query must be called exactly 3 times')
                expect(getFilterStub.calledOnce).to.be.true('query must be called exactly 1 time')
                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called')
                sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })
    })
})