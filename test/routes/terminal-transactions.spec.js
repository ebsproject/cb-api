/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


const { sequelize } = require('../../config/sequelize')
const responseHelper = require('../../helpers/responses')
const errorBuilder = require('../../helpers/error-builder')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

const next = {}
// stubs
let errorBuilderStub
let tokenHelperStub
let getErrorCodebyMessageStub
let queryStub


describe('Terminal Transactions', () => {

    describe('/POST terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new transaction record in the Data ' +
            'Terminal', async () => {

        })
    })

    describe('/PUT/:id terminal-transactions', () => {
        const { put } = require('../../routes/v3/terminal-transactions/_id/index')
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update a specific and existing transaction ' +
            'record given a transaction ID', async () => {

        })
    })

    describe('/DELETE/:id terminal-transactions', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete a specific and existing transaction ' +
            'record given a transaction ID', async () => {

        })
    })

    describe('/POST/:id/commit-requests terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/commit-requests');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully commit the dataset to the experiment given a ' +
            'transaction ID and deletes transaction dataset records', async () => {

        })
    })

    describe('/GET/:id/datasets terminal-transactions', () => {
        const { get } = require('../../routes/v3/terminal-transactions/_id/datasets');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve the datasets of a specific ' +
            'transaction given a transaction ID', async () => {

        })
    })

    describe('/POST/:id/datasets-search terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/datasets-search');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {
            getErrorCodebyMessageStub = sandbox.stub(responseHelper, 'getErrorCodebyMessage')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate BadRequestError if transactionDbId ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                params: {
                    id: 'notInt'
                },
                query: {}
            }

            getErrorCodebyMessageStub.resolves(400004)
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getErrorCodebyMessageStub.calledOnce).to.be.true('getErrorCodebyMessage must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004, 'Invalid format, transactionDbId must be an integer')
            expect(queryStub.called).to.be.false('no query should have been called')
        })

        xit('should successfully search and filter for plot dataset records ' +
            'in a specific and existing transaction record given a ' +
            'transaction ID', async () => {

        })
    })

    describe('/POST/:id/cross-datasets-table-search terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/cross-datasets-table-search');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully search and filter for cross dataset records ' +
            'in a transaction record given a transaction ID', async () => {

        })
    })

    describe('/POST/:id/datasets-table-search terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/datasets-table-search');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getErrorCodebyMessageStub = sandbox.stub(responseHelper, 'getErrorCodebyMessage')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully search and filter for dataset records in a ' +
            'transaction record given a transaction ID', async () => {

        })

        it('should generate BadRequestError if transactionDbId ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                params: {
                    id: 'notInt'
                },
                query: {}
            }

            getErrorCodebyMessageStub.resolves(400004)
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getErrorCodebyMessageStub.calledOnce).to.be.true('getErrorCodebyMessage must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004, 'Invalid format, transactionDbId must be an integer')
            expect(queryStub.called).to.be.false('no query should have been called')
        })
    })

    describe('/GET/:id/dataset-summary terminal-transactions', () => {
        const { get } = require('../../routes/v3/terminal-transactions/_id/dataset-summary');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve summary of the dataset of a ' +
            'specific and existing transaction record given a transaction ' +
            'ID', async () => {

        })
    })

    describe('/POST/:id/dataset-table terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/dataset-table');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new dataset record for a specific ' +
            'and existing transaction record given a transaction ID',
            async () => {

        })
    })

    describe('/GET/:id/files terminal-transactions', () => {
        const { get } = require('../../routes/v3/terminal-transactions/_id/files');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve files of a specific and existing ' +
            'transaction record given a transaction ID', async () => {

        })
    })

    describe('/POST/:id/files terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/files');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new file record in a specific and ' +
            'existing transaction record given a transaction ID', async () => {

        })
    })

    describe('/POST/:id/validation-requests terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/validation-requests');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully validate the dataset of a specific and ' +
            'existing transaction record given a transaction ID', async () => {

        })
    })

    describe('/POST/:id/variable-computations terminal-transactions', () => {
        const { post } = require('../../routes/v3/terminal-transactions/_id/variable-computations');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully compute formula related to the datasets of ' +
            'a specific and existing transaction record given a transaction ' +
            'ID', async () => {

        })
    })
})