/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')
const crossesHelper = require('../../helpers/crosses/index.js')
const seedlotHelper = require('../../helpers/seedlot/index.js')

const {post} = require("../../routes/v3/crosses");

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
};
const next = {};

describe('Crosses', () => {
    describe('POST crosses', () => {
        const { post } = require('../../routes/v3/crosses');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let queryStub
        let getGermplasmInfoStub
        let checkBreedingExperimentStub
        let findBackcrossRecurrentParentStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            queryStub = sandbox.stub(sequelize, 'query')
            getGermplasmInfoStub = sandbox.stub(crossesHelper, 'getGermplasmInfo')
            checkBreedingExperimentStub = sandbox.stub(crossesHelper, 'checkBreedingExperiment')
            findBackcrossRecurrentParentStub = sandbox.stub(seedlotHelper, 'findBackcrossRecurrentParent')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError(400009) when the request body is not set', async () => {
            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        it('should throw a BadRequestError(400005) when the records array is empty', async () => {
            //arrange
            req.body = {}

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)

        })

        it('should throw a BadRequestError(400238) when invalid format, germplasm ID must be an integer', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                        germplasmDbId: 'invalid'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400238)

        })

        it('should throw a BadRequestError(400236) when invalid format, seed ID must be an integer', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                        seedDbId: 'invalid'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400236)

        })

        it('should throw a BadRequestError(400025) when invalid format, experiment ID must be an integer', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                        experimentDbId: 'invalid'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400025)

        })

        it('should throw a BadRequestError(400020) when invalid format, entry ID must be an integer', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                        entryDbId: 'invalid'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400020)

        })

        it('should throw a BadRequestError(400021) when invalid format, entry list ID must be an integer', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                        entryListDbId: 'invalid'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400021)

        })

        it('should throw a BadRequestError(400241) when invalid format, occurrence ID must be an integer', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                        occurrenceDbId: 'invalid'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400241)

        })

        it('should throw a BadRequestError(400015) when user provided an input that was not validated.', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.resolves([1])
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)

        })

        it('should return a BadRequestError if isMethodAutofilled type is invalid', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        crossName: 'test',
                        crossMethod: '',
                        isMethodAutofilled: 'invalid'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should throw an InternalError(500001) if other errors were encountered', async () => {
            //arrange
            req.body = {
                records: {}
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            transactionStub.resolves({
                rollback: sinon.stub(),
            })

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should return a BadRequestError if the required parameters are missing', async () => {
            //arrange
            req.body = {
                records: [{}]
            }

            //mock
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should successfully insert a valid record with cross method input', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'test',
                        crossMethod: 'selfing',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        occurrenceDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 6}])
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with autofilled hybrid method', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves(1)
            getGermplasmInfoStub.onSecondCall().resolves(2)
            checkBreedingExperimentStub.resolves({
                isHybrid: true
            })
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])
            
            //act
            const result = await post(req, res, next)
            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with autofilled backcross method', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves({
                parentage: 'test'
            })
            getGermplasmInfoStub.onSecondCall().resolves({
                parentage: 'test'
            })
            checkBreedingExperimentStub.resolves({
                isBreeding: true
            })
            findBackcrossRecurrentParentStub.resolves({
                recurrentParentGermplasm : 1,
                nonRecurrentParentGermplasm : 2
            })
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with empty cross method when parentage is unknown', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }
    
            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves({
                parentage: 'unknown'
            })
            getGermplasmInfoStub.onSecondCall().resolves({
                parentage: 'test'
            })
            checkBreedingExperimentStub.resolves({
                isBreeding: true
            })
            findBackcrossRecurrentParentStub.resolves({
                recurrentParentGermplasm : 1,
                nonRecurrentParentGermplasm : 2
            })
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])
    
            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('queryStub must be called once')
            expect(getGermplasmInfoStub.calledTwice).to.be.true('getGermplasmInfoStub must be called twice')
            expect(checkBreedingExperimentStub.calledOnce).to.be.true('checkBreedingExperimentStub must be called once')
            expect(findBackcrossRecurrentParentStub.calledOnce).to.be.true('findBackcrossRecurrentParentStub must be called once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with double cross method when both parents are F1 and not fixed', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }
    
            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves({
                parentage: 'test',
                generation: 'F1',
                germplasmState: 'not_fixed'
            })
            getGermplasmInfoStub.onSecondCall().resolves({
                parentage: 'test',
                generation: 'F1',
                germplasmState: 'not_fixed'
            })
            checkBreedingExperimentStub.resolves({
                isBreeding: true
            })
            findBackcrossRecurrentParentStub.resolves(null)
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])
    
            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('queryStub must be called once')
            expect(getGermplasmInfoStub.calledTwice).to.be.true('getGermplasmInfoStub must be called twice')
            expect(checkBreedingExperimentStub.calledOnce).to.be.true('checkBreedingExperimentStub must be called once')
            expect(findBackcrossRecurrentParentStub.calledOnce).to.be.true('findBackcrossRecurrentParentStub must be called once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with single cross method when both parents are fixed and not F1', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }
    
            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves({
                parentage: 'test',
                generation: 'F2',
                germplasmState: 'fixed'
            })
            getGermplasmInfoStub.onSecondCall().resolves({
                parentage: 'test',
                generation: 'F2',
                germplasmState: 'fixed'
            })
            checkBreedingExperimentStub.resolves({
                isBreeding: true
            })
            findBackcrossRecurrentParentStub.resolves(null)
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('queryStub must be called once')
            expect(getGermplasmInfoStub.calledTwice).to.be.true('getGermplasmInfoStub must be called twice')
            expect(checkBreedingExperimentStub.calledOnce).to.be.true('checkBreedingExperimentStub must be called once')
            expect(findBackcrossRecurrentParentStub.calledOnce).to.be.true('findBackcrossRecurrentParentStub must be called once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with three-way cross method when female is fixed and not f1', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }
    
            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves({
                parentage: 'test',
                generation: 'F2',
                germplasmState: 'fixed'
            })
            getGermplasmInfoStub.onSecondCall().resolves({
                parentage: 'test',
                generation: 'F1',
                germplasmState: 'not_fixed'
            })
            checkBreedingExperimentStub.resolves({
                isBreeding: true
            })
            findBackcrossRecurrentParentStub.resolves(null)
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])
    
            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('queryStub must be called once')
            expect(getGermplasmInfoStub.calledTwice).to.be.true('getGermplasmInfoStub must be called twice')
            expect(checkBreedingExperimentStub.calledOnce).to.be.true('checkBreedingExperimentStub must be called once')
            expect(findBackcrossRecurrentParentStub.calledOnce).to.be.true('findBackcrossRecurrentParentStub must be called once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with three-way cross method when male is fixed and not f1', async () => {
            //arrange
            req.body = {
                records: [
                    {   crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }
    
            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves({
                parentage: 'test',
                generation: 'F1',
                germplasmState: 'not_fixed'
            })
            getGermplasmInfoStub.onSecondCall().resolves({
                parentage: 'test',
                generation: 'F2',
                germplasmState: 'fixed'
            })
            checkBreedingExperimentStub.resolves({
                isBreeding: true
            })
            findBackcrossRecurrentParentStub.resolves(null)
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])
    
            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('queryStub must be called once')
            expect(getGermplasmInfoStub.calledTwice).to.be.true('getGermplasmInfoStub must be called twice')
            expect(checkBreedingExperimentStub.calledOnce).to.be.true('checkBreedingExperimentStub must be called once')
            expect(findBackcrossRecurrentParentStub.calledOnce).to.be.true('findBackcrossRecurrentParentStub must be called once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record with top cross method when crop is WHEAT and eligible for three-way cross', async () => {
            //arrange
            req.body = {
                records: [
                    {   
                        crossName: 'female|male',
                        crossMethod: '',
                        germplasmDbId: '1', 
                        seedDbId: '1',
                        experimentDbId: '1',
                        entryDbId: '1',
                        personDbId: '1', 
                        isMethodAutofilled: false,
                        entryListDbId: '1',
                    }
                ]
            }
    
            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([{count: 5}])
            getGermplasmInfoStub.onFirstCall().resolves({
                parentage: 'test',
                generation: 'F1',
                germplasmState: 'not_fixed'
            })
            getGermplasmInfoStub.onSecondCall().resolves({
                parentage: 'test',
                generation: 'F2',
                germplasmState: 'fixed'
            })                                                                  
            checkBreedingExperimentStub.resolves({
                isBreeding: true,
                cropCode: 'WHEAT'
            })
            findBackcrossRecurrentParentStub.resolves(null)
            queryStub.onSecondCall().resolves([[{id: 262}]])
            transactionStub.resolves([[{id:1}]])

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('queryStub must be called once')
            expect(getGermplasmInfoStub.calledTwice).to.be.true('getGermplasmInfoStub must be called twice')
            expect(checkBreedingExperimentStub.calledOnce).to.be.true('checkBreedingExperimentStub must be called once')
            expect(findBackcrossRecurrentParentStub.calledOnce).to.be.true('findBackcrossRecurrentParentStub must be called once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })
    })


    describe('/PUT/:id crosses', () => {
        const { put } = require('../../routes/v3/crosses/_id/index');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError if cross ID is not an ' +
            'integer (in string format)', async () => {

            //arrange
            req.params = {
                id: 'notAnInteger'
            }

            //mock
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            const result = await put(req, res, next)

            //assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.called).to.be.false('query must not be called')

        })
    })

    describe('getGermplasmInfo', () => {
        const { getGermplasmInfo } = require('../../helpers/crosses/index');
        const sandbox = sinon.createSandbox()
        let queryStub

        beforeEach(function() {
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully retrieve germplasm information', async () => {
            let germplasmName = 'name'
            let germplasmInfo = [{
                'germplasmDbId':123,
                'parentage':'Parentage',
                'generation':'Generation',
                'germplasmState':'GermplasmState'
            }]

            queryStub.onFirstCall().resolves(germplasmInfo)

            let result = await getGermplasmInfo(germplasmName)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result !== null).to.be.true('germplasm information is not empty')
        })

        it('should return null if query fails', async () => {
            queryStub.onFirstCall().rejects

            let result = await getGermplasmInfo('')

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == null).to.be.true('germplasm information is empty')
        })

        it('should return empty if no record is found', async () => {
            let germplasmName = 'name'

            queryStub.onFirstCall().resolves([])

            let result = await getGermplasmInfo(germplasmName)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(result == undefined).to.be.true('germplasm information is empty')
        })
    })
})
