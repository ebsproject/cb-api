/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')
const forwarded = require('forwarded-for')
const logger = require('../../helpers/logger')

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe(' Germplasm File Upload Germplasm', () => {
    describe('/POST germplasm-file-upload-germplasm', () => {
        const { post } = require('../../routes/v3/germplasm-file-upload-germplasm');
        const sandbox = sinon.createSandbox()
        let queryStub
        let tokenHelperStub
        let errorBuilderStub
        let forwardedStub
        let transactionStub
        let logMessageStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            forwardedStub = sandbox.stub(forwarded, 'forwarded')
            logMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully INSERT new records',
            async ()=>{
                expected = {
                    rows: [
                        {
                            fileUploadGermplasmDbId: 1,
                            recordCount: 1,
                            href: "https://testHost/v3/germplasm-file-upload-germplas/1"
                        },
                        {
                            fileUploadGermplasmDbId: 2,
                            recordCount: 1,
                            href: "https://testHost/v3/germplasm-file-upload-germplas/1"
                        }
                    ]
                }

                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                queryStub.resolves([{count:5}])
                transactionStub.resolves(expected)

                let req = {
                    headers: {
                        host:'testHost'
                    },
                    body: { 
                        records:[
                            {fileUploadDbId:"1", germplasmDbId:"788692", seedDbId:"1983603",packageDbId:"2273219",remarks:"sample", entity:"germplasm_relation", entityDbId:"2"},
                            {fileUploadDbId:"2", germplasmDbId:"788712", seedDbId:"1983604",packageDbId:"2273220",remarks:"sample", entity:"germplasm_relation", entityDbId:"2"}
                        ]
                    }
                }

                const result = await post(req,res,next)

                expect(queryStub.callCount).to.be.equal(2,'check valid input count.')
        })

        it('should throw BadRequestError if there is user is not found', 
            async ()=>{
                // MOCK
                tokenHelperStub.resolves(null)
                errorBuilderStub.resolves('BadRequestError')
                
                req = {
                    headers: {
                        host:'testHost'
                    }
                }

                // ACT
                const result = await post(req,res,next)

                // ASSERT
                expect(tokenHelperStub.calledOnce).to.be.true('getUserId get called once')
                expect(errorBuilderStub.calledOnce).to.be.true('no user id retrieved')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw BadRequestError if there is no request body provided with', 
            async ()=>{
                // MOCK
                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    }
                }
                
                // ACT
                const result = await post(req,res,next)

                // ASSERT
                expect(tokenHelperStub.calledOnce).to.be.true('getUserId get called once')
                expect(errorBuilderStub.calledOnce).to.be.true('no request body provided')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw InternalError if no records are provided', 
            async ()=>{
                // MOCK
                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                errorBuilderStub.resolves('InternalError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {} 
                }
                
                // ACT
                const result = await post(req,res,next)

                // ASSERT
                expect(tokenHelperStub.calledOnce).to.be.true('getUserId get called once')
                expect(errorBuilderStub.calledOnce).to.be.true('no request records provided')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw BadRequestError if records is empty',
            async ()=>{
               // MOCK
               tokenHelperStub.resolves(1)
               forwardedStub.resolves({'secure': false})
               errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {records:[]}
                }
               
               // ACT
               const result = await post(req,res,next)

               // ASSERT
               expect(tokenHelperStub.calledOnce).to.be.true('getUserId get called once')
               expect(errorBuilderStub.calledOnce).to.be.true('records provided is empty provided')
               sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005) 
        })

        it('should throw BadRequestError if fileUploadDbId is not provided',
            async ()=>{

                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {
                        records:[
                            {'remarks':''}
                        ]
                    }
                }
               
                const result = await post(req,res,next)

                expect(errorBuilderStub.calledOnce).to.be.true('missing required parameters provided')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400240) 
        })

        it('should throw BadRequestError if fileUploadDbId provided is not an integer',
            async ()=>{

                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {
                        records:[
                            {fileUploadDbId:'a', germplasmDbId:1}
                        ]
                    }
                }
               
                const result = await post(req,res,next)

                expect(errorBuilderStub.calledOnce).to.be.true('invalid fileUploadDbId format.')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400246) 

        })

        it('should throw BadRequestError if germplasmDbId provided is not an integer',
            async ()=>{
                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {
                        records:[
                            {fileUploadDbId:'1', germplasmDbId:'a'}
                        ]
                    }
                }
               
                const result = await post(req,res,next)

                expect(errorBuilderStub.calledOnce).to.be.true('invalid germplasmDbId format.')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400246) 
        })

        it('should throw BadRequestError if seedDbId provided is not an integer',
            async ()=>{
                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {
                        records:[
                            {fileUploadDbId:'1', germplasmDbId:'1', seedDbId:'a'}
                        ]
                    }
                }
               
                const result = await post(req,res,next)

                expect(errorBuilderStub.calledOnce).to.be.true('invalid seedDbId format.')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400113)
        })

        it('should throw BadRequestError if packageDbId provided is not an integer',
            async ()=>{
                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {
                        records:[
                            {fileUploadDbId:'1', germplasmDbId:'1', seedDbId:'1',packageDbId:'a'}
                        ]
                    }
                }
               
                const result = await post(req,res,next)

                expect(errorBuilderStub.calledOnce).to.be.true('invalid packageDbId format.')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400222)
        })

        it('should throw BadRequestError if not all provided input are valid',
            async ()=>{
                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                queryStub.resolves([{count:1}])
                errorBuilderStub.resolves('BadRequestError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {
                        records:[
                            {fileUploadDbId:'1', germplasmDbId:'1', seedDbId:'1',packageDbId:'1'}
                        ]
                    }
                }

                const result = await post(req,res,next)

                expect(queryStub.callCount).to.be.equal(1,'check valid input count.')
                expect(errorBuilderStub.calledOnce).to.be.true('has invalid input.')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should throw InternalError if INSERT query fails',
            async ()=>{
                tokenHelperStub.resolves(1)
                forwardedStub.resolves({'secure': false})
                queryStub.onCall(0).resolves([{count:4}])
                queryStub.onCall(1).resolves([{count:4}])
                transactionStub.throws(new Error)
                logMessageStub.resolves('error')
                errorBuilderStub.resolves('InternalError')

                req = {
                    headers: {
                        host:'testHost'
                    },
                    body: {
                        records:[
                            {fileUploadDbId:'1', germplasmDbId:'1', seedDbId:'1',packageDbId:'1'},
                            {fileUploadDbId:'2', germplasmDbId:'2', seedDbId:'2',packageDbId:'2'}
                        ]
                    }
                }

                const result = await post(req,res,next)
                expect(queryStub.callCount).to.be.equal(2,'check valid input count.')
                expect(errorBuilderStub.calledOnce).to.be.true('has invalid input.')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })
})