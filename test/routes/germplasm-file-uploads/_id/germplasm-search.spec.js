/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../../../config/sequelize.js')
const errorBuilder = require('../../../../helpers/error-builder.js')
const errors = require('restify-errors')
const tokenHelper = require('../../../../helpers/auth/token.js')
const userValidator = require('../../../../helpers/person/validator')
const processQueryHelper = require('../../../../helpers/processQuery')
const germplasmUploadAbbrevHelper = require('../../../../helpers/germplasm/index')

let req = {
    headers: {
        host: 'testHost'
    },
    query: {},
    paginate: {
        limit: 1,
        offset: 1
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}
const transactionMock = {
    finished: "",
    commit: function () {
        this.finished = 'commit'
    },
    rollback: function () {
        this.finished = 'rollback'
    }
}

describe('/POST germplasm-file-uploads/{id}/germplasm-search', () => {
    const { post } = require('../../../../routes/v3/germplasm-file-uploads/_id/germplasm-search.js');
    const sandbox = sinon.createSandbox()

    let tokenHelperStub
    let errorBuilderStub
    let queryStub
    let transactionStub
    let getDistinctStringStub
    let getFilterStub
    let getOrderStringStub
    let getFinalSqlQueryStub
    let getCountFinalSqlQueryStub
    let getScaleValueByAbbrevStub

    beforeEach(() => {
        tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
        errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        queryStub = sandbox.stub(sequelize, 'query')
        transactionStub = sandbox.stub(sequelize, 'transaction')
        getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
        getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
        getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
        getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
        getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
        getScaleValueByAbbrevStub = sandbox.stub(germplasmUploadAbbrevHelper, 'getScaleValueByAbbrev')
    })

    afterEach(() => {
        sandbox.restore()
    })

    it('should throw InternalError if germplasm file upload ID is invalid', async () => {
        // arrange
        req.params = {}

        errorBuilderStub.resolves('InternalError')

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })

    it('should throw BadRequestError if invalid germplasm file upload ID', async () => {
        // arrange
        req.params = {
            id: 'a'
        }

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.called).to.be.false('errorBuilder not called')
    })

    it('should throw InternalError if query fails', async () => {
        // arrange
        req.params = {
            id: '1'
        }

        queryStub.rejects()
        errorBuilderStub.resolves('InternalError')

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })

    it('should throw BadRequestError if no record retrieved', async () => {
        // arrange
        req.params = {
            id: '1'
        }

        queryStub.resolves([])
        errorBuilderStub.resolves('InternalError')

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.called).to.be.false('errorBuilder not called')
    })

    it('should throw BadRequestError if conditionString has invalid value', async () => {
        // arrange
        req.params = {
            id: '1'
        }

        req.body = {
            distinctOn: 'distinctOn',
            'attribute': 'invalid'
        }

        queryStub.resolves([{ id: '1' }])
        getDistinctStringStub.resolves('string')
        getFilterStub.resolves('invalid')
        errorBuilderStub.resolves('BadRequestError')

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
    })

    it('should throw BadRequestError if orderString has invalid value', async () => {
        // arrange
        req.params = {
            id: '1'
        }

        req.query.sort = 'invalid'

        req.body = {
            distinctOn: 'distinctOn',
            'attribute': 'value'
        }

        queryStub.resolves([{ id: '1' }])
        getDistinctStringStub.resolves('string')
        getFilterStub.resolves('value')
        getOrderStringStub.resolves('invalid')
        errorBuilderStub.resolves('BadRequestError')

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
    })

    it('should throw BadRequestError if germplasm file upload germplasm query fails', async () => {
        // arrange
        req.params = {
            id: '1'
        }

        req.query.sort = 'sort: ASC'

        req.body = {
            distinctOn: 'distinctOn',
            'attribute': 'value'
        }

        queryStub.onFirstCall().resolves([{ id: '1' }])
        getDistinctStringStub.resolves('string')
        getFilterStub.resolves('value')
        getOrderStringStub.resolves('valid')
        queryStub.onSecondCall().rejects()
        errorBuilderStub.resolves('BadRequestError')

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })

    it('should throw InternalError if germplasm file upload germplasm count query fails', async () => {
        // arrange
        req.params = {
            id: '1'
        }

        req.query.sort = 'sort: ASC'

        req.body = {
            distinctOn: 'distinctOn',
            'attribute': 'value'
        }

        queryStub.onFirstCall().resolves([{ id: '1' }])
        getDistinctStringStub.resolves('string')
        getFilterStub.resolves('value')
        getOrderStringStub.resolves('valid')
        queryStub.onSecondCall().resolves([{ id: '2' }])
        getCountFinalSqlQueryStub.resolves('string')
        queryStub.onThirdCall().rejects()
        errorBuilderStub.resolves('BadRequestError')

        // act
        await post(req, res, next)

        // assert
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })

    it('successfully retrieves germplasm records for the given germplasm file upload ID', async () => {
        // arrange
        req.params = {
            id: '1'
        }

        req.query.sort = 'sort: ASC'

        req.body = {
            distinctOn: 'distinctOn',
            'attribute': 'value'
        }

        queryStub.onFirstCall().resolves([{ id: '1' }])
        getDistinctStringStub.resolves('string')
        getFilterStub.resolves('value')
        getOrderStringStub.resolves('valid')
        queryStub.onSecondCall().resolves([{ id: '2' }])
        getCountFinalSqlQueryStub.resolves('string')
        queryStub.onThirdCall().resolves([{ count: 1 }])

        // act
        await post(req, res, next)

        // assert
        expect(queryStub.callCount == 3).to.be.true('all queries successfully completed')

    })
}) 