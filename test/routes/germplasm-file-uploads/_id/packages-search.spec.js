/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../../../config/sequelize.js')
const errorBuilder = require('../../../../helpers/error-builder.js')
const tokenHelper = require('../../../../helpers/auth/token.js')

let req = {
    headers: {
        host: 'testHost'
    },
    query: {},
    paginate: {
        limit: 1,
        offset: 1
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('/POST germplasm-file-uploads/{id}/packages-search', () => {
    const { post } = require('../../../../routes/v3/germplasm-file-uploads/_id/packages-search');
    const sandbox = sinon.createSandbox()
    let tokenHelperStub
    let errorBuilderStub
    let queryStub
    let transactionStub

    beforeEach(() => {
        tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
        errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        queryStub = sandbox.stub(sequelize, 'query')
        transactionStub = sandbox.stub(sequelize, 'transaction')
    })

    afterEach(() => {
        sandbox.restore()
    })

    it('should generate an InternalError when no id is provided', async () => {
        req.params = {}

        errorBuilderStub.onCall(0).resolves('InternalError')

        const result = await post(req, res, next)

        expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })

    it('should generate a BadRequestError if id provided is not an integer',
        async () => {
            req.params = {
                id: 'abc'
            }

            const result = await post(req, res, next)

            expect(errorBuilderStub.callCount).to.be.equals(0, 'code does not use errorBuilder so no need to be called')
            expect(queryStub.called).to.be.false('query must not be called')
        })

    it('should generate an InternalError if the query fails', async () => {
        req.params = {
            id: '5'
        }

        queryStub.onCall(0).rejects()
        errorBuilderStub.onCall(0).resolves('InternalError')

        const result = await post(req, res, next)

        expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
        expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })

    it('should generate a BadRequestError if no germplasm file upload record is retrieved', async () => {
        req.params = {
            id: '5'
        }

        queryStub.onCall(0).rejects()
        errorBuilderStub.onCall(0).resolves('BadRequestError')

        const result = await post(req, res, next)

        expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
        expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
    })

    it('should successfully retrieve germplasm file upload record', async () => {
        req.params = {
            id: '5'
        }

        queryStub.onCall(0).resolves([
            {
                'germplasmFileUploadDbId': '5',
                'fileName': 'test file name'
            }
        ])
        queryStub.onCall(1).resolves([])
        queryStub.onCall(2).resolves([{ count: 0 }])

        const result = await post(req, res, next)

        expect(queryStub.callCount).to.be.equal(3, 'query must be called exactly once')
        expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
    })

    it('should successfully retrieve package record', async () => {
        req.params = {
            id: '5'
        }

        queryStub.onCall(0).resolves([
            {
                'germplasmFileUploadDbId': '5',
                'fileName': 'test file name'
            }
        ])
        queryStub.onCall(1).resolves([{ packageDbId: '123' }])
        queryStub.onCall(2).resolves([{ count: 1 }])

        const result = await post(req, res, next)

        expect(queryStub.callCount).to.be.equal(3, 'query must be called exactly once')
        expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
    })
})