/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
};
const next = {}

// Mock the transaction invoked after querying the database
const transactionMock = {
    finished: "",
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    },
    // Call this function in afterEach() to reset the value
    // of the property "finished"
    reset: function() {
        this.finished = ''
    }
}

describe('Package Logs', () => {
    const { post } = require('../../routes/v3/package-logs/index')
    const { put } = require('../../routes/v3/package-logs/_id/index')
    const pl = require('../../routes/v3/package-logs/_id/index');
    const sandbox = sinon.createSandbox()
    let getUserIdStub
    let queryStub
    let errorBuilderStub
    let badRequestErrorStub
    let notFoundErrorStub
    let tokenHelperStub

    describe("/POST/ package-logs", () => {
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            badRequestErrorStub = sandbox.stub(errors, 'BadRequestError')
            notFoundErrorStub = sandbox.stub(errors, 'NotFoundError')
        })

        afterEach(() => {
            sandbox.restore()
            transactionMock.reset()
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            'sample': 'record',
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError (400009) when the request body is not set', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw a BadRequestError (400005) when the records array is empty', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        it('should throw a BadRequestError when the records array is missing a required parameter', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            // Missing "packageDbId" parameter
                            "packageTransactionType": "withdraw",
                            "packageQuantity": 30,
                            "packageUnit": "g",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "144"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            badRequestErrorStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(badRequestErrorStub.calledOnce).to.be.true('badRequestError must be called exactly once')
        })

        it('should throw a BadRequestError when a packageDbId is not an integer', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "string",
                            "packageTransactionType": "withdraw",
                            "packageQuantity": 30,
                            "packageUnit": "g",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "144"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            badRequestErrorStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(badRequestErrorStub.calledOnce).to.be.true('badRequestError must be called exactly once')
        })

        it('should throw a NotFoundError when a packageDbId does not exist in the database', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "0",
                            "packageTransactionType": "withdraw",
                            "packageQuantity": 30,
                            "packageUnit": "g",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "144"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            queryStub.onCall(0).resolves([{ count: 0 }])
            notFoundErrorStub.resolves('NotFoundError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(notFoundErrorStub.calledOnce).to.be.true('notFoundError must be called exactly once')
        })

        it('should throw a BadRequestError when the provided value for a packageTransactionType is invalid', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "123",
                            "packageTransactionType": "invalid",
                            "packageQuantity": 30,
                            "packageUnit": "g",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "144"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            queryStub.onCall(0).resolves([{ count: 1 }])
            queryStub.onCall(1).resolves([{ count: 0 }])
            badRequestErrorStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(badRequestErrorStub.calledOnce).to.be.true('badRequestError must be called exactly once')
        })

        it('should throw a BadRequestError when a packageQuantity is not an integer', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "123",
                            "packageTransactionType": "withdraw",
                            "packageQuantity": "string",
                            "packageUnit": "g",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "144"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            queryStub.onCall(0).resolves([{ count: 1 }])
            queryStub.onCall(1).resolves([{ count: 1 }])
            badRequestErrorStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(badRequestErrorStub.calledOnce).to.be.true('badRequestError must be called exactly once')
        })

        it('should throw a BadRequestError when a packageUnit is invalid', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "123",
                            "packageTransactionType": "withdraw",
                            "packageQuantity": 30,
                            "packageUnit": "mg",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "144"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            queryStub.onCall(0).resolves([{ count: 1 }])
            queryStub.onCall(1).resolves([{ count: 1 }])
            queryStub.onCall(2).resolves([{ count: 0 }])
            badRequestErrorStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(badRequestErrorStub.calledOnce).to.be.true('badRequestError must be called exactly once')
        })

        it('should throw a NotFoundError when an entityAbbrev does not exist in the database', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "123",
                            "packageTransactionType": "withdraw",
                            "packageQuantity": 30,
                            "packageUnit": "g",
                            "entityAbbrev": "invalid",
                            "dataDbId": "144"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            queryStub.onCall(0).resolves([{ count: 1 }])
            queryStub.onCall(1).resolves([{ count: 1 }])
            queryStub.onCall(2).resolves([{ count: 1 }])
            queryStub.onCall(3).resolves([])
            notFoundErrorStub.resolves('NotFoundError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(4, 'query must be called 4 times')
            expect(notFoundErrorStub.calledOnce).to.be.true('notFoundError must be called exactly once')
        })

        it('should throw a BadRequestError when a dataDbId is not an integer', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "123",
                            "packageTransactionType": "withdraw",
                            "packageQuantity": 30,
                            "packageUnit": "g",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "string"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            queryStub.onCall(0).resolves([{ count: 1 }])
            queryStub.onCall(1).resolves([{ count: 1 }])
            queryStub.onCall(2).resolves([{ count: 1 }])
            queryStub.onCall(3).resolves([{ id: 123 }])
            badRequestErrorStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(4, 'query must be called 4 times')
            expect(badRequestErrorStub.calledOnce).to.be.true('badRequestError must be called exactly once')
        })

        it('should throw a NotFoundError when an dataDbId does not exist in the database', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            "packageDbId": "123",
                            "packageTransactionType": "withdraw",
                            "packageQuantity": 30,
                            "packageUnit": "g",
                            "entityAbbrev": "ENTRY",
                            "dataDbId": "0"
                        }
                    ]
                },
            }

            //mock
            getUserIdStub.onFirstCall().resolves(123)
            queryStub.onCall(0).resolves([{ count: 1 }])
            queryStub.onCall(1).resolves([{ count: 1 }])
            queryStub.onCall(2).resolves([{ count: 1 }])
            queryStub.onCall(3).resolves([{ id: 123 }])
            queryStub.onCall(4).resolves([{ count: 0 }])
            notFoundErrorStub.resolves('NotFoundError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(5, 'query must be called 5 times')
            expect(notFoundErrorStub.calledOnce).to.be.true('notFoundError must be called exactly once')
        })
    })

    describe("/PUT/:id package-logs", () => {
        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
            transactionMock.reset()
        })

        it('should throw a BadRequestError if package log ID is not an ' +
            'integer (in string format)', async () => {
            // arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    'id': 'notAnInteger'
                }
            }

            // mock
            errorBuilderStub.resolves('BadRequestError')
            
            // act
            const result = await put(req, res, next)

            // assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400081)
            expect(queryStub.called).to.be.false('query must not be called')
        })
        it('throws a BadRequestError when the user ID value is null', async () => {
            // arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    'id': '123'
                }
            }

            // mock
            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // act
            const result = await put(req, res, next)
            
            // assert
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })
        it('should generate a BadRequestError when req.body is undefined', async () => {
            // arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    'id': '123'
                }
            }

            // mock
            tokenHelperStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            // act
            const result = await put(req, res, next)

            // assert
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.called).to.be.false('query must not be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)  
        })
        it('should generate InternalError if error occurs when querying the database', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    'id': '123'
                },
                body: {
                    'packageTransactionType': 'deposit' 
                }
            }

            tokenHelperStub.resolves(1)
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await put(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
        it('should generate a NotFoundError when the package log does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    'id': '123'
                },
                body: {
                    'packageTransactionType': 'deposit' 
                }
            }

            tokenHelperStub.resolves(1)
            queryStub.onCall(0).resolves([])
            errorBuilderStub.onCall(0).resolves('NotFoundError')

            let result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })
    })

    describe("/DELETE/:id package-logs", () => {
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionMock.finished = ""
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError if package log ID is not an ' +
            'integer (in string format)', async () => {
            // arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    'id': 'notAnInteger'
                }
            }

            // mock
            errorBuilderStub.resolves('BadRequestError')
            tokenHelperStub.resolves(1)
            
            // act
            const result = await pl.delete(req, res, next)

            // assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400081)
            expect(queryStub.called).to.be.false('query must not be called')
        })
        it('throws a BadRequestError when the user ID value is null', async () => {
            // arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    'id': '123'
                }
            }

            // mock
            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // act
            const result = await pl.delete(req, res, next)
            
            // assert
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError when the package log retrieval '
            + 'fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await pl.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the package log'
            + ' does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            tokenHelperStub.resolves(1)
            queryStub.onCall(0).resolves([])
            errorBuilderStub.onCall(0).resolves('NotFoundError')

            const result = await pl.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })

        it('should successfully delete the package log record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            packageLog = [
                {
                    id: 456,
                    packageLogDbId: 123
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(packageLog)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves('success')

            const result = await pl.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

    })
})