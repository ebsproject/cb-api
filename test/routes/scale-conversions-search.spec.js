/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const processQueryHelper = require('../../helpers/processQuery/index')
const errorBuilder = require('../../helpers/error-builder')
const { expect } = require('chai')
const { knex } = require('../../config/knex')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Scale Conversions', () => {
    describe('/POST scale-conversions-search', () => {
        const { post } = require('../../routes/v3/scale-conversions-search')
        const sandbox = sinon.createSandbox()
        let queryStub
        let getFilterStub
        let getDistinctStringStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let getCountFinalSqlQueryStub
        let errorBuilderStub
        let knexRawStub
        let knexSelectStub
        let knexColumnStub
        let getFieldValuesStringStub

        beforeEach(() => {
            knexRawStub = sandbox.stub(knex, 'raw')
            knexSelectStub = sandbox.stub(knex, 'select')
            knexColumnStub = sandbox.stub(knex, 'column')
            queryStub = sandbox.stub(sequelize, 'query')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve scale conversion records ' + 
            'with EMPTY request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {}
            }

            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['scaleConversionRecord1, scaleConversionRecord2, scaleConversionRecord3'])
            getCountFinalSqlQueryStub.resolves(100)
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve scale conversion records ' + 
            'with distinctOn in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
                body: {
                    distinctOn: 'thisColumn'
                }
            }

            getDistinctStringStub.resolves('addedDistinctString')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['scaleConversionRecord1, scaleConversionRecord2, scaleConversionRecord3'])
            getCountFinalSqlQueryStub.resolves(100)
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getDistinctStringStub.calledOnce).to.be.true('getDistinctString must be successfully called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve scale conversion records ' + 
            'with fields in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
                body: {
                    fields: 'this.field AS thisField'
                }
            }

            knexColumnStub.resolves('column')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['scaleConversionRecord1, scaleConversionRecord2, scaleConversionRecord3'])
            getCountFinalSqlQueryStub.resolves(100)
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(knexColumnStub.calledOnce).to.be.true('knex.column must be called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve scale conversion records ' + 
            'with fields and distinctOn in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
                body: {
                    fields: 'this.field AS thisField',
                    distinctOn: 'thisColumn'
                }
            }

            getDistinctStringStub.resolves('addedDistinctString')
            getFieldValuesStringStub.resolves('fieldsString')
            knexRawStub.resolves('raw')
            knexSelectStub.resolves('select')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['scaleConversionRecord1, scaleConversionRecord2, scaleConversionRecord3'])
            getCountFinalSqlQueryStub.resolves(100)
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getDistinctStringStub.calledOnce).to.be.true('getDistinctString must be successfully called once')
            expect(knexRawStub.calledOnce).to.be.true('knex.raw must be called once')
            expect(knexSelectStub.calledOnce).to.be.true('knex.select must be called once')
            expect(getFieldValuesStringStub.calledOnce).to.be.true('getFieldValuesString must be successfully called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if sort argument ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'invalid:sorting'
                }
            }

            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
            expect(queryStub.called).to.be.false('no query should have been called')
        })

        it('should generate InternalError when the query for retrieving ' +
            'the scale conversion records fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate InternalError if the scale conversion count query fails',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            queryStub.onFirstCall().resolves(['scale conversion1'])
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate NotFoundError if no scale conversion records were found',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            queryStub.resolves([])

            await post(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should generate BadRequestError if conditionString is invalid',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {},
                body: {
                    test: 'invalidArgument'
                }
            }

            getFilterStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })
    })
})