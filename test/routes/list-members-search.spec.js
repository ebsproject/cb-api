/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const { knex } = require('../../config/knex')
const processQueryHelper = require('../../helpers/processQuery')
const test = require('../../helpers/test')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')

const { post } = require("../../routes/v3/list-members-search/index")
const { expect } = require('chai')

const req = {
    headers: {
        host: "testHost"
    },
    query: {},
    paginate: {
        limit: 1,
        offset: 1
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

const next = {};

describe('List Members', () => {
    describe('/POST list-members-search', () => {
        const sandbox = sinon.createSandbox()

        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let retrieveDataStub
        let errorBuilderStub
        let queryStub

        beforeEach(function () {
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryWoLimitStub = sandbox.stub(processQueryHelper, 'getFinalSqlQueryWoLimit')
            getFinalTotalCountQueryStub = sandbox.stub(processQueryHelper, 'getFinalTotalCountQuery')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            retrieveDataStub = sandbox.stub(test, 'retrieveData')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function () {
            sandbox.restore()
        })


        it('should throw InternalError if retrieval of list member records fails',
            async () => {
                req['body'] = {
                    "germplasmDbId": "12345"
                }
                req.query.sort = 'testSort:ASC'

                getFilterStub.resolves('testFilter')
                getOrderStringStub.resolves('testOrderString')
                getFinalSqlQueryWoLimitStub.resolves('testQueryWoLimit')
                getFinalTotalCountQueryStub.resolves('testFinalQuery')
                queryStub.rejects()

                const result = await post(req, res, next)

                expect(getFilterStub.called).to.be.true('getFilterStub has been called')
                expect(getOrderStringStub.called).to.be.true('getOrderStringStub has been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.true('getFinalSqlQueryWoLimitStub has been called')
                expect(getFinalTotalCountQueryStub.called).to.be.true('getFinalTotalCountQueryStub has been called')
                expect(queryStub.callCount).to.be.greaterThan(0, 'queryStub is called')
            })

        it('should successfully retrieve list members given dataDbId as request body parameters',
            async () => {
                req['body'] = {
                    "dataDbId": "123"
                }
                req.query.sort = 'testSort:ASC'

                getFilterStub.resolves('testFilter')
                getOrderStringStub.resolves(['testOrderString'])
                getFinalSqlQueryWoLimitStub.resolves('testQueryWoLimit')
                getFinalTotalCountQueryStub.resolves('testFinalQuery')
                queryStub.resolves([{ 'sample record': 'sample values' }])

                const result = await post(req, res, next)

                expect(getFilterStub.called).to.be.true('getFilterStub has been called')
                expect(getOrderStringStub.called).to.be.true('getOrderStringStub has been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.true('getFinalSqlQueryWoLimitStub has been called')
                expect(getFinalTotalCountQueryStub.called).to.be.true('getFinalTotalCountQueryStub has been called')
                expect(queryStub.calledOnce).to.be.true('queryStub is called once')
            })

        it('should throw BadRequestError when given dataDbId is not defined',
            async () => {
                req.body = { "dataDbId": "" }
                req.query.sort = 'testSort:ASC'

                errorBuilderStub.resolves('BadRequestError')

                const result = await post(req, res, next)

                expect(errorBuilderStub.called).to.be.true('errorBuilderStub is called')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
                expect(getFilterStub.called).to.be.false('getFilterStub has not been called')
                expect(getOrderStringStub.called).to.be.false('getOrderStringStub has not been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.false('getFinalSqlQueryWoLimitStub has not been called')
                expect(getFinalTotalCountQueryStub.called).to.be.false('getFinalTotalCountQueryStub has not been called')
                expect(queryStub.called).to.be.false('queryStub is not called')
            })

        it('should throw BadRequestError when given dataDbId has invalid format',
            async () => {
                req['body'] = {
                    "dataDbId": "abc"
                }
                req.query.sort = 'testSort:ASC'

                errorBuilderStub.resolves('BadRequestError')

                const result = await post(req, res, next)

                expect(errorBuilderStub.called).to.be.true('errorBuilderStub is called')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
                expect(getFilterStub.called).to.be.false('getFilterStub has not been called')
                expect(getOrderStringStub.called).to.be.false('getOrderStringStub has not been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.false('getFinalSqlQueryWoLimitStub has not been called')
                expect(getFinalTotalCountQueryStub.called).to.be.false('getFinalTotalCountQueryStub has not been called')
                expect(queryStub.called).to.be.false('queryStub is not called')
            })

        it('should throw BadRequestError when given dataDbId has no existing record',
            async () => {
                req['body'] = {
                    "dataDbId": "456"
                }
                req.query.sort = 'testSort:ASC'

                queryStub.resolves([])
                errorBuilderStub.resolves('BadRequestError')

                const result = await post(req, res, next)

                expect(errorBuilderStub.called).to.be.true('errorBuilder must be called')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should successfully retrieve list members records with fields parameters',
            async () => {
                req['body'] = {
                    "fields": "test fields params"
                }
                req.query.sort = 'testSort:ASC'

                getFilterStub.resolves('testFilter')
                getOrderStringStub.resolves(['testOrderString'])
                getFinalSqlQueryWoLimitStub.resolves('testQueryWoLimit')
                getFinalTotalCountQueryStub.resolves('testFinalQuery')
                queryStub.resolves([{ 'sample record': 'sample values' }])

                const result = await post(req, res, next)

                expect(getFilterStub.called).to.be.true('getFilterStub has been called')
                expect(getOrderStringStub.called).to.be.true('getOrderStringStub has been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.true('getFinalSqlQueryWoLimitStub has been called')
                expect(getFinalTotalCountQueryStub.called).to.be.true('getFinalTotalCountQueryStub has been called')
                expect(queryStub.calledOnce).to.be.true('queryStub is called once')
            })

        it('should successfully retrieve list members records with distinctOn parameters',
            async () => {
                req['body'] = {
                    "distinctOn": "testFieldsParams"
                }
                req.query.sort = 'testSort:ASC'

                getFilterStub.resolves('testFilter')
                getOrderStringStub.resolves(['testOrderString'])
                getFinalSqlQueryWoLimitStub.resolves('testQueryWoLimit')
                getFinalTotalCountQueryStub.resolves('testFinalQuery')
                queryStub.resolves([{ 'sample record': 'sample values' }])

                const result = await post(req, res, next)

                expect(getFilterStub.called).to.be.true('getFilterStub has been called')
                expect(getOrderStringStub.called).to.be.true('getOrderStringStub has been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.true('getFinalSqlQueryWoLimitStub has been called')
                expect(getFinalTotalCountQueryStub.called).to.be.true('getFinalTotalCountQueryStub has been called')
                expect(queryStub.calledOnce).to.be.true('queryStub is called once')
            })

        it('should successfully retrieve list members records without request body parameters',
            async () => {
                req['body'] = {}
                req.query.sort = 'testSort:ASC'

                getFilterStub.resolves('testFilter')
                getOrderStringStub.resolves(['testOrderString'])
                getFinalSqlQueryWoLimitStub.resolves('testQueryWoLimit')
                getFinalTotalCountQueryStub.resolves('testFinalQuery')
                queryStub.resolves([{ 'sample record': 'sample values' }])

                const result = await post(req, res, next)

                expect(getFilterStub.called).to.be.true('getFilterStub has been called')
                expect(getOrderStringStub.called).to.be.true('getOrderStringStub has been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.true('getFinalSqlQueryWoLimitStub has been called')
                expect(getFinalTotalCountQueryStub.called).to.be.true('getFinalTotalCountQueryStub has been called')
                expect(queryStub.calledOnce).to.be.true('queryStub is called once')
            })

        it('should successfully retrieve list members records corresponding to given filters',
            async () => {
                req['body'] = {
                    "germplasmDbId": "12345"
                }
                req.query.sort = 'testSort:ASC'

                getFilterStub.resolves('testFilter')
                getOrderStringStub.resolves(['testOrderString'])
                getFinalSqlQueryWoLimitStub.resolves('testQueryWoLimit')
                getFinalTotalCountQueryStub.resolves('testFinalQuery')
                queryStub.resolves([{ 'sample record': 'sample values' }])

                const result = await post(req, res, next)

                expect(getFilterStub.called).to.be.true('getFilterStub has been called')
                expect(getOrderStringStub.called).to.be.true('getOrderStringStub has been called')
                expect(getFinalSqlQueryWoLimitStub.called).to.be.true('getFinalSqlQueryWoLimitStub has been called')
                expect(getFinalTotalCountQueryStub.called).to.be.true('getFinalTotalCountQueryStub has been called')
                expect(queryStub.calledOnce).to.be.true('queryStub is called once')
            })
    })
})