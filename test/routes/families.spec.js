/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const processQueryHelper = require('../../helpers/processQuery/index')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Families', () => {
    describe('/GET families/{id}/members', () => {
        const { get } = require('../../routes/v3/families/_id/members');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub
        let getFilterStub
        let getOrderStringStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the'
            + ' family ID is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: "1a"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(0, 'getOrderString must NOT be called')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
            expect(queryStub.callCount).to.equal(0, 'query must NOT be called')
        })

        it('should generate an InternalError when the'
            + ' family retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('Internal Error')

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(0, 'getOrderString must NOT be called')
            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the'
            + ' family does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([])

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(0, 'getOrderString must NOT be called')
            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
            
        })

        it('should generate a BadRequestError when the'
            + ' the sort value is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort:"attrA:asc"
                },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{familyDbId:1}])
            getOrderStringStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly once')
            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate an InternalError when the'
            + ' family member retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort:"attrA:asc"
                },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{familyDbId:1}])
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly once')
            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully return the family record'
            + ' when the family has no members', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{familyDbId:1}])
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves([])

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(0, 'getOrderString must NOT be called')
            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })

        it('should generate an InternalError when'
            + ' the family member count fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{familyDbId:1}])
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves([{familyMemberDbId:1}])
            queryStub.onCall(2).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(0, 'getOrderString must NOT be called')
            expect(queryStub.callCount).to.equal(3, 'query must be called exactly thrice')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should set count to 0 when'
            + ' the member count does not contain the count', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{familyDbId:1}])
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves([{familyMemberDbId:1}])
            queryStub.onCall(2).resolves([])

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(0, 'getOrderString must NOT be called')
            expect(queryStub.callCount).to.equal(3, 'query must be called exactly thrice')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })

        it('should successfully retrieve all family members', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{familyDbId:1}])
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves([{familyMemberDbId:1}])
            queryStub.onCall(2).resolves([{count:1}])

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(0, 'getOrderString must NOT be called')
            expect(queryStub.callCount).to.equal(3, 'query must be called exactly thrice')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })
    })
})