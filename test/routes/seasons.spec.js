/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

describe('Seasons', () => {
    describe('/GET seasons', () => {
        const { get } = require('../../routes/v3/seasons/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve all accessible seasons records',
            async () => {

        })
    })

    describe('/POST seasons', () => {
        const { post } = require('../../routes/v3/seasons/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new season record', async () => {

        })
    })

    describe('/GET/:id seasons', () => {
        const { get } = require('../../routes/v3/seasons/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of a specific and ' +
            'existing season record given a season ID', async () => {

        })
    })

    describe('/PUT/:id seasons', () => {
        const { put } = require('../../routes/v3/seasons/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update a specific and existing season ' +
            'record given a season ID', async () => {

        })
    })

    describe('/DELETE/:id seasons', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete a specific and existing season ' +
            'record given a season ID', async () => {

        })
    })



})