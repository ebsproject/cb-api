/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

describe('Persons', () => {
    describe('/GET persons', () => {
        const { get } = require('../../routes/v3/persons/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible person ' +
            'records', async () => {

        })
    })

    describe('/POST persons', () => {
        const { post } = require('../../routes/v3/persons/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new person record', async () => {

        })
    })

    describe('/GET/:id persons', () => {
        const { get } = require('../../routes/v3/persons/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of a specific and ' +
            'existing person record given a person ID', async () => {

        })
    })

    describe('/PUT/:id persons', () => {
        const { put } = require('../../routes/v3/persons/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update a specific and existing person ' +
            'record given a person ID', async () => {

        })
    })

    describe('/DELETE/:id persons', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete a specific and existing person ' +
            'record given a person ID', async () => {

        })
    })

    describe('/GET/:id/dashboard-configurations persons', () => {
        const { get } = require('../../routes/v3/persons/_id/dashboard-configurations');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve the dashboard configuration of a ' +
            'specific and existing person record given a person ID',
            async () => {

        })
    })

    describe('/POST/:id/dashboard-configurations persons', () => {
        const { post } = require('../../routes/v3/persons/_id/dashboard-configurations');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new dashboard configuration of a ' +
            'specific and existing person record given a person ID',
            async () => {

        })
    })

    describe('/PUT/:id/dashboard-configurations persons', () => {
        const { put } = require('../../routes/v3/persons/_id/dashboard-configurations');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update a dashboard configuration of a ' +
            'specific and existing person record given a person ID',
            async () => {

        })
    })

    describe('/GET/:id/programs persons', () => {
        const { get } = require('../../routes/v3/persons/_id/programs');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of programs a person ' +
            'belongs to given a person ID', async () => {

        })
    })

    describe('/GET/:id/teams persons', () => {
        const { get } = require('../../routes/v3/persons/_id/teams');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of teams a person ' +
            'belongs to given a person ID', async () => {

        })
    })
})