/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize')
const processQueryHelper = require('../../../../helpers/processQuery/index')
const productSearchHelper = require('../../../../helpers/queryTool/index')
const listMemberHelper = require('../../../../helpers/listsMembers/index')
const errorBuilder = require('../../../../helpers/error-builder')
const tokenHelper = require('../../../../helpers/auth/token')

const { expect } = require('chai')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Lists', ()=>{
    describe('POST lists/:id/members-search',()=>{
        const { post } = require('../../../../routes/v3/lists/_id/members-search')
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            productSearchHelperStub = sandbox.stub(productSearchHelper, 'getFilterCondition')
            errorBuilderStub = sandbox.stub(errorBuilder,'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when list id has invalid format', 
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'a',
                        duplicatesOnly:'false'
                    },
                    query:{
                        sort:''
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                errorBuilderStub.resolves('BadRequestError')

                await post(req,res,next)

                expect(errorBuilderStub.called).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400089)
        })

        it('should generate an InternalError if retrieval of list information fails',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141',
                        duplicatesOnly:'false'
                    },
                    query:{
                        sort:''
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.rejects()
                errorBuilderStub.resolves('InternalError')

                await post(req,res,next)

                expect(queryStub.calledOnce).to.be.true('query is called once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError if no list record is found',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141',
                        duplicatesOnly:'false'
                    },
                    query:{
                        sort:''
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.resolves([])
                errorBuilderStub.resolves('NotFoundError')

                await post(req,res,next)

                expect(queryStub.calledOnce).to.be.true('query is called once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404031)
        })

        it('should generate a BadRequestError if orderString has invalid content',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141',
                        duplicatesOnly:'false'
                    },
                    body: '',
                    query:{
                        sort:''
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.onFirstCall().resolves([
                    {
                        'id':123,
                        'type':'package'
                    }
                ])
                getOrderStringStub.resolves('invalid')
                errorBuilderStub.resolves('BadRequestError')

                await post(req,res,next)

                expect(queryStub.calledOnce).to.be.true('queryStub is called once')
                expect(getOrderStringStub.calledOnce).to.be.true('getOrderString is called once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate an InternalError if retrieval of list members fails',
            async () =>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141',
                        duplicatesOnly:'false'
                    },
                    body: '',
                    query:{
                        sort:''
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.onFirstCall().resolves([
                    {
                        'id':123,
                        'type':'package'
                    }
                ])

                getOrderStringStub.resolves('')
                queryStub.onSecondCall().rejects()
                errorBuilderStub.resolves('InternalError')

                await post(req,res,next)

                expect(queryStub.calledTwice).to.be.true('query is called twice')
                expect(getOrderStringStub.calledOnce).to.be.true('getOrderString is called once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError if retrieval of members count fails',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141',
                        duplicatesOnly:'false'
                    },
                    body: '',
                    query:{},
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.onFirstCall().resolves([
                    { 'id':123,'type':'package' }
                ])
                queryStub.onSecondCall().resolves([
                    { 'id':1231,'displayValue':'1231' },
                    { 'id':1232,'displayValue':'1232' }
                ])
                queryStub.onThirdCall().rejects()
                errorBuilderStub.resolves('InternalError')

                await post(req,res,next)

                expect(queryStub.called).to.be.true('query is called 3 times')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully retrieved all list members of a list',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141',
                        duplicatesOnly:'true'
                    },
                    body: {
                        fields: 'listMember.id AS id|listMember.display_value AS displayValue',
                        isActive: 'true'
                    },
                    query:{
                        sort:''
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.onFirstCall().resolves([
                    { 'id':123,'type':'package' }
                ])
                getOrderStringStub.resolves('')
                queryStub.onSecondCall().resolves([
                    { 'id':1231,'displayValue':'1231' },
                    { 'id':1232,'displayValue':'1232' }
                ])
                queryStub.onThirdCall().resolves([{ count:2 }])

                await post(req,res,next)

                expect(queryStub.called).to.be.true('query is called 3 times')
                expect(getOrderStringStub.called).to.be.true('getOrderString is called once')
                expect(errorBuilderStub.called).to.be.false('no error is called')
        })

        it('should successfully retrieved all duplicate list members of a list',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141',
                        duplicatesOnly:'true'
                    },
                    body: {
                        fields: 'listMember.id AS id|listMember.display_value AS displayValue',
                        isActive: 'true',
                        'displayValue':'a'
                    },
                    query:{
                        sort:'displayValue:asc'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                productSearchHelperStub.resolves('a')
                queryStub.onFirstCall().resolves([
                    { 'id':123,'type':'package' }
                ])
                getOrderStringStub.resolves('')
                queryStub.onSecondCall().resolves([
                    { 'id':1231,'displayValue':'1231' },
                    { 'id':1232,'displayValue':'1231' }
                ])
                queryStub.onThirdCall().resolves([{ count:2 }])

                await post(req,res,next)

                expect(productSearchHelperStub.called).to.be.true('query is called once')
                expect(queryStub.called).to.be.true('query is called 3 times')
                expect(getOrderStringStub.called).to.be.true('getOrderString is called once')
                expect(errorBuilderStub.called).to.be.false('no error is called')
        })
    })

    describe('POST lists/:id/reorder-list-members',()=>{
        const { post } = require('../../../../routes/v3/lists/_id/reorder-list-members')
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            listMemberHelperStub = sandbox.stub(listMemberHelper, 'getMembersQuery')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            errorBuilderStub = sandbox.stub(errorBuilder,'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when list id has invalid format',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'a'
                    },
                    query:{
                        sort:''
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                errorBuilderStub.resolves('BadRequestError')

                await post(req,res,next)

                expect(errorBuilderStub.called).to.be.true('errorBuilder is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400089)
        })

        it('should generate an InternalError if it retrieval of list record fails',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {
                        reorderCondition:'SORT',
                        sort:'label:desc'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.rejects()
                errorBuilderStub.resolves('InternalError')

                await post(req,res,next)

                expect(queryStub.called).to.be.true('query is called once')
                expect(errorBuilderStub.called).to.be.true('errorBuilderStub is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an BadRequestError if reorderCondition is undefined',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {},
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.rejects()
                errorBuilderStub.resolves('InternalError')

                await post(req,res,next)
        })

        it('should generate a NotFoundError if no list record is retrieved',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {
                        reorderCondition:'SORT',
                        sort:'label:desc'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.resolves([])
                errorBuilderStub.resolves('NotFoundError')

                await post(req,res,next)

                expect(queryStub.called).to.be.true('query is called once')
                expect(errorBuilderStub.called).to.be.true('errorBuilderStub is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404031)
        })

        it('should generate a BadRequestError if sortOrderString has invalid value',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {
                        reorderCondition:'SORT',
                        sort:'label:desc'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.resolves([
                    { id:2141, type:'package' }
                ])
                getOrderStringStub.resolves('invalid')
                errorBuilderStub.resolves('UnauthorizedError')

                await post(req,res,next)

                expect(getOrderStringStub.called).to.be.true('getOrderString is called once')
                expect(errorBuilderStub.called).to.be.true('errorBuilderStub is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate an UnauthorizedError when no person record is retrieved',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {
                        reorderCondition:'SORT',
                        sort:'label:desc'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.resolves([
                    { id:2141, type:'package' }
                ])
                getOrderStringStub.resolves('')
                getFinalSqlQueryStub.resolves('')
                tokenHelperStub.resolves(null)
                errorBuilderStub.resolves('UnauthorizedError')

                await post(req,res,next)

                expect(tokenHelperStub.called).to.be.true('getUserId is called once')
                expect(errorBuilderStub.called).to.be.true('errorBuilderStub is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate an InternalError when update order number of list items fails',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {
                        reorderCondition:'SORT',
                        sort:'label:desc'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.onFirstCall().resolves([
                    { id:2141, type:'package' }
                ])
                getOrderStringStub.resolves('')
                getFinalSqlQueryStub.resolves('')
                tokenHelperStub.resolves(1)
                queryStub.onSecondCall().resolves(true)
                transactionStub.throws(new Error('Dummy Error'))
                errorBuilderStub.resolves('InternalError')

                await post(req,res,next)

                expect(transactionStub).to.throw(Error)
                expect(errorBuilderStub.called).to.be.true('errorBuilderStub is called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully update order number of list items using sort configuration',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {
                        reorderCondition:'SORT',
                        sort:'label:desc'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                queryStub.onFirstCall().resolves([
                    { id:2141, type:'package' }
                ])
                getOrderStringStub.resolves('')
                getFinalSqlQueryStub.resolves('')
                tokenHelperStub.resolves(1)
                transactionStub.resolves({
                    commit: sinon.stub()
                })
                queryStub.onSecondCall().resolves(true)

                await post(req,res,next)

                expect(queryStub.calledOnce).to.be.true('query is called once')
                expect(transactionStub.calledOnce).to.be.true('transaction is called once')
        })



        it('should successfully update order number of list items',
            async ()=>{
                req = {
                    headers: {
                        host:'testHost'
                    },
                    params: {
                        id:'2141'
                    },
                    body: {
                        reorderCondition:'ASC'
                    },
                    paginate: {
                        limit:100,
                        offset:0
                    }
                }

                tokenHelperStub.resolves(1)
                transactionStub.resolves([[{ id: 1 }]])

                await post(req,res,next)

                expect(transactionStub.calledOnce).to.be.true('transaction is called once')
        })
    })
})