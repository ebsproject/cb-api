/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errorBuilder = require('../../../../helpers/error-builder')
const processQueryHelper = require('../../../../helpers/processQuery/index')
const { knex } = require('../../../../config/knex')
const { expect } = require('chai')

// default request parameters
let req = {
    paginate: {
        limit: 100,
        offset: 0
    },
    params: {},
    query: {},
    body: {},
    headers: {
        host: 'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('/POST germplasm/:id/relations-search', () => {
    const { post } = require('../../../../routes/v3/germplasm/_id/relations-search')
    const sandbox = sinon.createSandbox()

    // instatiate stub variables
    let errorBuilderStub
    let queryStub

    beforeEach(()=>{
        errorBuilderStub = sandbox.stub(errorBuilder,'getError')
        queryStub = sandbox.stub(sequelize, 'query')
    })

    afterEach(()=>{
        sandbox.restore()
    })

    it('throws InternalError if germplasm ID provided is invalid',
        async () => {
        // 
        req['params'] = { id:'' }

        errorBuilderStub.resolves('InternalError')

        // act
        await post(req,res,next)

        // assert
        expect(errorBuilderStub.called).to.be.true('InternalError: invalid input')
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
    })

    it('throws InternalError if germplasm ID provided has invalid format.',
        async () => {
        // 
        req['params'] = { id:'notInteger' }

        errorBuilderStub.resolves('InternalError')

        // act
        await post(req,res,next)
        
        // assert
        expect(errorBuilderStub.called).to.be.true('InternalError: invalid germplasm ID format')
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400246)
    })

    it('throws InternalError if germplasm query fails to retrieve record',
        async () => {
            // 
            req['params'] = { id:'1364345' }

            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            // act
            await post(req,res,next)
            
            // assert
            expect(queryStub.called).to.be.true('Query called once')
            expect(errorBuilderStub.called).to.be.true('InternalError: query failed')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })

    it('throws NotFoundError if no germplasm record is retrieved', 
        async () => {
        // 
        req['params'] = { id:'000001' }

        queryStub.onFirstCall().resolves([])
        errorBuilderStub.resolves('NotFoundError')

        // act
        await post(req,res,next)

        // assert
        expect(queryStub.calledOnce).to.be.true('query 1st call success')
        expect(errorBuilderStub.called).to.be.true('NotFoundError: no germplasm record found')
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404042)
    })

    it('throws InternalError if format of value provided for depth is invalid', 
        async () => {
        // 
        req['params'] = { id:'1364345' }
        req['body'] = { depth: 'depth' }

        queryStub.onFirstCall().resolves(['germplasmRecord'])
        errorBuilderStub.resolves('InternalError')

        // act
        await post(req,res,next)

        // assert
        expect(queryStub.called).to.be.true('query success')
        expect(errorBuilderStub.called).to.be.true('InternalError: Invalid depth')
        expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
    })

    it('throws InternalError if germplasm query fails to retrieve record',
        async () => {
            // 
            req['params'] = { id:'1364345' }
            req['body'] = {}

            queryStub.onFirstCall().resolves(['record1'])
            queryStub.onSecondCall().resolves([{'config_value':{'ancestryExportDepth':"6"}}])
            queryStub.onThirdCall().rejects()
            errorBuilderStub.resolves('InternalError')

            // act
            const results = await post(req,res,next)
            
            // assert
            expect(queryStub.callCount).to.be.equals(3,'Query called twice')
    })

    it('should successfully retrieve germplasm relation records with depth parameter', 
        async () => {
        // 
        req['params'] = { id:'1364345' }
        req['body'] = { depth:'2' }

        queryStub.onFirstCall().resolves(['record1'])
        queryStub.onSecondCall().resolves({'rows':['ancestry'],'count':1})

        // act
        const results = await post(req,res,next)

        // assert
        expect(queryStub.called).to.be.true('query success')
        expect(errorBuilderStub.called).to.be.false('no error should have been called')
        expect(results !== null).to.be.true('successfully extracted the list member search query')
    })

    it('should successfully retrieve germplasm relation records', 
        async () => {
        // 
        req['params'] = { id:'1364345' }
        req['body'] = {}

        queryStub.onFirstCall().resolves(['record1'])
        queryStub.onSecondCall().resolves([{'config_value':{'ancestryExportDepth':"6"}}])
        queryStub.onThirdCall().resolves({'rows':['ancestry'],'count':1})

        // act
        const results = await post(req,res,next)

        // assert
        expect(queryStub.called).to.be.true('query success')
        expect(errorBuilderStub.called).to.be.false('no error should have been called')
        expect(results !== null).to.be.true('successfully extracted the list member search query')
    })
})