/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errorBuilder = require('../../../../helpers/error-builder')
const processQueryHelper = require('../../../../helpers/processQuery/index')
const { knex } = require('../../../../config/knex')
const { expect } = require('chai')

// default request parameters
let req = {
    paginate: {
        limit: 100,
        offset: 0
    },
    params: {},
    query: {},
    body: {},
    headers: {
        host: 'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

const next = {}

describe('/POST germplasm/:id/pedigrees-search', () => {
    const { post } = require('../../../../routes/v3/germplasm/_id/pedigrees-search.js')
    const sandbox = sinon.createSandbox()

    // instatiate stub variables
    let errorBuilderStub
    let queryStub

    beforeEach(()=>{
        errorBuilderStub = sandbox.stub(errorBuilder,'getError')
        queryStub = sandbox.stub(sequelize, 'query')
    })

    afterEach(()=>{
        sandbox.restore()
    })

    it('should successfully retrieve the pedigree info for a germplasm without depth', 
        async () => {

            req['params'] = { id:'1364510' }
            req['body'] = {}

            queryStub.onFirstCall().resolves({'data':[{ 'designation':"Rothiesholm" }]})
            queryStub.onSecondCall().resolves([{'config_value':{'ancestryExportDepth':"6"}}])
            queryStub.onThirdCall().resolves({'data':[{'individual':"Rothiesholm"}],'count':1})

            // act
            const results = await post(req,res,next)

            // assert
            expect(queryStub.called).to.be.true('query success')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
            expect(results !== null).to.be.true('successfully extracted the pedigree information for Rothiesholm')
    })

    it('should successfully retrieve the pedigree info for a germplasm with depth', 
        async () => {
            req['params'] = { id:'1364510' }
            req['body'] = { depth:"6" }

            queryStub.onFirstCall().resolves({'data':[{ 'designation':"Rothiesholm" }]})
            queryStub.onSecondCall().resolves({'data':[{'individual':"Rothiesholm"}],'count':1})

            // act
            const results = await post(req,res,next)

            // assert
            expect(queryStub.called).to.be.true('query success')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
            expect(results !== null).to.be.true('successfully extracted the pedigree information for Rothiesholm')
    })

    it('should successfully retrieve the pedigree info for a germplasm with depth and limit', 
        async () => {
            req['params'] = { id:'1364510', limit:'3' }
            req['body'] = { depth:"6" }

            queryStub.onFirstCall().resolves({'data':[{ 'designation':"Rothiesholm" }]})
            queryStub.onSecondCall().resolves({'data':[{'individual':"Rothiesholm"}],'count':1})

            // act
            const results = await post(req,res,next)

            // assert
            expect(queryStub.called).to.be.true('query success')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
            expect(results !== null).to.be.true('successfully extracted the pedigree information for Rothiesholm')
    })

    it('should throw InternalError if germplasm ID provided is invalid',
        async ()=> {
            req['params'] = { id:'' }
            req['body'] = {}

            errorBuilderStub.resolves('InternalError')

            // act
            await post(req,res,next)

            // assert
            expect(errorBuilderStub.called).to.be.true('InternalError: invalid input')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
    })

    it('should throw InternalError if germplasm ID has invalid format',
        async () => {
            req['params'] = { id:'invalid' }
            req['body'] = {}

            errorBuilderStub.resolves('InternalError')

            // act
            await post(req,res,next)

            // assert
            expect(errorBuilderStub.called).to.be.true('InternalError: invalid input')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400246)
    })

    it('should throw InternalError if retrieval of germplasm record fails',
        async () => {
            req['params'] = { id:'1364510' }
            req['body'] = {}

            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            // act
            await post(req,res,next)

            // assert
            expect(queryStub.called).to.be.true('Query called once')
            expect(errorBuilderStub.called).to.be.true('InternalError: query failed')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
    })
    
    it('should throw NotFoundError if no matching germplasm record is found',
        async () => {
            req['params'] = { id:'1364510' }
            req['body'] = {}

            queryStub.onFirstCall().resolves([])
            errorBuilderStub.resolves('NotFoundError')

            // act
            await post(req,res,next)

            // assert
            expect(queryStub.called).to.be.true('1st query call is success')
            expect(errorBuilderStub.called).to.be.true('NotFoundError: no germplasm record found')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404042)
    })

    it('should throw InternalError if format of depth value is invalid', 
        async () => {
            // 
            req['params'] = { id:'1364510' }
            req['body'] = { depth: 'depth' }

            queryStub.onFirstCall().resolves({'data':[{ 'designation':"Rothiesholm" }]})
            errorBuilderStub.resolves('InternalError')

            // act
            await post(req,res,next)

            // assert
            expect(queryStub.called).to.be.true('query success')
            expect(errorBuilderStub.called).to.be.true('InternalError: Invalid depth')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
    })

    it('should throw InternalError if retrieval of pedigree info fails', 
        async () => {
            // 
            req['params'] = { id:'1364510' }
            req['body'] = { depth: '2' }

            queryStub.onFirstCall().resolves({'data':[{ 'designation':"Rothiesholm" }]})
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            // act
            await post(req,res,next)
            
            // assert
            expect(queryStub.calledTwice).to.be.true('Query called twice')
    })
})