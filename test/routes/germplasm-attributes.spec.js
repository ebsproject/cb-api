/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')
const userValidator = require('../../helpers/person/validator')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}
const transactionMock = {
    finished: "",
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    }
}

describe('Germplasm Attributes', () => {
    const { put } = require('../../routes/v3/germplasm-attributes/_id/index');
    const ga = require('../../routes/v3/germplasm-attributes/_id/index');
        
    describe('/POST germplasm-attributes', () => {
        const { post } = require('../../routes/v3/germplasm-attributes/index');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the request body'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when records'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: []
                }
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequest when records'
            + ' is undefined', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: { }
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequest')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a InternalError when transaction creation'
            + ' fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should generate a BadRequestError when required parameters'
            + ' are missing', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when germplasmDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "abc",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when validation fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:0}])
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should generate a BadRequestError when dataQcCode'
            + ' is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1",
                            dataQcCode: "INVALID"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate InternalError when variable info retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate BadRequestError when dataValue'
            + ' is not included in the scale values (if any)', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).resolves([{
                variableDbId: 1001,
                scaleValues: [ 'x', 'y', 'z' ]
            }])

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a InternalError when insertion fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).resolves([{
                variableDbId: 1001,
                scaleValues: [ 'val1', 'val2', 'val3' ]
            }])
            queryStub.onCall(2).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully insert germplasm attribute record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            germplasmDbId: "123",
                            variableAbbrev: "VAR1",
                            dataValue: "val1"
                        }
                    ]
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:2}])
            queryStub.onCall(1).resolves([{
                variableDbId: 1001,
                scaleValues: [ 'val1', 'val2', 'val3' ]
            }])
            queryStub.onCall(2).resolves([[{id:2003}]])

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })
    })

    describe('/PUT germplasm-attributes/{id}', () => {
        const sandbox = sinon.createSandbox()

        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        req = {
            headers: {
                host: 'testHost'
            }
        }

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('throws a BadRequestError when the user ID value is null', 
            async () => {

                tokenHelperStub.onCall(0).resolves(null)
                errorBuilderStub.onCall(0).resolves('BadRequestError')

                // act
                const result = await put(req, res, next)
                
                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
                expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the user is not an ADMIN', async () => {
            req['params'] = { 'id': '123' }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(false)
            errorBuilderStub.onCall(0).resolves('BadRequestError')
            
            // act
            const result = await put(req, res, next)

            // assert
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401024)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when req.body is undefined', async () => {
            req = {
                headers: { host: 'testHost' },
                params: { 'id': '123' }
            }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            errorBuilderStub.onCall(0).resolves('BadRequestError')
            
            // act
            const result = await put(req, res, next)

            // assert
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.called).to.be.false('query must not be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            
        })

        it('throws a BadRequestError when the ID value is null', async () => {
            req['params'] = { }
            req['body'] = { }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            // act
            const result = await put(req, res, next)
            
            // assert
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when ID has invalid format', async () => {
            req['params'] = { 'id': 'ID' }
            req['body'] = { }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            // act
            const result = await put(req, res, next)
            
            // assert
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a InternalError when retrieving the germplasm attribute record fails', 
            async () => {
                req['params'] = { 'id': '123' }
                req['body'] = { }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                // act
                const result = await put(req, res, next)

                //assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            
        })

        it('should generate a NotFoundError when the germplasm attribute record is not found', 
            async () => {
                req['params'] = { 'id': '123' }
                req['body'] = { }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves([])

                // act
                const result = await put(req, res, next)

                //assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })

        it('should successfully update the germplasm attribute when'
            + ' no values are specified in the request body', async () => {
                req['params'] = { 'id': '123' }
                req['body'] = { }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{}])
                errorBuilderStub.onCall(0).resolves('BadRequestError')

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(2, 'query must be called twice')
                expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')  
        })

        it('should successfully update the germplasm attribute when'
            + ' all inputted values are equal to the current values', async () => {
                req['params'] = { 'id': '123' }
                req['body'] = {
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                 }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                queryStub.onCall(1).resolves([{count:2}])
                queryStub.onCall(2).resolves([{}])

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })

        it('should successfully update the germplasm attribute when'
            + ' all inputted values are not equal to the current values', async () => {
                req['params'] = { 'id': '123' }
                req['body'] = {
                    germplasmDbId: "4561",
                    variableDbId: "7891",
                    dataValue: "test data 1",
                    dataQcCode: "1"
                 }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{}])
                errorBuilderStub.onCall(0).resolves('BadRequestError')

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(2, 'query must be called twice')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should generate a BadRequestError when a parameter is provided is empty', 
            async () => {
                req['params'] = { 'id': '123' }
                req['body'] = { 
                    germplasmDbId: '',
                    variableDbId: '' 
                }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                transactionStub.onCall(0).resolves(transactionMock)

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called twice')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should generate a BadRequestError when germplasmDbId is not an integer', 
            async () => {
                req['params'] = { 'id': '123' }
                req['body'] = { 
                    germplasmDbId: 'abc'
                }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                transactionStub.onCall(0).resolves(transactionMock)

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })


        it('should generate a BadRequestError when variableDbId is not an integer', 
            async () => {
                req['params'] = { 'id': '123' }
                req['body'] = { 
                    variableDbId: 'abc' 
                }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                transactionStub.onCall(0).resolves(transactionMock)

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
                
        })

        it('should generate a BadRequestError when validation fails', 
            async () => {
                req['params'] = { 'id': '123' }
                req['body'] = { 
                    germplasmDbId: '5678' 
                }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }
    
                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{count:0}]) 
                errorBuilderStub.onCall(0).resolves('BadRequestError')
    
                const result = await put(req, res, next)
    
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(2, 'query must be called twice')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
                expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should generate a InternalError when updating the record fails', 
            async () => {
                req['params'] = { 'id': '123' }
                req['body'] = {
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                 }

                germplasmAttribute = {
                    germplasmAttributeDbId: "123",
                    germplasmDbId: "456",
                    variableDbId: "789",
                    dataValue: "test data",
                    dataQcCode: ""
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmAttribute)
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{count:2}])
                queryStub.onCall(2).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called thrice')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')  
        })

        it('should generate an InternalError when an error occurs', async () => {
            req['params'] = { 'id': '123' }
            req['body'] = { 'germplasmDbId':'456' }

            germplasmAttribute = {
                germplasmAttributeDbId: "123",
                germplasmDbId: "456",
                variableDbId: "789",
                dataValue: "test data",
                dataQcCode: ""
            }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves([])


            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves(germplasmAttribute)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })

    describe('/DELETE germplasm-attribute/{id}', () => {
        const sandbox = sinon.createSandbox()

        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        let result

        req = {
            headers: {
                host: 'testHost'
            }
        }

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError when the userDbId is null', 
            async () => {

            req['params'] = { id: 1234 }

            tokenHelperStub.onCall(0).resolves(null)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should throw a UnauthorizedError when the user is not an admin',
            async () => {

            req['params'] = { id: 1234 }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(false)
            errorBuilderStub.onCall(0).resolves('UnauthorizedError')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401027)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should throw BadRequestError when the germplasmAttributeDbId is not an integer',
            async () => {
            
            req['params'] = { id: 'abc' }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400249)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError when the germplasm attribute record retrieval fails',
            async () => {
            
            req['params'] = { id: '1234' }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the germplasm attribute record does not exist',
            async () => {
            
            req['params'] = { id: '1234' }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves([])
            errorBuilderStub.onCall(0).resolves('NotFoundError')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404055)
        })

        it('should successfully delete the germplasm attribute record', async () => {
            req['params'] = { id: '345' }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves([{ germplasmAttributeDbId: 345 }])
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves('success')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate an InternalError when deletion fails', async () => {
            req['params'] = { id: '345' }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves([{ germplasmAttributeDbId: 345 }])
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })

        it('should generate an InternalError when transaction creation fails', async () => {
            req['params'] = { id: '345' }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves([{ germplasmAttributeDbId: 345 }])
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            result = await ga.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })
    })
})