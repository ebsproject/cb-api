/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const processQueryHelper = require('../../helpers/processQuery/index')
const errorBuilder = require('../../helpers/error-builder')
const { expect } = require('chai')
const { knex } = require('../../config/knex')
const logger = require('../../helpers/logger/index')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Configurations', () => {
    describe('/POST configurations-search', () => {
        const { post } = require('../../routes/v3/configurations-search/index')
        const sandbox = sinon.createSandbox()
        let queryStub
        let getFilterWithInfoStub
        let getDistinctStringStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let getTotalCountQueryStub
        let errorBuilderStub
        let knexRawStub
        let knexSelectStub
        let getFieldValuesStringStub
        let logFailingQueryStub

        beforeEach(() => {
            knexRawStub = sandbox.stub(knex, 'raw')
            knexSelectStub = sandbox.stub(knex, 'select')
            queryStub = sandbox.stub(sequelize, 'query')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
            getFilterWithInfoStub = sandbox.stub(processQueryHelper, 'getFilterWithInfo')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            getTotalCountQueryStub = sandbox.stub(processQueryHelper, 'getTotalCountQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve configuration records ' + 
            'with EMPTY request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['record1, record2, record3'])
            getTotalCountQueryStub.resolves('totalCountQuery')
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getTotalCountQueryStub.calledOnce).to.be.true('getTotalCountQuery must be successfully called once')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve config records ' + 
            'with distinctOn in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
                body: {
                    distinctOn: 'thisColumn'
                }
            }

            getFilterWithInfoStub.resolves('getFilterWithInfo')
            getDistinctStringStub.resolves('addedDistinctString')
            getOrderStringStub.resolves('orderString')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['configRecord1, configRecord2, configRecord3'])
            getTotalCountQueryStub.resolves('totalCountQuery')
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilterWithInfo must be called once')
            expect(getDistinctStringStub.calledOnce).to.be.true('getDistinctString must be successfully called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getTotalCountQueryStub.calledOnce).to.be.true('getTotalCountQuery must be successfully called once')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve config records ' + 
            'with fields in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
                body: {
                    fields: 'this.field AS thisField'
                }
            }

            getFilterWithInfoStub.resolves('getFilterWithInfo')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['configRecord1, configRecord2, configRecord3'])
            getTotalCountQueryStub.resolves('totalCountQuery')
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilterWithInfo must be called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getTotalCountQueryStub.calledOnce).to.be.true('getTotalCountQuery must be successfully called once')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve config records ' + 
            'with fields and distinctOn in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
                body: {
                    fields: 'this.field AS thisField',
                    distinctOn: 'thisColumn'
                }
            }

            getFilterWithInfoStub.resolves('getFilterWithInfo')
            getDistinctStringStub.resolves('addedDistinctString')
            getOrderStringStub.resolves('orderString')
            getFieldValuesStringStub.resolves('fieldsString')
            knexRawStub.resolves('raw')
            knexSelectStub.resolves('select')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['configRecord1, configRecord2, configRecord3'])
            getTotalCountQueryStub.resolves('totalCountQuery')
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilterWithInfo must be called once')
            expect(getDistinctStringStub.calledOnce).to.be.true('getDistinctString must be successfully called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(knexRawStub.calledOnce).to.be.true('knex.raw must be called once')
            expect(knexSelectStub.calledOnce).to.be.true('knex.select must be called once')
            expect(getFieldValuesStringStub.calledOnce).to.be.true('getFieldValuesString must be successfully called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getTotalCountQueryStub.calledOnce).to.be.true('getTotalCountQuery must be successfully called once')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if sort argument ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'invalid:sorting'
                }
            }

            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400057)
            expect(queryStub.called).to.be.false('no query should have been called')
        })

        it('should generate InternalError when the query for retrieving ' +
            'the config records fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            getFinalSqlQueryStub.resolves('failingQuery')
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate InternalError if the config count query fails',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['config1'])
            getTotalCountQueryStub.resolves('totalCountQuery')
            queryStub.onSecondCall().rejects()
            logFailingQueryStub.resolves('logFailingQuery')
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getTotalCountQueryStub.calledOnce).to.be.true('getTotalCountQuery must be successfully called once')
            expect(logFailingQueryStub.calledOnce).to.be.true('logFailingQuery must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate NotFoundError if no config records were found',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            getFinalSqlQueryStub.resolves('query')
            queryStub.resolves([])

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should generate BadRequestError if conditionString is invalid',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {},
                body: {
                    test: 'invalidArgument'
                }
            }

            getFilterWithInfoStub.resolves({'mainQuery':'invalid'})
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilterWithInfo must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })
    })
})

