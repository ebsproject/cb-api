/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const processQueryHelper = require('../../helpers/processQuery/index.js')

let errors = require('restify-errors')
let errorBuilder = require('../../helpers/error-builder')
let logger = require('../../helpers/logger/index')
const { knex } = require('../../config/knex')

const {post} = require("../../routes/v3/entry-lists-search");

let req

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
};
const next = {};

describe('Entry Lists Data Search', () => {
    describe('POST entry-list-data-search', () => {
        const { post } = require('../../routes/v3/entry-list-data-search');
        const sandbox = sinon.createSandbox()
        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let getCountFinalSqlQueryStub
        let errorBuilderStub
        let queryStub
        let knexColumnStub


        beforeEach(() => {
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            knexColumnStub = sandbox.stub(knex, 'column')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully search entry lists data with EMPTY BODY', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            getFilterStub.resolves([])
            getOrderStringStub.resolves([])
            queryStub.onFirstCall().resolves(['entryListData1, entryListData2, entryListData3'])
            getCountFinalSqlQueryStub.resolves(100)
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            const result = await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be called successfully')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')

        })

        it('should successfully search entry lists data with BODY', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {},
                body: {
                    "entryListDbId" : "1"
                }
            }

            getFilterStub.resolves([])
            getOrderStringStub.resolves([])
            queryStub.onFirstCall().resolves(['entryListData1, entryListData2, entryListData3'])
            getCountFinalSqlQueryStub.resolves(100)
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            const result = await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be called successfully')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')

        })

        it('should successfully search entry lists data with fields parameter', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {},
                body: {
                    fields: 'this.field AS thisField'
                }
            }

            knexColumnStub.resolves('column')
            getFilterStub.resolves([])
            getOrderStringStub.resolves([])
            queryStub.onFirstCall().resolves(['entryListData1, entryListData2, entryListData3'])
            getCountFinalSqlQueryStub.resolves(100)
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            const result = await post(req, res, next)

            expect(knexColumnStub.calledOnce).to.be.true('knex.column must be called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be called successfully')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')

        })

        it('should generate BadRequestError(400022) if conditionString is invalid.', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {},
                body: {
                    test: 'invalidArguement'
                },
            }

            getFilterStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate BadRequestError(400004) if orderString is invalid.', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'invalidArguement'
                },
                body: {},
            }

            getFilterStub.resolves([])
            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate InternalError when the query for retrieving ' +
            'the entry list data record fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })
})
