/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')
let userValidator = require('../../helpers/person/validator.js')

const {post} = require("../../routes/v3/cross-parents")
const { expect } = require('chai')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}
const next = {}

describe('Entry List Data', () => {
    describe('POST entry-list-data', () => {
        const { post } = require('../../routes/v3/entry-list-data')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let errorStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError(400009) when the request body is not set', async () => {
            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        it('should throw a BadRequestError(400005) when the records array is empty', async () => {
            //arrange
            req.body = {}

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)

        })

        it('should throw an InternalError(500001) if other errors were encountered', async () => {
            //arrange
            req.body = {
                records: {}
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            transactionStub.resolves({
                rollback: sinon.stub(),
            })

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should return a BadRequestError if the required parameters are missing', async () => {
            //arrange
            req.body = {
                records: [{}]
            }

            //mock
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

    })

    describe('/PUT/:id entry-list-data', () => {
        const { put } = require('../../routes/v3/entry-list-data/_id/index');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub
        let getUserIdStub
        let transactionStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) if user ID is not an ' +
            'found', async () => {
                // arrange
                req.params = {
                    id: '1'
                }

                //mock
                getUserIdStub.resolves(null)
                errorBuilderStub.resolves('BadRequestError')
                
                //act
                await put(req, res, next)

                //assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError(400063) if entry list data ID is not an ' +
            'integer (in string format)', async () => {
                // arrange
                req.params = {
                    id: 'notAnInteger'
                }

                //mock
                getUserIdStub.resolves(1)
                errorBuilderStub.resolves('BadRequestError')
                
                //act
                await put(req, res, next)

                //assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400063)
        })

        it('should generate InternalError(500004) when the query retrieving ' +
            'the entry list data record fails', async () => {
                // arrange
                req.params = {
                    id: '123'
                }

                //mock
                getUserIdStub.resolves(1)
                queryStub.onFirstCall().rejects()
                errorBuilderStub.resolves('InternalError')
                
                //act
                await put(req, res, next)

                //assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })

    describe('/DELETE/:id entry-list-data', () => {
        const { delete:del } = require('../../routes/v3/entry-list-data/_id/index')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub
        let getUserIdStub
        let userValidatorStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) if user ID is not an ' +
            'found', async () => {
                // arrange
                req.params = {
                    id: '1'
                }

                //mock
                getUserIdStub.resolves(null)
                errorBuilderStub.resolves('BadRequestError')
                
                //act
                await del(req, res, next)

                //assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError(400063) if entry list data ID is not an ' +
            'integer (in string format)', async () => {
                // arrange
                req.params = {
                    id: "notAnInteger"
                }

                //mock
                errorBuilderStub.resolves('BadRequestError')
                
                //act
                await del(req, res, next)

                //assert
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400063)
        })

        it('should generate InternalError(500004) when the query retrieving ' +
        'the entry list data record fails', async () => {
            // arrange
            req.params = {
                id: '123'
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().rejects()
            userValidatorStub.resolves(true)
            errorBuilderStub.resolves('InternalError')
            
            //act
            await del(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate InternalError(500002) if transaction fails ', async () => {
            // arrange
            req.params = {
                id: '123'
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves('entryListData1')
            userValidatorStub.resolves(true)
            errorBuilderStub.resolves('InternalError')
            transactionStub.resolves({
                rollback: sinon.stub(),
            })
            
            //act
            await del(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(userValidatorStub.calledOnce).to.be.true('userValidator must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })
    })
})