/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const { knex } = require('../../config/knex')
const processQueryHelper = require('../../helpers/processQuery/index')
const queryToolHelper = require('../../helpers/queryTool/index.js')
const test = require('../../helpers/test')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const req = {
    headers: {},
    query: {},
    body: {
        "variableDbId": "141"
    },
    paginate: {
        limit: 1,
        offset: 1
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Variables', () => {
    describe('/POST variables-search', () => {
        const { post } = require('../../routes/v3/variables-search/index')
        const sandbox = sinon.createSandbox()
        let getFilterConditionStub
        let getOrderString1
        let getFinalSQLQuery1

        beforeEach(function() {
            getFilterConditionStub = sandbox.stub(queryToolHelper, 'getFilterCondition').resolves([]);
            getOrderString1 = sandbox.stub(processQueryHelper, 'getOrderString').resolves([]);
            getFinalSQLQuery1 = sandbox.stub(processQueryHelper, 'getFinalSqlQuery').resolves([]);
        })

        afterEach(function() {
            sandbox.restore()
        })

        it('should successfully search and filter accessible variable records',
            async () => {
                const result = await post(req, res, next)

                expect(getFilterConditionStub.calledOnce).to.be.true();
                expect(getOrderString1.calledOnce).to.be.false();
                expect(getFinalSQLQuery1.calledOnce).to.be.true();
        })
    })
})