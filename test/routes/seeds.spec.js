/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai');
const errorBuilder = require('../../helpers/error-builder')
const { sequelize } = require('../../config/sequelize');

const req = {
    headers: {
        host:'testHost'
    },
    query: {},
    paginate: {
        limit: 1,
        offset: 1
    }
}
const res = {
    send: (statusCode, data) => {
      return {
        statusCode,
        data
      }
    }
};
const next = {};

describe('Seeds', () => {
    describe('/POST seeds', () => {
        const { post } = require('../../routes/v3/seeds/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new seed record', async () => {

        })
    })

    describe('/GET/:id seeds', () => {
        const { get } = require('../../routes/v3/seeds/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of a specific and ' +
            'existing seed record given a seed ID', async () => {

        })
    })

    describe('/PUT/:id seeds', () => {
        const { put } = require('../../routes/v3/seeds/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update a specific and existing seed record ' +
            'given a seed ID', async () => {

        })
    })

    describe('/DELETE/:id seeds', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete a specific and existing seed record ' +
            'given a seed ID', async () => {

        })
    })

    describe('/GET/:id/attributes seeds', () => {
        const { get } = require('../../routes/v3/seeds/_id/attributes');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder,'getError')
            queryStub = sandbox.stub(sequelize,'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of a specific and ' +
            'existing seed record given a seed ID', async () => {
        })

        it('should generate a BadRequestError when' +
            ' the seed ID is not an integer', async () => {
            req.params = {
                id: '123a'
            }

            const result = await get(req, res, next)

            expect(errorBuilderStub.callCount).to.be.equals(0, 'code does not use errorBuilder so no need to be called')
        })

        it('should generate an InternalError when' +
            ' seed retrieval fails', async () => {
            req.params = {
                id: '123'
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res, next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })

        it('should generate an InternalError when' +
            ' the seed attribute retrieval fails', async () => {
            req.params = {
                id: '123'
            }

            queryStub.onCall(0).resolves([{seedDbId:123}])
            queryStub.onCall(1).resolves([{attributes:[]}])
            queryStub.onCall(2).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')
            errorBuilderStub.onCall(1).resolves('InternalError')

            const result = await get(req, res, next)

            expect(errorBuilderStub.callCount).to.be.equals(2, 'errorBuilder must be called exactly twice')
            expect(queryStub.callCount).to.be.equals(3, 'query must be called exactly thrice')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })

        it('should successfully retrieve the seed attributes', async () => {
            req.params = {
                id: '123'
            }

            queryStub.onCall(0).resolves([{seedDbId:123}])
            queryStub.onCall(1).resolves([{attributes:[]}])
            queryStub.onCall(2).resolves([{count:1}])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equals(3, 'query must be called exactly thrice')
        })
    })

    describe('/POST/:id/delete-packages seeds', () => {
        const { post } = require('../../routes/v3/seeds/_id/delete-packages');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete packages given the seed ID',
            async () => {

        })
    })

    describe('/POST/:id/delete-seed-attributes seeds', () => {
        const { post } = require('../../routes/v3/seeds/_id/delete-seed-attributes');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete seed attributes given the seed ID',
            async () => {

        })
    })

    describe('/POST/:id/delete-seed-relations seeds', () => {
        const { post } = require('../../routes/v3/seeds/_id/delete-seed-relations');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete seed relations given the seed ID',
            async () => {

        })
    })

    describe('/POST/:id/entries-count seeds', () => {
        const { post } = require('../../routes/v3/seeds/_id/entries-count');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve the number of entries of the seed ' +
            'given its seed ID and simple filters for the entries.',
            async () => {

        })
    })
})