/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errorBuilder = require('../../../../helpers/error-builder')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Plot Data', () => {
    describe('/GET plot-data/:id/event-logs', () => {
        const { get } = require('../../../../routes/v3/plot-data/_id/event-logs')

        const sandbox = sinon.createSandbox()

        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate BadRequestError if plotDataDbId ' +
        'is invalid', async () => {
            req = {
                params: {
                    id: 'notAnInteger'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            errorBuilderStub.resolves('BadRequestError')

            await get(req, res, next)

            expect(queryStub.called).to.be.false('no query should have been called')
        })
    })
})
