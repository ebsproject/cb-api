/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const forwarded = require('forwarded-for')
const { sequelize } = require('../../config/sequelize')
const tokenHelper = require('../../helpers/auth/token')
const errorBuilder = require('../../helpers/error-builder')
const userValidator = require('../../helpers/person/validator')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

// Mock the transaction invoked after querying the database
const transactionMock = {
    finished: "",
    commit: function () {
        this.finished = 'commit'
    },
    rollback: function () {
        this.finished = 'rollback'
    },
    // Call this function in afterEach() to reset the value
    // of the property "finished"
    reset: function () {
        this.finished = ''
    }
}

describe('Experiment Blocks', () => {
    describe('PUT experiment-blocks/:id', () => {
        const { put } = require('../../routes/v3/experiment-blocks/_id/index')
        const sandbox = sinon.createSandbox()
        let forwardedStub
        let queryStub
        let tokenHelperStub
        let errorBuilderStub
        let userValidatorStub
        let transactionStub

        beforeEach(() => {
            // forwardedStub = sandbox.stub(forwarded, 'secure')
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
            transactionMock.reset()
        })

        it('should successfully update the remarks field of an existing ' +
            'experiment block record given an experiment block ID', async () => {
                req = {
                    headers: {},
                    params: {
                        id: '10'
                    },
                    body: {
                        remarks: 'updated remark'
                    },
                }
                tokenHelperStub.resolves(100)  // personDbId
                userValidatorStub.resolves(true)  // isAdmin
                queryStub.onCall(0).resolves([{}])  // experimentBlock data
                transactionStub.resolves([[{ id: 1 }]])

                let result = await put(req, res, next)

                // TODO: debug why it's called 4x instead of 2x
                // expect(queryStub.callCount).to.equal(2,
                //     '\n 1: experiment block must be retrieved from database' +
                //     '\n 2: update the experiment block record \n')
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
                expect(transactionStub.called).to.true('transaction must be called')
            })

        xit('should generate BadRequestError if block ID is not an integer ' +
            '(in string format)', async () => {
                //    TODO implement
            })
    })
})

