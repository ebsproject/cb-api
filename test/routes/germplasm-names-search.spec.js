/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const processQueryHelper = require('../../helpers/processQuery/index.js');
const errorBuilder = require('../../helpers/error-builder')
const { sequelize } = require('../../config/sequelize');
const { expect } = require('chai');

const { post } = require('../../routes/v3/germplasm-names-search/index.js');

const req = {
    headers: {
        host:'testHost'
    },
    query: {},
    paginate: {
        limit: 1,
        offset: 1
    }
}

const res = {
    send: (statusCode, data) => {
      return {
        statusCode,
        data
      }
    }
  };
  const next = {};

  describe('Germplasm Names', ()=> {
      describe('POST germplasm-names-search', ()=> {
        const sandbox = sinon.createSandbox();

        let getDistinctStringStub
        let getFieldValuesStringStrub
        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let getCountFinalSqlQueryStub
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            getFilterStub = sandbox.stub(processQueryHelper,'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper,'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper,'getFinalSqlQuery')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper,'getCountFinalSqlQuery')
            getDistinctStringStub = sandbox.stub(processQueryHelper,'getDistinctString')
            getFieldValuesStringStrub = sandbox.stub(processQueryHelper,'getFieldValuesString')
            queryStub = sandbox.stub(sequelize,'query')
            errorBuilderStub = sandbox.stub(errorBuilder,'getError')
            
        })

        afterEach(()=> {
            sandbox.restore()
        })

        it('should successfully retrieve germplasm name records', async ()=> {
            req.body = {
                "fields":"fields",
                "distinctOn":"field"
            }

            getFilterStub.resolves([])
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves([['record 1', 'record 2']])
            getCountFinalSqlQueryStub.resolves(2)
            queryStub.onSecondCall().resolves([{count: 2}])

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(queryStub.calledTwice).to.be.true('query is called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery called once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder is not called')
        })

        it('should successfully retrieve germplasm name records without distinctOn', 
            async ()=> {
                req.body = {
                    "fields":"fields"
                }

                getFilterStub.resolves([])
                getFinalSqlQueryStub.resolves('query')
                queryStub.onFirstCall().resolves([['record 1'],['record 2']])
                getCountFinalSqlQueryStub.resolves('count query')
                queryStub.onSecondCall().resolves([{count: 2}])

                const result = await post(req, res, next)

                expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
                expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
                expect(queryStub.calledTwice).to.be.true('query is called twice')
                expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery called once')
                expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder is not called')
            }
        )

        it('should successfully retrieve germplasm name records without fields attribute', 
            async ()=> {
                req.body = {
                    "nameValue":"equals name1|name2"
                }

                getFilterStub.resolves([])
                getFinalSqlQueryStub.resolves('query')
                queryStub.onFirstCall().resolves([['record 1'],['record 2']])
                getCountFinalSqlQueryStub.resolves('count query')
                queryStub.onSecondCall().resolves([{count: 2}])

                const result = await post(req, res, next)

                expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
                expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
                expect(queryStub.calledTwice).to.be.true('query is called twice')
                expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery called once')
                expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder is not called')
            }
        )

        it('should generate BadRequestError if has invalid order clause', async ()=> {
            req.body = {}

            getFilterStub.resolves(['invalid'])
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(getDistinctStringStub.calledOnce).to.be.false('getDistinctString not called')
            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(getFieldValuesStringStrub.calledOnce).to.be.false('getFieldValuesString not called')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',400022)
        })

        it('should generate BadRequestError if has invalid order clause', async ()=> {
            req.body = {}
            req.query.sort = 'testSort:ASC'

            getFilterStub.resolves([])
            getOrderStringStub.resolves(['invalid'])
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(getDistinctStringStub.calledOnce).to.be.false('getDistinctString not called')
            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(getFieldValuesStringStrub.calledOnce).to.be.false('getFieldValuesString not called')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString called once')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',400004)
        })

        it('should generate InternalError if query helper fails', async ()=> {
            req.body = {}
            req.query = {}

            getFilterStub.resolves([])
            getFinalSqlQueryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(queryStub.calledOnce).to.be.false('query is not called')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })

        it('should generate BadRequestError if main query fails', async ()=> {
            req.body = {}
            req.query = {}

            getFilterStub.resolves([])
            getFinalSqlQueryStub.resolves('query')
            queryStub.rejects()
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(queryStub.calledOnce).to.be.true('query is called once')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })

        it('should generate InternalError if count query helper fails', async ()=> {
            req.body = {}
            req.query = {}

            getFilterStub.resolves([])
            getFinalSqlQueryStub.resolves('query')
            queryStub.resolves([['record 1'],['record 2']])
            getCountFinalSqlQueryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(queryStub.calledOnce).to.be.true('query is not called')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('query is not called')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })

        it('should generate InternalError if count query fails', async ()=> {
            req.body = {}
            req.query = {}

            getFilterStub.resolves([])
            getFinalSqlQueryStub.resolves('query')
            queryStub.resolves([['record 1'],['record 2']])
            getCountFinalSqlQueryStub.resolves('count query')
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('query is not called')
            expect(queryStub.calledTwice).to.be.true('query is called twice')
            expect(errorBuilderStub.called).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub,'testHost',500004)
        })
      })
  })