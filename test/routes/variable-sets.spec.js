/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

describe('Variable Sets', () => {
    describe('/GET variable-sets', () => {
        const { get } = require('../../routes/v3/variable-sets/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible variable ' +
            'set records', async () => {

        })
    })

    describe('/GET/:id variable-sets', () => {
        const { get } = require('../../routes/v3/variable-sets/_id/index')
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of a specific and ' +
            'existing variable set record given a variable set ID', async () => {

        })
    })

    describe('/GET/:id/members variable-sets', () => {
        const { get } = require('../../routes/v3/variable-sets/_id/members')
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible member ' +
            'records of a specific and existing variable set record given a ' +
            'variable set ID', async () => {

        })
    })
})