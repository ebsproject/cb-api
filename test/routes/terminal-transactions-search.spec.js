/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const processQueryHelper = require('../../helpers/processQuery/index')
const errorBuilder = require('../../helpers/error-builder')
const { expect } = require('chai')
const logger = require('../../helpers/logger/index')
const userValidator = require('../../helpers/person/validator.js')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Terminal Transactions', () => {
    describe('/POST terminal-transactions-search', () => {
        const { post } = require('../../routes/v3/terminal-transactions-search/index');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let errorBuilderStub
        let logFailingQueryStub
        let userValidatorStub

        beforeEach(() => {
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            queryStub = sandbox.stub(sequelize, 'query')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQueryWoLimit')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve transaction records ' + 
            'with EMPTY request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: "abc:asc"
                }
            }

            getOrderStringStub.resolves('valid')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['record1, record2, record3'])
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
        })

        it('should successfully retrieve transaction records ' + 
            'with fields in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: "abc:asc"
                },
                body: {
                    fields: 'this.field AS thisField'
                }
            }

            getOrderStringStub.resolves('valid')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['transactionRecord1, transactionRecord2, transactionRecord3'])
            queryStub.onSecondCall().resolves([ { count: 100 } ])

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
        })
    
        it('should generate InternalError if the transaction count query fails',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves(['transaction1'])
            queryStub.onSecondCall().rejects()
            logFailingQueryStub.resolves('logFailingQuery')
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(logFailingQueryStub.calledOnce).to.be.true('logFailingQuery must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate BadRequestError if conditionString is invalid',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {},
                body: {
                    test: 'invalidArgument'
                }
            }

            userValidatorStub.resolves(true)
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })
    })
})