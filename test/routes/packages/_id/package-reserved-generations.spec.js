/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize')
const tokenHelper = require('../../../../helpers/auth/token')
const errorBuilder = require('../../../../helpers/error-builder')
const errors = require('restify-errors')

const { expect } = require('chai')


let req = {
    headers: {
        host:'testHost'
    }
}
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Packages', () => {
    describe('POST packages/:id/package-reserved-generations', () => {
        const { post } = require('../../../../routes/v3/packages/_id/package-reserved-generations');
        const sandbox = sinon.createSandbox()
        let badRequestErrorStub
        let notFoundErrorStub
        let errorBuilderStub
        let queryStub
        let getUserIdStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            badRequestErrorStub = sandbox.stub(errors, 'BadRequestError')
            notFoundErrorStub = sandbox.stub(errors, 'NotFoundError')
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            req = {
                headers: {
                    host:'testHost'
                },
            	params: {
                    id: '123',
                },
            }

            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate BadRequestError when packageDbId is invalid', async () => {
            req = {
                headers: {
                    host:'testHost'
                },
            	params: {
                    id: 'notAnInteger'
                },
            }

            //mock
            getUserIdStub.resolves(1)
            badRequestErrorStub.resolves('BadRequestError')
            
            //act
            await post(req, res, next)
            
            //assert
            expect(badRequestErrorStub.calledOnce).to.be.true('badRquestError is called once')
        })

        it('should generate NotFoundError when packageDbId does not exist in the database', async () => {
            req = {
                headers: {
                    host:'testHost'
                },
            	params: {
                    id: '0'
                },
                body: {},
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.onCall(0).resolves(null)
            notFoundErrorStub.resolves('NotFoundError')
            
            //act
            await post(req, res, next)
            
            //assert
            expect(notFoundErrorStub.calledOnce).to.be.true('notFoundError is called once')      
        })
    })
})