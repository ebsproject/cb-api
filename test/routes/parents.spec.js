/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let errorBuilder = require('../../helpers/error-builder')
let tokenHelper = require('../../helpers/auth/token')

let req = {
    headers: {
        host:'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Parents', () => {
    describe('/PUT/:id parents', () => {
        const { put } = require('../../routes/v3/parents/_id/index');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub
        let transactionStub
        
        // invalid testing parameters
        let invalidParamsArgs = [
            {
                attribute: 'parentType',
                value: 'invalidParentType'
            },
            {
                attribute: 'parentRole',
                value: 'invalidParentRole'
            }
        ]

        // valid testing parameters
        let validParamsArgs = [
            {
                attribute: 'parentType',
                value: 'Line'
            },
            {
                attribute: 'parentRole',
                value: 'female'
            },
            {
                attribute: 'description',
                value: 'This is a test'
            }
        ]

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError if parent ID is not an ' +
            'integer (in string format)', async () => {

            // ARRANGE
            req.params = {
                id: 'notAnInteger'
            }

            // MOCK
            errorBuilderStub.resolves('BadRequestError')
            
            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly exactly once')
            expect(queryStub.called).to.be.false('query must not be called')

        })

        it('should throw a BadRequestError if user is not found', async () => {

            // ARRANGE
            req.params = {
                id: '10'
            }

            // MOCK
            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(tokenHelperStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)

        })

        it('should throw an InternalError if error occurs when querying ' +
            'parent record fails', async () => {
            
            // ARRANGE
            req.params = {
                id: "10"
            }

            // MOCK
            tokenHelperStub.resolves(1)
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(tokenHelperStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw an InternalError if error occurs when updating ' +
            'parent record fails', async () => {
            
            // ARRANGE
            req.body = {
                parentType : 'Line'
            }

            // MOCK
            tokenHelperStub.resolves(1)
            queryStub.onFirstCall().resolves([
                {
                    parentType: "Line",
                    parentRole: "male"
                }
            ])

            errorBuilderStub.resolves('InternalError')
            transactionStub.resolves({
                rollback: sinon.stub(),
            })

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(tokenHelperStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500003)
        })

        it('should throw a NotFoundError if parent ID does not exist', async () => {
            // ARRANGE
            req.params = {
                id: "10000"
            }

            // MOCK
            tokenHelperStub.resolves(1)
            queryStub.resolves([])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(tokenHelperStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should throw a BadRequestError when the request body is not set', async () => {
            req.body = null

            // MOCK
            tokenHelperStub.resolves(1)
            queryStub.onFirstCall().resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(tokenHelperStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        invalidParamsArgs.forEach((args) => {
            it(`should throw a BadRequestError if ${args.attribute} is invalid`, async () => {
                
                // ARRANGE
                req.body = {
                    [args.attribute] : args.value
                }

                // MOCK
                tokenHelperStub.resolves(1)
                queryStub.onFirstCall().resolves([
                    {
                        parentType: "Line",
                        parentRole: "male"
                    }
                ])

                queryStub.onCall(1).resolves([{count:0}])
                errorBuilderStub.resolves('BadRequestError')

                // ACT
                const result = await put(req, res, next)

                // ASSERT
                expect(tokenHelperStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)

            })
        })

        validParamsArgs.forEach((args) => {
            it(`should update the record if ${args.attribute} is valid`, async () => {
                
                // ARRANGE
                req.body = {
                    [args.attribute] : args.value
                }

                // MOCK
                tokenHelperStub.resolves(1)
                queryStub.onFirstCall().resolves([
                    {
                        parentType: "Line",
                        parentRole: "male"
                    }
                ])

                queryStub.onCall(1).resolves([{count:1}])
                queryStub.onCall(2).resolves([{id:1}])
                transactionStub.resolves({
                    commit: sinon.stub(),
                })
                errorBuilderStub.resolves('InternalError')

                // ACT
                const result = await put(req, res, next)

                // ASSERT
                expect(tokenHelperStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')

            })
        })
        
    })
})
