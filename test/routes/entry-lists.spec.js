/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const tokenHelper = require('../../helpers/auth/token')
const errorBuilder = require('../../helpers/error-builder')
const userValidator = require('../../helpers/person/validator.js')
const logger = require('../../helpers/logger')
const errors = require('restify-errors')

const { expect } = require('chai')


let req = {
    headers: {
        host:'testHost'
    }
}
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Entry Lists', () => {
    describe('/POST entry-lists', () => {
        const { post } = require('../../routes/v3/entry-lists/index');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let queryStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            // MOCK
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)

        })

        it('should throw a BadRequestError(400009) when the request body is not set', async () => {
            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        it('should throw a BadRequestError(400009) when the records array is empty', async () => {
            // ARRANGE
            req.body = {}

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        it('should throw an InternalError(500001) if other errors were encountered', async () => {
            // ARRANGE
            req.body = {
                records: {}
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should return a BadRequestError if the required parameters are missing', async () => {
            // ARRANGE
            req.body = {
                records: [{}]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should create a new record if all parameters are valid', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        "entryListCode": "CROSS_LIST_08",
                        "entryListName": "Test",
                        "entryListStatus": "draft",
                        "entryListType": "entry list",
                        "description": "Testdesc",
                        "experimentYear": "2022",
                        "experimentDbId": "17361",
                        "seasonDbId": "15",
                        "cropDbId": "1",
                        "programDbId": "101",
                        "siteDbId": "2632"
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            // Get validation count
            queryStub.onFirstCall().resolves([{count: 7}])
            // Generate entry list code
            queryStub.onSecondCall().resolves([{entrylistcode: 'ENTLIST00000001'}])
            // Insert entry list
            queryStub.onThirdCall().resolves([{id: 262}])

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')
        })

        it('should return a BadRequestError(400025) if the experiment id is invalid', async () => {
            // ARRANGE
            req.body = {
                records: [{
                    entryListCode: "CROSS_LIST_08",
                    entryListName: "Test",
                    entryListStatus: "created",
                    entryListType: "cross list",
                    description: "Testdesc",
                    experimentYear: "2022",
                    experimentDbId: "invalid",
                    seasonDbId: "15",
                    cropDbId: "1",
                    programDbId: "101",
                    siteDbId: "2632"
                }]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400025)
        })

        it('should return a BadRequestError if entryListType is invalid', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        entryListCode: "CROSS_LIST_08",
                        entryListName: "Test",
                        entryListStatus: "created",
                        entryListType: "invalid",
                        description: "Testdesc",
                        experimentYear: "2022",
                        experimentDbId: "236",
                        seasonDbId: "15",
                        cropDbId: "1",
                        programDbId: "101",
                        siteDbId: "2632"
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError(400129) if the experiment year not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [{
                    entryListCode: "CROSS_LIST_08",
                    entryListName: "Test",
                    entryListStatus: "created",
                    entryListType: "cross list",
                    description: "Testdesc",
                    experimentYear: "invalid",
                    experimentDbId: "236",
                    seasonDbId: "15",
                    cropDbId: "1",
                    programDbId: "101",
                    siteDbId: "2632"
                }]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400129)
        })

        it('should return a BadRequestError(400144) if the experiment year has invalid format', async () => {
            // ARRANGE
            req.body = {
                records: [{
                    entryListCode: "CROSS_LIST_08",
                    entryListName: "Test",
                    entryListStatus: "created",
                    entryListType: "cross list",
                    description: "Testdesc",
                    experimentYear: "090893",
                    experimentDbId: "236",
                    seasonDbId: "15",
                    cropDbId: "1",
                    programDbId: "101",
                    siteDbId: "2632"
                }]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400144)
        })

        it('should return a BadRequestError(400063) if the season id is invalid', async () => {
            // ARRANGE
            req.body = {
                records: [{
                    entryListCode: "CROSS_LIST_08",
                    entryListName: "Test",
                    entryListStatus: "created",
                    entryListType: "cross list",
                    description: "Testdesc",
                    experimentYear: "2022",
                    experimentDbId: "263",
                    seasonDbId: "invalid",
                    cropDbId: "1",
                    programDbId: "101",
                    siteDbId: "2632"
                }]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400063)
        })

        it('should return a BadRequestError(400064) if the site id is invalid', async () => {
            // ARRANGE
            req.body = {
                records: [{
                    entryListCode: "CROSS_LIST_08",
                    entryListName: "Test",
                    entryListStatus: "created",
                    entryListType: "cross list",
                    description: "Testdesc",
                    experimentYear: "2022",
                    experimentDbId: "263",
                    seasonDbId: "15",
                    cropDbId: "1",
                    programDbId: "101",
                    siteDbId: "invalid"
                }]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400064)
        })

        it('should return a BadRequestError if the crop id is not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        entryListCode: "CROSS_LIST_08",
                        entryListName: "Test",
                        entryListStatus: "created",
                        entryListType: "cross list",
                        description: "Testdesc",
                        experimentYear: "2022",
                        experimentDbId: "236",
                        seasonDbId: "15",
                        cropDbId: "invalid",
                        programDbId: "101",
                        siteDbId: "2632"
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError(400125) if the program id is not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [{
                    entryListCode: "CROSS_LIST_08",
                    entryListName: "Test",
                    entryListStatus: "created",
                    entryListType: "cross list",
                    description: "Testdesc",
                    experimentYear: "2022",
                    experimentDbId: "263",
                    seasonDbId: "15",
                    cropDbId: "1",
                    programDbId: "invalid",
                    siteDbId: "2632"
                }]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400125)
        })

        it('should return a BadRequestError(400015) if the user provided an input that was not validated', async () => {
            // ARRANGE
            req.body = {
                records: [{
                    entryListCode: "CROSS_LIST_08",
                    entryListName: "Test",
                    entryListStatus: "created",
                    entryListType: "cross list",
                    description: "Testdesc",
                    experimentYear: "2022",
                    experimentDbId: "263",
                    seasonDbId: "15",
                    cropDbId: "1",
                    programDbId: "101",
                    siteDbId: "2632"
                }]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            queryStub.onFirstCall().resolves([{count: 0}])

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('bad request error should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })
    })

    describe('POST entry-lists/:id/cross-count-generations', () => {
        const { post } = require('../../routes/v3/entry-lists/_id/cross-count-generations')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub
        let getUserIdStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate BadRequestError if entryListDbId is invalid', async () => {
            req = {
                headers: {
                    host:'testHost'
                },
            	params: {
                    id: 'notAnInteger'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }
            //mock
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            await post(req, res, next)
            
            //assert
            expect(queryStub.called).to.be.false('no query should have been called')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400021)
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            req = {
                headers: {
                    host:'testHost'
                },
            	params: {
                    id: '5250',
                }
            }
            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })
    })

    describe('POST entry-lists/:id/cross-data-table-search', () => {
        const { post } = require('../../routes/v3/entry-lists/_id/cross-data-table-search');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate BadRequestError if entryListDbId is invalid', async () => {
            req = {
                headers: {
                    host:'testHost'
                },
            	params: {
                    id: 'notAnInteger'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }
            errorBuilderStub.resolves('BadRequestError')
            await post(req, res, next)
            expect(queryStub.called).to.be.false('no query should have been called')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400021)
        })
    })

    describe('POST entry-lists/:id/parents-search', () => {
        const { post } = require('../../routes/v3/entry-lists/_id/parents-search');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate BadRequestError if entryListDbId is invalid', async () => {
            req = {
                headers: {
                    host:'testHost'
                },
            	params: {
                    id: 'notAnInteger'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }
            errorBuilderStub.resolves('BadRequestError')
            await post(req, res, next)
            expect(queryStub.called).to.be.false('no query should have been called')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400021)
        })

        it('should generate an InternalError if retrieval of entry list information fails', async ()=>{
            req = {
                headers: {
                    host:'testHost'
                },
                params: {
                    id:'5250',
                },
                query:{
                    sort:''
                },
                paginate: {
                    limit:100,
                    offset:0
                }
            }
            queryStub.onFirstCall().resolves({length : 0})
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')
            await post(req,res,next)
            expect(queryStub.calledTwice).to.be.true('query is called twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is called twice')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })

    describe('/PUT/:id entry-lists', () => {
        const { put } = require('../../routes/v3/entry-lists/_id/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let queryStub
        let userValidatorStub
        let logFailingQueryStub
        let logMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            queryStub = sandbox.stub(sequelize, 'query')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(400021) when entryListDbId is not an integer', async () => {
            // MOCK
            req.params = {id: 'notAnInteger'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.called).to.be.false('query must not be called')

        })

        it('should throw a BadRequestError(404006) when entry list is not found', async () => {
            // MOCK
            req.params = {id: '0'}
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('NotFoundError')
            userValidatorStub.resolves(true)
            queryStub.resolves([])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')

        })

        it('should throw a UnauthorizedError(401025) if user does not have a role', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(2)
            errorBuilderStub.resolves('UnauthorizedError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            queryStub.onSecondCall().resolves([{}])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        it('should throw a UnauthorizedError(401026) if user is not a program team member and is not a producer', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(2)
            errorBuilderStub.resolves('UnauthorizedError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            queryStub.onSecondCall().resolves([{'id': 1, 'role': 0}])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        it('should throw a BadRequestError(400009) if req.body is empty', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
        })

        it('should throw a BadRequestError(400129) if experimentYear is not an integer', async () => {
            // MOCK
            req.params = {id: '1'}
            req.body = {
                entryListName : 'new entry list name',
                entryListStatus : 'new status',
                description : 'new description',
                experimentYear : 'a2024',
                seasonDbId : '1',
                siteDbId : '1'
            }
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
        })

        it('should throw a BadRequestError(400144) if experimentYear is not in correct format', async () => {
            // MOCK
            req.params = {id: '1'}
            req.body = {
                entryListName : 'new entry list name',
                entryListStatus : 'new status',
                description : 'new description',
                experimentYear : '123',
                seasonDbId : '1',
                siteDbId : '1'
            }
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
        })

        it('should throw a BadRequestError(400063) if seasonDbId is not an integer', async () => {
            // MOCK
            req.params = {id: '1'}
            req.body = {
                entryListName : 'new entry list name',
                entryListStatus : 'new status',
                description : 'new description',
                experimentYear : '2025',
                seasonDbId : 'a',
                siteDbId : '1'
            }
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
        })

        it('should throw a BadRequestError(400064) if siteDbId is not an integer', async () => {
            // MOCK
            req.params = {id: '1'}
            req.body = {
                entryListName : 'new entry list name',
                entryListStatus : 'new status',
                description : 'new description',
                experimentYear : '2025',
                seasonDbId : '2',
                siteDbId : 'a'
            }
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
        })

        it('should throw a BadRequestError(400015) if the user provided an input that was not validated', async () => {
            // MOCK
            req.params = {id: '1'}
            req.body = {
                entryListName : 'new entry list name',
                entryListStatus : 'new status',
                description : 'new description',
                experimentYear : '2025',
                seasonDbId : '2',
                siteDbId : '2'
            }
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            queryStub.onSecondCall().resolves([{count:0}])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        it('should throw a InternalError(500003) if other errors were encountered', async () => {
            // MOCK
            req.params = {id: '1'}
            req.body = {
                entryListName : 'new entry list name',
                entryListStatus : 'new status',
                description : 'new description',
                experimentYear : '2025',
                seasonDbId : '2',
                siteDbId : '2'
            }
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            queryStub.onSecondCall().throws(new Error)

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        it('should update a specific and existing entry list if all parameters are valid', async () => {
            // MOCK
            req.params = {id: '1'}
            req.body = {
                entryListName : 'new entry list name',
                entryListStatus : 'new status',
                description : 'new description',
                experimentYear : '2025',
                seasonDbId : '2',
                siteDbId : '2'
            }
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            queryStub.onSecondCall().resolves([{count:3}])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must not be called')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })
    })

    describe('/DELETE/:id entry-lists', () => {
        const { delete:del } = require('../../routes/v3/entry-lists/_id/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let queryStub
        let userValidatorStub
        let logFailingQueryStub
        let logMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            queryStub = sandbox.stub(sequelize, 'query')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(400021) when entryListDbId is not an integer', async () => {
            // MOCK
            req.params = {id: 'notAnInteger'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.called).to.be.false('query must not be called')

        })

        it('should throw a BadRequestError(404006) when entry list is not found', async () => {
            // MOCK
            req.params = {id: '0'}
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('NotFoundError')
            userValidatorStub.resolves(true)
            queryStub.resolves([])

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')

        })

        it('should throw a UnauthorizedError(401025) if user does not have a role', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(2)
            errorBuilderStub.resolves('UnauthorizedError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            queryStub.onSecondCall().resolves([{}])

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        it('should throw a UnauthorizedError(401026) if user is not a program team member and is not a producer', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(2)
            errorBuilderStub.resolves('UnauthorizedError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            queryStub.onSecondCall().resolves([{'id': 1, 'role': 0}])

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        it('should throw a InternalError(500003) if other errors were encountered', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            transactionStub.onFirstCall().throws(new Error)

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
            expect(transactionStub.callCount).to.equal(1,'transaction must be called exactly once')
        })

        it('should delete a specific and existing entry list record given an entrty list ID', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                creatorDbId : 1,
                entryListDbId : 1,
                experimentDbId : 1,
                entryListName : 'entry list name',
                entryListStatus : 'status',
                description : 'description',
                experimentYear : 2024,
                seasonDbId : 1,
                siteDbId : 1
            }])
            transactionStub.onFirstCall().resolves()

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must not be called')
            expect(queryStub.callCount).to.equal(1,'query must be called exactly once')
            expect(transactionStub.callCount).to.equal(1,'transaction must be called exactly once')
        })
    })
})