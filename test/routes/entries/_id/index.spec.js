/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize.js')
const errorBuilder = require('../../../../helpers/error-builder.js')
const tokenHelper = require('../../../../helpers/auth/token.js')
const logger = require('../../../../helpers/logger/index.js')
const validator = require('validator')
const userValidator = require('../../../../helpers/person/validator.js')
const { expect } = require('chai')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Entries', () => {
    describe('PUT entries/:id', () => {
        const { put } = require('../../../../routes/v3/entries/_id/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let logMessageStub
        let logFailingQueryStub
        let isIntStub
        let isAdminStub

        //stub declaration based on existing functions within put
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            isIntStub = sandbox.stub(validator, 'isInt')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (400020) when entryDbId is not an integer', async () => {
            // Arrange
            req['params'] = { 'id': 'notInt' }

            isIntStub.returns(false)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await put(req, res, next)
            
            // Assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400020)
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await put(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError (400009) when the request body is not set', async () => {
            // Arrange
            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await put(req, res, next)

            // Assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw an InternalError (500004) when failing to retrieve ' +
            'entry record', async () => {
            // Arrange
            req['params'] = { 'id': '123' }
            req['body'] = { 'entryStatus': 'test' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.rejects()
            logFailingQueryStub.resolves('query error')
            errorBuilderStub.resolves('InternalError')

            // Act
            await put(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(logFailingQueryStub.calledOnce).to.be.true('logFailingQuery must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw a NotFoundError (404005) when retrieving entry record ' +
            'from database returns empty', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([])
            errorBuilderStub.resolves('NotFoundError')

            // Act
            await put(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404005)
        })

        it('should successfully update entryStatus of an entry', async () => {
            // Arrange    
            req['params'] = { 'id': '1' }
            req.body = { 'entryStatus': 'test' }

            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.onCall(0).resolves([{ 'entryStatus': 'check' }])


            queryStub.onCall(1).resolves([{ 'count': 2 }])
            transactionStub.resolves([{}])

            // Act
            await put(req, res, next)

            // Assert
            expect(transactionStub.calledOnce).to.be.true('transaction should be called exactly once')
        })

        it('should throw an InternalError (500003) if other errors were encountered', async () => {
            // Arrange
            req['params'] = { 'id': '1' }
            req.body = { 'entryListDbId': 'invalid' }

            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{ 'recordAttribute': 'value' }])
            isIntStub.onCall(1).throws(new Error)
            logMessageStub.resolves('err')
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await put(req, res, next)

            // Assert
            expect(logMessageStub.calledOnce).to.be.true('logMessage should be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500003)
        })
    })

    describe('DELETE entries/:id', () => {
        const { delete: del } = require('../../../../routes/v3/entries/_id/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let logMessageStub
        let logFailingQueryStub
        let isIntStub
        let isAdminStub

        //stub declaration based on existing functions within put
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            isIntStub = sandbox.stub(validator, 'isInt')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (400020) when entryDbId is not an integer', async () => {
            // Arrange
            req['params'] = { 'id': 'notInt' }

            isIntStub.returns(false)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await del(req, res, next)
            
            // Assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400020)
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await del(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw an InternalError (500004) when failing to retrieve ' +
            'entry record', async () => {
            // Arrange
            req['params'] = { 'id': '123' }
            req['body'] = { 'entryStatus': 'test' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.rejects()
            logFailingQueryStub.resolves('query error')
            errorBuilderStub.resolves('InternalError')

            // Act
            await del(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(logFailingQueryStub.calledOnce).to.be.true('logFailingQuery must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw a NotFoundError (404005) when retrieving entry record ' +
            'from database returns empty', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([])
            errorBuilderStub.resolves('NotFoundError')

            // Act
            await del(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404005)
        })

        it('should successfully delete an entry', async () => {
            // Arrange
            req['params'] = { 'id': '1' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{'entryListDbId': 1}])
            transactionStub.resolves([{'success': true}])

            // Act
            await del(req, res, next)

            // Assert
            expect(transactionStub.calledOnce).to.be.true('transaction should be called once')
        })

        it('should throw an InternalError (500002) if other errors were encountered', async () => {
            // Arrange
            req['params'] = { 'id': '1' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{'creatorDbId': 100}])
            transactionStub.throws(new Error('dummy error'))
            logMessageStub.resolves('err')
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await del(req, res, next)

            // Assert
            expect(logMessageStub.calledOnce).to.be.true('logMessage should be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })
    })
})
