/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize.js')
const errorBuilder = require('../../../helpers/error-builder.js')
const tokenHelper = require('../../../helpers/auth/token.js')
const logger = require('../../../helpers/logger/index.js')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Entries', () => {
    describe('/POST entries', () => {
        const { post } = require('../../../routes/v3/entries/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let logMessageStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            logMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError (400009) when the request body is not set', async () => {
            // Arrange
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw a BadRequestError (400005) when the records array is empty', async () => {
            // Arrange
            req.body = {}

            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        it('should successfully insert a valid record ' +
            'with only required parameters', async () => {
            // Arrange
            req.body = {
                records: [
                    {
                        "entryName": "string",
                        "entryType": "test, check, control, filler, parent, or cross",
                        "entryStatus": "active or inactive",
                        "entryListDbId": '1',
                        "germplasmDbId": '1',
                    }
                  ]
            }

            getUserIdStub.resolves(1)
            queryStub.onCall(0).resolves([ {'count': 4 }])
            queryStub.onCall(1).resolves([ {'entryCount': 0 }])
            transactionStub.resolves([ [ { 'id': 1 } ], 1 ])

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should throw an InternalError (500001) if other errors were encountered', async () => {
            // Arrange
            req.body = {
                records: [{
                    "entryName": "string",
                    "entryType": "test, check, control, filler, parent, or cross",
                    "entryStatus": "active or inactive",
                    "entryListDbId": '1',
                    // missing germplasmDbId
                }]
            }

            getUserIdStub.resolves(1)
            errorBuilderStub.onCall(0).rejects()
            logMessageStub.resolves('error')
            errorBuilderStub.onCall(1).resolves('InternalError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(logMessageStub.calledOnce).to.be.true('logMessage must be called exactly once')
            expect(errorBuilderStub.calledTwice).to.be.true('errorBuilder must be called exactly twice')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })
})
