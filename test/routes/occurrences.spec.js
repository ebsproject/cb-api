/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')
const processQueryHelper = require('../../helpers/processQuery/index')
const occurrenceHelper = require('../../helpers/occurrence/index')
const patternHelper = require('../../helpers/patternGenerator/index.js')
const userValidator = require('../../helpers/person/validator.js')
const logger = require('../../helpers/logger')
const errors = require('restify-errors')

let req = {
    headers: {
        host:'testHost'
    }
}
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Occurrences', () => {
    describe('/POST occurrences', () => {
        const { post } = require('../../routes/v3/occurrences/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let patternGetCounterStub
        let patternGetEntityValueStub
        let patternGetPaddedNumberStub
        let patternGetPatternStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let queryStub
        let logFailingQueryStub
        let logMessageStub
        let occurrenceHelperStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            patternGetCounterStub = sandbox.stub(patternHelper, 'getCounter')
            patternGetEntityValueStub = sandbox.stub(patternHelper, 'getEntityValue')
            patternGetPaddedNumberStub = sandbox.stub(patternHelper, 'getPaddedNumber')
            patternGetPatternStub = sandbox.stub(patternHelper, 'getPattern')
            occurrenceHelperStub = sandbox.stub(occurrenceHelper, 'generateCode')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            queryStub = sandbox.stub(sequelize, 'query')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            // MOCK
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)

        })

        it('should throw a BadRequestError(400009) when the request body is not set', async () => {
            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        it('should throw a BadRequestError(400005) when the records array is empty', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                records: []
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)

        })

        it('should return a BadRequestError if the required parameters are missing', async () => {
            // ARRANGE
            req.body = {
                records: [{}]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError if experimentDbId is not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        occurrenceName : 'occurrence name',
                        occurrenceStatus : 'status',
                        experimentDbId : '',
                        geospatialObjectDbId : '1',
                        siteDbId : '1',
                        fieldDbId : '1',
                        repCount : '1',
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError if geospatialObjectId is not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        occurrenceName : 'occurrence name',
                        occurrenceStatus : 'status',
                        experimentDbId : '1',
                        geospatialObjectDbId : 'a',
                        siteDbId : '1',
                        fieldDbId : '1',
                        repCount : '1',
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError if siteDbId is not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        occurrenceName : 'occurrence name',
                        occurrenceStatus : 'status',
                        experimentDbId : '1',
                        geospatialObjectDbId : '1',
                        siteDbId : 'a',
                        fieldDbId : '1',
                        repCount : '1',
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError if fieldDbId is not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        occurrenceName : 'occurrence name',
                        occurrenceStatus : 'status',
                        experimentDbId : '1',
                        geospatialObjectDbId : '1',
                        siteDbId : '1',
                        fieldDbId : 'a',
                        repCount : '1',
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError if repCount is not an integer', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        occurrenceName : 'occurrence name',
                        occurrenceStatus : 'status',
                        experimentDbId : '1',
                        geospatialObjectDbId : '1',
                        siteDbId : '1',
                        fieldDbId : '1',
                        repCount : 'a',
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should throw a BadRequestError(400015) if the user provided an input that was not validated', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        occurrenceName : 'occurrence name',
                        occurrenceStatus : 'status',
                        experimentDbId : '1',
                        geospatialObjectDbId : '1',
                        siteDbId : '1',
                        fieldDbId : '1',
                        repCount : '1',
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            // Get validation count
            queryStub.onFirstCall().resolves([{count: 0}])

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should throw an InternalError(500001) if other errors were encountered', async () => {
            // ARRANGE
            req.body = {
                records: {}
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should create a new record if all parameters are valid', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        occurrenceStatus : 'status',
                        experimentDbId : '1',
                        geospatialObjectDbId : '1',
                        siteDbId : '1',
                        fieldDbId : '1',
                        repCount : '1',
                    }
                ]
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            patternGetCounterStub.resolves('')
            patternGetEntityValueStub.resolves({experiment_code: ''})
            patternGetPaddedNumberStub.resolves('')
            patternGetPatternStub.resolves('')
            occurrenceHelperStub.resolves('occurrence name')
            // Get validation count
            queryStub.onFirstCall().resolves([{count: 5}])
            // Insert occurrence
            queryStub.onSecondCall().resolves([{id: 1}])

            // ACT
            const result = await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')
        })
    })

    describe('/PUT occurrences/:id', () => {
        const { put } = require('../../routes/v3/occurrences/_id/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let userValidatorStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let errorNotFoundStub
        let queryStub
        let logFailingQueryStub
        let logMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            errorNotFoundStub = sandbox.stub(errors, 'NotFoundError')
            queryStub = sandbox.stub(sequelize, 'query')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError when occurrenceDbId is not an integer', async () => {
            // MOCK
            req.params = {id: 'notAnInteger'}
            getUserIdStub.resolves(null)
            errorStub.resolves('error')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)

        })

        it('should throw a NotFoundError if occurrence is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorNotFoundStub.resolves('NotFoundError')
            userValidatorStub.resolves(true)
            queryStub.resolves([])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(errorNotFoundStub.callCount).to.equal(1,'errorBuilder must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')

        })

        it('should throw a BadRequestError if req.body is empty', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = null

            // MOCK
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1,'errorBuilder must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should return a BadRequestError if geospatialObjectId is not an integer', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'status',
                experimentDbId : '1',
                geospatialObjectDbId : 'a',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.callCount).to.equal(1,'bad request error should be called once')
        })

        it('should return a BadRequestError if siteDbId is not an integer', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'status',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : 'a',
                fieldDbId : '1',
                repCount : '1'
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.callCount).to.equal(1,'bad request error should be called once')
        })

        it('should return a BadRequestError if fieldDbId is not an integer', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'status',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : 'a',
                repCount : '1'
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.callCount).to.equal(1,'bad request error should be called once')
        })

        it('should return a BadRequestError if repCount is not an integer', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'status',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : 'a'
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.callCount).to.equal(1,'bad request error should be called once')
        })

        it('should throw a BadRequestError(400015) if the user provided an input that was not validated', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'status',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.onFirstCall().resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])
            // Get validation count
            queryStub.onSecondCall().resolves([{count: 0}])

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.callCount).to.equal(1,'bad request error should be called once')
        })

        xit('should throw an InternalError(500003) if other errors were encountered', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'new occurrence name',
                occurrenceStatus : 'new status',
                experimentDbId : '2',
                geospatialObjectDbId : '2',
                siteDbId : '2',
                fieldDbId : '2',
                repCount : '2'
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.onFirstCall().resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])
            // Get validation count
            queryStub.onSecondCall().resolves([{count: 4}])
            transactionStub.onFirstCall().throws(new Error)

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.callCount).to.equal(1,'errorBuilder must be called exactly once')
        })

        it('should update a new record if all parameters are valid', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'new occurrence name',
                occurrenceStatus : 'new status',
                experimentDbId : '2',
                geospatialObjectDbId : '2',
                siteDbId : '2',
                fieldDbId : '2',
                repCount : '2',
                description : 'new description',
                remarks : 'new remarks',
                accessData : {}
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.onFirstCall().resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])
            // Get validation count
            queryStub.onSecondCall().resolves([{count: 4}])
            transactionStub.onFirstCall().resolves()

            // ACT
            const result = await put(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')
        })
    })

    describe('/DELETE occurrences/:id', () => {
        const { delete:del } = require('../../routes/v3/occurrences/_id/index')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let userValidatorStub
        let errorBuilderStub
        let transactionStub
        let errorStub
        let errorNotFoundStub
        let queryStub
        let logFailingQueryStub
        let logMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            errorNotFoundStub = sandbox.stub(errors, 'NotFoundError')
            queryStub = sandbox.stub(sequelize, 'query')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub.resolves()
            logMessageStub.resolves()
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError when occurrenceDbId is not an integer', async () => {
            // MOCK
            req.params = {id: 'notAnInteger'}
            getUserIdStub.resolves(null)
            errorStub.resolves('error')

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)

        })

        it('should throw a NotFoundError if occurrence is not found', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(1)
            errorNotFoundStub.resolves('NotFoundError')
            userValidatorStub.resolves(true)
            queryStub.resolves([])

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorNotFoundStub.callCount).to.equal(1,'errorBuilder must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')

        })

        it('should throw a UnauthorizedError(401025) if user does not have a role', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(2)
            errorBuilderStub.resolves('UnauthorizedError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])
            queryStub.onSecondCall().resolves([{}])

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        it('should throw a UnauthorizedError(401026) if user is not a program team member and is not a producer', async () => {
            // MOCK
            req.params = {id: '1'}
            getUserIdStub.resolves(2)
            errorBuilderStub.resolves('UnauthorizedError')
            userValidatorStub.resolves(false)
            // Get entry list record
            queryStub.onFirstCall().resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])
            queryStub.onSecondCall().resolves([{'id': 1, 'role': 0}])

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
        })

        xit('should throw an InternalError(500003) if other errors were encountered', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'new occurrence name',
                occurrenceStatus : 'new status',
                experimentDbId : '2',
                geospatialObjectDbId : '2',
                siteDbId : '2',
                fieldDbId : '2',
                repCount : '2'
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.onFirstCall().resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])
            // Get validation count
            queryStub.onSecondCall().resolves([{count: 4}])
            transactionStub.onFirstCall().throws(new Error)

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.callCount).to.equal(1,'errorBuilder must be called exactly once')
        })

        it('should delete a new record if all parameters are valid', async () => {
            // ARRANGE
            req.params = {id: '1'}
            req.body = {
                occurrenceName : 'new occurrence name',
                occurrenceStatus : 'new status',
                experimentDbId : '2',
                geospatialObjectDbId : '2',
                siteDbId : '2',
                fieldDbId : '2',
                repCount : '2',
                description : 'new description',
                remarks : 'new remarks',
                accessData : {}
            }

            // MOCK
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            userValidatorStub.resolves(true)
            queryStub.onFirstCall().resolves([{
                occurrenceName : 'occurrence name',
                occurrenceStatus : 'occurrence status',
                description : 'description',
                remarks : 'remarks',
                experimentDbId : '1',
                geospatialObjectDbId : '1',
                siteDbId : '1',
                fieldDbId : '1',
                repCount : '1'
            }])
            // Get validation count
            queryStub.onSecondCall().resolves([{count: 4}])
            transactionStub.onFirstCall().resolves()

            // ACT
            const result = await del(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not be called')
        })
    })

    describe('/POST occurrences/:id/harvest-cross-data-search', () => {
        const { post } = require('../../routes/v3/occurrences/_id/harvest-cross-data-search');
        const sandbox = sinon.createSandbox()
        let queryStub
        let tokenHelperStub
        let errorBuilderStub
        let getFilterStub
        let getOrderStringStub
        let occurrenceHelperStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            occurrenceHelperStub = sandbox.stub(occurrenceHelper, 'getHMSupportedVariables')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve all cross records', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'field1|field2'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.resolves('valid')
            occurrenceHelperStub.resolves({
                committedDataSelect: ['a','b','c'],
                committedData: ['a','b','c'],
                committedDataAs: [],
                terminalDataSelect: ['a','b','c'],
                terminalData: ['a','b','c'],
                terminalDataAs: [],
                variableIds: '1,2,3'
            })
            queryStub.onCall(1).resolves([
                {
                    harvestDateVarId:1,
                    harvestMethodVarId:2,
                    crossingDateVarId:3,
                    noOfSeedVarId:4,
                    noOfBagVarId:5,
                    noOfPlantVarId:6,
                    specificPlantNoVarId:7,
                    noOfPanicleVarId:8,
                    noOfEarVarId:9
                }
            ])
            queryStub.onCall(2).resolves(['cross1','cross2'])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly 2 times')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly once')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError '
            + 'if the creatorId is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            let result = await post(req, res, next)

            expect(queryStub.called).to.be.false('query must NOT be called')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should terminate when the occurrence id '
            + 'is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: 'abc'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(123)

            let result = await post(req, res, next)

            expect(queryStub.called).to.be.false('query must NOT be called')
        })

        it('should generate InternalError when'
            + 'the occurrence query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully retrieve all cross records '
            + 'when fields, sort, and distinctOn are specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'field1|field2',
                    distinctOn: 'distinctField',
                    sort: {
                        attribute: 'crossDbId',
                        sortValue: 'sortVal'
                    }
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.resolves('valid')
            queryStub.onCall(1).resolves([
                {
                    harvestDateVarId:1,
                    harvestMethodVarId:2,
                    crossingDateVarId:3,
                    noOfSeedVarId:4,
                    noOfBagVarId:5,
                    noOfPlantVarId:6,
                    specificPlantNoVarId:7,
                    noOfPanicleVarId:8,
                    noOfEarVarId:9
                }
            ])
            occurrenceHelperStub.resolves({
                committedDataSelect: ['a','b','c'],
                committedData: ['a','b','c'],
                committedDataAs: [],
                terminalDataSelect: ['a','b','c'],
                terminalData: ['a','b','c'],
                terminalDataAs: [],
                variableIds: '1,2,3'
            })
            queryStub.onCall(2).resolves(['cross1','cross2'])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly 2 times')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError '
            + 'when condition string is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'field1|field2',
                    distinctOn: 'distinctField',
                    sort: {
                        attribute: 'crossDbId',
                        sortValue: 'sortVal'
                    }
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate BadRequestError '
            + 'when order string is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "sortParam1"
                },
                body: {
                    sort: {
                        attribute:"crossDbId",
                        sortValue:"val"
                    }
                },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.resolves('valid')
            queryStub.onCall(1).resolves([
                {
                    harvestDateVarId:1,
                    harvestMethodVarId:2,
                    crossingDateVarId:3,
                    noOfSeedVarId:4,
                    noOfBagVarId:5,
                    noOfPlantVarId:6,
                    specificPlantNoVarId:7,
                    noOfPanicleVarId:8,
                    noOfEarVarId:9
                }
            ])
            occurrenceHelperStub.resolves({
                committedDataSelect: ['a','b','c'],
                committedData: ['a','b','c'],
                committedDataAs: [],
                terminalDataSelect: ['a','b','c'],
                terminalData: ['a','b','c'],
                terminalDataAs: [],
                variableIds: '1,2,3'
            })
            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate InternalError '
            + 'crosses count query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'sort'
                },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves(['occurrence'])
            queryStub.onCall(1).resolves([
                {
                    harvestDateVarId:1,
                    harvestMethodVarId:2,
                    crossingDateVarId:3,
                    noOfSeedVarId:4,
                    noOfBagVarId:5,
                    noOfPlantVarId:6,
                    specificPlantNoVarId:7,
                    noOfPanicleVarId:8,
                    noOfEarVarId:9
                }
            ])
            getOrderStringStub.resolves('valid')
            queryStub.onCall(2).resolves(['cross1'])
            occurrenceHelperStub.resolves({
                committedDataSelect: ['a','b','c'],
                committedData: ['a','b','c'],
                committedDataAs: [],
                terminalDataSelect: ['a','b','c'],
                terminalData: ['a','b','c'],
                terminalDataAs: [],
                variableIds: '1,2,3'
            })
            errorBuilderStub.resolves('InternalError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly 2 times')
        })
    })

    describe('/POST occurrences/:id/packages-search', () => {
        const { post } = require('../../routes/v3/occurrences/_id/packages-search');
        const sandbox = sinon.createSandbox()
        let queryStub
        let tokenHelperStub
        let errorBuilderStub
        let getFilterStub
        let getOrderStringStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve all package records '
            + 'of the occurrence', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            queryStub.onCall(1).resolves(['package'])
            queryStub.onCall(2).resolves([{count:1}])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly 3 times')        })

        it('should generate BadRequestError when the occurrence id '
            + 'is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: 'abcd'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(0, 'query must NOT be called')
        })

        it('should generate InternalError when the '
            + 'occurrence query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await post(req, res, next)
            
            expect(queryStub.callCount).to.equal(1, 'query must be called exactly one time')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly one time')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate BadRequestError when request body is provided '
            + 'and the resulting condition string is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'a.aa AS aaa',
                    distinctOn: 'aaa'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            let result = await post(req, res, next)
            
            expect(queryStub.callCount).to.equal(1, 'query must be called exactly one time')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly one time')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly one time')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should successfully retrieve packages '
         + 'when fields are specified and distinctOn is not', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'a.aa AS aaa'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves(['package'])
            queryStub.onCall(2).resolves([{count:1}])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly 3 times')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly 1 time')
        })

        it('should successfully retrieve packages '
         + 'when fields and distinctOn are specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'a.aa AS aaa',
                    distinctOn: 'aaa'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves(['package'])
            queryStub.onCall(2).resolves([{count:1}])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly 3 times')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly 1 time')
        })

        it('should generate BadRequestError when '
            + 'sort is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'aaa:asc'
                },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'a.aa AS aaa',
                    distinctOn: 'aaa'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.onCall(0).resolves('valid')
            getOrderStringStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly 1 time')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly 1 time')
            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly 1 time')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly 1 time')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should successfully retrieve packages '
         + 'sort is valid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'aaa:asc'
                },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'a.aa AS aaa',
                    distinctOn: 'aaa'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.onCall(0).resolves('valid')
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves(['package'])
            queryStub.onCall(2).resolves([{count:1}])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly 3 times')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly 1 time')
            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly 1 time')
        })

        it('should generate InternalError when the package query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'aaa:asc'
                },
                params: {
                    id: '1234'
                },
                body: {
                    fields: 'a.aa AS aaa',
                    distinctOn: 'aaa'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.onCall(0).resolves('valid')
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InteralError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly 2 times')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly 1 time')
            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly 1 time')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly 1 time')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate InternalError when the package count query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'aaa:asc'
                },
                params: {
                    id: '1234'
                },
                body: {
                    distinctOn: 'aaa'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(['occurrence'])
            getFilterStub.onCall(0).resolves('valid')
            getOrderStringStub.onCall(0).resolves('valid')
            queryStub.onCall(1).resolves(['package'])
            queryStub.onCall(2).rejects()
            errorBuilderStub.onCall(0).resolves('InteralError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly 3 times')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly 1 time')
            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly 1 time')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly 1 time')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })

    describe('/GET occurrences/:id/plot-data-count', () => {
        const { get } = require('../../routes/v3/occurrences/_id/plot-data-count');
        const sandbox = sinon.createSandbox()
        let queryStub
        let tokenHelperStub
        let errorBuilderStub
        let getFilterStub
        let getOrderStringStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the occurrence ID'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: 'abcd'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            errorBuilderStub.onCall(0).resolves('BadRequestError')

            let result = await get(req, res, next)

            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400241)
        })

        it('should generate an InternalError when the occurrence retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the occurrence is not found', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([])

            let result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
        })

        it('should generate an InternalError when the plot data count retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{occurrenceDbId:1234}])
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully retrieve the plot data count of the occurrence', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: { },
                params: {
                    id: '1234'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{occurrenceDbId:1234}])
            queryStub.onCall(1).resolves([{count:20}])

            let result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
        })
    })
})