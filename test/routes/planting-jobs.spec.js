/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const errors = require('restify-errors')
const processQueryHelper = require('../../helpers/processQuery/index.js')
const validator = require('validator')
const tokenHelper = require('../../helpers/auth/token.js')
const userValidator = require('../../helpers/person/validator.js')
const { expect } = require('chai')

let req

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Planting Jobs', () => {
    describe('/POST planting-jobs', () => {
        const { post } = require('../../routes/v3/planting-jobs/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new planting job record',
            async () => {

        })
    })

    describe('/PUT/:id planting-jobs', () => {
        const { put } = require('../../routes/v3/planting-jobs/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update an existing planting job record ' +
            'given a planting job ID', async () => {

        })
    })

    describe('/DELETE/:id planting-jobs', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully void a specific and existing planting job ' +
            'record given a planting job ID.', async () => {

        })
    })

    describe('/POST/:id/entry-generations planting-jobs', () => {
        const { post } = require('../../routes/v3/planting-jobs/_id/entry-generations');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let userValidatorStub
        let errorBuilderStub
        let transactionStub
        let queryStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError if planting job ID is not an ' +
            'integer (in string format)', async () => {

            //arrange
            req = {
                params: {
                    id: 'notAnInteger'
                },
                headers: {
                    host: 'testHost',
                },
            }

            //mock
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            const result = await post(req, res, next)

            //assert
            expect(queryStub.called).to.be.false('query must not be called')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            
            //arrange
            req = {
                params: {
                    id: '123'
                },
                headers: {
                    host: 'testHost',
                },
            }

            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a NotFoundError(404048) when planting job record is not found', async () => {
            
            //arrange
            req = {
                params: {
                    id: '123'
                },
                headers: {
                    host: 'testHost',
                },
            }

            //mock
            getUserIdStub.resolves(1)
            userValidatorStub.resolves(true)
            errorBuilderStub.resolves('NotFoundError')
            queryStub.resolves([])

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404048)
        })

        it('should throw a BadRequestError when planting job entry records already exists', async () => {
            
            //arrange
            req = {
                params: {
                    id: '123'
                },
                headers: {
                    host: 'testHost',
                },
            }

            //mock
            let errorsStub = sandbox.stub(errors, 'BadRequestError')
            getUserIdStub.resolves(1)
            userValidatorStub.resolves(true)
            errorBuilderStub.resolves('BadRequestError')
            queryStub.onCall(0).resolves([{id: 123}])
            queryStub.onCall(1).resolves([])
            queryStub.onCall(2).resolves([{
                id: 123
            }])

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(3,'query must be called exactly thrice')
            sinon.assert.calledWithExactly(errorsStub, 'Planting job entry records already exist.')
        })

        it('should successfully generate entries of a planting job', async () => {
            //arrange
            req = {
                params: {
                    id: '123'
                },
                headers: {
                    host: 'testHost',
                },
            }

            //mock
            let errorsStub = sandbox.stub(errors, 'BadRequestError')
            getUserIdStub.resolves(1)
            userValidatorStub.resolves(true)
            errorBuilderStub.resolves('BadRequestError')
            queryStub.onCall(0).resolves([{id: 123}])
            queryStub.onCall(1).resolves([])
            queryStub.onCall(2).resolves([])
            transactionStub.resolves([[{ id: 1 }]])

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(3,'query must be called exactly thrice')
            expect(transactionStub.callCount).to.equal(1,'transaction must be called exactly once')
        })
    })

    describe('/GET/:id/summary planting-jobs', () => {
        const { GET } = require('../../routes/v3/planting-jobs/_id/summaries');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve the planting job summary given a ' +
            'planting job ID', async () => {

        })
    })

    describe('/GET/:id/entry-summaries planting-jobs', () => {
        const { GET } = require('../../routes/v3/planting-jobs/_id/entry-summaries');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve the planting job summary given a ' +
            'planting job ID.', async () => {

        })
    })

    describe('/GET/:id/occurrence-summaries planting-jobs', () => {
        const { get } = require('../../routes/v3/planting-jobs/_id/occurrence-summaries');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let getCountFinalSqlQueryStub
        let getFinalSqlQueryStub
        let getOrderStringStub
        let queryStub
        let isIntStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            queryStub = sandbox.stub(sequelize, 'query')
            isIntStub = sandbox.stub(validator, 'isInt')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve occurrence summaries ' +
            'given a planting job ID', async () => {
            // Set request parameters
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: null,
                },
                headers: {
                    host: 'host',
                },
            }

            // Simulate processing request
            tokenHelperStub.resolves(123)
            isIntStub.resolves(true)
            queryStub.onFirstCall().resolves(['plantingJob'])
            getFinalSqlQueryStub.resolves('final query')
            queryStub.onSecondCall().resolves(['record1, record2, record3'])
            getCountFinalSqlQueryStub.resolves('count query')
            queryStub.onThirdCall().resolves([ { count: 100 } ])

            await get(req, res, next)

            // Set validation
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
            expect(queryStub.calledThrice).to.be.true('query must be called thrice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('occurrence summaries must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate InternalError if query builder for planting job ' +
            'encounters an error', async () => {
            // Set request parameters
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: null,
                },
                headers: {
                    host: 'host',
                }
            }

            // Simulate processing request
            tokenHelperStub.resolves(123)
            isIntStub.resolves(true)
            queryStub.onFirstCall().rejects()
            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            // Set validation
            expect(isIntStub.calledOnce).to.be.true('isInt must be called once')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        })

        it('should generate InternalError if query builder for occurrence summaries ' +
            'encounters an error', async () => {
            // Set request parameters
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: null,
                },
                headers: {
                    host: 'host',
                }
            }

            // Simulate processing request
            tokenHelperStub.resolves(123)
            isIntStub.resolves(true)
            queryStub.onFirstCall().resolves(['plantingJob'])
            getFinalSqlQueryStub.resolves('final query')
            queryStub.onSecondCall().resolves(undefined)
            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            // Set validation
            expect(isIntStub.calledOnce).to.be.true('isInt must be called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        })

        it('should return empty rows and 0 count if query for occurrence summaries ' +
            'returns empty', async () => {
            // Set request parameters
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: null,
                },
                headers: {
                    host: 'host',
                }
            }

            // Simulate processing request
            tokenHelperStub.resolves(123)
            isIntStub.resolves(true)
            queryStub.onFirstCall().resolves(['plantingJob'])
            getFinalSqlQueryStub.resolves('final query')
            queryStub.onSecondCall().resolves([])

            await get(req, res, next)

            // Set validation
            expect(isIntStub.calledOnce).to.be.true('isInt must be called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be called once')
        })

        it('should generate IneternalError if query for occurrence summary count ' +
            'encounters an error', async () => {
            // Set request parameters
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: null,
                },
                headers: {
                    host: 'host',
                }
            }

            // Simulate processing request
            tokenHelperStub.resolves(123)
            isIntStub.resolves(true)
            queryStub.onFirstCall().resolves(['plantingJob'])
            getFinalSqlQueryStub.resolves('final query')
            queryStub.onSecondCall().resolves(['record1', 'record2', 'record3'])
            getCountFinalSqlQueryStub.resolves('count query')
            queryStub.onThirdCall().resolves(undefined)
            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            // Set validation
            expect(isIntStub.calledOnce).to.be.true('isInt must be called once')
            expect(queryStub.calledThrice).to.be.true('query must be called thrice')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be called once')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        })

        it('should generate NotFoundError if planting job does not exist', async () => {
            // Set request parameters
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: null,
                },
                headers: {
                    host: 'host',
                }
            }

            // Simulate processing request
            tokenHelperStub.resolves(123)
            isIntStub.resolves(true)
            queryStub.onFirstCall().resolves([])
            errorBuilderStub.resolves('NotFoundError')

            await get(req, res, next)

            // Set validation
            expect(isIntStub.calledOnce).to.be.true('isInt must be called once')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        })

        it('should generate BadRequestError if sort input is invalid', async () => {
            // Set request parameters
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: 'invalid:input',
                },
                headers: {
                    host: 'host',
                }
            }

            // Simulate processing request
            tokenHelperStub.resolves(123)
            isIntStub.resolves(true)
            queryStub.onFirstCall().resolves(['plantingJob'])
            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await get(req, res, next)

            // Set validation
            expect(isIntStub.calledOnce).to.be.true('isInt must be called once')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        })
    })
})