/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const processQueryHelper = require("../../helpers/processQuery/index.js");

let req = {}
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Seed Transfers Search', () => {
    describe('POST /seed-transfers-search', () => {
        const { post } = require('../../routes/v3/seed-transfers-search/index');
        const sandbox = sinon.createSandbox()
        let getDistinctStringStub
        let getFieldValuesStringStrub
        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let getCountFinalSqlQueryStub
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            getFilterStub = sandbox.stub(processQueryHelper,'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper,'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper,'getFinalSqlQuery')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper,'getCountFinalSqlQuery')
            getDistinctStringStub = sandbox.stub(processQueryHelper,'getDistinctString')
            getFieldValuesStringStrub = sandbox.stub(processQueryHelper,'getFieldValuesString')
            queryStub = sandbox.stub(sequelize,'query')
            errorBuilderStub = sandbox.stub(errorBuilder,'getError')

            // Initialize request parameter
            req = {
                headers: {
                    host:'testHost'
                },
                query: {},
                paginate: {
                    limit: 1,
                    offset: 1
                }
            }
        })

        afterEach(() => {
            sandbox.restore()
            req = {}
        })

        it('should generate a BadRequestError(400022) if filter condition is invalid', async () => {
            req.body = {
                "some_invalid_filter": true
            }
            getFilterStub.resolves(['invalid'])
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate a BadRequestError(400004) if sort condition is invalid', async () => {
            req.query.sort = 'sortField:invalidSortType'
            getOrderStringStub.resolves(['invalid'])
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate an InternalError(500004) if SELECT failed on search', async () => {
            queryStub.onFirstCall().rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(queryStub.calledOnce).to.be.true('query is called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError(500004) if SELECT failed on count', async () => {
            queryStub.onCall(0).resolves([['record 1', 'record 2']])
            queryStub.onCall(1).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery called once')
            expect(queryStub.calledTwice).to.be.true('query is called twice')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError(500001) if some other error occurs', async () => {
            getCountFinalSqlQueryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder is not called')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should successfully retrieve seed transfer records without any condition', async() => {
            queryStub.onFirstCall().resolves([['record 1', 'record 2']])
            queryStub.onSecondCall().resolves([{count: 2}])

            const result = await post(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery called once')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery called once')
            expect(queryStub.calledTwice).to.be.true('query is called twice')
            expect(errorBuilderStub.called).to.be.false('errorBuilder is not called')
        })
    })
})