/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize.js')
const errorBuilder = require('../../../helpers/error-builder.js')
const tokenHelper = require('../../../helpers/auth/token.js')
const logger = require('../../../helpers/logger/index.js')
const processQueryHelper = require('../../../helpers/processQuery/index.js')

let req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Background Jobs Search', () => {
    describe('/POST background-jobs-search', () => {
        const { post } = require('../../../routes/v3/background-jobs-search/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub
        let getOrderStringStub
        let getFilterStub
        let getAddedOrderStringStub
        let getDistinctStringStub
        let getFieldValuesStringStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getAddedOrderStringStub = sandbox.stub(processQueryHelper, 'getAddedOrderString')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve data with no parameters', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "abc:asc"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{totalCount:1}])
            getOrderStringStub.resolves('valid')

            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should successfully close when no data is available', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([])

            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should generate an InternalError when the query fails', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')


            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when the sort is invalid', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "xxx"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')


            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(0,'query stub must be called exactly zero times')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate a BadRequestError when the request body is invalid', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "abc:asc"
                },
                body: {
                    a: 'xxx'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getFilterStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')


            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(0,'query stub must be called exactly zero times')
            expect(getOrderStringStub.callCount).to.be.equal(0,'getOrderString stub must be called exactly zero times')
            expect(getFilterStub.callCount).to.be.equal(1,'getFilter stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should successfully retrieve data when the request body is valid', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "abc:asc"
                },
                body: {
                    a: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getFilterStub.resolves('valid')
            getOrderStringStub.resolves('valid')
            queryStub.onCall(0).resolves([{totalCount:1}])


            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
            expect(getFilterStub.callCount).to.be.equal(1,'getFilter stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should successfully retrieve data when the request body contains distinctOn', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "abc:asc"
                },
                body: {
                    distinctOn: 'a',
                    a: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getFilterStub.resolves('valid')
            getOrderStringStub.resolves('valid')
            getAddedOrderStringStub.resolves('valid')
            getDistinctStringStub.resolves('valid')
            queryStub.onCall(0).resolves([{totalCount:1}])


            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
            expect(getFilterStub.callCount).to.be.equal(1,'getFilter stub must be called exactly once')
            expect(getAddedOrderStringStub.callCount).to.be.equal(1,'getAddedOrderString stub must be called exactly once')
            expect(getDistinctStringStub.callCount).to.be.equal(1,'getDistinctString stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should successfully retrieve data when the request body contains fields', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "abc:asc"
                },
                body: {
                    fields: 'a as a',
                    a: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getFilterStub.resolves('valid')
            getOrderStringStub.resolves('valid')
            queryStub.onCall(0).resolves([{totalCount:1}])


            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
            expect(getFilterStub.callCount).to.be.equal(1,'getFilter stub must be called exactly once')
            expect(getAddedOrderStringStub.callCount).to.be.equal(0,'getAddedOrderString stub must be called exactly zero times')
            expect(getDistinctStringStub.callCount).to.be.equal(0,'getDistinctString stub must be called exactly zero times')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should successfully retrieve data when the request body contains fields and distinctOn', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: "abc:asc"
                },
                body: {
                    distinctOn: 'a',
                    fields: 'a as a',
                    a: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getFilterStub.resolves('valid')
            getOrderStringStub.resolves('valid')
            getAddedOrderStringStub.resolves('valid')
            getDistinctStringStub.resolves('valid')
            getFieldValuesStringStub.resolves('valid')
            queryStub.onCall(0).resolves([{}])


            // Act
            const result = await post(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(getOrderStringStub.callCount).to.be.equal(1,'getOrderString stub must be called exactly once')
            expect(getFilterStub.callCount).to.be.equal(1,'getFilter stub must be called exactly once')
            expect(getAddedOrderStringStub.callCount).to.be.equal(1,'getAddedOrderString stub must be called exactly once')
            expect(getDistinctStringStub.callCount).to.be.equal(1,'getDistinctString stub must be called exactly once')
            expect(getFieldValuesStringStub.callCount).to.be.equal(1,'getDistinctString stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })
    })
})