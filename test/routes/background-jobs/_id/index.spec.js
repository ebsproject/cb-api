/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize.js')
const errorBuilder = require('../../../../helpers/error-builder.js')
const tokenHelper = require('../../../../helpers/auth/token.js')
const logger = require('../../../../helpers/logger/index.js')
const processQueryHelper = require('../../../../helpers/processQuery/index.js')

let req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Background Jobs ID', () => {
    const { get, put } = require('../../../../routes/v3/background-jobs/_id/index');

    describe('/GET background-jobs/:id', () => {
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the background job' 
            + ' id is not an integer', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                },
                query: {
                    jobStatus: "IN_QUEUE"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            // Act
            const result = await get(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(0,'query stub must be called exactly zero times')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should generate an InternalError when the query fails', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            // Act
            const result = await get(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the query returns null', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(null)

            // Act
            const result = await get(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should generate a NotFoundError when the query returns empty result', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([])

            // Act
            const result = await get(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })

        it('should successfully retrieve the background job record', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{id:123}])

            // Act
            const result = await get(req, res, next)

            // Assert
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
        })
    })

    describe('/GET background-jobs/:id', () => {
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub
        let getUserIdStub
        let transactionStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError if request body is not provided', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            errorBuilderStub.onCall(0).resolves('BadRequestError')

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            expect(getUserIdStub.callCount).to.be.equal(0,'getUserId stub must be called exactly zero times')
            expect(queryStub.callCount).to.be.equal(0,'query stub must be called exactly zero times')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should generate a BadRequestError if background job id is not an integer', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                },
                body: {

                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            errorBuilderStub.onCall(0).resolves('BadRequestError')

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            expect(getUserIdStub.callCount).to.be.equal(0,'getUserId stub must be called exactly zero times')
            expect(queryStub.callCount).to.be.equal(0,'query stub must be called exactly zero times')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400143)
        })

        it('should terminate if user id is undefined', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {

                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(undefined)

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(0,'query stub must be called exactly zero times')
        })

        it('should generate a BadRequestError if user id is null', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {

                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(null)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(0,'query stub must be called exactly zero times')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate an InternalError if background job retrieval fails', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {

                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(987)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError if background job retrieval'
            + ' returns null', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {

                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(987)
            queryStub.onCall(0).resolves(null)

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
        })

        it('should generate a NotFoundError if background job retrieval'
            + ' returns empty array', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {

                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(987)
            queryStub.onCall(0).resolves([])

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
        })

        it('should generate a BadRequestError if job status is invalid', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {
                    jobStatus: 'ABC',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(987)
            queryStub.onCall(0).resolves([{
                jobDescription: 'test jobDescription og',
                jobStatus: 'IN_QUEUE',
                jobMessage: 'test jobMessage og',
                jobEndTime: 'test jobEndTime og',
                jobRemarks: 'test jobRemarks og',
                jobIsSeen: 'test jobIsSeen og',
                notes: 'test notes og'
            }])

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
        })

        it('should successfully update all record info', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {
                    jobDescription: 'test jobDescription new',
                    jobStatus: 'IN_PROGRESS',
                    jobMessage: 'test jobMessage new',
                    jobEndTime: 'test jobEndTime new',
                    jobRemarks: 'test jobRemarks new',
                    jobIsSeen: 'test jobIsSeen new',
                    notes: 'test notes new'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(987)
            queryStub.onCall(0).resolves([{
                jobDescription: 'test jobDescription og',
                jobStatus: 'IN_QUEUE',
                jobMessage: 'test jobMessage og',
                jobEndTime: 'test jobEndTime og',
                jobRemarks: 'test jobRemarks og',
                jobIsSeen: 'test jobIsSeen og',
                notes: 'test notes og'
            }])
            transactionStub.onCall(0).resolves([{id: 123}])

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder stub must be called exactly zero times')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(transactionStub.callCount).to.be.equal(1,'transaction stub must be called exactly once')
        })

        it('should generate InternalError when update query fails', async () => {
            // Arrange
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "123"
                },
                body: {
                    jobDescription: 'test jobDescription new',
                    jobStatus: 'IN_PROGRESS',
                    jobMessage: 'test jobMessage new',
                    jobEndTime: 'test jobEndTime new',
                    jobRemarks: 'test jobRemarks new',
                    jobIsSeen: 'test jobIsSeen new',
                    notes: 'test notes new'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getUserIdStub.onCall(0).resolves(987)
            queryStub.onCall(0).resolves([{
                jobDescription: 'test jobDescription og',
                jobStatus: 'IN_QUEUE',
                jobMessage: 'test jobMessage og',
                jobEndTime: 'test jobEndTime og',
                jobRemarks: 'test jobRemarks og',
                jobIsSeen: 'test jobIsSeen og',
                notes: 'test notes og'
            }])
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            // Act
            const result = await put(req, res, next)

            // Assert
            expect(errorBuilderStub.callCount).to.be.equal(1,'errorBuilder stub must be called exactly once')
            expect(getUserIdStub.callCount).to.be.equal(1,'getUserId stub must be called exactly once')
            expect(queryStub.callCount).to.be.equal(1,'query stub must be called exactly once')
            expect(transactionStub.callCount).to.be.equal(1,'transaction stub must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500003)
        })
    })
})