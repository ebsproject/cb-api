/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const processQueryHelper = require('../../helpers/processQuery/index')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Variables', () => {
    describe('/GET variables', () => {
        const { get } = require('../../routes/v3/variables/index');
        const sandbox = sinon.createSandbox()
        let getFilterStringStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let queryStub
        let getCountFinalSqlQueryStub
        let errorBuilderStub

        beforeEach(() => {
            getFilterStringStub = sandbox.stub(processQueryHelper, 'getFilterString')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            queryStub = sandbox.stub(sequelize, 'query')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve information of accessible variable ' +
            'records', async () => {
            req = {
                headers: {},
                query: {},
                paginate: {
                    limit: 1,
                    offset: 1
                }
            }
            queryStub.onFirstCall().resolves(['variable1', 'variable2'])
            getCountFinalSqlQueryStub.resolves(2)
            queryStub.onSecondCall().resolves([{ count: 2 }])

            const result = await get(req, res, next)

            expect(getFilterStringStub.called).to.be.false('abbrev should be undefined')
            expect(getOrderStringStub.called).to.be.false('sort should be undefined')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be called once')
            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('variables must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })
    })

    describe('/GET/:id variables', () => {
        const { get } = require('../../routes/v3/variables/_id/index');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve information of a specific and ' +
            'existing variable record given a variable ID', async () => {
            req = {
                headers: {},
                params: {
                    id: "10"
                }
            }
            queryStub.resolves(['variable1', 'variable2'])

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if variable ID is not an ' +
            'integer (in string format)', async () => {
            req = {
                headers: {
                    host: "testHost"
                },
                params: {
                    id: "notAnInteger"
                }
            }
            errorBuilderStub.resolves('BadRequestError')

            const result = await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400081)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate InternalError if error occurs when querying ' +
            'the database', async () => {
            req = {
                headers: {
                    host: "testHost"
                },
                params: {
                    id: "10"
                }
            }
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate NotFoundError if variable ID does not exist',
            async () => {
            req = {
                headers: {
                    host: "testHost"
                },
                params: {
                    id: "10"
                }
            }
            queryStub.resolves([])
            errorBuilderStub.resolves('NotFoundError')

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404029)
        })
    })

    describe('/GET/:id/methods variables', () => {
        const { get } = require('../../routes/v3/variables/_id/methods');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible method ' +
            'records of a specific and existing variable record given a ' +
            'variable ID', async () => {

        })
    })

    describe('/GET/:id/properties variables', () => {
        const { get } = require('../../routes/v3/variables/_id/properties');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible property ' +
            'records of a specific and existing variable record given a ' +
            'variable ID', async () => {

        })
    })

    describe('/GET/:id/scales variables', () => {
        const { get } = require('../../routes/v3/variables/_id/scales');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible scale ' +
            'records of a specific and existing variable record given a ' +
            'variable ID', async () => {

        })
    })

    describe('/POST/:id/scales-values-search variables', () => {
        const { post } = require('../../routes/v3/variables/_id/scale-values-search');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully search and filter accessible scale-value ' +
            'records', async () => {

        })
    })

    describe('/GET/:id/variable-sets variables', () => {
        const { get } = require('../../routes/v3/variables/_id/variable-sets');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible variable ' +
            'set records of a specific and existing variable record given a ' +
            'variable ID', async () => {

        })
    })
})



