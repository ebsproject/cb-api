/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize.js')
const errorBuilder = require('../../../helpers/error-builder.js')
const tokenHelper = require('../../../helpers/auth/token.js')
const logger = require('../../../helpers/logger/index.js')
const variableHelper = require('../../../helpers/variable/index.js')

const req = {
    headers: {
        host: 'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

const next = {}

describe('Germplasm MTA', () => {
    describe('/POST germplasm-mta', () => {
        const { post } = require('../../../routes/v3/germplasm-mta/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let logFailureStub
        let isInScaleValuesStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            logFailureStub = sandbox.stub(logger, 'logFailure')
            isInScaleValuesStub = sandbox.stub(variableHelper, 'isInScaleValues')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw BadRequestError when person ID in invalid', async () => {
            // arrange
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // act
            await post(req, res, next)

            // assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw BadRequestError if nor request body provided', async () => {
            // arrange
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // act
            await post(req, res, next)

            // assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw BadRequestError if records have missing required parameters', async () => {
            // ARRANGE
            req.body = {
                records: []
            }

            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        it('should throw error when germplasmDbId is invalid', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        "germplasmDbId": "123456",
                        "seedDbId": "abcd",
                        "mtaStatus": "1235"
                    }
                ]
            }

            getUserIdStub.resolves(1)

            // ACT
            await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        });

        it('should throw BadRequestError if records have missing required parameters', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        "germplasmDbId": "123456",
                        "seedDbId": "abcd"
                    }
                ]
            }

            getUserIdStub.resolves(1)

            // ACT
            await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })

        it('should throw BadRequestError when mtaStatus is invalid', async () => {
            // ARRANGE
            req.body = {
                records: [
                    {
                        "germplasmDbId": "17366750",
                        "seedDbId": "abcd",
                        "mtaStatus": "1235"
                    }
                ]
            }

            getUserIdStub.resolves(1)
            queryStub.resolves([{ exists: 1 }])
            isInScaleValuesStub.resolves([{ count: '0' }])

            // ACT
            await post(req, res, next)

            // ASSERT
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        });

        it('should throw BadRequestError when seedDbId provided is invalid', async () => {

            // ARRANGE
            req.body = {
                records: [
                    {
                        "germplasmDbId": "17366750",
                        "seedDbId": "abcd",
                        "mtaStatus": "UNKNOWN"
                    }
                ]
            }

            getUserIdStub.resolves(1)
            queryStub.resolves([{ exists: 1 }])
            isInScaleValuesStub.resolves([{ count: '1' }])
            errorBuilderStub.resolves('BadRequestError')

            // ACT
            await post(req, res, next)

            // ASSERT

            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400248)
        });

        it('should throw NotFoundError when seedDbId provided has no matching record', async () => {

            // ARRANGE
            req.body = {
                records: [
                    {
                        "germplasmDbId": "17366750",
                        "seedDbId": "123",
                        "mtaStatus": "UNKNOWN"
                    }
                ]
            }

            getUserIdStub.resolves(1)
            queryStub.onCall(0).resolves([{ exists: 1 }])
            isInScaleValuesStub.resolves([{ count: '1' }])
            queryStub.onCall(1).resolves([{ exists: 1 }])
            queryStub.onCall(2).resolves([{ exists: 0 }])
            errorBuilderStub.resolves('InternalError')

            // ACT
            await post(req, res, next)

            // ASSERT

            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        });

        it('should throw BadRequestError when records are empty', async () => {
            // ARRANGE
            req.body = {
                records: []
            }

            getUserIdStub.resolves(1)
            errorBuilderStub.onCall(0).rejects()
            logFailureStub.resolves('error')
            errorBuilderStub.onCall(1).resolves('BadRequestError')

            // ACT
            await post(req, res, next)

            // ASSERT

            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(logFailureStub.calledOnce).to.be.true('logFailure must be called exactly once')
            expect(errorBuilderStub.calledTwice).to.be.true('errorBuilder must be called exactly twice')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        });

        it('should successfully insert new germplasm_mta record with only the required parameters',
            async () => {

                // ARRANGE
                req.body = {
                    records: [
                        {
                            "germplasmDbId": "17366750",
                            "seedDbId": "12736853",
                            "mtaStatus": "UNKNOWN"
                        }
                    ]
                }

                getUserIdStub.resolves(1)
                queryStub.onCall(0).resolves([{ 'count': 1 }])
                transactionStub.resolves([[{ 'id': 1 }], 1])

                // ACT
                await post(req, res, next)

                // ASSERT
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(queryStub.calledOnce).to.be.true('query must be called once')
            });
    })
})