/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errors = require('restify-errors')
const { expect } = require('chai')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Transaction Files', () => {
    describe('/GET/:id transaction-files', () => {
        const { get } = require('../../routes/v3/transaction-files/_id/index')
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve the dataset of a specific and ' +
            'existing transaction file record given a transaction file ID in ' +
            'table format', async () => {

        })
    })

    describe('/GET/:id/cross-data transaction-files', () => {
        const { get } = require('../../routes/v3/transaction-files/_id/cross-data')
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve the cross dataset of a specific and ' +
            'existing transaction file record given a transaction file ID in ' +
            'table format', async () => {

        })
    })

    describe('/DELETE/:id transaction-files', () => {
        const { delete:del } = require('../../routes/v3/transaction-files/_id/index')
        const sandbox = sinon.createSandbox()
        let errorStub
        let queryStub

        beforeEach(() => {
            errorStub = sandbox.stub(errors, 'BadRequestError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError if transaction file ID is not an ' +
            'integer (in string format)', async () => {

            // Arrange
            req.params = {
                id: 'notAnInteger'
            }

            // Mock
            errorStub.resolves('BadRequestError')
            
            // Act  
            await del(req, res, next)

            //assert
            expect(errorStub.calledOnce).to.be.true('errorStub must be called exactly once')
            expect(queryStub.called).to.be.false('query must not be called')
        })
    })
})