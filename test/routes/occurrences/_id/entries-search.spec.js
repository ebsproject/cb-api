/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
const { knex } = require('../../../../config/knex')

let errorBuilder = require('../../../../helpers/error-builder')
const processQueryHelper = require('../../../../helpers/processQuery/index')
const tokenHelper = require('../../../../helpers/auth/token.js')
const responseHelper = require('../../../../helpers/responses')

let req

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Occurrences', () => {
    describe('/POST occurrences/:id/entries-search', () => {
        const { post } = require('../../../../routes/v3/occurrences/_id/entries-search')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let getFilterWithInfoStub
        let getFinalTotalCountQueryStub
        let getDistinctStringStub
        let getFieldValuesStringStub
        let getFinalSqlQueryWoLimitStub
        let getOrderStringStub
        let knexRawStub
        let knexSelectStub
        let knexColumnStub
        let queryStub
        let tokenHelperStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getFinalTotalCountQueryStub = sandbox.stub(processQueryHelper, 'getFinalTotalCountQuery')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFilterWithInfoStub = sandbox.stub(processQueryHelper, 'getFilterWithInfo')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
            getFinalSqlQueryWoLimitStub = sandbox.stub(processQueryHelper, 'getFinalSqlQueryWoLimit')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            knexRawStub = sandbox.stub(knex, 'raw')
            knexSelectStub = sandbox.stub(knex, 'select')
            knexColumnStub = sandbox.stub(knex, 'column')
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate BadRequestError if occurrenceDbId ' +
            'is invalid', async () => {

            req = {
            	params: {
                    id: 'notAnInteger'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(queryStub.called).to.be.false('no query should have been called')
        })

        it('should successfully retrieve entry records ' + 
            'with fields in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                params: {
                    id: '123'
                },
                query: {},
                body: {
                    fields: 'this.field AS thisField'
                }
            }
            tokenHelperStub.resolves(123)
            knexColumnStub.resolves('column')
            getFilterWithInfoStub.resolves('conditionString')
            getFinalSqlQueryWoLimitStub.resolves('query')
            queryStub.resolves(['entryRecord1, entryRecord2, entryRecord3'])
            getFinalTotalCountQueryStub.resolves(100)

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('entry records must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve entry records ' + 
            'with distinctOn in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                params: {
                    id: '123'
                },
                query: {},
                body: {
                    distinctOn: 'thisColumn'
                }
            }

            tokenHelperStub.resolves(123)
            getDistinctStringStub.resolves('addedDistinctString')
            getFilterWithInfoStub.resolves('query')
            getFinalSqlQueryWoLimitStub.resolves('query')
            queryStub.resolves(['entryRecord1, entryRecord2, entryRecord3'])
            getFinalTotalCountQueryStub.resolves(100)

            await post(req, res, next)

            expect(getDistinctStringStub.calledOnce).to.be.true('getDistinctString must be successfully called once')
            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('entry records must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve entry records ' + 
            'with fields and distinctOn in request body', async () => {
            req = {
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                params: {
                    id: '123'
                },
                query: {},
                body: {
                    fields: 'this.field AS thisField',
                    distinctOn: 'thisColumn'
                }
            }

            tokenHelperStub.resolves(123)
            getDistinctStringStub.resolves('addedDistinctString')
            getFilterWithInfoStub.resolves('query')
            getFieldValuesStringStub.resolves('fieldsString')
            knexRawStub.resolves('raw')
            knexSelectStub.resolves('select')
            getFinalSqlQueryWoLimitStub.resolves('query')
            queryStub.resolves(['entryRecord1, entryRecord2, entryRecord3'])
            getFinalTotalCountQueryStub.resolves(100)

            await post(req, res, next)

            expect(getDistinctStringStub.calledOnce).to.be.true('getDistinctString must be successfully called once')
            expect(knexRawStub.calledOnce).to.be.true('knex.raw must be called once')
            expect(knexSelectStub.calledOnce).to.be.true('knex.select must be called once')
            expect(getFieldValuesStringStub.calledOnce).to.be.true('getFieldValuesString must be successfully called once')
            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('entry records must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if sort argument ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },                
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'invalid:sorting'
                }
            }

            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
            expect(queryStub.called).to.be.false('no query should have been called')
        })

        it('should successfully retrieve entry records ' + 
            'with EMPTY request body', async () => {
            req = {
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {}
            }

            tokenHelperStub.resolves(123)
            getFinalSqlQueryWoLimitStub.resolves('query')
            queryStub.resolves(['entryRecord1, entryRecord2, entryRecord3'])
            getFinalTotalCountQueryStub.resolves(100)

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('field tags must be > 0')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if conditionString is invalid',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {},
                body: {
                    test: 'invalidArgument'
                }
            }

            getFilterWithInfoStub.resolves({'mainQuery':'invalid'})
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilterWithInfo must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')  
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate InternalError when the query for retrieving ' +
            'the entry records fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            tokenHelperStub.resolves(123)
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate InternalError if the entry count query fails',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '123'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {}
            }

            tokenHelperStub.resolves(123)
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            await post(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    }),

    describe('/POST occurrences/:id/entity-data-search', () => {
        const { post } = require('../../../../routes/v3/occurrences/_id/entity-data-search')
        const sandbox = sinon.createSandbox()
        let queryStub
        let getFilterStub
        let getDistinctStringStub
        let getOrderStringStub
        let getFinalSqlQueryWoLimitStub
        let getFinalTotalCountQueryStub
        let errorBuilderStub
        let knexRawStub
        let knexSelectStub
        let knexColumnStub
        let getFieldValuesStringStub
        let tokenHelperStub

        beforeEach(() => {
            knexRawStub = sandbox.stub(knex, 'raw')
            knexSelectStub = sandbox.stub(knex, 'select')
            knexColumnStub = sandbox.stub(knex, 'column')
            queryStub = sandbox.stub(sequelize, 'query')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getErrorCodebyMessageStub = sandbox.stub(responseHelper, 'getErrorCodebyMessage')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getOrderStringTraitStub = sandbox.stub(processQueryHelper, 'getOrderStringTrait')
            getFinalSqlQueryWoLimitStub = sandbox.stub(processQueryHelper, 'getFinalSqlQueryWoLimit')
            getFinalTotalCountQueryStub = sandbox.stub(processQueryHelper, 'getFinalTotalCountQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getErrorResponseStub = sandbox.stub(errorBuilder, 'getErrorResponse')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
        })

        afterEach(() => {
            sandbox.restore()
        })

        req = {
            headers: {
                host: 'testHost'
            },
            params: {
                id: '14093'
            },
            paginate: {
                limit: 100,
                offset: 0
            },
            query: {}
        }

        it('should successfully retrieve occurrence information with ' +
            'empty request body', async () => {

            req['body'] = {}
            
            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(2).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(3).resolves([{experimentCode:'queryReslt'}])
            queryStub.onCall(4).resolves([{protocolDbId:'queryReslt'}])
            queryStub.onCall(5).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(6).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(7).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(8).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(9).resolves([{totalCount:1},{text:'occurrenceData'}])

            await post(req,res,next)

            expect(queryStub.callCount).to.equal(3)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve occurrence information with '+
            '"fields" parameters', async () => {

            req['body'] = {
                fields: "experimentCode|experimentName|experimentYear|" +
                        "experimentType|experimentSubtype|experimentDesignType|" +
                        "experimentStatus|experimentDescription|" + 
                        "experimentRemarks|plantingSeason|experimentObjective|" + 
                        "experimentSeason|experimentStageCode|experimentProject|" + 
                        "experimentPipeline|experimentSteward|entryNumber|" + 
                        "entryCode|entryType|entryClass|entryRole|germplasmCode|" + 
                        "germplasmName|germplasmState|parentage|germplasmType|" + 
                        "germplasmNameType|taxonomyName|taxonId|" + 
                        "femaleParentGermplasmCode|maleParentGermplasmCode|" + 
                        "plotNumber|plotCode|plotType|rep|paX|paY|blockNumber"
            }
            
            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(2).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(3).resolves([{experimentCode:'queryReslt'}])
            queryStub.onCall(4).resolves([{protocolDbId:'queryReslt'}])
            queryStub.onCall(5).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(6).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(7).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(8).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(9).resolves([{totalCount:1},{text:'occurrenceData'}])

            await post(req,res,next)

            expect(queryStub.callCount).to.equal(3)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })
        
        it('should successfully retrieve all records with entities parameter', 
            async () => {
            req['body'] ={
                entities: "experiment|occurrence|planting_protocol|management_protocol|"+
                "entry|germplasm|plot|trait"
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(2).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(3).resolves([{experimentCode:'queryReslt'}])
            queryStub.onCall(4).resolves([{protocolDbId:'queryReslt'}])
            queryStub.onCall(5).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(6).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(7).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(8).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(9).resolves([{totalCount:1},{text:'occurrenceData'}])

            await post(req,res,next)

            expect(queryStub.callCount).to.equal(9)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })
        
        it('should successfully retrieve occurrence information with '+
            'entities = "experiment" ', async () => {
            
            req['body'] = {
                entities: "experiment"
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(2).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(3).resolves([{experimentCode:'queryReslt'}])
            queryStub.onCall(4).resolves([{protocolDbId:'queryReslt'}])
            queryStub.onCall(5).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(6).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(7).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(8).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(9).resolves([{totalCount:1},{text:'occurrenceData'}])

            await post(req,res,next)

            expect(queryStub.callCount).to.equal(4)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve occurrence information with '+
            'entities = "experiment|plot" ', async () => {
            
            req['body'] = {
                entities: "experiment|plot"
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(2).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(3).resolves([{experimentCode:'queryReslt'}])
            queryStub.onCall(4).resolves([{protocolDbId:'queryReslt'}])
            queryStub.onCall(5).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(6).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(7).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(8).resolves([{ids:'idString'},{columns:'columnString'}])
            queryStub.onCall(9).resolves([{totalCount:1},{text:'occurrenceData'}])

            await post(req,res,next)

            expect(queryStub.callCount).to.equal(4)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should return Bad Request when occurrence ID provided is not integer', 
            async () => {

            req['body'] = {}
            req['params'] = {
                id: 'notAnInteger'
            }
            
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            await post(req,res,next)

            expect(queryStub.called).to.be.false('no query should have been called')
        })
    })
})