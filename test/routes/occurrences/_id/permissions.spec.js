/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../../../helpers/error-builder')
const tokenHelper = require('../../../../helpers/auth/token')

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Occurrence Permissions', () => {
    describe('POST occurrence permissions', () => {
        const { post } = require('../../../../routes/v3/occurrences/_id/permissions')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let queryStub
        let errorBuilderStub
        let badRequestErrorStub
        let notFoundErrorStub
        let transactionStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            badRequestErrorStub = sandbox.stub(errors, 'BadRequestError')
            notFoundErrorStub = sandbox.stub(errors, 'NotFoundError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully insert a valid record with "read" permission', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entityDbId': 123,
                            'entity': 'program',
                            'permission': 'read',
                        },
                    ],
                },
            }

            //mock
            queryStub.onCall(0).resolves(['accessData'])
            getUserIdStub.onFirstCall().resolves(1)
            queryStub.onCall(1).resolves(['date'])
            queryStub.onCall(2).resolves(['entity'])
            queryStub.onCall(3).resolves(['updatedOccurrenceAccessData'])
            transactionStub.resolves({
                commit: sinon.stub(),
            })

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })

        it('should successfully insert a valid record with "write" permission', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entityDbId': 123,
                            'entity': 'program',
                            'permission': 'write',
                        },
                    ],
                },
            }

            //mock
            queryStub.onCall(0).resolves(['accessData'])
            getUserIdStub.onFirstCall().resolves(1)
            queryStub.onCall(1).resolves(['date'])
            queryStub.onCall(2).resolves(['entity'])
            queryStub.onCall(3).resolves(['updatedOccurrenceAccessData'])
            transactionStub.resolves({
                commit: sinon.stub(),
            })

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {}
                    ]
                },
            }

            //mock
            queryStub.resolves([{ 'id': '1234', 'accessData': '{...}' }])
            getUserIdStub.onFirstCall().resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError (400009) when the request body is not set', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
            }

            //mock
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            await post(req, res, next)

            //assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw a BadRequestError (400005) when the records array is empty', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                    ]
                },
            }

            //mock
            queryStub.resolves([{ 'id': '1234', 'accessData': '{...}' }])
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        it('should throw a BadRequestError (400089) if occurrenceDbId is not an integer', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: 'abc'
                },
            }

            //mock
            errorBuilderStub.resolves('InternalError')

            //act
            await post(req, res, next)

            //assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400089)
        })

        it('should throw a NotFoundError if the validateOccurrence is of length 0', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {},
            }

            //mock
            queryStub.resolves([])

            notFoundErrorStub.resolves('NotFoundError')

            //act
            await post(req, res, next)

            //assert
            expect(queryStub.calledOnce).to.be.true('query should be called thrice')
            expect(notFoundErrorStub.calledOnce).to.be.true('NotFoundError should be called once')
        })

        it('should throw a NotFoundError if the validateEntity is of length 0', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entityDbId': 123,
                            'entity': 'program',
                            'permission': 'read',
                        },
                    ],
                },
            }

            //mock
            queryStub.onCall(0).resolves(['accessData'])
            getUserIdStub.onFirstCall().resolves(1)
            queryStub.onCall(1).resolves(['date'])
            queryStub.onCall(2).resolves([])

            notFoundErrorStub.resolves('NotFoundError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            sinon.assert.callCount(queryStub, 3)
            expect(notFoundErrorStub.calledOnce).to.be.true('NotFoundError should be called once')
        })

        it('should throw a BadRequestError (400170) if the at least one of the required fields are missing', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entity': 'person',
                            'permission': 'read',
                        }
                    ]
                },
            }

            //mock
            queryStub.onCall(0).resolves(['accessData'])
            getUserIdStub.onFirstCall().resolves(1)
            queryStub.onCall(1).resolves(['date'])

            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledTwice).to.be.true('query should be called twice')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400170)
        })

        it('should throw a BadRequestError (400171) if permission is neither "read" nor "write"', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entityDbId': 11,
                            'entity': 'person',
                            'permission': 'execute',
                        }
                    ]
                },
            }

            //mock
            queryStub.onCall(0).resolves(['accessData'])
            getUserIdStub.onFirstCall().resolves(1)
            queryStub.onCall(1).resolves(['date'])

            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledTwice).to.be.true('query should be called twice')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400171)
        })

        it('should throw a BadRequestError if entity is neither "person" nor "program"', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entityDbId': 11,
                            'entity': 'vehicle',
                            'permission': 'read',
                        }
                    ]
                },
            }

            //mock
            queryStub.onCall(0).resolves(['accessData'])
            getUserIdStub.onFirstCall().resolves(1)
            queryStub.onCall(1).resolves(['date'])

            badRequestErrorStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledTwice).to.be.true('query should be called twice')
            expect(badRequestErrorStub.called).to.be.true('BadRequestError should be called')
        })

        it('should throw an InternalError (500004) once an Error is thrown in try-catch #1', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entityDbId': 123,
                            'entity': 'program',
                            'permission': 'read',
                        },
                    ],
                },
            }
            const error = new Error('some fake error')

            //mock
            queryStub.throws(error)

            errorBuilderStub.resolves('InternalError')

            //act
            await post(req, res, next)

            //assert
            expect(queryStub).to.throw(Error)
            expect(errorBuilderStub.calledOnce).to.be.true('InternalError should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw an InternalError (500001) once an Error is thrown in try-catch #2', async () => {
            //arrange
            const req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '1234'
                },
                body: {
                    records: [
                        {
                            'entityDbId': 123,
                            'entity': 'program',
                            'permission': 'read',
                        },
                    ],
                },
            }
            const error = new Error('some fake error')

            //mock
            queryStub.onCall(0).resolves([{'accessData': 'asdasd'}])
            getUserIdStub.onFirstCall().resolves(1)
            queryStub.onCall(1).resolves(['date'])
            queryStub.onCall(2).resolves([
                {'roleCode': 'DATA_PRODUCER', 'id': 1},
                {'roleCode': 'DATA_CONSUMER', 'id': 2},
            ])
            queryStub.throws(error)
            transactionStub.resolves({
                rollback: sinon.stub(),
            })

            errorBuilderStub.resolves('InternalError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub).to.throw(Error)
            expect(errorBuilderStub.calledOnce).to.be.true('InternalError should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

    })
})
