/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize')
const processQueryHelper = require('../../../../helpers/processQuery/index')
const errorBuilder = require('../../../../helpers/error-builder')
const responseHelper = require('../../../../helpers/responses')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Mapping Plots', () => {
    describe('/GET occurrence/:id/mapping-plots', () => {
        const { get } = require('../../../../routes/v3/occurrences/_id/mapping-plots')
        const sandbox = sinon.createSandbox()
        let queryStub
        let getCountFinalSqlQueryStub
        let errorBuilderStub
        let getErrorCodebyMessageStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getErrorCodebyMessageStub = sandbox.stub(responseHelper, 'getErrorCodebyMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve mapping plot records', async () => {
            req = {
                headers: {},
                params: {
                    id: '3388',
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                }
            }

            queryStub.onFirstCall().resolves([{ 'count': 1 }])
            queryStub.onSecondCall().resolves([ 'mappingPlot1', 'mappingPlot2', 'mappingPlot3'])

            await get(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if occurrence ID is not an ' +
            'integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: 'occ'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            await get(req, res, next)
        })

        it('should generate Internal when the query for retrieving ' +
            'the plot field tag records fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3388'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onFirstCall().resolves([{ 'count': 1 }])
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate NotFoundError if the occurrence does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '0'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.resolves(undefined)
            getErrorCodebyMessageStub.resolves('errorCode')
            errorBuilderStub.resolves('NotFoundError')

            await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(getErrorCodebyMessageStub.calledOnce).to.be.true('responseHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
        })

        it('should generate InternalError if the plot field tag count query fails',
            async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3390'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onFirstCall().resolves(['occurrence'])
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })
})