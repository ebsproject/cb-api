/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize')
const errorBuilder = require('../../../../helpers/error-builder')
const processQueryHelper = require('../../../../helpers/processQuery/index')
const searchHelper = require('../../../../helpers/queryTool/index.js')
const responseHelper = require('../../../../helpers/responses')
const { expect } = require('chai')
const { knex } = require('../../../../config/knex')
const tokenHelper = require('../../../../helpers/auth/token.js')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Occurrences', () => {
    describe('/POST occurrences/:id/plot-data-table-search', () => {
        const { post } = require('../../../../routes/v3/occurrences/_id/plot-data-table-search')
        const sandbox = sinon.createSandbox()
        let queryStub
        let getFilterStub
        let getDistinctStringStub
        let getOrderStringStub
        let getFinalSqlQueryWoLimitStub
        let getFinalTotalCountQueryStub
        let errorBuilderStub
        let knexRawStub
        let knexSelectStub
        let knexColumnStub
        let getFieldValuesStringStub
        let getFilterConditionStub
        let tokenHelperStub

        beforeEach(() => {
            knexRawStub = sandbox.stub(knex, 'raw')
            knexSelectStub = sandbox.stub(knex, 'select')
            knexColumnStub = sandbox.stub(knex, 'column')
            queryStub = sandbox.stub(sequelize, 'query')
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getFilterConditionStub = sandbox.stub(searchHelper, 'getFilterCondition')
            getErrorCodebyMessageStub = sandbox.stub(responseHelper, 'getErrorCodebyMessage')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getOrderStringTraitStub = sandbox.stub(processQueryHelper, 'getOrderStringTrait')
            getFinalSqlQueryWoLimitStub = sandbox.stub(processQueryHelper, 'getFinalSqlQueryWoLimit')
            getFinalTotalCountQueryStub = sandbox.stub(processQueryHelper, 'getFinalTotalCountQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getErrorResponseStub = sandbox.stub(errorBuilder, 'getErrorResponse')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve plot records ' + 
            'with EMPTY request body', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{
                'experiment_type': 'Observation',
                'experiment_design_type': 'Entry Order'
            }])
            queryStub.onCall(4).resolves([{
                'occurrence_design_type': 'Entry Order'
            }])
            queryStub.onCall(4).resolves([ 'variables' ])
            getFinalSqlQueryWoLimitStub.resolves('query')
            getFinalTotalCountQueryStub.resolves('query')
            queryStub.onCall(5).resolves(['plotRecord1, plotRecord2, plotRecord3'])

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be > 0')
            expect(queryStub.callCount).to.equal(6)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve plot records ' + 
            'with sort parameters', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: 'field1:asc'
                },
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([ {
                'experiment_type': 'Observation',
                'experiment_design_type': 'Entry Order'
            }])
            queryStub.onCall(3).resolves([ {
                'occurrence_design_type': 'Entry Order'
            }])
            queryStub.onCall(4).resolves([ 'variables' ])
            getOrderStringTraitStub.resolves('sort')
            getFinalSqlQueryWoLimitStub.resolves('query')
            getFinalTotalCountQueryStub.resolves('query')
            queryStub.onCall(5).resolves(['plotRecord1, plotRecord2, plotRecord3'])

            await post(req, res, next)

            expect(getOrderStringTraitStub.calledOnce).to.be.true('getOrderStringTrait must be successfully called once')
            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be > 0')
            expect(queryStub.callCount).to.equal(6)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve plot records ' + 
            'with Alpha-Lattice Experiment Design Type', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{
                'experiment_type': 'Observation',
                'experiment_design_type': 'Alpha-Lattice'
            }])
            queryStub.onCall(3).resolves([{
                'occurrence_design_type': 'Alpha-Lattice',
            }])
            queryStub.onCall(4).resolves([ 'variables' ])
            getFinalSqlQueryWoLimitStub.resolves('query')
            getFinalTotalCountQueryStub.resolves('query')
            queryStub.onCall(5).resolves(['plotRecord1, plotRecord2, plotRecord3'])

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be > 0')
            expect(queryStub.callCount).to.equal(6)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve plot records ' + 
            'with Augmented RCBD Experiment Design Type', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{ 
                'experiment_type': 'Observation',
                'experiment_design_type': 'Augmented RCBD',
            }])
            queryStub.onCall(3).resolves([{ 
                'occurrence_design_type': 'Augmented RCBD',
            }])
            queryStub.onCall(4).resolves([ 'variables' ])
            getFinalSqlQueryWoLimitStub.resolves('query')
            getFinalTotalCountQueryStub.resolves('query')
            queryStub.onCall(5).resolves(['plotRecord1, plotRecord2, plotRecord3'])

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be > 0')
            expect(queryStub.callCount).to.equal(6)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve plot records ' + 
            'with Variable List', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{
                'experiment_type': 'Observation',
                'experiment_design_type': 'Entry Order'
            }])
            queryStub.onCall(3).resolves([{
                'occurrence_design_type': 'Entry Order',
            }])
            queryStub.onCall(4).resolves([
                {
                    'variableId': [1, 2, 3,],
                    'crossTabColumns': 'crossTabColumn1, crossTabColumn2',
                    'selectColums': 'selectColumn1, selectColumn2',
                    'variableAbbrev': 'ABBREV_1, ABBREV_2, ABBREV_3',
                }
            ])
            getFinalSqlQueryWoLimitStub.resolves('query')
            getFinalTotalCountQueryStub.resolves('query')
            queryStub.onCall(5).resolves(['plotRecord1, plotRecord2, plotRecord3'])

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be > 0')
            expect(queryStub.callCount).to.equal(6)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve plot records ' + 
            'with fields in request body', async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    params: {
                        id: '3413'
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                    body: {
                        fields: 'some.field AS thisField'
                    }
                }

                tokenHelperStub.resolves(123)
                queryStub.onCall(0).resolves('query')
                queryStub.onCall(1).resolves([ { id: 111 }, ])
                queryStub.onCall(2).resolves([ {
                    'experiment_type': 'Observation',
                    'experiment_design_type': 'Entry Order',
                } ])
                queryStub.onCall(3).resolves([ {
                    'occurrence_design_type': 'Entry Order',
                } ])
                queryStub.onCall(4).resolves([ 'variables' ])
                getFilterStub.resolves('conditionString')
                getFinalSqlQueryWoLimitStub.resolves('query')
                getFinalTotalCountQueryStub.resolves('query')
                queryStub.onCall(5).resolves(['plotRecord1, plotRecord2, plotRecord3'])
    
                await post(req, res, next)
    
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be > 0')
                expect(queryStub.callCount).to.equal(6)
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should successfully retrieve plot records ' + 
            'with traits in request body', async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    params: {
                        id: '3413'
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                    body: {
                        'ABBREV_1': { 'dataValue': 'trait1' },
                        'ABBREV_2': { 'dataValue': 'trait2' },
                        'ABBREV_3': { 'dataValue': 'trait3' },
                    }
                }
    
                tokenHelperStub.resolves(123)
                queryStub.onCall(0).resolves('query')
                queryStub.onCall(1).resolves([ { id: 111 }, ])
                queryStub.onCall(2).resolves([ {
                    'experiment_type': 'Observation',
                    'experiment_design_type': 'Entry Order',
                } ])
                queryStub.onCall(3).resolves([ {
                    'occurrence_design_type': 'Entry Order',
                } ])
                queryStub.onCall(4).resolves([
                    {
                        'variableId': [1, 2, 3,],
                        'crossTabColumns': 'crossTabColumn1, crossTabColumn2',
                        'selectColums': 'selectColumn1, selectColumn2',
                        'variableAbbrev': 'ABBREV_1, ABBREV_2, ABBREV_3',
                    }
                ])
                getFilterStub.resolves('conditionString')
                queryStub.onCall(5).resolves([ { 'data_type': 'dataType' }])
                getFilterConditionStub.resolves('condition')
                getFinalSqlQueryWoLimitStub.resolves('query')
                getFinalTotalCountQueryStub.resolves('query')
                queryStub.onCall(6).resolves(['plotRecord1, plotRecord2, plotRecord3'])
    
                await post(req, res, next)
    
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be > 0')
                expect(queryStub.callCount).to.equal(7)
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if occurrenceDbId ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                params: {
                    id: 'notInt'
                },
                query: {}
            }

            tokenHelperStub.resolves(123)
            getErrorCodebyMessageStub.resolves(400004)
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getErrorCodebyMessageStub.calledOnce).to.be.true('getErrorCodebyMessage must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004, 'Invalid format, occurrenceDbId must be an integer')
            expect(queryStub.called).to.be.false('no query should have been called')
        })

        it('should generate BadRequestError if sort parameter ' + 
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {
                    sort: 'invalid:sorting'
                },
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{
                'experiment_type': 'Observation',
                'experiment_design_type': 'Entry Order',
            }])
            queryStub.onCall(3).resolves([{
                'occurrence_design_type': 'Entry Order',
            }])
            queryStub.onCall(4).resolves([ 'variables' ])
            getOrderStringTraitStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await post(req, res, next)

            expect(getOrderStringTraitStub.calledOnce).to.be.true('getOrderStringTrait must be successfully called once')
            expect(getFinalSqlQueryWoLimitStub.called).to.be.false('getFinalSqlQueryWoLimit must not be called')
            expect(getFinalTotalCountQueryStub.called).to.be.false('getFinalTotalCountQuery must not be called')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
            expect(queryStub.callCount).to.equal(5)
        })

        it('should generate InternalError if plotData sequelize ' + 
            'rejects', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{
                'experiment_type': 'Observation',
                'experiment_design_type': 'Entry Order',
            }])
            queryStub.onCall(3).resolves([{
                'occurrence_design_type': 'Entry Order',
            }])
            queryStub.onCall(4).resolves([ 'variables' ])
            getFinalSqlQueryWoLimitStub.resolves('query')
            getFinalTotalCountQueryStub.resolves('query')
            queryStub.onCall(5).rejects()
            getErrorResponseStub.resolves('InternalError')

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('plot records must be undefined')
            expect(queryStub.callCount).to.equal(6)
            expect(getErrorResponseStub.calledOnce).to.be.true('getErrorResponse must be called once')
        })

        it('should generate InternalError if plotDataCount sequelize ' + 
            'rejects', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{
                'experiment_type': 'Observation',
                'experiment_design_type': 'Entry Order',
            }])
            queryStub.onCall(3).resolves([{
                'occurrence_design_type': 'Entry Order',
            }])
            queryStub.onCall(4).resolves([ 'variables' ])
            getFinalSqlQueryWoLimitStub.resolves('query')
            getFinalTotalCountQueryStub.resolves('query')
            queryStub.onCall(5).rejects()
            getErrorResponseStub.resolves('InternalError')

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('getFinalTotalCountQuery must be called once')
            expect(queryStub.callCount).to.equal(6)
            expect(getErrorResponseStub.calledOnce).to.be.true('getErrorResponse must be called once')
        })

        it('should generate NotFoundError if occurrenceDbId ' +
            'does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                params: {
                    id: '0'
                },
                query: {}
            }

            tokenHelperStub.resolves(123)
            queryStub.onFirstCall().resolves([ { 'count': 0 } ])
            getErrorCodebyMessageStub.resolves(404004)
            errorBuilderStub.resolves('NotFoundError')

            await post(req, res, next)

            expect(getErrorCodebyMessageStub.calledOnce).to.be.true('getErrorCodebyMessage must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404004, 'The occurrenceDbId does not exist.')
            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should return empty rows if plotData ' + 
            'is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '3413'
                },
                paginate: {
                    limit: 100,
                    offset: 0,
                },
                query: {},
            }

            tokenHelperStub.resolves(123)
            queryStub.onCall(0).resolves('query')
            queryStub.onCall(1).resolves([ { id: 111 }, ])
            queryStub.onCall(2).resolves([{
                'experiment_type': 'Observation',
                'experiment_design_type': 'Entry Order',
            }])
            queryStub.onCall(3).resolves([{
                'occurrence_design_type': 'Entry Order',
            }])
            queryStub.onCall(4).resolves(null)
            getFinalSqlQueryWoLimitStub.resolves('query')
            queryStub.onCall(5).resolves([])

            await post(req, res, next)

            expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
            expect(queryStub.callCount).to.equal(6)
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

    })
})