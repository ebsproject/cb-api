/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')
const userValidator = require('../../helpers/person/validator.js')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
};
const next = {};

describe('Planting Instructions', () => {
    describe('/POST planting-instructions', () => {
        const { post } = require('../../routes/v3/planting-instructions/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new planting instruction record',
            async () => {

        })
    })

    describe('/PUT/:id planting-instructions', () => {
        const { put } = require('../../routes/v3/planting-instructions/_id/index');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let userValidatorStub
        let errorBuilderStub
        let transactionStub
        let queryStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            userValidatorStub = sandbox.stub(userValidator, 'isAdmin')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError if planting instruction ID is not an ' +
            'integer (in string format)', async () => {

            //arrange
            req.params = {
                id: 'notAnInteger'
            }

            //mock
            errorsStub = sandbox.stub(errors, 'BadRequestError')
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            const result = await put(req, res, next)

            //assert
            expect(queryStub.called).to.be.false('query must not be called')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid format, planting instruction ID must be an integer.')
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            
            //arrange
            req.params = {
                id: '123'
            }

            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await put(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw an InternalError (500004) when failing to retrieve ' +
            'planting instruction record', async () => {

            // arrange
            req.params = { 
                'id': '123' 
            }

            getUserIdStub.resolves(1)
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            // act
            const result = await put(req, res, next)

            // assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw an UnauthorizedError (401025) if user ' +
            'does not have a role', async () => {
            
            // arrange
            req.params = { 
                'id': '123'
            }

            getUserIdStub.resolves(1)
            userValidatorStub.resolves(false)
            errorBuilderStub.resolves('UnauthorizedError')
            queryStub.onFirstCall().resolves([{
                entryNumber : 123,
                entryName: 'Sample Entry',
                entryType: 'Type',
                entryRole: 'Role',
                entryClass: 'Class',
                entryStatus: 'Status',
                entryDbId: 123,
                creatorDbId: 123,
                germplasmDbId: 123,
                seedDbId: 123,
                packageDbId: 123,
                packageLogDbId: 123
            }])
            queryStub.onSecondCall().resolves([{}])

            // act
            const result = await put(req, res, next)

            // assert
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should throw an UnauthorizedError (401026) if user is not ' +
            'a program team member and is not a producer', async () => {
            
            // arrange
            req.params = { 
                'id': '123'
            }

            getUserIdStub.resolves(1)
            userValidatorStub.resolves(false)
            errorBuilderStub.resolves('UnauthorizedError')
            queryStub.onFirstCall().resolves([{
                entryNumber : 123,
                entryName: 'Sample Entry',
                entryType: 'Type',
                entryRole: 'Role',
                entryClass: 'Class',
                entryStatus: 'Status',
                entryDbId: 123,
                creatorDbId: 123,
                germplasmDbId: 123,
                seedDbId: 123,
                packageDbId: 123,
                packageLogDbId: 123
            }])
            queryStub.onSecondCall().resolves([{'id': 1, 'role': 0}])

            // act
            const result = await put(req, res, next)

            // assert
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
        })

        it('should successfully update a specific and existing planting ' +
            'instruction record given a planting instruction ID', async () => {
            
            // arrange
            req.params = { 
                id: '123'
            }
            req.body = {
                'entryName': 'Test Name',
                'entryNumber': '1',
                'entryClass': 'Test Class'
            }

            getUserIdStub.resolves(1)
            userValidatorStub.resolves(false)
            errorBuilderStub.resolves('InternalError')
            queryStub.onFirstCall().resolves([{
                entryNumber : 123,
                entryName: 'Sample Entry',
                entryType: 'Type',
                entryRole: 'Role',
                entryClass: 'Class',
                entryStatus: 'Status',
                entryDbId: 123,
                creatorDbId: 123,
                germplasmDbId: 123,
                seedDbId: 123,
                packageDbId: 123,
                packageLogDbId: 123
            }])
            queryStub.onSecondCall().resolves([{'id': 1, 'role': 1}])
            transactionStub.resolves([[{ id: 1 }]])

            // act
            const result = await put(req, res, next)

            // assert
            expect(queryStub.callCount).to.equal(2,'query must be called exactly twice')
            expect(transactionStub.callCount).to.equal(1,'transaction must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must not be called')
        })
    })

    describe('/DELETE/:id planting-instructions', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete a specific and existing planting ' +
            'instruction record given a planting instruction ID', async () => {

        })
    })
})