/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')
const tokenHelper = require('../../helpers/auth/token')

const {post} = require("../../routes/v3/cross-parents")

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}
const next = {}

describe('Cross-Parents', () => {
    describe('POST cross-parents', () => {
        const { post } = require('../../routes/v3/cross-parents')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let errorStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError(400009) when the request body is not set', async () => {
            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        it('should throw a BadRequestError(500004) when the records array is empty', async () => {
            //arrange
            req.body = {}

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)

        })

        it('should throw an InternalError(500001) if other errors were encountered', async () => {
            //arrange
            req.body = {
                records: {}
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should return a BadRequestError if the required parameters are missing', async () => {
            //arrange
            req.body = {
                records: [{}]
            }

            //mock
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

    })

    describe('/PUT/:id parents', () => {
        const { put } = require('../../routes/v3/cross-parents/_id/index');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError if cross-parents ID is not an ' +
            'integer (in string format)', async () => {

            //arrange
            req.params = {
                id: 'notAnInteger'
            }

            //mock
            errorBuilderStub.resolves('BadRequestError')
            
            //act
            const result = await put(req, res, next)

            //assert
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must be called exactly exactly once')
            expect(queryStub.called).to.be.false('query must not be called')

        })
    })
})
