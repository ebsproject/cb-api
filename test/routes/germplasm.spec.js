/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai');
const processQueryHelper = require('../../helpers/processQuery/index')
const errorBuilder = require('../../helpers/error-builder');
const tokenHelper = require('../../helpers/auth/token');
const { sequelize } = require('../../config/sequelize');

const req = {
    headers: {
        host: 'testHost'
    },
    query: {},
    paginate: {
        limit: 1,
        offset: 1
    }
}
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
};

const next = {};

// Mock the transaction invoked after querying the database
const transactionMock = {
    finished: "",
    commit: function () {
        this.finished = 'commit'
    },
    rollback: function () {
        this.finished = 'rollback'
    },
    // Call this function in afterEach() to reset the value
    // of the property "finished"
    reset: function () {
        this.finished = ''
    }
}

describe('Germplasm', () => {
    const { post, get } = require('../../routes/v3/germplasm/index.js')

    describe('/GET germplasm', () => {
        const sandbox = sinon.createSandbox()

        let getUserIdStub
        let transactionStub
        let errorBuilderStub
        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let getCountFinalSqlQueryStub
        let queryStub


        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilterString')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            queryStub = sandbox.stub(sequelize, 'query')

        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw BadRequestError if conditionString is invalid', async () => {
            // arrange
            req.query = { germplasm: 'invalid' }

            getFilterStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            // act
            let actual = await get(req, res, next)

            // assert
            expect(getFilterStub.calledOnce).to.be.true('getFilter has been called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should throw BadRequestError if orderString is invalid', async () => {
            // arrange
            req.query = {
                germplasm: 'hellow world',
                sort: 'invalid'
            }

            getFilterStub.resolves('hello world')
            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            // act
            let actual = await get(req, res, next)

            // assert
            expect(getFilterStub.calledOnce).to.be.true('getFilter has been called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString has been called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should throw InternalError if query fails', async () => {
            // arrange
            req.query = {
                germplasm: 'hellow world',
                sort: 'id: ASC'
            }

            getFilterStub.resolves('hello world')
            getOrderStringStub.resolves('ORDER BY id ASC')
            getFinalSqlQueryStub.resolves('query')
            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            // act
            let actual = await get(req, res, next)

            // assert
            expect(getFilterStub.calledOnce).to.be.true('getFilter has been called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString has been called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery has been called')
            expect(queryStub.calledOnce).to.be.true('queryStub has been called')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw InternalError if count query fails', async () => {
            // arrange
            req.query = {
                germplasm: 'hellow world',
                sort: 'id: ASC'
            }

            getFilterStub.resolves('hello world')
            getOrderStringStub.resolves('ORDER BY id ASC')
            getFinalSqlQueryStub.resolves('query')
            queryStub.onFirstCall().resolves([{ 'id': 1 }])
            getCountFinalSqlQueryStub.resolves('count query')
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            // act
            let actual = await get(req, res, next)

            // assert
            expect(getFilterStub.calledOnce).to.be.true('getFilter has been called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString has been called once')
            expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery has been called')
            expect(queryStub.calledTwice).to.be.true('queryStub has been called twice')
            expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery has been called')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully retrieve information of accessible germplasm ' +
            'records', async () => {
                // arrange
                req.query = {
                    germplasm: 'hellow world',
                    sort: 'id: ASC'
                }

                getFilterStub.resolves('hello world')
                getOrderStringStub.resolves('ORDER BY id ASC')
                getFinalSqlQueryStub.resolves('query')
                queryStub.resolves([[{ 'id': '1' }]])
                getCountFinalSqlQueryStub.resolves('count query here')

                // act
                let actual = await get(req, res, next)

                // assert
                expect(getFilterStub.calledOnce).to.be.true('getFilter has been called once')
                expect(getOrderStringStub.calledOnce).to.be.true('getOrderString has been called once')
                expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery has been called')
                expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('getCountFinalSqlQuery has been called')
                expect(queryStub.called).to.be.true('queryStub has been called')
            })
    })

    describe('/GET/:id germplasm', () => {
        const { get } = require('../../routes/v3/germplasm/_id/index')
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of a specific and ' +
            'existing germplasm record given a germplasm ID', async () => {

            })
    })

    describe('/GET/:id/names germplasm', () => {
        const { get } = require('../../routes/v3/germplasm/_id/names')
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully retrieve information of accessible name ' +
            'records of a specific and existing germplasm record given a ' +
            'germplasm ID', async () => {

            })
    })

    describe('/GET/:id/seeds germplasm', () => {
        const { get } = require('../../routes/v3/germplasm/_id/seeds')
        const sandbox = sinon.createSandbox()

        let getOrderStringStub
        let getFinalSqlQueryStub
        let getCountFinalSqlQuery
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            getCountFinalSqlQuery = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve information of accessible seed ' +
            'records of a specific and existing germplasm record given a ' +
            'germplasm ID', async () => {

                let req = {
                    headers: {},
                    params: {
                        id: "1"
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    },
                    query: {
                        sort: 'sort'
                    }
                };

                queryStub.onFirstCall().resolves([{ 'count': 1 }])
                getOrderStringStub.resolves(['sort'])
                getFinalSqlQueryStub.resolves('query')
                queryStub.onSecondCall().resolves(['germplasm-seed 1', 'germplasm-seed 2'])
                getCountFinalSqlQuery.resolves('totalCountQuery')
                queryStub.onThirdCall().resolves([{ count: 2 }])

                const result = await get(req, res, next)

                expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
                expect(getCountFinalSqlQuery.calledOnce).to.be.true('getCountFinalSqlQuery must be successfully called once')
                expect(queryStub.calledThrice).to.be.true('query must be called exactly 3 times')
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
            })

        it('should successfully retrieve information on sorted and limited number of accessible seed ' +
            'records of a specific and existing germplasm record given a ' +
            'germplasm ID', async () => {

                let req = {
                    headers: {},
                    params: {
                        id: "1"
                    },
                    paginate: {
                        limit: 1,
                        offset: 0
                    },
                    query: {
                        sort: 'attr:ASC'
                    }
                };

                queryStub.onFirstCall().resolves([{ 'count': 1 }])
                getOrderStringStub.resolves(['sort'])
                getFinalSqlQueryStub.resolves('query')
                queryStub.onSecondCall().resolves(['germplasm-seed 1'])
                getCountFinalSqlQuery.resolves('totalCountQuery')
                queryStub.onThirdCall().resolves([{ count: 1 }])

                const result = await get(req, res, next)

                expect(getFinalSqlQueryStub.calledOnce).to.be.true('getFinalSqlQuery must be successfully called once')
                expect(getCountFinalSqlQuery.calledOnce).to.be.true('getCountFinalSqlQuery must be successfully called once')
                expect(queryStub.calledThrice).to.be.true('query must be called exactly 3 times')
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
            })

        it('should throw InternalError if validation query fails', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'sort'
                }
            };

            queryStub.onFirstCall().rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw NotFoundError if no germplasm record matches given ID', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'sort'
                }
            };

            queryStub.onFirstCall().resolves([{ 'count': 0 }])
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly 1 time')
        })

        it('should throw BadRequestError if sort provided is invalid', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'invalid sort'
                }
            };

            queryStub.onFirstCall().resolves([{ 'count': 1 }])
            getOrderStringStub.resolves(['invalid'])
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called exactly 1 time')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should throw InternalError if main query fails', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'sort'
                }
            };

            queryStub.onFirstCall().resolves([{ 'count': 1 }])
            getOrderStringStub.resolves(['sort'])
            getFinalSqlQueryStub.resolves('query')
            queryStub.onSecondCall().rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called exactly 2 times')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw InternalError if totalCount query fails', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'sort'
                }
            };

            queryStub.onFirstCall().resolves([{ 'count': 1 }])
            getOrderStringStub.resolves(['sort'])
            getFinalSqlQueryStub.resolves('query')
            queryStub.onSecondCall().resolves(['germplasm-seed 1', 'germplasm-seed 2'])
            getCountFinalSqlQuery.resolves('totalCountQuery')
            queryStub.onThirdCall().rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res, next)

            expect(queryStub.calledThrice).to.be.true('query must be called exactly 3 times')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw BadRequestError if germplasm Id provided is invalid', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'sort'
                }
            };


            const result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(0, 'no queries called')
        })
    })

    describe('/GET/:id/attributes germplasm', () => {
        const { get } = require('../../routes/v3/germplasm/_id/attributes')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when' +
            ' the germplasm ID is not an integer', async () => {
                req.params = {
                    id: '123a'
                }

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(0, 'code does not use errorBuilder so no need to be called')
            })

        it('should generate an InternalError when' +
            ' germplasm retrieval fails', async () => {
                req.params = {
                    id: '123'
                }

                queryStub.onCall(0).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should generate an InternalError when' +
            ' the germplasm attribute retrieval fails', async () => {
                req.params = {
                    id: '123'
                }

                queryStub.onCall(0).resolves([{ germplasmDbId: 123 }])
                queryStub.onCall(1).resolves([{ attributes: [] }])
                queryStub.onCall(2).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')
                errorBuilderStub.onCall(1).resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(2, 'errorBuilder must be called exactly twice')
                expect(queryStub.callCount).to.be.equals(3, 'query must be called exactly thrice')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should successfully retrieve the germplasm attributes', async () => {
            req.params = {
                id: '123'
            }

            queryStub.onCall(0).resolves([{ germplasmDbId: 123 }])
            queryStub.onCall(1).resolves([{ attributes: [] }])
            queryStub.onCall(2).resolves([{ count: 1 }])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equals(3, 'query must be called exactly thrice')
        })
    })

    describe('/GET/:id/event-logs germplasm', () => {
        const { get } = require('../../routes/v3/germplasm/_id/event-logs')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when' +
            ' the germplasm ID is not an integer', async () => {
                req.params = {
                    id: '123a'
                }

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(0, 'code does not use errorBuilder so no need to be called')
            })

        it('should generate an InternalError when' +
            ' the germplasm event log retrieval fails', async () => {
                req.params = {
                    id: '123'
                }

                queryStub.onCall(0).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should successfully retrieve the germplasm event logs', async () => {
            req.params = {
                id: '123'
            }

            queryStub.onCall(0).resolves([{ germplasmEventLog: {} }])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
        })
    })

    describe('/GET germplasm/:id/entries-count', () => {
        const { get } = require('../../routes/v3/germplasm/_id/entries-count')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when' +
            ' the germplasm ID is not an integer', async () => {
                req.params = {
                    id: '123a'
                }

                errorBuilderStub.onCall(0).resolves('BadRequestError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400238)
            })

        it('should generate an InternalError when' +
            ' germplasm retrieval fails', async () => {
                req.params = {
                    id: '123'
                }

                queryStub.onCall(0).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should generate a NotFoundError when' +
            ' the germplasm does not exist', async () => {
                req.params = {
                    id: '9999999999'
                }

                queryStub.onCall(0).resolves([])
                errorBuilderStub.onCall(0).resolves('NotFoundError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404046)
            })

        it('should generate an InternalError when' +
            ' the entry count retrieval fails', async () => {
                req.params = {
                    id: '123'
                }

                queryStub.onCall(0).resolves([{ germplasmDbId: 123 }])
                queryStub.onCall(1).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(2, 'query must be called exactly twice')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should successfully retrieve the entries count', async () => {
            req.params = {
                id: '123'
            }

            queryStub.onCall(0).resolves([{ germplasmDbId: 123 }])
            queryStub.onCall(1).resolves([{ count: 1 }])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equals(2, 'query must be called exactly twice')
        })
    })

    describe('/GET germplasm/:id/seeds-count', () => {
        const { get } = require('../../routes/v3/germplasm/_id/seeds-count')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when' +
            ' the germplasm ID is not an integer', async () => {
                req.params = {
                    id: '123a'
                }

                errorBuilderStub.onCall(0).resolves('BadRequestError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400238)
            })

        it('should generate an InternalError when' +
            ' germplasm retrieval fails', async () => {
                req.params = {
                    id: '123'
                }

                queryStub.onCall(0).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should generate a NotFoundError when' +
            ' the germplasm does not exist', async () => {
                req.params = {
                    id: '9999999999'
                }

                queryStub.onCall(0).resolves([])
                errorBuilderStub.onCall(0).resolves('NotFoundError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404046)
            })

        it('should generate an InternalError when' +
            ' the seed count retrieval fails', async () => {
                req.params = {
                    id: '123'
                }

                queryStub.onCall(0).resolves([{ germplasmDbId: 123 }])
                queryStub.onCall(1).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                const result = await get(req, res, next)

                expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
                expect(queryStub.callCount).to.be.equals(2, 'query must be called exactly twice')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('should successfully retrieve the seeds count', async () => {
            req.params = {
                id: '123'
            }

            queryStub.onCall(0).resolves([{ germplasmDbId: 123 }])
            queryStub.onCall(1).resolves([{ count: 1 }])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equals(2, 'query must be called exactly twice')
        })
    })

    describe('POST germplasm/:id/delete-family-members', () => {
        const { post } = require('../../routes/v3/germplasm/_id/delete-family-members')
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub
        let transactionStub
        let tokenHelperStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
            transactionMock.reset()
        })

        it('should generate a BadRequestError when the germplasm id is not an integer', async () => {
            req.params = {
                id: '1a'
            }

            errorBuilderStub.onCall(0).resolves('BadRequestError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(0, 'query must NOT be called')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(0, 'tokenHelper must NOT be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400246)
        })

        it('should generate an UnauthorizedError when the person id is null', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(null)
            errorBuilderStub.onCall(0).resolves('UnauthorizedError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(0, 'query must NOT be called')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate an InternalError when the germplasm retrieval fails', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the germplasm id does not exist', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: false }])
            errorBuilderStub.onCall(0).resolves('NotFoundError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404042)
        })

        it('should generate an InternalError when the seed retrieval fails', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the seed does not exist', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).resolves([{ exists: true }])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })

        it('should generate an InternalError when the family member retrieval fails', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).resolves([{ exists: false }])
            queryStub.onCall(2).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly three times')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the seed does not exist', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).resolves([{ exists: false }])
            queryStub.onCall(2).resolves([])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly three times')
            expect(transactionStub.callCount).to.equal(0, 'transaction must NOT be called')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })

        it('should successfully delete the family member record', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).resolves([{ exists: false }])
            queryStub.onCall(2).resolves([{ familyDbId: 456 }])
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(3).resolves([[{ id: 333 }]])
            queryStub.onCall(4).resolves([{ exists: true }])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(5, 'query must be called exactly five times')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called exactly once')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })

        it('should generate an InternalError when the family member check fails', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).resolves([{ exists: false }])
            queryStub.onCall(2).resolves([{ familyDbId: 456 }])
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(3).resolves([[{ id: 333 }]])
            queryStub.onCall(4).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(5, 'query must be called exactly five times')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called exactly once')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError when the family member check fails', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).resolves([{ exists: false }])
            queryStub.onCall(2).resolves([{ familyDbId: 456 }])
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(3).resolves([[{ id: 333 }]])
            queryStub.onCall(4).resolves([{ exists: false }])
            transactionMock.reset();
            transactionStub.onCall(1).resolves(transactionMock)
            queryStub.onCall(5).resolves([[{ id: 456 }]])

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(6, 'query must be called exactly six times')
            expect(transactionStub.callCount).to.equal(2, 'transaction must be called exactly twice')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })

        it('should generate an InternalError when transaction creation fails', async () => {
            req.params = {
                id: '1'
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([{ exists: true }])
            queryStub.onCall(1).resolves([{ exists: false }])
            queryStub.onCall(2).resolves([{ familyDbId: 456 }])
            transactionStub.onCall(0).rejects(transactionMock)
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(3, 'query must be called exactly three times')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called exactly once')
            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })

    describe('POST germplasm', () => {
        const sandbox = sinon.createSandbox()

        let getUserIdStub
        let transactionStub
        let errorBuilderStub
        let queryStub

        beforeEach(function () {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            // transactionStub = sandbox.stub(sequelize, 'transaction')
            transactionStub = sandbox.stub(sequelize, 'transaction').callsFake(() => ({
                commit: sinon.stub().resolves(),
                rollback: sinon.stub().resolves()
            }))
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(function () {
            sandbox.restore()
        })

        it('should throw BadRequestError if has invalid user ID', async () => {
            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should return BadRequestError if request body is not provided', async () => {
            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should return InternalError if error encountered with data provided', async () => {
            // assign
            req['body'] = {}

            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')

            // act
            await post(req, res, next)

            // assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should return InternalError if error encountered with records provided', async () => {
            // assign
            req['body'] = { records: [] }

            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // act
            await post(req, res, next)

            // assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        it('should return BadRequestError if records have missing required parameters', async () => {
            // assign
            req.body = {
                records: [{
                    designation: "designation",
                    parentage: "parentage",
                    generation: "generation",
                    state: "state",
                    nameType: "nameType",
                    cropDbId: "2",
                    taxonomyDbId: "1"
                }]
            }

            getUserIdStub.resolves(1)
            transactionStub.resolves({
                rollback: sinon.stub(),
            })
            errorBuilderStub.resolves('BadRequestError')

            // act
            await post(req, res, next)

            // assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })

        it('should return BadRequestError if records have invalid cropDbId parameters', async () => {
            // assign
            req.body = {
                records: [{
                    designation: "designation",
                    parentage: "parentage",
                    generation: "generation",
                    state: "state",
                    nameType: "nameType",
                    cropDbId: "cropDbId",
                    taxonomyDbId: "1"
                }]
            }

            getUserIdStub.resolves(1)
            transactionStub.resolves({
                rollback: sinon.stub(),
            })
            errorBuilderStub.resolves('BadRequestError')

            // act
            await post(req, res, next)

            // assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })

        it('should return BadRequestError if records have invalid taxonomyDbId parameters', async () => {
            // assign
            req.body = {
                records: [{
                    designation: "designation",
                    parentage: "parentage",
                    generation: "generation",
                    state: "state",
                    nameType: "nameType",
                    cropDbId: "2",
                    taxonomyDbId: "taxonomyDbId"
                }]
            }

            getUserIdStub.resolves(1)
            transactionStub.resolves({
                rollback: sinon.stub(),
            })
            errorBuilderStub.resolves('BadRequestError')

            // act
            await post(req, res, next)

            // assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })

        it('should return BadRequestError if records have invalid productProfileDbId parameters', async () => {
            // assign
            req.body = {
                records: [{
                    designation: "designation",
                    parentage: "parentage",
                    generation: "generation",
                    state: "state",
                    nameType: "nameType",
                    cropDbId: "2",
                    taxonomyDbId: "1",
                    productProfileDbId: "productProfileDbId"
                }]
            }

            getUserIdStub.resolves(1)
            transactionStub.resolves({
                rollback: sinon.stub(),
            })
            errorBuilderStub.resolves('BadRequestError')

            // act
            await post(req, res, next)

            // assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })

        it('should return InternalError if record creation fails', async () => {
            // assign
            req.body = {
                records: [{
                    designation: "designation",
                    parentage: "parentage",
                    generation: "generation",
                    state: "state",
                    nameType: "nameType",
                    cropDbId: "2",
                    taxonomyDbId: "1",
                    productProfileDbId: "3"
                }]
            }

            getUserIdStub.resolves(1)
            queryStub.onFirstCall().rejects()
            transactionStub.resolves({
                rollback: sinon.stub(),
            })
            errorBuilderStub.resolves('InternalError')

            // act
            await post(req, res, next)

            // assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
        })
    })
})