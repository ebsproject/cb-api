/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const processQueryHelper = require('../../helpers/processQuery/index')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const { expect } = require('chai')

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Taxonomies', () => {
    const { get } = require('../../routes/v3/taxonomies/index');

    describe('/GET taxonomies', () => {
        const sandbox = sinon.createSandbox()

        let getFilterStub
        let getOrderStringStub
        let getFinalSqlQueryStub
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            getFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getFinalSqlQuery')
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve all taxonomy records', async () => {
            let req = {
                headers: {
                    host: 'testHost'
                },
                query: {},
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            let data = [ {'id': 1, 'taxonId':'testValue'} ]

            getFinalSqlQueryStub.onCall(0).resolves('')
            queryStub.onCall(0).resolves(data)
            queryStub.onCall(1).resolves([{count:1}])

            const result = await get(req, res, next)

            expect(getFinalSqlQueryStub.calledOnce).to.be.true('generate final sql query once')
            expect(queryStub.callCount).to.be.equal(2, 'call query twice')
            expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder was not called')
        })

        it('should successfully retrieve all taxonomy records according to sort order identified', 
            async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        sort: 'taxonId:DESC'
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                let data = [ 
                    {'id': 2, 'taxonId':'testValue2'},
                    {'id': 1, 'taxonId':'testValue1'} 
                ]

                getOrderStringStub.resolves('ORDER BY taxonId DESC')
                getFinalSqlQueryStub.resolves('')
                queryStub.onCall(0).resolves(data)
                queryStub.onCall(1).resolves([{count:2}])

                const result = await get(req, res, next)

                expect(getOrderStringStub.calledOnce).to.be.true('generate order string once')
                expect(getFinalSqlQueryStub.calledOnce).to.be.true('generate final sql query once')
                expect(queryStub.callCount).to.be.equal(2, 'call query twice')
                expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder was not called')
        })

        it('should successfully retrieve all taxonomy record for matching taxonId', 
            async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        taxonId: 5
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                let data = [ {'id': 2, 'taxonId':'testValue'} ]

                queryStub.onFirstCall().resolves([{count:1}])
                getFilterStub.resolves('taxonId = 5')
                getFinalSqlQueryStub.resolves('')
                queryStub.onSecondCall().resolves(data)
                queryStub.onThirdCall().resolves([{count:1}])

                const result = await get(req, res, next)

                expect(getFilterStub.calledOnce).to.be.true('generate search filter once')
                expect(getFinalSqlQueryStub.calledOnce).to.be.true('generate final sql query once')
                expect(queryStub.callCount).to.be.equal(3, 'call query thrice')
                expect(errorBuilderStub.callCount).to.be.equal(0,'errorBuilder was not called')
        })

        it('should generate a BadRequestError if an invalid sort order was identified',
            async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        sort: 'INVALID'
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                getOrderStringStub.resolves('invalid')
                errorBuilderStub.resolves('BadRequestError')                

                const result = await get(req, res, next)

                expect(getOrderStringStub.calledOnce).to.be.true('generate order string once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder was called') 
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)                               
        })

        it('should generate an InternalError if an invalid taxonId was provided',
            async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        taxonId: ''
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                errorBuilderStub.resolves('BadRequestError')                

                const result = await get(req, res, next)

                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder was called') 
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)                               
        })

        it('should generate an InternalError if the retrieval of record that matches the given ' + 
            'taxonId fails', async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        taxonId: 5
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.rejects()
                errorBuilderStub.resolves('InternalError')                

                const result = await get(req, res, next)

                expect(queryStub.calledOnce).to.be.true('query was called once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder was called') 
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)                               
        })

        it('should generate a BadRequestError if no record matches the given taxonId', 
            async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        taxonId: 5
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.resolves([{count:0}])                

                const result = await get(req, res, next)

                expect(queryStub.calledOnce).to.be.true('query was called once')                          
        })

        it('should generate a BadRequestError if generated filter is invalid', 
            async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        taxonId: 5
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves([{count:1}])
                getFilterStub.resolves('invalid')
                errorBuilderStub.resolves('BadRequestError')

                const result = await get(req, res, next)

                expect(queryStub.calledOnce).to.be.true('query was called once')
                expect(getFilterStub.calledOnce).to.be.true('generate search filter once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder was called')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate a InternalError if retrieval of taxonomies fail', 
            async () => {
                let req = {
                    headers: {
                        host: 'testHost'
                    },
                    query: {
                        taxonId: 5
                    },
                    paginate: {
                        limit: 100,
                        offset: 0
                    }
                }

                queryStub.onFirstCall().resolves([{count:1}])
                getFilterStub.resolves('testFilter')
                getFinalSqlQueryStub.resolves('testQuery')
                queryStub.onSecondCall().rejects()
                errorBuilderStub.resolves('BadRequestError')

                const result = await get(req, res, next)

                expect(queryStub.calledTwice).to.be.true('query was called once')
                expect(getFinalSqlQueryStub.calledOnce).to.be.true('fine query was generated')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder was called')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })
})