/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../../config/sequelize.js')
const errorBuilder = require('../../../../helpers/error-builder.js')
const tokenHelper = require('../../../../helpers/auth/token.js')
const logger = require('../../../../helpers/logger/index.js')
const experimentHelper = require('../../../../helpers/experiment/index.js')
const validator = require('validator')
const userValidator = require('../../../../helpers/person/validator.js')
const { expect } = require('chai')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Experiments', () => {
    describe('PUT experiments/:id', () => {
        const { put } = require('../../../../routes/v3/experiments/_id/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let logMessageStub
        let logFailingQueryStub
        let getPatternStub
        let generateCodeStub
        let isIntStub
        let isAdminStub

        //stub declaration based on existing functions within put
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            getPatternStub = sandbox.stub(experimentHelper, 'getPattern')
            generateCodeStub = sandbox.stub(experimentHelper, 'generateCode')
            isIntStub = sandbox.stub(validator, 'isInt')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (400025) when experimentDbId is not an integer', async () => {
            // Arrange
            req['params'] = { 'id': 'notInt' }

            isIntStub.returns(false)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await put(req, res, next)
            
            // Assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400025)
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await put(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw an InternalError (500004) when retrieving experiment record ' +
            'from database fails', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.rejects()
            logFailingQueryStub.resolves('query error')
            errorBuilderStub.resolves('InternalError')

            // Act
            await put(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(logFailingQueryStub.calledOnce).to.be.true('logFailingQuery must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw a NotFoundError (404008) when retrieving experiment record ' +
            'from database returns empty', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([])
            errorBuilderStub.resolves('NotFoundError')

            // Act
            await put(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404008)
        })

        /**
         * Iterate on test cases wherein
         * either person ID or role is undefined
        */
        const personInfoArray = {
            'id': {
                'id': undefined,
                'role': 'role',
            },
            'role': {
                'id': 123,
                'role': undefined,
            }
        }

        for (const personInfo of Object.entries(personInfoArray)) {
            it(`should throw an UnauthorizedError (401025) when person ${personInfo[0]} is undefined`, async () => {
                // Arrange
                req['params'] = { 'id': '123' }
    
                isIntStub.returns(true)
                getUserIdStub.resolves(1)
                isAdminStub.resolves(false)
                queryStub.resolves([{
                    'recordCreatorDbId': '456'
                }])
                queryStub.resolves([ personInfo[1] ])
                errorBuilderStub.resolves('UnauthorizedError')
    
                // Act
                await put(req, res, next)
    
                // Assert
                expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401025)
            })
        }

        it('should throw an UnauthorizedError (401026) when person is ' +
            'neither a program team member nor do they have a producer role', async () => {
            // Arrange
            req['params'] = { 'id': '1' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1) // represents personDbId
            isAdminStub.resolves(false)
            queryStub.resolves([{
                'recordCreatorDbId': '999'
            }])
            queryStub.resolves([{
                'id': 123, // should be == personDbId
                'role': '1', // should be == '26'
            }])
            errorBuilderStub.resolves('UnauthorizedError')

            // Act
            await put(req, res, next)

            // Assert
            expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401026)
        })

        /**
         * Iterate on test cases wherein either:
         * person[0].id != personDbId; OR
         * person[0].role != '26'
        */
        const testCaseArray = {
            'person is not a program team member': {
                'id': 123, // should be == personDbId
                'role': '26',
            },
            'person does not have a producer role': {
                'id': 1,
                'role': '1', // should be == '26'
            },
        }

        for (const testCase of Object.entries(testCaseArray)) {
            it(`should throw a BadRequestError (400009) when ${testCase[0]} and ` + 
                `the request body is not set`, async () => {
                // Arrange
                req['params'] = { 'id': '1' }
                req.body = undefined

                isIntStub.returns(true)
                getUserIdStub.resolves(1) // represents personDbId
                isAdminStub.resolves(false)
                queryStub.resolves([{
                    'recordCreatorDbId': '999'
                }])
                queryStub.resolves([ testCase[1] ])
                errorBuilderStub.resolves('BadRequestError')

                // Act
                await put(req, res, next)

                // Assert
                expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            })
        }

        /**
         * Iterate on test cases wherein an input for a parameter
         * is invalid
        */
        const attributes = {
            'dataProcessDbId': 400257,
            'stageDbId': 400251,
            'projectDbId': 400056,
            'pipelineDbId': 400250,
            'experimentYear': 400252,
            'seasonDbId': 400063,
            'programDbId': 400125,
            'stewardDbId': 400253,
        }

        for (const attribute of Object.entries(attributes)) {
            it(`should throw a BadRequestError (${attribute[1]}) if the data type ` +
            `of ${attribute[0]} is invalid`, async () => {
                // Arrange
                req['params'] = { 'id': '1' }
                req.body = { [attribute[0]]: 'invalid' }

                isIntStub.onCall(0).returns(true)
                getUserIdStub.resolves(1)
                isAdminStub.resolves(true)
                queryStub.resolves([{ 'recordAttribute': 'value' }])
                isIntStub.onCall(1).returns(false)
                errorBuilderStub.resolves('BadRequestError')

                // Act
                await put(req, res, next)

                // Assert
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', attribute[1])
                })
        }

        it('should throw a BadRequestError (400254) if experimentYear ' +
            `is not in format of YYYY`, async () => {
            // Arrange
            req['params'] = { 'id': '1' }
            req.body = { 'experimentYear': '20240' }

            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{ 'recordAttribute': 'value' }])
            isIntStub.onCall(1).returns(true)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await put(req, res, next)

            // Assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400254)
        })

        it(`should throw a BadRequestError (400258) if generateExperimentName ` +
            `is present BUT there are no updates to ` +
            `neither stageDbId, seasonDbId, nor expreimentYear`, async () => {
            // Arrange
            req['params'] = { 'id': '1' }
            req.body = { 'generateExperimentName': 'true' }

            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{ 'recordAttribute': 'value' }])
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await put(req, res, next)

            // Assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400258)
        })
            

        it('should throw a BadRequestError (400015) if inputCount ' +
            'does not equal validateCount', async () => {
            // Arrange
            req['params'] = { 'id': '1' }
            req.body = { 'dataProcessDbId': '123' }

            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.onCall(0).resolves([{ 'recordAttribute': 'value' }])
            isIntStub.onCall(1).returns(true)
            queryStub.onCall(1).resolves([{ 'count': 2 }])
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await put(req, res, next)

            // Assert
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        /**
         * Test different successful update cases
        */
        const updateTestCaseMap = {
            'should successfully update fields of an experiment': {
                'updateAttributes': {
                    'dataProcessDbId': '123',
                    'stageDbId': '123',
                    'projectDbId': '123',
                    'pipelineDbId': '123',
                    'experimentYear': '2024',
                    'seasonDbId': '123',
                    'programDbId': '123',
                    'stewardDbId': '123',
                    'experimentObjective': 'experimentObjective',
                    'experimentDesignType': 'experimentDesignType',
                    'experimentSubType': 'experimentSubType',
                    'experimentSubSubType': 'experimentSubSubType',
                    'description': 'description',
                    'remarks': 'remarks',
                    'plantingSeason': 'plantingSeason',
                    'experimentStatus': 'experimentStatus',
                    'notes': 'notes',
                    'experimentName': 'experimentName',
                },
                'recordAttributes': {
                    'creatorDbId': '1',
                    'projectDbId': '456',
                    'pipelineDbId': '456',
                    'stageDbId': '456',
                    'experimentObjective': 'recordExperimentObjective',
                    'experimentSubType': 'recordExperimentSubType',
                    'experimentSubSubType': 'recordExperimentSubSubType',
                    'experimentYear': 'recordExperimentYear',
                    'seasonDbId': '456',
                    'programDbId': '456',
                    'stewardDbId': '456',
                    'description': 'recordDescription',
                    'remarks': 'recordRemarks',
                    'plantingSeason': 'recordPlantingSeason',
                    'experimentStatus': 'recordExperimentStatus',
                    'experimentDesignType': 'recordExperimentDesignType',
                    'dataProcessDbId': '456',
                    'notes': 'recordNotes',
                    'experimentName': 'recordExperimentName',
                },
                'validateCount': 7,
            },
            'should successfully update projectDbId to "null"': {
                'updateAttributes': {
                    'projectDbId': 'null',
                },
                'recordAttributes': {
                    'projectDbId': '456',
                },
                'validateCount': 1,
            },
            'should successfully update pipelineDbId to "null"': {
                'updateAttributes': {
                    'pipelineDbId': 'null',
                },
                'recordAttributes': {
                    'pipelineDbId': '456',
                },
                'validateCount': 1,
            },
            'should successfully update notes to null': {
                'updateAttributes': {
                    'notes': null,
                },
                'recordAttributes': {
                    'notes': 'notes',
                },
                'validateCount': 1,
            },
            'should successfully generate a new experiment name given I updated stageDbId': {
                'updateAttributes': {
                    'stageDbId': '123',
                    'generateExperimentName': 'true',
                },
                'recordAttributes': {
                    'stageDbId': '456',
                },
                'validateCount': 1,
            },
            'should successfully generate a new experiment name given I updated seasonDbId': {
                'updateAttributes': {
                    'seasonDbId': '123',
                    'generateExperimentName': 'true',
                },
                'recordAttributes': {
                    'seasonDbId': '456',
                },
                'validateCount': 1,
            },
            'should successfully generate a new experiment name given I updated experimentYear': {
                'updateAttributes': {
                    'experimentYear': '2024',
                    'generateExperimentName': 'true',
                },
                'recordAttributes': {
                    'experimentYear': '2023',
                },
                'validateCount': 1,
            },
        }

        for (const updateTestCase of Object.entries(updateTestCaseMap)) {
            it(updateTestCase[0], async () => {
                // Arrange    
                req['params'] = { 'id': '1' }
                req.body = updateTestCase[1]['updateAttributes']
    
                isIntStub.onCall(0).returns(true)
                getUserIdStub.resolves(1)
                isAdminStub.resolves(true)
                queryStub.onCall(0).resolves([ updateTestCase[1]['recordAttributes'] ])
    
                for (let i = 1; i <= 8; ++i) {
                    isIntStub.onCall(i).returns(true)
                }
    
                if (updateTestCase[1]['updateAttributes']['generateExperimentName'] == 'true') {
                    getPatternStub.resolves('pattern')
                    generateCodeStub.resolves('experimentName')
                }

                queryStub.onCall(1).resolves([{ 'count': updateTestCase[1]['validateCount'] }])
                transactionStub.resolves([{}])
    
                // Act
                await put(req, res, next)
    
                // Assert
                if (updateTestCase[1]['updateAttributes']['generateExperimentName'] == 'true') {
                    expect(getPatternStub.calledOnce).to.be.true('getPattern should be called exactly once')
                    expect(generateCodeStub.calledOnce).to.be.true('generateCode should be called exactly once')
                }
                expect(transactionStub.calledOnce).to.be.true('transaction should be called exactly once')
            })
        }

        it('should throw an InternalError (500003) if other errors were encountered', async () => {
            // Arrange
            req['params'] = { 'id': '1' }
            req.body = { 'dataProcessDbId': 'invalid' }

            isIntStub.onCall(0).returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{ 'recordAttribute': 'value' }])
            isIntStub.onCall(1).throws(new Error)
            logMessageStub.resolves('err')
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await put(req, res, next)

            // Assert
            expect(logMessageStub.calledOnce).to.be.true('logMessage should be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500003)
        })
    })

    describe('DELETE experiments/:id', () => {
        const { delete: del } = require('../../../../routes/v3/experiments/_id/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let logMessageStub
        let logFailingQueryStub
        let isIntStub
        let isAdminStub

        //stub declaration based on existing functions within put
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            logFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            isIntStub = sandbox.stub(validator, 'isInt')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (400025) when experimentDbId is not an integer', async () => {
            // Arrange
            req['params'] = { 'id': 'notInt' }

            isIntStub.returns(false)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await del(req, res, next)
            
            // Assert
            expect(isIntStub.calledOnce).to.be.true('isInt must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400025)
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await del(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw an InternalError (500004) when retrieving experiment record ' +
            'from database fails', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.rejects()
            logFailingQueryStub.resolves('query error')
            errorBuilderStub.resolves('InternalError')

            // Act
            await del(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(logFailingQueryStub.calledOnce).to.be.true('logFailingQuery must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should throw a NotFoundError (404008) when retrieving experiment record ' +
            'from database returns empty', async () => {
            // Arrange
            req['params'] = { 'id': '123' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([])
            errorBuilderStub.resolves('NotFoundError')

            // Act
            await del(req, res, next)

            // Assert
            expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404008)
        })

        /**
         * Iterate on test cases wherein
         * either person ID or role is undefined
        */
        const personInfoArray = {
            'id': {
                'id': undefined,
                'role': 'role',
            },
            'role': {
                'id': 123,
                'role': undefined,
            }
        }

        for (const personInfo of Object.entries(personInfoArray)) {
            it(`should throw an UnauthorizedError (401025) when person ${personInfo[0]} is undefined`, async () => {
                // Arrange
                req['params'] = { 'id': '123' }
    
                isIntStub.returns(true)
                getUserIdStub.resolves(1)
                isAdminStub.resolves(false)
                queryStub.resolves([{
                    'recordCreatorDbId': 456
                }])
                queryStub.resolves([ personInfo[1] ])
                errorBuilderStub.resolves('UnauthorizedError')
    
                // Act
                await del(req, res, next)
    
                // Assert
                expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401025)
            })
        }

        it('should throw an UnauthorizedError (401026) when person is ' +
            'neither a program team member nor do they have a producer role', async () => {
            // Arrange
            req['params'] = { 'id': '1' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1) // represents personDbId
            isAdminStub.resolves(false)
            queryStub.resolves([{
                'recordCreatorDbId': 999
            }])
            queryStub.resolves([{
                'id': 123, // should be == personDbId
                'role': '1', // should be == '26'
            }])
            errorBuilderStub.resolves('UnauthorizedError')

            // Act
            await del(req, res, next)

            // Assert
            expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401026)
        })

        it('should successfully delete an experiment', async () => {
            // Arrange
            req['params'] = { 'id': '1' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{'creatorDbId': 100}])
            transactionStub.resolves([{'success': true}])

            // Act
            await del(req, res, next)

            // Assert
            expect(transactionStub.calledOnce).to.be.true('transaction should be called once')
        })

        it('should throw an InternalError (500002) if other errors were encountered', async () => {
            // Arrange
            req['params'] = { 'id': '1' }

            isIntStub.returns(true)
            getUserIdStub.resolves(1)
            isAdminStub.resolves(true)
            queryStub.resolves([{'creatorDbId': 100}])
            transactionStub.throws(new Error('dummy error'))
            logMessageStub.resolves('err')
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await del(req, res, next)

            // Assert
            expect(logMessageStub.calledOnce).to.be.true('logMessage should be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })
    })
})
