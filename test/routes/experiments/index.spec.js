/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize.js')
const errorBuilder = require('../../../helpers/error-builder.js')
const tokenHelper = require('../../../helpers/auth/token.js')
const logger = require('../../../helpers/logger/index.js')
const experimentHelper = require('../../../helpers/experiment/index.js')

const req = {
    headers: {
        host:'testHost'
    }
}

const res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}

const next = {}

describe('Experiments', () => {
    describe('/POST experiments', () => {
        const { post } = require('../../../routes/v3/experiments/index.js');
        const sandbox = sinon.createSandbox()
        let queryStub
        let getUserIdStub
        let errorBuilderStub
        let transactionStub
        let logMessageStub
        let getPatternStub
        let generateCodeStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            logMessageStub = sandbox.stub(logger, 'logMessage')
            getPatternStub = sandbox.stub(experimentHelper, 'getPattern')
            generateCodeStub = sandbox.stub(experimentHelper, 'generateCode')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError (401002) when a user is not found', async () => {
            // Arrange
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError (400009) when the request body is not set', async () => {
            // Arrange
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')
            
            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
        })

        it('should throw a BadRequestError (400005) when the records array is empty', async () => {
            // Arrange
            req.body = {}

            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        /**
         * Iterate on test cases wherein a required parameter or
         * any combination is missing
        */
        const parameters = {
            'programDbId': {
                'stageDbId': 'stageDbId', 
                'experimentYear': 'experimentYear', 
                'experimentType': 'experimentType', 
                'seasonDbId': 'seasonDbId', 
                'experimentStatus': 'experimentStatus', 
                'stewardDbId': 'stewardDbId', 
                'cropDbId': 'cropDbId', 
            },
            'stageDbId': {
                'programDbId': 'programDbId', 
                'experimentYear': 'experimentYear', 
                'experimentType': 'experimentType', 
                'seasonDbId': 'seasonDbId', 
                'experimentStatus': 'experimentStatus', 
                'stewardDbId': 'stewardDbId', 
                'cropDbId': 'cropDbId', 
            },
            'experimentYear': {
                'stageDbId': 'stageDbId', 
                'programDbId': 'programDbId', 
                'experimentType': 'experimentType', 
                'seasonDbId': 'seasonDbId', 
                'experimentStatus': 'experimentStatus', 
                'stewardDbId': 'stewardDbId', 
                'cropDbId': 'cropDbId', 
            },
            'experimentType': {
                'stageDbId': 'stageDbId', 
                'experimentYear': 'experimentYear', 
                'programDbId': 'programDbId', 
                'seasonDbId': 'seasonDbId', 
                'experimentStatus': 'experimentStatus', 
                'stewardDbId': 'stewardDbId', 
                'cropDbId': 'cropDbId', 
            },
            'seasonDbId': {
                'stageDbId': 'stageDbId', 
                'experimentYear': 'experimentYear', 
                'experimentType': 'experimentType', 
                'programDbId': 'programDbId', 
                'experimentStatus': 'experimentStatus', 
                'stewardDbId': 'stewardDbId', 
                'cropDbId': 'cropDbId', 
            },
            'experimentStatus': {
                'stageDbId': 'stageDbId', 
                'experimentYear': 'experimentYear', 
                'experimentType': 'experimentType', 
                'seasonDbId': 'seasonDbId', 
                'programDbId': 'programDbId', 
                'stewardDbId': 'stewardDbId', 
                'cropDbId': 'cropDbId', 
            },
            'stewardDbId': {
                'stageDbId': 'stageDbId', 
                'experimentYear': 'experimentYear', 
                'experimentType': 'experimentType', 
                'seasonDbId': 'seasonDbId', 
                'experimentStatus': 'experimentStatus', 
                'programDbId': 'programDbId', 
                'cropDbId': 'cropDbId', 
            },
            'cropDbId': {
                'stageDbId': 'stageDbId', 
                'experimentYear': 'experimentYear', 
                'experimentType': 'experimentType', 
                'seasonDbId': 'seasonDbId', 
                'experimentStatus': 'experimentStatus', 
                'stewardDbId': 'stewardDbId', 
                'programDbId': 'programDbId', 
            },
            'all': {},
        }

        for (const parameter of Object.entries(parameters)) {
            it(`should return a BadRequestError (400255) if ${parameter[0]} parameter(s) are missing`, async () => {
                // Arrange
                req.body = {
                    records: [parameter[1]]
                }
    
                getUserIdStub.resolves(1)
                errorBuilderStub.resolves('BadRequestError')
    
                // Act
                await post(req, res, next)
    
                // Assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400255)
            })
        }

        /**
         * Iterate on test cases wherein an input for a parameter
         * is invalid
        */
        const attributes = {
            // some of the required parameters
            'programDbId': 400051,
            'stageDbId': 400251,
            'experimentYear': 400252,
            'seasonDbId': 400063,
            'stewardDbId': 400253,
            'cropDbId': 400182,
            // optional parameters
            'pipelineDbId': 400250,
            'experimentPlanDbId': 400256,
            'projectDbId': 400056,
            'dataProcessDbId': 400257,
        }

        for (const attribute of Object.entries(attributes)) {
            it(`should return a BadRequestError (${attribute[1]}) if the data type `     +
                `of ${attribute[0]} is invalid`, async () => {
                // Arrange
                req.body = {
                    records: [{
                        // required parameters
                        'programDbId': '123',
                        'stageDbId': '123',
                        'experimentYear': '2024',
                        'experimentType': 'experimentType',
                        'seasonDbId': '123',
                        'experimentStatus': 'experimentStatus',
                        'stewardDbId': '123',
                        'cropDbId': '123',
                    }]
                }

                // e.g. Change records[0].programDbId's value to 'invalid'
                req.body.records[0][`${attribute[0]}`] = 'invalid'
    
                getUserIdStub.resolves(1)
                errorBuilderStub.resolves('BadRequestError')
    
                // Act
                await post(req, res, next)
    
                // Assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', attribute[1])
        })
        }

        it('should return a BadRequestError (400254) if experimentYear ' +
            `is not in format of YYYY`, async () => {
            // Arrange
            req.body = {
                records: [{
                    'programDbId': '123',
                    'pipelineDbId': '123',
                    'stageDbId': '123',
                    'experimentYear': '20240',
                    'experimentType': 'experimentType',
                    'seasonDbId': '123',
                    'experimentStatus': 'experimentStatus',
                    'stewardDbId': '123',
                    'cropDbId': '123',
                }]
            }

            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400254)
        })

        it('should throw a BadRequestError (400015) if inputCount ' +
            'does not equal validateCount', async () => {
                // Arrange
                req.body = {
                    records: [{
                        // required parameters
                        'programDbId': '123',
                        'stageDbId': '123',
                        'experimentYear': '2024',
                        'experimentType': 'experimentType',
                        'seasonDbId': '123',
                        'experimentStatus': 'experimentStatus',
                        'stewardDbId': '123',
                        'cropDbId': '123',
                    }]
                }
    
                getUserIdStub.resolves(1)
                queryStub.onCall(0).resolves([ {'count': 0 }])
                errorBuilderStub.resolves('BadRequestError')
    
                // Act
                await post(req, res, next)
    
                // Assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(queryStub.calledOnce).to.be.true('query must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder should be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should successfully insert a valid record ' +
            'with only required parameters', async () => {
            // Arrange
            req.body = {
                records: [{
                    // required parameters
                    'programDbId': '123',
                    'stageDbId': '123',
                    'experimentYear': '2024',
                    'experimentType': 'experimentType',
                    'seasonDbId': '123',
                    'experimentStatus': 'experimentStatus',
                    'stewardDbId': '123',
                    'cropDbId': '123',
                }]
            }

            getUserIdStub.resolves(1)
            queryStub.onCall(0).resolves([ {'count': 5 }])
            queryStub.onCall(1).resolves([ {'experimentCount': 0 }])
            getPatternStub.resolves('pattern')
            generateCodeStub.resolves('experimentName')
            transactionStub.resolves([ [ { 'id': 1 } ], 1 ])

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
            expect(getPatternStub.calledOnce).to.be.true('getPattern must be called exactly once')
            expect(generateCodeStub.calledOnce).to.be.true('generateCode must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record ' +
            'with required and optional parameters', async () => {
            // Arrange
            req.body = {
                records: [{
                    // required parameters
                    'programDbId': '123',
                    'stageDbId': '123',
                    'experimentYear': '2024',
                    'experimentType': 'experimentType',
                    'seasonDbId': '123',
                    'experimentStatus': 'experimentStatus',
                    'stewardDbId': '123',
                    'cropDbId': '123',
                    // optional parameters
                    'pipelineDbId': '123',
                    'experimentPlanDbId': '123',
                    'projectDbId': '123',
                    'dataProcessDbId': '123',
                    'experimentSubType': 'experimentSubType',
                    'experimentSubSubType': 'experimentSubSubType',
                    'experimentDesignType': 'experimentDesignType',
                    'description': 'description',
                }]
            }

            getUserIdStub.resolves(1)
            queryStub.onCall(0).resolves([ {'count': 9 }])
            queryStub.onCall(1).resolves([ {'experimentCount': 0 }])
            getPatternStub.resolves('pattern')
            generateCodeStub.resolves('experimentName')
            transactionStub.resolves([ [ { 'id': 1 } ], 1 ])

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
            expect(getPatternStub.calledOnce).to.be.true('getPattern must be called exactly once')
            expect(generateCodeStub.calledOnce).to.be.true('generateCode must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert a valid record ' +
            'with experimentCount > 0', async () => {
            // Arrange
            req.body = {
                records: [{
                    // required parameters
                    'programDbId': '123',
                    'stageDbId': '123',
                    'experimentYear': '2024',
                    'experimentType': 'experimentType',
                    'seasonDbId': '123',
                    'experimentStatus': 'experimentStatus',
                    'stewardDbId': '123',
                    'cropDbId': '123',
                }]
            }

            getUserIdStub.resolves(1)
            queryStub.onCall(0).resolves([ {'count': 5 }])
            queryStub.onCall(1).resolves([ {'experimentCount': 1 }])
            getPatternStub.resolves('pattern')
            generateCodeStub.resolves('experimentName')
            transactionStub.resolves([ [ { 'id': 1 } ], 1 ])

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.calledTwice).to.be.true('query must be called exactly twice')
            expect(getPatternStub.calledOnce).to.be.true('getPattern must be called exactly once')
            expect(generateCodeStub.calledOnce).to.be.true('generateCode must be called exactly once')
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should successfully insert multiple valid records ' +
            'with experimentCount > 0', async () => {
            // Arrange
            req.body = {
                records: [
                    {
                        // required parameters
                        'programDbId': '123',
                        'stageDbId': '123',
                        'experimentYear': '2024',
                        'experimentType': 'experimentType',
                        'seasonDbId': '123',
                        'experimentStatus': 'experimentStatus',
                        'stewardDbId': '123',
                        'cropDbId': '123',
                        'experimentName': 'experimentName',
                    },
                    {
                        // required parameters
                        'programDbId': '123',
                        'stageDbId': '123',
                        'experimentYear': '2024',
                        'experimentType': 'experimentType',
                        'seasonDbId': '123',
                        'experimentStatus': 'experimentStatus',
                        'stewardDbId': '123',
                        'cropDbId': '123',
                        'experimentName': 'experimentName',
                    },
                    {
                        // required parameters
                        'programDbId': '123',
                        'stageDbId': '123',
                        'experimentYear': '2024',
                        'experimentType': 'experimentType',
                        'seasonDbId': '123',
                        'experimentStatus': 'experimentStatus',
                        'stewardDbId': '123',
                        'cropDbId': '123',
                        'experimentName': 'experimentName',
                    },
                    {
                        // required parameters
                        'programDbId': '123',
                        'stageDbId': '123',
                        'experimentYear': '2024',
                        'experimentType': 'experimentType',
                        'seasonDbId': '123',
                        'experimentStatus': 'experimentStatus',
                        'stewardDbId': '123',
                        'cropDbId': '123',
                        'experimentName': 'experimentName',
                    },
                    {
                        // required parameters
                        'programDbId': '123',
                        'stageDbId': '123',
                        'experimentYear': '2024',
                        'experimentType': 'experimentType',
                        'seasonDbId': '123',
                        'experimentStatus': 'experimentStatus',
                        'stewardDbId': '123',
                        'cropDbId': '123',
                        'experimentName': 'experimentName',
                    },
                ]
            }
            const recordCount = req.body.records.length
            const queryStubCallCount = recordCount * 2

            getUserIdStub.resolves(1)

            for (let i = 0; i < recordCount; ++i) {
                queryStub.onCall(2*i).resolves([ {'count': 5 }])
                queryStub.onCall((2*i)+1).resolves([ {
                    'experimentCount': i % 2,
                    'code': i % 2,
                }])
                transactionStub.resolves([ [ { 'id': 1 } ], 1 ])
            }

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.be.eq(queryStubCallCount, `query must be called exactly ${queryStubCallCount} times`)
            expect(transactionStub.calledOnce).to.be.true('transaction must be called exactly once')
        })

        it('should throw an InternalError (500001) if other errors were encountered', async () => {
            // Arrange
            req.body = {
                records: [{
                    'stageDbId': '123',
                    'experimentYear': '2024',
                    'experimentType': 'experimentType',
                    'seasonDbId': '123',
                    'experimentStatus': 'experimentStatus',
                    'stewardDbId': '123',
                    'cropDbId': '123',
                }]
            }

            getUserIdStub.resolves(1)
            errorBuilderStub.onCall(0).rejects()
            logMessageStub.resolves('error')
            errorBuilderStub.onCall(1).resolves('InternalError')

            // Act
            await post(req, res, next)

            // Assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(logMessageStub.calledOnce).to.be.true('logMessage must be called exactly once')
            expect(errorBuilderStub.calledTwice).to.be.true('errorBuilder must be called exactly twice')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })
})
