/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize')
const { expect } = require('chai')
const { knex } = require('../../config/knex')
const errorBuilder = require('../../helpers/error-builder')
const processQueryHelper = require('../../helpers/processQuery/index.js')
const tokenHelper = require('../../helpers/auth/token.js')

let req

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}

const next = {}

describe('Planting Job Entries', () => {
    describe('/POST planting-job-entries-search', () => {
        const { post } = require('../../routes/v3/planting-job-entries-search/index');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let getDistinctStringStub
        let getFilterWithInfoStub
        let getFieldValuesStringStub
        let getFinalSqlQueryWoLimitStub
        let getFinalTotalCountQueryStub
        let getOrderStringStub
        let knexRawStub
        let knexSelectStub
        let knexColumnStub
        let queryStub
        let tokenHelperStub

        beforeEach(() => {
            getDistinctStringStub = sandbox.stub(processQueryHelper, 'getDistinctString')
            getFilterWithInfoStub = sandbox.stub(processQueryHelper, 'getFilterWithInfo')
            getFinalSqlQueryWoLimitStub = sandbox.stub(processQueryHelper, 'getFinalSqlQueryWoLimit')
            getFieldValuesStringStub = sandbox.stub(processQueryHelper, 'getFieldValuesString')
            getFinalTotalCountQueryStub = sandbox.stub(processQueryHelper, 'getFinalTotalCountQuery')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            knexRawStub = sandbox.stub(knex, 'raw')
            knexSelectStub = sandbox.stub(knex, 'select')
            knexColumnStub = sandbox.stub(knex, 'column')
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should get distinct string successfully',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                    'body': {
                        'distinctOn': 'someField',
                    },
                }

                tokenHelperStub.resolves(1)

                getDistinctStringStub.resolves('addedDistinctString')
                getFilterWithInfoStub.resolves('conditionString')

                getFinalSqlQueryWoLimitStub.resolves('plantingJobEntriesFinalSqlQuery')
                getFinalTotalCountQueryStub.resolves('plantingJobEntriesCountFinalSqlQuery')
                queryStub.onCall(0).resolves(['plantingJobEntry1'])

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.false('no error should have been called')
                expect(getDistinctStringStub.calledOnce).to.be.true('distinctOn should be defined')
                expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilter must be successfully called once')
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('planting job entry records must be > 0')
                expect(queryStub.callCount).to.equal(1)
        })

        it('should generate BadRequestError when ' +
            'conditionString has an invalid input',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                    body: {},
                }

                tokenHelperStub.resolves(1)

                getFilterWithInfoStub.resolves({ 'mainQuery': 'invalid' })
                errorBuilderStub.resolves('BadRequestError')

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.true('BadRequestError should be generated')
                expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilter must be successfully called once')
        })

        it('should respond with Planting Job Entry records when ' +
            'req.body has fields',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                    'body': {
                        'fields': 'field1|field2'
                    },
                }

                tokenHelperStub.resolves(1)

                getFilterWithInfoStub.resolves('conditionString')

                knexColumnStub.resolves('plantingJobEntriesQuery')

                getFinalSqlQueryWoLimitStub.resolves('plantingJobEntriesFinalSqlQuery')
                getFinalTotalCountQueryStub.resolves('plantingJobEntriesCountFinalSqlQuery')
                queryStub.onCall(0).resolves(['plantingJobEntry1'])

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.false('no error should have been called')
                expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilter must be successfully called once')
                expect(knexColumnStub.calledOnce).to.be.true('knex.column must be successfully called once')
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('planting job entry records must be > 0')
                expect(queryStub.callCount).to.equal(1)
        })

        it('should respond with Planting Job Entry records when ' +
            'req.body has fields AND distinctOn',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                    'body': {
                        'fields': 'field1|field2',
                        'distinctOn': 'someField'
                    },
                }

                tokenHelperStub.resolves(1)

                getFilterWithInfoStub.resolves('conditionString')

                getFieldValuesStringStub.resolves('fieldsString')
                knexRawStub.resolves('selectString')
                knexSelectStub.resolves('plantingJobEntriesQuery')

                getFinalSqlQueryWoLimitStub.resolves('plantingJobEntriesFinalSqlQuery')
                getFinalTotalCountQueryStub.resolves('plantingJobEntriesCountFinalSqlQuery')
                queryStub.onCall(0).resolves(['plantingJobEntry1'])

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.false('no error should have been called')
                expect(getFilterWithInfoStub.calledOnce).to.be.true('getFilter must be successfully called once')
                expect(getFieldValuesStringStub.calledOnce).to.be.true('getFieldValuesString must be successfully called once')
                expect(knexRawStub.calledOnce).to.be.true('knex.raw must be successfully called once')
                expect(knexSelectStub.calledOnce).to.be.true('knex.select must be successfully called once')
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('planting job entry records must be > 0')
                expect(queryStub.callCount).to.equal(1)
        })

        it('should respond with Planting Job Entry records when ' +
            'req.query has sort',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    'query': {
                        'sort': 'sort:desc',
                    },
                }

                tokenHelperStub.resolves(1)

                getOrderStringStub.resolves('orderString')

                getFinalSqlQueryWoLimitStub.resolves('plantingJobEntriesFinalSqlQuery')
                getFinalTotalCountQueryStub.resolves('plantingJobEntriesCountFinalSqlQuery')
                queryStub.onCall(0).resolves(['plantingJobEntry1'])

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.false('no error should have been called')
                expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be successfully called once')
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('planting job entry records must be > 0')
                expect(queryStub.callCount).to.equal(1)
        })

        it('should respond with an empty array when no results are found',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                }

                tokenHelperStub.resolves(1)

                getFinalSqlQueryWoLimitStub.resolves('plantingJobEntriesFinalSqlQuery')
                getFinalTotalCountQueryStub.resolves('plantingJobEntriesCountFinalSqlQuery')
                queryStub.onCall(0).resolves([])

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.false('no error should have been called')
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('planting job entry records must be > 0')
                expect(queryStub.callCount).to.equal(1)
        })

        it('should generate BadRequestError when ' +
            'req.query has sort AND orderString has invalid input',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    'query': {
                        'sort': 'sort:desc',
                    },
                }

                tokenHelperStub.resolves(1)

                getOrderStringStub.resolves('orderString has invalid')
                errorBuilderStub.resolves('BadRequestError')

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.true('BadRequestError should be generated')
                expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be successfully called once')
        })

        it('should generate InternalError when the query for retrieving ' +
            'the planting job entry records and totalCount fails',
            async () => {
                req = {
                    headers: {
                        host: 'local',
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    },
                    query: {},
                }

                tokenHelperStub.resolves(1)

                getFinalSqlQueryWoLimitStub.resolves('plantingJobEntriesFinalSqlQuery')
                getFinalTotalCountQueryStub.resolves('plantingJobEntriesCountFinalSqlQuery')
                queryStub.onCall(0).rejects()
                errorBuilderStub.resolves('InternalError')

                await post(req, res, next)

                expect(errorBuilderStub.called).to.be.true('InternalError should be generated')
                expect(getFinalSqlQueryWoLimitStub.calledOnce).to.be.true('getFinalSqlQueryWoLimit must be successfully called once')
                expect(getFinalTotalCountQueryStub.calledOnce).to.be.true('planting job entry records must be > 0')
                expect(queryStub.callCount).to.equal(1)
        })
    })
})