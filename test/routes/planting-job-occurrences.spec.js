/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

describe('Planting Job Occurrences', () => {
    describe('/POST planting-job-occurrences', () => {
        const { post } = require('../../routes/v3/planting-job-occurrences/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully create a new planting job occurrence record',
            async () => {

        })
    })

    describe('/PUT/:id planting-job-occurrences', () => {
        const { put } = require('../../routes/v3/planting-job-occurrences/_id/index');
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully update an existing planting job occurrence ' +
            'record given a planting job occurrence ID', async () => {

        })
    })

    describe('/DELETE/:id planting-job-occurrences', () => {
        const sandbox = sinon.createSandbox()

        beforeEach(() => {

        })

        afterEach(() => {
            sandbox.restore()
        })

        xit('should successfully delete a specific and existing planting job ' +
            'occurrence record given a planting job occurrence ID',
            async () => {

        })
    })
})