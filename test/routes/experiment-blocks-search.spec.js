/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const {sequelize} = require("../../config/sequelize");
const errorBuilder = require("../../helpers/error-builder");
const processQueryHelper = require("../../helpers/processQuery/index");
const {post} = require("../../routes/v3/experiment-blocks-search");

let req = {
    headers: {
        host: 'testHost'
    },
    query: { },
    body: {
        experimentDbId: '10'
    },
    paginate: {
        limit: 100,
        offset: 0
    }
}
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Experiment Blocks', () => {
    describe('POST experiment-blocks-search', () => {
        const { post } = require('../../routes/v3/experiment-blocks-search')
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub
        let getOrderStringStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve all experiment block records', async() => {
            queryStub.onFirstCall().resolves([
                {
                    experimentBlockDbId: 2,
                    experimentBlockCode: 'EXP0005251-NB-1',
                    experimentBlockName: 'Nursery block 1'
                }
            ])  // experimentBlocks data
            queryStub.onSecondCall().resolves([{count: 1}]) // count

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(2,
                '\n 1. experiment block data' +
                '\n 2. number of records found\n')
            expect(errorBuilderStub.called).to.be.false(
                'no error should have been called')
        })

        it('should successfully return when no records are found', async() => {
            queryStub.onFirstCall().resolves([])  // experimentBlocks data

            let result = await post(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'experiment block data must be retrieved')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate an InternalError when searching for experiment ' +
            'blocks fail', async() => {
            queryStub.onFirstCall().rejects()
            errorBuilderStub.resolves('InternalError')

            let result = await post(req, res, next)
            expect(queryStub.callCount).to.equal(1, 'search for experiment block data must have occurred')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when sort argument is invalid', async() => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'bad:sort'  // getOrderString is called when this is specified
                },
                body: {
                    experimentDbId: '10'
                },
                paginate: {
                    limit: 100,
                        offset: 0
                }
            }
            getOrderStringStub.resolves('invalid')  // orderString
            errorBuilderStub.resolves('BadRequestError')

            let result = await post(req, res, next)

            expect(getOrderStringStub.callCount).to.eq(1, 'helper function must be called if sort param is specified')
            expect(errorBuilderStub.callCount).to.eq(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })
    })
})