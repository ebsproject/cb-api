/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const processQueryHelper = require('../../helpers/processQuery/index')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Shipment Tags', () => {
    describe('/GET shipment-tags', () => {
        const { get } = require('../../routes/v3/shipment-tags/index')
        const sandbox = sinon.createSandbox()
        let getFilterStub
        let getOrderStringStub
        let queryStub
        let getCountFinalSqlQueryStub
        let errorBuilderStub
        let testingArgs = [
            {
                word: 'EMPTY occurrenceDbId',
                occurrenceDbId: '',
            },
            {
                word: 'SINGLE occurrenceDbId',
                occurrenceDbId: '3388',
            },
            {
                word: 'MULTIPLE occurrenceDbIds',
                occurrenceDbId: '3388,3389,3390',
            },
        ]

        beforeEach(() => {
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            queryStub = sandbox.stub(sequelize, 'query')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        testingArgs.forEach((args) => {
            it('should successfully retrieve plot records to be used for ' +
            `shipment tags with ${args.word} in query`, async () => {
                req = {
                    headers: {},
                    query: {
                        occurrenceDbId: args.occurrenceDbId,
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    }
                }

                getFilterStub.resolves(`WHERE occurrenceDbId ILIKE ${args.occurrenceDbId}`)
                queryStub.onFirstCall().resolves(['shipmentTag1, shipmentTag2, shipmentTag3'])
                getCountFinalSqlQueryStub.resolves(100)
                queryStub.onSecondCall().resolves([ { count: 100 } ])

                await get(req, res, next)

                expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
                expect(getOrderStringStub.called).to.be.false('sort should be undefined')
                expect(queryStub.calledTwice).to.be.true('query must be called twice')
                expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('shipment tags must be > 0')
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
            })
        })
    
        it('should generate BadRequestError if occurrence ID is not an ' +
            'integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: 'occ'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            errorBuilderStub.resolves('BadRequestError')

            await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400241)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate BadRequestError if the condition string ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: ''
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getFilterStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await get(req, res, next)

            expect(queryStub.called).to.be.false('query must not be called')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate BadRequestError if the order string ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '',
                    sort: 'sort1'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getFilterStub.resolves('')
            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await get(req, res, next)

            expect(queryStub.called).to.be.false('query must not be called')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate Internal when the query for retrieving ' +
            'the shipment tag records fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: ''
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should generate NotFoundError if the occurrence does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.resolves([])

            await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should generate InternalError if the shipment tag count query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onFirstCall().resolves(['occurrence'])
            getFilterStub.resolves('filter')
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })
})