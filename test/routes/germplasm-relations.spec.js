/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const errors = require('restify-errors')
const tokenHelper = require('../../helpers/auth/token')
const userValidator = require('../../helpers/person/validator')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}
const transactionMock = {
    finished: "",
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    }
}

describe('Germplasm Relations', () => {

    describe('/POST germplasm-relations', () => {
        const { post } = require('../../routes/v3/germplasm-relations/index');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let transactionStub
        let queryStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionMock.finished = ""
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should generate a InternalError when records'
            + ' is not defined', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {}
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when records'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: []
                }
            }

            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
        })

        it('should generate a InternalError when transaction'
            + ' throws an error', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {}
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should generate a BadRequestError when parentGermplasmId is not specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {}
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
        })

        it('should generate a BadRequestError when childGermplasmId is not specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
        })

        it('should generate a BadRequestError when orderNumber is not specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: 'abc',
                            childGermplasmDbId: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
        })

        it('should generate a BadRequestError when parentGermplasmDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: 'abc',
                            childGermplasmDbId: 'abc',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
        })

        it('should generate an InternalError when retrieval of parent germplasm fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: 'abc',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'queryStub must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when parent germplasm'
            + ' result is undefined', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: 'abc',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves(undefined)

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'queryStub must be called once')
        })

        it('should generate a BadRequestError when parent germplasm'
            + ' result is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: 'abc',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'queryStub must be called once')
        })

        it('should generate a BadRequestError when childGermplasmDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: 'abc',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'queryStub must be called once')
        })

        it('should generate an InternalError when retrieval of child germplasm fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '123',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'queryStub must be called twice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when child germplasm'
            + ' result is undefined', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '123',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves(undefined)

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'queryStub must be called twice')
        })

        it('should generate a BadRequestError when child germplasm'
            + ' result is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '123',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'queryStub must be called twice')
        })

        it('should generate a BadRequestError when orderNumber'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '123',
                            orderNumber: 'abc'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'queryStub must be called twice')
        })

        it('should generate a BadRequestError when parentGermplasmDbId'
            + ' is the same as the childGermplasmDbId', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '123',
                            orderNumber: '1'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'queryStub must be called twice')
        })

        it('should generate an InternalError when checking of child-order number'
            + ' combination fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '456',
                            orderNumber: '1'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'queryStub must be called thrice')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when child-order number'
            + ' combination already exists', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '456',
                            orderNumber: '1'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{count:1}])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'queryStub must be called thrice')
        })

        it('should generate an InternalError when checking of child-parent'
            + ' combination fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '456',
                            orderNumber: '1'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{count:0}])
            queryStub.onCall(3).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(4, 'queryStub must be called 4 times')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when child-order number'
            + ' combination already exists', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '456',
                            orderNumber: '1'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{count:0}])
            queryStub.onCall(3).resolves([{count:1}])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(4, 'queryStub must be called 4 times')
        })

        it('should generate an InternalError insertion fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '456',
                            orderNumber: '1'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{count:0}])
            queryStub.onCall(3).resolves([{count:0}])
            queryStub.onCall(4).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(5, 'queryStub must be called 5 times')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully insert germplasm relation', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            parentGermplasmDbId: '123',
                            childGermplasmDbId: '456',
                            orderNumber: '1'
                        }
                    ]
                }
            }

            tokenHelperStub.resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{count:1}])
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{count:0}])
            queryStub.onCall(3).resolves([{count:0}])
            queryStub.onCall(4).resolves([[{id:999}]])

            const result = await post(req, res, next)
            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(5, 'queryStub must be called 5 times')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must NOT be called')
        })
    })
})