/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../config/sequelize.js')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder.js')
const tokenHelper = require('../../helpers/auth/token.js')
const userValidator = require('../../helpers/person/validator.js')
const validator = require('validator')
const logger = require('../../helpers/logger')

const { expect } = require('chai')

let req = {
    headers: {
        host:'testHost'
    }
}

let res = {
  send: (statusCode, data) => {
    return {
      statusCode,
      data
    }
  }
}
const next = {}
const transactionMock = {
    finished: '',
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    }
}

describe('Breeding Portal Data', () => {
    describe('POST breeding-portal-data', () => {
        const { post } = require('../../routes/v3/breeding-portal-data/index.js')
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let queryStub
        let transactionStub
        let errorStub
        let jsonValidatorStub
        let loggerFailingQueryStub
        let loggerMessageStub

        //stub declaration based on existing functions within post
        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            jsonValidatorStub = sandbox.stub(validator, 'isJSON')
            loggerFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            loggerMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) when a user is not found', async () => {
            //mock
            getUserIdStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            //act
            const result = await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError(400009) when the request body is not set', async () => {
            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)

        })

        it('should throw a BadRequestError(400005) when the records array is empty', async () => {
            //arrange
            req.body = {}

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('BadRequestError')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)

        })

        it('should throw an InternalError(500001) if other errors were encountered', async () => {
            //arrange
            req.body = {
                records: {}
            }

            //mock
            getUserIdStub.resolves(1)
            errorBuilderStub.resolves('InternalError')
            transactionStub.resolves({
                rollback: sinon.stub(),
            })

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should return a BadRequestError if the required parameters are missing', async () => {
            //arrange
            req.body = {
                records: [{}]
            }

            //mock
            getUserIdStub.resolves(1)
            errorStub.resolves('error')

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(errorStub.calledOnce).to.be.true('bad request error should be called once')
        })

        it('should successfully create a breeding portal data record when required fields are valid', async () => {
            //arrange
            req.body = {
                records: [
                    {
                        entityName: 'breeding-portal-entity',
                        entityValue: [{'key': 'value'}],
                        transformedData: [{'key': 'value'}],
                        description: 'description',
                        notes: 'notes'
                    }
                ]
            }

            //mock
            getUserIdStub.resolves(1)
            errorStub.resolves('error')
            jsonValidatorStub.resolves(true)
            queryStub.resolves([])   // validation query
            transactionStub.resolves([[{'breedingPortalDataDbId':"1"}]])

            //act
            await post(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must not be called')
        })

    })

    describe('/PUT/:id breeding-portal-data', () => {
        const { put } = require('../../routes/v3/breeding-portal-data/_id/index');
        const sandbox = sinon.createSandbox()
        let getUserIdStub
        let errorBuilderStub
        let queryStub
        let transactionStub
        let errorStub
        let jsonValidatorStub
        let loggerFailingQueryStub
        let loggerMessageStub

        beforeEach(() => {
            getUserIdStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            errorStub = sandbox.stub(errors, 'BadRequestError')
            jsonValidatorStub = sandbox.stub(validator, 'isJSON')
            loggerFailingQueryStub = sandbox.stub(logger, 'logFailingQuery')
            loggerMessageStub = sandbox.stub(logger, 'logMessage')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw a BadRequestError(401002) if user ID is not an ' +
            'found', async () => {
                // arrange
                req.params = {
                    id: '1'
                }

                //mock
                getUserIdStub.resolves(null)
                errorBuilderStub.resolves('BadRequestError')

                //act
                await put(req, res, next)

                //assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
        })

        it('should throw a BadRequestError(400063) if breeding portal data ID is not an integer (in string format)', async () => {
                // arrange
                req.params = {
                    id: 'notAnInteger'
                }

                //mock
                getUserIdStub.resolves(1)
                errorBuilderStub.resolves('BadRequestError')

                //act
                await put(req, res, next)

                //assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400063)
        })

        it('should generate InternalError(500004) when the query retrieving the breeding portal data record fails', async () => {
                // arrange
                req.params = {
                    id: '123'
                }

                //mock
                getUserIdStub.resolves(1)
                queryStub.onFirstCall().rejects()
                errorBuilderStub.resolves('InternalError')

                //act
                await put(req, res, next)

                //assert
                expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully update a breeding portal data record when required fields are valid', async () => {
            // arrange
            req.params = {
                id: '123'
            }
            req.body = {
                entityName: 'breeding-portal-entity',
                entityValue: [{'key': 'value'}],
                transformedData: [{'key': 'value'}],
                description: 'description',
                notes: 'notes'
            }

            //mock
            getUserIdStub.resolves(1)
            queryStub.onFirstCall().resolves([
                {
                    entityName: 'breeding-portal-entity-old',
                    entityValue: [{'key': 'value'}],
                    transformedData: [{'key': 'value'}],
                    description: 'description-old',
                    notes: 'notes-old'
                }
            ])
            jsonValidatorStub.resolves(true)
            transactionStub.resolves([[{'breedingPortalDataDbId':"1"}]])

            //act
            await put(req, res, next)

            //assert
            expect(getUserIdStub.calledOnce).to.be.true('getUserId must be called exactly once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must not be called')
    })
    })
})