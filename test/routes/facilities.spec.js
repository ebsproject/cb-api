/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const processQueryHelper = require('../../helpers/processQuery/index')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Facilities', () => {
    describe('/GET facilities', () => {
        const { get } = require('../../routes/v3/facilities/index');
        const sandbox = sinon.createSandbox()
        let queryStub
        let errorBuilderStub
        let getFilterStub
        let getOrderStringStub

        beforeEach(() => {
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the'
            + ' generated order string is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'attr:ASC'
                },
                params: { },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getOrderStringStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate a BadRequestError when the'
            + ' generated filter string is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'attr:ASC',
                    facilityCode: 'ABCD',
                    facilityType: 'type a',
                    facilityClass: 'class x'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getOrderStringStub.onCall(0).resolves('')
            getFilterStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly once')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate an InternalError when the'
            + ' facility retrieval via facility code fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    sort: 'attr:ASC',
                    facilityCode: 'ABCD'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getOrderStringStub.onCall(0).resolves('')
            getFilterStub.onCall(0).resolves('')
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await get(req, res, next)

            expect(getOrderStringStub.callCount).to.equal(1, 'getOrderString must be called exactly once')
            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly once')
            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when the'
            + ' facility code is non-existent', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    facilityCode: 'ABCD'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getOrderStringStub.onCall(0).resolves('')
            getFilterStub.onCall(0).resolves('')
            queryStub.onCall(0).resolves([])

            let result = await get(req, res, next)

            expect(getFilterStub.callCount).to.equal(1, 'getFilter must be called exactly once')
            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
        })

        it('should generate an InternalError when the'
            + ' actual facility retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError when the'
            + ' facility count retrieval fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{dbId:123}])
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            let result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully retrieve facilities', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves([{dbId:123}])
            queryStub.onCall(1).resolves([{count:1}])

            let result = await get(req, res, next)

            expect(queryStub.callCount).to.equal(2, 'query must be called exactly twice')
        })
    })
})