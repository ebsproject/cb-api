/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let processQueryHelper = require('../../helpers/processQuery/index.js')
let errorBuilder = require('../../helpers/error-builder')
const { expect } = require('chai')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Plot Harvest Tags', () => {
    describe('/GET plot-harvest-tags', () => {
        const { get } = require('../../routes/v3/plot-harvest-tags/index');
        const sandbox = sinon.createSandbox()
        let getFilterStub
        let getOrderStringStub
        let queryStub
        let errorBuilderStub

        beforeEach(() => {
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            queryStub = sandbox.stub(sequelize, 'query')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should successfully retrieve plot records to be used for ' +
            'harvest tags', async () => {
            req = {
                headers: {},
                query: {
                    occurrenceDbId: "1"
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onFirstCall().resolves(['occurrence'])
            getFilterStub.resolves('')
            queryStub.onSecondCall().resolves(['plot1', 'plot2', 'plot3'])
            queryStub.onThirdCall().resolves([{ count: 3 }])

            const result = await get(req, res, next)

            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called exactly 1 time')
            expect(queryStub.calledThrice).to.be.true('query must be called exactly 3 times')
            expect(errorBuilderStub.called).to.be.false('no error should have been called')
        })

        it('should generate BadRequestError if occurrence ID is not an ' +
            'integer (in string format)', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: 'occ'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            errorBuilderStub.resolves('BadRequestError')

            const result = await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400241)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate Internal when the query for retriving' +
            ' the occurrence fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should generate NotFoundError if the occurrence does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.resolves([])

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
        })

        it('should generate BadRequestError if the condition string' +
            ' is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.resolves(['occurrence'])
            getFilterStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate BadRequestError if the order string' +
            ' is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1',
                    sort: 'sort1'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.resolves(['occurrence'])
            getFilterStub.resolves('')
            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            const result = await get(req, res, next)

            expect(queryStub.calledOnce).to.be.true('query must be called once')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate InternalError if the plot query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onFirstCall().resolves(['occurrence'])
            getFilterStub.resolves('')
            queryStub.onSecondCall().rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await get(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should immediately send result when no plots were found', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onFirstCall().resolves(['occurrence'])
            getFilterStub.resolves('')
            queryStub.onSecondCall().resolves([])

            const result = await get(req, res, next)

            expect(queryStub.calledTwice).to.be.true('query must be called twice')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
        })

        it('should generate InternalError if the plot count query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '1',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onFirstCall().resolves(['occurrence'])
            getFilterStub.resolves('filter')
            queryStub.onSecondCall().resolves(['plot1', 'plot2'])
            queryStub.onThirdCall().rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await get(req, res, next)

            expect(queryStub.calledThrice).to.be.true('query must be called thrice')
            expect(getFilterStub.calledOnce).to.be.true('getFilter must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })
})