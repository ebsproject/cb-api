/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const errors = require('restify-errors')
const tokenHelper = require('../../helpers/auth/token')
const userValidator = require('../../helpers/person/validator')
const processQueryHelper = require('../../helpers/processQuery')
const germplasmUploadAbbrevHelper = require('../../helpers/germplasm/index')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}
const transactionMock = {
    finished: "",
    commit: function() {
        this.finished = 'commit'
    },
    rollback: function() {
        this.finished = 'rollback'
    }
}

describe('Germplasm File Uploads', () => {
    const { get, put } = require('../../routes/v3/germplasm-file-uploads/_id/index');
    const gfu = require('../../routes/v3/germplasm-file-uploads/_id/index');

    describe('/POST germplasm-file-uploads', () => {
        const { post } = require('../../routes/v3/germplasm-file-uploads/index');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub
        let getScaleValueByAbbrevStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            getScaleValueByAbbrevStub = sandbox.stub(germplasmUploadAbbrevHelper, 'getScaleValueByAbbrev')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the request body'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when records'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: []
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400005)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a InternalError when records'
            + ' is undefined', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: { }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.resolves(123)
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a InternalError when transaction creation'
            + ' fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ ]
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

        it('should generate a BadRequestError when required parameters'
            + ' are missing', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001"
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Required parameters are missing. Ensure that the programDbId, fileName, fileData, fileUploadOrigin, fileUploadAction and entity fields are not empty.')
        })

        it('should generate a BadRequestError when programDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "12a",
                            fileName: "file001",
                            fileData: [ ]
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when fileName'
            + ' is an empty string', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "",
                            fileData: [ ]
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when fileData'
            + ' is not an array', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: "not an array"
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when fileData'
            + ' array is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ ]
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when fileUploadOrigin is an ' +
            'empty string', async() => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ {} ],
                            fileUploadOrigin: '',
                            fileUploadAction: 'create',
                            entity: 'germplasm'
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid input, fileUploadOrigin must not be empty.')
        })

        it('should generate a BadRequestError when fileUploadAction is an ' +
            'empty string', async() => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ {} ],
                            fileUploadOrigin: 'ABBREV',
                            fileUploadAction: '',
                            entity: 'germplasm'
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            let errorsStub = sandbox.stub(errors, 'BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            sinon.assert.calledWithExactly(errorsStub, 'Invalid input, fileUploadAction must not be empty.')
        })

        it('should generate a BadRequestError when fileStatus'
            + ' was provided AND is not "in queue"', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ {} ],
                            fileStatus: 'completed'
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(0, 'query must not be called')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when validation fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ {} ],
                            fileUploadOrigin: 'ABBREV',
                            fileUploadAction: 'create',
                            fileStatus: 'in queue',
                            remarks: 'remarks',
                            notes: 'notes',
                            entity: 'germplasm'
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{id:0}]) // validation count for entity
            queryStub.onCall(1).resolves([{count:0}])  // validation count if successful should be 3
            errorBuilderStub.resolves('BadRequestError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should generate an InternalError when insertion fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ {} ],
                            fileUploadOrigin: 'ABBREV',
                            fileUploadAction : 'create',
                            fileStatus: 'in queue',
                            remarks: 'remarks',
                            notes: 'notes',
                            entity: 'entity'
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{id:0}])      // entity query
            queryStub.onCall(1).resolves([{count:4}])   // validation query
            queryStub.onCall(2).rejects()               // insert query
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError when no records were inserted', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ {} ],
                            fileUploadOrigin: 'ABBREV',
                            fileUploadAction : 'create',
                            fileStatus: 'in queue',
                            remarks: 'remarks',
                            notes: 'notes',
                            entity: 'entity'
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{id:0}])   // validation query
            queryStub.onCall(1).resolves([{count:4}])   // validation query
            queryStub.onCall(2).resolves([[]])          // insert query
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully insert germplasm file upload record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                body: {
                    records: [
                        {
                            programDbId: "123",
                            fileName: "file001",
                            fileData: [ {} ],
                            fileUploadOrigin: 'ABBREV',
                            fileUploadAction: 'create',
                            fileStatus: 'created',
                            entity: 'germplasm'
                        }
                    ]
                }
            }

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(0).resolves([{id:0}])   // entity query
            queryStub.onCall(1).resolves([{count:4}])   // validation query
            queryStub.onCall(2).resolves([[{'germplasmFileUploadDbId':"12345"}]])
            errorBuilderStub.resolves('InternalError')

            const result = await post(req, res, next)

            expect(tokenHelperStub.calledOnce).to.be.true('tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(0, 'errorBuilder must not be called')
        })
    })

    describe('/PUT germplasm-file-uploads/{id}', () => {
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub
        let getScaleValueByAbbrevStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            transactionMock.finished = ""
            getScaleValueByAbbrevStub = sandbox.stub(germplasmUploadAbbrevHelper, 'getScaleValueByAbbrev')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                }
            }

            tokenHelperStub.onCall(0).resolves(null)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the germplasmFileUploadDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the req.body'
            + ' is not specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a InternalError when the germplasm file upload retrieval '
            + ' fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {

                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the germplasm file upload'
            + ' does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {

                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
        })

        it('should successfully update the germplasm file upload record'
            + ' when all values are not equal to the current values', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    fileName: "new file name 002",
                    fileData: [
                        {
                            "data65":"xyz"
                        }
                    ],
                    fileStatus: 'validated',
                    germplasmCount: '3',
                    seedCount: '4',
                    packageCount: '5',
                    programDbId: '104',
                    errorLog: 'some new error here',
                    remarks: 'new remarks'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            getScaleValueByAbbrevStub.onCall(0).resolves(['validated', 'in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the germplasm file upload record'
            + ' when all values are equal to the current values', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue', 'in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the germplasm file upload record'
            + ' when no values are specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: { }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when fileData'
            + ' is not an array', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    fileData: "not an array" 
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should generate a BadRequestError when fileData'
            + ' is empty', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    fileData: []
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should successfully update the germplasm file upload record'
            + ' when fileData is valid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    fileData: [
                        { germplasm: 'valid' }
                    ]
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when fileStatus'
            + ' is not a valid status', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    fileStatus: 'invalid status'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]
            getScaleValueByAbbrevStub.onCall(0).resolves(['in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should successfully update the germplasm file upload record'
            + ' when fileStatus is valid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    fileStatus: 'validated'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            getScaleValueByAbbrevStub.onCall(0).resolves(['validated', 'in queue'])
            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(germplasmFileUpload)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when germplasmCount'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    germplasmCount: 'not an integer'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should successfully update the germplasm file upload record'
            + ' when germplasmCount is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    germplasmCount: '12'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(germplasmFileUpload)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when seedCount'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    seedCount: 'not an integer'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should successfully update the germplasm file upload record'
            + ' when seedCount is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    seedCount: '12'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(germplasmFileUpload)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when packageCount'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    packageCount: 'not an integer'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should successfully update the germplasm file upload record'
            + ' when packageCount is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    packageCount: '12'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(germplasmFileUpload)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when programDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    programDbId: 'not an integer'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
        })

        it('should successfully update the germplasm file upload record'
            + ' when programDbId is an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    programDbId: '12'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{count:1}])
            queryStub.onCall(2).resolves([{}])

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the germplasm file upload record'
            + ' when errorLog is specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    errorLog: 'new error log'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(germplasmFileUpload)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should successfully update the germplasm file upload record'
            + ' when remarks is specified', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    remarks: 'new remarks'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(germplasmFileUpload)

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate a BadRequestError when validation fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    programDbId: '103'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileUploadAction: 'create',
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves(germplasmFileUpload)
            queryStub.onCall(2).resolves([{count:0}])
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should generate an InternalError when an error occurs', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                },
                body: {
                    programDbId: '103'
                }
            }

            germplasmFileUpload = [
                {
                    fileName: "file name 001",
                    fileData: [
                        {
                            "data1":"abc"
                        }
                    ],
                    fileStatus: 'in queue',
                    germplasmCount: '1',
                    seedCount: '1',
                    packageCount: '1',
                    programDbId: '101',
                    errorLog: 'some error here',
                    remarks: 'remarks'
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves("InternalError")

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })

    })

    describe('/DELETE germplasm-file-uploads/{id}', () => {
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub
        let isAdminStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            transactionMock.finished = ""
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when the userDbId'
            + ' is null', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: 1234
                }
            }

            tokenHelperStub.onCall(0).resolves(null)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await gfu.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate a BadRequestError when the germplasmFileUploadDbId'
            + ' is not an integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "abc"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await gfu.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400249)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError when the germplasm file upload retrieval '
            + ' fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await gfu.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a NotFoundError when the germplasm file upload'
            + ' does not exist', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves([])
            errorBuilderStub.onCall(0).resolves('NotFoundError')

            const result = await gfu.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 404055)
        })

        it('should successfully delete the germplasm file upload record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            germplasmFileUpload = [
                {
                    germplasmFileUploadDbId: 456
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves('success')

            const result = await gfu.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
        })

        it('should generate an InternalError when deletion fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            germplasmFileUpload = [
                {
                    germplasmFileUploadDbId: 456
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await gfu.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })

        it('should generate an InternalError when transaction creation fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "456"
                }
            }

            germplasmFileUpload = [
                {
                    germplasmFileUploadDbId: 456
                }
            ]

            tokenHelperStub.onCall(0).resolves(123)
            queryStub.onCall(0).resolves(germplasmFileUpload)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await gfu.delete(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(queryStub.callCount).to.equal(1, 'query must be called once')
            expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
        })
    })

    describe('/GET germplasm-file-uploads/{id}', () => {
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate an InternalError when no id is provided', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {}
            }

            errorBuilderStub.onCall(0).resolves("InternalError")

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if id provided is not an integer', 
            async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    params: {
                        id: "abc"
                    }
                }

                errorBuilderStub.onCall(0).resolves("BadRequestError")

                const result = await get(req, res,next)

                expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError if the query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "5"
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves("InternalError")

            const result = await get(req, res,next)

            expect(queryStub.callCount).to.be.equal(1, 'query is called once')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if no record is retrieved', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "5"
                }
            }

            queryStub.onCall(0).resolves([])
            errorBuilderStub.onCall(0).resolves("BadRequestError")

            const result = await get(req, res,next)

            expect(queryStub.callCount).to.be.equal(1, 'query is called once')
        })

        it('should successfully retrieve germplasm file upload record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: "5"
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'germplasmFileUploadDbId':'5', 
                    'fileName':'test file name'
                }
            ])

            const result = await get(req, res, next)
            
            expect(queryStub.callCount).to.be.equal(1, 'query is called once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder is not called')
        })
    })

    describe('/POST germplasm-file-uploads/{id}/data-search', () => {
        const { post } = require('../../routes/v3/germplasm-file-uploads/_id/data-search');
        const sandbox = sinon.createSandbox()
        let errorBuilderStub
        let queryStub
        let getFilterStub
        let getOrderStringStub

        beforeEach(() => {
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            getFilterStub = sandbox.stub(processQueryHelper, 'getFilter')
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate a BadRequestError when no entity is provided', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: ''
                },
                params: {}
            }

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(0, 'query must NOT be called')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should generate a BadRequestError when no entity is null', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: null
                },
                params: {}
            }

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(0, 'query must NOT be called')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should generate a BadRequestError when entity is'
            + ' not supported', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'anywho'
                },
                params: {}
            }

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(0, 'query must NOT be called')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should generate an InternalError id is not provided', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {}
            }

            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(0, 'query must NOT be called')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError id is empty', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: ''
                }
            }

            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(0, 'query must NOT be called')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError id is not an integer', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: 'abc'
                }
            }

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(0, 'query must NOT be called')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should generate an InternalError when gfu lookup fails', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError when look up result is null', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                }
            }

            queryStub.onCall(0).resolves(null)

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should generate a BadRequestError when look up result is empty', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                }
            }

            queryStub.onCall(0).resolves([])

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(1, 'query must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should generate a BadRequestError when condition string is invalid (with fields and distinct)', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                },
                body: {
                    fields: 'table.id as id',
                    distinctOn: 'id',
                    filterX: 'valueX'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            getFilterStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(1, 'query must be called once')
            expect(getFilterStub.callCount).to.be.equals(1, 'getFilter must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate a BadRequestError when condition string is invalid (without fields and distinct)', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: '',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                },
                body: {
                    filterX: 'valueX'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            getFilterStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(1, 'query must be called once')
            expect(getFilterStub.callCount).to.be.equals(1, 'getFilter must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400022)
        })

        it('should generate an InternalError when data retrieval fails', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: null,
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            queryStub.onCall(1).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(2, 'query must be called twice')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate an InternalError when data count retrieval fails', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: null,
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            queryStub.onCall(1).resolves([{datId:1}])
            queryStub.onCall(2).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(3, 'query must be called thrice')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should successfully retrieve germplasm file upload data (with filters)', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: null,
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                },
                body: {
                    filterX: 'valueX'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            getFilterStub.onCall(0).resolves('WHERE a = b')
            queryStub.onCall(1).resolves([{datId:1}])
            queryStub.onCall(2).resolves([{count:1}])

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(3, 'query must be called thrice')
            expect(getFilterStub.callCount).to.be.equals(1, 'getFilter must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should successfully retrieve germplasm file upload data (with fields)', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: null,
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                },
                body: {
                    fields: 'table.id = id',
                    distinctOn: 'a',
                    filterX: 'valueX'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            getFilterStub.onCall(0).resolves('WHERE a = b')
            queryStub.onCall(1).resolves([{datId:1}])
            queryStub.onCall(2).resolves([{count:1}])

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(3, 'query must be called thrice')
            expect(getFilterStub.callCount).to.be.equals(1, 'getFilter must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

        it('should generate BadRequestError when sort is invalid', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'id:desc',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                },
                body: {
                    fields: 'table.id = id',
                    filterX: 'valueX'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            getFilterStub.onCall(0).resolves('WHERE a = b')
            getOrderStringStub.onCall(0).resolves('invalid')
            errorBuilderStub.onCall(0).resolves('BadRequestError')
            // queryStub.onCall(1).resolves([{datId:1}])
            // queryStub.onCall(2).resolves([{count:1}])
            

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(1, 'query must be called once')
            expect(getFilterStub.callCount).to.be.equals(1, 'getFilter must be called once')
            expect(getOrderStringStub.callCount).to.be.equals(1, 'getOrderString must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should successfully retrieve germplasm file upload data (with sort)', async() =>{
            req = {
                headers: {
                    host: 'testHost'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                },
                query: {
                    sort: 'id:desc',
                    entity: 'germplasm_relation'
                },
                params: {
                    id: '123'
                },
                body: {
                    fields: 'table.id = id',
                    filterX: 'valueX'
                }
            }

            queryStub.onCall(0).resolves([{germplasmFileUploadDbId:1}])
            getFilterStub.onCall(0).resolves('WHERE a = b')
            getOrderStringStub.onCall(0).resolves('ORDER by a')
            queryStub.onCall(1).resolves([{datId:1}])
            queryStub.onCall(2).resolves([{count:1}])
            

            const result = await post(req, res,next)

            expect(queryStub.callCount).to.be.equals(3, 'query must be called thrice')
            expect(getFilterStub.callCount).to.be.equals(1, 'getFilter must be called once')
            expect(getOrderStringStub.callCount).to.be.equals(1, 'getOrderString must be called once')
            expect(errorBuilderStub.callCount).to.be.equals(0, 'errorBuilder must NOT be called')
        })

    })

    describe('/POST germplasm-file-uploads/{id}/germplasm-search', () => {
        const { post } = require('../../routes/v3/germplasm-file-uploads/_id/germplasm-search');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })



        it('should generate an InternalError when no id is provided', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {}
            }

            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if id provided is not an integer',
            async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    params: {
                        id: 'abc'
                    }
                }

                const result = await get(req, res,next)

                expect(errorBuilderStub.callCount).to.be.equals(0, 'code does not use errorBuilder so no need to be called')
                expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError if the query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if no germplasm file upload record is retrieved', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
        })

        it('should successfully retrieve germplasm file upload record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'germplasmFileUploadDbId':'5',
                    'fileName':'test file name'
                }
            ])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
        })

        it('should successfully retrieve germplasm record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'germplasmFileUploadDbId':'5',
                    'fileName':'test file name'
                }
            ])
            queryStub.onCall(1).resolves([{germplasmDbId:'123'}])
            queryStub.onCall(2).resolves([{count:1}])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
        })
    })

    describe('/POST germplasm-file-uploads/{id}/seeds-search', () => {
        const { post } = require('../../routes/v3/germplasm-file-uploads/_id/seeds-search');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })



        it('should generate an InternalError when no id is provided', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {}
            }

            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if id provided is not an integer',
            async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    params: {
                        id: 'abc'
                    }
                }

                const result = await get(req, res,next)

                expect(errorBuilderStub.callCount).to.be.equals(0, 'code does not use errorBuilder so no need to be called')
                expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError if the query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if no germplasm file upload record is retrieved', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
        })

        it('should successfully retrieve germplasm file upload record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'germplasmFileUploadDbId':'5',
                    'fileName':'test file name'
                }
            ])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
        })

        it('should successfully retrieve seed record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'germplasmFileUploadDbId':'5',
                    'fileName':'test file name'
                }
            ])
            queryStub.onCall(1).resolves([{seedDbId:'123'}])
            queryStub.onCall(2).resolves([{count:1}])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
        })
    })

    describe('/POST germplasm-file-uploads/{id}/packages-search', () => {
        const { post } = require('../../routes/v3/germplasm-file-uploads/_id/packages-search');
        const sandbox = sinon.createSandbox()
        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should generate an InternalError when no id is provided', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {}
            }

            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if id provided is not an integer',
            async () => {
                req = {
                    headers: {
                        host: 'testHost'
                    },
                    params: {
                        id: 'abc'
                    }
                }

                const result = await get(req, res,next)

                expect(errorBuilderStub.callCount).to.be.equals(0, 'code does not use errorBuilder so no need to be called')
                expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate an InternalError if the query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })

        it('should generate a BadRequestError if no germplasm file upload record is retrieved', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await get(req, res,next)

            expect(errorBuilderStub.callCount).to.be.equals(1, 'errorBuilder must be called exactly once')
            expect(queryStub.callCount).to.be.equals(1, 'query must be called exactly once')
        })

        it('should successfully retrieve germplasm file upload record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'germplasmFileUploadDbId':'5',
                    'fileName':'test file name'
                }
            ])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
        })

        it('should successfully retrieve package record', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                params: {
                    id: '5'
                }
            }

            queryStub.onCall(0).resolves([
                {
                    'germplasmFileUploadDbId':'5',
                    'fileName':'test file name'
                }
            ])
            queryStub.onCall(1).resolves([{packageDbId:'123'}])
            queryStub.onCall(2).resolves([{count:1}])

            const result = await get(req, res, next)

            expect(queryStub.callCount).to.be.equal(1, 'query must be called exactly once')
            expect(errorBuilderStub.calledOnce).to.be.false('errorBuilder must not called')
        })
    })
})