/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const processQueryHelper = require('../../helpers/processQuery/index')
const { sequelize } = require('../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../helpers/error-builder')

let req
const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}

describe('Data Collection Summaries', () => {
    describe('/GET data-collection-summaries', () => {
        const { get } = require('../../routes/v3/data-collection-summaries/index')
        const sandbox = sinon.createSandbox()
        let getOrderStringStub
        let queryStub
        let getCountFinalSqlQueryStub
        let errorBuilderStub
        let testingArgs = [
            {
                word: 'SINGLE occurrenceDbId, trait column header type, empty crossTabColumns, and empty selectColumns',
                occurrenceDbId: '3388',
                columnHeaderType: 'trait',
                queryCallCount: 3,
                variableList: [{
                    'variableId': 'a',
                    'traitNames': 'b,c',
                    'traitAbbrevs': 'c,d',
                    'crosstabColumns': null,
                    'selectColumns': null,
                }],
            },
            {
                word: 'MULTIPLE occurrenceDbIds, trait column header type, non-empty crossTabColumns, and empty selectColumns',
                occurrenceDbId: '3388,3389,3390',
                columnHeaderType: 'trait',
                queryCallCount: 3,
                variableList: [{
                    'variableId': 'a',
                    'traitNames': 'b,c',
                    'traitAbbrevs': 'c,d',
                    'crosstabColumns': 'w,x',
                    'selectColumns': null,
                }],
            },
            {
                word: 'SINGLE occurrenceDbId, occurrence column header type, empty crossTabColumns, and non-empty selectColumns',
                occurrenceDbId: '3388',
                columnHeaderType: 'occurrence',
                queryCallCount: 4,
                variableList: [{
                    'variableId': 'a',
                    'traitNames': 'b,c',
                    'traitAbbrevs': 'c,d',
                    'crosstabColumns': null,
                    'selectColumns': 'y,z',
                }],
            },
            {
                word: 'MULTIPLE occurrenceDbIds, occurrences column header type, non-empty crossTabColumns, and non-empty selectColumns',
                occurrenceDbId: '3388,3389,3390',
                columnHeaderType: 'occurrence',
                queryCallCount: 4,
                variableList: [{
                    'variableId': 'a',
                    'traitNames': 'b,c',
                    'traitAbbrevs': 'c,d',
                    'crosstabColumns': 'w,x',
                    'selectColumns': 'y,z',
                }],
            },
        ]
        let variableList = [{
            'variableId': '1',
            'traitNames': 'b,c',
            'traitAbbrevs': 'c,d',
            'crosstabColumns': 'w',
            'selectColumns': 'y',
        }]

        beforeEach(() => {
            getOrderStringStub = sandbox.stub(processQueryHelper, 'getOrderString')
            queryStub = sandbox.stub(sequelize, 'query')
            getCountFinalSqlQueryStub = sandbox.stub(processQueryHelper, 'getCountFinalSqlQuery')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            errorStub = sandbox.stub(errors, 'BadRequestError')
        })

        afterEach(() => {
            sandbox.restore()
        })

        testingArgs.forEach((args) => {
            it('should successfully retrieve data collection summaries ' +
            `with ${args.word} in query`, async () => {
                req = {
                    headers: {},
                    query: {
                        occurrenceDbId: args.occurrenceDbId,
                        columnHeaderType: args.columnHeaderType,
                    },
                    paginate: {
                        limit: 100,
                        offset: 0,
                    }
                }
                let queryCallCount = 0

                queryStub.onCall(queryCallCount++).resolves(args.variableList)

                if (args.columnHeaderType == 'occurrence') {
                    queryStub.onCall(queryCallCount++).resolves(['occurrenceNames'])
                }
 
                queryStub.onCall(queryCallCount++).resolves(['dataCollectionSummary1, dataCollectionSummary2, dataCollectionSummary3'])
                getCountFinalSqlQueryStub.resolves(100)
                queryStub.onCall(queryCallCount++).resolves([ { count: 100 } ])

                await get(req, res, next)

                expect(getOrderStringStub.called).to.be.false('sort should be undefined')
                expect(queryStub.callCount).to.equal(args.queryCallCount, `query must be called exactly ${args.queryCallCount} times`)
                expect(getCountFinalSqlQueryStub.calledOnce).to.be.true('summaries must be > 0')
                expect(errorBuilderStub.called).to.be.false('no error should have been called')
            })
        })
    
        it('should generate BadRequestError if query parameter lacks ' +
            ' occurrenceDbId', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '',
                    columnHeaderType: 'trait',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            errorStub.resolves('BadRequestError')

            await get(req, res, next)

            expect(errorStub.calledOnce).to.be.true('errorBuilder must be called once')
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate BadRequestError if occurrence ID is not an ' +
            'integer', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: 'occ',
                    columnHeaderType: 'trait',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            errorBuilderStub.resolves('BadRequestError')

            await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400241)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('should generate BadRequestError if the order string ' +
            'is invalid', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '123',
                    columnHeaderType: 'trait',
                    sort: 'sort1'
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            getOrderStringStub.resolves('invalid')
            errorBuilderStub.resolves('BadRequestError')

            await get(req, res, next)

            expect(queryStub.called).to.be.false('query must not be called')
            expect(getOrderStringStub.calledOnce).to.be.true('getOrderString must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400004)
        })

        it('should generate Internal when the query for retrieving ' +
            'the data collection summary records fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '123',
                    columnHeaderType: 'occurrence',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(variableList)

            queryStub.onCall(1).resolves(['occurrenceNames'])
            queryStub.onCall(2).rejects()

            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            expect(queryStub.calledThrice).to.be.true('query must be called thrice')
        })

        it('should generate InternalError if the data collection summary count query fails', async () => {
            req = {
                headers: {
                    host: 'testHost'
                },
                query: {
                    occurrenceDbId: '123',
                    columnHeaderType: 'occurrence',
                },
                paginate: {
                    limit: 100,
                    offset: 0
                }
            }

            queryStub.onCall(0).resolves(variableList)

            queryStub.onCall(1).resolves(['occurrenceNames'])
            queryStub.onCall(2).resolves(['dataCollectionSummary1, dataCollectionSummary2, dataCollectionSummary3'])
            queryStub.onCall(3).rejects()

            errorBuilderStub.resolves('InternalError')

            await get(req, res, next)

            expect(queryStub.callCount).to.equal(4, `query must be called exactly 4 times`)
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
        })
    })
})