/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { expect } = require('chai')
const { sequelize } = require('../../config/sequelize')
const errorBuilder = require('../../helpers/error-builder')
const errors = require('restify-errors')

const tokenHelper = require('../../helpers/auth/token')
const userValidator = require('../../helpers/person/validator')
const processQueryHelper = require('../../helpers/processQuery')
const germplasmHelper = require('../../helpers/germplasm/index')

let req = {
    headers: {
        host: 'testHost'
    }
}

const res = {
    send: (statusCode, data) => {
        return {
            statusCode,
            data
        }
    }
}
const next = {}
const transactionMock = {
    finished: "",
    commit: function () {
        this.finished = 'commit'
    },
    rollback: function () {
        this.finished = 'rollback'
    }
}

describe('Germplasm Names', () => {
    const { delete: del, put } = require('../../routes/v3/germplasm-names/_id/index')

    describe('/PUT germplasm-names/{id}', () => {
        const sandbox = sinon.createSandbox()

        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub
        let isAdminStub
        let getScaleValueByAbbrevStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            getScaleValueByAbbrevStub = sandbox.stub(germplasmHelper, 'getScaleValueByAbbrev')

            transactionMock.finished = ""
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('throws a BadRequestError when the ID value is null',
            async () => {

                tokenHelperStub.onCall(0).resolves(null)
                errorBuilderStub.onCall(0).resolves('BadRequestError')

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
                expect(queryStub.called).to.be.false('query must not be called')
            })

        it('throws a BadRequestError when user is not an ADMIN',
            async () => {

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(false)
                errorBuilderStub.onCall(0).resolves('BadRequestError')

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401024)
                expect(queryStub.called).to.be.false('query must not be called')
            })

        it('throws a BadRequestError when the ID value is not provided',
            async () => {
                req['params'] = {}

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                errorBuilderStub.resolves('BadRequestError')

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.called).to.be.false('query must not be called')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            })

        it('throws a BadRequestError when the ID value is not an integer',
            async () => {
                req['params'] = { id: "abc" }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                errorBuilderStub.resolves('BadRequestError')

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.called).to.be.false('query must not be called')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
            })

        it('throws BadRequestError when request body is not specified',
            async () => {
                req['params'] = { id: "123" }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                errorBuilderStub.onCall(0).resolves('BadRequestError')

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400009)
                expect(queryStub.called).to.be.false('query must not be called')
            })

        it('throws InternalError when retrieval of germplasm name record fails',
            async () => {
                // assemble
                req['params'] = { id: "123" }
                req['body'] = {}

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).rejects()
                errorBuilderStub.onCall(0).resolves('InternalError')

                // act
                const result = await put(req, res, next)

                //assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('throws a NotFoundError when the germplasm name record is not found',
            async () => {
                // assemble
                req['params'] = { id: "123" }
                req['body'] = {}

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves([])

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
            })

        it('should successfully update the germplasm name record when '
            + 'no values are specified in the request body', async () => {
                req['params'] = { id: "123" }
                req['body'] = {}

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{}])

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(2, 'query must be called twice')
                expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            })

        it('should sucessfully update the germplasm name record when '
            + 'all inputed values are equals to the current values', async () => {

                req['params'] = { id: "123" }
                req['body'] = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line_name",
                    germplasmNormalizedName: "TESTNAME"
                }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line_name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                getScaleValueByAbbrevStub.onCall(0).resolves(['line_name', 'derivative_name'])
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{ count: 1 }])
                queryStub.onCall(2).resolves([{}])

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(getScaleValueByAbbrevStub.callCount).to.equal(1, 'getScaleValueByAbbrev must be called once')
                expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
                expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            })

        it('should sucessfully update the germplasm name record when '
            + 'all inputed values are not equals to the current values', async () => {

                req['params'] = { id: "123" }
                req['body'] = {
                    germplasmDbId: "891",
                    nameValue: "test name 1",
                    germplasmNameStatus: "stanard",
                    germplasmNameType: "derivative_name",
                    germplasmNormalizedName: "TESTNAME1"
                }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line_name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                getScaleValueByAbbrevStub.onCall(0).resolves(['line_name', 'derivative_name'])
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{ count: 1 }])
                queryStub.onCall(2).resolves([{}])

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(getScaleValueByAbbrevStub.callCount).to.equal(1, 'getScaleValueByAbbrev must be called once')
                expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
                expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            })

        it('should throw BadRequestError when a parameter provided is empty',
            async () => {
                req['params'] = { id: "456" }
                req['body'] = { nameValue: "" }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                errorBuilderStub.onCall(0).resolves('BadRequestError')
                transactionStub.onCall(0).resolves(transactionMock)

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            })

        it('should throw BadRequestError when germplasmDbId is not an integer',
            async () => {
                req['params'] = { id: "456" }
                req['body'] = { germplasmDbId: "ID" }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                errorBuilderStub.onCall(0).resolves('BadRequestError')
                transactionStub.onCall(0).resolves(transactionMock)

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            })

        it('should throw BadRequestError when germplasmNameStatus is not a valid value',
            async () => {
                req['params'] = { id: "456" }
                req['body'] = { germplasmNameStatus: "" }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                errorBuilderStub.onCall(0).resolves('BadRequestError')
                transactionStub.onCall(0).resolves(transactionMock)

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            })

        it('should throw BadRequestError when germplasmNameType is not a valid value',
            async () => {
                req['params'] = { id: "456" }
                req['body'] = { germplasmNameType: "test_type" }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line_name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                getScaleValueByAbbrevStub.onCall(0).resolves(['line_name'])
                errorBuilderStub.onCall(0).resolves('BadRequestError')
                transactionStub.onCall(0).resolves(transactionMock)

                // act
                const result = await put(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(1, 'query must be called once')
                expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            })

        it('should sucessfully update the germplasmDbId record when '
            + 'the germplasmDbId provided is in valid format', async () => {
                req['params'] = { id: "123" }
                req['body'] = { germplasmDbId: "890" }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line_name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{ count: 1 }])
                queryStub.onCall(2).resolves([{}])

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(3, 'query must be called thrice')
                expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            })

        it('should sucessfully update the germplasm name record when '
            + 'the germplasmNameStatus is a valid status value', async () => {

                req['params'] = { id: "123" }
                req['body'] = { germplasmNameStatus: "active" }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line_name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{ count: 1 }])
                queryStub.onCall(2).resolves([{}])

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(2, 'query must be called thrice')
                expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')

            })

        it('should sucessfully update the germplasm name record when '
            + 'the germplasmNameType is a valid status value', async () => {

                req['params'] = { id: "123" }
                req['body'] = { germplasmNameType: "line_name" }

                germplasmName = {
                    germplasmDbId: "890",
                    nameValue: "test name",
                    germplasmNameStatus: "active",
                    germplasmNameType: "line_name",
                    germplasmNormalizedName: "TESTNAME"
                }

                tokenHelperStub.onCall(0).resolves(123)
                isAdminStub.onCall(0).resolves(true)
                queryStub.onCall(0).resolves(germplasmName)
                getScaleValueByAbbrevStub.onCall(0).resolves(['line_name'])
                transactionStub.onCall(0).resolves(transactionMock)
                queryStub.onCall(1).resolves([{ count: 1 }])
                queryStub.onCall(2).resolves([{}])

                const result = await put(req, res, next)

                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
                expect(queryStub.callCount).to.equal(2, 'query must be called thrice')
                expect(getScaleValueByAbbrevStub.callCount).to.equal(1, 'getScaleValueByAbbrev must be called once')
                expect(transactionStub.callCount).to.equal(1, 'transaction must be called once')
            })

        it('should throw BadRequestError when validation fails', async () => {
            req['params'] = { id: "123" }
            req['body'] = { germplasmDbId: "456" }

            germplasmName = {
                germplasmDbId: "890",
                nameValue: "test name",
                germplasmNameStatus: "active",
                germplasmNameType: "line_name",
                germplasmNormalizedName: "TESTNAME"
            }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves(germplasmName)
            transactionStub.onCall(0).resolves(transactionMock)
            queryStub.onCall(1).resolves([{ count: 0 }])
            queryStub.onCall(2).resolves([{}])
            errorBuilderStub.onCall(0).resolves('BadRequestError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 400015)
        })

        it('should generate an InternalError when an error occurs', async () => {
            req['params'] = { id: "123" }
            req['body'] = { germplasmDbId: "456" }

            germplasmName = {
                germplasmDbId: "890",
                nameValue: "test name",
                germplasmNameStatus: "active",
                germplasmNameType: "line_name",
                germplasmNormalizedName: "TESTNAME"
            }

            tokenHelperStub.onCall(0).resolves(123)
            isAdminStub.onCall(0).resolves(true)
            queryStub.onCall(0).resolves(germplasmName)
            transactionStub.onCall(0).rejects()
            errorBuilderStub.onCall(0).resolves('InternalError')

            const result = await put(req, res, next)

            expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
            expect(isAdminStub.callCount).to.equal(1, 'isAdmin must be called once')
            expect(queryStub.callCount).to.equal(2, 'query must be called twice')
            expect(transactionStub.callCount).to.equal(0, 'transaction must not be called')
            expect(errorBuilderStub.callCount).to.equal(1, 'errorBuilder must be called once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500001)
        })
    })

    describe('/DELETE germplasm-names/{id}', () => {
        const sandbox = sinon.createSandbox()

        let tokenHelperStub
        let errorBuilderStub
        let queryStub
        let transactionStub
        let isAdminStub
        let getScaleValueByAbbrevStub

        beforeEach(() => {
            tokenHelperStub = sandbox.stub(tokenHelper, 'getUserId')
            errorBuilderStub = sandbox.stub(errorBuilder, 'getError')
            queryStub = sandbox.stub(sequelize, 'query')
            transactionStub = sandbox.stub(sequelize, 'transaction')
            isAdminStub = sandbox.stub(userValidator, 'isAdmin')
            getScaleValueByAbbrevStub = sandbox.stub(germplasmHelper, 'getScaleValueByAbbrev')

            transactionMock.finished = ""
        })

        afterEach(() => {
            sandbox.restore()
        })

        it('should throw BadRequestError when user ID is invalid', async () => {
            tokenHelperStub.resolves(null)
            errorBuilderStub.resolves('BadRequestError')

            // act
            const result = await del(req, res, next)

            // assert
            expect(tokenHelperStub.calledOnce).to.true('tokenHelper must be called once')
            expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
            sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 401002)
            expect(queryStub.called).to.be.false('query must not be called')
        })

        it('throws a BadRequestError when the ID value is not an integer',
            async () => {
                req['params'] = { id: "abc" }

                tokenHelperStub.resolves(123)

                // act
                const result = await del(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(queryStub.called).to.be.false('query must not be called')
            })

        it('throws a InternalError when query for germplasm name fails',
            async () => {
                req['params'] = { id: "123" }

                tokenHelperStub.resolves(123)
                transactionStub.resolves(transactionMock)
                queryStub.onCall(0).rejects()
                errorBuilderStub.resolves('InternalError')

                // act
                const result = await del(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500004)
            })

        it('throws a InternalError when query for germplasm name fails',
            async () => {
                req['params'] = { id: "123" }

                tokenHelperStub.resolves(123)
                transactionStub.resolves(transactionMock)
                queryStub.onCall(0).resolves([{ id: '1' }])
                queryStub.onCall(1).rejects()
                errorBuilderStub.resolves('InternalError')

                // act
                const result = await del(req, res, next)

                // assert
                expect(tokenHelperStub.callCount).to.equal(1, 'tokenHelper must be called once')
                expect(errorBuilderStub.calledOnce).to.be.true('errorBuilder must be called exactly once')
                sinon.assert.calledWithExactly(errorBuilderStub, 'testHost', 500002)
            })
    })
})