/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('./config/sequelize')
let errorBuilder = require('./helpers/error-builder')
let logger = require('./helpers/logger/index.js')
const restify = require('restify')
const paginate = require('./helpers/pagination')
const routerMagic = require('restify-router-magic')
const responseFormatter = require('./helpers/response-formatter')
const errors = require('restify-errors')
const compression = require('compression')
const corsMiddleware = require('restify-cors-middleware2')
const cookieParser = require('restify-cookies')
const fs = require('fs')
const jwksClient = require('jwks-rsa')
const { createVerifier, createDecoder } = require('fast-jwt')

// Load configuration files
require('dotenv').config({ path: 'config/.env' })

// Initialize Restify server
const server = restify.createServer({
    formatters: {
        'application/json': responseFormatter
    }
})

server.use(compression())
server.use(cookieParser.parse)

server.pre(restify.plugins.pre.userAgentConnection())

const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['Authorization'],
    exposeHeaders: ['Authorization']
})

server.pre(cors.preflight)
server.use(cors.actual)

server.get('/', restify.plugins.serveStatic({
    directory: __dirname,
    default: 'index.html'
}))

server.get('/responses.html', restify.plugins.serveStatic({
    directory: __dirname,
}))

server.get('/css/*', restify.plugins.serveStatic({
    directory: __dirname,
}))

server.get('/assets/*', restify.plugins.serveStatic({
    directory: __dirname,
}))

server.get('/js/*', restify.plugins.serveStatic({
    directory: __dirname,
}))

server.get('/errors.json', restify.plugins.serveStatic({
    directory: __dirname,
}))

server.get('/swagger-paths/*', restify.plugins.serveStatic({
    directory: __dirname,
}))

let swaggerUi = require('swagger-ui-restify'),
    swaggerDocument = require('./swagger.json')

let serverUrl = "https://" + process.env.CBAPI_HOST + "/v3"

let options

if(process.env.NODE_ENV == "production"){
    options = {
        swaggerOptions: {
            supportedSubmitMethods: [],
        }
    }
    swaggerDocument.components.securitySchemes = null
}else{
    swaggerDocument.servers = [
        {
            "url":  serverUrl 
        }
    ]
}


server.get('*/swagger-ui+.*', ...swaggerUi.serve)
server.get('/v3/docs', swaggerUi.setup(swaggerDocument, options))

// Ensure that protected paths cannot be accessed without authorization
const client = jwksClient({
    strictSsl: false,
    jwksUri: process.env.CBAPI_JWKS_URI
})

// Route middleware to verify a token
server.use(
    async (req, res, next) => {

        let path = req.route.path.replace(/\?.*$/, '')

        let url = req.url

        let whitelist = [
            '/v3/auth/login',
            '/v3/auth/token',
            '/v3/auth/callback',
            '/v3/status',
            '/',
            '/v3/docs',
            '/v3/docs#/',
            '/v3/swagger-ui-init.js',
            '/v3/favicon-32x32.png',
            '/v3/favicon-16x16.png',
            '/v3/swagger-ui-standalone-preset.js',
            '/v3/swagger-ui.css',
            '/v3/swagger-ui-bundle.js',
            '/v3/swagger-ui-standalone-preset.js',
            '/favicon.ico',
            'errors.json',
            '/errors.json',
            '/responses.html',
            '/css/*',
            '/assets/*',
            '/swagger-paths/*',
            '*/swagger-ui+.*'
        ]

        if (whitelist.indexOf(path) > -1 || whitelist.indexOf(url) > -1) {
            return next()
        }
        else {

            let token = req.headers['x-access-token'] || req.headers['authorization']

            if (token == undefined || token.length <= 0) {
                let errMsg = errorBuilder.getError(req.headers.host, 401016)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }

            token = token.slice(7, token.length).trimLeft().trim()

            try {

                const verifyWithCallback = createVerifier({
                    key: getKey,
                    complete: true,
                    cache: true,
                    algorithms: ['HS256', 'HS384', 'HS512', 'RS256']
                })

                verifyWithCallback(token, (err, sections) => {

                    if (err) {
                        logger.logMessage(__filename, 'Request: '+ url + ' Token Verification ' + err, 'error')
                        let errMsg = errorBuilder.getError(req.headers.host, 401016)
                        res.send(new errors.UnauthorizedError(errMsg))
                        return
                    }
                    else {

                        const decode = createDecoder()
                        let payload = decode(token)
                        let email = (payload['http://wso2.org/claims/emailaddress'] != undefined) ?
                            payload['http://wso2.org/claims/emailaddress'] : payload['email']

                        let personQuery = `
                            SELECT
                                person.id
                            FROM 
                                tenant.person person
                            WHERE
                                lower(person.email) = lower($$${email}$$) AND
                                person.is_void = FALSE
                        `

                        let person = sequelize.query(personQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                            .then(data => {
                                if (data != null && data != undefined && data.length > 0) {
                                    return next()
                                }
                                else {
                                    let errMsg = errorBuilder.getError(req.headers.host, 401016)
                                    res.send(new errors.UnauthorizedError(errMsg))
                                    return
                                }
                            })
                            .catch(async err => {
                                logger.logMessage(__filename, 'Error fetching tenant.person info: ' + err, 'error')
                                let errMsg = errorBuilder.getError(req.headers.host, 500004)
                                res.send(new errors.InternalError(errMsg))
                                return
                            })
                    }
                })
            } catch (err) {
                logger.logMessage(__filename, 'Unauthorized: ' + err, 'error')
                let errMsg = errorBuilder.getError(req.headers.host, 401016)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }
    })

server.use(
    restify.plugins.queryParser({
        mapParams: true
    })
)

server.use(
    restify.plugins.bodyParser({
        mapParams: true
    })
)

server.use(
    paginate(server, {
        paramsNames: {
            page: 'page',
            per_page: 'limit'
        },
        defaults: {
            per_page: 100
        }
    }),
    (req, res, next) => {
        if (req.paginate.per_page > 100) {
            res.send(
                new errors.BadRequestError(
                    'Supported page size is only up to 100 records.'
                )
            )
            return
        }

        req.paginate.limit = req.paginate.per_page
        req.paginate.offset = (req.paginate.page - 1) * req.paginate.per_page

        next()
    }
)

// Load router
routerMagic(server, [{ indexWithSlash: 'never' }])

// Start server
server.listen(process.env.CBAPI_PORT, function () {
    console.log('Server is now running...')
})

function loadStaticFile(req, res, next) {
    var filePath = __dirname + getFileName(req)

    fs.readFile(filePath, function (err, data) {
        if (err) {
            res.writeHead(500)
            res.end('')
            next(err)
            return
        }
        res.header('Content-Type', 'text/html')
        res.send(data)

        return next()
    })
}

function getFileName(req) {
    let filename = ''
    if (req.url.indexOf('/') == req.url.length - 1) {
        filename = req.url + 'index.html'
    }

    return filename
}

function getKey(header, callback) {
    client.getSigningKey(header.kid, (err, key) => {
        let signingKey = key.publicKey || key.rsaPublicKey
        callback(null, signingKey)
    })
}


module.exports = server

// Catch and log unhandled rejections and uncaught exceptions
process
  .on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
  })
  .on('uncaughtException', err => {
    console.error(err, 'Uncaught Exception thrown');
  })
  .on('warning', e => console.warn(e.stack));