/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const Sequelize = require('sequelize')
const path = require('path')
require('dotenv').config({path:'config/.env'})

// Define database models
const AuthorizationModel = require('../models/authorization.js')

const sequelize = new Sequelize(process.env.CBAPI_DB_NAME,process.env.CBAPI_DB_USERNAME,process.env.CBAPI_DB_PASSWORD, {
  host: process.env.CBAPI_DB_HOST,
  port: process.env.CBAPI_DB_PORT,
  dialect: 'postgres',
  logging: false,
  pool: {
    max: 450,
    min: 0,
    idle: 5000,
    evict: 30000
  },
  dialectOptions: {
    application_name: 'CB-API: Sequelize connection pool'
  }
});

const Authorization = AuthorizationModel(sequelize,Sequelize)

module.exports = {
    Authorization, sequelize
}