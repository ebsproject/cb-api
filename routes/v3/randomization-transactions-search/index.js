/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errorBuilder = require('../../../helpers/error-builder')
let errors = require('restify-errors')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']
      // Get the filter condition
      conditionString = await processQueryHelper
        .getFilter(req.body, excludedParametersArray)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let randomizationTransactionsQuery = null
    // Check if the client specified values for the field/s
    if (parameters['fields']) {
      randomizationTransactionsQuery =
        knex.column(parameters['fields'].split('|'))
      randomizationTransactionsQuery += `
        FROM
          operational_data_terminal.randomization_transaction "randomizationTransaction"
        LEFT JOIN
          master.user actor ON "randomizationTransaction".actor_id = actor.id
        LEFT JOIN
          master.user creator ON "randomizationTransaction".creator_id = creator.id
        LEFT JOIN
          master.user modifier ON "randomizationTransaction".modifier_id = modifier.id
        LEFT JOIN
          operational.study study ON "randomizationTransaction".study_id = study.id
        WHERE
          "randomizationTransaction".is_void = FALSE
        ORDER BY
          "randomizationTransaction".id
      `
    } else {
      randomizationTransactionsQuery = `
        SELECT
          randomizationTransaction.id AS "randomizationTransactionDbId",
          randomizationTransaction.type,
          study.id AS "studyDbId",
          randomizationTransaction.input_file AS "inputFile",
          actor.id AS "actorDbId",
          randomizationTransaction.start_action_timestamp AS "startActionTimestamp",
          randomizationTransaction.end_action_timestamp AS "endActionTimestamp",
          randomizationTransaction.is_successful AS "isSuccessful",
          randomizationTransaction.generated_input AS "generatedInput",
          randomizationTransaction.randomization_output AS "randomizationOutput",
          randomizationTransaction.elapsed_time AS "elapsedTime",
          randomizationTransaction.data_results AS "dataResults",
          randomizationTransaction.plan_id AS "planDbId",
          randomizationTransaction.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.display_name AS "creator",
          randomizationTransaction.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.display_name AS "modifier"
        FROM
          operational_data_terminal.randomization_transaction randomizationTransaction
        LEFT JOIN
          master.user actor ON randomizationTransaction.actor_id = actor.id
        LEFT JOIN
          master.user creator ON randomizationTransaction.creator_id = creator.id
        LEFT JOIN
          master.user modifier ON randomizationTransaction.modifier_id = modifier.id
        LEFT JOIN
          operational.study study ON randomizationTransaction.study_id = study.id
        WHERE
          randomizationTransaction.is_void = FALSE
        ORDER BY
          randomizationTransaction.id
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    randomizationTransactionsFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        randomizationTransactionsQuery,
        conditionString,
        orderString
      )

    // Retrieve the randomization transaction records
    let randomizationTransactions = await sequelize
      .query(randomizationTransactionsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (
      await randomizationTransactions == undefined ||
      await randomizationTransactions.length < 1
    ) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let randomizationTransactionsCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        randomizationTransactionsQuery,
        conditionString,
        orderString
      )
    
    let randomizationTransactionsCount = await sequelize
      .query(randomizationTransactionsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    count = randomizationTransactionsCount[0].count

    res.send(200, {
      rows: randomizationTransactions,
      count: count
    })
    return
  }
}