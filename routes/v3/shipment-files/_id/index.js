/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let validator = require("validator");
let userValidator = require("../../../../helpers/person/validator.js");
let tokenHelper = require("../../../../helpers/auth/token.js");
let errorBuilder = require("../../../../helpers/error-builder");
let format = require("pg-format");

module.exports = {
  // Endpoint for voiding an existing file-cabinet record
  // DELETE /v3/shipment-file/:id
  delete: async function (req, res, next) {
    // Retrieve the file cabinet ID
    let shipmentFileDbId = req.params.id;

    if (!validator.isInt(shipmentFileDbId)) {
      let errMsg = `Invalid format, shipment file  ID must be an integer.`;
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req);

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    let isAdmin = await userValidator.isAdmin(personDbId);

    let transaction;
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false });

      let shipmentFileQuery = `
        SELECT
         shipment_file.id
        FROM
          inventory.shipment_file shipment_file
        WHERE
          shipment_file.is_void = FALSE AND
          shipment_file.id = ${shipmentFileDbId}
      `;

      let shipmentFile = await sequelize
        .query(shipmentFileQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      if (
        (await shipmentFile) == undefined ||
        (await shipmentFile.length) < 1
      ) {
        let errMsg = `The shipment file you have requested does not exist.`;
        res.send(new errors.NotFoundError(errMsg));
        return;
      }

      let deleteshipmentFileQuery = format(`
        UPDATE
          inventory.shipment_file
        SET
          is_void = TRUE,
          remarks = CONCAT('VOIDED-', $$${shipmentFileDbId}$$),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          id = ${shipmentFileDbId}
      `);

      await sequelize.query(deleteshipmentFileQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      await transaction.commit();
      res.send(200, {
        rows: { shipmentFileDbId: shipmentFileDbId },
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500002);
      res.send(new errors.InternalError(errMsg));
      return;
    }
  },
};
