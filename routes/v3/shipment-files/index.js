/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../config/sequelize");
let errors = require("restify-errors");
let validator = require("validator");
let tokenHelper = require("../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let format = require("pg-format");
let errorBuilder = require("../../../helpers/error-builder");

module.exports = {
  // Endpoint for adding a file cabinet record
  post: async function (req, res, next) {
    let resultArray = [];

    // Retrieve the ID of the client via the access token
    let personDbId = await tokenHelper.getUserId(req);

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentFileUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipment-files";

    // Check for parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Parse the input
    let data = req.body;
    let records = [];

    try {
      records = data.records;
      recordCount = records.length;

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004);
      res.send(new errors.InternalError(errMsg));
      return;
    }

    let transaction;
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false });

      let shipmentFileValuesArray = [];

      for (var record of records) {
        let validateQuery = ``;
        let validateCount = 0;

        // Declare variables
        let fileName = null;
        let fileType = null;
        let shipmentDbId = null;
        let filePath = null;
        let fileSize = null;

        // Check if the required parameters are in the body
        if (record.fileName == undefined || record.fileType == undefined) {
          let errMsg = `Required parameters are missing. Ensure that the fileName, fileType fields are not empty.`;
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        fileName = record.fileName != undefined ? record.fileName : null;
        fileType = record.fileType != undefined ? record.fileType : null;
        filePath = record.filePath != undefined ? record.filePath : null;
        fileSize = record.fileSize != undefined ? record.fileSize : null;

        if (record.shipmentDbId != undefined) {
          shipmentDbId = record.shipmentDbId;
          // Check if it is a valid integer
          if (!validator.isInt(shipmentDbId)) {
            let errMsg = "Invalid format, occurrence ID must be an integer.";
            res.send(new errors.BadRequestError(errMsg));
            return;
          }

          // Check if it actually exists
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
              --- Check if shipment exists, return 1
              SELECT
                count(1)
              FROM
                inventory.shipment shipment
              WHERE
                shipment.is_void = FALSE AND
                shipment.id = ${shipmentDbId}
            )
          `;
          validateCount += 1;
        }

        // Validate the input
        let checkInputQuery = `
          SELECT ${validateQuery} AS count
        `;

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT,
        });

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015);
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        //check if existing file exist in the same shipment transaction
        // Build query
        let shipmentFileQuery = `
                SELECT shipment_file.file_path AS file_path
                FROM inventory.shipment_file shipment_file
                WHERE
                shipment_file.is_void = FALSE AND
                shipment_file.file_path = '${filePath}'
        `;

        let shipmentFile = await sequelize
          .query(shipmentFileQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
              filePath: filePath,
            },
          })
          .catch(async (err) => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004);
            res.send(new errors.InternalError(errMsg));
            return;
          });

        if ((await shipmentFile.length) > 0) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400032);
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Get values
        let tempArray = [
          fileName,
          fileType,
          filePath,
          shipmentDbId,
          fileSize,
          personDbId,
        ];

        shipmentFileValuesArray.push(tempArray);
      }

      // Create a shipment file record
      let shipmentFilesQuery = format(
        `
        INSERT INTO
          inventory.shipment_file (
            file_name, file_type, file_path, shipment_id, file_size, creator_id
          )
        VALUES
          %L
        RETURNING
          id
        `,
        shipmentFileValuesArray
      );

      let shipmentFiles = await sequelize.query(shipmentFilesQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction,
      });

      for (shipmentFile of shipmentFiles[0]) {
        shipmentFileDbId = shipmentFile.id;
        let fcObj = {
          shipmentFileDbId: shipmentFileDbId,
          recordCount: 1,
          href: shipmentFileUrlString + "/" + shipmentFileDbId,
        };
        resultArray.push(fcObj);
      }

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: resultArray,
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500001);
      res.send(new errors.InternalError(errMsg));
      return;
    }
  },
};
