/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let patternHelper = require('../../../helpers/patternGenerator/index.js')

module.exports = {

    // Endpoint for creating a new location record
    // POST /v3/locations
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []

        let locationDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let locationUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/locations'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let locationValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Set defaults
                let locationCode = null
                let locationName = null
                let locationStatus = null
                let locationType = null
                let description = null
                let stewardDbId = null
                let plantingDate = null
                let harvestDate = null
                let geospatialObjectDbId = null
                let locationYear = null
                let seasonDbId = null
                let siteDbId = null
                let fieldDbId = null

                let locationYearRegex = /^\d{4}$/

                // Check if required columns are in the request body
                if (
                    !record.locationStatus ||
                    !record.stewardDbId || !record.geospatialObjectDbId ||
                    !record.locationYear || !record.seasonDbId ||
                    !record.siteDbId
                ) {
                    let errMsg = `Required parameters are missing. Ensure that locationStatus, stewardDbId, geospatialObjectDbId, locationYear, siteDbId and seasonDbId fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */

                locationStatus = record.locationStatus.trim()
                stewardDbId = record.stewardDbId
                geospatialObjectDbId = record.geospatialObjectDbId
                locationYear = record.locationYear
                seasonDbId = record.seasonDbId

                siteDbId = record.siteDbId
                fieldDbId = record.fieldDbId

                // validation for location status
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'LOCATION_STATUS' AND
                            scaleValue.value ILIKE $$${locationStatus}$$
                    )
                `
                validateCount += 1

                // validation for steward ID
                if (record.stewardDbId !== undefined) {
                    stewardDbId = record.stewardDbId

                    if (!validator.isInt(stewardDbId)) {
                        let errMsg = `Invalid format, steward ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate steward ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if steward is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.person
                            WHERE 
                                is_void = FALSE AND
                                id = ${stewardDbId}
                        )
                    `
                    validateCount += 1
                }

                // validation for location year
                if (record.locationYear !== undefined) {
                    locationYear = record.locationYear

                    if (!validator.isInt(locationYear)) {
                        let errMsg = `Invalid format, location year must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (!locationYear.match(locationYearRegex)) {
                        let errMsg = `Invalid format, location year must be in this format: YYYY.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // validate location planting date
                if (record.plantingDate !== undefined) {
                    plantingDate = record.plantingDate

                    var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
                    if (!plantingDate.match(validDateFormat)) { // Invalid format
                        let errMsg = `Invalid format, planting date must be in format YYYY-MM-DD.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // validate location harvest date
                if (record.harvestDate !== undefined) {
                    harvestDate = record.harvestDate

                    var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
                    if (!harvestDate.match(validDateFormat)) { // Invalid format
                        let errMsg = `Invalid format, harvest date must be in format YYYY-MM-DD.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // validate geospatial object ID
                if (record.geospatialObjectDbId !== undefined) {
                    geospatialObjectDbId = record.geospatialObjectDbId

                    if (!validator.isInt(geospatialObjectDbId)) {
                        let errMsg = `Invalid format, geospatial object ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if geospatial object is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object
                            WHERE 
                                is_void = FALSE AND
                                id = ${geospatialObjectDbId}
                        )
                    `
                    validateCount += 1
                }

                // validate season ID
                if (record.seasonDbId !== undefined) {
                    seasonDbId = record.seasonDbId

                    if (!validator.isInt(seasonDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400063)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if season is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.season
                            WHERE 
                                is_void = FALSE AND
                                id = ${seasonDbId}
                        )
                    `
                    validateCount += 1
                }

                // validate site ID
                if (record.siteDbId !== undefined) {
                    siteDbId = record.siteDbId

                    if (!validator.isInt(siteDbId)) {
                        let errMsg = `Invalid format, site ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if site is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object
                            WHERE 
                                is_void = FALSE AND
                                geospatial_object_type = 'site' AND
                                id = ${siteDbId}
                        )
                    `
                    validateCount += 1
                }

                // validate field ID
                if (record.fieldDbId !== undefined) {
                    fieldDbId = record.fieldDbId

                    if (!validator.isInt(fieldDbId)) {
                        let errMsg = `Invalid format, field ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate field ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if field is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object
                            WHERE 
                                is_void = FALSE AND
                                id = ${fieldDbId}
                        )
                    `
                    validateCount += 1
                }

                // validation for location type
                if (record.locationType !== undefined) {
                    locationType = record.locationType.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'LOCATION_TYPE' AND
                                scaleValue.value ILIKE $$${locationType}$$
                        )
                    `
                    validateCount += 1
                }

                description = (record.description !== undefined) ? record.description : null

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // get season code
                let seasonCode = await patternHelper.getEntityValue('tenant',
                    'season', 'season_code', seasonDbId)
                seasonCode = seasonCode['season_code']

                // get site code
                let siteCode = await patternHelper.getEntityValue('place',
                    'geospatial_object', 'geospatial_object_code', siteDbId)
                siteCode = siteCode['geospatial_object_code']

                let counterQuery = `
                    SELECT 
                        MAX("orderNumber") + 1 as max
                    FROM (
                        SELECT 
                            location_number AS "orderNumber"
                        FROM 
                            experiment.location
                        WHERE
                            location_year= '${locationYear}' 
                            AND site_id = ${siteDbId} 
                            AND season_id = ${seasonDbId}
                            AND is_void = FALSE
                    )e
                `
                let maxCounter = await sequelize.query(counterQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
        
                if (maxCounter[0]['max'] == undefined) {
                    locationNumber = 1
                }else{
                    locationNumber = maxCounter[0]['max']
                }

                locationNumber = await patternHelper.getPaddedNumber(
                    locationNumber, 'leading', 3)

                locationCode = siteCode + '-' + locationYear + '-' + seasonCode
                    + '-' + locationNumber

                // if locationName is not specified, set locationName same as locationCode
                if (record.locationName == undefined) {
                    locationName = locationCode
                } else {
                    locationName = record.locationName
                }

                // get values
                let tempArray = [
                    locationCode, locationNumber, locationName, locationStatus,
                    locationType, description, stewardDbId, plantingDate, harvestDate,
                    geospatialObjectDbId, locationYear, seasonDbId, siteDbId,
                    fieldDbId, personDbId
                ]

                locationValuesArray.push(tempArray)
            }

            // Create location record
            let locationsQuery = format(`
                INSERT INTO
                    experiment.location (
                        location_code, location_number, location_name, location_status, location_type, description,
                        steward_id, location_planting_date, location_harvest_date,
                        geospatial_object_id, location_year, season_id, site_id, field_id, creator_id
                    )
                VALUES
                    %L
                RETURNING id`, locationValuesArray
            )

            let locations = await sequelize.query(locationsQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            for (location of locations[0]) {
                locationDbId = location.id

                let locationRecord = {
                    locationDbId: locationDbId,
                    recordCount: 1,
                    href: locationUrlString + '/' + locationDbId
                }

                resultArray.push(locationRecord)
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            console.log(err)
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}