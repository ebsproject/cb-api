/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let userValidator = require('../../../../helpers/person/validator.js')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for updating an existing location record
    // PUT /v3/locations/:id
    put: async function (req, res, next) {
        // Retrieve the location ID
        let locationDbId = req.params.id
        
        if(!validator.isInt(locationDbId)) {
            let errMsg = `Invalid format, location ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let locationName = null
        let locationStatus = null
        let locationType = null
        let description = null
        let geospatialObjectDbId = null
        let siteDbId = null
        let fieldDbId = null
        let plantingDate = null
        let harvestDate = null
        let stewardDbId = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if location is existing
        // Build query
        let locationQuery = `
            SELECT
                location.location_name AS "locationName",
                location.location_status AS "locationStatus",
                location.location_type AS "locationType",
                location.description,
                location.geospatial_object_id AS "geospatialObjectDbId",
                location.site_id AS "siteDbId",
                location.field_id AS "fieldDbId",
                location.steward_id AS "stewardDbId",
                location.location_planting_date AS "plantingDate",
                location.location_harvest_date AS "harvestDate"
            FROM
                experiment.location
            WHERE
                is_void = FALSE AND
                id = ${locationDbId}
        `

        // Retrieve location from the database
        let location = await sequelize.query(locationQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await location === undefined || await location.length < 1) {
            let errMsg = `The location you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordLocationName = location[0].locationName
        let recordLocationStatus = location[0].locationStatus
        let recordLocationType = location[0].locationType
        let recordDescription = location[0].description
        let recordGeospatialObjectDbId = location[0].geospatialObjectDbId
        let recordSiteDbId = location[0].siteDbId
        let recordFieldDbId = location[0].fieldDbId
        let recordPlantingDate = location[0].plantingDate
        let recordHarvestDate = location[0].harvestDate
        let recordStewardDbId = location[0].stewardDbId

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let locationUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/locations/"
            + locationDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body

        // Start transaction
        let transaction
        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/
            // validate location status
            if (data.locationStatus !== undefined) {
                locationStatus = data.locationStatus.trim()

                // Check if user input is same with the current value in the database
                if (locationStatus != recordLocationStatus) {

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'LOCATION_STATUS' AND
                                scaleValue.value ILIKE $$${locationStatus}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        location_status = $$${locationStatus}$$
                    `
                }
            }

            // update location name
            if (data.locationName !== undefined) {
                locationName = data.locationName

                // Check if user input is same with the current value in the database
                if (locationName != recordLocationName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        location_name = $$${locationName}$$
                    `
                }
            }

            // update description
            if (data.description !== undefined) {
                description = data.description

                // Check if user input is same with the current value in the database
                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        description = $$${description}$$
                    `
                }
            }

            // validate geospatial object ID
            if (data.geospatialObjectDbId !== undefined) {
                geospatialObjectDbId = data.geospatialObjectDbId

                if (!validator.isInt(geospatialObjectDbId)) {
                    let errMsg = `Invalid format, geospatial object ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (geospatialObjectDbId != recordGeospatialObjectDbId) {

                    // Validate geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if geospatial object is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object
                            WHERE 
                                is_void = FALSE AND
                                id = ${geospatialObjectDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        geospatial_object_id = $$${geospatialObjectDbId}$$
                    `
                }
            }

            // validate location type
            if (data.locationType !== undefined) {
                locationType = data.locationType.trim()
                
                // Check if user input is same with the current value in the database
                if (locationType != recordLocationType) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'LOCATION_TYPE' AND
                                scaleValue.value ILIKE $$${locationType}$$
                        )
                    `
                    validateCount += 1
                    
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        location_type = $$${locationType}$$
                    `
                }
            }

            // validate site ID
            if (data.siteDbId !== undefined) {
                siteDbId = data.siteDbId

                if (!validator.isInt(siteDbId)) {
                    let errMsg = `Invalid format, site ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (siteDbId != recordSiteDbId) {
                    // validate site ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if site is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            place.geospatial_object
                        WHERE 
                            is_void = FALSE AND
                            geospatial_object_type = 'site' AND
                            id = ${siteDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        site_id = $$${siteDbId}$$
                    `
                }
            }

            // validate field ID
            if (data.fieldDbId !== undefined) {
                fieldDbId = data.fieldDbId

                if (!validator.isInt(fieldDbId)) {
                    let errMsg = `Invalid format, field ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (fieldDbId != recordFieldDbId) {
                    // validate field ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if field is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object
                            WHERE 
                                is_void = FALSE AND
                                id = ${fieldDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        field_id = $$${fieldDbId}$$
                    `
                }
            }

            // validate planting date
            if (data.plantingDate !== undefined) {
                plantingDate = data.plantingDate

                var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
                if (!plantingDate.match(validDateFormat)) { // Invalid format
                    let errMsg = `Invalid format, planting date must be in format YYYY-MM-DD.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if(plantingDate != recordPlantingDate) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        location_planting_date = $$${plantingDate}$$
                    `
                }
            }

            // validate harvest date
            if (data.harvestDate !== undefined) {
                harvestDate = data.harvestDate

                var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
                if (!harvestDate.match(validDateFormat)) { // Invalid format
                    let errMsg = `Invalid format, harvest date must be in format YYYY-MM-DD.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                    }

                // Check if user input is same with the current value in the database
                if(harvestDate != recordHarvestDate) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        location_harvest_date = $$${harvestDate}$$
                    `
                }
            }

            // validate steward ID
            if (data.stewardDbId !== undefined) {
                stewardDbId = data.stewardDbId

                if (!validator.isInt(stewardDbId)) {
                    let errMsg = `Invalid format, steward ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if(stewardDbId != recordStewardDbId) {
                    // Validate steward ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if steward is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.person
                            WHERE 
                                is_void = FALSE AND
                                id = ${stewardDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        steward_id = $$${stewardDbId}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the location record
            let updateLocationQuery = `
                UPDATE
                    experiment.location  
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${locationDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updateLocationQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let resultArray = {
                locationDbId: locationDbId,
                recordCount: 1,
                href: locationUrlString
            }
            
            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },
    // Endpoint for voiding an existing location record
    // DELETE /v3/locations/:id
    delete: async function (req, res, next) {
         // Retrieve the occurrence ID
         let locationDbId = req.params.id

         if(!validator.isInt(locationDbId)) {
             let errMsg = `Invalid format, location ID must be an integer.`
             res.send(new errors.BadRequestError(errMsg))
             return
         }
 
         // Retrieve person ID of client from access token
         let personDbId = await tokenHelper.getUserId(req)

         if (personDbId == null) {
           let errMsg = await errorBuilder.getError(req.headers.host, 401002)
           res.send(new errors.BadRequestError(errMsg))
           return
         }

         let isAdmin = await userValidator.isAdmin(personDbId)
 
         let transaction
         try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let locationQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    location.id AS "locationDbId"
                FROM
                    experiment.location location
                    LEFT JOIN
                        tenant.person creator ON creator.id = location.creator_id
                WHERE
                    location.is_void = FALSE AND
                    location.id = ${locationDbId}
            `

            let location = await sequelize.query(locationQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await location == undefined || await location.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404005)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let deleteLocationQuery = format(`
                UPDATE
                    experiment.location location
                SET
                    location_code = CONCAT('-', $$${locationDbId}$$)::int,
                    location_name = CONCAT('VOIDED-', $$${locationDbId}$$),
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${locationDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    location.id = ${locationDbId}
            `)

            await sequelize.query(deleteLocationQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { locationDbId: locationDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}