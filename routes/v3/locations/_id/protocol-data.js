/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index')

module.exports = {

  // Implementation of GET call for /v3/locations/:id/protocol-data
  get: async function (req, res, next) {
    // Set defaults 
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let count = 0
    let conditionString = ''
    let orderString = ''

    // Retrieve the location ID
    let locationDbId = req.params.id

    // Retrieve protocol type
    let protocolType = params.type

    if (!validator.isInt(locationDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400063)
      res.send(new errors.BadRequestError(errMsg))
      return
    } else {
      // Build query
      let protocolsQuery = `
        SELECT
            l.id AS "locationDbId",
            o.id AS "occurrenceDbId",
            od.variable_id AS "variableDbId",
            v.abbrev AS "variableAbbrev",
            od.data_value AS "dataValue"
        FROM
            experiment.location l 
        LEFT OUTER JOIN
            experiment.location_occurrence_group log ON ( l.id = log.location_id )
        LEFT OUTER JOIN
            experiment.occurrence o ON ( o.id = log.occurrence_id )
        LEFT OUTER JOIN
            experiment.occurrence_data od ON ( od.occurrence_id = o.id )
        LEFT OUTER JOIN
            master.variable v ON ( od.variable_id = v.id )
        LEFT OUTER JOIN
            tenant.protocol p ON ( od.protocol_id = p.id )
        WHERE
            v.abbrev = 'TRAIT_PROTOCOL_LIST_ID'
            AND l.id = ${locationDbId}
            AND l.is_void = FALSE
            AND log.is_void = FALSE
            AND o.is_void = FALSE
            AND od.is_void = FALSE
            AND v.is_void = FALSE
            AND p.is_void = FALSE
      `.concat((protocolType === undefined) ? `` : `AND p.protocol_type = $$${protocolType}$$`)

      // Parse the sort parameters
      if (sort != null) {
        orderString = await processQueryHelper.getOrderString(sort)

        if (orderString.includes('invalid')) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400004)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
      }

      // Generate the final sql query 
      protocolFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
        protocolsQuery,
        conditionString,
        orderString
      )

      // Retrieve protocols from the database   
      let protocols = await sequelize.query(protocolFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      if (await protocols === undefined || await protocols.length < 1) {
        res.send(200, {
          rows: protocols,
          count: 0
        })
        return
      } else {
        // Get count
        protocolCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(protocolsQuery, conditionString, orderString)

        protocolCount = await sequelize.query(protocolCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

        count = protocolCount[0].count
      }

      res.send(200, {
        rows: protocols,
        count: count
      })
      return
    }
  },
}
