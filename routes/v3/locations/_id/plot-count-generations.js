/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // POST /v3/locations/:id/plot-count-generations
    post: async function (req, res, next) {
        // Retrieve location ID
        let locationDbId = req.params.id

        // Check if location ID is an integer
        if (!validator.isInt(locationDbId)) {
            let errMsg = `You have provided an invalid format for the location ID.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if location is existing
        let locationQuery = `
            SELECT
                id
            FROM
                experiment.location
            WHERE
                id = ${locationDbId}
        `

        // Retrieve location from the database
        let location = await sequelize.query(locationQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await location == undefined || await location.length < 1) {
            let errMsg = `The location you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction

        try {
            transaction = await sequelize.transaction({ autocommit: false })

            // Update plot count of the location
            let query = `
                UPDATE
                    experiment.location AS loc
                SET
                    plot_count = t.plot_count
                FROM (
                    SELECT
                        loc.id AS location_id,
                        (
                            SELECT
                                count(*)
                            FROM
                                experiment.plot AS plot
                                JOIN 
                                    experiment.planting_instruction AS plantinst
                                ON 
                                    plantinst.plot_id = plot.id
                                JOIN 
                                    experiment.entry AS ent
                                ON 
                                    ent.id = plot.entry_id
                            WHERE
                                plot.location_id = ${locationDbId}
                                AND plot.is_void = FALSE
                                AND plantinst.is_void = FALSE
                                AND ent.is_void = FALSE
                        ) AS plot_count
                    FROM
                        experiment.location AS loc
                    WHERE
                        loc.id = ${locationDbId}
                ) AS t
                WHERE
                    loc.id = t.location_id
            `

            let updatePlotCountQuery = await sequelize.query(query, {
                type: sequelize.QueryTypes.UPDATE
            })

            resultArray = {
                locationDbId: locationDbId,
                recordCount: 1
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })

            return

        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}