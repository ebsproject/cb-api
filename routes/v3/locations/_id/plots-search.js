/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let logger = require('../../../../helpers/logger')

module.exports = {
    post: async function(req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let addedOrderString = `
            ORDER BY
                plot.id
        `
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let responseColString = `
            plot.id AS "plotDbId",
            plot.occurrence_id AS "occurrenceDbId",
            occurrence.occurrence_code AS "occurrenceCode",
            occurrence.occurrence_name AS "occurrenceName",
            occurrence.occurrence_status AS "occurrenceStatus",
            plot.entry_id AS "entryDbId",
            pi.entry_code AS "entryCode",
            pi.entry_number AS "entryNumber",
            pi.entry_name AS "entryName",
            pi.entry_type AS "entryType",
            pi.entry_role AS "entryRole",
            pi.entry_class AS "entryClass",
            pi.entry_status AS "entryStatus",
            pi.germplasm_id AS "germplasmDbId",
            g.germplasm_code AS "germplasmCode",
            g.germplasm_state AS "germplasmState",
            g.germplasm_type AS "germplasmType",
            g.parentage AS "parentage",
            pi.seed_id AS "seedDbId",
            seed.seed_code AS "seedCode",
            seed.seed_name AS "seedName",
            pi.package_id AS "packageDbId",
            package.package_code AS "packageCode",
            package.package_label AS "packageLabel",
            plot.plot_code AS "plotCode",
            plot.plot_number AS "plotNumber",
            plot.plot_type AS "plotType",
            plot.rep,
            plot.design_x AS "designX",
            plot.design_y AS "designY",
            plot.plot_order_number AS "plotOrderNumber",
            plot.pa_x AS "paX",
            plot.pa_y AS "paY",
            plot.field_x AS "fieldX",
            plot.field_y AS "fieldY",
            plot.block_number AS "blockNumber",
            descol1.block_value AS "rowBlockNumber",
            descol2.block_value AS "colBlockNumber",
            plot.harvest_status AS "harvestStatus",
            plot.plot_status AS "plotStatus",
            plot.plot_qc_code AS "plotQcCode",
            pi.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS "creator",
            plot.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS "modifier"
        `

        const endpoint = 'locations/:id/plots-search'
        const operation = 'SELECT'

        let locationDbId = req.params.id

        if (!validator.isInt(locationDbId)) {
            let errMsg = 'Invalid request, locationDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let locationQuery = `
            SELECT
                location.id AS "locationDbId",
                location.location_code AS "locationCode",
                location.location_name AS "locationName",
                location.location_status AS "locationStatus",
                location.location_type AS "locationType"
            FROM
                experiment.location location
            WHERE
                location.is_void = FALSE AND
                location.id = ${locationDbId}
        `

        let location = await sequelize
            .query(locationQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await location == undefined || await location.length < 1) {
            let errMsg = 'Resource not found, the location you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                        ${parameters['distinctOn']}
                `
            }
            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the plots value retrieval query
        let plotsQuery = null
        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                plotsQuery = knex.select(selectString)
            } else {
                plotsQuery = knex.column(parameters['fields'].split('|'))
            }

            plotsQuery += `
                FROM
                    experiment.plot plot
                LEFT JOIN
                    experiment.occurrence occurrence 
                ON 
                    plot.occurrence_id = occurrence.id
                JOIN
                    experiment.planting_instruction pi
                ON
                    plot.id = pi.plot_id AND
                    pi.is_void = FALSE
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'RowBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol1
                    ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'ColBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol2
                    ON TRUE
                LEFT JOIN
                    germplasm.package package
                ON
                    package.id = pi.package_id
                JOIN
                    germplasm.germplasm g
                ON
                    pi.germplasm_id = g.id
                LEFT JOIN
                    germplasm.seed seed
                ON
                    seed.id = pi.seed_id
                JOIN
                    tenant.person creator
                ON
                    plot.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier
                ON
                    plot.modifier_id = modifier.id
                WHERE
                    plot.entry_id = pi.entry_id AND
                    plot.location_id = ${locationDbId} AND
                    plot.is_void = FALSE
                ORDER BY
                    ${(parameters['distinctOn']) ? parameters['distinctOn'] : ''}
                    plot.plot_number
            `
        } else {
            plotsQuery = `
                SELECT
                    ${addedDistinctString}
                    ${responseColString}
                FROM
                    experiment.plot plot
                LEFT JOIN
                    experiment.occurrence occurrence 
                ON 
                    plot.occurrence_id = occurrence.id
                JOIN
                    experiment.planting_instruction pi
                ON
                    plot.id = pi.plot_id AND pi.is_void = FALSE
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'RowBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol1
                    ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'ColBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol2
                    ON TRUE
                LEFT JOIN
                    germplasm.package package
                ON
                    package.id = pi.package_id
                JOIN
                    germplasm.germplasm g
                ON
                    pi.germplasm_id = g.id
                LEFT JOIN
                    germplasm.seed seed
                ON
                    seed.id = pi.seed_id
                JOIN
                    tenant.person creator
                ON
                    plot.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier
                ON
                    plot.modifier_id = modifier.id
                WHERE
                    plot.entry_id = pi.entry_id AND
                    plot.location_id = ${locationDbId} AND
                    plot.is_void = FALSE
                ORDER BY
                    ${(parameters['distinctOn']) ? parameters['distinctOn'] : ''}
                    plot.plot_number
            `
        }
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let plotsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            plotsQuery,
            conditionString,
            orderString
        )

        // Get count
        plotsFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            plotsFinalSqlQuery,
            orderString
        )

        let plots = await sequelize.query(plotsFinalSqlQuery, {
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        location[0]['plots'] = plots

        if (await plots == undefined || await plots.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = (plots[0][0]) ? plots[0][0].totalCount : 0

        res.send(200, {
            rows: location,
            count: count
        })
        return
    }
}