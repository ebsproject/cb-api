/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = params.sort
        let count = 0
        let harvestDateVarId = 0
        let harvestMethodVarId = 0
        let noOfPlantVarId = 0
        let noOfPanicleVarId = 0
        let specificPlantNoVarId = 0
        let noOfEarVarId = 0
        let orderString = ''
        let addedOrderString = `
            ORDER BY
                plot.id
        `
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let experimentDbIdString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let attributes = ["plotDbId", "locationDbId", "occurrenceDbId",
            "experimentDbId", "entryDbId", "germplasmDbId", "plotCode", "plotNumber",
            "rep", "harvestStatus", "designation", "parentage", "state", "type",
            "cropDbId", "cropCode", "harvestDate", "harvestMethod", "noOfPlant",
            "noOfPanicle", "specificPlantNo", "noOfEar", "terminalHarvestDate",
            "terminalHarvestMethod", "terminalNoOfPlant", "terminalNoOfPanicle",
            "terminalSpecificPlantNo", "terminalNoOfEar", "creatorDbId", "creator",
            "modifierDbId", "modifier"]

        let creatorId = await tokenHelper.getUserId(req)

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let locationDbId = req.params.id

        if (!validator.isInt(locationDbId)) {
            let errMsg = 'Invalid request, locationDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let locationQuery = `
            SELECT
                location.id AS "locationDbId",
                location.location_code AS "locationCode",
                location.location_name AS "locationName",
                location.location_status AS "locationStatus",
                location.location_type AS "locationType"
            FROM
                experiment.location location
            WHERE
                location.is_void = FALSE AND
                location.id = ${locationDbId}
        `

        let location = await sequelize
            .query(locationQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await location == undefined || await location.length < 1) {
            let errMsg = 'Resource not found, the location you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // if experimentDbId is a url parameter
        if (params.experimentDbId != null) {
            let experimentDbId = params.experimentDbId
            
            // check if experimentDbId is an integer
            if (!validator.isInt(experimentDbId)) {
                let errMsg = 'Invalid request, experimentDbId must be an integer.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            let experimentQuery = `
                SELECT
                    experiment.id AS "experimentDbId",
                    experiment.experiment_code AS "experimentCode",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_status AS "experimentStatus"
                FROM
                    experiment.experiment experiment
                WHERE
                    experiment.is_void = FALSE AND
                    experiment.id = ${experimentDbId}
            `

            let experiment = await sequelize
                .query(experimentQuery, {
                    type: sequelize.QueryTypes.SELECT,
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await experiment == undefined || await experiment.length < 1) {
                let errMsg = 'Resource not found, the experiment you have requested for does not exist.'
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            experimentDbIdString = `occurrence.experiment_id = ${experimentDbId} AND`
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                        ${parameters['distinctOn']}
                `
            }

            if (req.body.sort != undefined) {
                if (typeof req.body.sort == 'object') {
                    if (
                        req.body.sort.attribute != undefined
                        && req.body.sort.sortValue != undefined
                        && req.body.sort.attribute.trim() != ""
                        && req.body.sort.sortValue.trim() != ""
                        && attributes.includes(req.body.sort.attribute.trim())
                    ) {
                        orderString = await processQueryHelper.getOrderStringBySortValue(req.body.sort)
                    }
                }
            }
            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
                'sort'
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Retrieve variable ids of harvest data
        let getVariableIdsQuery = `
          SELECT
        	(SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT') AS "harvestDateVarId",
        	(SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC') AS "harvestMethodVarId",
        	(SELECT id FROM master.variable WHERE abbrev='NO_OF_PLANTS') AS "noOfPlantVarId",
        	(SELECT id FROM master.variable WHERE abbrev='PANNO_SEL') AS "noOfPanicleVarId",
        	(SELECT id FROM master.variable WHERE abbrev='SPECIFIC_PLANT') AS "specificPlantNoVarId",
        	(SELECT id FROM master.variable WHERE abbrev='NO_OF_EARS') AS "noOfEarVarId"
              
        `

        let variableIds = await sequelize.query(getVariableIdsQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (variableIds == undefined || variableIds.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        harvestDateVarId = variableIds[0].harvestDateVarId
        harvestMethodVarId = variableIds[0].harvestMethodVarId
        noOfPlantVarId = variableIds[0].noOfPlantVarId
        noOfPanicleVarId = variableIds[0].noOfPanicleVarId
        specificPlantNoVarId = variableIds[0].specificPlantNoVarId
        noOfEarVarId = variableIds[0].noOfEarVarId

        // Build the plots value retrieval query
        let plotsQuery = null
        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                plotsQuery = knex.select(selectString)
            } else {
                plotsQuery = knex.column(parameters['fields'].split('|'))
            }

            plotsQuery += `
            FROM
                experiment.plot plot
            LEFT JOIN
                experiment.planting_instruction pi ON plot.id = pi.plot_id
            LEFT JOIN
                experiment.entry entry ON plot.entry_id = entry.id
            LEFT JOIN
                experiment.occurrence occurrence ON plot.occurrence_id = occurrence.id
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = pi.germplasm_id
            LEFT JOIN
                tenant.crop crop ON crop.id = germplasm.crop_id
            LEFT JOIN
                tenant.person creator ON plot.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON plot.modifier_id = modifier.id
                WHERE
                    plot.entry_id = pi.entry_id AND
                    plot.id = pi.plot_id AND
                    plot.location_id = ${locationDbId} AND
                    ${experimentDbIdString}
                    pi.is_void = FALSE AND
                    plot.is_void = FALSE
                ${addedOrderString}
            `
        } else {
            plotsQuery = `
                SELECT
                    ${addedDistinctString}
                    plot.id AS "plotDbId",
                    plot.location_id AS "locationDbId",
                    plot.occurrence_id AS "occurrenceDbId",
                    occurrence.experiment_id AS "experimentDbId",
                    plot.entry_id AS "entryDbId",
                    entry.entry_number AS "entryNumber",
                    pi.germplasm_id AS "germplasmDbId",
                    plot.plot_code AS "plotCode",
                    plot.plot_number AS "plotNumber",
                    plot.rep AS "rep",
                    plot.harvest_status as "harvestStatus",
                    germplasm.designation AS "designation",
                    germplasm.parentage AS "parentage",
                    germplasm.germplasm_state AS "state",
                    germplasm.germplasm_type AS "type",
                    crop.id AS "cropDbId",
                    crop.crop_code AS "cropCode",
                    (SELECT data_value FROM experiment.plot_data WHERE plot_id=plot.id AND variable_id=${harvestDateVarId} AND is_void=false limit 1) AS "harvestDate",
                    (SELECT data_value FROM experiment.plot_data WHERE plot_id=plot.id AND variable_id=${harvestMethodVarId} AND is_void=false limit 1) AS "harvestMethod",
                    (SELECT data_value FROM experiment.plot_data WHERE plot_id=plot.id AND variable_id=${noOfPlantVarId} AND is_void=false limit 1) AS "noOfPlant",
                    (SELECT data_value FROM experiment.plot_data WHERE plot_id=plot.id AND variable_id=${noOfPanicleVarId} AND is_void=false limit 1) AS "noOfPanicle",
                    (SELECT data_value FROM experiment.plot_data WHERE plot_id=plot.id AND variable_id=${specificPlantNoVarId} AND is_void=false limit 1) AS "specificPlantNo",
                    (SELECT data_value FROM experiment.plot_data WHERE plot_id=plot.id AND variable_id=${noOfEarVarId} AND is_void=false limit 1) AS "noOfEar",
                    (SELECT value FROM data_terminal.transaction_dataset WHERE entity_id=plot.id AND variable_id=${harvestDateVarId} AND creator_id=${creatorId} AND is_void=false limit 1) AS "terminalHarvestDate",
                    (SELECT value FROM data_terminal.transaction_dataset WHERE entity_id=plot.id AND variable_id=${harvestMethodVarId} AND creator_id=${creatorId} AND is_void=false limit 1) AS "terminalHarvestMethod",
                    (SELECT value FROM data_terminal.transaction_dataset WHERE entity_id=plot.id AND variable_id=${noOfPlantVarId} AND creator_id=${creatorId} AND is_void=false limit 1) AS "terminalNoOfPlant",
                    (SELECT value FROM data_terminal.transaction_dataset WHERE entity_id=plot.id AND variable_id=${noOfPanicleVarId} AND creator_id=${creatorId} AND is_void=false limit 1) AS "terminalNoOfPanicle",
                    (SELECT value FROM data_terminal.transaction_dataset WHERE entity_id=plot.id AND variable_id=${specificPlantNoVarId} AND creator_id=${creatorId} AND is_void=false limit 1) AS "terminalSpecificPlantNo",
                    (SELECT value FROM data_terminal.transaction_dataset WHERE entity_id=plot.id AND variable_id=${noOfEarVarId} AND creator_id=${creatorId} AND is_void=false limit 1) AS "terminalNoOfEar",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    experiment.plot plot
                LEFT JOIN
                    experiment.planting_instruction pi ON plot.id = pi.plot_id
                LEFT JOIN
                    experiment.entry entry ON plot.entry_id = entry.id
                LEFT JOIN
                    experiment.occurrence occurrence ON plot.occurrence_id = occurrence.id
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = pi.germplasm_id
                LEFT JOIN
                    tenant.crop crop ON crop.id = germplasm.crop_id
                LEFT JOIN
                    tenant.person creator ON plot.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON plot.modifier_id = modifier.id
                WHERE
                    plot.entry_id = pi.entry_id AND
                    plot.id = pi.plot_id AND
                    plot.location_id = ${locationDbId} AND
                    ${experimentDbIdString}
                    pi.is_void = FALSE AND
                    plot.is_void = FALSE
                ${addedOrderString}
            `
        }
        // Parse the sort parameters
        if (sort != null) {

            if (orderString != '') {
                orderString = orderString + (await processQueryHelper.getOrderString(sort)).replace("ORDER BY", ', ')
            } else {
                orderString = await processQueryHelper.getOrderString(sort)
            }

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let plotsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plotsQuery,
            conditionString,
            orderString,
        )

        let plots = await sequelize
            .query(plotsFinalSqlQuery, {
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (plots === undefined || plots.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let plotsCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                plotsQuery,
                conditionString,
                orderString,
            )

        let plotsCount = await sequelize
            .query(
                plotsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        rows = plots[0]
        count = plotsCount[0].count

        res.send(200, {
            rows: rows,
            count: count
        })
        return
    }
}
