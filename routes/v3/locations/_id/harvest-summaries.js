/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

    // Endpoint for retrieving an existing harvest summary record of a location
    // GET /v3/locations/:id/harvest-summaries
    get: async function (req, res, next) {

        // Retrieve location ID
        let locationDbId = req.params.id
        let params = req.query
        let conditionString = ''
        let conditionStringSeed = ''

        // Get filter condition for experimentDbId only
        if (params.experimentDbId !== undefined) {
            // Check if ID is an integer
            if (!validator.isInt(params.experimentDbId)) {
                let errMsg = `Invalid format, experiment ID must be an integer.`
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            conditionString = 'AND occurrence.experiment_id=' + params.experimentDbId
            conditionStringSeed = 'AND seed.source_experiment_id=' + params.experimentDbId
        }

        // Check if ID is an integer
        if (!validator.isInt(locationDbId)) {
            let errMsg = `Invalid format, location ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let harvestSummaryQuery = `
            WITH 
                plot AS (
                    SELECT
                        location_id,count(plot) AS total_plot,
                        max(rep)::varchar(4) AS total_rep
                    FROM
                        experiment.plot plot
                    LEFT JOIN
                        experiment.occurrence occurrence ON plot.occurrence_id = occurrence.id
                    WHERE
                        plot.location_id=${locationDbId}
                        AND plot.is_void=FALSE
                        AND occurrence.is_void=FALSE
                        ${conditionString}
                    GROUP BY
                        location_id
                ),
                plotharvest AS (
                    SELECT
                        count(plot) AS total_plot_harvest
                    FROM
                        experiment.plot plot
                    LEFT JOIN
                        experiment.occurrence occurrence ON plot.occurrence_id = occurrence.id
                    WHERE
                        plot.location_id=${locationDbId}
                        AND plot.harvest_status='COMPLETED'
                        AND plot.is_void=FALSE
                        AND occurrence.is_void=FALSE
                        ${conditionString}
                ),
                entry AS (
                    SELECT
                        location_id,
                        count(entry) total_entry
                    FROM
                        experiment.location_occurrence_group AS loc_grp
                    LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.id=loc_grp.occurrence_id
                    LEFT JOIN
                        experiment.entry_list entry_list ON entry_list.experiment_id=occurrence.experiment_id
                    LEFT JOIN
                        experiment.entry entry ON entry.entry_list_id=entry_list.id
                    WHERE
                        loc_grp.location_id=${locationDbId}
                        AND loc_grp.is_void= FALSE
                        AND occurrence.is_void = FALSE
                        AND entry_list.is_void = FALSE
                        AND entry.is_void = FALSE
                        ${conditionString}
                    GROUP BY
                        location_id
                ),
                seed AS (
                    SELECT
                        source_location_id,
                        count(seed) AS total_seed
                    FROM
                        germplasm.seed seed
                    WHERE
                        seed.source_location_id=${locationDbId}
                        AND seed.is_void=FALSE
                        ${conditionStringSeed}
                    GROUP BY
                        source_location_id
                ),
                package AS (
                    SELECT
                        count(package) AS total_package
                    FROM
                        germplasm.package AS package
                    LEFT JOIN
                        germplasm.seed AS seed ON package.seed_id=seed.id
                    WHERE
                        seed.source_location_id=${locationDbId}
                        AND package.is_void=FALSE
                        AND seed.is_void=FALSE
                        AND package.package_code not ilike 'VOIDED-%'
                        ${conditionStringSeed}
                ),
                germplasmCross AS (
                    SELECT
                        location_id,
                        count(germplasmCross) total_cross
                    FROM
                        germplasm.cross germplasmCross
                    LEFT JOIN 
                        experiment.experiment e ON e.id = germplasmCross.experiment_id
                    LEFT JOIN 
                        experiment.occurrence occurrence ON occurrence.experiment_id = e.id
                    LEFT JOIN
                        experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                    WHERE
                        log.location_id = ${locationDbId} AND
                        germplasmCross.is_void = FALSE
                        ${conditionString}
                    GROUP BY
                        log.location_id
                ),
                seedPlot AS (
                    SELECT
                        source_location_id,
                        COUNT(seed) AS total_seed_from_plots
                    FROM
                        germplasm.seed seed
                    WHERE
                        seed.source_location_id = ${locationDbId} 
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'plot'
                        ${conditionStringSeed}
                    GROUP BY
                        source_location_id
                ),
                seedCross AS (
                    SELECT
                        source_location_id,
                        COUNT(seed) AS total_seed_from_crosses
                    FROM
                        germplasm.seed seed
                    WHERE
                        seed.source_location_id = ${locationDbId} 
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'cross'
                        ${conditionStringSeed}
                    GROUP BY
                        source_location_id
                ),
                crossWithSeed AS (
                    SELECT
                        location.id AS location_id,
                        COUNT(DISTINCT(seed.cross_id)) AS cross_with_seed
                    FROM
                        germplasm.seed seed
                    LEFT JOIN
                        experiment.location location ON location.id = seed.source_location_id
                    LEFT JOIN 
                        experiment.location_occurrence_group log ON log.location_id = location.id
                    LEFT JOIN 
                        experiment.occurrence occurrence ON occurrence.id = log.occurrence_id
                    LEFT JOIN 
                        experiment.experiment experiment ON experiment.id = occurrence.experiment_id
                    LEFT JOIN 
                        germplasm.cross c ON c.experiment_id = experiment.id
                    WHERE
                        location.id = ${locationDbId}
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'cross'
                        ${conditionStringSeed}
                    GROUP BY
                        location.id
                ),
                plotWithSeed AS (
                    SELECT
                        location.id AS location_id,
                        COUNT(DISTINCT(seed.source_plot_id)) AS plot_with_seed
                    FROM
                        germplasm.seed seed
                    LEFT JOIN
                        experiment.plot p ON p.id = seed.source_plot_id
                    LEFT JOIN
                        experiment.location location ON location.id = p.location_id
                    WHERE
                        location.id = ${locationDbId}
                        AND seed.source_location_id = location.id
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'plot'
                        ${conditionStringSeed}
                    GROUP BY
                        location.id
                )
            SELECT 
                total_entry AS "totalEntryCount",
                total_rep AS "totalRepCount",
                total_plot AS "totalPlotCount",
                total_cross AS "totalCrossCount",
                total_plot_harvest AS "totalPlotHarvestCount",
                COALESCE(NULL,total_seed,0) AS "totalSeedCount",
                COALESCE(NULL,total_package,0) AS "totalPackageCount",
                COALESCE(NULL, plot_with_seed,0) AS "plotsWithSeeds",
                COALESCE(NULL, total_seed_from_plots, 0) AS "totalSeedsFromPlots",
                COALESCE(NULL, cross_with_seed,0) AS "crossesWithSeeds",
                COALESCE(NULL, total_seed_from_crosses, 0) AS "totalSeedsFromCrosses"
            FROM
                entry e
            LEFT JOIN
                plot p ON e.location_id=p.location_id
            LEFT JOIN
                plotharvest pd ON e.location_id=p.location_id
            LEFT JOIN
                seed AS seed ON seed.source_location_id=e.location_id
            LEFT JOIN
                package AS package ON seed.source_location_id=e.location_id
            LEFT JOIN
                germplasmCross AS gc ON gc.location_id = e.location_id
            LEFT JOIN
                seedPlot AS sp ON sp.source_location_id = e.location_id
            LEFT JOIN
                seedCross AS sc ON sc.source_location_id = e.location_id
            LEFT JOIN
                crossWithSeed AS cws ON cws.location_id = e.location_id
            LEFT JOIN
                plotWithSeed AS pws ON pws.location_id = e.location_id

        `

        // Retrieve plot harvest FROM the database
        let harvestSummary = await sequelize.query(harvestSummaryQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (await harvestSummary == undefined || await harvestSummary.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get count
            let harvestSummaryCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(harvestSummaryQuery)

            let harvestSummaryCount = await sequelize.query(harvestSummaryCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            let count = harvestSummaryCount[0].count

            res.send(200, {
                rows: harvestSummary,
                count: count
            })
            return
        }
    }
}
