/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0

        let orderString = ''
        let addedOrderString = `
            ORDER BY
                "germplasmCross".id
        `
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        let harvestDateVarId = 0
        let harvestMethodVarId = 0
        let crossingDateVarId = 0
        let noOfSeedVarId = 0

        let creatorId = await tokenHelper.getUserId(req)

        let attributes = ["crossDbId", "crossName", "crossMethod",
            "crossMethodAbbrev", "remarks", "isMethodAutofilled", "harvestStatus",
            "germplasmDbId", "germplasmDesignation", "germplasmParentage",
            "germplasmGeneration", "germplasmType", "cropDbId", "cropCode",
            "entryDbId", "entryNo", "entryName", "entryRole", "experimentDbId",
            "experimentName", "experimentCode", "experimentDataProcessId",
            "crossFemaleParent", "femaleParentSeedSource", "crossMaleParent",
            "maleParentSeedSource", "femaleSourceEntryDbId", "femaleSourceEntry",
            "femaleSourceSeedName", "femaleSourcePlotDbId", "femaleSourcePlot",
            "femaleParentage", "maleSourceEntryDbId", "maleSourceEntry",
            "maleSourceSeedName", "maleSourcePlotDbId", "maleSourcePlot",
            "maleParentage", "harvestDate", "harvestMethod", "crossingDate",
            "noOfSeed", "noOfPlants", "specificPlantNo", "noOfPanicle", "noOfEar",
            "terminalHarvestDate", "terminalHarvestMethod", "terminalCrossingDate",
            "terminalNoOfSeed", "terminalNoOfPlants", "terminalSpecificPlantNo",
            "terminalNoOfPanicle", "terminalNoOfEar", "creationTimestamp",
            "creatorDbId", "creator", "modificationTimestamp", "modifierDbId",
            "modifier",]

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            return res.send(new errors.BadRequestError(errMsg))
        }

        let locationDbId = req.params.id

        if (!validator.isInt(locationDbId)) {
            let errMsg = 'Invalid request, locationDbId must be an integer.'
            return res.send(new errors.BadRequestError(errMsg))
        }

        let locationQuery = `
            SELECT
                location.id AS "locationDbId",
                location.location_code AS "locationCode",
                location.location_name AS "locationName",
                location.location_status AS "locationStatus",
                location.location_type AS "locationType"
            FROM
                experiment.location location
            WHERE
                location.is_void = FALSE AND
                location.id = ${locationDbId}
        `

        let location = await sequelize.query(locationQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                console.log(err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                return res.send(new errors.InternalError(errMsg))
            })

        if (await location == undefined || await location.length < 1) {
            let errMsg = 'Resource not found, the location you have requested for does not exist.'
            return res.send(new errors.NotFoundError(errMsg))
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `${parameters['distinctOn']}`

                let distinctOnValue = parameters['distinctOn'];

                // Split the value of distinctOn via |
                let columns = distinctOnValue.split('|');
                let distinctOnSort = ''
                for (col of columns) {
                    distinctOnSort += `"${col}",`
                }

                distinctOnSort = distinctOnSort.replace(/,\s*$/, "")
                addedOrderString = `ORDER BY ${distinctOnSort}`
            }

            if (req.body.sort != undefined) {
                if (typeof req.body.sort == 'object') {
                    if (
                        req.body.sort.attribute != undefined
                        && req.body.sort.sortValue != undefined
                        && req.body.sort.attribute.trim() != ""
                        && req.body.sort.sortValue.trim() != ""
                        && attributes.includes(req.body.sort.attribute.trim())
                    ) {
                        orderString = await processQueryHelper.getOrderStringBySortValue(req.body.sort)
                    }
                }
            }
            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
                'sort'
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
                "||"
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                return res.send(new errors.BadRequestError(errMsg))
            }
        }

        // Retrieve variable IDs of harvest data
        let variableIdsQuery = `
            SELECT
                (SELECT id FROM master.variable WHERE abbrev='HVDATE_CONT' AND is_void = FALSE) AS "harvestDateVarId",
                (SELECT id FROM master.variable WHERE abbrev='HV_METH_DISC' AND is_void = FALSE) AS "harvestMethodVarId",
                (SELECT id FROM master.variable WHERE abbrev='DATE_CROSSED' AND is_void = FALSE) AS "crossingDateVarId",
                (SELECT id FROM master.variable WHERE abbrev='NO_OF_SEED' AND is_void = FALSE) AS "noOfSeedVarId",
                (SELECT id FROM master.variable WHERE abbrev='NO_OF_PLANTS' AND is_void = FALSE) AS "noOfPlantVarId",
                (SELECT id FROM master.variable WHERE abbrev='SPECIFIC_PLANT' AND is_void = FALSE) AS "specificPlantNoVarId",
                (SELECT id FROM master.variable WHERE abbrev='PANNO_SEL' AND is_void = FALSE) AS "noOfPanicleVarId",
                (SELECT id FROM master.variable WHERE abbrev='NO_OF_EARS' AND is_void = FALSE) AS "noOfEarVarId"
        `

        let variableIds = await sequelize.query(variableIdsQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (variableIds == undefined || variableIds.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            return res.send(new errors.BadRequestError(errMsg))
        }

        harvestDateVarId = variableIds[0].harvestDateVarId
        harvestMethodVarId = variableIds[0].harvestMethodVarId
        crossingDateVarId = variableIds[0].crossingDateVarId
        noOfSeedVarId = variableIds[0].noOfSeedVarId
        noOfPlantVarId = variableIds[0].noOfPlantVarId
        specificPlantNoVarId = variableIds[0].specificPlantNoVarId
        noOfPanicleVarId = variableIds[0].noOfPanicleVarId
        noOfEarVarId = variableIds[0].noOfEarVarId

        // Build base query
        let crossesQuery = null

        let crossParentsSourceQuery = `
            (
                SELECT g.designation
                FROM germplasm.germplasm g, germplasm.cross_parent cp
                WHERE cp.germplasm_id = g.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 1 AND
                    cp.is_void = FALSE AND 
                    g.is_void = FALSE 
            ) AS "crossFemaleParent",
            (
                SELECT s.seed_code 
                FROM germplasm.seed s, germplasm.cross_parent cp 
                WHERE cp.seed_id = s.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 1 AND
                    cp.is_void = FALSE AND 
                    s.is_void = FALSE 
            ) AS "femaleParentSeedSource",
            (
                SELECT g.designation
                FROM germplasm.germplasm g, germplasm.cross_parent cp
                WHERE cp.germplasm_id = g.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 2 AND
                    cp.is_void = FALSE AND 
                    g.is_void = FALSE
            ) AS "crossMaleParent",
            (
                SELECT s.seed_code 
                FROM germplasm.seed s, germplasm.cross_parent cp 
                WHERE cp.seed_id = s.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 2 AND
                    cp.is_void = FALSE AND 
                    s.is_void = FALSE
            ) AS "maleParentSeedSource",
        `

        let seedSourceQuery = `
            femaleEntryTbl."femaleSourceEntryDbId",
            femaleEntryTbl."femaleSourceEntry",
            femaleEntryTbl."femaleSourceSeedName",
            femalePlotTbl."femaleSourcePlotDbId",
            femalePlotTbl."femaleSourcePlot",
            femaleParentage."femaleParentage",
            femaleParentage."femaleGermplasmState",
            maleEntryTbl."maleSourceEntryDbId",
            maleEntryTbl."maleSourceEntry",
            maleEntryTbl."maleSourceSeedName",
            malePlotTbl."maleSourcePlotDbId",
            malePlotTbl."maleSourcePlot",
            maleParentage."maleParentage",
            maleParentage."maleGermplasmState",
        `

        let seedSourceInfoFromQuery = `
            LEFT JOIN
                (
                    SELECT
                        e.id AS "femaleSourceEntryDbId",
                        e.entry_code AS "femaleSourceEntry",
                        s.seed_name AS "femaleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role = 'female' AND
                        cross_parent.is_void = false AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS femaleEntryTbl ON femaleEntryTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        e.id AS "maleSourceEntryDbId",
                        e.entry_code AS "maleSourceEntry",
                        s.seed_name AS "maleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role = 'male' AND
                        cross_parent.is_void = FALSE AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS maleEntryTbl ON maleEntryTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        p.id AS "femaleSourcePlotDbId",
                        p.plot_code AS "femaleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role = 'female' AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE AND 
                        s.is_void = FALSE
                ) AS femalePlotTbl ON femalePlotTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        p.id AS "maleSourcePlotDbId",
                        p.plot_code AS "maleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role = 'male' AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE AND 
                        s.is_void = FALSE
                ) AS malePlotTbl ON malePlotTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        g.parentage AS "femaleParentage",
                        cross_parent.cross_id AS "crossDbId",
                        g.germplasm_state AS "femaleGermplasmState"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        (cross_parent.parent_role = 'female' 
                            OR cross_parent.parent_role = 'female-and-male'
                        ) AND
                        g.is_void = FALSE
                ) AS femaleParentage ON femaleParentage."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        g.parentage AS "maleParentage",
                        cross_parent.cross_id AS "crossDbId",
                        g.germplasm_state AS "maleGermplasmState"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role = 'male' AND
                        g.is_void = FALSE
                ) AS maleParentage ON maleParentage."crossDbId" = "germplasmCross".id
        `

        let crossDataQuery = `
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${harvestDateVarId} AND 
                    is_void = FALSE
            ) AS "harvestDate",
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${harvestMethodVarId} AND 
                    is_void = FALSE
            ) AS "harvestMethod",
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${crossingDateVarId} AND 
                    is_void = FALSE
            ) AS "crossingDate",
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${noOfSeedVarId} AND 
                    is_void = FALSE
            ) AS "noOfSeed",
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${noOfPlantVarId} AND 
                    is_void = FALSE
            ) AS "noOfPlant",
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${specificPlantNoVarId} AND 
                    is_void = FALSE
            ) AS "specificPlantNo",
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${noOfPanicleVarId} AND 
                    is_void = FALSE
            ) AS "noOfPanicle",
            (
                SELECT 
                    data_value
                FROM 
                    germplasm.cross_data 
                WHERE
                    cross_id = "germplasmCross".id AND
                    variable_id = ${noOfEarVarId} AND 
                    is_void = FALSE
            ) AS "noOfEar",
        `

        let crossDataTransactionDatasetQuery = `
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${harvestDateVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalHarvestDate",
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${harvestMethodVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalHarvestMethod",
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${crossingDateVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalCrossingDate",
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${noOfSeedVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalNoOfSeed",
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${noOfPlantVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalNoOfPlant",
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${specificPlantNoVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalSpecificPlantNo",
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${noOfPanicleVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalNoOfPanicle",
            (
                SELECT 
                    value
                FROM 
                    data_terminal.transaction_dataset
                WHERE
                    entity_id = "germplasmCross".id AND
                    variable_id = ${noOfEarVarId} AND 
                    creator_id = ${creatorId} AND
                    is_void = FALSE
                LIMIT 1
            ) AS "terminalNoOfEar",
        `

        let fromQuery = `
            FROM
                germplasm.cross "germplasmCross"
            LEFT JOIN
                experiment.experiment experiment ON "germplasmCross".experiment_id = experiment.id
            LEFT JOIN
                tenant.program program ON program.id = experiment.program_id
            LEFT JOIN
                tenant.crop_program cp ON cp.id = program.crop_program_id
            LEFT JOIN
                tenant.crop crop ON crop.id = cp.crop_id
            LEFT JOIN
                germplasm.cross_parent ON cross_parent.cross_id = "germplasmCross".id AND cross_parent.parent_role = 'female-and-male'
            LEFT JOIN
                germplasm.germplasm g2 ON g2.id = cross_parent.germplasm_id
            LEFT JOIN
                germplasm.germplasm germplasm ON "germplasmCross".germplasm_id = germplasm.id
            LEFT JOIN
                germplasm.seed seed ON "germplasmCross".seed_id = seed.id
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
            LEFT JOIN
                experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
            LEFT JOIN
                experiment.location location ON location.id = log.location_id
            LEFT JOIN
                experiment.entry entry ON "germplasmCross".entry_id = entry.id
            LEFT JOIN
                tenant.person creator ON "germplasmCross".creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON "germplasmCross".modifier_id = modifier.id
            ${seedSourceInfoFromQuery}
            WHERE
                "germplasmCross".is_void = FALSE AND
                location.id = ${locationDbId}
            ${addedOrderString}
        `

        // Check if user specified values for fields parameter
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(parameters['fields'])
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                crossesQuery = knex.select(selectString)
            } else {
                crossesQuery = knex.column(parameters['fields'].split('|'))
            }

            crossesQuery += fromQuery
        } else {
            crossesQuery = `
                SELECT
                    ${addedDistinctString}
                    "germplasmCross".id AS "crossDbId",
                    "germplasmCross".cross_name AS "crossName",
                    "germplasmCross".cross_method AS "crossMethod",
                    (
                        SELECT 
                            abbrev
                        FROM 
                            master.scale_value
                        WHERE 
                            scale_id = (
                                SELECT 
                                    scale_id 
                                FROM 
                                    master.variable 
                                WHERE 
                                    abbrev = 'CROSS_METHOD'
                            ) 
                            AND LOWER(value) = LOWER(cross_method)
                    ) AS "crossMethodAbbrev",
                    "germplasmCross".remarks,
                    "germplasmCross".is_method_autofilled AS "isMethodAutofilled",
                    "germplasmCross".harvest_status AS "harvestStatus",
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation AS "germplasmDesignation",
                    germplasm.parentage AS "germplasmParentage",
                    germplasm.generation AS "germplasmGeneration",
                    g2.germplasm_state AS "state",
                    crop.id AS "cropDbId",
                    crop.crop_code AS "cropCode",
                    entry.id AS "entryDbId",
                    entry.entry_number AS "entryNo",
                    entry.entry_name AS "entryName",
                    entry.entry_role AS "entryRole",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_code AS "experimentCode",
                    experiment.data_process_id AS "experimentDataProcessId",
                    ${crossParentsSourceQuery}
                    ${seedSourceQuery}
                    ${crossDataQuery}
                    ${crossDataTransactionDatasetQuery}
                    "germplasmCross".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    "germplasmCross".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            if (orderString != '') {
                orderString = orderString + (await processQueryHelper.getOrderString(sort)).replace("ORDER BY", ', ')
            } else {
                orderString = await processQueryHelper.getOrderString(sort)
            }
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                return res.send(new errors.BadRequestError(errMsg))
            }
        }

        // Generate final SQL query
        let crossesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            crossesQuery,
            conditionString,
            orderString,
        )

        let crosses = await sequelize.query(crossesFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                console.log(err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                return res.send(new errors.InternalError(errMsg))
            })

        if (await crosses == undefined || await crosses.length < 1) {
            return res.send(200, {
                rows: [],
                count: 0
            })
        }

        let crossesCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            crossesQuery,
            conditionString,
        )

        let crossesCount = await sequelize.query(crossesCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        count = crossesCount[0].count

        return res.send(200, {
            rows: crosses,
            count: count
        })
    }
}