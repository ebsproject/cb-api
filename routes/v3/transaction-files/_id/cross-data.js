/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let validator = require('validator')

module.exports = {

    /**
     * Retrieve specific record in data_terminal.transaction_file
     * GET /v3/transaction-files/:id
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    get: async function (req, res) {

        let transactionFileDbId = req.params.id

        // check if transactionFileDbId is valid
        if (!validator.isInt(transactionFileDbId)) {
            let errMsg = 'Invalid format, transactionFileDbId must be an integer'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transactionFileDbId exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction_file 
                WHERE 
                    id=${transactionFileDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The transaction file ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') AS "variableId",
                STRING_AGG( abbrev || ' text', ',') AS columns
            FROM(
                SELECT 
                    distinct ((dataset ->> 'variableDbId')::int) AS variable_id,
                    v.abbrev
                FROM 
                    (
                        SELECT 
                            JSONB_ARRAY_ELEMENTS(dataset) AS dataset
                        FROM 
                            data_terminal.transaction_file
                        WHERE
                            id= ${transactionFileDbId}
                    ) transaction_file
                        LEFT JOIN master.variable v ON v.id::text = transaction_file.dataset ->> 'variableDbId'
                ORDER BY v.abbrev
            )a
        `
        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        let variableId = variableList[0]['variableId']
        let columns = variableList[0]['columns']
        let fileQuery = `
            SELECT
                *
            FROM
                CROSSTAB(
                '
                    SELECT 
                        (dataset ->> ''entityDbId'')::int AS crossDbId,
                        "cross".cross_name AS "crossName",
                        "cross".cross_method AS "crossMethod",
                        germplasm.designation AS "germplasmDesignation",
                        germplasm.parentage AS "germplasmParentage",
                        germplasm.generation AS "germplasmGeneration",
                        
                        "femaleParent"."crossFemaleParent",
                        "femaleParent"."femaleParentage",
                        "femaleParent"."femaleParentSeedSource",

                        "maleParent"."crossMaleParent",
                        "maleParent"."maleParentage",
                        "maleParent"."maleParentSeedSource",

                        "femaleEntryTbl"."femaleSourceEntryDbId",
                        "femaleEntryTbl"."femaleSourceEntry",
                        "femaleEntryTbl"."femaleSourceSeedName",
                        "femalePlotTbl"."femaleSourcePlotDbId",
                        "femalePlotTbl"."femaleSourcePlot",
                        
                        "maleEntryTbl"."maleSourceEntryDbId",
                        "maleEntryTbl"."maleSourceEntry",
                        "maleEntryTbl"."maleSourceSeedName",
                        "malePlotTbl"."maleSourcePlotDbId",
                        "malePlotTbl"."maleSourcePlot",
                        (dataset ->> ''variableDbId'')::int AS variable_id,
                        CASE WHEN (dataset ->> ''value'') IS NULL THEN ''voided'' ELSE (dataset ->> ''value'') END
                    FROM (
                        SELECT 
                            JSONB_ARRAY_ELEMENTS(dataset) AS dataset
                        FROM 
                            data_terminal.transaction_file
                        WHERE
                            id= ${transactionFileDbId}
                    ) transaction_file
                        LEFT JOIN germplasm.cross "cross"
                        ON "cross".id::text =  (transaction_file.dataset ->> ''entityDbId'')
                        LEFT JOIN germplasm.cross_data cd 
                            ON cd.cross_id = "cross".id
                        LEFT JOIN germplasm.germplasm germplasm
                            ON "cross".germplasm_id = germplasm.id
                        LEFT JOIN
                            (
                                SELECT
                                    e.id AS "femaleSourceEntryDbId",
                                    e.entry_code AS "femaleSourceEntry",
                                    s.seed_name AS "femaleSourceSeedName",
                                    cross_parent.cross_id AS "crossDbId",
                                    germplasm.designation AS "crossFemaleParent"
                                FROM
                                    germplasm.cross_parent cross_parent
                                    LEFT JOIN
                                        germplasm.germplasm germplasm ON germplasm.id = cross_parent.germplasm_id
                                    LEFT JOIN
                                        germplasm.seed s ON s.id = cross_parent.seed_id
                                    LEFT JOIN
                                        experiment.entry e ON e.id = s.source_entry_id
                                WHERE
                                    cross_parent.parent_role = ''female'' AND
                                    cross_parent.is_void = false AND
                                    s.is_void = FALSE AND
                                    e.is_void = FALSE
                            ) AS "femaleEntryTbl" ON "femaleEntryTbl"."crossDbId" = "cross".id
                        LEFT JOIN
                            (
                                SELECT
                                    e.id AS "maleSourceEntryDbId",
                                    e.entry_code AS "maleSourceEntry",
                                    s.seed_name AS "maleSourceSeedName",
                                    cross_parent.cross_id AS "crossDbId"
                                FROM
                                    germplasm.cross_parent cross_parent
                                    LEFT JOIN
                                        germplasm.seed s ON s.id = cross_parent.seed_id
                                    LEFT JOIN
                                        experiment.entry e ON e.id = s.source_entry_id
                                WHERE
                                    cross_parent.parent_role = ''male'' AND
                                    cross_parent.is_void = FALSE AND
                                    s.is_void = FALSE AND
                                    e.is_void = FALSE
                            ) AS "maleEntryTbl" ON "maleEntryTbl"."crossDbId" = "cross".id
                        LEFT JOIN
                            (
                                SELECT
                                    p.id AS "femaleSourcePlotDbId",
                                    p.plot_code AS "femaleSourcePlot",
                                    cross_parent.cross_id AS "crossDbId"
                                FROM
                                    germplasm.cross_parent cross_parent
                                    LEFT JOIN
                                        germplasm.seed s ON s.id = cross_parent.seed_id
                                    LEFT JOIN
                                        experiment.plot p ON p.id = s.source_plot_id
                                WHERE
                                    cross_parent.parent_role = ''female'' AND
                                    cross_parent.is_void = FALSE AND
                                    p.is_void = FALSE
                            ) AS "femalePlotTbl" ON "femalePlotTbl"."crossDbId" = "cross".id
                        LEFT JOIN
                            (
                                SELECT
                                    p.id AS "maleSourcePlotDbId",
                                    p.plot_code AS "maleSourcePlot",
                                    cross_parent.cross_id AS "crossDbId"
                                FROM
                                    germplasm.cross_parent cross_parent
                                    LEFT JOIN
                                        germplasm.seed s ON s.id = cross_parent.seed_id
                                    LEFT JOIN
                                        experiment.plot p ON p.id = s.source_plot_id
                                WHERE
                                    cross_parent.parent_role = ''male'' AND
                                    cross_parent.is_void = FALSE AND
                                    p.is_void = FALSE
                            ) AS "malePlotTbl" ON "malePlotTbl"."crossDbId" = "cross".id
                        LEFT JOIN
                            (
                                SELECT
                                    g.parentage AS "femaleParentage",
                                    cross_parent.cross_id AS "crossDbId",
                                    g.designation AS "crossFemaleParent",
                                    s.seed_code AS "femaleParentSeedSource"
                                FROM
                                    germplasm.cross_parent cross_parent
                                    LEFT JOIN
                                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                                    LEFT JOIN
                                        germplasm.seed s ON s.id = cross_parent.seed_id
                                WHERE
                                    cross_parent.is_void = FALSE AND
                                    cross_parent.parent_role = ''female'' AND
                                    g.is_void = FALSE
                            ) AS "femaleParent" ON "femaleParent"."crossDbId" = "cross".id
                        LEFT JOIN
                            (
                                SELECT
                                    g.parentage AS "maleParentage",
                                    cross_parent.cross_id AS "crossDbId",
                                    g.designation AS "crossMaleParent",
                                    s.seed_code AS "maleParentSeedSource"
                                FROM
                                    germplasm.cross_parent cross_parent
                                    LEFT JOIN
                                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                                    LEFT JOIN
                                        germplasm.seed s ON s.id = cross_parent.seed_id
                                WHERE
                                    cross_parent.is_void = FALSE AND
                                    cross_parent.parent_role = ''male'' AND
                                    g.is_void = FALSE AND
                                    s.is_void = FALSE
                            ) AS "maleParent" ON "maleParent"."crossDbId" = "cross".id
                    WHERE
                        (dataset ->> ''entity'')= ''cross_data''
                    ORDER BY "cross".id
                ',
                '
                    SELECT UNNEST(ARRAY[${variableId}])
                '       
                ) AS ("crossDbId" text, "crossName" text , "crossMethod" text , 
                "germplasmDesignation" text, "germplasmParentage" text , "germplasmGeneration" text , 
                "crossFemaleParent" text , "femaleParentage" text , "femaleParentSeedSource" text , 
                "crossMaleParent" text , "maleParentage" text , "maleParentSeedSource" text , 
                "femaleSourceEntryDbId" integer , "femaleSourceEntry" text , "femaleSourceSeedName" text , 
                "femaleSourcePlotDbId" integer , "femaleSourcePlot" text , 
                "maleSourceEntryDbId" integer , "maleSourceEntry" text , "maleSourceSeedName" text , 
                "maleSourcePlotDbId" integer , "maleSourcePlot" text , 
                     ${columns})
        `
        // Retrieve transaction file from the database
        let transactionFile = await sequelize.query(fileQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        res.send(200, {
            rows: transactionFile
        })
        return
    }
}