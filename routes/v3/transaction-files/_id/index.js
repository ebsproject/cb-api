/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let forwarded = require('forwarded-for')
let validator = require('validator')

module.exports = {

    /**
     * Retrieve specific record in data_terminal.transaction_file
     * GET /v3/transaction-files/:id
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    get: async function (req, res) {

        let transactionFileDbId = req.params.id

        // check if transactionFileDbId is valid
        if (!validator.isInt(transactionFileDbId)) {
            let errMsg = 'Invalid format, transactionFileDbId must be an integer'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transactionFileDbId exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction_file 
                WHERE 
                    id=${transactionFileDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The transaction file ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') AS "variableId",
                STRING_AGG( abbrev || ' text', ',') AS columns
            FROM(
                SELECT 
                    distinct ((dataset ->> 'variableDbId')::int) AS variable_id,
                    v.abbrev
                FROM 
                    (
                        SELECT 
                            JSONB_ARRAY_ELEMENTS(dataset) AS dataset
                        FROM 
                            data_terminal.transaction_file
                        WHERE
                            id= ${transactionFileDbId}
                    ) transaction_file
                        LEFT JOIN master.variable v ON v.id::text = transaction_file.dataset ->> 'variableDbId'
                ORDER BY v.abbrev
            )a
        `
        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        let variableId = variableList[0]['variableId']
        let columns = variableList[0]['columns']
        let fileQuery = `
            SELECT
                *
            FROM
                CROSSTAB(
                '
                    SELECT 
                        (dataset ->> ''entityDbId'')::int AS entity_id,
                        l.location_code AS "locationCode" ,
                        o.occurrence_code AS "occurrenceCode" ,
                        p.plot_code AS "plotCode",
                        p.plot_number AS "plotNumber",
                        p.plot_type AS "plotType",
                        p.rep, 
                        p.design_x AS "designX",
                        p.design_y AS "designY",
                        p.pa_x AS "paX",
                        p.pa_y AS "paY",
                        p.field_x AS "fieldX",
                        p.field_y AS "fieldY",
                        (dataset ->> ''variableDbId'')::int AS variable_id,
                        CASE WHEN (dataset ->> ''value'') IS NULL THEN ''voided'' ELSE (dataset ->> ''value'') END
                    FROM (
                        SELECT 
                            JSONB_ARRAY_ELEMENTS(dataset) AS dataset
                        FROM 
                            data_terminal.transaction_file
                        WHERE
                            id= ${transactionFileDbId}
                    ) transaction_file
                        LEFT JOIN experiment.plot p ON p.id::text=(dataset ->> ''entityDbId'')
                        LEFT JOIN experiment.location l ON l.id= p.location_id
                        LEFT JOIN experiment.occurrence o ON o.id= p.occurrence_id
                    WHERE
                        (dataset ->> ''entity'')= ''plot_data''
                    ORDER BY p.plot_order_number
                ',
                '
                    SELECT UNNEST(ARRAY[${variableId}])
                '       
                ) AS ("plotDbId" text, "locationCode" text , "occurrenceCode" text , 
                    "plotCode" text , "plotNumber" text , "plotType" text , 
                    rep text , "designX" text , "designY" text , "paX" text , 
                    "paY" text , "fieldX" text , "fieldY" text , ${columns})
        `
        // Retrieve transaction file from the database
        let transactionFile = await sequelize.query(fileQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        res.send(200, {
            rows: transactionFile
        })
        return
    },

    
    /**
     * Deletes a transaction files record
     * DELETE v3/transaction-files/:id
     * @param {*} req 
     * @param {*} res 
     */
    delete: async function (req, res) {
        let transactionFileDbId = req.params.id

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let transactionFilesUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/transaction-files/'
            + transactionFileDbId

        // check if transactionFileDbId is integer
        if (!validator.isInt(transactionFileDbId)) {
            let errMsg = 'Invalid format, transaction file ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }   

        // Check if transactionFileDbId exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction_file 
                WHERE 
                    id=${transactionFileDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (!transactionResult[0]['exists']) {
            let errMsg = 'The transaction file record you have requested does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        try {
            let resultArray
            await sequelize.transaction(async transaction => {
                let deleteQuery = `
                    DELETE FROM
                        data_terminal.transaction_file
                    WHERE
                        id = ${transactionFileDbId}
                `
                await sequelize.query(deleteQuery, {
                    type: sequelize.QueryTypes.DELETE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })

                resultArray = {
                    transactionFileDbId: transactionFileDbId,
                    recordCount: 1,
                    href: transactionFilesUrl
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}