/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require ('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
  get: async (req, res, next) => {
    // Retrieve the user ID
    let userDbId = req.params.id
    let count = 0

    if (!validator.isInt(userDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400077)
      res.send(new errors.BadRequestError(errMsg))
      return
    } 

    let userQuery = `
      SELECT
        id
      FROM
        tenant.person
      WHERE
        is_void = FALSE AND
        id = ${userDbId}
    `
    // Validate if user ID exists in the database
    let userRecord = await sequelize.query(userQuery, {
      type: sequelize.QueryTypes.SELECT
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    if (await userRecord == undefined || await userRecord.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404027)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Retrieve the user ID of the client from the access token
    let userId = await tokenHelper.getUserId(req)

    if (userId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(userId)

    if (!isAdmin) {
      if (userId != userDbId) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401005)
        res.send(new errors.UnauthorizedError(errMsg))
        return
      }
    }

    let userProgramsQuery = `
    SELECT 
      DISTINCT ON (program.id) program.id as "programDbId",
      program.program_code as "programCode",
      program.program_name "programName",
      program.description,
      program.program_type as "programType",
      program.program_status as "programStatus",
      program.crop_program_id as "cropProgramDbId",
      creator.id AS "creatorDbId",
      creator.person_name as "creator",
      program.creation_timestamp AS "creationTimestamp",
      modifier.id AS "modifierDbId",
      modifier.person_name AS "modifier",
      program.modification_timestamp AS "modificationTimestamp"
    FROM 
      tenant.program program
    LEFT JOIN 
      tenant.person creator ON program.creator_id = creator.id
    LEFT JOIN 
      tenant.person modifier ON program.modifier_id = modifier.id
    LEFT JOIN 
      tenant.program_team programTeam ON programTeam.program_id = program.id AND
        programTeam.is_void = FALSE
    LEFT JOIN 
      tenant.team_member teamMember ON teamMember.team_id = programTeam.team_id AND
        teamMember.is_void = FALSE
    LEFT JOIN
      tenant.person "user" ON "user".id = teamMember.person_id
    WHERE
      program.is_void = FALSE AND
      "user".id = ${userDbId}
    `

    // Retrieve users from the database
    let userPrograms = await sequelize.query(userProgramsQuery, {
      type: sequelize.QueryTypes.SELECT
    })
    .catch(async err => {
    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
    })
    
    if (await userPrograms == undefined || await userPrograms.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } 
    else {
      count = await userPrograms.length

      res.send(200, {
        rows: userPrograms,
        count: count
      })
      return
    }
  }
}