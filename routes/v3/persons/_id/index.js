/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, User, Team, TeamMember } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator.js')
let errorBuilder = require('../../../../helpers/error-builder')
let forwarded = require('forwarded-for')

module.exports = {
    // Implementation of GET call for /v3/persons/:id
    get: async (req, res, next) => {
        // Retrieve the user ID
        let userDbId = req.params.id

        if (!validator.isInt(userDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400077)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            // Retrieve the user ID of the client from the access token
            let userId = await tokenHelper.getUserId(req)

            if (userId == null) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401002)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            let personsQuery = ''

            if ((userId == userDbId) || await userValidator.isAdmin(userId)) {
                personsQuery = `
                    SELECT
                        person.id AS "personDbId",
                        person.email,
                        person.username,
                        person.first_name AS "firstName",
                        person.last_name AS "lastName",
                        person.person_name AS "personName",
                        person.person_type AS "personType",
                        person.person_status AS "personStatus",
                        person.person_role_id AS "personRoleId",
                        person.is_active AS "isActive",
                        person.creation_timestamp AS "creationTimestamp",
                        person.creator_id AS "creatorDbId",
                        creator.person_name AS "creator",
                        person.modification_timestamp AS "modificationTimestamp",
                        person.modifier_id AS "modifierDbId",
                        modifier.person_name AS "modifier"
                    FROM
                        tenant.person person
                    LEFT JOIN 
                        tenant.person creator ON creator.id = person.creator_id
                    LEFT JOIN 
                        tenant.person modifier ON modifier.id = person.modifier_id
                    WHERE 
                        person.id = (:userDbId) AND
                        person.is_void = FALSE
                `
            } else {
                personsQuery = `
                    SELECT 
                        person.id AS "personDbId",
                        person.person_name AS "personName"
                    FROM
                        tenant.person person
                    WHERE 
                        person.id = (:userDbId) AND
                        person.is_void = FALSE
                `
            }

            // Retrieve persons from the database   
            let persons = await sequelize.query(personsQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    userDbId: userDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found      
            if (await persons == undefined || await persons.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404027)
                res.send(new errors.NotFoundError(errMsg))
                return
            } else {
                res.send(200, {
                rows: persons
                })
                return 
            }
        }
    },

    // Implementation of POST call for /v3/persons/:id
    put: async function (req, res, next) {
        // Check if user is an administrator. If not, no access.
        let modifierDbId = await tokenHelper.getUserId(req)

        if (modifierDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(modifierDbId)
        if (!isAdmin) {
            let errMsg = `401: Unauthorized request. You do not have access to update this person's account.`
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Retrieve user ID
        let userDbId = req.params.id

        // Check if user ID data type is valid
        if (!validator.isInt(userDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400077)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if request body exists
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let email = null
        let firstName = null
        let lastName = null
        let personName = null
        let personType = null
        let personStatus = null
        let personRoleId = null
        let isActive = null

        // Set valid values
        let validEmailDomains = [
            'irri.org',
            'gmail.com',
            'yahoo.com',
            'cgiar.org',
            'cimmyt.org',
            'cornell.edu',
        ]

        let resultArray = []

        // Build query
        let personQuery = `
            SELECT
                person.email,
                person.username,
                person.first_name AS "firstName",
                person.last_name AS "lastName",
                person.person_name AS "personName",
                person.person_type AS "personType",
                person.person_status AS "personStatus",
                person.person_role_id AS "personRoleId",
                person.is_active AS "isActive"
            FROM
                tenant.person person
            WHERE
                person.is_void = FALSE AND
                person.id = ${userDbId}
        `

        // Retrieve person record from database
        let person = await sequelize.query(personQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (await person == undefined || await person.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404027)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database
        let currentEmail = person[0].email || null
        let currentUsername = person[0].username || null
        let currentFirstName = person[0].firstName || null
        let currentLastName = person[0].lastName || null
        let currentPersonName = person[0].personName || null
        let currentPersonType = person[0].personType || null
        let currentPersonStatus = person[0].personStatus || null
        let currentPersonRoleId = person[0].personRoleId || null
        let currentIsActive = person[0].isActive || null

        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let personUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/persons"

        let data = req.body
        let transaction

        try {
            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /* Validate and set values */

            // Validate email
            if (data.email !== undefined) {
                // Validate first if email domain is valid
                if (!validEmailDomains.includes(data.email.split('@')[1])) {
                    let errMsg = '400: Email domain is invalid. Please ensure email domain is whitelisted in the system.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                email = data.email

                // Emulate "system-generated" username
                username = data.email.split('@')[0]

                if (currentUsername != username) {
                    // Build username validation query
                    let findUsernamesQuery = `
                        SELECT COUNT
                            (username)
                        FROM
                            tenant.person person
                        WHERE
                            person.is_void = FALSE AND
                            person.email != '${username}@${currentEmail.split('@')[1]}' AND
                            person.username ~ '^${username}([0-9]+)?$' AND
                            person.username != '${currentUsername}'
                    `

                    // Check if username is already existing in the databse
                    let usernameCount = await sequelize.query(findUsernamesQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    // Append number to tail-end of username
                    if (
                        usernameCount != undefined &&
                        usernameCount[0] != undefined &&
                        usernameCount[0]['count'] != undefined &&
                        parseInt(usernameCount[0]['count']) > 0
                    )
                        username += (usernameCount[0]['count'])
                }

                // Check if the input email differs from email found in database
                if (currentEmail != email) {

                    // Validate email
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            SELECT
                                CASE WHEN (count(1) = 0)
                                THEN 1
                                ELSE 0
                                END AS count
                            FROM
                                tenant.person person
                            WHERE
                                person.is_void = FALSE AND
                                person.email LIKE '${email}' AND
                                person.id != ${userDbId}
                        )
                    `
                    validateCount += 1
                }

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    email = $$${(currentEmail != email) ? email : currentEmail}$$
                `
                
                setQuery += ','
                setQuery += `
                    username = $$${(currentUsername != username) ? username : currentUsername}$$
                `
            } else { // if no input, assign current value
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    email = $$${currentEmail}$$
                `
                setQuery += ','
                setQuery += `
                    username = $$${currentUsername}$$
                `
            }

            // Validate first_name
            if (data.firstName != undefined) {
                firstName = data.firstName

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    first_name = $$${(currentFirstName != firstName) ? firstName : currentFirstName}$$
                `
            } else { // if no input, assign current value
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    first_name = $$${currentFirstName}$$
                `
            }

            // Validate last_name
            if (data.lastName != undefined) {
                lastName = data.lastName

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    last_name = $$${(currentLastName != lastName) ? lastName : currentLastName}$$
                `
            } else { // if no input, assign current value
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    last_name = $$${currentLastName}$$
                `
            }

            newFirstName = (data.firstName == undefined) ? currentFirstName : data.firstName
            newLastName = (data.lastName == undefined) ? currentLastName : data.lastName

            // emulate "system-generated" display name
            if (
                (newFirstName != null || newFirstName != '') &&
                (newLastName != null || newLastName != '')
            )
                personName = newLastName.concat(`, ${newFirstName}`)
            else
                personName = currentPersonName

            setQuery += (setQuery != '') ? ',' : ''
            setQuery += `
                person_name = $$${(currentPersonName != personName) ? personName : currentPersonName}$$
            `

            // Validate person_type
            if (data.personType != undefined) {
                personType = data.personType.trim()

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'PERSON_TYPE' AND
                            scaleValue.value ILIKE $$${personType}$$
                    )
                `
                validateCount += 1

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    person_type = $$${(currentPersonType != personType) ? personType : currentPersonType}$$
                `
            } else { // if no input, assign current value
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    person_type = $$${currentPersonType}$$
                `
            }

            // Validate person_status
            if (data.personStatus != undefined) {
                personStatus = data.personStatus.trim()

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'PERSON_STATUS' AND
                            scaleValue.value ILIKE $$${personStatus}$$
                    )
                `
                validateCount += 1

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    person_status = $$${(currentPersonStatus != personStatus) ? personStatus : currentPersonStatus}$$
                `
            } else { // if no input, assign current value
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    person_status = $$${currentPersonStatus}$$
                `
            }

            // Validate person_role_id
            if (data.personRoleId != undefined) {
                // Validate data type
                if (!Number.isInteger(data.personRoleId)) {
                    let errMsg = '400: Invalid format for person role ID. Please ensure ID is an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                personRoleId = data.personRoleId

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    person_role_id = $$${(currentPersonRoleId != personRoleId) ? personRoleId : currentPersonRoleId}$$
                `
            } else { // if no input, assign current value
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    person_role_id = $$${currentPersonRoleId}$$
                `
            }
            // Validate is_active
            if (data.isActive != undefined) {
                isActive = data.isActive

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    is_active = $$${(currentIsActive != isActive) ? isActive : currentIsActive}$$
                `
            } else { // if no input, assign current value
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    is_active = $$${currentIsActive}$$
                `
            }

            /* Validate input in DB */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            // Update the user account record
            let updateUserQuery = `
                UPDATE
                    tenant.person person
                SET
                    ${setQuery},
                    modification_timestamp = NOW(),
                    modifier_id = ${modifierDbId}
                WHERE
                    person.id = ${userDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updateUserQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let array = {
                userDbId: userDbId,
                recordCount: 1,
                href: personUrlString + '/' + userDbId
            }
            resultArray.push(array)

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Implementation of DELETE call for /v3/persons/:id
    delete: async function (req, res, next) {
        // Check if user is an administrator
        let modifierDbId = await tokenHelper.getUserId(req)

        if (modifierDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        const isAdmin = await userValidator.isAdmin(modifierDbId)
        if (!isAdmin) {
            let errMsg = 'Unauthorized request. You do not have access to delete this user account.'
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Defaults
        let userDbId = req.params.id

        if (!validator.isInt(userDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400077)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else {
            // Start transaction
            let transaction

            try {
                // Get transaction
                transaction = await sequelize.transaction({ autocommit: false })

                // Validate the user
                // Check if user is existing
                // Build query
                let userQuery = `
                    SELECT
                        count(1)
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = ${userDbId}
                `

                // Retrieve user accounts from the database
                let hasUser = await sequelize.query(userQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                    .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                // Check if user is non-existent in database
                if (parseInt(hasUser[0].count) === 0) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 404027)
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                // Build query for voiding
                let deleteUserQuery = `
                    UPDATE
                        tenant.person person
                    SET
                        is_void = TRUE,
                        email = CONCAT(person.email, '-VOIDED-', person.id),
                        username = CONCAT(person.username, '-VOIDED-', person.id),
                        modification_timestamp = NOW(),
                        modifier_id = ${modifierDbId}
                    WHERE
                        person.id = ${userDbId}
                `

                await sequelize.query(deleteUserQuery, {
                    type: sequelize.QueryTypes.UPDATE
                })

                // Commit the transaction
                await transaction.commit()

                res.send(200, {
                    rows: {
                        userDbId: userDbId
                    }
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500002)
                res.send(new errors.InternalError(errMsg))
                return
            }
        }
    }
}