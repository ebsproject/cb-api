/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let Sequelize = require('sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errorBuilder = require('../../../../helpers/error-builder')
let validator = require('validator')
let errors = require('restify-errors')
let format = require('pg-format')
let forwarded = require('forwarded-for')
let userValidator = require('../../../../helpers/person/validator.js')
let logger = require('../../../../helpers/logger')

module.exports = {
  get: async (req, res, next) => {
    let userDbId = req.params.id
    if (!validator.isInt(userDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400037)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if the current user is the owner of the configuration being accessed
    let currentlyLoggedInUser = await tokenHelper.getUserId(req)

    if (currentlyLoggedInUser == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    if (!await userValidator.isAdmin(currentlyLoggedInUser) && userDbId != currentlyLoggedInUser) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401017)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let dashboardConfigurationQuery = `
      SELECT
        dashboard.id AS "dashboardConfigDbId",
        owner.id AS "personDbId",
        owner.person_name AS "personDisplayName",
        dashboard.data,
        dashboard.description,
        dashboard.remarks,
        dashboard.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS "creator",
        dashboard.modification_timestamp AS "modificationTimestamp",
        dashboard.modifier_id AS "modifierDbId",
        modifier.person_name AS "modifier"
      FROM
        platform.user_dashboard_config dashboard
      JOIN
        tenant.person owner ON dashboard.user_id = owner.id
      JOIN
        tenant.person creator ON dashboard.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON dashboard.modifier_id = modifier.id
      WHERE
        dashboard.is_void = FALSE AND
        dashboard.user_id = ${userDbId}
    `

    // Retrieve configuration
    let dashboardConfiguration = await sequelize
      .query(dashboardConfigurationQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async err => {
        await logger.logFailingQuery('GET user/_id/dashboard-configurations', 'SELECT', err)

        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Error occurred during query
    if (dashboardConfiguration === undefined) return

    if (await dashboardConfiguration == undefined || await dashboardConfiguration.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404040)
      res.send(new errors.NotFoundError(errMsg))
      return
    } else {
      res.send(200, {
        rows: dashboardConfiguration
      })
      return 
    }

  },
  
  post: async (req, res, next) => {
    let userDbId = req.params.id

    if (!validator.isInt(userDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400155)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = null
    let description = null
    let remarks = null

    let isSecure = forwarded(req, req.headers).secure

    let dashboardUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/persons/dashboard-configurations'

    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let record = req.body
    let transaction
    try {
      transaction = await sequelize.transaction({ autocommit: false })

      if (!userDbId || !record.data) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400156)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      // Validate if there are no other dashboard configuration with the user ID in DB
      let validateQuery = `
        SELECT
          CASE WHEN
            (count(1) = 0)
            THEN 1
            ELSE 0
          END AS count
        FROM
          platform.user_dashboard_config dashboardConfig
        WHERE
          dashboardConfig.is_void = FALSE AND
          dashboardConfig.user_id = ${userDbId}
      `
      let validateCount = 0
      validateCount = await sequelize.query(validateQuery, {
        type: sequelize.QueryTypes.SELECT
      })

      if (!validateCount[0]['count']) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400157)
        res.send(new errors.BadRequestError(errMsg))
        return
      }


      remarks = (record.remarks !== undefined) ? record.remarks : null
      description = (record.description !== undefined) ? record.description : null

      data = record.data
      // Validate if data is a valid JSON string
      try {
        let dataValidation = JSON.parse(data)
        let dashboardConfiguration = [
          userDbId, data, description, remarks, 'Created via CB API', userDbId
        ]
        let dashboardConfigQuery = format(`
          INSERT INTO
            platform.user_dashboard_config (
              user_id, data, description, remarks, notes, creator_id
            )
          VALUES (
            %L
          )
          RETURNING id`, dashboardConfiguration
        )

        dashboardConfigQuery = dashboardConfigQuery.trim()

        let dashboardConfig = await sequelize.query(dashboardConfigQuery, {
          type: sequelize.QueryTypes.INSERT,
          transaction: transaction
        })

        let result = {
          dashboardConfigId: dashboardConfig[0][0]['id'],
          recordCount: 1,
          href: dashboardUrlString + '/' + dashboardConfig[0][0]['id']
        }

        await transaction.commit()

        res.send(200, {
          rows: result
        })
        return

      } catch (err) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400158)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  },
  put: async (req, res, next) => {
    let userDbId = req.params.id

    if (!validator.isInt(userDbId)) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400077)
        res.send(new errors.BadRequestError(errMsg))
        return
    }

    let userQuery = `
      SELECT
        id
      FROM
        tenant.person
      WHERE
        is_void = FALSE
        AND id = ${userDbId}
    `
    // Validate if user ID exists in the database
    let userRecord = await sequelize.query(userQuery, {
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await userRecord == undefined || await userRecord.length < 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404027)
        res.send(new errors.NotFoundError(errMsg))
        return
    }

    // Retrieve the user ID of the client from the access token
    let userId = await tokenHelper.getUserId(req)

    if (userId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(userId)

    if (!isAdmin) {
      if (userId != userDbId) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401005)
        res.send(new errors.UnauthorizedError(errMsg))
        return
      }
    }

    let resultArray = []
    let data = null                 // data request body
    let record = req.body
    let transaction

    // Retrieve data from database
    let userDashboardConfigsQuery = `
      SELECT 
        dashboard.data,
        dashboard.notes
      FROM
        platform.user_dashboard_config dashboard
        LEFT JOIN
          tenant.person owner ON dashboard.user_id = owner.id
        LEFT JOIN
          tenant.person creator ON dashboard.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON dashboard.modifier_id = modifier.id
      WHERE
        dashboard.is_void = FALSE AND
        dashboard.user_id = ${userDbId}
    `

    let userDashboardConfigs = await sequelize.query(userDashboardConfigsQuery, {
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await userDashboardConfigs == undefined || await userDashboardConfigs.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404040)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    let isSecure = forwarded(req, req.headers).secure

    // Set URL for response
    let userDashboardConfigsUrlString = (isSecure ? 'https' : 'http')
      + "://"
      + req.headers.host
      + "/v3/persons/" + userDbId + "/dashboard-configurations"

    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    try {

      let setQuery = ``

      if (!userDbId || !record.data) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400156)
          res.send(new errors.BadRequestError(errMsg))
          return
      }

      data = record.data

      // Validate data if JSON string
      try {
        let dataValidation = JSON.parse(data)

        if (data != undefined) {
          setQuery += (setQuery != '') ? ',' : ''
            setQuery += `
              data = $$${data}$$`
        }

        // Update user dashboard configs record
        let updateUserDashboardConfigsQuery = `
          UPDATE 
            platform.user_dashboard_config userDashboardConfig
          SET
            ${setQuery},
            modification_timestamp = NOW(),
            modifier_id = ${userId}
          WHERE
            userDashboardConfig.user_id = ${userDbId}
        `

        // Get transaction
        transaction = await sequelize.transaction({ autocommit : false })

        await sequelize.query(updateUserDashboardConfigsQuery, {
          type: sequelize.QueryTypes.UPDATE,
          transaction: transaction
        })

        let array = {
          userDbId: userDbId,
          recordCount: 1,
          href: userDashboardConfigsUrlString
        }

        resultArray.push(array)

        await transaction.commit()

        res.send(200, {
          rows: resultArray,
          count: 1
        })
        return

      } catch (err) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400158)
          res.send(new errors.BadRequestError(errMsg))
          return
      }

    } catch (err) {
        if (err) await transaction.rollback()
        let errMsg = await errorBuilder.getError(req.headers.host, 500003)
        res.send(new errors.InternalError(errMsg))
        return
    }
  }
}