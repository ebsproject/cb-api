/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator.js')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    get: async (req, res, next) => {

        // Retreive the user ID

        let userDbId = req.params.id
        let count = 0

        if (!validator.isInt(userDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400077)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let userQuery = `
            SELECT
                id
            FROM
                tenant.person
            WHERE
                is_void = FALSE
                AND id = ${userDbId}
        `
        // Validate if user ID exists in the database
        let userRecord = await sequelize.query(userQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                // sequelize.close()
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await userRecord == undefined || await userRecord.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404027)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(userId)

        if (!isAdmin) {
            if (userId != userDbId) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401005)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let userTeamsQuery = `
            SELECT
                team.id AS "teamDbId",
                team.team_name AS "teamName",
                team.team_code AS "teamCode",
                role.id AS "roleDbId",
                role.person_role_code AS "personRoleCode",
                role.person_role_name AS "personRoleName",
                team.description,
                member.id AS "teamMemberDbId"
            FROM  
                tenant.team team 
            LEFT JOIN 
                tenant.team_member member ON member.team_id=team.id 
                AND member.is_void = false
            LEFT JOIN 
                tenant.person "user" ON "user".id = member.person_id
            LEFT JOIN 
                tenant.person_role role ON role.id = member.person_role_id
            WHERE
                team.is_void = FALSE
                AND team.team_code != 'SG'
                AND "user".id = ${userDbId}
        `

        // Retrieve users from the database
        let userTeams = await sequelize.query(userTeamsQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await userTeams == undefined || await userTeams.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }
        else {
            // Get count 
            userTeamsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(userTeamsQuery, '', '')

            userTeamsCount = await sequelize.query(userTeamsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = await  userTeamsCount[0].count

            res.send(200, {
                rows: userTeams,
                count: count
            })
            return
        }
    }
}