/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, User, Team, TeamMember } = require('../../../config/sequelize')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token')
let userValidator = require('../../../helpers/person/validator.js')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {
    // Implementation of GET call for /v3/persons
    get: async (req, res, next) => {
        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''
        let orderString = ''

        let personsQuery = ''

        let selectClause = ''
        let whereClause = ''
        let orderByClause = ''

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let addedConditionString = ''

        // Check if user is an administrator. If no, display minimal information only.
        if (!await userValidator.isAdmin(userId)) {
            selectClause = `
                SELECT 
                    person.id AS "userDbId",
                    person.id AS "personDbId",
                    person.person_name AS "personName"
                FROM
                    tenant.person person
            `

            // Check whether API caller needs both active and inactive seasons
            if (params.forDataBrowser !== undefined) {
                whereClause = `
                    WHERE 
                        person.is_void = FALSE
                    ` + addedConditionString + `
                `
            } else {
                whereClause = `
                    WHERE 
                        person.is_void = FALSE AND
                        person.is_active = TRUE
                    ` + addedConditionString + `
                `
            }

            orderByClause = `
                ORDER BY
                person.id
            `

            personsQuery = selectClause + whereClause + orderByClause
        } else {
            selectClause = `
                SELECT 
                    person.id AS "personDbId",
                    person.email,
                    person.username,
                    person.first_name AS "firstName",
                    person.last_name AS "lastName",
                    person.person_name AS "personName",
                    person.person_type AS "personType",
                    person.person_status AS "personStatus",
                    person.person_role_id AS "personRoleId",
                    person.is_active AS "isActive",
                    person.creation_timestamp AS "creationTimestamp",
                    person.creator_id AS "creatorDbId",
                    creator.person_name AS "creator",
                    person.modification_timestamp AS "modificationTimestamp",
                    person.modifier_id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    tenant.person person
                LEFT JOIN 
                    tenant.person creator ON creator.id = person.creator_id
                LEFT JOIN 
                    tenant.person modifier ON modifier.id = person.modifier_id
            `

            // Check whether API caller needs both active and inactive seasons
            if (params.forDataBrowser !== undefined) {
                whereClause = `
                    WHERE 
                        person.is_void = FALSE
                        ` + addedConditionString + `
                `
            } else {
                whereClause = `
                    WHERE 
                        person.is_void = FALSE AND
                        person.is_active = TRUE
                        ` + addedConditionString + `
                `
            }

            orderByClause = `
                ORDER BY
                person.id
            `

            personsQuery = selectClause + whereClause + orderByClause
        }

        // Get filter condition for person name only
        if (params.personName !== undefined) {
            let parameters = {
                personName: params.personName
            }

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = '400: You have provided an invalid value for personName filter'
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        personFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            personsQuery,
            conditionString,
            orderString
        )

        // Retrieve persons from the database   
        let persons = await sequelize.query(personFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
                })

        if (await persons == undefined || await persons.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get count 
            personCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                personsQuery,
                conditionString,
                orderString
            )

            personCount = await sequelize.query(personCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = personCount[0].count
        }

        res.send(200, {
            rows: persons,
            count: count
        })
        return
    },

    // Implementation of POST call for /v3/persons
    post: async function (req, res, next) {
        // Set defaults
        let email = null
        let username = null
        let firstName = null
        let lastName = null
        let personName = null
        let personType = null
        let personStatus = null
        let personRoleId = null
        let isActive = null

        // Set valid email domains
        let validEmailDomains = [
            'irri.org',
            'gmail.com',
            'yahoo.com',
            'cgiar.org',
            'cimmyt.org',
            'cornell.edu',
        ]

        // Verify if user is an admin
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        if (!await userValidator.isAdmin(userDbId)) {
            let errMsg = '401: You are not allowed to perform this operation. Make sure you have the proper role assigned to you.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let personUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/persons'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            /*
                Records = [
                {},
                {},
                ...
                ]
            */
            records = data.records
            recordCount = records.length
            // Check if the input data is empty
            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Start transaction
        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let userValuesArray = []
            let visitedArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Check if the required colums are in the input
                if (
                    !record.email ||
                    !record.firstName ||
                    !record.lastName ||
                    !record.personType ||
                    !record.isActive
                ) {
                    let errMsg = '400: Required parameters are missing. Ensure that the email, firstName, lastName, personType, and isActive fields are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /* Validate and set values */

                // Validate first if email domain is valid
                if (!validEmailDomains.includes(record.email.split('@')[1])) {
                    let errMsg = '400: Email domain is invalid.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                } else {
                    email = record.email

                    // Validate if the email already exists in the DB
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the email already exists; if so, return 0
                            SELECT
                                CASE WHEN (count(1) > 0)
                                THEN 0
                                ELSE 1
                                END
                            FROM
                                tenant.person person
                            WHERE
                                person.is_void = FALSE AND
                                person.email = '${email}'
                        )
                    `
                    validateCount += 1
                }

                // Emulate "system-generated" username
                username = record.email.split('@')[0]

                // Build username validation query
                let findUsernamesQuery = `
                    SELECT COUNT
                        (username)
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.username ~ '^${username}([0-9]+)?$'
                `

                // Check if username is already existing in the databse
                let usernameCount = await sequelize.query(findUsernamesQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                // Append number to tail-end of username
                if (
                    usernameCount != undefined &&
                    usernameCount[0] != undefined &&
                    usernameCount[0]['count'] != undefined &&
                    parseInt(usernameCount[0]['count']) > 0
                )
                    username += (usernameCount[0]['count'])

                // Store first and last name
                firstName = record.firstName
                lastName = record.lastName

                // Emulate "system-generated" person_name
                personName = record.lastName.concat(`, ${record.firstName}`)

                // Validate personType
                personType = record.personType.trim()

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'PERSON_TYPE' AND
                            scaleValue.value ILIKE $$${personType}$$
                    )
                `
                validateCount += 1

                // Validate personStatus
                if (record.personStatus !== undefined) {
                    personStatus = record.personStatus.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'PERSON_STATUS' AND
                                scaleValue.value ILIKE $$${personStatus}$$
                        )
                    `
                    validateCount += 1
                } else {
                    personStatus = 'active'
                }

                personRoleId = (record.personRoleId != undefined) ? record.personRoleId : 21
                isActive = record.isActive
                creatorId = userDbId

                // Validate the filters in DB; Count must be equal to validateCount
                let checkInputQuery = `SELECT ${validateQuery} AS count`

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (recordCount > 1) {
                    // Check if email is unique among bulk inputs
                    if (visitedArray.includes(email)) {
                        let errMsg = '400: Invalid request. You have provided duplicate emails.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else {
                        visitedArray.push(email)
                    }
                }

                    let tempArray = [
                    email,
                    username,
                    firstName,
                    lastName,
                    personName,
                    personType,
                    personStatus,
                    personRoleId,
                    isActive,
                    creatorId
                ]

                userValuesArray.push(tempArray)
            }

            // Create person record
            let personQuery = format(`
                INSERT INTO
                    tenant.person
                        (
                        email,
                        username,
                        first_name,
                        last_name,
                        person_name,
                        person_type,
                        person_status,
                        person_role_id,
                        is_active,
                        creator_id
                        )
                VALUES
                    %L
                RETURNING id`, userValuesArray
            )

            personQuery = personQuery.trim()

            let persons = await sequelize.query(personQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            let resultArray = []

            for (let person of persons[0]) {
                userDbId = person.id

                // Return the user info to Client
                let array = {
                    userDbId: userDbId,
                    recordCount: 1,
                    href: personUrlString + '/' + userDbId
                }

                resultArray.push(array)
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
