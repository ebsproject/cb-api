/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let queryToolHelper = require('../../../helpers/queryTool/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let distinctString = ''
        let fields = null
        let distinctOn = ''
        let searchQuery = ''

        if (req.body != null) {
            // Set columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', 'distinctOn']
            if (req.body.fields != null) {
                fields = req.body.fields
            }

            let parameters = req.body
            let browserFilters = []
            let condition = ``
            //Build query
            for (column in parameters) {
                if (excludedParametersArray.includes(column)) {
                    continue
                }

                if (column == '__browser_filters__') {
                    browserFilters = parameters['__browser_filters__']
                    continue
                }

                let filters = parameters[column]
                condition = ``
                condition = queryToolHelper.getFilterCondition(filters, column)
                if (condition !== '') {
                    condition = `(`+condition+`)`
                    if (searchQuery == '') {
                    searchQuery = searchQuery +
                        ` WHERE `
                        + condition
                    } else {
                    searchQuery = searchQuery +
                        ` AND `
                        + condition
                    }
                }
            }

            // check if request came from cb browser; add filter from it
            if (browserFilters !== undefined && browserFilters.length != 0) {
                for (column in browserFilters) {
                    let filters = browserFilters[column]
                    let condition = queryToolHelper.getFilterCondition(filters, column)
                    if(['dataLevel', 'description', 'creator', 'modifier'].includes(column)) condition = `("${column}"::text ILIKE ($$${filters.like}$$))`
                    condition = `(`+condition+`)`

                    if (addedConditionString == '') {
                        addedConditionString = addedConditionString
                            + condition
                    } else {
                        addedConditionString = addedConditionString + 
                            ` AND `
                            + condition
                    }
                }
                if (addedConditionString != '') {
                    if (searchQuery == '') {
                        searchQuery = searchQuery +
                            ` WHERE `
                            + addedConditionString
                    } else {
                        searchQuery = searchQuery +
                            ` AND `
                            + addedConditionString
                    }
                }
            }

            if (req.body.distinctOn != undefined) {
                distinctOn = req.body.distinctOn
                distinctString = await processQueryHelper
                    .getDistinctString(
                    distinctOn
                    )
                distinctOn = `"${distinctOn}"`
            }
        }

        conditionString = searchQuery

        // Build the base query
        let variableQuery = null

        let orderQuery = ``
        if (distinctOn) {
            orderQuery = `
            ORDER BY ${distinctOn}
            `
        }

        // Check if the client specified values for the fields parameter
        let fromQuery = `
            FROM
                master.variable variable
            LEFT JOIN
                master.property property ON variable.property_id = property.id
            LEFT JOIN
                master.method method ON variable.method_id = method.id
            LEFT JOIN
                master.scale scale ON variable.scale_id = scale.id
            INNER JOIN tenant.person AS creator
                ON creator.id = variable.creator_id
            LEFT JOIN tenant.person AS modifier
                ON modifier.id = variable.modifier_id
            WHERE
                variable.is_void = FALSE
            ${orderQuery}
        `

        if (fields != null) {
            if (distinctOn) {
                let fieldValues = fields.split('|')
                let fieldValuesString = ''
                for (let value of fieldValues) {
                    fieldValuesString += `${value}, `
                }
                // clean the fieldValuesString
                fieldValuesString = fieldValuesString.replace(/,\s*$/, "");
                let x = knex.raw(`${fieldValuesString}`)
                variableQuery = knex.select(x)
            } else {
                variableQuery = knex.column(fields.split('|'))
            }
            variableQuery += fromQuery
        } else {
            variableQuery = `
                SELECT
                    ${distinctString}
                    variable.id AS "variableDbId",
                    variable.abbrev,
                    variable.label,
                    variable.name,
                    variable.data_type AS "dataType",
                    variable.not_null AS "notNull",
                    variable.type,
                    variable.usage,
                    variable.status,
                    variable.description,
                    variable.display_name AS "displayName",
                    variable.data_level AS "dataLevel",
                    variable.ontology_reference AS "ontologyReference",
                    variable.bibliographical_reference AS "bibliographicalReference",
                    variable.synonym,
                    variable.remarks,
                    variable.is_computed AS "isComputed",
                    variable.target_table AS "targetTable",
                    variable.target_model AS "targetModel",
                    variable.variable_document AS "variableDocument",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    variable.creation_timestamp AS "creationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier",
                    variable.modification_timestamp AS "modificationTimestamp",
                    property.abbrev AS "propertyAbbrev",
                    property.name AS "propertyName",
                    property.description AS "propertyDescription",
                    property.display_name AS "propertyDisplayName",
                    property.remarks AS "propertyRemarks",
                    method.name AS "methodName",
                    method.description AS "methodDescription",
                    method.formula AS "methodFormula",
                    method.remarks AS "methodRemarks",
                    scale.id AS "scaleDbId",
                    scale.type AS "scaleType",
                    scale.unit AS "scaleUnit",
                    scale.level AS "scaleLevel",
                    scale.description AS "scaleDescription"
                ` + fromQuery
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } else {
            if (orderQuery == ``) {
                orderString = `ORDER BY 1 DESC`
            }
        }

        variableFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            variableQuery,
            conditionString,
            orderString,
            distinctString
        )

        // Retrieve variables from the database
        let variables = await sequelize.query(variableFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await variables == undefined || await variables.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get the final count of the records
            let variableCountFinalSqlQuery = await processQueryHelper
                .getCountFinalSqlQuery(
                    variableQuery,
                    conditionString,
                    orderString,
                    distinctString
            )

            let variableCount = await sequelize.query(variableCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = await variableCount[0].count
        }

        res.send(200, {
            rows: variables,
            count: count
        })
        return
    }
}