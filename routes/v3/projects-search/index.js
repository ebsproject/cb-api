/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let userValidator = require('../../../helpers/person/validator.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let aclStudy = require('../../../helpers/acl/study.js')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let projectHelper = require('../../../helpers/project/index.js')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Implementation of advanced search functionality for projects
    post: async function (req, res, next) {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let sort = req.query.sort

        let count = 0

        let orderString = ''

        let conditionString = ''

        let addedConditionString = ''

        let parameters = {}

        if (req.body != undefined) {

            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields']

            // Get filter condition
            conditionString = await processQueryHelper.getFilterString(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Start building the query

        let projectQuery = null

        // Check if client specifies field
        if (parameters['fields'] != null) {

            projectQuery = knex.column(parameters['fields'].split('|'))

            projectQuery += `
                FROM 
                    master.project project
                LEFT JOIN 
                    master.user creator ON project.creator_id = creator.id
                LEFT JOIN 
                    master.user modifier ON project.modifier_id = modifier.id
                LEFT JOIN 
                    master.user leader ON project.leader_id = leader.id
                LEFT JOIN
                    master.program program ON project.program_id = program.id
                WHERE 
                    project.is_void = FALSE 
                ` + addedConditionString +
                `ORDER BY 
                    project.id`

        } else {
            projectQuery = await projectHelper.getProjectQuerySql(addedConditionString)
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        projectFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            projectQuery,
            conditionString,
            orderString
        )

        // Retrieve projects from the database   
        let projects = await sequelize.query(projectFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (projects === undefined || projects.length < 1) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            // Get count 
            projectCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(projectQuery, conditionString, orderString)

            projectCount = await sequelize.query(projectCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = projectCount[0].count
        }

        res.send(200, {
            rows: projects,
            count: count
        })
        return
    }
}