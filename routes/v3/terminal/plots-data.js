/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Endpoint for adding a plot data record
    post: async function (req, res, next) {

        // Default
        let resultArray = []
        let transactionDbId = null
        let recordCount = 0

        // Retrieve the user ID of the client from the access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let transactionUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/transactions"

        // Check for parameters
        if (req.body != null) {

            // Parse the input
            let data = req.body
            let records = []
            try {
                records = data.records

                recordCount = records.length

                if (records == undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            } catch (e) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            // Start transaction
            let transaction
            try {
                // Get transaction
                transaction = await sequelize.transaction({ autocommit: false })

                // Create transaction in operational data terminal
                let transactionQuery = `
                    INSERT INTO operational_data_terminal.transaction2
                    (
                        status, start_action_timestamp, creation_timestamp, creator_id, study_name, dataset, dataset_name, remarks, is_void
                    )
                    SELECT
                        'uploaded', NOW(), NOW(), (:userDbId), study.name, null,'study', 'Created via CB API', FALSE
                    FROM 
                        operational.plot plot,
                        operational.study study
                    WHERE plot.study_id = study.id
                        AND plot.id = (:plotDbId)
                    RETURNING 
                        id
                `

                await sequelize.query(transactionQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    replacements: {
                        userDbId: userDbId,
                        plotDbId: records[0].plotDbId // Get the plotDbId of the input
                    },
                    transaction: transaction
                })
                    .then(function (transactionArray) {
                        transactionDbId = transactionArray[0][0]["id"]
                    })

                let visitedArray = []
                // Create record in terminal
                for (var item in records) {
                    if (!records[item].variableDbId || !records[item].variableValue || !records[item].plotDbId) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400073)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if the plot data info is already visited
                    let plotDataInfo = records[item].plotDbId + '-' + records[item].variableDbId

                    if (visitedArray.includes(plotDataInfo)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400074)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else {
                        visitedArray.push(plotDataInfo)
                    }

                    // Get variable info

                    let plotDataQuery = `
                        INSERT INTO
                            operational_data_terminal.terminal (
                                transaction_id, 
                                variable_id, 
                                value, 
                                data_unit, 
                                creator_id, 
                                creation_timestamp, 
                                identifier,
                                identifier_key, 
                                key, 
                                collector_id, 
                                collection_timestamp
                            )
                        SELECT
                            :transactionDbId, 
                            :variableDbId, 
                            :variableValue,
                            (
                                SELECT
                                    CASE WHEN type = 'metadata' THEN 'plot_metadata'
                                    ELSE 'plot_data'
                                    END AS data_unit
                                FROM master.variable 
                                WHERE id = :variableDbId
                            ),
                            :userDbId,
                            NOW(),
                            ('{"PLOTNO":' || plotno || ',"REP":' || rep || '}') ::json,
                            plotno || '-' || rep,
                            key,
                            :userDbId,
                            NOW()
                        FROM
                            operational.plot
                        WHERE
                            id = :plotDbId
                    `

                    await sequelize.query(plotDataQuery, {
                        type: sequelize.QueryTypes.INSERT,
                        replacements: {
                            transactionDbId: transactionDbId,
                            variableDbId: records[item].variableDbId,
                            variableValue: records[item].variableValue,
                            userDbId: userDbId,
                            plotDbId: records[item].plotDbId
                        },
                        transaction: transaction
                    })
                }

                // Update the transaction info
                let updateTransactionQuery = `
                    WITH t AS (
                        SELECT 
                            terminal.transaction_id, 
                            STRING_AGG(
                                (variable.abbrev),',' 
                                ORDER BY variable.abbrev 
                            ) AS "variableStringAgg"
                        FROM 
                            operational_data_terminal.terminal terminal,
                            master.variable variable, 
                            operational_data_terminal.transaction2 transaction
                        WHERE 
                            transaction.id = :transactionDbId
                            AND transaction.id = terminal.transaction_id
                            AND terminal.variable_id = variable.id
                        GROUP BY 
                            terminal.transaction_id
                    )
                    UPDATE 
                        operational_data_terminal.transaction2 transaction
                    SET 
                        record_count = :recordCount,
                        dataset = ('{"study_metadata":"null","entry_metadata":"null","plot_metadata":"' || t."variableStringAgg" || '"} ')::json
                    FROM 
                        t
                    WHERE 
                        t.transaction_id = transaction.id  
                `

                await sequelize.query(updateTransactionQuery, {
                    replacements: {
                        transactionDbId: transactionDbId,
                        recordCount: recordCount
                    },
                    transaction: transaction
                })

                // Commit the transaction
                await transaction.commit()

                // Return the transaction info
                let array = {
                    transactionDbId: transactionDbId,
                    recordCount: recordCount,
                    href: transactionUrlString + '/' + transactionDbId + '/data'
                }
                resultArray.push(array)

                res.send(200, {
                    rows: resultArray
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            }

        } else {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
    }
}