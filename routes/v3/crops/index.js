/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, Phase } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let userValidator = require('../../../helpers/person/validator.js')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../helpers/logger')
const endpoint = 'crops'

module.exports = {
    // Endpoint for retrieving all crops
    // GET /v3/crops

    get: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let conditionString = ''
        let orderString = ''

        // Build base query for crops
        let cropsQuery = `
            SELECT
                crop.id AS "cropDbId",
                crop.crop_code AS "cropCode",
                crop.crop_name AS "cropName",
                crop.description,
                crop.notes,
                crop.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                crop.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                tenant.crop crop
            LEFT JOIN
                tenant.person creator ON crop.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON crop.modifier_id = modifier.id
            WHERE
                crop.is_void = FALSE
            ORDER BY
                crop.id
        `

        // Get filter condition/s for abbrev
        if (params.abbrev !== undefined) {
            let parameters = {
                abbrev: params.abbrev
            }

            conditionString = await processQueryHelper.getFilterString(
                parameters
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Get filter condition/s for sort
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the final query
        cropFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            cropsQuery,
            conditionString,
            orderString
        )

        // Retrieve crop records from database
        let crops = await sequelize
            .query(cropFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await crops === undefined || await crops.length < 1) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            // Get crop count
            cropCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                cropsQuery,
                conditionString,
                orderString
            )

            cropCount = await sequelize
                .query(cropCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = cropCount[0].count
        }

        res.send(200, {
            rows: crops,
            count: count
        })
        return
    },

    // Endpoint for creating a new crop record
    // POST /v3/crops
    post: async function (req, res, next) {
        let resultArray = []
        let cropDbId = null
        
        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)
        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        //check if user is Admin
        let isAdmin = await userValidator.isAdmin(personDbId)
        if (!isAdmin) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401028)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let cropsUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/crops'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        records = data.records
        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let cropValuesArray = []
            for (const record of records) {
                // Set defaults
                let cropCode = null
                let cropName = null
                let description = null

                // Check if required columns are in the request body
                if (
                    !record.cropCode || !record.cropName 
                ) {
                    let errMsg = `Required parameters are missing. Ensure that cropCode and cropName fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                cropCode = record.cropCode
                cropName = record.cropName

                description = (record.description !== undefined) ? record.description : null

                // get values
                let tempArray = [
                    cropCode, cropName, description, personDbId
                ]
                cropValuesArray.push(tempArray)
            }

            // Create crop record
            let cropsQuery = format(`
                INSERT INTO
                    tenant.crop (
                        crop_code, crop_name, description, creator_id
                    )
                VALUES
                    %L
                RETURNING id`, cropValuesArray
            )

            const crops = await sequelize.transaction(async transaction => {
                return await sequelize.query(cropsQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (const crop of crops[0]) {
                cropDbId = crop.id
                let cropRecord = {
                    cropDbId: cropDbId,
                    recordCount: 1,
                    href: cropsUrlString + '/' + cropDbId
                }
                resultArray.push(cropRecord)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
