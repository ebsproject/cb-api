/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

  // Endpoint for retrieving crop information given a specific crop id
  // GET /v3/crops/:id

  get: async function (req, res, next) {
    // Retrieve the crop id
    let cropDbId = req.params.id

    if (!validator.isInt(cropDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400182)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let cropsQuery = `
      SELECT
        crop.id AS "cropDbId",
        crop.crop_code AS "cropCode",
        crop.crop_name AS "cropName",
        crop.description,
        crop.notes,
        crop.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS creator,
        crop.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS modifier
      FROM
        tenant.crop crop
      LEFT JOIN
        tenant.person creator ON crop.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON crop.modifier_id = modifier.id
      WHERE
        crop.id = ${cropDbId}
        AND crop.is_void = FALSE
    `
    // Retrieve crops from the database
    let crops = await sequelize
      .query(cropsQuery, { 
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (await crops === undefined || await crops.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404001)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: crops
    })
    return
  }
}
