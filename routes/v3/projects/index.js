/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let projectHelper = require('../../../helpers/project/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let userValidator = require('../../../helpers/person/validator.js')
let validator = require('validator')
let format = require('pg-format')

module.exports = {

    // Endpoint for retrieving the list of projects
    // GET /v3/projects
    get: async function (req, res, next) {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''

        let orderString = ''

    // Build query
    let projectsQuery = `
      SELECT
        project.id AS "projectDbId",
        project.project_name AS "project",
        program.id AS "programDbId",
        program.program_name AS "programName",
        project.project_code AS "projectCode",
        project.project_status AS "projectStatus",
        project.description,
        project.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS creator,
        project.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS modifier,
        leader.id AS "leaderDbId",
        leader.person_name AS leader
      FROM 
        tenant.project project
      LEFT JOIN 
       tenant.person creator ON project.creator_id = creator.id
      LEFT JOIN 
        tenant.person modifier ON project.modifier_id = modifier.id
      LEFT JOIN 
        tenant.person leader ON project.leader_id = leader.id
      LEFT JOIN
        tenant.program program ON project.program_id = program.id
      WHERE 
        project.is_void = FALSE 
      ORDER BY
        project.id
    `

        // Get filter condition for abbrev only
        if (params.abbrev !== undefined) {
            let parameters = {
                abbrev: params.abbrev
            }

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

    // Generate the final sql query 
    projectFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      projectsQuery,
      conditionString,
      orderString
    )
    
    // Retrieve projects from the database   
    let projects = await sequelize.query(projectFinalSqlQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        limit: limit,
        offset: offset
      }
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })
    
    if (await projects === undefined || await projects.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get count 
      projectCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(projectsQuery, conditionString, orderString)
      
      projectCount = await sequelize.query(projectCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

            count = projectCount[0].count
        }

        res.send(200, {
            rows: projects,
            count: count
        })
        return
    },

    /**
     * Create project records
     * POST /v3/projects
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res, next) {
        let projectCode
        let projectName
        let projectStatus
        let description
        let programDbId
        let pipelineDbId
        let leaderDbId
        let notes
        let supportedProjectStatus = ['active', 'inactive', 'discarded', 'draft']
        let resultArray = []

        // Get user ID
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the user is an admin
        let isAdmin = await userValidator.isAdmin(userDbId)

        // If not an admin, return an error
        if (!isAdmin) {
            let errMsg = "Unauthorized request. You do not have the permission to create new project records."
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let projectsUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/projects'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let data = req.body
        let records = [] 

        try {
            records = data.records
            // Check if the input data is empty
            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } 
        catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            let projectsArray = []
            for (var record of records) {
                // Get record values
                programDbId = record.programDbId
                projectCode = record.projectCode
                projectName = record.projectName
                projectStatus = record.projectStatus
                description = record.description
                pipelineDbId = record.pipelineDbId
                leaderDbId = record.leaderDbId
                notes = record.notes

                // Check required fields
                if (programDbId == null || projectCode == null
                    || projectName == null || projectStatus == null
                    || pipelineDbId == null || leaderDbId == null) {
                    let errMsg = "Required parameters are missing. Ensure that the projectCode, projectName, projectStatus, programDbId, pipelineDbId, and leaderDbId are specified."
                    res.send(new errors.BadRequestError(errMsg))
                    return

                }

                // Check projectCode format
                // Should be alphanumeric. Only allowed symbol is underscore.
                if (!/^[A-Z0-9_]+$/.test(projectCode)) {
                    let errMsg = "Invalid projectCode format. Must be all uppercase alphanumeric characters and underscores only eg. 'PROJECT_ABC_123'."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if projectCode does not exist yet
                let projectCodeQuery = `
                    SELECT count(1)
                    FROM tenant.project
                    WHERE LOWER(project_code) = '${projectCode.toLowerCase()}'
                `

                let projectCodes = await sequelize.query(projectCodeQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    
                    let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (projectCodes == null) {
                    return
                }

                if (projectCodes[0] != null && projectCodes[0].count != null && projectCodes[0].count > 0) {
                    let errMsg = `The projectCode '${projectCode}' already exists.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if projectName does not exist yet
                let projectNameQuery = `
                    SELECT count(1)
                    FROM tenant.project
                    WHERE LOWER(project_name) = '${projectName.toLowerCase()}'
                `

                let projectNames = await sequelize.query(projectNameQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (projectNames == null) {
                    return
                }

                if (projectNames[0] != null && projectNames[0].count != null && projectNames[0].count > 0) {
                    let errMsg = `The projectName '${projectName}' already exists.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the project status is valid
                if (supportedProjectStatus.indexOf(projectStatus) === -1) {
                    let errMsg = "Invalid projectStatus value. Should be: " + supportedProjectStatus.join(', ')
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the programDbId is an integer
                if (!validator.isInt(programDbId)) {
                    let errMsg = "Invalid programDbId value. Should be an integer."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the programDbId exists
                let programQuery = `
                    SELECT id
                    FROM tenant.program
                    WHERE id = :programDbId
                `

                let programs = await sequelize.query(programQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        programDbId: programDbId
                    }
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                    res.send(new errors.InternalError(errMsg))
                    return   
                })

                if (programs == null) {
                    return
                }

                if (programs.length == 0) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400054)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the pipelineDbId is an integer
                if (!validator.isInt(pipelineDbId)) {
                    let errMsg = "Invalid pipelineDbId value. Should be an integer."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the pipelineDbId exists
                let pipelineQuery = `
                    SELECT id
                    FROM tenant.pipeline
                    WHERE id = :pipelineDbId
                `

                let pipelines = await sequelize.query(pipelineQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        pipelineDbId: pipelineDbId
                    }
                })

                if (pipelines == null) {
                    return
                }

                if (pipelines.length == 0) {
                    let errMsg = 'You have provided a pipelineDbId that does not exist.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the leaderDbId is an integer
                if (!validator.isInt(leaderDbId)) {
                    let errMsg = "Invalid leaderDbId value. Should be an integer."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the pipelineDbId exists
                let leaderQuery = `
                    SELECT id
                    FROM tenant.person
                    WHERE id = :leaderDbId
                `

                let persons = await sequelize.query(leaderQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        leaderDbId: leaderDbId
                    }
                })

                if (persons == null) {
                    return
                }

                if (persons.length == 0) {
                    let errMsg = 'You have provided a leaderDbId (personDbId) that does not exist.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Add data to insertion array
                let tempArray = [
                    projectCode,
                    projectName,
                    projectStatus,
                    description,
                    programDbId,
                    pipelineDbId,
                    leaderDbId,
                    notes,
                    userDbId
                ]

                projectsArray.push(tempArray)
            }

            // Insert new record
            let projectsInsertQuery = format(`
                INSERT INTO tenant.project 
                    (project_code, project_name, project_status, description, program_id, pipeline_id, leader_id, notes, creator_id)
                VALUES 
                    %L 
                RETURNING 
                    id`, projectsArray
            );

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let projects = await sequelize.query(projectsInsertQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            }).catch(async err => {
                if (transaction != null && transaction.finished !== 'commit') transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            })

            if (projects == null) {
                return
            }

            // Commit the transaction
            await transaction.commit()

            let projectsCreated = projects[0] != null ? projects[0] : []
            
            // Add id to resultsArray
            for (let project of projectsCreated) {
                let currentProjectDbId = project.id
                resultArray.push({
                    projectDbId: currentProjectDbId,
                    recordCount: 1,
                    href: projectsUrlString + '/' + currentProjectDbId
                });
            }

            res.send(200, {
                rows: resultArray
            })
            return
            
        } catch (e) {
            // Rollback transaction (if still open)
            if (transaction != null && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return   
        }
    }
}