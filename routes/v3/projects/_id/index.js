/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let projectHelper = require('../../../../helpers/project/index.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the list of projects
    // GET /v3/projects/{id}
    get: async function (req, res, next) {

        let projectDbId = req.params.id
        let count = 0

        if (!validator.isInt(projectDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400056)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let projectsQuery = `
            SELECT
                project.id AS "projectDbId",
                project.name AS "project",
                program.id AS "programDbId",
                program.name AS "program",
                project.abbrev,
                project.description,
                project.remarks,
                project.creation_timestamp AS "creationTimestamp",
                creator.display_name AS creator,
                creator.id AS "creatorDbId",
                project.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.display_name AS modifier 
            FROM 
                master.project project
            LEFT JOIN 
                master.user creator ON project.creator_id = creator.id
            LEFT JOIN 
                master.user modifier ON project.modifier_id = modifier.id
            LEFT JOIN
                master.program program ON project.program_id = program.id
            WHERE 
                project.id = (:projectDbId) AND
                project.is_void = FALSE 
        `

        // Retrieve project from the database   
        let projects = await sequelize.query(projectsQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                projectDbId: projectDbId
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (await projects === undefined || await projects.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404017)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        res.send(200, {
            rows: projects
        })
        return
    },

    // Updates a project record given its project ID
    put: async function (req, res, next) {

        // Default
        let resultArray = []
        let programDbId = null
        let name = ''
        let abbrev = ''
        let description = ''
        let status = ''
        let leaderDbId = null
        let projectRootDbId = null

        // Retrieve user ID of the client from the access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrive project ID
        let projectDbId = req.params.id

        // Checks if project ID is valid
        if (!validator.isInt(projectDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400056)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else {
            // Get project record
            let projectQuery =
                `
                SELECT 
                    *
                FROM
                    master.project project
                WHERE
                    project.id = :projectDbId
                    AND project.is_void = false
            `

            let projects = await sequelize.query(projectQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    projectDbId: projectDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await projects == undefined || await projects.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404017)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Check if the connection is secure or not (http or https)
            let isSecure = forwarded(req, req.headers).secure

            // Set URL for response
            let projectUrlString = (isSecure ? 'https' : 'http')
                + "://"
                + req.headers.host
                + "/v3/projects/"

            let baseProjectQuery = `SELECT * FROM master.project `
            let setQuery = ``
            let replacements = {}
            // Check parameters
            if (req.body != null) {

                let data = req.body
                let records = []
                try {
                    records = data.records

                    if (records == undefined) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                } catch (err) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                }

                let transaction

                try {
                    baseProjectQuery += `WHERE `

                    // Get information from request body
                    for (item in records) {

                        // Checks if name, abbrev, or description has single quotes in it, if yes, adds another single quote into the string
                        if (records[item].description != undefined) {
                            description = records[item].description
                            description = description.replace(/'/g, "\'")
                            replacements['description'] = description

                            if (setQuery.length == 0) setQuery += `description = :description`
                            else setQuery += `, description = :description`
                        }

                        if (records[item].name != undefined) {
                            name = records[item].name
                            name = name.replace(/'/g, "\'")

                            baseProjectQuery += `name = :name `
                            replacements['name'] = name

                            if (setQuery.length == 0) setQuery += `name = :name`
                            else setQuery += `, name = :name`
                        }

                        if (records[item].abbrev != undefined) {
                            abbrev = records[item].abbrev
                            abbrev = abbrev.replace(/'/g, "\'")

                            if (name != undefined) baseProjectQuery += `OR `

                            baseProjectQuery += `abbrev = :abbrev`
                            replacements['abbrev'] = abbrev

                            if (setQuery.length == 0) setQuery += `abbrev = :abbrev`
                            else setQuery += `, abbrev = :abbrev`
                        }

                        if (records[item].name != undefined && records[item].abbrev != undefined) {

                            let projectsQuery = baseProjectQuery

                            let projects = await sequelize.query(projectsQuery, {
                                type: sequelize.QueryTypes.SELECT,
                                replacements: {
                                    name: name,
                                    abbrev: abbrev
                                }
                            })

                            // Checks if name or abbrev has duplicates
                            if (projects.length > 0) {
                                let nameDb = projects[0]['name']
                                let abbrevDb = projects[0]['abbrev']

                                if (name.toLowerCase() == nameDb.toLowerCase() || abbrev.toLowerCase() == abbrevDb.toLowerCase()) {
                                    let errMsg = await errorBuilder.getError(req.headers.host, 400052)
                                    res.send(new errors.BadRequestError(errMsg))
                                    return
                                }
                            }
                        }

                        if (records[item].programDbId != undefined) {

                            programDbId = records[item].programDbId

                            let programQuery =
                                `
                                SELECT * 
                                FROM master.project
                                WHERE program_id = :programDbId
                            `

                            let programs = await sequelize.query(programQuery, {
                                type: sequelize.QueryTypes.SELECT,
                                replacements: {
                                    programDbId: programDbId
                                }
                            })

                            // Checks if programDbId exists if not, 400
                            if (programs.length == 0) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400054)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            replacements['programDbId'] = programDbId

                            if (setQuery.length == 0) setQuery += `program_id = :programDbId`
                            else setQuery += `, program_id = :programDbId`
                        }

                        if (records[item].leaderDbId != undefined) {

                            leaderDbId = records[item].leaderDbId

                            let userQuery =
                                `
                                SELECT * 
                                FROM master.user
                                WHERE id = :leaderDbId
                            `

                            let users = await sequelize.query(userQuery, {
                                type: sequelize.QueryTypes.SELECT,
                                replacements: {
                                    leaderDbId: leaderDbId
                                }
                            })

                            // Checks if leaderDbId exists if not, 400  
                            if (users.length == 0) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400055)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            replacements['leaderDbId'] = leaderDbId

                            if (setQuery.length == 0) setQuery += `leader_id = :leaderDbId`
                            else setQuery += `, leader_id = :leaderDbId`
                        }

                        if (records[item].status !== undefined) {
                            status = records[item].status
                            // Checks if format for status is valid if not, 400
                            if (status != 'active' && status != 'inactive') {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400053)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            replacements['status'] = status

                            if (setQuery.length == 0) setQuery += `status = :status`
                            else setQuery += `, status = :status`
                        }

                        // Adds modification_timestamp and modifier_id into setQuery
                        if (setQuery.length == 0) setQuery += `modification_timestamp = NOW(), modifier_id = :userDbId,`
                        else setQuery += `, modification_timestamp = NOW(), modifier_id = :userDbId,`
                        replacements['userDbId'] = userDbId

                        // Checks notes if already has 'Updated by CB-API' to avoid duplicate notes
                        let noteQuery =
                            `
                            SELECT notes
                            FROM master.project
                            WHERE id = :projectDbId
                        `

                        let notes = await sequelize.query(noteQuery, {
                            type: sequelize.QueryTypes.SELECT,
                            replacements: {
                                projectDbId: projectDbId
                            }
                        })

                        let updateNoteString = 'Updated by CB-API'
                        let noteDbString = notes[0]['notes']

                        // if notes is null
                        if (noteDbString == null) {
                            setQuery += ` notes = 'Updated by CB-API' `
                        } else {
                            // Update the project records
                            if (noteDbString.includes(updateNoteString)) {
                                setQuery +=
                                    ` notes = notes `
                            } else {
                                setQuery +=
                                    ` notes = notes || '; Updated by CB-API' `
                            }
                        }

                        // Add projectDbId to replacements
                        replacements['projectDbId'] = projectDbId
                        let projectUpdateQuery =
                            `
                            UPDATE master.project
                            SET ` + setQuery + `
                            WHERE
                                id = :projectDbId
                                AND is_void = false
                        `

                        // Get transaction
                        transaction = await sequelize.transaction({ autocommit: false })

                        await sequelize.query(projectUpdateQuery, {
                            type: sequelize.QueryTypes.UPDATE,
                            replacements: replacements,
                            transaction: transaction
                        })
                            .catch(async err => {
                                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                                res.send(new errors.InternalError(errMsg))
                                return
                            })

                        // Return project
                        let array = {
                            projectDbId: projectDbId,
                            recordCount: 1,
                            href: projectUrlString + projectDbId
                        }

                        resultArray.push(array)

                    }
                    // Commit transaction
                    await transaction.commit()

                    res.send(200, {
                        rows: resultArray
                    })
                    return
                } catch (err) {
                    if (err) await transaction.rollback()
                    let errMsg = await errorBuilder.getError(req.headers.host, 500003)
                    res.send(new errors.InternalError(errMsg))
                    return
                }
            } else {
                let errMsg = await errorBuilder.getError(req.headers.host, 400009)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
    }
}
