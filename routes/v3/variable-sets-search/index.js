/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let userValidator = require('../../../helpers/person/validator.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')

/**
 * Get the teams of the user 
 * @param userId integer
 * 
 * @return team IDs string
 */
async function getTeamIdString(userId) {

    let sql = `
        SELECT
            STRING_AGG(DISTINCT team.id::text, ',') as "teamIdString"
        FROM
            master.team team,
            master.team_member "teamMember"
        WHERE
            "teamMember".member_id = (:userId) AND
            "teamMember".team_id = team.id AND
            "teamMember".is_void = false AND
            team.is_void = false
    `

    // Retrieve teams from the database   
    let teams = await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
                userId: userId
        }
    })

    return (await teams == undefined || await teams.length < 1) ? '' : teams[0]['teamIdString']
}

module.exports = {

    // Implementation of advanced search functionality for variable sets
    post: async function (req, res, next) {
        // Set defaults 
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let columnString = ''
        let parameters = {}
        let isCheckPermission = true;
        let permissionColumnString = '';
        parameters['fields'] = null
        parameters['isOwned'] = false
        parameters['isSharedToMe'] = false
        parameters['usage'] = null

        if (req.body != null) {
            // Retrieve parameters
            if (req.body.fields != null) {
                    parameters['fields'] = req.body.fields
            }

            // Check if the output will be owned only
            if (req.body.isOwned != null) {
                if (req.body.isOwned.toUpperCase() == 'TRUE') {
                        parameters['isOwned'] = true
                }
            }

            // Check if the output will be draft only
            if (req.body.isSharedToMe != null) {
                if (req.body.isSharedToMe.toUpperCase() == 'TRUE') {
                        parameters['isSharedToMe'] = true
                }
            }

            if (parameters['isOwned'] && parameters['isSharedToMe']) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400080)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            // Retrieve usage parameter
            if (req.body.usage != null) {
                parameters['usage'] = req.body.usage

                if(parameters['usage'].toLowerCase() == 'application'){
                    isCheckPermission = false;
                }
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', 'isOwned', 'isSharedToMe','usage']

            // Get filter condition
            conditionString = await processQueryHelper.getFilter(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        //Sets to false when a parameter usage = application is specified
        if(isCheckPermission){
            // Set the default column string for getting the permission
            permissionColumnString = `
                CASE WHEN
                    ("variableSetAccess".collaborator_id = ` + userId + ` AND
                    lower("variableSet".access_type) != 'public')
                THEN
                    "variableSetAccess".permission
                ELSE
                    'read-write'
                END AS
                    permission,
            `

            // Check if user is an administrator. If yes, don't check the permission of the variable sets.
            if (!await userValidator.isAdmin(userId)) {
                // Add restriction for non-admin for the shared variable sets
                if (parameters['isSharedToMe']) {
                    let teamIdString = await getTeamIdString(userId)
                    addedConditionString += `
                        AND (
                            (
                                lower("variableSet".access_type) = 'public' AND
                                "variableSet".creator_id <> ` + userId + `
                            )
                            OR
                            (
                                (
                                    (
                                        lower("variableSetAccess".collaborator_type) = 'group' AND "variableSetAccess".collaborator_id IN (`+ teamIdString + `)
                                    )
                                    OR
                                    (
                                        lower("variableSetAccess".collaborator_type) = 'user' AND "variableSet".creator_id = ` + userId + `
                                    )may 
                                )
                            AND
                                    lower("variableSet".access_type) <>'public'
                            )
                        )
                    `

                    permissionColumnString = `
                            CASE WHEN
                                ("variableSetAccess".permission IS NOT NULL)
                            THEN
                                "variableSetAccess".permission
                            ELSE
                                'read-write'
                            END AS
                                permission,
                    `
                } else {
                    addedConditionString += `
                        AND (
                            (
                                "variableSet".creator_id = ` + userId + ` OR
                                lower("variableSet".access_type) = 'public'
                            )
                            OR
                            (
                                "variableSetAccess".collaborator_id = ` + userId + ` AND
                                lower("variableSet".access_type) != 'public'
                            )
                        )
                    `
                }
            }
        
            // Show own variables sets only
            if (parameters['isOwned']) {
                addedConditionString += 
                    `AND "variableSet".creator_id = ` + userId
            }

            // Show shared variable sets only
            if (parameters['isSharedToMe']) {
                addedConditionString +=
                    `AND "variableSet".creator_id <> ` + userId
            }
        }
        // Start building the query
        let variableSetQuery = null

        if (parameters['fields'] != null) {
            // Parse the fields
            let fields = parameters['fields'].split('|').map(item => item.trim())
            for (field of fields) {
                columnArr = field.split('.')
                if (columnString != '') columnString += ','
                columnString += '"' + columnArr[0] + '".' + columnArr[1]
            }
            variableSetQuery =
                knex.select(knex.raw('DISTINCT ON ("variableSet".id) ' + columnString))
        } else {
            columnString = `
                DISTINCT ON
                    ("variableSet".id)
                    "variableSet".id AS "variableSetDbId",
                    "variableSet".abbrev,
                    "variableSet".name,
                    "variableSet".description,
                    "variableSet".display_name AS "displayName",
                    "variableSet".remarks,
                    "variableSet".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    "variableSet".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier,
                    "variableSet".ontology_id AS "ontologyId",
                    "variableSet".varsetusage,
                    "variableSet".bibref,
                    "variableSet".parvarsetid,
                    "variableSet".usage,
                    "variableSet".access_type AS "accessType",
                ` + permissionColumnString + `
                    (
                        SELECT
                            count("variableSetMember".id) 
                        FROM
                            master.variable_set_member "variableSetMember" 
                        WHERE
                            "variableSetMember".variable_set_id = "variableSet".id 
                        AND
                            "variableSetMember".is_void = false
                )
                AS
                    "memberCount"
            `

            variableSetQuery = knex.select(knex.raw(columnString))
        }

        variableSetQuery = variableSetQuery + `
            FROM 
                master.variable_set "variableSet"
            LEFT JOIN 
                tenant.person creator ON "variableSet".creator_id = creator.id
            LEFT JOIN 
                tenant.person modifier ON "variableSet".modifier_id = modifier.id
            LEFT JOIN
                master.variable_set_access "variableSetAccess" ON "variableSet".id = "variableSetAccess".variable_set_id
            WHERE 
                "variableSet".is_void = FALSE
            ` + addedConditionString + `
            ORDER BY
                "variableSet".id
        `
    
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        variableSetFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            variableSetQuery,
            conditionString,
            orderString
        )
        
        // Retrieve variableSets from the database   
        let variableSets = await sequelize.query(variableSetFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await variableSets == undefined || await variableSets.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })

            return
        } else {
            // Get count 
            variableSetCountFinalSqlQuery = await processQueryHelper
                .getCountFinalSqlQuery(
                    variableSetQuery,
                    conditionString,
                    orderString
                )

            variableSetCount = await sequelize
                .query(variableSetCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = variableSetCount[0].count
        }

        res.send(200, {
            rows: variableSets,
            count: count
        })
        return
    }
}