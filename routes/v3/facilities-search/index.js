/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn',
        'includeInactive',
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray
        )
      
      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        
        parameters['distinctOn'] = `"${parameters['distinctOn']}",`
      }
    }
    // Build the base retrieval query
    let facilitiesQuery = null
    // Check if the client specified values for the field/s
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        facilitiesQuery = knex.select(selectString)
      } else {
        facilitiesQuery = knex.column(parameters['fields'].split('|'))
      }

      facilitiesQuery += `
        FROM
          place.facility facility
        LEFT JOIN
          place.facility root ON facility.root_facility_id = root.id
        LEFT JOIN
          tenant.person creator ON facility.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON facility.modifier_id = modifier.id
        WHERE
          facility.is_void = FALSE
        ORDER BY
          ${parameters['distinctOn']}
          facility.id
      `
    } else {
      facilitiesQuery = `
        SELECT
          ${addedDistinctString}
          facility.id AS "facilityDbId",
          facility.facility_code AS "facilityCode",
          facility.facility_name AS "facilityName",
          facility.facility_class AS "facilityClass",
          facility.facility_type AS "facilityType",
          facility.geospatial_coordinates AS "geospatialCoordinates",
          facility.description,
          facility.parent_facility_id AS "parentFacilityDbId",
          root.id AS "rootFacilityDbId",
          root.facility_code AS "rootFacilityCode",
          root.facility_name AS "rootFacilityName",
          root.facility_class AS "rootFacilityClass",
          root.facility_type AS "rootFacilityType",
          facility.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          facility.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          place.facility facility
        LEFT JOIN
          place.facility root ON facility.root_facility_id = root.id
        LEFT JOIN
          tenant.person creator ON facility.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON facility.modifier_id = modifier.id
        WHERE
          facility.is_void = FALSE
        ORDER BY
          ${parameters['distinctOn']}
          facility.id
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    let facilitiesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      facilitiesQuery,
      conditionString,
      orderString,
    )

    // Retrieve facilities from the database
    let facilities = await sequelize
      .query(facilitiesFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await facilities == undefined || await facilities.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let facilitiesCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        facilitiesQuery,
        conditionString,
        orderString,
      )

    let facilitiesCount = await sequelize
      .query(
        facilitiesCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT,
        }
      )
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    count = facilitiesCount[0].count

    res.send(200, {
      rows: facilities,
      count: count
    })
    return
  }
}