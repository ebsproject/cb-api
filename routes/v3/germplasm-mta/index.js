/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../config/sequelize")

let forwarded = require("forwarded-for")
let format = require("pg-format")
let validator = require("validator")

let tokenHelper = require("../../../helpers/auth/token")
let variableHelper = require("../../../helpers/variable")
let seedlotHelper = require("../../../helpers/seedlot")
let germplasmHelper = require("../../../helpers/germplasm")

let errorBuilder = require("../../../helpers/error-builder")
let logger = require('../../../helpers/logger/index')
let errors = require("restify-errors")

let endpoint = 'germplasm-mta'

module.exports = {
    // Endpoint for creating new germplasm MTA_STATUS records
    post: async function (req, res, next) {
        // Set defaults
        let resultsArr = [];

        // Check person ID
        let personDbId = await tokenHelper.getUserId(req);
        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure;

        // Set URL for response
        let germplasmMtaUrlString = (isSecure ? "https" : "http") +
            "://" + req.headers.host + "/v3/germplasm-mta";

        // Check if request body is empty or does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let data = req.body
            let records = []

            records = data.records

            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            let validateQuery = '';
            let validateCount = 0;

            // Declare variables
            let germplasmDbId = null; //required
            let seedDbId = null;
            let germplasmMtaStatus = null; //required
            let germplasmUsage = null;
            let germplasmMlsAncestors = null;
            let germplasmGeneticStock = null;
            let remarks = null;

            let germplasmMtaStatusValuesArr = [];

            // check and validate each record
            // build insert query
            for (let record of records) {
                // Check for required parameters
                if (record.germplasmDbId == undefined ||
                    record.mtaStatus == undefined) {
                    let errMsg = 'Required parameters are missing. Ensure that the germplasm ID and MTA Status fields are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let germplasmRecord = await germplasmHelper.hasGermplasmRecord(req, res, record.germplasmDbId);

                if (!germplasmRecord) {
                    let errMsg = 'Requested germplasm record does not exist.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }


                germplasmDbId = record.germplasmDbId
                germplasmMtaStatus = record.mtaStatus

                // check if provided MTA_STATUS is in stored scale values
                let scaleValues = await variableHelper.isInScaleValues('MTA_STATUS', germplasmMtaStatus);

                if (scaleValues[0].count == '0') {
                    let errMsg = 'Invalid value for MTA_STATUS.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // validate seed ID, if provided
                if (record.seedDbId != undefined && !validator.isInt(record.seedDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400248)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // validate seed ID, check if matches existing seed record
                if (record.seedDbId != undefined && validator.isInt(record.seedDbId)) {
                    let hasSeed = await seedlotHelper.hasSeedRecord(req, res, record.seedDbId)

                    if (record.seedDbId != undefined &&
                        validator.isInt(record.seedDbId) &&
                        hasSeed) {
                        seedDbId = record.seedDbId
                    }
                }

                // validate if germplasm ID matches the germplasm_id of the seed record provided
                if (germplasmDbId && seedDbId) {
                    let additionalConditions = `
                        AND seed.id = ${seedDbId}
                        AND seed.germplasm_id = ${germplasmDbId}
                    `

                    let seedRecord = await seedlotHelper.getSeedRecord(req, res, additionalConditions);

                    if (seedRecord.length < 1) {
                        let errMsg = 'Seed sources do not match the germplasm ID provided.';
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                germplasmUsage = record.use ?? null;
                germplasmMlsAncestors = record.mlsAncenstors ?? null;
                germplasmGeneticStock = record.geneticStock ?? null;

                let insertArrRecord = [
                    germplasmDbId,
                    seedDbId,
                    germplasmMtaStatus,
                    germplasmUsage,
                    germplasmMlsAncestors,
                    germplasmGeneticStock,
                    remarks,
                    personDbId
                ];

                germplasmMtaStatusValuesArr.push(insertArrRecord);
            }

            // insert record to database
            let germplasmMtaStatusQuery = format(`
                INSERT INTO germplasm.germplasm_mta (
                    germplasm_id,
                    seed_id,
                    mta_status,
                    use,
                    mls_ancestors,
                    genetic_stock,
                    remarks,
                    creator_id
                )
                VALUES
                    %L
                RETURNING id
            `, germplasmMtaStatusValuesArr);

            let mtaRecords = await sequelize.transaction(async transaction => {
                return await sequelize.query(germplasmMtaStatusQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {

                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            });

            for (let record of mtaRecords[0]) {
                let recordDbId = record.id;

                let mtaStatusObj = {
                    mtaStatusDbId: recordDbId,
                    recordCount: 1,
                    href: `${germplasmMtaUrlString}/${recordDbId}`
                };

                resultsArr.push(mtaStatusObj);
            }

            res.send(200, {
                rows: resultsArr
            })
            return
        }
        catch (error) {
            await logger.logFailure(endpoint, 'INSERT', error)

            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return

        }
    }
};