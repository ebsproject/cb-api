/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let validator = require('validator')
let errors = require('restify-errors')
let germplasmHelper = require('../../../helpers/germplasm/index')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let distinctOrderString = ``
        let namesCondition = ''
        let workingListCondition = ''
        let addedNamesString = ''
        let sortByInputOrderString = ''
        let parameters = {}
        let addGroupLimit = false;
        let groupLimit = 0;
        let retainSelection = false;
        let retainSelectionStr = '';
        let retainSelectionStrSelect = '';
        let packageDistinctColumns = ''
        parameters['distinctOn'] = ''

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', 'distinctOn', 'sortByInput', 'names', 'workingListDbId', 'multipleSelect', 'groupLimit', 'retainSelection', '__browser_filters__']

            // Get filter condiiton
            conditionString = await processQueryHelper
                .getFilter(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.__browser_filters__ !== undefined) {
                let browserFilters = req.body.__browser_filters__
                let condition = await processQueryHelper.getFilter(browserFilters, excludedParametersArray)

                if (conditionString != '') {
                    condition = condition.replace("WHERE", '')
                    conditionString += ` AND ` + condition
                } else {
                    conditionString = condition
                }
            }

            if (req.body.names !== undefined) {
                let namesFilter = req.body.names
                let namesArray = []

                for (value of namesFilter.split('|')) {
                    let name = await germplasmHelper.normalizeText(value)

                    name = `$$` + name + `$$`
                    namesArray.push(name)
                }
                namesCondition = namesCondition +
                    `germplasm.germplasm_normalized_name IN (${namesArray.toString()}) OR germplasm.id IN 
                        (
                            SELECT 
                                germplasm_id 
                            FROM 
                                germplasm.germplasm_name "germplasmName" 
                            WHERE 
                                "germplasmName".germplasm_normalized_name IN (${namesArray.toString()}) AND 
                                "germplasmName".is_void = FALSE
                        )`

                if (namesCondition != '') addedNamesString = `AND (${namesCondition})`
            }

            if (req.body.workingListDbId !== undefined && req.body.multipleSelect !== undefined) {

                let isMutipleSelect = req.body.multipleSelect
                let workingListDbId = req.body.workingListDbId
                if (req.body.retainSelection !== undefined) {
                    if (req.body.retainSelection == 'true') {
                        retainSelection = true
                    }
                }

                if (retainSelection) {
                    retainSelectionStr = `
                        LEFT JOIN
                            platform.list_member lm
                        ON
                            lm.data_id = package.id
                            AND lm.is_void = FALSE
                            AND lm.list_id = ${workingListDbId}
                    `
                    retainSelectionStrSelect = `,
					    CASE WHEN lm.id IS NULL THEN false ELSE true END as "isDisabled"`

                } else {
                    if (isMutipleSelect == 'true') {
                        workingListCondition = `
                            "packageDbId" NOT IN (SELECT
                                lm.data_id
                                FROM platform.list_member lm
                                where
                                    lm.list_id = ${workingListDbId}
                                    AND lm.is_void = FALSE
                            )
                        `
                    } else {
                        workingListCondition = `
                            "germplasmDbId" NOT IN (SELECT
                                s.germplasm_id
                                FROM platform.list_member lm,
                                germplasm.package p,
                                germplasm.seed s
                                where
                                    lm.list_id = ${workingListDbId}
                                    AND lm.is_void = FALSE
                                    AND lm.data_id = p.id
                                    AND s.id = p.seed_id
                                    AND p.is_void = FALSE
                                    AND s.is_void = FALSE
                            )
                        `
                    }
                }
            }

            if (req.body.groupLimit !== undefined) {
                if (req.body.groupLimit > 0) {
                    addGroupLimit = true
                    groupLimit = req.body.groupLimit
                }
            }

            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `${parameters['distinctOn']}`

                packageDistinctColumns = knex.raw(`${addedDistinctString}`)

                let distinctOnValue = parameters['distinctOn'];

                // Split the value of distinctOn via |
                let columns = distinctOnValue.split('|');
                let distinctOnSort = ''
                for (col of columns) {
                    distinctOnSort += `"${col}",`
                }

                distinctOnSort = distinctOnSort.replace(/,\s*$/, "")
                distinctOrderString = `ORDER BY ${distinctOnSort}`
            }

            // check if results should be sorted by input list
            if (req.body.sortByInput !== undefined) {
                parameters['sortByInput'] = req.body.sortByInput

                if (req.body.sortByInput.column === undefined) {
                    res.send(new errors.BadRequestError("Column is required when sorting results by input"))
                    return
                }

                if (req.body.sortByInput.value === undefined) {
                    res.send(new errors.BadRequestError("Value is required when sorting results by input"))
                    return
                }

                let sortValues = req.body.sortByInput.value.split('|').join(',')
                let sortColumn = req.body.sortByInput.column

                sortByInputOrderString = `
                    POSITION("${sortColumn}" IN 
                        '${sortValues}'
                    )
                `
            }
        }

        // Build base retrieval query
        let packagesQuery = null
        let fromQuery = `
            FROM
                germplasm.package package
            LEFT JOIN
                tenant.program program ON program.id = package.program_id
                AND program.is_void = FALSE
            LEFT JOIN
                place.facility container ON container.id = package.facility_id
                AND container.is_void = FALSE
            LEFT JOIN
                place.facility subFacility ON subFacility.id = container.parent_facility_id
                AND subFacility.is_void = FALSE
            LEFT JOIN
                place.facility facility ON facility.id = container.root_facility_id
                AND facility.is_void = FALSE
            JOIN
                germplasm.seed seed ON seed.id = package.seed_id
                AND seed.is_void = FALSE
            JOIN
                germplasm.germplasm germplasm ON germplasm.id = seed.germplasm_id
                AND germplasm.is_void = FALSE
            LEFT JOIN (
                SELECT
                    STRING_AGG(gn.name_value, '|'),
                    gn.germplasm_id
                FROM
                    germplasm.germplasm_name gn
                WHERE
                    gn.is_void = FALSE
                GROUP BY
                    gn.germplasm_id
            ) AS gn (germplasm_other_names, germplasm_id)
                ON gn.germplasm_id = germplasm.id
            LEFT JOIN
                experiment.location "location" ON "location".id = seed.source_location_id
                AND location.is_void = FALSE
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = seed.source_experiment_id
                AND experiment.is_void = FALSE
            LEFT JOIN
                experiment.plot plot ON plot.id = seed.source_plot_id
                AND plot.is_void = FALSE
            LEFT JOIN
                experiment.entry "entry" ON "entry".id = seed.source_entry_id
                AND entry.is_void = false
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id
                AND occurrence.is_void = FALSE
            LEFT JOIN
                tenant.season season ON season.id = experiment.season_id
                AND season.is_void = FALSE
            LEFT JOIN
                tenant.stage stage ON stage.id = experiment.stage_id
                AND stage.is_void = FALSE
            JOIN
                tenant.person creator ON creator.id = package.creator_id
            LEFT JOIN
                tenant.person modifier ON modifier.id = package.modifier_id
            ${retainSelectionStr}
            WHERE
                package.is_void = FALSE
            ${addedNamesString}
        `

        // Check if user specified values in fields parameter
        let addPackageCount = false
        if (parameters['fields']) {
            if (parameters['distinctOn']) {

                let fieldsString = await processQueryHelper
                    .getFieldValuesString(parameters['fields'])
                let selectString = knex.raw(`${fieldsString}`)
                packagesQuery = knex.select(selectString)
                // if fields is specied, surround columns in double quotes
                packagesQuery = packagesQuery.toString().replace(/(\s+AS\s*)(\w+)(\s*)/g, "$1\"$2\"$3")

            } else {
                packagesQuery = knex.column(parameters['fields'].split('|'))
            }
            packagesQuery += fromQuery
        } else {
            addPackageCount = true
            packagesQuery = `
                SELECT 
                    experiment.id AS "experimentDbId",
                    experiment.experiment_name AS "experiment",
                    experiment.experiment_year AS "experimentYear",
                    experiment.experiment_type AS "experimentType",
                    experiment.experiment_status AS "experimentStatus",
                    stage.id AS "experimentStageDbId",
                    stage.stage_name AS "experimentStage",
                    stage.stage_code AS "experimentStageCode",
                    occurrence.occurrence_name AS "experimentOccurrence",
                    occurrence.id AS "sourceOccurrenceDbId",
                    occurrence.occurrence_name AS "sourceOccurrenceName",
                    program.id AS "programDbId",
                    program.program_code AS "seedManager",
                    seed.id AS "seedDbId",  
                    seed.seed_name AS "seedName",
                    seed.seed_code AS "GID",
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation,
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.germplasm_type AS "germplasmType",
                    gn.germplasm_other_names AS "germplasmOtherNames",
                    "location".id AS "locationDbId",
                    "location".location_code AS "location",
                    "location".location_name AS "locationName",
                    season.id AS "sourceStudySeasonDbId",
                    season.season_code AS "sourceStudySeason",
                    "entry".entry_code AS "sourceEntryCode",
                    "entry".entry_number AS "seedSourceEntryNumber",
                    plot.id AS "seedSourcePlotDbId",
                    plot.plot_code AS "seedSourcePlotCode",
                    plot.plot_number AS "seedSourcePlotNumber",
                    plot.rep AS "replication",
                    seed.cross_id AS "crossDbId",
                    seed.harvest_date AS "harvestDate",
                    seed.harvest_method AS "harvestMethod",
                    seed.harvest_source AS "harvestSource",
                    (
                        SELECT 
                            EXTRACT (YEAR FROM seed.harvest_date)
                    ) AS "sourceHarvestYear",
                    package.id AS "packageDbId",
                    package.package_code AS "packageCode",
                    package.package_label AS "label",
                    package.package_quantity AS "quantity",
                    package.package_unit AS "unit",
                    package.package_document AS "packageDocument",
                    (
                        SELECT
                            data_value 
                        FROM 
                            germplasm.package_data
                        WHERE 
                            is_void=FALSE AND
                            package_id = package.id AND
                            variable_id = (
                                SELECT 
                                    id
                                FROM
                                    master.variable
                                WHERE
                                    abbrev = 'MC_CONT'
                            )
                        ORDER BY
                            id DESC
                        LIMIT 1
                    ) AS "moistureContent",
                    facility.id AS "facilityDbId", 
                    container.id AS "containerDbId",
                    subFacility.id AS "subFacilityDbId",
                    facility.facility_name AS "facility",
                    container.facility_name AS "container",
                    subFacility.facility_name AS "subFacility",
                    creator.person_name AS "creator",
                    modifier.person_name AS "modifier"
                    ${retainSelectionStrSelect}
                `
                + fromQuery
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        if (sortByInputOrderString != '') {
            let columnOrders = '';
            if (orderString != '') {
                columnOrders = ` ,` + orderString.replace('ORDER BY', '').trim()
            }
            orderString = `ORDER BY ` + sortByInputOrderString + columnOrders
        }

        if (distinctOrderString !== '') {
            /**
             * If distinctOn is specified, add the column to Sort condition first
             * If fields and sort are specified, the sort columns must be specified in the field
             */
            orderString = orderString.replace("ORDER BY", ',')
        }
        let packageCountQuery = ``;
        if (addPackageCount) {
            packageCountQuery = `
                COUNT(1) OVER (PARTITION BY "designation") AS "packageCount",
                array_length(uniq(sort(ARRAY_AGG("seedDbId") over (partition by "designation"))), 1) AS "seedCount",
            `

            if (addGroupLimit) {
                packageCountQuery += `
                    ROW_NUMBER() OVER (PARTITION BY "designation" ORDER BY "harvestDate" DESC nulls last) AS "orderNumber",
                `
            }
        }

        if (workingListCondition != '') {

            if (conditionString != '') {
                workingListCondition = ' AND ' + workingListCondition
            } else {
                workingListCondition = ' WHERE ' + workingListCondition
            }

            conditionString += workingListCondition
        }

        // Generate final SQL query
        let packagesFinalSqlQuery = `
            SELECT `
            + packageDistinctColumns
            + packageCountQuery +
            `
                *
            FROM
                (
            `
            + packagesQuery

            + `
                ) AS tbl
            `+ conditionString
            + distinctOrderString
            + orderString


        if (addGroupLimit) {
            packagesFinalSqlQuery = `
                SELECT *
                FROM
                    (
                `+ packagesFinalSqlQuery + `        
                    ) AS t
                WHERE
                    "orderNumber" <= `+ groupLimit +
                ` LIMIT (:limit) OFFSET (:offset)`
        } else {
            packagesFinalSqlQuery +=
                ` LIMIT (:limit) OFFSET (:offset)`
        }

        let packages = await sequelize
            .query(packagesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await packages === undefined || await packages.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let packagesCountFinalSqlQuery

        if (req.body == undefined) {
            packagesCountFinalSqlQuery = `
                SELECT
                    count(1)
                FROM
                    germplasm.package
                WHERE
                    is_void = FALSE
            `
        } else {

            if (addGroupLimit) {
                packagesCountFinalSqlQuery = `
                    select 
                        count(1)
                    from
                        (
                        select
                            ROW_NUMBER() OVER (PARTITION BY "designation" ORDER BY "harvestDate" DESC nulls last) AS "orderNumber",
                            *
                        from
                            (
                    `
                    + packagesQuery +
                    `
                            ) as tbl
                    `
                    + conditionString +
                    `
                        ) as tbl2
                        WHERE
                        "orderNumber" <= `+ groupLimit

            } else {
                packagesCountFinalSqlQuery = await processQueryHelper
                    .getCountFinalSqlQuery(
                        packagesQuery,
                        conditionString,
                        '',
                        packageDistinctColumns
                    )
            }
        }

        let packagesCount = await sequelize
            .query(packagesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })

        count = packagesCount[0].count

        res.send(200, {
            rows: packages,
            count: count
        })
        return
    }
}