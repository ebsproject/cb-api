/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let jwtDecode = require('jwt-decode')
let aclStudy = require('../../../../helpers/acl/study.js')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/process-paths/:id/tree
    get: async function (req, res, next) {

        // Retrieve the process path ID
        let processPathDbId = req.params.id

        // Check if the ID is valid
        if (!validator.isInt(processPathDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400041)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {

            // Get the process
            let processPathSql = `
                SELECT
                    item.id AS "processPathDbId",
                    item.abbrev,
                    item.name
                FROM
                    master.item item
                WHERE 
                    item.id = (:processPathDbId)
                    AND item.is_void = false
            `

            let process = await sequelize.query(processPathSql, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    processPathDbId: processPathDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await process == undefined || await process.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404014)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Get the activities of the process
            let acitvitySql = `
                SELECT
                    item.id AS "processPathDbId",
                    item.abbrev,
                    item.name
                FROM
                    master.item item, master.item_relation "itemRelation"
                WHERE
                    "itemRelation".parent_id = (:processPathDbId)  
                    AND "itemRelation".child_id = item.id
                    AND "itemRelation".is_void = FALSE
                    AND item.is_void = FALSE
                ORDER BY
                    "itemRelation".order_number
            `

            let activities = await sequelize.query(acitvitySql, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    processPathDbId: processPathDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await activities.length != undefined && await activities.length > 0) {

                for (activity of activities) {
                    // Get task for each activity
                    let taskSql = `
                        SELECT
                            item.id AS "processPathDbId",
                            item.abbrev,
                            item.name
                        FROM
                            master.item item, 
                            master.item_relation "itemRelation",
                            (
                                SELECT
                                    item.id as "processPathDbId",
                                    *
                                FROM
                                    master.item_relation "itemRelation",
                                    master.item item
                                WHERE
                                    "itemRelation".parent_id = (:processPathDbId)
                                    AND "itemRelation".child_id = item.id
                                    AND "itemRelation".is_void = false
                                    AND item.is_void = false
                            ) AS parent
                        WHERE
                            "itemRelation".parent_id = parent.child_id
                            AND "itemRelation".child_id = item.id
                            AND "itemRelation".root_id = (:processPathDbId)
                            AND "itemRelation".is_void = FALSE
                            AND item.is_void = FALSE
                            AND parent."processPathDbId" = (:activityDbId)
                        ORDER BY
                            "itemRelation".order_number
                    `

                    let tasks = await sequelize.query(taskSql, {
                        type: sequelize.QueryTypes.SELECT,
                        replacements: {
                            processPathDbId: processPathDbId,
                            activityDbId: activity.processPathDbId
                        }
                    })
                        .catch(async err => {
                            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                            res.send(new errors.InternalError(errMsg))
                            return
                        })

                    if (await tasks.length != undefined && await process.length > 0) {
                        activity['tasks'] = tasks
                    }

                }

                process[0]['activities'] = activities
            }

            res.send(200, {
                rows: process
            })
            return
        }
    }
}
