/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(req.body, excludedParametersArray)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let itemRelationsQuery = null
    // Check if the client specified values for the fields
    if (parameters['fields']) {
      itemRelationsQuery = knex.column(parameters['fields'].split('|'))
      itemRelationsQuery += `
        FROM
          master.item_relation item_relation
        LEFT JOIN
          tenant.person creator ON item_relation.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON item_relation.modifier_id = modifier.id
        LEFT JOIN
          master.item child ON item_relation.child_id = child.id
        LEFT JOIN
          master.item root ON item_relation.root_id = root.id
        LEFT JOIN
          master.item parent ON item_relation.parent_id = parent.id
        WHERE
          item_relation.is_void = FALSE
      ` + addedConditionString + `
        ORDER BY
          item_relation.id
      `
    } else {
      itemRelationsQuery = `
        SELECT
          item_relation.id AS "itemRelationDbId",
          item_relation.parent_id AS "parentDbId",
          item_relation.child_id AS "childDbId",
          item_relation.root_id AS "rootDbId",
          item_relation.order_number AS "orderNo",
          item_relation.tooltip,
          item_relation.url,
          item_relation.icon,
          item_relation.visible,
          item_relation.task,
          item_relation.options,
          item_relation.title,
          item_relation.itemrel_url AS "itemRelUrl",
          item_relation.remarks,
          item_relation.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          item_relation.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          master.item_relation item_relation
        LEFT JOIN
          tenant.person creator ON item_relation.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON item_relation.modifier_id = modifier.id
        LEFT JOIN
          master.item child ON item_relation.child_id = child.id
        LEFT JOIN
          master.item root ON item_relation.root_id = root.id
        LEFT JOIN
          master.item parent ON item_relation.parent_id = parent.id
        WHERE
          item_relation.is_void = FALSE
      ` + addedConditionString + `
        ORDER BY
          item_relation.id
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final SQL query
    let itemRelationsFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        itemRelationsQuery,
        conditionString,
        orderString
      )

    // Retrieve the item relation records
    let itemRelations = await sequelize
      .query(itemRelationsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })
    
    if (await itemRelations == undefined || await itemRelations.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    // Get the final count of the itemRelation records
    let itemRelationsCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        itemRelationsQuery,
        conditionString,
        orderString
      )

    let itemRelationsCount = await sequelize
      .query(itemRelationsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    count = itemRelationsCount[0].count

    res.send(200, {
      rows: itemRelations,
      count: count
    })
    return
  }
}