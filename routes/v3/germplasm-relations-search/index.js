/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger/index')

module.exports = {

    /**
     * Search germplasm relation records
     * POST /v3/germplasm-relations-search
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let params = req.query
        let count = 0

        let requestBody

        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let addedOrderString = `
            ORDER BY germplasm_relation.id
        `

        let excludeParametersArray = ['fields','distinctOn']
        let searchParams = {}
        searchParams['distinctOn'] = ''

        const endpoint = 'germplasm-relations-search'

        // set fields if body.fields is not undefined / empty
        if(req.body != undefined){
            requestBody = req.body

            if(req.body.fields != null){
                searchParams['fields'] = req.body.fields
            }

            // set distinctOn if body.distinctOn is not undefined / empty
            if(req.body.distinctOn != undefined){
                searchParams['distinctOn'] = req.body.distinctOn

                addedDistinctString = await processQueryHelper.getDistinctString(
                    searchParams['distinctOn']
                )

                searchParams['distinctOn'] = `"${searchParams['distinctOn']}"`
                addedOrderString = `
                    ORDER BY ${searchParams['distinctOn']}
                `
            }
            
            // Get the filter condition/s
            conditionString = await processQueryHelper.getFilter(
                requestBody,
                excludeParametersArray
            )

            if(conditionString.includes('invalid')){
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse sort parameters
        if(sort != null && addedDistinctString === '') {
            orderString = await processQueryHelper.getOrderString(sort)

            if(orderString.includes('invalid')){
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build base query
        let germplasmRelationQuery = null
        
        if(searchParams['fields'] != null){

            germplasmRelationQuery = knex.column(searchParams['fields'].split('|'))

            germplasmRelationQuery += `
                FROM
                    germplasm.germplasm_relation
                JOIN
                    germplasm.germplasm parent_germplasm
                        ON parent_germplasm.id = germplasm_relation.parent_germplasm_id
                        AND parent_germplasm.is_void = FALSE
                JOIN
                    germplasm.germplasm child_germplasm
                        ON child_germplasm.id = germplasm_relation.child_germplasm_id
                        AND child_germplasm.is_void = FALSE
                JOIN
                    tenant.person creator ON creator.id = germplasm_relation.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = germplasm_relation.modifier_id
                WHERE
                    germplasm_relation.is_void = FALSE
            `
        }
        else{
            germplasmRelationQuery = `
                SELECT
                    germplasm_relation.id AS "germplasmRelationDbId",
                    germplasm_relation.order_number AS "orderNumber",
                    parent_germplasm.id AS "parentGermplasmDbId",
                    parent_germplasm.designation AS "parentGermplasmName",
                    parent_germplasm.germplasm_code AS "parentGermplasmCode",
                    child_germplasm.id AS "childGermplasmDbId",
                    child_germplasm.designation AS "childGermplasmName",
                    child_germplasm.germplasm_code AS "childGermplasmCode",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    germplasm_relation.creation_timestamp AS "creationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier",
                    germplasm_relation.modification_timestamp AS "modificationTimestamp"
                FROM
                    germplasm.germplasm_relation
                JOIN
                    germplasm.germplasm parent_germplasm
                        ON parent_germplasm.id = germplasm_relation.parent_germplasm_id
                        AND parent_germplasm.is_void = FALSE
                JOIN
                    germplasm.germplasm child_germplasm
                        ON child_germplasm.id = germplasm_relation.child_germplasm_id
                        AND child_germplasm.is_void = FALSE
                JOIN
                    tenant.person creator ON creator.id = germplasm_relation.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = germplasm_relation.modifier_id
                WHERE
                    germplasm_relation.is_void = FALSE
            `
        
        }

        try{

            // Generate the final SQL query
            let germplasmRelationFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
                germplasmRelationQuery,
                conditionString,
                orderString,
                addedDistinctString,
            )

            let germplasmRelations = await sequelize.query(
                germplasmRelationFinalSqlQuery, 
                {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
                return
            })

            if ( await germplasmRelations == undefined || 
                await germplasmRelations.length < 1 ) {
                    res.send(200, {
                        rows: [],
                        count: 0
                    })
                    return
            }

            let germplasmRelationsCountSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                germplasmRelationQuery,
                conditionString,
                orderString,
                addedDistinctString
            )

            let germplasmRelationsCount = await sequelize.query(
                germplasmRelationsCountSqlQuery,
                {
                    type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            // If count is undefined or returned result is null, return
            if ( await germplasmRelationsCount == undefined || await germplasmRelationsCount == null ) {
                return
            }

            // If count is set, get value and set it as the count value for the result
            if (germplasmRelationsCount[0] !== undefined && germplasmRelationsCount[0] !== null
                && germplasmRelationsCount[0]['count'] !== undefined && germplasmRelationsCount[0]['count'] !== null
            ) {
                count = germplasmRelationsCount[0]['count']
            }

            res.send(200, {
                rows: germplasmRelations,
                count: count,
            })
            return
        }
        catch(error){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}