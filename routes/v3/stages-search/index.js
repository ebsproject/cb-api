/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields']
            // Get the filter condition
            conditionString = await processQueryHelper
                .getFilter(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let stagesQuery = null
        // Check if the client specified values for fields
        if (parameters['fields']) {
            stagesQuery = knex.column(parameters['fields'].split('|'))
            stagesQuery += `
                FROM
                    tenant.stage stage
                LEFT JOIN
                    tenant.person creator ON stage.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON stage.modifier_id = modifier.id
                WHERE
                    stage.is_void = FALSE
                ORDER BY
                    stage.id
            `
        } else {
            stagesQuery = `
                SELECT
                    stage.id AS "stageDbId",
                    stage.stage_code AS "stageCode",
                    stage.stage_name AS "stageName",
                    stage.description,
                    stage.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    stage.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    tenant.stage stage
                LEFT JOIN
                    tenant.person creator ON stage.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON stage.modifier_id = modifier.id
                WHERE
                    stage.is_void = FALSE
                ORDER BY
                    stage.id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let stagesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            stagesQuery,
            conditionString,
            orderString,
        )

        // Retrieve the stage records
        let stages = await sequelize
            .query(stagesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await stages == undefined || await stages.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let stagesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                stagesQuery,
                conditionString,
                orderString,
            )

        let stagesCount = await sequelize
            .query(stagesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = stagesCount[0].count

        res.send(200, {
            rows: stages,
            count: count
        })
        return
    }
}