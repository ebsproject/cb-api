/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY protocol.id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn',
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray,
        )

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }
    // Build the base retrieval query
    let protocolQuery = null
    // Check if the client specified values for fields
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])

        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        protocolQuery = knex.select(selectString)
      } else {
        protocolQuery = knex.column(parameters['fields'].split('|'))
      }

      protocolQuery += `
        FROM
          tenant.protocol protocol
        LEFT JOIN
          tenant.program program ON protocol.program_id = program.id
        LEFT JOIN
          tenant.person creator ON protocol.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON protocol.modifier_id = modifier.id
        WHERE
          protocol.is_void = FALSE
        ${addedOrderString}
      `
    } else {
      protocolQuery = `
        SELECT
          protocol.id AS "protocolDbId",
          protocol.protocol_code AS "protocolCode",
          protocol.protocol_name AS "protocolName",
          protocol.protocol_type AS "protocolType",
          protocol.description,
          program.id AS "programDbId",
          program.program_code AS "programCode",
          protocol.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          protocol.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          tenant.protocol protocol
        LEFT JOIN
          tenant.program program ON protocol.program_id = program.id
        LEFT JOIN
          tenant.person creator ON protocol.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON protocol.modifier_id = modifier.id
        WHERE
          protocol.is_void = FALSE
        ${addedOrderString}
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let protocolFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        protocolQuery,
        conditionString,
        orderString
      )

    let protocol = await sequelize
      .query(protocolFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await protocol == undefined || await protocol.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let protocolCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        protocolQuery,
        conditionString,
      )

    let protocolCount = await sequelize
      .query(protocolCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })

    count = protocolCount[0].count

    res.send(200, {
      rows: protocol,
      count: count
    })
    return
  }
}