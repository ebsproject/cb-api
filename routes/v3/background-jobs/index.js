/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let validator = require('validator')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../helpers/logger')

const endpoint = 'background-jobs'

module.exports = {
    post: async (req, res, next) => {
        
        // Set defaults
        let resultArray = []

        let workerName = null
        let description = null
        let jobStatus = null
        let entityDbId = null
        let entity = null
        let endpointEntity = null
        let application = null
        let method = null
        let message = null
        let startTime = null
        let remarks = null
        let notes = null

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let backgroundJobUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/background-jobs'

        // Checks if there is a request body
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Get userDbId
        let userDbId = await tokenHelper.getUserId(req, res)

        // If userDbId is undefined, terminate
        if (userDbId === undefined) {
            return
        }
        // If userDbId is null, return 401: User not found error
        else if (userDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        // Check if the user provided a valid records array
        try {
            records = data.records

            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction

        try {
            let backgroundJobsValuesArray = []
        
            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Check if required columns are in request body
                if (record.entity == undefined || record.endpointEntity == undefined ||
                    record.application == undefined || record.method == undefined) {
                    let errMsg = `Required parameters are missing. Ensure that entity, endpointEntity,
                        application, and method fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }

                entity = record.entity
                endpointEntity = record.endpointEntity
                application = record.application
                method = record.method

                /** Validation of required parameters and set values */

                // Validate entity
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                    --- Check if entity is existing return 1
                    SELECT 
                        count(1)
                    FROM 
                        dictionary.entity entity 
                    WHERE 
                        entity.is_void = FALSE AND
                        entity.abbrev = $$${entity}$$
                    )
                `
                validateCount += 1

                // Validate endpointEntity
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                    --- Check if endpointEntity is existing return 1
                    SELECT 
                        count(1)
                    FROM 
                        dictionary.entity endpointEntity 
                    WHERE 
                        endpointEntity.is_void = FALSE AND
                        endpointEntity.abbrev = $$${endpointEntity}$$
                    )
                `
                validateCount += 1

                // Validate application
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                    --- Check if application is existing return 1
                    SELECT 
                        count(1)
                    FROM 
                        platform.application application 
                    WHERE 
                        application.is_void = FALSE AND
                        application.abbrev = $$${application}$$
                    )
                `
                validateCount += 1

                /** Assignment of non-required parameters */

                workerName = (record.workerName != undefined) ? record.workerName : null
                description = (record.description != undefined) ? record.description : null
                jobStatus = (record.jobStatus != undefined) ? record.jobStatus : null
                message = (record.message != undefined) ? record.message : null
                remarks = (record.remarks != undefined) ? record.remarks : null
                notes = (record.notes != undefined) ? record.notes : null
                startTime = (record.startTime != undefined) ? record.startTime : null
                entityDbId = (record.entityDbId !== undefined) ? record.entityDbId : null

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let tempArray = [
                    workerName,
                    description,
                    jobStatus,
                    entityDbId,
                    entity,
                    endpointEntity,
                    application,
                    method,
                    message,
                    startTime,
                    userDbId,
                    remarks,
                    notes
                ]

                backgroundJobsValuesArray.push(tempArray)
            }

            // Create a background process record
            let backgroundJobsQuery = format(`
                INSERT INTO
                    api.background_job
                        (
                            worker_name,
                            description,
                            job_status,
                            entity_id,
                            entity,
                            endpoint_entity,
                            application,
                            method,
                            message,
                            start_time,
                            creator_id,
                            remarks,
                            notes
                        )
                VALUES
                    %L
                RETURNING
                    id      
                `, backgroundJobsValuesArray
            )

            transaction = await sequelize.transaction(async transaction => {
                let backgroundJobs = await sequelize.query(
                    backgroundJobsQuery, {
                        type: sequelize.QueryTypes.INSERT,
                        transaction: transaction,
                        raw: true
                    }
                ).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })

                for (let backgroundJob of backgroundJobs[0]) {
                    backgroundJobDbId = backgroundJob.id

                    let bjObject = {
                        backgroundJobDbId: backgroundJobDbId,
                        recordCount: 1,
                        href: backgroundJobUrlString
                            + '/' + backgroundJobDbId
                    }

                    resultArray.push(bjObject)
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(`${err}`))
            return
        }
    }
}