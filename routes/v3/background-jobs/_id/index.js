/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let logger = require('../../../../helpers/logger')

const endpoint = 'background-jobs/:id'

module.exports = {
    get: async function (req, res, next) {
        let queryParams = req.query ?? {}
        let jobStatus = queryParams.jobStatus
        let jobStatusString = ''

        if (jobStatus != null) {
            jobStatusString = `
                AND backgroundJobStatus ${jobStatus}
            `
        }

        let backgroundJobDbId = req.params.id

        if (!validator.isInt(backgroundJobDbId)) {
            let errMsg = 'Invalid format, backgroundJobDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let backgroundJobQuery = `
            SELECT
                backgroundJob.id AS "backgroundJobDbId",
                backgroundJob.worker_name AS "workerName",
                backgroundJob.description,
                backgroundJob.job_status AS "jobStatus",
                backgroundJob.message,
                backgroundJob.start_time AS "startTime",
                backgroundJob.end_time AS "endTime",
                backgroundJob.is_seen AS "isSeen",
                backgroundJob.entity_id as "entityDbId",
                backgroundJob.entity,
                backgroundJob.endpoint_entity as "endpointEntity",
                backgroundJob.method,
                backgroundJob.application,
                backgroundJob.remarks AS "jobRemarks",
                backgroundJob.notes AS "successfulIds",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                backgroundJob.creation_timestamp AS "creationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier",
                backgroundJob.modification_timestamp AS "modificationTimestamp"
            FROM
                api.background_job backgroundJob
            LEFT JOIN
                tenant.person creator ON backgroundJob.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON backgroundJob.modifier_id = modifier.id
            WHERE
                backgroundJob.is_void = FALSE AND
                backgroundJob.id = ${backgroundJobDbId}
        `
        // Retrieve the background process record from the database
        let backgroundJob = await sequelize
            .query(backgroundJobQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        if(backgroundJob === undefined) {
            return
        } 

        // Check if the background process exists
        if (
            await backgroundJob === null ||
            await backgroundJob.length < 1
        ) {
            let errMsg = "The background job record you are looking for does not exist."
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        res.send(200, {
            rows: backgroundJob
        })
        return
    },
    put: async function (req, res, next) {
        let backgroundJobDbId = req.params.id
        
        // Checks if there is a request body
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the ID provided in the URL is a valid integer
        if (!validator.isInt(backgroundJobDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400143)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve the client's ID via access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if (personDbId === undefined){
            return
        }
        // If personDbId is null, return error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if background job ID exists
        let backgroundJobQuery = `
            SELECT
                job.description AS "jobDescription",
                job.job_status AS "jobStatus",
                job.message AS "jobMessage",
                job.end_time AS "jobEndTime",
                job.remarks AS "jobRemarks",
                job.is_seen AS "jobIsSeen",
                job.notes
            FROM
                api.background_job job
            WHERE
                job.is_void = FALSE AND
                job.id = ${backgroundJobDbId}
        `

        // Retrieve background job data record from database
        let backgroundJob = await sequelize
            .query(backgroundJobQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        if(backgroundJob === undefined) {
            return
        } 

        // Check if the background process exists
        if (
            await backgroundJob === null ||
            await backgroundJob.length < 1
        ) {
            let errMsg = `The background job record you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get the existing data from the database
        let recordJobDescription = backgroundJob[0].jobDescription
        let recordJobStatus = backgroundJob[0].jobStatus
        let recordJobMessage = backgroundJob[0].jobMessage
        let recordJobEndTime = backgroundJob[0].jobEndTime
        let recordJobRemarks = backgroundJob[0].jobRemarks
        let recordJobIsSeen = backgroundJob[0].jobIsSeen
        let recordNotes = backgroundJob[0].notes

        let isSecure = forwarded(req, req.header).secure

        // Set URL for response
        let backgroundJobUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + 'v3/background-jobs/'
            + backgroundJobDbId

        let data = req.body
        let transaction

        try {
            let setQuery = ``

            if (
                data.jobDescription !== undefined &&
                data.jobDescription !== recordJobDescription
            ) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    description = $$${data.jobDescription}$$
                `
            }

            if (
                data.jobStatus !== undefined &&
                data.jobStatus !== recordJobStatus
            ) {
                let validStatus = [
                    'DONE',
                    'FAILED',
                    'IN_PROGRESS',
                    'IN_QUEUE',
                ]
                if (!validStatus.includes(data.jobStatus)) {
                    let errMsg = "Invalid request, you have entered an invalid job status value."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    job_status = $$${data.jobStatus}$$
                `
            }

            if (
                data.jobMessage !== undefined &&
                data.jobMessage !== recordJobMessage
            ) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    message = $$${data.jobMessage}$$
                `
            }
            
            if (
                data.jobEndTime !== undefined &&
                data.jobEndTime !== recordJobEndTime
            ) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    end_time = $$${data.jobEndTime}$$
                `
            }

            if (
                data.jobRemarks !== undefined &&
                data.jobRemarks !== recordJobRemarks
            ) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    remarks = $$${data.jobRemarks}$$
                `
            }

            if (
                data.notes !== undefined &&
                data.notes !== recordNotes
            ) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    notes = $$${data.notes}$$
                `
            }

            if (
                data.jobIsSeen !== undefined &&
                data.jobIsSeen !== recordJobIsSeen
            ) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    is_seen = $$${data.jobIsSeen}$$
                `
            }

            if (setQuery.length > 0) setQuery += `,`
            
            // Update the background job record
            let updateBackgroundJobQuery = `
                UPDATE
                    api.background_job
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${backgroundJobDbId}
            `

            let resultArray

            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateBackgroundJobQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
    
                // Return the transaction info
                resultArray = {
                    backgroundJobDbId: backgroundJobDbId,
                    recordCount: 1,
                    href: backgroundJobUrlString
                }
            })
            
            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}