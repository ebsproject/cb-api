/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../config/sequelize")
let tokenHelper = require("../../../helpers/auth/token")
let errors = require("restify-errors")
let errorBuilder = require("../../../helpers/error-builder")
let forwarded = require("forwarded-for")
let format = require("pg-format")
let validator = require("validator")

module.exports = {
    // Endpoint for creating a new package data record
    // POST /v3/package-data
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []
        let recordCount = 0

        let packageDataDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let packageDataUrlString =
            (isSecure ? "https" : "http") +
            "://" +
            req.headers.host +
            "/v3/package-data"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = data.records

        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(
                req.headers.host,
                400005
            )
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let packageDataValuesArray = []
            let dataQcCodeDefault = 'G'
            let validDataQcCodeValues = ['N', 'G', 'Q', 'S', 'M', 'B']
            let variableInfo = {}

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                let variableAbbrev = null
                let dataValue = null
                let packageDbId = null
                let dataQcCode = null

                // Check if required columns are in the request body
                if (
                    record.variableAbbrev == undefined ||
                    record.dataValue == undefined ||
                    record.packageDbId == undefined
                ) {
                    let errMsg = ` Required parameters are missing. Ensure that variableAbbrev, dataValue, and packageDbId fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */
                variableAbbrev = record.variableAbbrev
                dataValue = record.dataValue
                packageDbId = record.packageDbId
                dataQcCode = record.dataQcCode

                // database validation for package ID
                if (!validator.isInt(packageDbId)) {
                    let errMsg = `Invalid format, package ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += validateQuery != "" ? " + " : ""
                validateQuery += `
                    (
                      --- Check if package is existing return 1
                      SELECT 
                        count(1)
                      FROM 
                        germplasm.package package
                      WHERE 
                        package.is_void = FALSE AND
                        package.id = ${packageDbId}
                    )`
                validateCount += 1

                // database validation for variable abbrev
                validateQuery += validateQuery != "" ? " + " : ""
                validateQuery += `
                    (
                      --- Check if variable is existing return 1
                      SELECT 
                        count(1)
                      FROM 
                        master.variable variable
                      WHERE 
                        variable.is_void = FALSE AND
                        variable.abbrev = '${variableAbbrev}'
                    )`
                validateCount += 1

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                      ${validateQuery}
                    AS count`

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT,
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(
                        req.headers.host,
                        400015
                    )
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (dataQcCode != undefined && dataQcCode != null) {
                    // Check if data QC code is valid
                    if(!validDataQcCodeValues.includes(dataQcCode)) {
                        let errMsg = `Invalid format, data QC code must be: ` + validDataQcCodeValues.join(', ')
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }
                else {
                    dataQcCode = dataQcCodeDefault
                }

                // Retrieve variable id and scale values
                if (variableInfo[variableAbbrev] === undefined) {
                    let variableQuery = `
                        SELECT
                            v.id AS "variableDbId",
                            array_agg(sv.value) AS "scaleValues"
                        FROM
                            master.variable v
                        LEFT JOIN
                            master.scale_value sv ON sv.scale_id = v.scale_id AND sv.is_void = FALSE
                        WHERE
                            v.is_void = FALSE
                            AND v.abbrev = '${variableAbbrev}'
                        GROUP BY "variableDbId"
                    `
                    let variable = await sequelize.query(variableQuery, {
                        type: sequelize.QueryTypes.SELECT,
                    })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                    // Return error if result is undefined
                    if (await variable == undefined) {
                        return
                    }
                    variable = variable[0] !== undefined ? variable[0] : []
                    let variableDbId = variable["variableDbId"] !== null ? variable["variableDbId"] : []
                    let variableScaleValues = variable["scaleValues"] !== null ? variable["scaleValues"] : []
                    variableInfo[variableAbbrev] = {
                        variableDbId: variableDbId,
                        scale: variableScaleValues[0] !== null ? variableScaleValues : []
                    }
                }

                // Check if dataValue matches the scale values for the variable
                let currentVariableScaleValues = variableInfo[variableAbbrev].scale
                let currentVariableDbId = variableInfo[variableAbbrev].variableDbId
                if (currentVariableScaleValues.length > 0 && !currentVariableScaleValues.includes(dataValue)) {
                    let errMsg = `Invalid format, ${variableAbbrev} must be: ` + currentVariableScaleValues.join(', ')
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [
                    packageDbId,
                    currentVariableDbId,
                    dataValue,
                    dataQcCode,
                    personDbId
                ]

                packageDataValuesArray.push(tempArray)
            }

            // Create packageData record(s)
            let packageDataQuery = format(
                `
                INSERT INTO
                  germplasm.package_data (
                    package_id, variable_id, data_value, data_qc_code, creator_id
                  )
                VALUES 
                  %L
                RETURNING id`,
                packageDataValuesArray
            )

            let packageData = await sequelize.query(packageDataQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction,
            })
                .catch(async err => {
                    return
                })

            // Check if insertion query returned results
            if (packageData == undefined || packageData[0].length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            for (packageDatum of packageData[0]) {
                packageDataDbId = packageDatum.id

                // Return the packageDatum info
                let array = {
                    packageDataDbId: packageDataDbId,
                    recordCount: 1,
                    href: packageDataUrlString + "/" + packageDataDbId,
                }
                resultArray.push(array)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray,
            })
            return
        } catch (err) {
            // Rollback transaction (if still open)
            if (transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },
}
