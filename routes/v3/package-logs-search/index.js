/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let addedConditionString = ''
        let parameters = { 'distinctOn': '' }
        const endpoint = 'package-logs-search'

        let excludedParametersArray = {}
        let tableAliases = {}
        let columnAliases = {}

        let responseColString = `
            package_log.id AS "packageLogDbId",
            package_log.package_id AS "packageDbId",
            package.package_label AS "packageLabel",
            package.package_code AS "packageCode",
            package_log.package_quantity AS "packageQuantity",
            package_log.package_unit AS "packageUnit",
            package_log.package_transaction_type AS "packageTransactionType",
            package_log.entity_id AS "entityDbid",
            package_log.data_id AS "dataDbId",
            package_log.remarks AS "remarks",
            package_log.creation_timestamp AS "creationTimestamp",
            package_log.creator_id AS "creatorDbId",
            creator.person_name AS "creator",
            package_log.modification_timestamp AS "modificationTimestamp",
            package_log.modifier_id AS "modifierDbId",
            modifier.person_name AS "modifier"
        `

        if (req.body !== undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields

                // extract first attribute for sort
                let parseFields = parameters['fields'].split('|')

                let sortAttrib = ''
                // retrieve 1st attribute alias as sort
                sortAttrib = parseFields[0].split(new RegExp('AS', 'i'))
                
                // if has no alias, retrieve column name as sort
                sortAttrib = sortAttrib[1] ?? sortAttrib[0].split('.')[1]

                // if has no alias and column name for sort
                sortAttrib = sortAttrib ?? 'invalid'

                sort = sort ?? `${sortAttrib.trim()}`
            }

            excludedParametersArray = ['fields', 'distinctOn']

            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ?  conditionStringArr.mainQuery : ''
            tableAliases = conditionStringArr.tableAliases
            columnAliases = conditionStringArr.columnAliases

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn

                sort = `"${parameters['distinctOn']}" packageLogDbId`

                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 400057)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        else{
            orderString = `ORDER BY package_log.id`
        }
        
        let packageLogQuery = null
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(
                        parameters['fields'],
                    )

                let selectString
                    = knex.raw(`${addedDistinctString} ${fieldsString}`)
                packageLogQuery = knex.select(selectString)
            } else {
                packageLogQuery = knex.column(parameters['fields'].split('|'))
            }

            packageLogQuery += `
            FROM
                germplasm.package_log package_log
                JOIN germplasm.package
                    ON package_log.package_id = package.id AND package.is_void = FALSE
                JOIN tenant.person creator
                    ON package_log.creator_id = creator.id
                LEFT JOIN tenant.person modifier
                    ON package_log.modifier_id = modifier.id
            WHERE
                package_log.is_void = FALSE
            ${addedConditionString}
            ${orderString}
            `
        } else {
            packageLogQuery = `
                SELECT
                    ${addedDistinctString}
                    ${responseColString}
                FROM
                    germplasm.package_log package_log
                    JOIN germplasm.package
                        ON package_log.package_id = package.id AND package.is_void = FALSE
                    JOIN tenant.person creator
                        ON package_log.creator_id = creator.id
                    LEFT JOIN tenant.person modifier
                        ON package_log.modifier_id = modifier.id
                WHERE
                    package_log.is_void = FALSE
                ${orderString}
            `
        }

        packageLogFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            packageLogQuery,
            conditionString
        )
        
        // Retrieve package logs from the database
        let packageLogs = await sequelize
            .query(packageLogFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (packageLogs == undefined || packageLogs.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let tableJoins = {
            'package' : ' JOIN germplasm.package package ON package.id = package_log.package_id AND package.is_void = FALSE ',
            'creator': ' JOIN tenant.person creator ON package.creator_id = creator.id',
            'modifier':' LEFT JOIN tenant.person modifier ON package.modifier_id = modifier.id'
        }

        // retrieval of totalCount values will be processed separately
        let packageLogCountFinalSqlQuery = await processQueryHelper
            .getTotalCountQuery(
                req.body,
                excludedParametersArray,
                conditionString, //filter string
                addedDistinctString, //distinct string used in the main query
                tableJoins, //join statements used in the resource
                'germplasm.package_log package_log',
                tableAliases, //table aliases involved in the join statements
                'package_log.id', //main column
                columnAliases, //columns used in the filter condition
                orderString,
                responseColString
            )

        let packageLogCount = await sequelize
            .query(
                packageLogCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = packageLogCount[0].count

        res.send(200, {
            rows: packageLogs,
            count: count
        })
        return
    }
}