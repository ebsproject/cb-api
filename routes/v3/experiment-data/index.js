/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

module.exports = {
  post: async function (req, res, next) {
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure
    // Set the URL for the response
    let experimentDataUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/experiment-data'

    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let records = []
    let recordCount = null

    try {
      if (req.body.records == undefined || req.body.records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      records = req.body.records
      recordCount = records.length
    } catch (e) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction

    try {
      transaction = await sequelize.transaction({ autocommit: false })

      let experimentDataValuesArray = []

      for (var record of records) {
        // Check if the user specified the experimentId
        if (
          record.experimentDbId == undefined ||
          record.variableDbId == undefined ||
          record.dataValue == undefined
        ) {
          let errMsg =
            'Invalid request, required parameters experimentDbId, variableDbId, and dataValue are missing.'
          res.send(new errors.BadRequestError(errMsg))
          return
        } 

        let validateQuery = ``
        let validateCount = 0

        // Validate all values that relate to another table
        let experimentDbId = null
        // Check if the experimentDbId is a valid integer
        if (record.experimentDbId != undefined) {
          experimentDbId = record.experimentDbId
          if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
          // Validate if the experiment exists
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if the experiment exists, return 1
              SELECT
                count(1)
              FROM
                experiment.experiment experiment
              WHERE
                experiment.is_void = FALSE AND
                experiment.id = ${experimentDbId}
            )
          `

          validateCount += 1
        }

        let variableDbId = null
        if (record.variableDbId != undefined) {
          variableDbId = record.variableDbId
          if (!validator.isInt(variableDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400081)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
          // Validate if the variable exists
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if the variable exists, return 1
              SELECT
                count(1)
              FROM
                master.variable variable
              WHERE
                variable.is_void = FALSE AND
                variable.id = ${variableDbId}
            )
          `
          
          validateCount += 1
        }

        let protocolDbId = null
        if (record.protocolDbId != undefined) {
          protocolDbId = record.protocolDbId
          if (!validator.isInt(protocolDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400084)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
          // Validate if the protocol exists
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if the protocol exists, return 1
              SELECT
                count(1)
              FROM
                master.protocol protocol
              WHERE
                protocol.is_void = FALSE AND
                protocol.id = ${protocolDbId}
            )
          `

          validateCount += 1
        }

        let dataValue = (record.dataValue !== undefined) ? record.dataValue : null

        if (
          record.experimentDbId !== undefined ||
          record.variableDbId !== undefined ||
          record.protocolDbId !== undefined
        ) {
          let checkInputQuery = `SELECT ${validateQuery} AS count`
          let inputCount = await sequelize.query(checkInputQuery, {
            type: sequelize.QueryTypes.SELECT
          })

          if (inputCount[0]['count'] != validateCount) {
            let errMsg = await errorBuilder.getError(req.headers.host,  400015)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        let tempArray = [
          experimentDbId,
          variableDbId,
          dataValue,
          'G',
          protocolDbId,
          userDbId     
        ]

        experimentDataValuesArray.push(tempArray)
      }

      // Create an experiment data record
      let experimentDataQuery = format(`
        INSERT INTO
          experiment.experiment_data
            (
              experiment_id,
              variable_id,
              data_value,
              data_qc_code,
              protocol_id,
              creator_id
            )
          VALUES
            %L
          RETURNING id`, experimentDataValuesArray
      )

      experimentDataQuery = experimentDataQuery.trim()

      let experimentData = await sequelize
        .query(experimentDataQuery, {
          type: sequelize.QueryTypes.INSERT,
          transaction: transaction
        })

      let resultArray = []
      for (let data of experimentData[0]) {
        experimentDataDbId = data.id

        let obj = {
          experimentDataDbId: experimentDataDbId,
          recordCount: 1,
          href: experimentDataUrlString + '/' + experimentDataDbId
        }
  
        resultArray.push(obj)
      }

      await transaction.commit()

      res.send(200, {
        rows: resultArray,
        count: resultArray.length
      })
      return
    } catch (e) {
      if (e) await transaction.rollback()

      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}