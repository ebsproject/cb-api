/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator.js')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {
 
  // PUT /experiment-data/:id
  put: async function (req, res, next) {
    // Retrieve experiment data ID
    let experimentDataDbId = req.params.id

    // Check if experiment data ID is an integer
    if (!validator.isInt(experimentDataDbId)) {
      let errMsg = `Invalid format, experiment data ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Retrieve user ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(personDbId)

    // Set defaults
    let dataValue = null

    // Build query
    let experimentDataQuery = `
      SELECT 
        experimentData.data_value AS "dataValue",
        experiment.id AS "experimentDbId",
        creator.id AS "creatorDbId"
      FROM
        experiment.experiment_data experimentData
        LEFT JOIN
          experiment.experiment experiment ON experiment.id = experimentData.experiment_id
        LEFT JOIN
          tenant.person creator ON creator.id = experimentData.creator_id
      WHERE
        experimentData.is_void = FALSE AND
        experimentData.id = ${experimentDataDbId}
    `

    // Retrieve experiment data record from database
    let experimentData = await sequelize.query(experimentDataQuery,{
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (experimentData === undefined || experimentData.length < 1) {
      let errMsg = `The experiment data record you have requested does not exist.`
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Get values from database, record = the current value in the database
    let recordDataValue = experimentData[0].dataValue
    let recordExperimentDbId = experimentData[0].experimentDbId
    let recordCreatorDbId = experimentData[0].creatorDbId

    // Check if user is an admin or a program member of the experiment
    if(!isAdmin || (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
            SELECT 
                person.id,
                person.person_role_id AS "role"
            FROM 
                tenant.person person
            LEFT JOIN 
                tenant.team_member teamMember ON teamMember.person_id = person.id
            LEFT JOIN 
                tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
            LEFT JOIN 
                tenant.program program ON program.id = programTeam.program_id 
            LEFT JOIN
                experiment.experiment experiment ON experiment.program_id = program.id
            WHERE 
                experiment.id = ${recordExperimentDbId} AND
                person.id = ${personDbId} AND
                person.is_void = FALSE
        `
        
        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
    }

    let isSecure = forwarded(req, req.header).secure

    // Set URL for response
    let experimentDataUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/experiment-data/'
      + experimentDataDbId

    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let transaction

    try {

      let setQuery = ``

      if (data.dataValue !== undefined) {
        dataValue = data.dataValue

        // Check if user input is same with the current value in the database
        if (dataValue != recordDataValue) {
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_value = $$${dataValue}$$
          `
        }
      }

      if(setQuery.length > 0) setQuery += `,`

      let updateExperimentDataQuery = `
        UPDATE
          experiment.experiment_data experimentData
        SET
          ${setQuery}
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          experimentData.id = ${experimentDataDbId}
      `

      transaction = await sequelize.transaction({ autocommit: false })

      await sequelize.query(updateExperimentDataQuery, {
        type: sequelize.QueryTypes.UPDATE,
        transaction: transaction
      })

      // Return transaction info
      let resultArray = {
        experimentDataDbId: experimentDataDbId,
        recordCount: 1,
        href: experimentDataUrlString
      }

      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      res.send(new errors.InternalError(errMsg))
      return
    }
  },

  // Endpoint for voiding an existing experiment data record
  // DELETE /v3/experiment-data/:id
  delete: async function (req, res, next) {

    // Retreive experiment-data ID
    let experimentDataDbId = req.params.id

    if(!validator.isInt(experimentDataDbId)) {
      let errMsg = `Invalid format, experiment data ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    let isAdmin = await userValidator.isAdmin(personDbId)

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let experimentDataQuery = `
        SELECT
          experimentData.id AS "experimentDataDbId",
          experiment.id AS "experimentDbId",
          creator.id AS "creatorDbId"
        FROM
          experiment.experiment_data experimentData
          LEFT JOIN 
            tenant.person creator ON creator.id = experimentData.creator_id
          LEFT JOIN
            experiment.experiment experiment ON experiment.id = experimentData.experiment_id
        WHERE
          experimentData.is_void = FALSE AND
          experimentData.id = ${experimentDataDbId}
      `

      let experimentData = await sequelize.query(experimentDataQuery, {
        type: sequelize.QueryTypes.SELECT
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      if (experimentData == undefined || experimentData.length < 1) {
        let errMsg = `The experiment data you have requested does not exist.`
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      let recordCreatorDbId = experimentData[0].creatorDbId
      let recordExperimentDbId = experimentData[0].experimentDbId

      // Check if user is an admin or an owner of the cross
      if(!isAdmin && (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
          SELECT 
            person.id,
            person.person_role_id AS "role"
          FROM 
            tenant.person person
            LEFT JOIN 
              tenant.team_member teamMember ON teamMember.person_id = person.id
            LEFT JOIN 
              tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
            LEFT JOIN 
              tenant.program program ON program.id = programTeam.program_id 
            LEFT JOIN
              experiment.experiment experiment ON experiment.program_id = program.id
          WHERE 
            experiment.id = ${recordExperimentDbId} AND
            person.id = ${personDbId} AND
            person.is_void = FALSE
        `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (
          await person[0].id === undefined ||
          await person[0].role === undefined
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
      }

      let deleteExperimentDataQuery = format(`
        UPDATE
          experiment.experiment_data experimentData
        SET
          is_void = TRUE,
          notes = CONCAT('VOIDED-', $$${experimentDataDbId}$$),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          experimentData.id = ${experimentDataDbId}
      `)
      
      await sequelize.query(deleteExperimentDataQuery, {
        type: sequelize.QueryTypes.UPDATE
      })

      await transaction.commit()
      res.send(200, {
        rows: { experimentDataDbId: experimentDataDbId }
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}