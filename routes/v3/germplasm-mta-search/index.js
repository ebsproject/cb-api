/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require("../../../config/sequelize");
let { knex } = require("../../../config/knex");
let processQueryHelper = require("../../../helpers/processQuery/index");
let errors = require("restify-errors");
let errorBuilder = require("../../../helpers/error-builder");

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit;
    let offset = req.paginate.offset;
    let sort = req.query.sort;
    let count = 0;
    let orderString = "";
    let conditionString = "";
    let addedDistinctString = "";
    let addedOrderString = `ORDER BY "germplasm_mta".id`;
    let parameters = {};
    parameters["distinctOn"] = "";

    if (req.body) {
      if (req.body.fields != null) {
        parameters["fields"] = req.body.fields;
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ["fields", "distinctOn"];

      // Get filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      );

      if (req.body.distinctOn != undefined) {
        parameters["distinctOn"] = req.body.distinctOn;
        addedDistinctString = await processQueryHelper.getDistinctString(
          parameters["distinctOn"]
        );
        parameters["distinctOn"] = `"${parameters["distinctOn"]}"`;
        addedOrderString = `
          ORDER BY
            ${parameters["distinctOn"]}
        `;
      }
    }

    // Build the base retrieval query
    let germplasmMtaDataQuery = null;
    // Check if the client specified values for fields
    if (parameters["fields"]) {
      if (parameters["distinctOn"]) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters["fields"]
        );

        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`);
        germplasmMtaDataQuery = knex.select(selectString);
      } else {
        germplasmMtaDataQuery = knex.column(parameters["fields"].split("|"));
      }

      germplasmMtaDataQuery += `
        FROM
          germplasm.germplasm_mta "germplasm_mta"
        WHERE
          "germplasm_mta".is_void = FALSE
          ${addedOrderString}
      `;
    } else {
      germplasmMtaDataQuery = `
          SELECT
            germplasm_mta.id AS "germplasmMtaDbId",
            germplasm_mta.germplasm_id AS "germplasmDbId",
            germplasm_mta.seed_id AS "seedDbId",
            germplasm_mta.mta_status AS "mtaStatus",
            germplasm_mta.use AS "use",
            germplasm_mta.mls_ancestors AS "mlsAncestors",
            germplasm_mta.genetic_stock AS "geneticStock",
            germplasm_mta.remarks AS "remarks",
            germplasm_mta.creator_id AS "creatorDbId",
            germplasm_mta.modifier_id AS "modifierDbId",
            germplasm_mta.notes  AS "notes"
          FROM
            germplasm.germplasm_mta germplasm_mta
          WHERE
            germplasm_mta.is_void = FALSE 
          ${addedOrderString}
          `;
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort);
      if (orderString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    let germplasmMtaDataFinalSqlQuery =
      await processQueryHelper.getFinalSqlQuery(
        germplasmMtaDataQuery,
        conditionString,
        orderString
      );

    let germplasmMtaData = await sequelize
      .query(germplasmMtaDataFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        },
      })
      .catch(async (err) => {
        console.log(err);
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    if (
      (await germplasmMtaData) == undefined ||
      (await germplasmMtaData.length) < 1
    ) {
      res.send(200, {
        rows: [],
        count: 0,
      });
      return;
    }

    let germplasmMtaDataCountFinalSqlQuery =
      await processQueryHelper.getCountFinalSqlQuery(
        germplasmMtaDataQuery,
        conditionString,
        orderString
      );

    let germplasmMtaDataCount = await sequelize.query(
      germplasmMtaDataCountFinalSqlQuery,
      {
        type: sequelize.QueryTypes.SELECT,
      }
    );

    count = germplasmMtaDataCount[0].count;

    res.send(200, {
      rows: germplasmMtaData,
      count: count,
    });
    return;
  },
};
