/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let validator = require('validator')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    /**
     * Retrieve packages for germplasm file upload
     * GET /v3/germplasm-file-upload-packages
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = []
        let germplasmFileUploadDbId

        // Retrieve the file upload ID
        germplasmFileUploadDbId = params.germplasmFileUploadDbId

        // Check if required parameter germplasmFileUploadDbId is provided
        if(germplasmFileUploadDbId === null || germplasmFileUploadDbId === undefined) {
            let errMsg = 'Missing parameter. The germplasmFileUploadDbId is required.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if germplasmFileUploadDbId is an integer
        if(!validator.isInt(germplasmFileUploadDbId)) {
            let errMsg = 'Invalid parameter. The germplasmFileUploadDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let gfuQuery = `
            SELECT
                gfu.id AS "germplasmFileUploadDbId",
                gfu.file_name AS "fileName"
            FROM
                germplasm.file_upload gfu
            WHERE
                gfu.is_void = FALSE AND
                gfu.id = ${germplasmFileUploadDbId}
        `

        // Find file upload in the database
        let gfu = await sequelize
            .query(gfuQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If the file upload does not exist, return an error
        if (await gfu == undefined || await gfu.length < 1) {
            let errMsg = 'Resource not found. The germplasm file upload you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Build query
        let packagesQuery = `
            SELECT
                gfu.id AS "germplasmFileUploadDbId",
                gfu.file_name AS "fileName",
                gfu.germplasm_count AS "germplasmCount",
                gfu.seed_count AS "seedCount",
                gfu.package_count AS "packageCount",
                package.id AS "packageDbId",
                package.package_code AS "packageCode",
                package.package_label AS "packageLabel",
                package.package_quantity AS "packageQuantity",
                package.package_unit AS "packageUnit",
                program.id AS "programDbId",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                container.id AS "containerDbId",
                container.facility_code AS "containerCode",
                container.facility_name AS "containerName",
                sub_facility.id AS "subFacilityDbId",
                sub_facility.facility_code AS "subFacilityCode",
                sub_facility.facility_name AS "subFacilityName",
                facility.id AS "facilityDbId",
                facility.facility_code AS "facilityCode",
                facility.facility_name AS "facilityName",
                seed.id AS "seedDbId",
                seed.seed_code AS "seedCode",
                seed.seed_name AS "seedName",
                seed.harvest_date AS "harvestDate",
                seed.harvest_method AS "harvestMethod",
                seed.description AS "description",
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.designation AS "designation",
                germplasm.germplasm_state AS "germplasmState",
                germplasm.germplasm_type AS "germplasmType"
            FROM
                germplasm.file_upload_germplasm gfug
            LEFT JOIN
                germplasm.file_upload gfu ON gfu.id = gfug.file_upload_id AND gfu.is_void = FALSE
            LEFT JOIN
                germplasm.package package on package.id = gfug.package_id AND package.is_void = FALSE
            LEFT JOIN
                tenant.program program ON program.id = package.program_id AND program.is_void = FALSE
            LEFT JOIN
                place.facility container ON container.id = package.facility_id
            LEFT JOIN
                place.facility sub_facility ON sub_facility.id = container.parent_facility_id
            LEFT JOIN
                place.facility facility ON facility.id = container.root_facility_id
            LEFT JOIN
                germplasm.seed seed ON seed.id = package.seed_id AND seed.is_void = FALSE
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = seed.germplasm_id AND germplasm.is_void = FALSE
            WHERE
                gfu.id = ${germplasmFileUploadDbId}
                AND gfug.is_void = FALSE
                AND package.id IS NOT NULL
        `

        // Get the filter condition
        conditionString = await processQueryHelper.getFilter(
            searchParams,
            []
        )

        if (conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let packagesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            packagesQuery,
            conditionString,
            orderString,
        )

        // Retrieve packages from the database   
        let packages = await sequelize
            .query(packagesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await packages == undefined || await packages.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get the final count of the package records
        let packagesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                packagesQuery,
                conditionString,
                orderString
            )

        let packagesCount = await sequelize
            .query(packagesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = packagesCount === undefined ? 0 : packagesCount[0].count

        res.send(200, {
            rows: packages,
            count: count
        })
        return
    }
}
