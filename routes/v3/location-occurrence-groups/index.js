/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {
  // Endpoint for creating a new location occurrence group record
  // POST /v3/location-occurrence-groups
  post: async function (req, res, next) {
    // Set defaults
    let resultArray = []
    let recordCount = 0

    let locationDbId = null
    let occurrenceDbId = null
    let orderNo = null

    // Get person Id from token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure

    // Set URL for response
    let locationUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/location-occurrence-groups'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      records = data.records
      recordCount = records.length

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    try {
      transaction = await sequelize.transaction({ autocommit: false })

      let locOccGroupValuesArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        // Check if required columns are in the request body
        if (!record.locationDbId || !record.occurrenceDbId) {
          let errMsg = `Required parameters are missing. Ensure that locationDbId and occurrenceDbId fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        locationDbId = record.locationDbId
        occurrenceDbId = record.occurrenceDbId

        // Validate if location exists
        let locQuery = `
          SELECT
            count(1)
          FROM
            experiment.location
          WHERE
            id = ${locationDbId} AND
            is_void = false
        `
        let locResult = await sequelize.query(locQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (locResult[0].count == 0) {
          let errMsg = `Location does not exist.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Validate if occurrence exists
        let occQuery = `
          SELECT
            count(1)
          FROM
            experiment.occurrence
          WHERE
            id = ${occurrenceDbId}
            and is_void = false
        `
        let occResult = await sequelize.query(occQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (occResult[0].count == 0) {
          let errMsg = `Occurrence does not exist.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Validate if occurrence is already mapped to a different location
        let occMappedQuery = `
          SELECT
            count(1)
          FROM
            experiment.location_occurrence_group
          WHERE
            occurrence_id = ${occurrenceDbId} AND
            is_void = false
        `
        let occMappedResult = await sequelize.query(occMappedQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (occMappedResult[0].count > 0) {
          let errMsg = `Occurrence was already mapped to a location`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Validate if location and occurrence combination already exists
        let locOccCombQuery = `
          SELECT
            count(1)
          FROM
            experiment.location_occurrence_group
          WHERE
            location_id = ${locationDbId} AND
            occurrence_id = ${occurrenceDbId} AND
            is_void = false
        `

        let locOccCombResult = await sequelize.query(locOccCombQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        // if combination already exists
        if (locOccCombResult[0].count > 0) {
          let errMsg = `Location and occurrence combination already exists.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // get order number for the location-occurrence combination
        let orderNoQuery = `
          SELECT
            count(1)+1 as "orderno"
          FROM
            experiment.location_occurrence_group
          WHERE
            location_id = ${locationDbId} AND
            is_void = false
        `

        let orderNoResult = await sequelize.query(orderNoQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        orderNo = orderNoResult[0].orderno

        // get values
        let tempArray = [
          locationDbId,
          occurrenceDbId,
          orderNo,
          personDbId
        ]

        locOccGroupValuesArray.push(tempArray)
      }

      // Create location occurrence group record
      let locOccGroupsQuery = format(`
        INSERT INTO
          experiment.location_occurrence_group (
            location_id,
            occurrence_id,
            order_number,
            creator_id
          )
        VALUES
          %L
        RETURNING id
      `, locOccGroupValuesArray
      )

      let locOccGroups = await sequelize.query(locOccGroupsQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      for (locOccGroups of locOccGroups[0]) {
        locationOccurrenceGroupDbId = locOccGroups.id

        let locOccGroupRecord = {
          locationOccurrenceGroupDbId: locationOccurrenceGroupDbId,
          recordCount: 1,
          href: locationUrlString + '/' + locationOccurrenceGroupDbId
        }

        resultArray.push(locOccGroupRecord)
      }

      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}