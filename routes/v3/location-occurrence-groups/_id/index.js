/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let userValidator = require('../../../../helpers/person/validator.js')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for voiding an existing location occurrence group record
    // DELETE /v3/location-occurrence-groups/:id
    delete: async function (req, res, next) {
         // Retrieve the location occurrence group ID
         let locationOccurrenceGroupDbId = req.params.id

         if(!validator.isInt(locationOccurrenceGroupDbId)) {
             let errMsg = `Invalid format, location occurrence group ID must be an integer.`
             res.send(new errors.BadRequestError(errMsg))
             return
         }
 
         // Retrieve person ID of client from access token
         let personDbId = await tokenHelper.getUserId(req)

         if (personDbId == null) {
           let errMsg = await errorBuilder.getError(req.headers.host, 401002)
           res.send(new errors.BadRequestError(errMsg))
           return
         }

         let isAdmin = await userValidator.isAdmin(personDbId)
 
         let transaction
         try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let locationQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    location.id AS "locationDbId"
                FROM
                    experiment.location_occurrence_group location
                    LEFT JOIN
                        tenant.person creator ON creator.id = location.creator_id
                WHERE
                    location.is_void = FALSE AND
                    location.id = ${locationOccurrenceGroupDbId}
            `

            let location = await sequelize.query(locationQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await location == undefined || await location.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404005)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let deleteLocationQuery = format(`
                UPDATE
                    experiment.location_occurrence_group location
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${locationOccurrenceGroupDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    location.id = ${locationOccurrenceGroupDbId}
            `)

            await sequelize.query(deleteLocationQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { locationOccurrenceGroupDbId: locationOccurrenceGroupDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}