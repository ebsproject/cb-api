/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields', 'distinctOn']
      // Get filter condition
      conditionString = await processQueryHelper.getFilter(req.body, excludedParametersArray)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])

        parameters['distinctOn'] = `"${parameters['distinctOn']}",`
      }
    }
    // Build the base retrieval query
    let placesQuery = null
    // Check if the client specified values for the field/s
    if (parameters['fields']) {
      placesQuery = knex.column(parameters['fields'].split('|'))
      placesQuery += `
        FROM
          master.place place
        LEFT JOIN
          master.user creator ON place.creator_id = creator.id
        LEFT JOIN
          master.user modifier ON place.modifier_id = modifier.id
        WHERE
          place.is_void = FALSE
        ` + addedConditionString +
        ` ORDER BY
          place.id
        `
    } else {
      placesQuery = `
        SELECT
          ${addedDistinctString}
          place.id AS "placeDbId",
          place.abbrev,
          place.name,
          place.display_name AS "displayName",
          place.place_type AS "placeType",
          place.description,
          place.rank,
          geolocation.id AS "geolocationDbId",
          geolocation.latitude,
          geolocation.longitude,
          geolocation.elevation,
          country.name AS "country",
          place.is_active AS "isActive",
          place.remarks,
          place.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.display_name AS "creator",
          place.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.display_name AS "modifier"
        FROM
          master.place place
        LEFT JOIN
          master.user creator ON place.creator_id = creator.id
        LEFT JOIN
          master.user modifier ON place.modifier_id = modifier.id
        LEFT JOIN
          master.institute institute ON place.institute_id = institute.id
        LEFT JOIN
          master.geolocation geolocation ON place.geolocation_id = geolocation.id
        LEFT JOIN
          master.country country ON geolocation.country_id = country.id
        WHERE
          place.is_void = FALSE
        ` + addedConditionString + `
        ORDER BY
          ${parameters['distinctOn']}
          place.id
        `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    placesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      placesQuery,
      conditionString,
      orderString
    )

    // Retrieve the place records
    let places = await sequelize
      .query(placesFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await places == undefined || await places.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get the final count of the place records
      let placesCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          placesQuery,
          conditionString,
          orderString
        )
      
      let placesCount = await sequelize
        .query(placesCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = placesCount[0].count
    }

    res.send(200, {
      rows: places,
      count: count
    })
    return
  }
}