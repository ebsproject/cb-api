/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize, Variable } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/variables
    get: async function (req, res, next) {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''

        let orderString = ''

        // Build query
        let variablesQuery = `
            SELECT
                variable.id AS "variableDbId",
                variable.abbrev,
                variable.label,
                variable.name,
                variable.data_type AS "dataType",
                variable.not_null AS "notNull",
                variable.type,
                variable.status,
                variable.display_name AS "displayName",
                variable.ontology_reference AS "ontologyReference",
                variable.bibliographical_reference AS "bibliographicalReference",
                variable.target_table AS "targetTable",
                variable.target_model AS "targetModel",
                property.id AS "propertyDbId",
                property.name AS property,
                method.id AS "methodDbId",
                method.name AS "methodName",
                method.description AS "methodDescription",
                scale.id AS "scaleDbId",
                scale.name AS "scaleName",
                scale.type AS "scaleType",
                variable.variable_set AS "variableSet",
                variable.synonym,
                variable.remarks,
                variable.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                variable.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier,
                variable.description,
                variable.default_value AS "defaultValue",
                variable.usage,
                variable.data_level AS "dataLevel",
                variable.is_computed AS "isComputed"
            FROM 
                master.variable variable
            LEFT JOIN 
                master.property property ON variable.property_id = property.id
            LEFT JOIN 
                master.method method ON variable.method_id = method.id
            LEFT JOIN 
                master.scale scale ON variable.scale_id = scale.id
            LEFT JOIN 
                tenant.person creator ON variable.creator_id = creator.id
            LEFT JOIN 
                tenant.person modifier ON variable.modifier_id = modifier.id
            WHERE 
                variable.is_void = FALSE  
            ORDER BY
                variable.id  
        `

        // Get filter condition for abbrev only
        if (params.abbrev !== undefined) {
            let parameters = {
                abbrev: params.abbrev
            }

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        variableFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            variablesQuery,
            conditionString,
            orderString
        )

        // Retrieve variables from the database   
        let variables = await sequelize.query(variableFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await variables === undefined || await variables.length < 1) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            // Get count 
            variableCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(variablesQuery, conditionString, orderString)

            variableCount = await sequelize.query(variableCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await variableCount == undefined || await variableCount.length == 0) {
                count = 0
            } else {
                count = variableCount[0].count
            }
        }

        res.send(200, {
            rows: variables,
            count: count
        })
        return
    }
}
