/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { variable } = require('../../../../config/sequelize')
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/variables/:id/methods
    get: async function (req, res, next) {

        // Retrieve the variable ID

        let variableDbId = req.params.id

        if (!validator.isInt(variableDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400081)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let variableQuery = `
                SELECT
                    variable.id AS "variableDbId",
                    variable.abbrev,
                    variable.label,
                    variable.name,
                    variable.data_type AS "dataType",
                    variable.not_null AS "notNull",
                    variable.type,
                    variable.status,
                    variable.display_name AS "displayName",
                    variable.ontology_reference AS "ontologyReference",
                    variable.bibliographical_reference AS "bibliographicalReference",
                    variable.target_table AS "targetTable",
                    variable.target_model AS "targetModel",
                    property.id AS "propertyDbId",
                    property.name AS property,
                    method.id AS "methodDbId",
                    method.name AS "methodName",
                    method.description AS "methodDescription",
                    scale.id AS "scaleDbId",
                    scale.name AS "scaleName",
                    scale.type AS "scaleType",
                    variable.variable_set AS "variableSet",
                    variable.synonym,
                    variable.remarks,
                    variable.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    variable.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier,
                    variable.description,
                    variable.default_value AS "defaultValue",
                    variable.usage,
                    variable.data_level AS "dataLevel",
                    variable.is_computed AS "isComputed"
                FROM 
                    master.variable variable
                LEFT JOIN 
                    master.property property ON variable.property_id = property.id
                LEFT JOIN 
                    master.method method ON variable.method_id = method.id
                LEFT JOIN 
                    master.scale scale ON variable.scale_id = scale.id
                LEFT JOIN 
                    tenant.person creator ON variable.creator_id = creator.id
                LEFT JOIN 
                    tenant.person modifier ON variable.modifier_id = modifier.id
                WHERE 
                    variable.is_void = FALSE
                    AND variable.id = (:variableDbId)
            `;

            // Retrieve variables from the database   
            let variables = await sequelize.query(variableQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    variableDbId: variableDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await variables == undefined || await variables.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404029)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (variable of variables) {
                let methodsQuery = `
                    SELECT
                        method.id AS "methodDbId",
                        method.remarks,
                        formula.formula,
                        method.creation_timestamp AS "creationTimestamp",
                        creator.id AS "creatorDbId",
                        creator.person_name AS creator,
                        method.modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.person_name AS modifier
                    FROM master.method method
                    LEFT JOIN
                        master.formula formula ON method.formula_id = formula.id
                        AND formula.is_void = FALSE
                    LEFT JOIN 
                        tenant.person creator ON method.creator_id = creator.id
                    LEFT JOIN 
                        tenant.person modifier ON method.modifier_id = modifier.id
                    WHERE
                        method.is_void = FALSE
                        AND method.id = (:methodDbId)
                    ORDER BY method.id    
                `

                let methods = await sequelize.query(methodsQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        methodDbId: variable.methodDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                variable["methods"] = methods
            }

            res.send(200, {
                rows: variables
            })
            return
        }
    }
}
