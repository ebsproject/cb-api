/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
  post: async function(req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let addedOrderString = `
      ORDER BY
        scale_value.id
    `
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let parameters = {}
    parameters['distinctOn'] = ''

    let variableDbId = req.params.id

    if (!validator.isInt(variableDbId)) {
      let errMsg = 'Invalid request, variableDbId must be an integer.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let variableQuery = `
      SELECT
        variable.id AS "variableDbId",
        variable.abbrev,
        variable.label,
        variable.type,
        variable.name,
        variable.data_type AS "dataType",
        variable.data_level AS "dataLevel",
        variable.status,
        variable.scale_id AS "scaleDbId"
      FROM
        master.variable variable
      WHERE
        variable.is_void = FALSE AND
        variable.id = ${variableDbId}
    `

    let variable = await sequelize
      .query(variableQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })
    
    if (await variable == undefined || await variable.length < 1) {
      let errMsg = 'Resource not found, the variable you have requested for does not exist.'
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    let scaleDbId = variable[0].scaleDbId

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(
            parameters['distinctOn']
          )
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
      // Set the parameters that will be excluded for filtering
      let excludedParametersArray = ['fields', 'distinctOn']
      // Get the filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray,
      )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Build the scale value retrieval query
    let scaleValuesQuery = null
    // Check if the client specified values for fields
    if (parameters['fields'] != null) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters['fields'],
        )
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        scaleValuesQuery = knex.select(selectString)
      } else {
        scaleValuesQuery = knex.column(parameters['fields'].split('|'))
      }

      scaleValuesQuery += `
        FROM
          master.scale_value scale_value
        LEFT JOIN
          tenant.person creator ON scale_value.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON scale_value.modifier_id = modifier.id
        WHERE
          scale_value.is_void = FALSE AND
          scale_value.scale_id = ${scaleDbId} AND
          (scale_value.scale_value_status is null OR scale_value.scale_value_status != 'hide')
        ` + addedOrderString
    } else {
      scaleValuesQuery = `
        SELECT
          ${addedDistinctString}
          scale_value.id AS "scaleValueDbId",
          scale_value.abbrev,
          scale_value.value,
          scale_value.order_number AS "orderNumber",
          scale_value.description,
          scale_value.display_name AS "displayName",
          scale_value.scale_value_status AS "scaleValueStatus",
          scale_value.remarks,
          scale_value.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          scale_value.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          master.scale_value scale_value
        LEFT JOIN
          tenant.person creator ON scale_value.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON scale_value.modifier_id = modifier.id
        WHERE
          scale_value.is_void = FALSE AND
          scale_value.scale_id = ${scaleDbId} AND
          (scale_value.scale_value_status is null OR scale_value.scale_value_status != 'hide')
        ` + addedOrderString
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    let scaleValuesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      scaleValuesQuery,
      conditionString,
      orderString,
    )

    let scaleValues = await sequelize
      .query(scaleValuesFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    variable[0]['scaleValues'] = scaleValues

    res.send(200, {
      rows: variable
    })
    return
  }
}