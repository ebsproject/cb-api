/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY "experimentDesign".id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn'
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray,
        )
        
      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }
    // Build the base retrieval query
    let experimentDesignQuery = null
    // Check if the client specified values for fields
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])
        
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        experimentDesignQuery = knex.select(selectString)
      } else {
        experimentDesignQuery = knex.column(parameters['fields'].split('|'))
      }

      experimentDesignQuery += `
        FROM
          experiment.experiment_design "experimentDesign"
        LEFT JOIN
          experiment.occurrence occurrence ON "experimentDesign".occurrence_id = occurrence.id
        LEFT JOIN
          experiment.plot plot ON "experimentDesign".plot_id = plot.id
        LEFT JOIN
          tenant.person creator ON "experimentDesign".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "experimentDesign".modifier_id = modifier.id
        WHERE
          "experimentDesign".is_void = FALSE
        ${addedOrderString}
      `
    } else {
      experimentDesignQuery = `
        SELECT
          "experimentDesign".id AS "experimentDesignDbId",
          "experimentDesign".block_type AS "experimentDesignBlockType",
          "experimentDesign".block_value AS "experimentDesignBlockValue",
          "experimentDesign".block_level_number AS "experimentDesignBlockLevelNo",
          "experimentDesign".block_name AS "experimentDesignBlockName",
          occurrence.id AS "occurrenceDbId",
          occurrence.occurrence_code AS "occurrenceCode",
          occurrence.occurrence_name AS "occurrenceName",
          occurrence.occurrence_status AS "occurrenceStatus",
          occurrence.experiment_id AS "experimentDbId",
          occurrence.geospatial_object_id AS "geospatialObjectId",
          geospatialObject.geospatial_object_name AS "geospatialObjectName",
          geospatialObject.geospatial_object_type AS "geospatialObjectType",
          plot.id AS "plotDbId",
          plot.location_id AS "locationDbId",
          (
            SELECT
              l.location_name
            FROM
              experiment.location l
            WHERE
              l.id = plot.location_id
          ) AS "locationName",
          plot.plot_code AS "plotCode",
          plot.plot_number AS "plotNo",
          plot.entry_id AS "entryDbId",
          "experimentDesign".creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          "experimentDesign".modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          experiment.experiment_design "experimentDesign"
        LEFT JOIN
          experiment.occurrence occurrence ON "experimentDesign".occurrence_id = occurrence.id
        LEFT JOIN
          place.geospatial_object geospatialObject ON occurrence.geospatial_object_id = geospatialObject.id
        LEFT JOIN
          experiment.plot plot ON "experimentDesign".plot_id = plot.id
        LEFT JOIN
          tenant.person creator ON "experimentDesign".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "experimentDesign".modifier_id = modifier.id
        WHERE
          "experimentDesign".is_void = FALSE
        ${addedOrderString}
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final SQL query
    let experimentDesignFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        experimentDesignQuery,
        conditionString,
        orderString,
      )

    let experimentDesign = await sequelize
      .query(experimentDesignFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await experimentDesign == undefined || await experimentDesign.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let experimentDesignCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        experimentDesignQuery,
        conditionString,
        orderString,
        addedDistinctString
      )
      
    let experimentDesignCount = await sequelize
      .query(experimentDesignCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
      })

    count = experimentDesignCount[0].count

    res.send(200, {
      rows: experimentDesign,
      count: count
    })
    return
  }
}