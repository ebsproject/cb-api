/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving all places
    // GET /v3/places
    get: async function (req, res, next) {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''

        let orderString = ''

        // Build query
        let placesQuery = `
            SELECT
                place.id AS "placeDbId",
                place.abbrev,
                place.name,
                place.display_name AS "displayName",
                place.description,
                place.place_type AS "placeType",
                place.institute_id AS "instituteDbId",
                place.geolocation_id AS "geolocationDbId",
                place.rank,
                place.remarks,
                place.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.display_name AS creator,
                place.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.display_name AS modifier   
            FROM 
                master.place place
            LEFT JOIN 
                master.user creator ON place.creator_id = creator.id
            LEFT JOIN 
                master.user modifier ON place.modifier_id = modifier.id
            WHERE
                place.is_void = FALSE
            ORDER BY
                place.id
        `

        // Get filter condition for abbrev only
        if (params.abbrev !== undefined) {
            let parameters = {
                abbrev: params.abbrev
            }

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        placeFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            placesQuery,
            conditionString,
            orderString
        )

        // Retrieve places from the database   
        let places = await sequelize.query(placeFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await places == undefined || await places.length < 1) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            // Get count 
            placeCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(placesQuery, conditionString, orderString)

            placeCount = await sequelize.query(placeCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = placeCount[0].count
        }

        res.send(200, {
            rows: places,
            count: count
        })
        return
    }
}
