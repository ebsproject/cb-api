/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving a specific place
    // GET /v3/places/:id
    get: async function (req, res, next) {

        // Retrieve the place ID

        let placeDbId = req.params.id

        if (!validator.isInt(placeDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400037)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let placesQuery = `
                SELECT
                    place.id AS "placeDbId",
                    place.abbrev,
                    place.name,
                    place.display_name AS "displayName",
                    place.description,
                    place.place_type AS "placeType",
                    place.institute_id AS "instituteDbId",
                    place.geolocation_id AS "geolocationDbId",
                    place.rank,
                    place.remarks,
                    place.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    place.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier
                FROM 
                    master.place place
                LEFT JOIN 
                    master.user creator ON place.creator_id = creator.id
                LEFT JOIN 
                    master.user modifier ON place.modifier_id = modifier.id
                WHERE place.id = (:placeDbId)
                    AND place.is_void = FALSE 
            `

            // Retrieve places from the database   
            let places = await sequelize.query(placesQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    placeDbId: placeDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await places == undefined || await places.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404011)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: places
            })
            return
        }
    },
}
