/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let addedDistinctString = ''

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', 'distinctOn']

            // Get filter condition
            conditionString = await processQueryHelper.getFilter(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            // Get distinctOn
            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }
        }

        // Build the base retrieval query
        let entryListQuery = null

        // Check if the client specified values for the field/s
        if (parameters['fields']) {
            // If paramters has distinctOn
            if(parameters['distinctOn']){
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(
                        parameters['fields']
                    )
                
                entryListQuery = knex.select(fieldsString)
            }
            else{
                entryListQuery = knex.column(parameters['fields'].split('|'))
            }

            entryListQuery += `
            FROM
                experiment.entry_list "entryList"
            LEFT JOIN
                tenant.person creator ON "entryList".creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON "entryList".modifier_id = modifier.id
            LEFT JOIN
                tenant.season ON "entryList".season_id =  season.id
            LEFT JOIN
                place.geospatial_object ON "entryList".site_id = geospatial_object.id
            WHERE
                "entryList".is_void = FALSE
            ` + addedConditionString +
            `ORDER BY
                ${parameters['distinctOn']}
                "entryList".id
            `
        } else {
            entryListQuery = `
                SELECT
                    ${addedDistinctString}
                    entryList.id AS "entryListDbId",
                    entryList.entry_list_code AS "entryListCode",
                    entryList.entry_list_name AS "entryListName",
                    entryList.description,
                    entryList.entry_list_status "entryListStatus",
                    entryList.experiment_id as "experimentDbId",
                    experiment.experiment_type as "experimentType",
                    entryList.entry_list_type as "entryListType",
                    entryList.experiment_year AS "experimentYear",
                    entryList.season_id AS "seasonDbId",
                    season.season_name AS "seasonName",
                    season.season_code AS "seasonCode",
                    entryList.site_id AS "siteDbId",
                    site.geospatial_object_name AS "geospatialObjectName",
                    site.geospatial_object_code AS "siteCode",
                    entryList.cross_count AS "crossCount",
                    entryList.crop_id AS "cropDbId",
                    entryList.program_id AS "programDbId",
                    creator.id AS "creatorDbId",
                    entryList.creation_timestamp AS "creationTimestamp",
                    creator.person_name AS "creator",
                    entryList.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    experiment.entry_list entryList
                LEFT JOIN
                    tenant.person creator ON entryList.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON entryList.modifier_id = modifier.id
                LEFT JOIN
                    tenant.season ON entryList.season_id = season.id
                LEFT JOIN
                    place.geospatial_object site ON entryList.site_id = site.id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = entryList.experiment_id
                WHERE
                    entryList.is_void = FALSE
                ` + addedConditionString + `
                ORDER BY
                    ${parameters['distinctOn']}
                    entryList.id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                return
            }
        }

        // Generate the final SQL query
        entryListFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            entryListQuery,
            conditionString,
            orderString
        )

        // Retrieve the entry lists records
        let entryLists = await sequelize
            .query(entryListFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If query returns error, return error
        if (await entryLists === undefined ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            
            return
        }

        // If query returns empty, return empty
        if (await entryLists.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }
        
        // Get the final count of the entry lists records
        entryListCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(entryListQuery, conditionString,
            orderString)
        let entryListCount = await sequelize
            .query(entryListCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        count = entryListCount[0].count

        res.send(200, {
            rows: entryLists,
            count: count
        })
        return
    }
}