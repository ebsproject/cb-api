/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let errors = require('restify-errors')
let logger = require('../../../helpers/logger/index.js')
const axios = require('axios')
const qs = require('qs')
let base64 = require('base-64')
let { createDecoder } = require('fast-jwt')

module.exports = {

    get: async function (req, res, next) {

        let authorizationUrl = process.env.CBAPI_AUTH_URL
        let clientId = process.env.CBAPI_CLIENT_ID
        let clientSecret = process.env.CBAPI_CLIENT_SECRET
        let redirectUri = process.env.CBAPI_REDIRECT_URI

        res.writeHead(301, {
            Location: `${authorizationUrl}?scope=openid&response_type=code&client_id=${clientId}&client_secret=${clientSecret}&redirect_uri=${redirectUri}`
        })
        res.end()

        return
    },
    post: async function (req, res, next) {

        // Check if request body exists
        if (req.body == undefined || req.body == null) {
            let errMsg = 'Invalid request, you must provide a request body.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let clientId = req.body.clientId
        let clientSecret = req.body.clientSecret
        let grantType = req.body.grantType
        let refreshToken = req.body.refreshToken
        let username = req.body.username
        let password = req.body.password

        // Define URL and headers
        let url = process.env.CBAPI_TOKEN_URL
        let authHeader = base64.encode(`${clientId}:${clientSecret}`)
        let headers = {
            'Authorization': `Basic ${authHeader}`,
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        // Grant type: password
        if (grantType === 'password') {
            // Check if username and password are provided
            if (clientId == undefined ||
                clientSecret == undefined ||
                grantType == undefined ||
                username == undefined || password == undefined
            ) {
                let errMsg = 'Invalid request for password grant type, make sure you have provided the following required parameters: client ID, client secret, grant type, username and password'
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            let requestData = {
                'username': username,
                'password': password,
                'grant_type': grantType,
                'scope': 'openid'
            }

            let response = await axios({
                method: 'post',
                url: url,
                data: qs.stringify(requestData),
                headers: headers
            }).catch(async err => {
                logger.logMessage(__filename, 'Error in axios: ' + err.response.status + ' '
                + err.response.statusText +  ' ' + JSON.stringify(err.response.data), 'error')
                return
            })

            if(response == undefined) return

            if (response.status == 400) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401001)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        
            let body = response.data
            let token = body.access_token
            refreshToken = body.refresh_token

            let decode = createDecoder()
            let payload = decode(token)
            let iat = payload.iat
            let exp = payload.exp

            res.send(200, {
                iat: iat,
                exp: exp,
                token: token,
                refreshToken: refreshToken
            })

            return
        }
        // Grant type: *others*
        else {
            // Check if the user provided all the body parameters
            if (
                clientId == undefined ||
                clientSecret == undefined ||
                grantType == undefined ||
                refreshToken == undefined
            ) {
                let errMsg = 'Invalid request, make sure you have provided the following required parameters: client ID, client secret, grant type, and refreshToken'
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            let requestData = {
                'refresh_token': refreshToken,
                'grant_type': grantType
            }

            let response = await axios({
                method: 'post',
                url: url,
                data: qs.stringify(requestData),
                headers: headers
            }).catch(async err => {
                logger.logMessage(__filename, 'Error in axios: ' + err.response.status + ' '
                + err.response.statusText +  ' ' + JSON.stringify(err.response.data), 'error')
                return
            })

            if(response == undefined) return

            if (response.status == 400) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401001)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        
            let body = response.data
            let token = body.id_token
            let newRefreshToken = refreshToken

            let decode = createDecoder()
            let payload = decode(token)
            let email = (payload['http://wso2.org/claims/emailaddress'] != undefined) ?
                payload['http://wso2.org/claims/emailaddress'] : payload['email'];
            let iat = payload.iat
            let exp = payload.exp

            if (email == undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401002)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }

            // Check if authenticated user has access rights
            let personQuery = `
                SELECT 
                    person.id
                FROM
                    tenant.person person
                WHERE
                    LOWER(person.email) = LOWER($$${email}$$) AND
                    person.is_void = FALSE AND
                    person.is_active = TRUE
            `

            let person = await sequelize
                .query(personQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await person === undefined || await person.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401002)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }

            res.send(200, {
                iat: iat,
                exp: exp,
                token: token,
                refreshToken: newRefreshToken,
            })

            return
        }
    }
}
