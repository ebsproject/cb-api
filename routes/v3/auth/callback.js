/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let errors = require('restify-errors')
const axios = require('axios')
const qs = require('qs')
const { createDecoder } = require('fast-jwt')

module.exports = {

    get: async function (req, res, next) {
        try {
            if (req.params.code == null) {
                let errMsg = errorBuilder.getError(req.headers.host, 400001)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            let authorizationCode = req.params.code

            let clientId = (req.params.client_id == null) ? process.env.CBAPI_CLIENT_ID : req.params.client_id

            let clientSecret = (req.params.client_secret == null) ? process.env.CBAPI_CLIENT_SECRET : req.params.client_secret

            let redirectUri = (req.params.redirect_uri == null) ? process.env.CBAPI_REDIRECT_URI : req.params.redirect_uri

            let tokenUrl = process.env.CBAPI_TOKEN_URL

            let requestData = {
                client_id: clientId,
                client_secret: clientSecret,
                redirect_uri: redirectUri,
                code: authorizationCode,
                grant_type: 'authorization_code'
            }

            let headers = {
                'Content-Type': 'application/x-www-form-urlencoded',
            }

            let response = await axios({
                method: 'post',
                url: tokenUrl,
                data: qs.stringify({
                    client_id: clientId,
                    client_secret: clientSecret,
                    redirect_uri: redirectUri,
                    code: authorizationCode,
                    grant_type: "authorization_code"
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            if (response.status == 400) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401001)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            else {
                let body = response.data
                let token = body.id_token
                let refreshToken = body.refresh_token

                const decode = createDecoder()
                let payload = decode(token)
                let email = (payload['http://wso2.org/claims/emailaddress'] != undefined) ?
                    payload['http://wso2.org/claims/emailaddress'] : payload['email'];
                let iat = payload.iat
                let exp = payload.exp

                // Check if email is existing
                if (email == undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401002)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }

                // Check if authenticated user has access rights
                let personQuery = `
                    SELECT 
                        person.id
                    FROM
                        tenant.person person
                    WHERE
                        lower(person.email) = lower($$${email}$$) AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(personQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                if (await person === undefined || await person.length < 1) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401002)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                else {
                    res.send(200, {
                        iat: iat,
                        exp: exp,
                        token: token,
                        refreshToken: refreshToken,
                    })
                    return
                }
            }
        } catch (e) {
            console.log(e)
            let errMsg = errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
