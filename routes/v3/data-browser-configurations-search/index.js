/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

//Import Dependencies
let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
module.exports = {  
  // POST call /v3/data-browser-configurations-search
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let parameters = {}
    let userDbId = null
    let dataBrowserDbId = null
    let type = null

    if (req.body != undefined) {
        if (req.body.fields != undefined) {        
          parameters['fields'] = req.body.fields
        }
        // Set the columns to be excluded in parameters for filtering
        let excludedParametersArray = ['fields']
        // Get filter condition
        conditionString = await processQueryHelper.getFilter(req.body, excludedParametersArray)

        if (conditionString.includes('invalid')) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400022)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
    }
  
    //check if userDbId is given through req.body      
    userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Build the base retrieval query
    let configsQuery = null
    // Check if the client specified values for the field/s
    if (parameters['fields'] != null) {
      configsQuery = knex.column(parameters['fields'].split('|'))
      configsQuery += `
      FROM
        platform.user_data_browser_configuration configs
      LEFT JOIN
        tenant.person "user" ON "user".id = configs.user_id AND
        "user".is_void = FALSE
      LEFT JOIN
        tenant.person modifier ON modifier.id = configs.modifier_id
      LEFT JOIN
        tenant.person creator ON creator.id = configs.creator_id
      WHERE
        user_id = $$${userDbId}$$
        and configs.is_void = FALSE
      ORDER BY
        configs.id
        `
    } else {
      configsQuery = `
        SELECT
          configs.id AS "configDbId",
          configs.user_id AS "userDbId",
          configs.data_browser_id AS "dataBrowserDbId",
          configs.name,
          configs.data,
          configs.type AS "type"
        FROM
          platform.user_data_browser_configuration configs
        LEFT JOIN
          tenant.person "user" ON "user".id = configs.user_id AND
          "user".is_void = FALSE
        LEFT JOIN
          tenant.person modifier ON modifier.id = configs.modifier_id
        LEFT JOIN
          tenant.person creator ON creator.id = configs.creator_id
        WHERE
          configs.user_id = ${userDbId}
          and configs.is_void = FALSE
        ORDER BY
          configs.id
      `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    
    // Generate the final SQL query
    configsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      configsQuery,
      conditionString,
      orderString
    )
    
    // Retrieve the user_data_browser_configurations records
    let configs = await sequelize
      .query(configsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await configs == undefined || await configs.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get the final count of the place records
      let configsCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          configsQuery,
          conditionString,
          orderString
        )
      
      let configsCount = await sequelize
        .query(configsCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = configsCount[0].count
    }
    
    res.send(200, {
      rows: configs,
      count: count
    })
    return
  }
}
