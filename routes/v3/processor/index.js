/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let amqp = require('amqplib')
let errors = require('restify-errors')
let forwarded = require('forwarded-for')
let tokenHelper = require('../../../helpers/auth/token')
let processorHelper = require('../../../helpers/processor/index.js')
let logger = require('../../../helpers/logger/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let validator = require('validator')
let entryHelper = require('../../../helpers/entries/index.js')
let { sequelize } = require('../../../config/sequelize')

module.exports = {

    // POST /v3/processor
    post: async function (req, res, next) {
        // Set worker name
        let workerName = 'ExecuteProcessor'

        // Set defaults
        let httpMethod = null
        let endpoint = null
        let dbIds = null
        let requestData = null
        let dependents = null
        let prerequisite = null
        let isDraftedExperiment = null
        let cascadePersistent = null

        let description = null
        let entity = null
        let entityDbId = null
        let endpointEntity = null
        let application = null
        let tokenObject = null

        let bulkUpdateArray = []
        let dbIdArray = []

        let token = req.headers.authorization
        let validHttpMethods = ["DELETE", "PUT", "POST"]
        let allowedCascade = ["crosses-cross-parents", "entries-entry-data",
            "plots-planting-instructions", "plots-plot-data", "plots-experiment-designs",
            "germplasm-packages", "germplasm-seed-relations", "germplasm-seed-attributes",
            "germplasm-seeds", "germplasm-germplasm-relations", "germplasm-germplasm-attributes", "germplasm-family-members",
            "germplasm-germplasm-names", "seeds-seed-attributes", "seeds-seed-relations", "seeds-packages"]
        let allowedPrerequisite = ["members-packages-search", "list-members-members-search"]

        // Checks if there is a request body
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Get personDbId
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if (personDbId === undefined) {
            return
        }
        // If personDbId is null, return 401: User not found error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Checks if user has put required parameters
        if (
            req.body.httpMethod === undefined ||
            req.body.endpoint === undefined ||
            req.body.entity === undefined ||
            req.body.endpointEntity === undefined ||
            req.body.application === undefined ||
            req.body.tokenObject === undefined
        ) {
            let errMsg = 'Required parameters are missing. Ensure that the httpMethod, endpoint, entity, endpointEntity, and application fields are not empty.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Checks if tokenObject contains token and refreshToken
        if (
            req.body.tokenObject.token === undefined ||
            req.body.tokenObject.refreshToken === undefined
        ) {
            let errMsg = 'Invalid tokenObject. Ensure that token and refreshToken are specified.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Checks if token and refreshToken are of data type string
        if (
            typeof req.body.tokenObject.token !== 'string' ||
            typeof req.body.tokenObject.refreshToken !== 'string'
        ) {
            let errMsg = 'Invalid tokenObject. Ensure that token and refreshToken are of type string.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        entity = req.body.entity
        endpointEntity = req.body.endpointEntity
        application = req.body.application
        tokenObject = req.body.tokenObject
        dbIds = (req.body.dbIds != undefined) ? req.body.dbIds : null
        entityDbId = (req.body.entityDbId != undefined) ? req.body.entityDbId : null
        description = (req.body.description != undefined) ? req.body.description : null

        // Checks for POST -search calls
        if (req.body.endpoint != undefined) {
            endpoint = req.body.endpoint

            if (endpoint.includes('-search')) {
                let errMsg = 'Invalid request, background processing is not allowed in POST -search endpoints.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        if (req.body.dependents != undefined) {
            dependents = req.body.dependents
            cascadePersistent = (req.body.cascadePersistent != undefined) ? req.body.cascadePersistent : false

            if (!Array.isArray(dependents)) {
                let errMsg = 'Invalid request, ensure that the dependents field is an array.'
                res.send(new errors.BadRequestError(errMsg))
                return
            } else {
                for (var dependent of dependents) {
                    let endpointDependencyStr = endpoint.slice(3) + '-' + dependent
                    if (!allowedCascade.includes(endpointDependencyStr)) {
                        let errMsg = 'You have provided an invalid dependency.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }
            }
        }

        if (req.body.httpMethod != undefined) {
            httpMethod = req.body.httpMethod
            httpMethod = httpMethod.toUpperCase()

            if (!validHttpMethods.includes(httpMethod)) {
                let errMsg = 'You have provided an invalid HTTP method.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        if ((httpMethod == "DELETE" || httpMethod == "PUT")) {
            requestData = req.body.requestData != undefined ? req.body.requestData : [] 
            if ((dbIds == null || dbIds.length == 0) && requestData.processName == undefined) {
                let errMsg = 'Invalid request, ensure that the dbIds field is not empty.'
                res.send(new errors.BadRequestError(errMsg))
                return
            } else {
                if(dbIds != null && dbIds.length != 0){
                    dbIdArray = dbIds.split('|').map(item => item.trim())
                }
                if(requestData.processName == undefined){
                    if (endpoint === 'v3/experiments' && dbIdArray.length === 1 && req.body.isDraftedExperiment == true) {
                        /** To uncomment once experiments have the experiment_status drafted
                        let tempIsDraftedExperiment = await processorHelper.isDraftedExperiment(dbIdArray[0])
                        if (tempIsDraftedExperiment == null) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 404008)
                            res.send(new errors.NotFoundError(errMsg))
                            return
                        }
                        
                        if (!tempIsDraftedExperiment) {
                            let errMsg = 'Invalid request, experiment is not a draft.'
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                        */
                        isDraftedExperiment = true
                    }
                }
            }
        }

        // Checks if requestData field is not empty and if it is an object
        if (req.body.requestData !== undefined) {
            requestData = req.body.requestData

            if (httpMethod == "PUT" || httpMethod == "POST") {
                if (requestData === undefined) {
                    let errMsg = 'Invalid request, ensure that the requestData field is not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (typeof (requestData) != 'object') {
                    let errMsg = 'Invalid format, ensure that the requestData field is an object.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (httpMethod === "PUT") {
                if (dependents != null) {
                    let errMsg = 'Invalid request, dependents should be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if this bulk update will have multiple values
                if (requestData.multipleValues !== undefined && requestData.multipleValues == true) {
                    if (requestData.data == undefined || requestData.data.length == 0) {
                        let errMsg = 'Invalid request, ensure that the data field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    bulkUpdateArray = requestData.data
                    // Checks if # of objects in requestData.data = # of dbIds
                    if (bulkUpdateArray.length != dbIdArray.length) {
                        let errMsg = `Invalid request, ensure that the number of objects passed in the data array of the requestData field is equal to the number of dbIds.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    requestData = bulkUpdateArray
                }

                if(requestData.processName == 'update-filter-records'){

                    workerName = 'GenericProcessor'

                    if (
                        requestData.searchPath == undefined && 
                        requestData.filters == undefined && 
                        requestData.updatePath == undefined && 
                        requestData.updateField == undefined && 
                        requestData.updateValue == undefined && 
                        requestData.idName == undefined
                        
                    ) {
                        let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if(requestData.processName == 'reorder-all-entries'){
                    workerName = 'ExperimentCreationProcessor'

                    if (
                        requestData.experimentDbId == undefined && 
                        requestData.sort == undefined
                        
                    ) {
                        let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if(requestData.processName == 'update-plant_inst'){
                    workerName = 'ExperimentCreationProcessor'

                    if (
                        requestData.experimentDbId == undefined && 
                        requestData.occurrenceIds == undefined
                        
                    ) {
                        let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }
            }

            if (httpMethod === "POST") {
                if(requestData.processName != undefined){
                    let validateQuery = ``
                    let validateCount = 0

                    if(requestData.processName == 'create-entry-records'){
                        workerName = 'CreateEntryRecords'
                        
                        if (
                            requestData.entity == undefined && 
                            requestData.entityId == undefined && 
                            requestData.defaultValues == undefined && 
                            requestData.experimentDbId == undefined
                            
                        ) {
                            let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        if (requestData.entityId !== undefined) {
                            let requestEntityId = requestData.entityId

                            if (!validator.isInt(requestEntityId)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }
                        }

                        if (requestData.experimentDbId !== undefined) {
                            let experimentDbId = requestData.experimentDbId

                            if (!validator.isInt(experimentDbId)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            // Validate experiment ID
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if experiment is existing return 1
                                    SELECT 
                                        count(1)
                                    FROM 
                                        experiment.experiment experiment
                                    WHERE 
                                        experiment.is_void = FALSE AND
                                        experiment.id = ${experimentDbId}
                                )
                            `
                            validateCount += 1
                        }

                        /** Validation of input in database */
                        // count must be equal to validateCount if all conditions are met
                        let checkInputQuery = `
                            SELECT
                                ${validateQuery}
                            AS count
                        `

                        let inputCount = await sequelize.query(checkInputQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                        if (inputCount[0].count != validateCount) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                    } else if(requestData.processName == 'create-pa-records'){
                        workerName = 'CreateExperimentPARecords'

                        if (
                            requestData.blockIds == undefined && 
                            requestData.experimentDbId == undefined
                            
                        ) {
                            let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        if (requestData.experimentDbId !== undefined) {
                            let experimentDbId = requestData.experimentDbId

                            if (!validator.isInt(experimentDbId)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            // Validate experiment ID
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if experiment is existing return 1
                                    SELECT 
                                        count(1)
                                    FROM 
                                        experiment.experiment experiment
                                    WHERE 
                                        experiment.is_void = FALSE AND
                                        experiment.id = ${experimentDbId}
                                )
                            `
                            validateCount += 1
                        }

                        /** Validation of input in database */
                        // count must be equal to validateCount if all conditions are met
                        let checkInputQuery = `
                            SELECT
                                ${validateQuery}
                            AS count
                        `

                        let inputCount = await sequelize.query(checkInputQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        
                        if (inputCount[0].count != validateCount) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                    }else if(requestData.processName == 'create-occurrence-records'){
                        workerName = 'CreateExperimentOccurrenceRecords'

                        if (
                            requestData.records == undefined &&
                            requestData.experimentDbId == undefined

                        ) {
                            let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        if (requestData.experimentDbId !== undefined) {
                            let experimentDbId = requestData.experimentDbId

                            if (!validator.isInt(experimentDbId)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            // Validate experiment ID
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if experiment is existing return 1
                                    SELECT 
                                        count(1)
                                    FROM 
                                        experiment.experiment experiment
                                    WHERE 
                                        experiment.is_void = FALSE AND
                                        experiment.id = ${experimentDbId}
                                )
                            `
                            validateCount += 1
                        }

                        /** Validation of input in database */
                        // count must be equal to validateCount if all conditions are met
                        let checkInputQuery = `
                            SELECT
                                ${validateQuery}
                            AS count
                        `

                        let inputCount = await sequelize.query(checkInputQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                        if (inputCount[0].count != validateCount) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    } else {
                        workerName = 'ExperimentCreationProcessor'

                        if (
                            requestData.experimentDbId == undefined && 
                            requestData.processName == undefined
                        ) {
                            let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        if (requestData.experimentDbId !== undefined) {
                            experimentDbId = requestData.experimentDbId

                            if (!validator.isInt(experimentDbId)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            // Validate experiment ID
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if experiment is existing return 1
                                    SELECT 
                                        count(1)
                                    FROM 
                                        experiment.experiment experiment
                                    WHERE 
                                        experiment.is_void = FALSE AND
                                        experiment.id = ${experimentDbId}
                                )
                            `
                            validateCount += 1
                        }

                        /** Validation of input in database */
                        // count must be equal to validateCount if all conditions are met
                        let checkInputQuery = `
                            SELECT
                                ${validateQuery}
                            AS count
                        `

                        let inputCount = await sequelize.query(checkInputQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                        if (inputCount[0].count != validateCount) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }
                } else if (requestData.copyExperimentDbId == undefined) {

                    if (dbIds != null) {
                        let errMsg = 'Invalid request, dbIds should be empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (requestData.prerequisite == undefined && requestData.records == undefined) {
                        let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // for cascade create: checks if there is specified request bodies for child entities
                    if (dependents != null) {
                        let records = requestData.records
                        for (var record of records) {
                            for (var dependent of dependents) {
                                if (record[dependent] == undefined || record[dependent].length == 0) {
                                    let errMsg = `Invalid request, ensure that all ${dependent} fields in records array is not empty.`
                                    res.send(new errors.BadRequestError(errMsg))
                                    return
                                }
                            }
                        }
                    }

                    // for retrieving columns through POST search calls and adding it in bulk create
                    if (requestData.prerequisite != undefined) {
                        prerequisite = requestData.prerequisite

                        if (prerequisite.endpoint == undefined || prerequisite.data == undefined || prerequisite.mapping == undefined) {
                            let errMsg = "Invalid request, ensure that the endpoint, data, and mapping fields in prerequisite is not empty."
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        let prerequisiteEndpointString = prerequisite.endpoint
                        let endpointString = endpoint.split('/')
                        endpointString = endpointString[endpointString.length - 1]

                        if (!allowedPrerequisite.includes(`${endpointString}-${prerequisiteEndpointString}`)) {
                            let errMsg = "Prerequisite endpoint is not allowed."
                            res.send(new errors.ForbiddenError(errMsg))
                            return
                        }
                    }
                } else {
                    let validateQuery = ``
                    let validateCount = 0

                    workerName = 'CopyExperiment'

                    if (requestData.experimentDbId == undefined && requestData.copyExperimentDbId == undefined && requestData.copyInformation == undefined) {
                        let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (requestData.experimentDbId !== undefined) {
                        experimentDbId = requestData.experimentDbId

                        if (!validator.isInt(experimentDbId)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        // Validate experiment ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if experiment is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    experiment.experiment experiment
                                WHERE 
                                    experiment.is_void = FALSE AND
                                    experiment.id = ${experimentDbId}
                            )
                        `
                        validateCount += 1
                    }

                    if (requestData.copyExperimentDbId !== undefined) {
                        let copyExperimentDbId = requestData.copyExperimentDbId

                        if (!validator.isInt(copyExperimentDbId)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        // Validate experiment ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if experiment is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    experiment.experiment experiment
                                WHERE 
                                    experiment.is_void = FALSE AND
                                    experiment.id = ${copyExperimentDbId}
                            )
                        `
                        validateCount += 1
                    }

                    if (!Array.isArray(requestData.copyInformation)) {
                        let errMsg = 'Invalid request, ensure that the copyInformation field is an array.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    /** Validation of input in database */
                    // count must be equal to validateCount if all conditions are met
                    let checkInputQuery = `
                        SELECT
                            ${validateQuery}
                        AS count
                    `

                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (inputCount[0].count != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                }
            }

            // for retrieving dbIds through POST search calls before deleting records 
            if (httpMethod === "DELETE") {
                if(requestData.processName == undefined){
                    if (validator.isInt(dbIdArray[0]) && requestData == null) {
                        let errMsg = "Invalid request, ensure that dbIds is a string and that the prerequisite field is not empty."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (requestData.prerequisite != undefined) {
                        prerequisite = requestData.prerequisite

                        if (prerequisite.endpoint == undefined) {
                            let errMsg = "Invalid request, ensure that the endpoint field in prerequisite is not empty."
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        let prerequisiteEndpointString = prerequisite.endpoint
                        let endpointString = endpoint.split('/')
                        endpointString = endpointString[endpointString.length - 1]
                        prerequisiteEndpointString = prerequisiteEndpointString.split('/')
                        prerequisiteEndpointString = prerequisiteEndpointString[prerequisiteEndpointString.length - 1]

                        if (!allowedPrerequisite.includes(`${endpointString}-${prerequisiteEndpointString}`)) {
                            let errMsg = "Prerequisite endpoint is not allowed."
                            res.send(new errors.ForbiddenError(errMsg))
                            return
                        }
                    }
                }   else if(requestData.processName == 'delete-design-records'){
                    workerName = 'ExperimentCreationProcessor'
                    
                    if (
                        requestData.experimentDbId == undefined && 
                        requestData.occurrenceIds == undefined
                        
                    ) {
                        let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }  else if(requestData.processName == 'delete-occurrences'){
                    workerName = 'ExperimentCreationProcessor'
                    
                    if (
                        requestData.experimentDbId == undefined && 
                        requestData.occurrenceIds == undefined
                        
                    ) {
                        let errMsg = 'Invalid request, ensure that the records in requestData field is not empty.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                } 
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set URL for getData
        let urlString = (isSecure ? `https` : `http`)
            + `://`
            + req.headers.host
            + `/v3/background-jobs`

        let backgroundJobParameters = {
            "records": [
                {
                    "workerName": workerName,
                    "description": description,
                    "entity": entity,
                    "entityDbId": entityDbId,
                    "endpointEntity": endpointEntity,
                    "application": application,
                    "method": httpMethod,
                    "jobStatus": "IN_QUEUE",
                    "startTime": new Date().toDateString() + ' ' + new Date().toLocaleTimeString(),
                    "creatorDbId": `${personDbId}`
                }
            ],
            "userId": `${personDbId}`
        }

        try {
            // one background_job record = one bulk operation
            let backgroundJob = await processorHelper
                .getBackgroundJobData(
                    "POST",
                    urlString,
                    token,
                    JSON.stringify(backgroundJobParameters))

            if (backgroundJob.status != 200) {
                let errMsg = backgroundJob.response[0].message
                switch (backgroundJob.status) {
                    case 400:
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    case 401:
                        res.send(new errors.UnauthorizedError(errMsg))
                        return
                    case 403:
                        res.send(new errors.ForbiddenError(errMsg))
                        return
                    case 500:
                        res.send(new errors.InternalError(errMsg))
                        return
                    default:
                        res.send(new errors.InternalError(errMsg))
                        return
                }
            }

            // Set data to pass to RabbitMQ worker
            let data = {
                "token": token.slice(7),                        // slice(7) to remove 'Bearer ', IMPORTANT: remove this after refactoring processor worker to use tokenObject (cb-workers)
                "tokenObject": tokenObject,                     // contains token and refreshToken
                "personDbId": personDbId,
                "httpMethod": httpMethod,
                "endpointEntity": endpointEntity,
                "url": endpoint.slice(3),                       // slice(3) to remove 'v3/'
                "dbIds": dbIdArray,
                "requestData": requestData,
                "dependents": dependents,
                "isDraftedExperiment": isDraftedExperiment,
                "cascadePersistent": cascadePersistent,
                "backgroundJobDbId": backgroundJob.id,
            }

            // Retrieve config values
            let host = (process.env.CB_RABBITMQ_HOST)
            let port = (process.env.CB_RABBITMQ_PORT)
            let user = (process.env.CB_RABBITMQ_USER)
            let password = (process.env.CB_RABBITMQ_PASSWORD)
            password = encodeURIComponent(password)

            // Create connection to RabbitMQ worker
            let connection = await amqp.connect('amqp://' + user + ':' + password + '@' + host + ':' + port)

            // Create channel
            let channel = await connection.createChannel()

            // Assert the queue for the worker
            await channel.assertQueue(workerName, { durable: true })

            // Send data to RabbitMQ worker
            await channel.sendToQueue(workerName, Buffer.from(JSON.stringify(data)), {
                contentType: 'application/json',
                persistent: true
            })

            result = {
                backgroundJobDbId: backgroundJob.id,
                description: description
            }

            let infoObject = {
                httpMethod: httpMethod,
                url: endpoint.slice(3),
                backgroundJobDbId: backgroundJobDbId
            }
            await logger.logCustomStatus('processor', 'Data successfully passed to RabbitMQ worker.', infoObject)

            res.send(200, {
                rows: result
            })
            return
        } catch (err) {
            console.log('proc err:', err)
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}