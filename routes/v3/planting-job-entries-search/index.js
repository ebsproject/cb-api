/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
const logger = require('../../../helpers/logger')

module.exports = {
    /**
     * Search method for experiment.plot_data
     * POST /v3/planting-job-entries-search
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        const actualAttributes = [
            'actualEntryDbId',
            'actualGermplasmDbId',
            'actualGermplasmName',
            'actualParentage',
            'actualSeedName',
            'actualPackageDbId',
            'actualPackageLabel',
            'actualAvailableQuantity',
            'actualPackageUnit',
            'actualPackageReserved',
        ]
        let conditionStringArr = []
        let excludedParametersArray = []
        let responseColString = `
            pje.id AS "plantingJobEntryDbId",
            pjo.id AS "plantingJobOccurrenceDbId",
            pj.id AS "plantingJobDbId",
            pj.planting_job_code AS "plantingJobCode",
            e.id AS "experimentDbId",
            e.experiment_code AS "experimentCode",
            e.experiment_name AS "experimentName",
            gs.id AS "siteDbId",
            gs.geospatial_object_code AS "siteCode",
            gs.geospatial_object_name AS "siteName",
            o.id AS "occurrenceDbId",
            o.occurrence_code AS "occurrenceCode",
            o.occurrence_name AS "occurrenceName",
            pjo.seeds_per_envelope AS "seedsPerEnvelope",
            pje.envelope_count AS "envelopeCount",
            et.id AS "entryDbId",
            et.entry_number AS "entryNumber",
            et.entry_name AS "entryName",
            et.entry_type AS "entryType",
            et.germplasm_id AS "germplasmDbId",
            g.designation AS "germplasmName",
            g.germplasm_code AS "germplasmCode",
            g.parentage AS "parentage",
            seed.id AS "seedDbId",
            seed.seed_code AS "seedCode",
            seed.seed_name AS "seedName",
            pje.required_package_quantity AS "requiredPackageQuantity",
            pje.required_package_unit AS "requiredPackageUnit",
            p.id AS "packageDbId",
            p.package_label AS "packageLabel",
            p.package_unit AS "packageUnit",
            p.package_quantity AS "packageQuantity",
            p.package_reserved AS "packageReserved",
            pje.is_sufficient AS "isSufficient",
            pje.is_replaced AS "isReplaced",
            pje.replacement_type AS "replacementType",
            rseed.id AS "replacementSeedDbId",
            rseed.seed_code AS "replacementSeedCode",
            rseed.seed_name AS "replacementSeedName",
            rp.id AS "replacementPackageDbId",
            rp.package_code AS "replacementPackageCode",
            rp.package_label AS "replacementPackageLabel",
            rp.package_unit AS "replacementPackageUnit",
            rp.package_quantity AS "replacementPackageQuantity",
            rp.package_reserved AS "replacementPackageReserved",
            rg.id AS "replacementGermplasmDbId",
            rg.germplasm_code AS "replacementGermplasmCode",
            rg.designation AS "replacementGermplasmName",
            rg.parentage AS "replacementParentage",
            ret.id AS "replacementEntryDbId",
            ret.entry_name AS "replacementEntryName",
            ret.entry_type AS "replacementEntryType",
            ret.seed_id AS "replacementEntrySeedDbId",
            crop.id AS "cropDbId",
            pje.package_log_id AS "packageLogDbId",
            pje.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS "creator",
            pje.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS "modifier"`
        const endpoint = 'planting-job-entries-search'
        const operation = 'SELECT'
        parameters['distinctOn'] = ''

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get filter condition
            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body, 
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let plantingJobEntriesQuery = null

        let fromQuery = `
            FROM
                experiment.planting_job_entry pje
            JOIN
                experiment.entry et
            ON
                pje.entry_id = et.id
                AND pje.is_void = FALSE
                AND et.is_void = FALSE
            LEFT JOIN
                germplasm.seed seed
            ON
                et.seed_id = seed.id
            LEFT JOIN
                experiment.entry ret
            ON
                pje.replacement_entry_id = ret.id
            LEFT JOIN
                germplasm.package p
            ON
                et.package_id = p.id
            JOIN
                experiment.planting_job_occurrence pjo
            ON
                pje.planting_job_occurrence_id = pjo.id
                AND pjo.is_void = FALSE
                AND pje.is_void = FALSE
            LEFT JOIN
                experiment.planting_job pj
            ON
                pjo.planting_job_id = pj.id
            LEFT JOIN
                experiment.occurrence o
            ON
                pjo.occurrence_id = o.id
            LEFT JOIN
                experiment.experiment e
            ON
                o.experiment_id = e.id
            LEFT JOIN
                place.geospatial_object gs
            ON
                o.site_id = gs.id
            LEFT JOIN
                germplasm.germplasm g
            ON
                et.germplasm_id = g.id
            LEFT JOIN
                germplasm.germplasm rg
            ON
                pje.replacement_germplasm_id = rg.id
            LEFT JOIN
                germplasm.package rp
            ON
                pje.replacement_package_id = rp.id
            LEFT JOIN
                germplasm.seed rseed
            ON
                rp.seed_id = rseed.id
            LEFT JOIN
                tenant.program program
            ON
                program.id = e.program_id
            LEFT JOIN
                tenant.crop_program crop_program
            ON
                crop_program.id = program.crop_program_id
            LEFT JOIN
                tenant.crop crop
            ON
                crop.id = crop_program.crop_id
            JOIN
                tenant.person creator
            ON
                pje.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier
            ON
                pje.modifier_id = modifier.id
        `

        if (parameters['fields'] != null) {
            let hasActualAttributes = false
            let fieldsArray = parameters['fields'].split('|').map(field => field.trim())

            let commonAttributes = actualAttributes.filter(
                element => fieldsArray.includes(element)
            )

            if (commonAttributes.length > 0) {
                // remove actualAttributes present in the "fields" prop
                for (const attribute of commonAttributes) {
                    fieldsArray.splice(fieldsArray.indexOf(attribute), 1)
                }

                parameters['fields'] = fieldsArray.join('|')
                hasActualAttributes = true
            }

            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(
                        parameters['fields'],
                    )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)

                plantingJobEntriesQuery = knex.select(selectString)
            } else {
                plantingJobEntriesQuery = knex.column(parameters['fields'].split('|'))
            }

            if(conditionString.includes("entryPlotCount")){
                occurrenceQuery += `,(
                    SELECT
                        count(p.id)
                    FROM
                        experiment.plot p,
                        experiment.entry e
                    WHERE
                        e.id = pje.entry_id AND
                        p.entry_id = e.id AND
                        e.is_void = FALSE AND
                        p.occurrence_id = pjo.occurrence_id AND
                        p.is_void = FALSE
                )::int AS "entryPlotCount"`
            }
    
            
            if(conditionString.includes("entryCode")){
                occurrenceQuery += `,(
                    SELECT
                        pi.entry_code
                    FROM
                        experiment.planting_instruction pi,
                        experiment.plot p
                    WHERE
                        et.id = pi.entry_id AND
                        pi.is_void = FALSE AND
                        pi.plot_id = p.id AND
                        et.id = pje.entry_id AND
                        p.entry_id = et.id AND
                        et.is_void = FALSE AND
                        p.occurrence_id = pjo.occurrence_id AND
                        p.is_void = FALSE
                    LIMIT 1
                ) AS "entryCode"`
            }
    
            // Add SELECT argument for "actual" attributes
            if (hasActualAttributes) {
                for (const attribute of commonAttributes) {
                    switch (attribute) {
                        case 'actualEntryDbId':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE AND
                                        (
                                            SELECT
                                                replacement_entry.id
                                            FROM
                                                experiment.entry replacement_entry
                                            WHERE
                                                replacement_entry.id = ret.id
                                        ) IS NOT NULL THEN
                                            (
                                                SELECT
                                                    replacement_entry.id
                                                FROM
                                                    experiment.entry replacement_entry
                                                WHERE
                                                    replacement_entry.id = ret.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                entry.id
                                            FROM
                                                experiment.entry entry
                                            WHERE
                                                entry.id = et.id
                                        )
                                END AS "actualEntryDbId"`
                            break;
                        case 'actualGermplasmDbId':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE AND
                                        (
                                            SELECT
                                                replacement_germplasm.id
                                            FROM
                                                germplasm.germplasm replacement_germplasm
                                            WHERE
                                                replacement_germplasm.id = rg.id
                                        ) IS NOT NULL THEN
                                            (
                                                SELECT
                                                    replacement_germplasm.id
                                                FROM
                                                    germplasm.germplasm replacement_germplasm
                                                WHERE
                                                    replacement_germplasm.id = rg.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                germplasm.id
                                            FROM
                                                germplasm.germplasm germplasm
                                            WHERE
                                                germplasm.id = g.id
                                        )
                                END AS "actualGermplasmDbId"`
                            break;
                        case 'actualGermplasmName':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE AND
                                        (
                                            SELECT
                                                replacement_germplasm.designation
                                            FROM
                                                germplasm.germplasm replacement_germplasm
                                            WHERE
                                                replacement_germplasm.id = rg.id
                                        ) IS NOT NULL THEN
                                            (
                                                SELECT
                                                    replacement_germplasm.designation
                                                FROM
                                                    germplasm.germplasm replacement_germplasm
                                                WHERE
                                                    replacement_germplasm.id = rg.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                germplasm.designation
                                            FROM
                                                germplasm.germplasm germplasm
                                            WHERE
                                                germplasm.id = g.id
                                        )
                                END AS "actualGermplasmName"`
                            break;
                        case 'actualParentage':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE AND
                                        (
                                            SELECT
                                                replacement_germplasm.parentage
                                            FROM
                                                germplasm.germplasm replacement_germplasm
                                            WHERE
                                                replacement_germplasm.id = rg.id
                                        ) IS NOT NULL THEN
                                            (
                                                SELECT
                                                    replacement_germplasm.parentage
                                                FROM
                                                    germplasm.germplasm replacement_germplasm
                                                WHERE
                                                    replacement_germplasm.id = rg.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                germplasm.parentage
                                            FROM
                                                germplasm.germplasm germplasm
                                            WHERE
                                                germplasm.id = g.id
                                        )
                                END AS "actualParentage"`
                            break;
                        case 'actualSeedName':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE THEN
                                            (
                                                SELECT
                                                    replacement_seed.seed_name
                                                FROM
                                                    germplasm.seed replacement_seed
                                                WHERE
                                                    replacement_seed.id = rseed.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                s.seed_name
                                            FROM
                                                germplasm.seed s
                                            WHERE
                                                s.id = seed.id
                                        )
                                END AS "actualSeedName"`
                            break;
                        case 'actualPackageDbId':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE THEN
                                            (
                                                SELECT
                                                    replacement_package.id
                                                FROM
                                                    germplasm.package replacement_package
                                                WHERE
                                                    replacement_package.id = rp.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                package.id
                                            FROM
                                                germplasm.package package
                                            WHERE
                                                package.id = p.id
                                        )
                                END AS "actualPackageDbId"`
                            break;
                        case 'actualPackageLabel':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE THEN
                                            (
                                                SELECT
                                                    replacement_package.package_label
                                                FROM
                                                    germplasm.package replacement_package
                                                WHERE
                                                    replacement_package.id = rp.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                package.package_label
                                            FROM
                                                germplasm.package package
                                            WHERE
                                                package.id = p.id
                                        )
                                END AS "actualPackageLabel"`
                            break;
                        case 'actualPackageReserved':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE THEN
                                            (
                                                SELECT
                                                    replacement_package.package_reserved
                                                FROM
                                                    germplasm.package replacement_package
                                                WHERE
                                                    replacement_package.id = rp.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                package.package_reserved
                                            FROM
                                                germplasm.package package
                                            WHERE
                                                package.id = p.id
                                        )
                                END AS "actualPackageReserved"`
                            break;
                        case 'actualAvailableQuantity':
                            plantingJobEntriesQuery += `,
                            ((COALESCE
                                (
                                    CASE
                                        WHEN
                                            (
                                                SELECT
                                                    planting_job_entry.is_replaced
                                                FROM
                                                    experiment.planting_job_entry planting_job_entry
                                                WHERE
                                                    planting_job_entry.id = pje.id
                                            ) = TRUE THEN
                                                (
                                                    SELECT
                                                        replacement_package.package_quantity
                                                    FROM
                                                        germplasm.package replacement_package
                                                    WHERE
                                                        replacement_package.id = rp.id
                                                )
                                        ELSE
                                            (
                                                SELECT
                                                    package.package_quantity
                                                FROM
                                                    germplasm.package package
                                                WHERE
                                                    package.id = p.id
                                            )
                                    end
                                ,0)
                            )
                            -
                            (COALESCE
                                (
                                    CASE
                                        WHEN
                                            (
                                                SELECT
                                                    planting_job_entry.is_replaced
                                                FROM
                                                    experiment.planting_job_entry planting_job_entry
                                                WHERE
                                                    planting_job_entry.id = pje.id
                                            ) = TRUE THEN
                                                (
                                                    SELECT
                                                        replacement_package.package_reserved
                                                    FROM
                                                        germplasm.package replacement_package
                                                    WHERE
                                                        replacement_package.id = rp.id
                                                )
                                        ELSE
                                            (
                                                SELECT
                                                    package.package_reserved
                                                FROM
                                                    germplasm.package package
                                                WHERE
                                                    package.id = p.id
                                            )
                                    END
                                ,0)
                            ))
                            AS "actualAvailableQuantity"`
                            break;
                        case 'actualPackageUnit':
                            plantingJobEntriesQuery += `,
                                CASE
                                    WHEN
                                        (
                                            SELECT
                                                planting_job_entry.is_replaced
                                            FROM
                                                experiment.planting_job_entry planting_job_entry
                                            WHERE
                                                planting_job_entry.id = pje.id
                                        ) = TRUE THEN
                                            (
                                                SELECT
                                                    replacement_package.package_unit
                                                FROM
                                                    germplasm.package replacement_package
                                                WHERE
                                                    replacement_package.id = rp.id
                                            )
                                    ELSE
                                        (
                                            SELECT
                                                package.package_unit
                                            FROM
                                                germplasm.package package
                                            WHERE
                                                package.id = p.id
                                        )
                                END AS "actualPackageUnit"`
                            break;
                    }
                }
            }
            plantingJobEntriesQuery += fromQuery
        } else {
            plantingJobEntriesQuery = `
                SELECT
                    ${responseColString},
                    (
                        SELECT
                            count(p.id)
                        FROM
                            experiment.plot p,
                            experiment.entry e
                        WHERE
                            e.id = pje.entry_id AND
                            p.entry_id = e.id AND
                            e.is_void = FALSE AND
                            p.occurrence_id = pjo.occurrence_id AND
                            p.is_void = FALSE
                    )::int AS "entryPlotCount",
                    (
                        SELECT
                            pi.entry_code
                        FROM
                            experiment.planting_instruction pi,
                            experiment.plot p
                        WHERE
                            et.id = pi.entry_id AND
                            pi.is_void = FALSE AND
                            pi.plot_id = p.id AND
                            et.id = pje.entry_id AND
                            p.entry_id = et.id AND
                            et.is_void = FALSE AND
                            p.occurrence_id = pjo.occurrence_id AND
                            p.is_void = FALSE
                        LIMIT 1
                    ) AS "entryCode",
                    -- Get Actual Entry DB ID
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE AND
                            (
                                SELECT
                                    replacement_entry.id
                                FROM
                                    experiment.entry replacement_entry
                                WHERE
                                    replacement_entry.id = ret.id
                            ) IS NOT NULL THEN
                                (
                                    SELECT
                                        replacement_entry.id
                                    FROM
                                        experiment.entry replacement_entry
                                    WHERE
                                        replacement_entry.id = ret.id
                                )
                        ELSE
                            (
                                SELECT
                                    entry.id
                                FROM
                                    experiment.entry entry
                                WHERE
                                    entry.id = et.id
                            )
                    END AS "actualEntryDbId",
                    -- Get Actual Germplasm DB ID
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE AND
                            (
                                SELECT
                                    replacement_germplasm.id
                                FROM
                                    germplasm.germplasm replacement_germplasm
                                WHERE
                                    replacement_germplasm.id = rg.id
                            ) IS NOT NULL THEN
                                (
                                    SELECT
                                        replacement_germplasm.id
                                    FROM
                                        germplasm.germplasm replacement_germplasm
                                    WHERE
                                        replacement_germplasm.id = rg.id
                                )
                        ELSE
                            (
                                SELECT
                                    germplasm.id
                                FROM
                                    germplasm.germplasm germplasm
                                WHERE
                                    germplasm.id = g.id
                            )
                    END AS "actualGermplasmDbId",
                    -- Get Actual Germplasm Name
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE AND
                            (
                                SELECT
                                    replacement_germplasm.designation
                                FROM
                                    germplasm.germplasm replacement_germplasm
                                WHERE
                                    replacement_germplasm.id = rg.id
                            ) IS NOT NULL THEN
                                (
                                    SELECT
                                        replacement_germplasm.designation
                                    FROM
                                        germplasm.germplasm replacement_germplasm
                                    WHERE
                                        replacement_germplasm.id = rg.id
                                )
                        ELSE
                            (
                                SELECT
                                    germplasm.designation
                                FROM
                                    germplasm.germplasm germplasm
                                WHERE
                                    germplasm.id = g.id
                            )
                    END AS "actualGermplasmName",
                    -- Get Actual Parentage
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE AND
                            (
                                SELECT
                                    replacement_germplasm.parentage
                                FROM
                                    germplasm.germplasm replacement_germplasm
                                WHERE
                                    replacement_germplasm.id = rg.id
                            ) IS NOT NULL THEN
                                (
                                    SELECT
                                        replacement_germplasm.parentage
                                    FROM
                                        germplasm.germplasm replacement_germplasm
                                    WHERE
                                        replacement_germplasm.id = rg.id
                                )
                        ELSE
                            (
                                SELECT
                                    germplasm.parentage
                                FROM
                                    germplasm.germplasm germplasm
                                WHERE
                                    germplasm.id = g.id
                            )
                    END AS "actualParentage",
                    -- Get Actual Seed Name
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE THEN
                                (
                                    SELECT
                                        replacement_seed.seed_name
                                    FROM
                                        germplasm.seed replacement_seed
                                    WHERE
                                        replacement_seed.id = rseed.id
                                )
                        ELSE
                            (
                                SELECT
                                    s.seed_name
                                FROM
                                    germplasm.seed s
                                WHERE
                                    s.id = seed.id
                            )
                    END AS "actualSeedName",
                    -- Get Actual Package DB ID
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE THEN
                                (
                                    SELECT
                                        replacement_package.id
                                    FROM
                                        germplasm.package replacement_package
                                    WHERE
                                        replacement_package.id = rp.id
                                )
                        ELSE
                            (
                                SELECT
                                    package.id
                                FROM
                                    germplasm.package package
                                WHERE
                                    package.id = p.id
                            )
                    END AS "actualPackageDbId",
                    -- Get Actual Package Label
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE THEN
                                (
                                    SELECT
                                        replacement_package.package_label
                                    FROM
                                        germplasm.package replacement_package
                                    WHERE
                                        replacement_package.id = rp.id
                                )
                        ELSE
                            (
                                SELECT
                                    package.package_label
                                FROM
                                    germplasm.package package
                                WHERE
                                    package.id = p.id
                            )
                    END AS "actualPackageLabel",
                    -- Get Actual Package Reserved
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE THEN
                                (
                                    SELECT
                                        replacement_package.package_reserved
                                    FROM
                                        germplasm.package replacement_package
                                    WHERE
                                        replacement_package.id = rp.id
                                )
                        ELSE
                            (
                                SELECT
                                    package.package_reserved
                                FROM
                                    germplasm.package package
                                WHERE
                                    package.id = p.id
                            )
                    END AS "actualPackageReserved",
                    -- Get Actual Available Quantity
                    ((COALESCE
                        (
                            CASE
                                WHEN
                                    (
                                        SELECT
                                            planting_job_entry.is_replaced
                                        FROM
                                            experiment.planting_job_entry planting_job_entry
                                        WHERE
                                            planting_job_entry.id = pje.id
                                    ) = TRUE THEN
                                        (
                                            SELECT
                                                replacement_package.package_quantity
                                            FROM
                                                germplasm.package replacement_package
                                            WHERE
                                                replacement_package.id = rp.id
                                        )
                                ELSE
                                    (
                                        SELECT
                                            package.package_quantity
                                        FROM
                                            germplasm.package package
                                        WHERE
                                            package.id = p.id
                                    )
                            end
                        ,0)
                    )
                    -
                    (COALESCE
                        (
                            CASE
                                WHEN
                                    (
                                        SELECT
                                            planting_job_entry.is_replaced
                                        FROM
                                            experiment.planting_job_entry planting_job_entry
                                        WHERE
                                            planting_job_entry.id = pje.id
                                    ) = TRUE THEN
                                        (
                                            SELECT
                                                replacement_package.package_reserved
                                            FROM
                                                germplasm.package replacement_package
                                            WHERE
                                                replacement_package.id = rp.id
                                        )
                                ELSE
                                    (
                                        SELECT
                                            package.package_reserved
                                        FROM
                                            germplasm.package package
                                        WHERE
                                            package.id = p.id
                                    )
                            END
                        ,0)
                    ))
                    AS "actualAvailableQuantity",
                    -- Get Actual Package Unit
                    CASE
                        WHEN
                            (
                                SELECT
                                    planting_job_entry.is_replaced
                                FROM
                                    experiment.planting_job_entry planting_job_entry
                                WHERE
                                    planting_job_entry.id = pje.id
                            ) = TRUE THEN
                                (
                                    SELECT
                                        replacement_package.package_unit
                                    FROM
                                        germplasm.package replacement_package
                                    WHERE
                                        replacement_package.id = rp.id
                                )
                        ELSE
                            (
                                SELECT
                                    package.package_unit
                                FROM
                                    germplasm.package package
                                WHERE
                                    package.id = p.id
                            )
                    END AS "actualPackageUnit"
                ${fromQuery}
            `
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let plantingJobEntriesFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            plantingJobEntriesQuery,
            conditionString,
            orderString
        )

        plantingJobEntriesFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            plantingJobEntriesFinalSqlQuery,
            orderString
        )

        let plantingJobEntries = await sequelize
            .query(plantingJobEntriesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                return undefined
            })
        
        if (plantingJobEntries == undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        if ( await plantingJobEntries == undefined || await plantingJobEntries.length < 1 ) {
            res.send(200, {
                rows: [],
                count: 0
            })

            return
        }

        count = plantingJobEntries[0].totalCount

        res.send(200, {
            rows: plantingJobEntries,
            count: count
        })

        return
    }
}