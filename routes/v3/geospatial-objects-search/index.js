/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY "geospatialObject".id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn',
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray,
        )

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }
    // Build the base retrieval query
    let geospatialObjectsQuery = null
    // Check if the client specified values for fields
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])
        let selectString =  knex.raw(`${addedDistinctString} ${fieldsString}`)
        geospatialObjectsQuery = knex.select(selectString)
      } else {
        geospatialObjectsQuery = knex.column(parameters['fields'].split('|'))
      }
      geospatialObjectsQuery += `
        FROM
          place.geospatial_object "geospatialObject"
        LEFT JOIN
          tenant.person creator ON "geospatialObject".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "geospatialObject".modifier_id = modifier.id
        LEFT JOIN
          place.geospatial_object parent ON "geospatialObject".parent_geospatial_object_id = parent.id
        LEFT JOIN
          place.geospatial_object root ON "geospatialObject".root_geospatial_object_id = root.id
        WHERE
          "geospatialObject".is_void = FALSE
        ${addedOrderString}
      `
    } else {
      geospatialObjectsQuery = `
        SELECT
          "geospatialObject".id AS "geospatialObjectDbId",
          "geospatialObject".geospatial_object_code AS "geospatialObjectCode",
          "geospatialObject".geospatial_object_name AS "geospatialObjectName",
          "geospatialObject".geospatial_object_type AS "geospatialObjectType",
          "geospatialObject".geospatial_object_subtype AS "geospatialObjectSubtype",
          "geospatialObject".geospatial_coordinates AS "geospatialObjectCoordinates",
          "geospatialObject".altitude,
          "geospatialObject".description,
          "geospatialObject".geospatial_object_name||' ('||root.geospatial_object_code||')' AS "scaleName",
          parent.id AS "parentGeospatialObjectDbId",
          parent.geospatial_object_code AS "parentGeospatialObjectCode",
          parent.geospatial_object_name AS "parentGeospatialObjectName",
          parent.geospatial_object_type AS "parentGeospatialObjectType",
          root.id aS "rootGeospatialObjectDbId",
          root.geospatial_object_code AS "rootGeospatialObjectCode",
          root.geospatial_object_name AS "rootGeospatialObjectName",
          root.geospatial_object_type AS "rootGeospatialObjectType",
          "geospatialObject".creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          "geospatialObject".modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          place.geospatial_object "geospatialObject"
        LEFT JOIN
          tenant.person creator ON "geospatialObject".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "geospatialObject".modifier_id = modifier.id
        LEFT JOIN
          place.geospatial_object parent ON "geospatialObject".parent_geospatial_object_id = parent.id
        LEFT JOIN
          place.geospatial_object root ON "geospatialObject".root_geospatial_object_id = root.id
        WHERE
          "geospatialObject".is_void = FALSE
        ${addedOrderString}
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let geospatialObjectsFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        geospatialObjectsQuery,
        conditionString,
        orderString,
      )

    let geospatialObjects = await sequelize
      .query(geospatialObjectsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await geospatialObjects == undefined || await geospatialObjects.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let geospatialObjectsCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        geospatialObjectsQuery,
        conditionString,
        orderString,
      )

    let geospatialObjectsCount = await sequelize
      .query(geospatialObjectsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })

    count = geospatialObjectsCount[0].count

    res.send(200, {
      rows: geospatialObjects,
      count: count
    })
    return
  }
}