/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let responseHelper = require('../../../helpers/responses')
let tokenHelper = require('../../../helpers/auth/token.js')
let validator = require('validator')
var url = require('url')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    /**
    * Create record(s) in germplasm.cross_data
    * POST /v3/cross-data
    * @param {*} req Request parameters
    * @param {*} res Response
    */
    post: async function (req, res) {

        let crossDataIdArray = []
        let creatorId = await tokenHelper.getUserId(req)
        let transaction
        let variableDbId
        let dataValue
        let dataQCCode
        let crossDbId
        let remarks = ''
        let transactionDbId = ''
        let collectionTimestamp = ''
        let params = req.query
        let allowMultiple = params.allowMultiple != null && params.allowMultiple.toLowerCase() == 'true'
        let dataQCCodeList = ['N', 'G', 'Q', 'S', 'M', 'B']

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Get the url
        var crossDataUrlString = (req.connection && req.connection.encrypted ? 'https' : 'http')
            + "://"
            + req.headers.host
            + (url.parse(req.url)).pathname

        // Check for parameters
        if (req.body != undefined) {

            // Parse the input
            let data = req.body
            let records = []
            try {
                records = data.records

                if (records == undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            } catch (e) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            // Start transaction
            try {
                // get transaction
                transaction = await sequelize.transaction({ autocommit: false });
                let insertColumns
                let insertValues
                for (var item in records) {
                    insertColumns = []
                    insertValues = []
                    if (
                        records[item].variableDbId === undefined
                        && records[item].dataValue === undefined
                        && records[item].dataQCCode === undefined
                        && records[item].crossDbId === undefined
                    ) {
                        continue;
                    }
                    if (
                        records[item].variableDbId === undefined
                        || records[item].dataValue === undefined
                        || records[item].dataQCCode === undefined
                        || records[item].crossDbId === undefined
                    ) {
                        let errMsg = 'Required parameters are missing. Ensure that crossDbId, variableDbId, dataValue, and dataQCCode are not empty.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    variableDbId = records[item].variableDbId
                    dataValue = records[item].dataValue
                    dataQCCode = records[item].dataQCCode
                    crossDbId = records[item].crossDbId

                    // Check if cross ID exist
                    crossQuery = `
                        SELECT exists(
                            SELECT 1
                            FROM 
                                germplasm.cross
                            WHERE 
                                id = ${crossDbId} 
                        )
                    `
                    let crossResult = await sequelize.query(crossQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (!crossResult[0]['exists']) {
                        let errMsg = 'The cross ID does not exist.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            404,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }
                    // Check if variable ID exist
                    variableQuery = `
                        SELECT *
                        FROM 
                            master.variable
                        WHERE 
                            id = ${variableDbId} 
                    `
                    let variableResult = await sequelize.query(variableQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (await variableResult == undefined || await variableResult.length == 0) {
                        let errMsg = 'The variable ID does not exist.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            404,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    // check if dataQCCode
                    if (dataQCCodeList.indexOf(dataQCCode) == -1) {
                        let errMsg = 'The dataQCCode is invalid. dataQCCode should be one '
                            + 'of the following: ' + dataQCCodeList.toString()
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // check if value is valid
                    if (variableResult[0]['data_type'] == 'integer' && !validator.isInt(dataValue)) {
                        let errMsg = 'Invalid format, variableDbId with dataType of an integer must have an integer dataValue.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (
                        (variableResult[0]['data_type'] == 'float' || variableResult[0]['data_type'] == 'double precision')
                        && !validator.isFloat(dataValue)) {
                        let errMsg = 'Invalid format, variableDbId with dataType of a float or double precision must have a float dataValue.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (variableResult[0]['data_type'] == 'boolean' && !validator.isBoolean(dataValue)) {
                        let errMsg = 'Invalid format, variableDbId with dataType of a boolean must have an boolean dataValue.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (variableResult[0]['data_type'] == 'date' && validator.toDate(dataValue) == null) {
                        let errMsg = 'Invalid format, variableDbId with dataType of a date must be a valid date in format YYYY-MM-DD.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else if (variableResult[0]['data_type'] == 'date' && validator.toDate(dataValue) != null) {

                        var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
                        if (!dataValue.match(validDateFormat)) {    // Invalid format
                            let errMsg = 'Invalid format, variableDbId with dataType of a date must be in format YYYY-MM-DD.'
                            errorCode = await responseHelper.getErrorCodebyMessage(
                                400,
                                errMsg)
                            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }

                    // check for scale values
                    if (variableResult[0]['scale_id'] != null) {
                        scaleQuery = `
                            SELECT 
                                ARRAY_AGG(value)
                            FROM 
                                master.scale_value
                            WHERE
                                scale_id=${variableResult[0]['scale_id']}
                                AND is_void=false
                        `
                        let scaleResult = await sequelize.query(scaleQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                        if (scaleResult[0]['array_agg'] != null && scaleResult[0]['array_agg'].indexOf(dataValue) == -1) {

                            let errMsg = 'The dataValue is invalid. dataValue should be one '
                                + 'of the following: ' + scaleResult[0]['array_agg'].toString()
                            errorCode = await responseHelper.getErrorCodebyMessage(
                                400,
                                errMsg)
                            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }

                    if (records[item].transactionDbId !== undefined) {

                        // Check if transaction ID exist
                        transactionQuery = `
                            SELECT exists(
                                SELECT 1
                                FROM 
                                    data_terminal.transaction 
                                WHERE 
                                    id = ${records[item].transactionDbId} 
                            )
                        `
                        let transactionResult = await sequelize.query(transactionQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        if (!transactionResult[0]['exists']) {
                            let errMsg = 'The terminal transaction ID does not exist.'
                            errorCode = await responseHelper.getErrorCodebyMessage(
                                404,
                                errMsg)
                            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                            res.send(new errors.NotFoundError(errMsg))
                            return
                        } else {
                            transactionDbId = records[item].transactionDbId
                            insertValues.push(transactionDbId)
                            insertColumns.push("transaction_id")
                        }
                    }

                    if (records[item].collectionTimestamp !== undefined) {
                        collectionTimestamp = `${records[item].collectionTimestamp}`

                        let validTimestamp = /^\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d\d\dZ$/
                        if (!collectionTimestamp.match(validTimestamp)) {    // Invalid format
                            let errMsg = 'Invalid format, collectionTimestamp must be in this format YYYY-MM-DDThh:mm:ss.sssZ.'
                            errorCode = await responseHelper.getErrorCodebyMessage(
                                400,
                                errMsg)
                            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        collectionTimestamp = `'${records[item].collectionTimestamp}'`
                    }
                    else {
                        collectionTimestamp = 'NOW()'
                    }
                    
                    let uniquenessWhereClause = ``
                    if (!allowMultiple) {
                        uniquenessWhereClause = `
                            WHERE
                                NOT EXISTS(
                                    SELECT 1 
                                    FROM 
                                        germplasm.cross_data
                                    WHERE 
                                        variable_id = ${variableDbId}
                                        AND cross_id = ${crossDbId}
                                        AND is_void = false
                                )    
                        `
                    }

                    insertValues = insertValues.toString()
                    insertColumns = insertColumns.toString()
                    if (insertValues != '') {
                        insertValues = ',' + insertValues
                    }
                    if (insertValues != '') {
                        insertColumns = ',' + insertColumns
                    }
                    let crossDataQuery = `
                        INSERT INTO
                            germplasm.cross_data (
                                cross_id,
                                variable_id,
                                data_value,
                                data_qc_code,
                                creator_id,
                                creation_timestamp,
                                is_void,
                                collection_timestamp
                                ${insertColumns}
                            )
                        SELECT
                            ${crossDbId},
                            ${variableDbId},
                            '${dataValue}',
                            '${dataQCCode}',
                            ${creatorId},
                            NOW(),
                            FALSE,
                            ${collectionTimestamp}
                            ${insertValues}
                        ${uniquenessWhereClause}
                        RETURNING 
                                id
                `
                    await sequelize.query(crossDataQuery, {
                        type: sequelize.QueryTypes.INSERT,
                        transaction: transaction
                    })
                        .catch(async err => {
                            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                            res.send(new errors.InternalError(errMsg))
                            return
                        })
                        .then(function (crossDataArray) {

                            if (crossDataArray[1] > 0) {
                                let crossDataDbId = crossDataArray[0][0]["id"]
                                let returnArray = {
                                    crossDataDbId: crossDataDbId,
                                    href: crossDataUrlString + "/" + crossDataDbId
                                }
                                crossDataIdArray.push(returnArray)
                            }
                        })
                }
                // Commit the transaction
                await transaction.commit()

                res.send(200, {
                    rows: crossDataIdArray
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback();
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            }
        }
    }
}