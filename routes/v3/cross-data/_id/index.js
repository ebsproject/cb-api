/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {

    /**
    * Deletes a specific record in germplasm.cross_data
    * DELETE /v3/cross-data/:id
    * @param {*} req Request parameters
    * @param {*} res Response
    */
    delete: async function (req, res, next) {

        let crossDataDbId = req.params.id

        // Check if crossDataDbId is integer
        if (!validator.isInt(crossDataDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400247)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let crossDataQuery = `
                SELECT
                    creator.id AS "creatorDbId", 
                    occurrence.id AS "occurrenceDbId"
                FROM 
                    germplasm.cross_data cd
                LEFT JOIN
                    tenant.person creator ON creator.id = cd.creator_id
                LEFT JOIN
                    germplasm.cross "germplasmCross" ON "germplasmCross".id = cd.cross_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = "germplasmCross".experiment_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                WHERE 
                    cd.is_void = FALSE AND 
                    cd.id = ${crossDataDbId}
            `

            let crossDataRecord = await sequelize.query(crossDataQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
      
            if (await crossDataRecord === undefined || await crossDataRecord.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404043)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let deleteCrossDataQuery = format(`
                UPDATE
                    germplasm.cross_data
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${crossDataDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${crossDataDbId}
            `)

            await sequelize.query(deleteCrossDataQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { crossDataDbId: crossDataDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}