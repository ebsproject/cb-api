/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger/index')
let { knex } = require('../../../config/knex')

module.exports = {
    // Endpoint for searching formula_parameter record with advanced filters
    // Implementation of POST call for /v3/formula_parameters-search
    post: async function (req, res, next) {
        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}
        let params = req.params
        let recursiveTable = ''
        let recursiveSelectString = ''
        let mainOrderString = 'ORDER by formula_parameter.id'

        let endpoint = 'formula-parameters-search'

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', "paramVariableDbId"]
            // Get filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        if (params.paramVariableDbId !== undefined) {
            let values = params.paramVariableDbId.replace('|', ',')
            mainOrderString = 'ORDER BY formula_parameter.order_number'
            recursiveSelectString = `formula_parameter.order_number AS "orderNumber",`
            recursiveTable = `
            (
                WITH RECURSIVE formula (
                    id, 
                    formula_id,
                    result_variable_id, 
                    param_variable_id,
                    data_level, 
                    creator_id, 
                    creation_timestamp,
                    modifier_id,
                    modification_timestamp,
                    remarks,
                    notes,
                    is_void,
                    is_array,
                    order_number)
                AS(
                    SELECT
                        fp.id,
                        fp.formula_id,
                        fp.result_variable_id,
                        fp.param_variable_id,
                        fp.data_level,
                        fp.creator_id,
                        fp.creation_timestamp,
                        fp.modifier_id,
                        fp.modification_timestamp,
                        fp.remarks,
                        fp.notes,
                        fp.is_void,
                        fp.is_array,
                        1 AS order_number
                    FROM 
                        master.formula_parameter fp 
                    LEFT JOIN
                        master.variable parameter_variable ON 
                        parameter_variable.id = fp.param_variable_id
                    LEFT JOIN
                        master.variable result_variable ON 
                        result_variable.id = fp.result_variable_id
                    WHERE 
                        fp.param_variable_id IN (${values})
                        AND fp.is_void = FALSE AND
                        result_variable.is_void = FALSE  AND
                        parameter_variable.is_void = FALSE
                    UNION ALL
                    SELECT 
                        fp1.id,
                        fp1.formula_id,
                        fp1.result_variable_id,
                        fp1.param_variable_id,
                        fp1.data_level,
                        fp1.creator_id,
                        fp1.creation_timestamp,
                        fp1.modifier_id,
                        fp1.modification_timestamp,
                        fp1.remarks,
                        fp1.notes,
                        fp1.is_void,
                        fp1.is_array, 
                        formula.order_number + 1
                    FROM 
                        formula
                    JOIN
                        master.formula_parameter fp1 ON formula.result_variable_id = fp1.param_variable_id
                        AND formula.param_variable_id NOT IN (${values})
                    LEFT JOIN
                        master.variable parameter_variable ON 
                        parameter_variable.id = fp1.param_variable_id
                    LEFT JOIN
                        master.variable result_variable ON 
                        result_variable.id = fp1.result_variable_id
                    WHERE 
                        fp1.is_void = FALSE AND
                        result_variable.is_void = FALSE  AND
                        parameter_variable.is_void = FALSE AND
                        formula.is_void = FALSE
                ) 
                SELECT 
                    DISTINCT(formula_id)AS distinct,
                    * 
                FROM 
                    formula 
                ORDER BY 
                    order_number DESC
            )
        `
        } else {
            recursiveTable = `master.formula_parameter`
        }
        // Build the base retrieval query
        let formulaParametersQuery = null
        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            formulaParametersQuery = knex.column(parameters['fields'].split('|'))
            formulaParametersQuery += `
                FROM
                    ${recursiveTable} formula_parameter
                    LEFT JOIN
                            master.formula ON formula.id = formula_parameter.formula_id
                    LEFT JOIN
                        tenant.person creator ON formula_parameter.creator_id = creator.id
                    LEFT JOIN
                        tenant.person modifier ON formula_parameter.modifier_id = modifier.id
                    LEFT JOIN
                        master.variable parameter_variable ON 
                        parameter_variable.id = formula_parameter.param_variable_id
                    LEFT JOIN
                        master.variable result_variable ON 
                        result_variable.id = formula_parameter.result_variable_id 
                
                WHERE
                    formula_parameter.is_void = FALSE AND
                    result_variable.is_void = FALSE  AND
                    parameter_variable.is_void = FALSE AND
                    formula.is_void = FALSE
                    ` + addedConditionString + `
                    ${mainOrderString}
            `
        } else {
            formulaParametersQuery = `
                SELECT
                    formula_parameter.id AS "formulaParameterDbId",
                    formula_parameter.formula_id AS "formulaDbId",
                    formula.formula AS "formula",
                    formula_parameter.param_variable_id AS "paramVariableDbId",
                    parameter_variable.label AS "paramVariableLabel",
                    parameter_variable.abbrev AS "paramVariableAbbrev",
                    formula_parameter.result_variable_id AS "resultVariableDbId",
                    result_variable.label AS "resultVariableLabel",
                    result_variable.abbrev AS "resultVariableAbbrev",
                    formula_parameter.data_level AS "dataLevel",
                    formula_parameter.is_array AS "isArray",
                    formula_parameter.remarks,
                    formula_parameter.notes,
                    ${recursiveSelectString}
                    formula_parameter.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    formula_parameter.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier
                
                FROM
                    ${recursiveTable} formula_parameter
                        LEFT JOIN
                            master.formula ON formula.id = formula_parameter.formula_id
                        LEFT JOIN
                            tenant.person creator ON formula_parameter.creator_id = creator.id
                        LEFT JOIN
                            tenant.person modifier ON formula_parameter.modifier_id = modifier.id
                        LEFT JOIN
                            master.variable parameter_variable ON 
                            parameter_variable.id = formula_parameter.param_variable_id
                        LEFT JOIN
                            master.variable result_variable ON 
                            result_variable.id = formula_parameter.result_variable_id 
                
                WHERE
                    formula_parameter.is_void = FALSE AND
                    result_variable.is_void = FALSE  AND
                    parameter_variable.is_void = FALSE AND
                    formula.is_void = FALSE
                ` + addedConditionString + `
                ${mainOrderString}
            `
        }
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Generate the final SQL query
        formulaParametersFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            formulaParametersQuery,
            conditionString,
            orderString
        )
        // Retrieve the records
        let formulaParameters = await sequelize
            .query(formulaParametersFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
            })

        if (await formulaParameters == undefined){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
        
        if(await formulaParameters.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get the final count of the records retrieved
            let countFinalSqlQuery = await processQueryHelper
                .getCountFinalSqlQuery(
                    formulaParametersQuery,
                    conditionString,
                    orderString
                )
            let formulaParametersCount = await sequelize
                .query(
                    countFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = formulaParametersCount[0].count
        }

        res.send(200, {
            rows: formulaParameters,
            count: count
        })
        return
    }
}