/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
		let limit = req.paginate.limit
		let offset = req.paginate.offset
		let sort = req.query.sort
		let count = 0
		let orderString = ''
		let conditionString = ''
		let addedDistinctString = ''
		let addedConditionString = '' 
		let parameters = {}
		parameters['distinctOn'] = ''

		if (req.body) {
			if (req.body.fields != null) {
				parameters['fields'] = req.body.fields
			}

			// Set the columns to be excluded in parameters for filtering
			let excludedParametersArray = ['fields', 'distinctOn']
			// Get filter condition
			conditionString = await processQueryHelper
				.getFilter(req.body, excludedParametersArray)
			
			if (conditionString.includes('invalid')) {
				let errMsg = await errorBuilder
					.getError(req.headers.host, 400022)
				res.send(new errors.BadRequestError(errMsg))
				return
			}

			if (req.body.distinctOn != undefined) {
				parameters['distinctOn'] = req.body.distinctOn
				addedDistinctString = await processQueryHelper
					.getDistinctString(parameters['distinctOn'])

				parameters['distinctOn'] = `"${parameters['distinctOn']}",`
			}
		}
		// Build the base retrieval query
		let planTemplatesQuery = null
		// Check if the client specified values for the field/s
		if (parameters['fields']) {
			planTemplatesQuery = knex.column(parameters['fields'].split('|'))
			planTemplatesQuery += `
				FROM
					platform.plan_template "planTemplate"
				LEFT JOIN
					tenant.person creator ON "planTemplate".creator_id = creator.id
				LEFT JOIN
					tenant.person modifier ON "planTemplate".modifier_id = modifier.id
				WHERE
					"planTemplate".is_void = FALSE
				` + addedConditionString +
				`
				ORDER BY
					"planTemplate".id
			`
		} else {
			planTemplatesQuery = `
				SELECT
					${addedDistinctString}
					planTemplate.id AS "planTemplateDbId",
					planTemplate.template_name AS "templateName",
					planTemplate.mandatory_info AS "mandatoryInfo",
					planTemplate.temp_file_cabinet_id AS "tempFileCabinetDbId",
					planTemplate.randomization_results AS "randomizationResults",
					planTemplate.randomization_data_results AS "randomizationDataResults",
					planTemplate.randomization_input_file AS "randomizationInputFile",
					planTemplate.status,
					planTemplate.remarks,
					planTemplate.design,
					planTemplate.occurrence_applied as "occurrenceApplied",
					planTemplate.program_id AS "programDbId",
					planTemplate.entity_type AS "entityType",
					planTemplate.entity_id AS "entityDbId",
					planTemplate.request_id AS "requestDbId",					
					planTemplate.notes,
					planTemplate.creation_timestamp AS "creationTimestamp",
					creator.id AS "creatorDbId",
					creator.person_name AS "creator",
					planTemplate.modification_timestamp AS "modificationTimestamp",
					modifier.id AS "modifierDbId",
					modifier.person_name AS "modifier"
				FROM
					platform.plan_template planTemplate
				LEFT JOIN
					tenant.person creator ON planTemplate.creator_id = creator.id
				LEFT JOIN
					tenant.person modifier ON planTemplate.modifier_id = modifier.id
				WHERE
					planTemplate.is_void = FALSE
				` + addedConditionString + `
				ORDER BY
					${parameters['distinctOn']}
					planTemplate.id
			`
		}

		// Parse the sort parameters
		if (sort != null) {
			orderString = await processQueryHelper.getOrderString(sort)
			if (orderString.includes('invalid')) {
				let errMsg = await errorBuilder
					.getError(req.headers.host, 400004)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}

		// Generate the final SQL query
		planTemplateFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
			planTemplatesQuery,
			conditionString,
			orderString
		)

		// Retrieve the plan template records
		let planTemplates = await sequelize
			.query(planTemplateFinalSqlQuery, {
				type: sequelize.QueryTypes.SELECT,
				replacements: {
					limit: limit,
					offset: offset
				}
			})
			.catch (async err => {
				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

		if (await planTemplates == undefined || await planTemplates.length < 1) {
			res.send(200, {
				rows: [],
				count: 0
			})
			return
		}
      
		let planTemplatesCountFinalSqlQuery = await processQueryHelper
			.getCountFinalSqlQuery(
				planTemplatesQuery,
				conditionString,
				orderString
			)

		let planTemplatesCount = await sequelize
			.query(planTemplatesCountFinalSqlQuery, {
				type: sequelize.QueryTypes.SELECT
			})
			.catch(async err => {
				let errMsg = await errorBuilder
					.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

		count = planTemplatesCount[0].count

		res.send(200, {
			rows: planTemplates,
			count: count
		})
		return
    }
}
