/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

  // Endpoint for voiding an existing file-cabinet record
  // DELETE /v3/file-cabinets/:id
  delete: async function (req, res, next) {
    // Retrieve the file cabinet ID
    let fileCabinetDbId = req.params.id

    if(!validator.isInt(fileCabinetDbId)) {
      let errMsg = `Invalid format, file cabinet ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(personDbId)

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let fileCabinetQuery = `
        SELECT
          creator.id AS "creatorDbId",
          occurrence.id AS "occurrenceDbId"
        FROM
          operational.file_cabinet fc
          LEFT JOIN
            tenant.person creator ON creator.id = fc.creator_id
          LEFT JOIN
            experiment.occurrence occurrence ON occurrence.id = fc.occurrence_id
        WHERE
          fc.is_void = FALSE AND
          fc.id = ${fileCabinetDbId}
      `

      let fileCabinet = await sequelize.query(fileCabinetQuery, {
        type: sequelize.QueryTypes.SELECT
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      if (await fileCabinet == undefined || await fileCabinet.length < 1) {
        let errMsg = `The file cabinet you have requested does not exist.`
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      let recordCreatorDbId = fileCabinet[0].creatorDbId
      let recordOccurrenceDbId = fileCabinet[0].occurrenceDbId

      // Check if user is an admin or an owner of the cross
      if(!isAdmin && (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
          SELECT 
            person.id,
            person.person_role_id AS "role"
          FROM 
            tenant.person person
            LEFT JOIN 
              tenant.team_member teamMember ON teamMember.person_id = person.id
            LEFT JOIN 
              tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
            LEFT JOIN 
              tenant.program program ON program.id = programTeam.program_id 
            LEFT JOIN
              experiment.experiment experiment ON experiment.program_id = program.id
            LEFT JOIN
              experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
          WHERE 
          occurrence.id = ${recordOccurrenceDbId} AND
            person.id = ${personDbId} AND
            person.is_void = FALSE
        `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
      }

      let deleteFileCabinetQuery = format(`
        UPDATE
          operational.file_cabinet
        SET
          is_void = TRUE,
          remarks = CONCAT('VOIDED-', $$${fileCabinetDbId}$$),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          id = ${fileCabinetDbId}
      `)

      await sequelize.query(deleteFileCabinetQuery, {
        type: sequelize.QueryTypes.UPDATE
      })

      await transaction.commit()
      res.send(200, {
        rows: { fileCabinetDbId: fileCabinetDbId }
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}