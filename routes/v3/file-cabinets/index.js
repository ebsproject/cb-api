/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  // Endpoint for adding a file cabinet record
  post: async function (req, res, next) {
    let resultArray = []
    
    // Retrieve the ID of the client via the access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isSecure = forwarded(req, req.headers).secure

    // Set the URL for response
    let fileCabinetUrlString = (isSecure ? 'https' : 'http')
      + "://"
      + req.headers.host
      + "/v3/file-cabinets"

    // Check for parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Parse the input
    let data = req.body
    let records = []

    try {
      records = data.records
      recordCount = records.length

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let fileCabinetValuesArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        // Declare variables
        let folderName = null
        let source = null
        let occurrenceDbId = null
        let folderPath = null
        let files = null
        let transactionDbId = null

        // Check if the required parameters are in the body
        if (
          record.folderName == undefined ||
          record.source == undefined ||
          record.transactionDbId == undefined
        ) {
          let errMsg =
            `Required parameters are missing. Ensure that the folderName, source, and transactionDbId fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        folderName = record.folderName
        source = record.source

        folderPath = (record.folderPath != undefined) ? folderPath : null
        files = (record.files != undefined) ? files : null

        // Validate the value for transactionDbId
        transactionDbId = record.transactionDbId

        if (!validator.isInt(transactionDbId)) {
          let errMsg = 'Invalid format, transaction ID must be an integer.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        if (record.occurrenceDbId != undefined) {
          occurrenceDbId = record.occurrenceDbId
          // Check if it is a valid integer
          if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid format, occurrence ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          // Check if it actually exists
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if occurrence exists, return 1
              SELECT
                count(1)
              FROM
                experiment.occurrence occurrence
              WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
            )
          `
          validateCount += 1
        }
        // Validate the input
        let checkInputQuery = `
          SELECT ${validateQuery} AS count
        `

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Get values
        let tempArray = [
          folderName,
          source,
          folderPath,
          occurrenceDbId,
          transactionDbId,
          files,
          personDbId,
        ]

        fileCabinetValuesArray.push(tempArray)
      }

      // Create a file cabinet record
      let fileCabinetsQuery = format(`
        INSERT INTO
          operational.file_cabinet (
            folder_name, source, folder_path, occurrence_id, transaction_id, files, creator_id
          )
        VALUES
          %L
        RETURNING
          id
        `, fileCabinetValuesArray
      )

      let fileCabinets = await sequelize.query(fileCabinetsQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      for (fileCabinet of fileCabinets[0]) {
        fileCabinetDbId = fileCabinet.id
        let fcObj = {
          fileCabinetDbId: fileCabinetDbId,
          recordCount: 1,
          href: fileCabinetUrlString + '/' + fileCabinetDbId
        }
        resultArray.push(fcObj)
      }

      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}