/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let format = require('pg-format')

module.exports = {
    // Implementation of PUT call for /v3/entry-list-data/:id
    put: async function (req, res, next) {
        // Retrieve entry list data ID
        let entryListDataDbId = req.params.id
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!validator.isInt(entryListDataDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400063)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let dataValue = null
        let dataQcCode = null
        let protocolDbId = null

        // Build query
        let entryListDataQuery = `
            SELECT
                data_value AS "dataValue",
                data_qc_code AS "dataQcCode",
                entry_list_id AS "entryListDbId",
                protocol_id AS "protocolDbId",
                creator_id AS "creatorDbId"
            FROM 
                experiment.entry_list_data
            WHERE
                is_void = FALSE AND
                id = ${entryListDataDbId}
        `

        // Retrieve entry list data record from database
        let entryListData = await sequelize.query(entryListDataQuery,{
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (
            await entryListData == undefined ||
            await entryListData.length < 1
        ) {
            let errMsg = `The entry list data record you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordDataValue = entryListData[0].dataValue
        let recordDataQcCode = entryListData[0].dataQcCode
        let recordEntryListDbId = entryListData[0].entryListDbId
        let recordProtocolDbId = entryListData[0].protocolDbId
        let recordCreatorDbId = entryListData[0].creatorDbId

        let isSecure = forwarded(req, req.header).secure

        // Set URL for response
        let entryListDataUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/entry-list-data/'
            + entryListDataDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let transaction

        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            if (data.dataValue !== undefined) {
                dataValue = data.dataValue

                // Check if user input is same with the current value in the database
                if (dataValue != recordDataValue) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    data_value = $$${dataValue}$$
                    `
                }
            }

            if (data.dataQcCode !== undefined) {
                dataQcCode = data.dataQcCode

                // Check if user input is same with the current value in the database
                if (dataQcCode != recordDataQcCode) {

                let validDataQcCodes = ['N', 'G', 'Q', 'S', 'M', 'B']

                // Validate data QC code
                if (!validDataQcCodes.includes(dataQcCode.toUpperCase())) {
                    let errMsg = `Invalid request, you have provided an invalid value for data qc code.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                data_qc_code = $$${dataQcCode}$$
                `
                }
            }

            if (data.protocolDbId !== undefined) {
                protocolDbId = data.protocolDbId

                if (!validator.isInt(protocolDbId)) {
                    let errMsg = 'Invalid request, protocolDbId must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the user input is the same with the current value
                if (protocolDbId != recordProtocolDbId) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        -- Check if protocol ID exists, return 1
                        SELECT
                            count(1)
                        FROM
                            tenant.protocol p
                        WHERE
                            p.is_void = FALSE AND
                            p.id = ${protocolDbId}
                        )
                    `

                    validateCount += 1
                    

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        protocol_id = $$${protocolDbId}$$
                    `
                }
            }

            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS
                        count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if(setQuery.length > 0) setQuery += `,`

            let updateEntryListDataQuery = `
                UPDATE
                    experiment.entry_list_data
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${entryListDataDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updateEntryListDataQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction

            })

            // Return transaction info
            let resultArray = {
                entryListDataDbId: entryListDataDbId,
                recordCount: 1,
                href: entryListDataUrlString
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for deleting an entry list data record
    // DELETE /v3/entry-list-data/:id
    delete: async function (req, res, next) {
        // Retrieve entryListData ID
        let entryListDataDbId = req.params.id

        // Validation for the entryListData ID
        if (!validator.isInt(entryListDataDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400063)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve the user ID from the access token
        let personDbId = await tokenHelper.getUserId(req)

        // Validation for the user ID
        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }        

        let isAdmin = await userValidator.isAdmin(personDbId)
        let transaction
        try{
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            // Query if entryListData exists
            let entryListDataQuery = `
                SELECT
                    "entryListData".creator_id AS "creatorDbId"
                FROM 
                    experiment.entry_list_data "entryListData"
                LEFT JOIN
                    tenant.person creator ON creator.id = "entryListData".creator_id
                WHERE
                    "entryListData".is_void = FALSE AND
                    "entryListData".id = ${entryListDataDbId}
            `

            // Run query
            let entryListData = await sequelize.query(entryListDataQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
            
            // Return error if the entryListData does not exist
            if(entryListData === undefined || entryListData.length < 1){
                let errMsg = `The entry list data you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = entryListData[0].creatorDbId
            let recordExperimentDbId = entryListData[0].recordExperimentDbId

            // Check if user is an admin or an owner of the entry list data
            if(!isAdmin && (personDbId != recordCreatorDbId)){
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                        LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                        LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                        LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                        LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    WHERE 
                        experiment.id = ${recordExperimentDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `
                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            // Delete entry list data
            let deleteEntryListDataQuery = format(`
                UPDATE
                    experiment.entry_list_data
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${entryListDataDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${entryListDataDbId}
            `)

            // Run query
            await sequelize.query(deleteEntryListDataQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: {entryListDataDbId: entryListDataDbId}
            })
        }
        catch(err){
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}