/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require("../../../config/sequelize");
let { knex } = require("../../../config/knex");
let processQueryHelper = require("../../../helpers/processQuery/index");
let errors = require("restify-errors");
let errorBuilder = require("../../../helpers/error-builder");

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit;
    let offset = req.paginate.offset;
    let sort = req.query.sort;
    let count = 0;
    let orderString = "";
    let conditionString = "";
    let addedConditionString = "";
    let addedDistinctString = "";
    let addedOrderString = `ORDER BY "shipment_file".id`;
    let parameters = {};
    parameters["distinctOn"] = "";

    if (req.body) {
      if (req.body.fields != null) {
        parameters["fields"] = req.body.fields;
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ["fields", "distinctOn"];
      // Get filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      );

      if (req.body.distinctOn != undefined) {
        parameters["distinctOn"] = req.body.distinctOn;
        addedDistinctString = await processQueryHelper.getDistinctString(
          parameters["distinctOn"]
        );
        parameters["distinctOn"] = `"${parameters["distinctOn"]}"`;
        addedOrderString = `
          ORDER BY
            ${parameters["distinctOn"]}
        `;
      }
    }
    // Build the base retrieval query
    let shipmentFilesQuery = null;
    // Check if the client specified values for fields
    if (parameters["fields"]) {
      if (parameters["distinctOn"]) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters["fields"]
        );

        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`);
        shipmentFilesQuery = knex.select(selectString);
      } else {
        shipmentFilesQuery = knex.column(parameters["fields"].split("|"));
      }

      shipmentFilesQuery += `
        FROM
          inventory.shipment_file shipment_file
        LEFT JOIN
          inventory.shipment shipment ON shipment_file.shipment_id = shipment.id
        LEFT JOIN
          tenant.person creator ON shipment_file.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON shipment_file.modifier_id = modifier.id
        WHERE
          shipment_file.is_void = FALSE
          ${addedOrderString}
      `;
    } else {
      shipmentFilesQuery = `
        SELECT
          shipment_file.id AS "shipmentFileDbId",
          shipment_file.shipment_id AS "shipmentDbId",
          shipment_file.file_name AS "fileName",
          shipment_file.file_type AS "fileType",
          shipment_file.file_size AS "fileSize",
          shipment_file.file_path AS "filePath",
          shipment_file.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          shipment_file.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          inventory.shipment_file shipment_file
        LEFT JOIN
          inventory.shipment shipment ON shipment_file.shipment_id = shipment.id
        LEFT JOIN
          tenant.person creator ON shipment_file.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON shipment_file.modifier_id = modifier.id
        WHERE
          shipment_file.is_void = FALSE
          ${addedOrderString}
      `;
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort);
      if (orderString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    let shipmentFilesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      shipmentFilesQuery,
      conditionString,
      orderString
    );

    let shipmentFiles = await sequelize
      .query(shipmentFilesFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        },
      })
      .catch(async (err) => {
        console.log(err);
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    if (
      (await shipmentFiles) == undefined ||
      (await shipmentFiles.length) < 1
    ) {
      res.send(200, {
        rows: [],
        count: 0,
      });
      return;
    }

    let shipmentFilesCountFinalSqlQuery =
      await processQueryHelper.getCountFinalSqlQuery(
        shipmentFilesQuery,
        conditionString,
        orderString
      );

    let shipmentFilesCount = await sequelize.query(
      shipmentFilesCountFinalSqlQuery,
      {
        type: sequelize.QueryTypes.SELECT,
      }
    );

    count = shipmentFilesCount[0].count;

    res.send(200, {
      rows: shipmentFiles,
      count: count,
    });
    return;
  },
};
