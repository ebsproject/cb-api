/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let validator = require('validator')

module.exports = {
  get: async function (req, res, next) {
    let moduleDbId = req.params.id

    if (!validator.isInt(moduleDbId)) {
      let errMsg = 'Invalid request, the moduleDbId you have provided is not a valid integer.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let moduleQuery = `
      SELECT
        module.id AS "moduleDbId",
        module.abbrev,
        module.name,
        module.description,
        module.module_id AS "moduleId",
        module.controller_id AS "controllerId",
        module.action_id AS "actionId",
        module.required_status,
        module.program_id AS "programDbId",
        module.remarks,
        module.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS "creator",
        module.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS "modifier",
        module.notes
      FROM
        platform.module module
      LEFT JOIN
        tenant.person creator ON module.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON module.modifier_id = modifier.id
      WHERE
        module.is_void = FALSE AND
        module.id = ${moduleDbId}
      ORDER BY
        module.id
    `


    let module = await sequelize
      .query(moduleQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await module == undefined || await module.length < 1) {
      let errMsg = "Resource not found. Make sure that the module ID you have indicated exists."
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: module
    })
    return
  }
}