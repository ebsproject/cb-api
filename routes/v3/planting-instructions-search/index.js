/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')

module.exports = {
    post: async function (req, res, next) {
        const endpoint = 'planting-instructions-search'

        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let distinctString = ''
        let fields = null
        let distinctOn = ''
        let orderQuery = ''

        let excludedParametersArray = {}
        let tableAliases = {}
        let columnAliases = {}
        let responseColString = `
                "plantingInstruction".id AS "plantingInstructionDbId",
                entry.id AS "entryDbId", --recommended to replace with "plantingInstruction".entry_id AS "entryDbId"
                entry.entry_code AS "entryCode",
                entry.entry_number AS "entryNumber",
                "plantingInstruction".entry_name AS "entryName",
                "plantingInstruction".entry_type AS "entryType",
                "plantingInstruction".entry_role AS "entryRole",
                "plantingInstruction".entry_class AS "entryClass",
                "plantingInstruction".entry_status AS "entryStatus",
                plot.location_id AS "locationDbId",
                plot.occurrence_id AS "occurrenceDbId",
                plot.id AS "plotDbId", --recommended to replace with "plantingInstruction".plot_id AS "plotDbId"
                plot.plot_code AS "plotCode",
                plot.plot_number AS "plotNumber",
                germplasm.id AS "germplasmDbId", --recommended to replace with "plantingInstruction".germplasm_id AS "germplasmDbId"
                germplasm.designation AS "designation",
                germplasm.parentage AS "parentage",
                germplasm.germplasm_state AS "state",
                package.id AS "packageDbId", --recommended to replace with "plantingInstruction".package_id AS "packageDbId"
                package.package_code AS "packageCode",
                package.package_label AS "packageLabel",
                package.package_quantity AS "packageQuantity",
                package.package_unit AS "packageUnit",
                "plantingInstruction".seed_id AS "seedDbId",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier"
            `


        if (req.body != undefined) {
            // Set columns to be excluded in parameters for filtering
            excludedParametersArray = [
                'fields',
                'distinctOn',
            ]
            
            if (req.body.fields != null) {
                fields = req.body.fields
            }

            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ?  conditionStringArr.mainQuery : ''
            tableAliases = conditionStringArr.tableAliases
            columnAliases = conditionStringArr.columnAliases

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                distinctOn = req.body.distinctOn
                distinctString = await processQueryHelper
                    .getDistinctString(distinctOn)

                distinctOn = `"${distinctOn}"`

                orderQuery = `
                    ORDER BY ${distinctOn}
                `
            }
        }

        let plantingInstructionQuery = null

        if (fields != null) {
            if (distinctOn) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(fields)
                let selectString = knex.raw(`${fieldsString}`)
                plantingInstructionQuery = knex.select(selectString)
            } else {
                plantingInstructionQuery = knex.column(fields.split('|'))
            }

            plantingInstructionQuery += `
                FROM
                    experiment.planting_instruction "plantingInstruction"
                LEFT JOIN
                    experiment.entry entry ON entry.id = "plantingInstruction".entry_id
                    AND entry.is_void = FALSE
                LEFT JOIN
                    experiment.plot plot ON plot.id = "plantingInstruction".plot_id
                    AND plot.is_void = FALSE
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = "plantingInstruction".germplasm_id
                    AND germplasm.is_void = FALSE
                LEFT JOIN
                    germplasm.package package ON "plantingInstruction".package_id = package.id
                LEFT JOIN
                    tenant.person creator ON plot.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON plot.modifier_id = modifier.id
                WHERE
                    "plantingInstruction".is_void = FALSE
                ${orderQuery}  
            `
        } else {
            plantingInstructionQuery = `
                SELECT
                    ${responseColString}
                FROM
                    experiment.planting_instruction "plantingInstruction"
                LEFT JOIN
                    experiment.entry entry ON entry.id = "plantingInstruction".entry_id
                    AND entry.is_void = FALSE
                LEFT JOIN
                    experiment.plot plot ON plot.id = "plantingInstruction".plot_id
                    AND plot.is_void = FALSE
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = "plantingInstruction".germplasm_id
                    AND germplasm.is_void = FALSE
                LEFT JOIN
                    germplasm.package package ON "plantingInstruction".package_id = package.id
                LEFT JOIN
                    tenant.person creator ON plot.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON plot.modifier_id = modifier.id
                WHERE
                    "plantingInstruction".is_void = FALSE
                ${orderQuery}
            `
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let plantingInstructionFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                plantingInstructionQuery,
                conditionString,
                orderString,
                distinctString,
            )

        // Retrieve planting instruction from the database
        let plantingInstructions = await sequelize
            .query(plantingInstructionFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)

                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingInstructions == []
        ) {
            res.send(200, {
                rows: [],
                count: 0
            })
            
            return
        }

       
        let tableJoins = {
            'entry' : ` LEFT JOIN
                        experiment.entry entry ON entry.id = "plantingInstruction".entry_id
                        AND entry.is_void = FALSE `,
            'plot' :  ` LEFT JOIN
                        experiment.plot plot ON plot.id = "plantingInstruction".plot_id
                        AND plot.is_void = FALSE`,
            'germplasm': `  LEFT JOIN
                            germplasm.germplasm germplasm ON germplasm.id = "plantingInstruction".germplasm_id
                            AND germplasm.is_void = FALSE `,
            'package': ` LEFT JOIN
                         germplasm.package package ON "plantingInstruction".package_id = package.id `,
            'creator': ' JOIN tenant.person creator ON plot.creator_id = creator.id',
            'modifier':' LEFT JOIN tenant.person modifier ON plot.modifier_id = modifier.id'
        }

        let plantingInstructionCountFinalSqlQuery = await processQueryHelper
            .getTotalCountQuery(
                req.body,
                excludedParametersArray,
                conditionString, //filter string
                distinctString, //distinct string used in the main query
                tableJoins, //join statements used in the resource
                'experiment.planting_instruction "plantingInstruction"',
                tableAliases, //table aliases involved in the join statements
                '"plantingInstruction".id', //main column
                columnAliases, //columns used in the filter condition,
                orderQuery,
                responseColString
            )

        let plantingInstructionsCount = await sequelize
            .query(
                plantingInstructionCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if(plantingInstructions !== undefined){
            count = plantingInstructionsCount[0].count

            res.send(200, {
                rows: plantingInstructions,
                count: count
            })
        }
        

        return
    }
}