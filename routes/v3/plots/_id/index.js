/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let harvestStatusHelper = require('../../../../helpers/harvestStatus/index.js')
let variableHelper = require('../../../../helpers/variable/index.js')
let errorBuilder = require('../../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
const logger = require('../../../../helpers/logger/index.js')
const endpoint = 'plots/:id'

module.exports = {

    // Endpoint for updating an existing plot record
    // PUT /v3/plots/:id
    put: async function (req, res, next) {

        // Retrieve the plot ID
        let plotDbId = req.params.id

        if (!validator.isInt(plotDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let locationDbId = null
        let plotCode = null
        let plotType = null
        let plotStatus = null
        let plotQcCode = null
        let rep = null
        let designX = null
        let designY = null
        let paX = null
        let paY = null
        let fieldX = null
        let fieldY = null
        let occurrenceDbId = null
        let entryDbId = null
        let harvestStatus = null
        let validateStatus = false
        let validateStatusValues = [ 'true', 'false' ]

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }
        // If personDbId is null, return error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if occurrence is existing
        // Build query
        let plotQuery = `
            SELECT
                location.id AS "locationDbId",
                plot.plot_code AS "plotCode",
                plot.plot_number AS "plotNumber",
                plot.plot_type AS "plotType",
                plot.plot_status AS "plotStatus",
                plot.harvest_status AS "harvestStatus",
                plot.plot_qc_code AS "plotQcCode",
                plot.rep,
                plot.design_x AS "designX",
                plot.design_y AS "designY",
                plot.pa_x AS "paX",
                plot.pa_y AS "paY",
                plot.field_x AS "fieldX",
                plot.field_y AS "fieldY",
                occurrence.id AS "occurrenceDbId",
                entry.id AS "entryDbId",
                creator.id AS "creatorDbId"
            FROM
                experiment.plot plot
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
            LEFT JOIN
                experiment.location location ON location.id = plot.location_id
            LEFT JOIN
                experiment.entry entry ON entry.id = plot.entry_id
            LEFT JOIN
                tenant.person creator ON creator.id = plot.creator_id
            WHERE
                plot.is_void = FALSE AND
                plot.id = ${plotDbId}
        `

        // Retrieve plot from the database
        let plot = await sequelize.query(plotQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError("err1: " + err))
                return
            })

        if (await plot === undefined || await plot.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404013)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordLocationDbId = (plot[0].locationDbId == undefined) ? null : plot[0].locationDbId
        let recordPlotCode = plot[0].plotCode
        let recordPlotNumber = plot[0].plotNumber
        let recordPlotType = plot[0].plotType
        let recordPlotStatus = plot[0].plotStatus
        let recordHarvestStatus = plot[0].harvestStatus
        let recordPlotQcCode = plot[0].plotQcCode
        let recordRep = (plot[0].rep == undefined) ? null : plot[0].rep

        let recordDesignX = (plot[0].designX == undefined) ? null : plot[0].designX
        let recordDesignY = (plot[0].designY == undefined) ? null : plot[0].designY
        let recordPaX = (plot[0].paX == undefined) ? null : plot[0].paX
        let recordPaY = (plot[0].paY == undefined) ? null : plot[0].paY
        let recordFieldX = (plot[0].fieldX == undefined) ? null : plot[0].fieldX
        let recordFieldY = (plot[0].fieldY == undefined) ? null : plot[0].fieldY

        let recordEntryDbId = plot[0].entryDbId
        let recordOccurrenceDbId = plot[0].occurrenceDbId
        let recordCreatorDbId = plot[0].creatorDbId

        // Check if user is an admin or an owner of the plot
        // Remove checking of permission to enable collaborator role update during 22.05
        /**
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                WHERE 
                    occurrence.id = ${recordOccurrenceDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }
        */

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let plotUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/plots/"
            + plotDbId

        // Parse the input
        let data = req.body    

        // Get validateStatus flag value
        if (data.validateStatus !== undefined) {
            // Check if validate status value is boolean
            if (!validateStatusValues.includes(data.validateStatus.toLowerCase())) {
                let errMsg = 'Invalid value for validateStatus. Accepted values are: true or false.'
                res.send(new errors.BadRequestError(errMsg))
            }

            validateStatus = JSON.parse(data.validateStatus.toLowerCase())
        }

        try {
            // Check flag for running the harvest status validation
            if (validateStatus) {
                // Perform validation and apply correct harvest status
                await harvestStatusHelper.validatePlotHarvestStatus(req, res, plotDbId)
            }
            else {
                let setQuery = ``
                let validateQuery = ``
                let validateCount = 0

                /** Validation of parameters and set values **/

                if (data.locationDbId !== undefined) {
                    locationDbId = data.locationDbId

                    if (!validator.isInt(locationDbId)) {
                        let errMsg = `Invalid format, location ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if user input is same with the current value in the database
                    if (locationDbId != recordLocationDbId) {

                        // Validate location ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if location is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    experiment.location location
                                WHERE 
                                    location.is_void = FALSE AND
                                    location.id = ${locationDbId}
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            location_id = $$${locationDbId}$$ 
                        `
                    }
                }

                if (data.plotCode !== undefined) {
                    plotCode = data.plotCode

                    // Check if user input is same with the current value in the database
                    if (plotCode != recordPlotCode) {

                        // Validate plot code
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if plotCode-occurrenceDbId pair does not exist return 1
                                SELECT 
                                    CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                    END AS count
                                FROM 
                                    experiment.plot plot
                                WHERE 
                                    plot.is_void = FALSE AND
                                    plot.occurrence_id = ${recordOccurrenceDbId} AND
                                    plot.plot_code = $$${plotCode}$$
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            plot_code = $$${plotCode}$$ 
                        `
                    }
                }

                if (data.plotType !== undefined) {
                    plotType = data.plotType

                    if (plotType != recordPlotType) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            plot_type = $$${plotType}$$
                        `
                    }
                }

                if (data.plotStatus !== undefined) {
                    plotStatus = data.plotStatus.trim()

                    // Check if user input is same with the current value in the database
                    if (plotStatus != recordPlotStatus) {

                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if entry status is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    master.scale_value scaleValue
                                LEFT JOIN
                                    master.variable variable ON scaleValue.scale_id = variable.scale_id
                                WHERE
                                    variable.abbrev LIKE 'PLOT_STATUS' AND
                                    scaleValue.value ILIKE $$${plotStatus}$$
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            plot_status = $$${plotStatus}$$
                        `
                    }
                }

                if (data.harvestStatus !== undefined) {
                    harvestStatus = data.harvestStatus
            
                    // Check if user input is same with the current value in the database
                    if (harvestStatus != recordHarvestStatus) {
            
                        let validHarvestStatusValues = ['no_harvest', 'completed', 'failed', 'done', 'in_queue', 'in_progress', 'deletion_in_progress', 'revert_in_progress']

                        // retrieve HARVEST_STATUS scale values
                        let scaleValues = await variableHelper.getScaleValues('HARVEST_STATUS')

                        // set as validHarvestStatusValues
                        if(scaleValues){
                            validHarvestStatusValues = scaleValues.map(x => { return x.toLowerCase() })
                        }

                        // Validate plot status
                        if(!validHarvestStatusValues.includes(harvestStatus.toLowerCase())) {
                            let errMsg = `Invalid request, you have provided an invalid
                            value for harvest status.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            harvest_status = $$${harvestStatus}$$
                        `
                    }
                }

                if (data.plotQcCode !== undefined) {
                    plotQcCode = data.plotQcCode

                    // Check if user input is same with the current value in the database
                    if (plotQcCode != recordPlotQcCode) {

                        let validPlotQcCodeValues = ['G', 'Q', 'S']

                        // Validate plot qc code
                        if(!validPlotQcCodeValues.includes(plotQcCode.toLowerCase())) {
                            let errMsg = `Invalid request, you have provided an invalid
                            value for plot QC code.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            plot_qc_code = $$${plotQcCode}$$
                        `
                    }
                }

                if (data.rep !== undefined) {
                    rep = data.rep

                    if (!validator.isInt(rep)) {
                        let errMsg = `Invalid format, rep must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (rep != recordRep) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            rep = $$${rep}$$
                        `
                    }
                }

                if (data.occurrenceDbId !== undefined) {
                    occurrenceDbId = data.occurrenceDbId
                    let tempPlotCode = (data.plotCode !== undefined) ? plotCode : recordPlotCode

                    if (occurrenceDbId != "null") {
                        // Check if user input is same with the current value in the database
                        if (occurrenceDbId != recordOccurrenceDbId) {
            
                            // Validate occurrence ID
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if occurrence is existing return 1
                                    SELECT 
                                        count(1)
                                    FROM 
                                        experiment.occurrence occurrence
                                    WHERE 
                                        occurrence.is_void = FALSE AND
                                        occurrence.id = ${occurrenceDbId}
                                )
                            `
                            validateCount += 1

                            // Validate occurrenceDbId-recordDesignX-recordDesignY combination is unique
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if occurrenceDbId-designX-designY combination does not exist return 1
                                    SELECT 
                                        CASE WHEN
                                            (count(1) = 0)
                                            THEN 1
                                            ELSE 0
                                        END AS count
                                    FROM 
                                        experiment.plot plot
                                    WHERE 
                                        plot.is_void = FALSE AND
                                        plot.occurrence_id = ${occurrenceDbId} AND
                                        plot.design_x = ${recordDesignX} AND
                                        plot.design_y = ${recordDesignY} 
                                )
                            `
                            validateCount += 1

                            // Validate occurrenceDbId-plotCode pair is unique
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if occurrenceDbId-plotCode pair does not exist return 1
                                    SELECT 
                                        CASE WHEN
                                            (count(1) = 0)
                                            THEN 1
                                            ELSE 0
                                        END AS count
                                    FROM 
                                        experiment.plot plot
                                    WHERE 
                                        plot.is_void = FALSE AND
                                        plot.occurrence_id = ${occurrenceDbId} AND
                                        plot.plot_code = $$${tempPlotCode}$$
                                )
                            `
                            validateCount += 1

                            // Validate occurrenceDbId-plotNumber pair is unique
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if occurrenceDbId-plotNumber pair does not exist return 1
                                    SELECT 
                                        CASE WHEN
                                            (count(1) = 0)
                                            THEN 1
                                            ELSE 0
                                        END AS count
                                    FROM 
                                        experiment.plot plot
                                    WHERE 
                                        plot.is_void = FALSE AND
                                        plot.occurrence_id = ${occurrenceDbId} AND
                                        plot.plot_number = ${recordPlotNumber}
                                )
                            `
                            validateCount += 1
                
                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                occurrence_id = $$${occurrenceDbId}$$ 
                            `
                        }
                    }  
                    else {
                        occurrenceDbId = null
            
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            occurrence_id = NULL
                        `
                    }
                }
        
                if (data.entryDbId !== undefined) {
                    entryDbId = data.entryDbId
            
                    if (entryDbId != "null") {
                        // Check if user input is same with the current value in the database
                        if (entryDbId != recordEntryDbId) {
            
                            // Validate entry ID
                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if entry is existing return 1
                                    SELECT 
                                        count(1)
                                    FROM 
                                        experiment.entry entry
                                    WHERE 
                                        entry.is_void = FALSE AND
                                        entry.id = ${entryDbId}
                                )
                            `
                            validateCount += 1
                
                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                entry_id = $$${entryDbId}$$ 
                            `
                        }
                    }  else {
                        entryDbId = null
            
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            entry_id = NULL
                        `
                    }
                }

                /** Validation for x & y values
                 * if x and y are going to be updated, check if user input x-y pair exists
                 * else if x is the only one to be updated, check if user input x-db value y pair exists
                 * else if y is the only one to be updated, check if user input y-db value x pair exists
                 * */
                let occDbId 
                if (data.designX !== undefined && data.designY !== undefined) {
                    designX = data.designX
                    designY = data.designY
                    occDbId = (data.occurrenceDbId !== undefined) ? occurrenceDbId : recordOccurrenceDbId
                    if (occurrenceDbId == "null") occDbId = null

                    if (!validator.isInt(designX)) {
                        let errMsg = `Invalid format, designX field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (!validator.isInt(designY)) {
                        let errMsg = `Invalid format, designY field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate design x and y 
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if occurrenceDbId-designX-designY combination does not exist return 1
                            SELECT 
                                CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                END AS count
                            FROM 
                                experiment.plot plot
                            WHERE 
                                plot.is_void = FALSE AND
                                plot.occurrence_id = ${occDbId} AND
                                plot.design_x = ${designX} AND
                                plot.design_y = ${designY} 
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        design_x = $$${designX}$$,
                        design_y = $$${designY}$$
                    `
                } else if (data.designX !== undefined) {
                    designX = data.designX
                    occDbId = (data.occurrenceDbId !== undefined) ? occurrenceDbId : recordOccurrenceDbId
                    if (occurrenceDbId == "null") occDbId = null

                    if (!validator.isInt(designX)) {
                        let errMsg = `Invalid format, designX field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (designX != recordDesignX) {
                        // Validate design x
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if occurrenceDbId-designX-designY combination does not exist return 1
                                SELECT 
                                    CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                    END AS count
                                FROM 
                                    experiment.plot plot
                                WHERE 
                                    plot.is_void = FALSE AND
                                    plot.occurrence_id = ${occDbId} AND
                                    plot.design_x = ${designX} AND
                                    plot.design_y = ${recordDesignY} 
                            )
                        `
                        validateCount += 1
                    
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            design_x = $$${designX}$$
                        `
                    }
                } else if (data.designY !== undefined) {
                    designY = data.designY
                    occDbId = (data.occurrenceDbId !== undefined) ? occurrenceDbId : recordOccurrenceDbId
                    if (occurrenceDbId == "null") occDbId = null

                    if (!validator.isInt(designY)) {
                        let errMsg = `Invalid format, designY field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (designY != recordDesignY) {
                
                        // Validate design y
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if occurrenceDbId-designX-designY combination does not exist return 1
                                SELECT 
                                    CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                    END AS count
                                FROM 
                                    experiment.plot plot
                                WHERE 
                                    plot.is_void = FALSE AND
                                    plot.occurrence_id = ${occDbId} AND
                                    plot.design_x = ${recordDesignX} AND
                                    plot.design_y = ${designY} 
                            )
                        `
                        validateCount += 1
                    
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            design_y = $$${designY}$$
                        `
                    }
                }

                if (data.paX !== undefined && data.paY !== undefined) {
                    paX = data.paX
                    paY = data.paY                

                    if (paX !== "null") {
                        if (!validator.isInt(paX)) {
                            let errMsg = `Invalid format, paX field must be an integer.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    } else {
                        paX = null;
                    }

                    if (paY !== "null") {
                        if (!validator.isInt(paY)) {
                            let errMsg = `Invalid format, paY field must be an integer.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    } else {
                        paY = null;
                    }

                    /*
                    TEMPORARILY REMOVED THIS VALIDATION
                    // Validate pa x and y 
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if locationDbId-paX-paY combination does not exist return 1
                            SELECT 
                                CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                END AS count
                            FROM 
                                experiment.plot plot
                            WHERE 
                                plot.is_void = FALSE AND
                                plot.location_id = ${recordLocationDbId} AND
                                plot.pa_x = ${paX} AND
                                plot.pa_y = ${paY} 
                        )
                    `
                    validateCount += 1
                    */

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        pa_x = ${paX},
                        pa_y = ${paY}
                    `
                    
                } else if (data.paX !== undefined) {

                    paX = data.paX

                    if (!validator.isInt(paX)) {
                        let errMsg = `Invalid format, paX field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (paX != recordPaX) {
                        /*
                        TEMPORARILY REMOVED THIS VALIDATION
                        // Validate pa x
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if locationDbId-paX-paY combination does not exist return 1
                                SELECT 
                                    CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                    END AS count
                                FROM 
                                    experiment.plot plot
                                WHERE 
                                    plot.is_void = FALSE AND
                                    plot.location_id = ${recordLocationDbId} AND
                                    plot.pa_x = ${paX} AND
                                    plot.pa_y = ${recordPaY} 
                            )
                        `
                        validateCount += 1
                        */

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            pa_x = $$${paX}$$
                        `
                    }
                } else if (data.paY !== undefined) {

                    paY = data.paY

                    if (!validator.isInt(paY)) {
                        let errMsg = `Invalid format, paY field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (paY != recordPaY) {
                        /*
                        TEMPORARILY REMOVED THIS VALIDATION
                        // Validate pa y
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if locationDbId-paX-paY combination does not exist return 1
                                SELECT 
                                    CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                    END AS count
                                FROM 
                                    experiment.plot plot
                                WHERE 
                                    plot.is_void = FALSE AND
                                    plot.location_id = ${recordLocationDbId} AND
                                    plot.pa_x = ${recordPaX} AND
                                    plot.pa_y = ${paY} 
                            )
                        `
                        validateCount += 1
                        */

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            pa_y = $$${paY}$$
                        `
                    }
                }

                if (data.fieldX !== undefined && data.fieldY !== undefined) {
                    fieldX = data.fieldX
                    fieldY = data.fieldY

                    if (fieldX !== "null") {
                        if (!validator.isInt(fieldX)) {
                            let errMsg = `Invalid format, fieldX field must be an integer.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    } else {
                        fieldX = null
                    }

                    if (fieldY !== "null") {
                        if (!validator.isInt(fieldY)) {
                            let errMsg = `Invalid format, fieldY field must be an integer.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    } else {
                        fieldY = null
                    }

                        /*
                        TEMPORARILY REMOVED THIS VALIDATION
                        // Validate field x and y 
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                        (
                            --- Check if locationDbId-fieldX-fieldY combination does not exist return 1
                            SELECT 
                                CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                END AS count
                            FROM 
                                experiment.plot plot
                            WHERE 
                                plot.is_void = FALSE AND
                                plot.location_id = ${recordLocationDbId} AND
                                plot.field_x = ${fieldX} AND
                                plot.field_y = ${fieldY} 
                        )
                    `
                    validateCount += 1
                    */

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        field_x = ${fieldX},
                        field_y = ${fieldY}
                    `
                } else if (data.fieldX !== undefined) {
                    fieldX = data.fieldX

                    if (!validator.isInt(fieldX)) {
                        let errMsg = `Invalid format, fieldX field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                
                    if (fieldX != recordFieldX) {
                    
                        /*
                        TEMPORARILY REMOVED THIS VALIDATION
                        // Validate field x
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if locationDbId-fieldX-fieldY combination does not exist return 1
                                SELECT 
                                    CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                    END AS count
                                FROM 
                                    experiment.plot plot
                                WHERE 
                                    plot.is_void = FALSE AND
                                    plot.location_id = ${recordLocationDbId} AND
                                    plot.field_x = ${fieldX} AND
                                    plot.field_y = ${recordFieldY} 
                            )
                        `
                        validateCount += 1
                        */
                    
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            field_x = $$${fieldX}$$
                        `
                    }
                } else if (data.fieldY !== undefined) {
                    fieldY = data.fieldY

                    if (!validator.isInt(fieldY)) {
                        let errMsg = `Invalid format, fieldY field must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (fieldY != recordFieldY) {
                    
                        /*
                        TEMPORARILY REMOVED THIS VALIDATION
                        // Validate field y
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if locationDbId-fieldX-fieldY combination does not exist return 1
                                SELECT 
                                    CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                    END AS count
                                FROM 
                                    experiment.plot plot
                                WHERE 
                                    plot.is_void = FALSE AND
                                    plot.location_id = ${recordLocationDbId} AND
                                    plot.field_x = ${recordFieldX} AND
                                    plot.field_y = ${fieldY} 
                            )
                        `
                        validateCount += 1
                        */

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            field_y = $$${fieldY}$$
                        `
                    }
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                if (validateCount > 0) {
                    let checkInputQuery = `
                        SELECT
                            ${validateQuery}
                        AS count
                    `
                    
                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    
                    if (inputCount[0]['count'] != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (setQuery.length > 0) setQuery += `,`

                // Update the plot record
                let updatePlotQuery = `
                    UPDATE
                        experiment.plot
                    SET
                        ${setQuery}
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE
                        id = ${plotDbId}
                `

                // Update plot record
                await sequelize.transaction(async transaction => {
                    return await sequelize.query(updatePlotQuery, {
                        type: sequelize.QueryTypes.UPDATE,
                        transaction: transaction,
                        raw: true
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'UPDATE', err)
                        throw new Error(err)
                    })
                })
            }

            // Return the transaction info
            let resultArray = {
                plotDbId: plotDbId,
                recordCount: 1,
                href: plotUrlString
            }

            if (!res.headersSent) {
                res.send(200, {
                    rows: resultArray
                })
                return
            }
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing plot record
    // DELETE /v3/plots/:id
    delete: async function (req, res, next) {
        // Retrieve plot ID
        let plotDbId = req.params.id

        if(!validator.isInt(plotDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let isAdmin = await userValidator.isAdmin(personDbId)

        try {
            let plotQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    occurrence.id AS "occurrenceDbId",
                    plot.plot_code AS "plotCode"
                FROM
                    experiment.plot plot
                LEFT JOIN
                    tenant.person creator ON creator.id = plot.creator_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                WHERE
                    plot.is_void = FALSE AND
                    plot.id = ${plotDbId}
            `

            // Retrieve plot from the database
            let plot = await sequelize.query(plotQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                logger.logFailingQuery(endpoint, 'SELECT', err)

                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            if (await plot === undefined || await plot.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404013)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordOccurrenceDbId = plot[0].occurrenceDbId
            let recordCreatorDbId = plot[0].creatorDbId
        
            // Check if user is an admin or an owner of the plot
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                    WHERE 
                        occurrence.id = ${recordOccurrenceDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `
        
                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deletePlotQuery = format(`
                UPDATE
                    experiment.plot plot
                SET
                    is_void = TRUE,
                    plot_code = 'VOIDED-${plotDbId}',
                    plot_number = CONCAT('-', $$${plotDbId}$$)::int,
                    plot_order_number = CONCAT('-', $$${plotDbId}$$)::int,
                    design_x = CONCAT('-', $$${plotDbId}$$)::int,
                    design_y = CONCAT('-', $$${plotDbId}$$)::int,
                    pa_x = CONCAT('-', $$${plotDbId}$$)::int,
                    pa_y = CONCAT('-', $$${plotDbId}$$)::int,
                    field_x = CONCAT('-', $$${plotDbId}$$)::int,
                    field_y = CONCAT('-', $$${plotDbId}$$)::int,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    plot.id = ${plotDbId}
            `)

            await sequelize.transaction( async transaction => {
                await sequelize.query(deletePlotQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                })
                .catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
            });

            res.send(200, {
                rows: { 
                    occurrenceDbId: recordOccurrenceDbId,
                    plotDbId: plotDbId 
                }
            })
            return

        } catch (err) {
            logger.logFailure(endpoint, 'DELETE', err)

            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for retrieving plot record
    // GET /v3/plots/:id
    get: async function (req, res, next) {
        let accessDataQuery = ''

        // retrieve the plot ID
        let plotDbId = req.params.id

        if (!validator.isInt(plotDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if user is admin or not
        let isAdmin = await userValidator.isAdmin(personDbId)

        // if not admin, build access data condition
        if(!isAdmin){
            // get all programs that user belongs to
            let getProgramsQuery =  `
                SELECT 
                    program.id 
                FROM 
                    tenant.program program,
                    tenant.program_team pt,
                    tenant.team_member tm
                WHERE
                    tm.person_id = ${personDbId}
                    AND pt.team_id = tm.team_id
                    AND program.id = pt.program_id
                    AND tm.is_void = FALSE
                    AND pt.is_void = FALSE
                    AND program.is_void = FALSE
            `
            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                accessDataQuery += `occurrence.access_data #> $$\{program,${programObj.id}}$$ is not null`

            }

            // include occurrences shared to the user
            let userQuery = `occurrence.access_data #> $$\{person,${personDbId}}$$ is not null`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            }

            if(accessDataQuery != ``){
                accessDataQuery = ` AND (${accessDataQuery})`
            }
        }

        // build query
        let plotQuery = `
            SELECT
                plot.id AS "plotDbId",
                plot.location_id AS "locationDbId",
                plot.occurrence_id AS "occurrenceDbId",
                entry.id AS "entryDbId",
                entry.entry_number AS "entryNumber",
                entry.entry_type AS "entryType",
                plot.plot_code AS "plotCode",
                plot.plot_qc_code AS "plotQcCode",
                plot.plot_status AS "plotStatus",
                plot.plot_number AS "plotNumber",
                plot.plot_type AS "plotType",
                plot.rep AS "rep",
                plot.design_x AS "designX",
                plot.design_y AS "designY",
                plot.field_x AS "fieldX",
                plot.field_y AS "fieldY",
                plot.pa_x AS "paX",
                plot.pa_y AS "paY",
                plot.block_number AS "blockNumber",
                plot.harvest_status AS "harvestStatus",
                occurrence.experiment_id AS "experimentDbId",
                entry.entry_name AS "entryName",
                entry.germplasm_id AS "germplasmDbId",
                germplasm.designation AS "designation",
                germplasm.parentage AS "parentage",
                germplasm.germplasm_state AS "state",
                crop.id AS "cropDbId",
                crop.crop_code AS "cropCode",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier"
            FROM
                experiment.occurrence occurrence,
                experiment.plot plot
                LEFT JOIN
                    experiment.entry entry ON entry.id = plot.entry_id
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = entry.germplasm_id
                LEFT JOIN
                    tenant.crop crop ON crop.id = germplasm.crop_id
                LEFT JOIN
                    tenant.person creator ON plot.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON plot.modifier_id = modifier.id
            WHERE
                plot.is_void = FALSE
                AND occurrence.id = plot.occurrence_id
                AND plot.id = ${plotDbId}
                ${accessDataQuery}
        `

        // Retrieve plot from the database   
        let plot = await sequelize.query(plotQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (await plot === undefined || await plot.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404013)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        res.send(200, {
            rows: plot
        })
        return

    }
}