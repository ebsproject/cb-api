/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let jwtDecode = require('jwt-decode')
let aclStudy = require('../../../../helpers/acl/study.js')
let studyModel = require('../../../../helpers/study/index.js')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/plots/:id/data
    get: async function (req, res, next) {

        // Retrieve the plot ID

        let plotDbId = req.params.id

        if (!validator.isInt(plotDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            // Endpoint for retrieving the list of files for a study
            let userId = await tokenHelper.getUserId(req)

            if (userId == null) {
              let errMsg = await errorBuilder.getError(req.headers.host, 401002)
              res.send(new errors.BadRequestError(errMsg))
              return
            }

            let applyAcl = true

            // Check if user is an administrator. If yes, apply ACL.
            if (await userValidator.isAdmin(userId)) {
                applyAcl = false
            }

            if (applyAcl == true) {

                let canAccess = await aclStudy.isStudyAccessible('plot', '' + plotDbId, 'user', '' + userId)

                if (!canAccess) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401005)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            // Check if the study is committed to set the entry and plot tables
            let isCommitted = req.params.committed

            let plotTable = 'plot'
            let entryTable = 'entry'
            let plotDbIdName = 'plotDbId'
            let entryDbIdName = 'entryDbId'
            if ((isCommitted != null) && (isCommitted.toUpperCase() == 'FALSE')) {
                plotTable = 'temp_plot'
                entryTable = 'temp_entry'
                plotDbIdName = 'tempPlotDbId'
                entryDbIdName = 'tempEntryDbId'
            }

            let plotQuery = `
                SELECT  
                    plot.id AS "` + plotDbIdName + `",
                    entry.id AS "` + entryDbIdName + `",
                    plot.key,
                    entry.entno,
                    entry.metno,
                    entry.product_id AS "productDbId",
                    entry.product_name AS "productName",
                    plot.plotno,
                    entry.product_gid AS "productGID",
                    plot.rep, 
                    entry.entcode,
                    plot.code,
                    product.id AS "productDbId",
                    RTRIM(product.parentage) AS parentage,
                    product.generation,
                    plot.design_x AS "designX",
                    plot.design_y AS "designY",
                    plot.map_x AS "mapX",
                    plot.map_y AS "mapY",
                    plot.field_x AS "fieldX",
                    plot.field_y AS "fieldY",
                    plot.block_no AS "blockNo",
                    plot.remarks,
                    plot.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    plot.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier
                FROM 
                    operational.study study,
                    operational.` + entryTable + ` entry
                LEFT JOIN 
                    master.product product ON product.id = entry.product_id,
                    operational.`+ plotTable + ` plot
                LEFT JOIN 
                    master.user creator ON creator.id = plot.creator_id
                LEFT JOIN 
                    master.user modifier ON  modifier.id = plot.modifier_id
                WHERE 
                    plot.id = (:plotDbId)
                    AND plot.study_id = study.id
                    AND plot.is_void = FALSE
                    AND plot.entry_id = entry.id
                    AND entry.is_void = FALSE 
                    AND entry.is_active = TRUE
                    AND study.is_void = FALSE
            `

            // Retrieve plot from the database   
            let plots = await sequelize.query(plotQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    plotDbId: plotDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await plots == undefined || await plots.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404013)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (plot of plots) {

                let plotDataQuery = `
                    SELECT
                        "plotData".variable_id AS "variableDbId",
                        variable.abbrev AS "variableAbbrev",
                        variable.label AS "variableLabel",
                        "plotData".value AS "variableValue",
                        "plotData".creation_timestamp AS "creationTimestamp",
                        creator.id AS "creatorDbId",
                        creator.display_name AS creator,
                        "plotData".modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.display_name AS modifier
                    FROM 
                        operational.` + plotTable + `_data "plotData"
                    LEFT JOIN 
                        master.variable variable ON variable.id = "plotData".variable_id
                    LEFT JOIN 
                        master.user creator ON "plotData".creator_id = creator.id
                    LEFT JOIN 
                        master.user modifier ON "plotData".modifier_id = modifier.id
                    WHERE 
                        "plotData".is_void = FALSE
                        AND "plotData".plot_id = (:plotDbId)
                        AND variable.abbrev NOT IN (
                            'MAP_X','MAP_Y','FLDROW_CONT','FLDCOL_CONT','BLOCK_NO_CONT'
                        )
                    ORDER BY variable.abbrev
                `

                let plotData = await sequelize.query(plotDataQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        plotDbId: plot[plotDbIdName]
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                plot["data"] = plotData
            }

            res.send(200, {
                rows: plots
            })
            return
        }
    },
}
