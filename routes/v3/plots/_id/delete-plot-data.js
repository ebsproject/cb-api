/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let format = require('pg-format')

module.exports = {

    /**
     * POST /v3/plots/:id/delete-plot-data allows the deletion of all plot data 
     * under a specific plot given the plot ID
     * 
     * @param plotDbId integer as a path parameter 
     * @param token string token
     * 
     * @return object response data
     */

    post: async function (req, res, next) {
        
        // Retrieve plot ID
        let plotDbId = req.params.id

        if (!validator.isInt(plotDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let plotQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    occurrence.id AS "occurrenceDbId",
                    plot.plot_code AS "plotCode"
                FROM
                    experiment.plot plot
                LEFT JOIN
                    tenant.person creator ON creator.id = plot.creator_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                WHERE
                    plot.is_void = FALSE AND
                    plot.id = ${plotDbId}
            `

            // Retrieve plot from the database
            let plot = await sequelize.query(plotQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await plot === undefined || await plot.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404013)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordOccurrenceDbId = plot[0].occurrenceDbId
            let recordCreatorDbId = plot[0].creatorDbId

            // Check if user is an admin or an owner of the plot
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                    WHERE 
                        occurrence.id = ${recordOccurrenceDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `
        
                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let plotDataQuery = `
                SELECT 
                    id
                FROM
                    experiment.plot_data
                WHERE
                    is_void = FALSE AND
                    plot_id = ${plotDbId}
                ORDER BY
                    id
            `

            let plotDataRecords = await sequelize.query(plotDataQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            let resultArray = []
            let plotDataObj = {}

            if (plotDataRecords.length != 0) {
                let deletePlotDataQuery = format(`
                    UPDATE
                        experiment.plot_data
                    SET
                        is_void = TRUE,
                        notes = CONCAT('VOIDED-', $$id$$),
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE
                        plot_id = ${plotDbId}
                `)

                await sequelize.query(deletePlotDataQuery, {
                    type: sequelize.QueryTypes.UPDATE
                })

                await transaction.commit()

                for (plotData of plotDataRecords) {
                    plotDataObj = {
                        plotDataDbId: plotData.id
                    }
                    resultArray.push(plotDataObj)
                }
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}