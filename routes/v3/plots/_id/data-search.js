/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        let plotDbId = req.params.id

        if (!validator.isInt(plotDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plotQuery = `
            SELECT
                plot.id AS "plotDbId",
                plot.plot_code AS "plotCode",
                plot.plot_number AS "plotNumber",
                plot.plot_type AS "plotType",
                plot.harvest_status AS "harvestStatus",
                plot.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                plot.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier"
            FROM
                experiment.plot plot
            LEFT JOIN
                tenant.person creator ON plot.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON plot.modifier_id = modifier.id
            WHERE
                plot.is_void = FALSE AND
                plot.id = ${plotDbId}
        `
        
        let plot = await sequelize.query(plotQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (plot == undefined || plot.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404013)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

           // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper.getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the excluded parameters
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the plot data retrieval query
        let plotDataQuery = null

        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                plotDataQuery = knex.select(selectString)
            } else {
                plotDataQuery = knex.column(parameters['fields'].split('|'))
            }

            plotDataQuery += `
                FROM
                    experiment.plot_data plot_data
                LEFT JOIN
                    experiment.plot plot ON plot.id = plot_data.plot_id
                LEFT JOIN
                    master.variable variable ON variable.id = plot_data.variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = plot_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = plot_data.modifier_id
                WHERE
                    plot_data.is_void = FALSE AND
                    plot.id = ${plotDbId}
            `
        } else {
            plotDataQuery = `
                SELECT
                    ${addedDistinctString}
                    plot_data.id::text AS "plotDataDbId",
                    plot_data.plot_id::text AS "plotDbId",
                    plot_data.variable_id::text AS "variableDbId",
                    variable.abbrev AS "variableAbbrev",
                    variable.label AS "variableLabel",
                    plot_data.data_value AS "dataValue",
                    plot_data.data_qc_code AS "dataQCCode",
                    plot_data.transaction_id::text AS "transactionDbId",
                    plot_data.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    plot_data.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier,
                    plot_data.collection_timestamp AS "collectionTimestamp",
                    plot_data.remarks
                FROM
                    experiment.plot_data plot_data
                LEFT JOIN
                    experiment.plot plot ON plot.id = plot_data.plot_id
                LEFT JOIN
                    master.variable variable ON variable.id = plot_data.variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = plot_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = plot_data.modifier_id
                WHERE
                    plot_data.is_void = FALSE AND
                    plot.id = ${plotDbId}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let plotDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plotDataQuery,
            conditionString,
            orderString
        )

        let plotData = await sequelize.query(plotDataFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset,
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        plot[0]['data'] = plotData

        let plotDataCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            plotDataQuery,
            conditionString,
            orderString
        )

        let plotDataCount = await sequelize.query(plotDataCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        count = plotDataCount[0].count

        res.send(200, {
            rows: plot,
            count: count
        })
        return
    }
}