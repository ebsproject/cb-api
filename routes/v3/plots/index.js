/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let validator = require('validator')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')
let errors = require('restify-errors')
let logger = require('../../../helpers/logger')
const endpoint = 'plots'

module.exports = {
    // Endpoint for creating a plot record
    // POST /v3/plots
    post: async function (req, res, next) {
        let autoGeneratePlotNumber = true

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        // Retrieve the ID of the client via the access token
        let personDbId = await tokenHelper.getUserId(req, res)

         // If personDbId is undefined, terminate
        if (personDbId === undefined) {
            return
        }
        // If personDbId is null, return 401: User not found error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isSecure = forwarded(req, req.headers).secure

        let resultArray = []

        // Set the URL for response
        let plotUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/plots'

        // Parse the input
        let data = req.body
        let records = []
        let recordCount = 0

        records = data.records
        recordCount = records.length
        
        if (data.autoGeneratePlotNumber != undefined) {
            autoGeneratePlotNumber = data.autoGeneratePlotNumber
        }

        if (records == undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (typeof autoGeneratePlotNumber !== 'boolean') {
            let errMsg = 'Invalid request, autoGeneratePlotNumber must be a boolean.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let plotValuesArray = []
            let occInfo
            let plotNumber = 0
            let plotCode = null
            let occIds = []

            let visited = {
                plotNumber: [],
                design: []
            }

            for (let record of records) {
                let validateQuery = ''
                let validateCount = 0

                // Declare variables
                let occurrenceDbId = null // **
                let entryDbId = null // **
                let plotType = null // **
                let rep = null // **
                let plotStatus = null // **
                let plotQcCode = null
                let designX = null
                let designY = null
                let blockNumber = null
                let notes = null

                if (
                    record.plotType == undefined ||
                    record.rep == undefined ||
                    record.plotStatus == undefined ||
                    record.occurrenceDbId == undefined
                ) {
                    let errMsg = 'Required parameters are missing. Ensure that the plot type, rep, occurrenceDbId, and plot status fields are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                if (autoGeneratePlotNumber == false && record.plotNumber == undefined) {
                    let errMsg = 'Required parameters are missing. Ensure that the plot type, rep, plot status, and plot number fields are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                rep = record.rep
                plotType = record.plotType
                occurrenceDbId = record.occurrenceDbId
                plotStatus = record.plotStatus.trim()

                // Validate rep
                if (!validator.isInt(rep)) {
                    let errMsg = 'Invalid request, rep must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate plotStatus
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'PLOT_STATUS' AND
                            scaleValue.value ILIKE $$${plotStatus}$$
                    )
                `
                validateCount += 1

                if (record.occurrenceDbId != undefined) {
                    if (!validator.isInt(occurrenceDbId)) {
                        let errMsg = 'Invalid format, occurrence ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if occurrence exists
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if occurrence exists, return 1
                            SELECT
                                count(1)
                            FROM
                                experiment.occurrence occurrence
                            WHERE
                                occurrence.is_void = FALSE AND
                                occurrence.id = ${occurrenceDbId}
                        )
                    `

                    validateCount += 1
                }

                if (record.entryDbId != undefined) {
                    entryDbId = record.entryDbId
                    if (!validator.isInt(entryDbId)) {
                        let errMsg = 'Invalid format, entry ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if entry exists
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry exists, return 1
                            SELECT
                                count(1)
                            FROM
                                experiment.entry entry
                            WHERE
                                entry.is_void = FALSE AND
                                entry.id = ${entryDbId}
                        )
                    `

                    validateCount += 1
                }


                if (record.plotQcCode != undefined) {
                    plotQcCode = record.plotQcCode

                    const validPlotQcCode = ['G', 'Q', 'S']

                    // Check if valid qc code
                    if (!validPlotQcCode.includes(plotQcCode.toUpperCase())) {
                        let errMsg = 'Invalid request, you have provided an invalid value for plot QC code.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.blockNumber != undefined) {
                    blockNumber = record.blockNumber

                    if (!validator.isInt(blockNumber)) {
                        let errMsg = 'Invalid request, block number must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // designX
                if (record.designX != undefined) {
                    designX = record.designX
                    // Check if designX is a valid integer
                    if (!validator.isInt(designX)) {
                        let errMsg = 'Invalid request, value for design X must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // designY
                if (record.designY != undefined) {
                    designY = record.designY
                    // Check if designY is a valid integer
                    if (!validator.isInt(designY)) {
                        let errMsg = 'Invalid request, value for design Y must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // Check if occurrence, designX, and designY combination is unique
                if (designX != null && designY != null && occurrenceDbId != null) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the occurrence, designX, and designY combination is unique
                            SELECT
                                CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                                END AS count
                            FROM
                                experiment.plot plot
                            WHERE
                                plot.is_void = FALSE AND
                                plot.occurrence_id = ${occurrenceDbId} AND
                                plot.design_x = ${designX} AND
                                plot.design_y = ${designY}
                        )
                    `
                    validateCount += 1
                }

                notes = (record.notes != undefined) ? record.notes : null

                if (validateQuery.length != 0) {
                    let checkInputQuery = `
                        SELECT ${validateQuery} AS count
                    `
                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    
                    if (inputCount[0]['count'] != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                let maxPlotNumberQuery = `
                    SELECT
                        CASE WHEN
                            (count(1) = 0)
                            THEN
                                1
                            ELSE
                                MAX(plot.plot_number) + 1  
                        END AS "maxPlotNumber",
                        count(id) AS "plotCount"
                    FROM
                        experiment.plot plot
                    WHERE
                        plot.is_void = FALSE AND
                        plot.occurrence_id = ${occurrenceDbId}
                `

                let plotNumberCheck = await sequelize.query(maxPlotNumberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if(plotNumberCheck == undefined){
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (autoGeneratePlotNumber == false) {
                    plotNumber = record.plotNumber
                } else {
                    if (plotNumberCheck[0].plotCount == 0) {

                        if(!occIds.includes(occurrenceDbId)){ //restart plotNumber if new occurrenceDbId is encountered
                            occIds.push(occurrenceDbId)
                            plotNumber = 1
                        } else {
                            plotNumber += 1
                        }

                    } else {
                        plotNumber = parseInt(plotNumberCheck[0].maxPlotNumber)
                        // if bulk creation, it will get the last visited plot number + 1
                        if (records.indexOf(record) > 0) {
                            let lastPlotNumberCombi = visited.plotNumber[visited.plotNumber.length - 1].split("-")
                            plotNumber = parseInt(lastPlotNumberCombi[1]) + 1
                        }
                    }
                }

                occInfo = {
                    plotNumber: `${occurrenceDbId}-${plotNumber}`,
                    design: `${occurrenceDbId}-${designX}-${designY}`
                }

                if (recordCount > 1) {
                    if (visited.plotNumber.includes(occInfo.plotNumber)) {
                        plotNumber += 1
                    } else {
                        visited.plotNumber.push(occInfo.plotNumber)
                    }

                    occInfo.plotNumber = `${occurrenceDbId}-${plotNumber}`

                    if (visited.design.includes(occInfo.design)) {
                        if (designX != null && designY != null) {
                            let errMsg = `You have provided duplicate occurrenceDbId-designX-designY combination.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    } else {
                        visited.design.push(occInfo.design)
                    }
                }

                // check if plotcode value is defined
                if (record.plotCode != undefined) {
                    plotCode = record.plotCode
                } else {
                    plotCode = plotNumber
                }

                // Get values
                let tempArray = [
                    occurrenceDbId,
                    entryDbId,
                    plotCode,
                    plotNumber,
                    plotType,
                    rep,
                    designX,
                    designY,
                    plotStatus,
                    plotQcCode,
                    personDbId,
                    notes,
                    blockNumber
                ]

                plotValuesArray.push(tempArray)
            }

            let plotQuery = format(`
                INSERT INTO experiment.plot (
                    occurrence_id,
                    entry_id,
                    plot_code,
                    plot_number,
                    plot_type,
                    rep,
                    design_x,
                    design_y,
                    plot_status,
                    plot_qc_code,
                    creator_id,
                    notes,
                    block_number
                )
                VALUES
                    %L
                RETURNING id`, plotValuesArray
            )
            console.log(plotQuery)
            let plots = await sequelize.transaction(async transaction => {
                return await sequelize.query(plotQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (let plot of plots[0]) {
                plotDbId = plot.id

                // Return the plot info
                let plotObj = {
                    plotDbId: plotDbId,
                    recordCount: 1,
                    href: plotUrlString + '/' + plotDbId
                }

                resultArray.push(plotObj)
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}