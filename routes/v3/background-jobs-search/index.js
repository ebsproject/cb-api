/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger/index')
let endpoint = 'background-jobs-search'

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let reqQuery = req.query ?? {}
        let sort = reqQuery.sort ?? null
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        let addedOrderString = `ORDER BY backgroundJob.id`
        parameters['distinctOn'] = ''

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get filter condition
            conditionString = await processQueryHelper
                .getFilter(req.body, excludedParametersArray)

            if(conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                let addedOrder = await processQueryHelper.getAddedOrderString(parameters['distinctOn'])
                addedDistinctString = await processQueryHelper
                  .getDistinctString(parameters['distinctOn'])
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                  ORDER BY
                    ${addedOrder},
                    backgroundJob.id DESC 
                `
            }
        }

        // Build base retrieval query
        let backgroundJobsQuery = null

        // Check if client specified values for fields
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                  .getFieldValuesString(parameters['fields'])
                
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                backgroundJobsQuery = knex.select(selectString)
            } else {
                backgroundJobsQuery = knex.column(parameters['fields'].split('|'))
            }

            backgroundJobsQuery += `
            FROM
                api.background_job backgroundJob
            LEFT JOIN
                tenant.person creator ON backgroundJob.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON backgroundJob.modifier_id = modifier.id
            WHERE
                backgroundJob.is_void = FALSE
            ${addedOrderString}
            `
        } else {
            backgroundJobsQuery = `
            SELECT
                ${addedDistinctString}
                backgroundJob.id AS "backgroundJobDbId",
                backgroundJob.worker_name AS "workerName",
                backgroundJob.description,
                backgroundJob.job_status AS "jobStatus",
                backgroundJob.message,
                backgroundJob.start_time AS "startTime",
                backgroundJob.end_time AS "endTime",
                backgroundJob.is_seen AS "isSeen",
                backgroundJob.entity_id as "entityDbId",
                backgroundJob.entity,
                backgroundJob.endpoint_entity as "endpointEntity",
                backgroundJob.method,
                backgroundJob.application,
                backgroundJob.remarks AS "jobRemarks",
                backgroundJob.notes AS "successfulIds",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                backgroundJob.creation_timestamp AS "creationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier",
                backgroundJob.modification_timestamp AS "modificationTimestamp"
            FROM
                api.background_job backgroundJob
            LEFT JOIN
                tenant.person creator ON backgroundJob.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON backgroundJob.modifier_id = modifier.id
            WHERE
                backgroundJob.is_void = FALSE
            ${addedOrderString}
            `
        }

        // Parse sort parameters
        if (sort != undefined) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build final query
        let newFinalQuery = `
            WITH baseQuery AS (
                ${backgroundJobsQuery}
            ),
            withFilter AS (
                SELECT
                    baseQuery.*
                FROM
                    baseQuery
                ${conditionString}
            ),
            withSort AS (
                SELECT
                    withFilter.*
                FROM
                    withFilter
                ${orderString}
            ),
            withLimit AS (
                SELECT
                    withSort.*
                FROM
                    withSort
                LIMIT (:limit) OFFSET (:offset)
            ),
            countQuery AS (
                SELECT
                    COUNT(1) AS "totalCount"
                FROM
                    withFilter
            )
            
            SELECT
                countQuery."totalCount",
                withLimit.*
            FROM
                countQuery,
                withLimit
        `
      
        // Retrieve background job records
        let backgroundJobs = await sequelize.query(newFinalQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch (async err => {
                // Log error when query fails
                await logger.logFailingQuery(endpoint, 'SELECT', err)
            })

        // Return error when query fails
        if (await backgroundJobs === undefined) {
            errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // If query returns empty, return empty values
        if (await backgroundJobs.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get total count
        count = backgroundJobs[0] != null && backgroundJobs[0]["totalCount"] != null ? backgroundJobs[0]["totalCount"] : 0 

        res.send(200, {
            rows: backgroundJobs,
            count: count
        })
        return
    }
}