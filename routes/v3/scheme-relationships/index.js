/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let schemeHelper = require('../../../helpers/scheme/index.js')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Endpoint for adding a scheme relation record
    post: async function (req, res, next) {

        // Default
        let resultArray = []
        let schemeDbId = null
        let phaseParentDbId = null
        let phaseChildDbId = null
        let stageParentDbId = null
        let stageChildDbId = null
        let projectRootDbId = null
        let phaseCount = 0
        let stageCount = 0

        // Retrieve the user ID of the client from the access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let schemeRelationUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/schemes"

        // Check parameters
        if (req.body != null) {

            // Parse the input
            let data = req.body
            let records = []

            try {
                records = data.records

                if (records == undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            } catch (e) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            // Start transaction
            let transaction

            try {
                // get transaction
                transaction = await sequelize.transaction({ autocommit: false })

                let projectDbId = null

                let visitedArray = []

                // Loop the records array
                for (var i = 0, recordsLen = records.length; i < recordsLen; i++) {

                    // Get the information for projects
                    code = records[i]['code']
                    name = records[i]['name']
                    description = records[i]['description']

                    // Check if the scheme data info is already visited
                    let schemeDataInfo = code + '-' + name

                    if (visitedArray.includes(schemeDataInfo)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400058)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else {
                        visitedArray.push(schemeDataInfo)
                    }

                    // Get the master.project ID
                    let projectQuery = `
                        SELECT
                            project.id AS "projectDbId"
                        FROM
                            master.project project
                        WHERE 
                            project.is_void = FALSE
                            AND (
                                project.abbrev = :projectAbbrev
                                OR project.name = :projectName
                            )
                    `

                    let projects = await sequelize.query(projectQuery, {
                        type: sequelize.QueryTypes.SELECT,
                        replacements: {
                            projectAbbrev: code,
                            projectName: name
                        }
                    })

                    if (projects.length != undefined && projects.length > 0) {
                        projectDbId = projects[0]['projectDbId']
                    } else {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400059)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Retrieve the scheme ID
                    schemeDbId = await schemeHelper.getSchemeDbId(code, name, description, 10, projectDbId, userDbId)

                    // Check if a relation is existing already for the scheme
                    let schemeRelationCountQuery = `
                        SELECT
                            count(1)
                        FROM
                            master.scheme_relation "schemeRelation"
                        WHERE
                            "schemeRelation".is_void = FALSE
                            AND "schemeRelation".root_scheme_id = :schemeDbId
                    `

                    let schemeRelationCounts = await sequelize.query(schemeRelationCountQuery, {
                        type: sequelize.QueryTypes.SELECT,
                        replacements: {
                            schemeDbId: schemeDbId
                        }
                    })

                    if (schemeRelationCounts.length != undefined && schemeRelationCounts.length > 0) {
                        let schemeRelationCount = schemeRelationCounts[0]['count']

                        if (schemeRelationCount != null && schemeRelationCount > 0) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400060)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }

                    // Assign the scheme ID to project root ID and phase parent ID
                    projectRootDbId = schemeDbId
                    phaseParentDbId = schemeDbId

                    // Loop the projects array
                    let schemeProjects = records[i]
                    for (var key in schemeProjects) {
                        if (key != 'code' && key != 'name' && key != 'description') {

                            // Get the phases
                            let phases = schemeProjects[key]
                            for (var j = 0, phasesLen = phases.length; j < phasesLen; j++) {

                                // Get the information for phases
                                code = phases[j]['code']
                                name = phases[j]['name']
                                description = phases[j]['description']

                                // Check if the scheme data info is already visited
                                let schemeDataInfo = code + '-' + name

                                if (visitedArray.includes(schemeDataInfo)) {
                                    let errMsg = await errorBuilder.getError(req.headers.host, 400058)
                                    res.send(new errors.BadRequestError(errMsg))
                                    return
                                } else {
                                    visitedArray.push(schemeDataInfo)
                                }

                                // Retrieve the scheme ID and assign as the phase child ID
                                phaseChildDbId = await schemeHelper.getSchemeDbId(code, name, description, 20, projectDbId, userDbId)

                                // Assign the phase child ID from phase as the stage parent ID
                                stageParentDbId = phaseChildDbId

                                // Count phases
                                phaseCount++

                                // Create the scheme relation between the project and phase
                                let phaseSchemeRelationDbId = await schemeHelper.createSchemeRelationRecord(projectRootDbId, phaseParentDbId, phaseChildDbId, phaseCount, userDbId)

                                if (phaseSchemeRelationDbId == null) {
                                    let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                                    res.send(new errors.InternalError(errMsg))
                                    return
                                }

                                // Loop the phase array
                                let phasesObj = phases[j]
                                for (var key in phasesObj) {
                                    if (key != 'code' && key != 'name' && key != 'description') {

                                        // Get the stages
                                        let stages = phasesObj[key]
                                        for (var k = 0, stagesLen = stages.length; k < stagesLen; k++) {

                                            // Get the information for stages
                                            code = stages[k]['code']
                                            name = stages[k]['name']
                                            description = stages[k]['description']

                                            // Check if the scheme data info is already visited
                                            let schemeDataInfo = code + '-' + name

                                            if (visitedArray.includes(schemeDataInfo)) {
                                                let errMsg = await errorBuilder.getError(req.headers.host, 400058)
                                                res.send(new errors.BadRequestError(errMsg))
                                                return
                                            } else {
                                                visitedArray.push(schemeDataInfo)
                                            }

                                            // Retrieve the scheme ID and assign as the stage child ID
                                            stageChildDbId = await schemeHelper.getSchemeDbId(code, name, description, 30, projectDbId, userDbId)

                                            // Count stages
                                            stageCount++

                                            // Create scheme relation between the project, phase and stage
                                            let stageSchemeRelationDbId = schemeHelper.createSchemeRelationRecord(projectRootDbId, stageParentDbId, stageChildDbId, stageCount, userDbId)

                                            if (stageSchemeRelationDbId == null) {
                                                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                                                res.send(new errors.InternalError(errMsg))
                                                return
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Return the scheme info
                    let array = {
                        schemeDbId: schemeDbId,
                        recordCount: 1,
                        href: schemeRelationUrlString + '/' + schemeDbId + '/tree'
                    }
                    resultArray.push(array)
                }

                // Commit the transaction
                await transaction.commit()

                res.send(200, {
                    rows: resultArray
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback();
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            }
        } else {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
    }
}