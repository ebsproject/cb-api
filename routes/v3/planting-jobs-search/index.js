/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let userValidator = require('../../../helpers/person/validator.js')

module.exports = {
    /**
     * Search method for experiment.plot_data
     * POST /v3/planting-jobs-search
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let accessDataQuery = ''
        let addedDistinctString = ''
        let hasAccessCondString = ''
        let hasAccessQueryString = ''
        let parameters = {}
        let programCond = ''
        parameters['distinctOn'] = ''

        // retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req);

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if user is admin or not
        let isAdmin = await userValidator.isAdmin(userId)

        // if not admin, retrieve only planting jobs user have access to
        if(!isAdmin){

            // if collaborator program is set, retrieve only shared to the collaborator program
            if(req.query.collaboratorProgramCode != null){
                programCond = ` AND program.program_code = '${req.query.collaboratorProgramCode}'`
            }

            // get all programs that user belongs to
            let getProgramsQuery =  `
            SELECT 
                program.id 
            FROM 
                tenant.program program,
                tenant.program_team pt,
                tenant.team_member tm
            WHERE
                tm.person_id = ${userId}
                AND pt.team_id = tm.team_id
                AND program.id = pt.program_id
                AND tm.is_void = FALSE
                AND pt.is_void = FALSE
                AND program.is_void = FALSE
                ${programCond}
            `
            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // if program does not exist
            if(programIds.length == 0){
                accessDataQuery = `occurrence.access_data #> $$\{program,0}$$ is not null`
            }

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                accessDataQuery += `occurrence.access_data #> $$\{program,${programObj.id}}$$ is not null`

            }

            // include occurrences shared to the user
            let userQuery = `occurrence.access_data #> $$\{person,${userId}}$$ is not null`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            }

            if(accessDataQuery != ``){
                accessDataQuery = ` AND (${accessDataQuery})`
            }

            // retrieve occurrences user has access to
            let occurrenceIdsQuery = `
                SELECT 
                    ARRAY_AGG(occurrence.id) AS occurrenceids
                FROM
                    experiment.occurrence occurrence
                WHERE
                    occurrence.is_void = FALSE
                    ${accessDataQuery}
            `

            let occurrenceIds = await sequelize.query(occurrenceIdsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            hasAccessQueryString = 'array['+ occurrenceIds[0].occurrenceids.toString() +'] @> ARRAY_AGG(occurrence.id) AS "hasAccess",'
            hasAccessCondString = ' WHERE "hasAccess" = TRUE'

        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let plantingJobsQuery = null
        // Check if the client specified values for the field/s
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                plantingJobsQuery = knex.select(selectString)
            } else {
                plantingJobsQuery = knex.column(parameters['fields'].split('|'))
            }

            plantingJobsQuery += `
                FROM
                    experiment.planting_job planting_job
                JOIN
                    tenant.program program
                ON
                    program.id = planting_job.program_id
                    AND program.is_void = FALSE
                LEFT JOIN
                    place.facility facility 
                ON 
                    facility.id = planting_job.facility_id
                    AND facility.is_void = FALSE
                JOIN
                    experiment.planting_job_occurrence planting_job_occurrence
                ON 
                    planting_job_occurrence.planting_job_id = planting_job.id
                    AND planting_job_occurrence.is_void = FALSE
                JOIN
                    experiment.occurrence occurrence
                ON 
                    occurrence.id = planting_job_occurrence.occurrence_id
                    AND occurrence.is_void = FALSE
                JOIN
                    experiment.experiment experiment
                ON 
                    experiment.id = occurrence.experiment_id
                    AND experiment.is_void = FALSE
                JOIN
                    tenant.person creator 
                ON 
                    creator.id = planting_job.creator_id
                LEFT JOIN
                    tenant.person modifier
                ON 
                    modifier.id = planting_job.modifier_id
                WHERE
                    planting_job.is_void = FALSE
                    AND occurrence.is_void = FALSE
                GROUP BY
                    planting_job.id,facility.id,creator.id, modifier.id, program.id
            `
        } else {
            plantingJobsQuery = `
                WITH t AS (
                    SELECT
                        ${addedDistinctString}
                        planting_job.id AS "plantingJobDbId",
                        planting_job.planting_job_code AS "plantingJobCode",
                        planting_job.planting_job_status AS "plantingJobStatus",
                        planting_job.planting_job_type AS "plantingJobType",
                        planting_job.planting_job_due_date AS "plantingJobDueDate",
                        planting_job.planting_job_instructions AS "plantingJobInstructions",
                        program.id AS "programDbId",
                        program.program_code AS "programCode",
                        program.program_name AS "programName",
                        facility.id AS "facilityDbId",
                        facility.facility_code AS "facilityCode",
                        facility.facility_name AS "facilityName",
                        COUNT(DISTINCT occurrence.id) AS "occurrenceCount",
                        JSON_AGG(occurrence.id) AS "occurrenceDbIds",
                        ${hasAccessQueryString} -- check if all occurrences in planting job exists in the list of occurrences that user has access to
                        COUNT(DISTINCT experiment.id) AS "experimentCount",
                        JSON_AGG(DISTINCT experiment.experiment_name) AS "experiments",
                        (
                            SELECT
                                SUM("envelopeCount")
                            FROM (
                                SELECT
                                    COUNT(p.id),
                                    pj.occurrence_id,
                                    pj.envelopes_per_plot,
                                    COUNT(p.id) * pj.envelopes_per_plot AS "envelopeCount"
                                FROM    
                                    experiment.plot p,
                                    experiment.planting_job_occurrence pj
                                WHERE
                                    p.is_void = FALSE AND
                                    pj.is_void = FALSE AND
                                    p.occurrence_id = pj.occurrence_id AND
                                    pj.planting_job_id = planting_job.id 
                                GROUP BY
                                    pj.occurrence_id,
                                    pj.envelopes_per_plot
                            ) SUM 
                        ) AS "envelopeCount",
                        planting_job.is_submitted AS "isSubmitted",
                        creator.id AS "creatorDbId",
                        creator.person_name AS creator,
                        planting_job.creation_timestamp AS "creationTimestamp",
                        planting_job.modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.person_name AS modifier
                    FROM
                        experiment.planting_job planting_job
                    JOIN
                        tenant.program program
                    ON
                        program.id = planting_job.program_id
                        AND program.is_void = FALSE
                    LEFT JOIN
                        place.facility facility 
                    ON 
                        facility.id = planting_job.facility_id
                        AND facility.is_void = FALSE
                    JOIN
                        experiment.planting_job_occurrence planting_job_occurrence
                    ON 
                        planting_job_occurrence.planting_job_id = planting_job.id
                        AND planting_job_occurrence.is_void = FALSE
                    JOIN
                        experiment.occurrence occurrence
                    ON 
                        occurrence.id = planting_job_occurrence.occurrence_id
                        AND occurrence.is_void = FALSE
                    JOIN
                        experiment.experiment experiment
                    ON 
                        experiment.id = occurrence.experiment_id
                        AND experiment.is_void = FALSE
                    JOIN
                        tenant.person creator 
                    ON 
                        creator.id = planting_job.creator_id
                    LEFT JOIN
                        tenant.person modifier
                    ON 
                        modifier.id = planting_job.modifier_id
                    WHERE
                        planting_job.is_void = FALSE
                        AND occurrence.is_void = FALSE
                    GROUP BY
                        planting_job.id, program.id, facility.id, creator.id, modifier.id
            )
            SELECT
                *
            FROM
                t
            ${hasAccessCondString} -- retrieve planting job if user has access to all occurrences under the planting job
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        plantingJobsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plantingJobsQuery,
            conditionString,
            orderString
        )

        // Retrieve the plot data records
        let plantingJobs = await sequelize.query(plantingJobsFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            await logger.logFailingQuery('planting-jobs-search', 'SELECT', err)

            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await plantingJobs == undefined || await plantingJobs.length < 1) {
            res.send(200, {
                rows: plantingJobs,
                count: 0
            })
            return
        }

        // Get the final count of the plot data records
        let plantingJobsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            plantingJobsQuery,
            conditionString,
            orderString
        )

        let plantingJobsCount = await sequelize.query(plantingJobsCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            await logger.logFailingQuery('planting-jobs-search', 'SELECT', err)

            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        count = plantingJobsCount[0].count

        res.send(200, {
            rows: plantingJobs,
            count: count
        })
        return
    }

}