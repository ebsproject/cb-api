/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let addedConditionString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let voidedEntriesString = req.query.includeVoidedEntries === 'true' ? '' : 'AND entry.is_void = FALSE'

        // TODO: Directly check for occurrence WRITE permission for faster checking
        if(req.query.distinctOccurrences == 'true') {
            if(req.body == undefined){
                let errMsg = `Missing plotDbId (should be comma-separated)`
                res.send(new errors.BadRequestError(errMsg))
                return false
            }

            let distinctOccurrencesSqlQuery = `
                SELECT DISTINCT occurrence_id AS "occurrenceDbId"
                FROM experiment.plot
                WHERE is_void = FALSE
                    AND id IN (${req.body.plotDbId})
            `

            let distinctOccurrencesList = await sequelize
                .query(
                    distinctOccurrencesSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    }
                )
                .catch(async err => {
                    await logger.logFailingQuery(endpoint, 'SELECT', err)

                    let errMsg = await errorBuilder
                        .getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                })

            res.send(200, {
                rows: distinctOccurrencesList,
                count: distinctOccurrencesList.count
            })
            return
        }

        let excludedParametersArray = {}
        let tableAliases = {}
        let columnAliases = {}
        let responseColString = `
                plot.id AS "plotDbId",
                plot.location_id AS "locationDbId",
                plot.occurrence_id AS "occurrenceDbId",
                plot.entry_id AS "entryDbId",
                entry.entry_number AS "entryNumber",
                entry.entry_type AS "entryType",
                plot.plot_code AS "plotCode",
                plot.plot_qc_code AS "plotQcCode",
                plot.plot_status AS "plotStatus",
                plot.plot_number AS "plotNumber",
                plot.plot_type AS "plotType",
                plot.rep AS "rep",
                plot.design_x AS "designX",
                plot.design_y AS "designY",
                plot.field_x AS "fieldX",
                plot.field_y AS "fieldY",
                plot.pa_x AS "paX",
                plot.pa_y AS "paY",
                plot.block_number AS "blockNumber",
                plot.harvest_status AS "harvestStatus",
                occurrence.experiment_id AS "experimentDbId",
                entry.entry_name AS "entryName",
                entry.germplasm_id AS "germplasmDbId",
                germplasm.designation AS "designation",
                germplasm.parentage AS "parentage",
                germplasm.germplasm_state AS "state",
                germplasm.germplasm_code AS "germplasmCode",
                crop.id AS "cropDbId",
                crop.crop_code AS "cropCode",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier"
            `

        if (req.body !== undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields

                // extract first attribute for sort
                let parseFields = parameters['fields'].split('|')

                let sortAttrib = ''
                // retrieve 1st attribute alias as sort
                sortAttrib = parseFields[0].split(new RegExp('AS', 'i'))
                
                // if has no alias, retrieve column name as sort
                sortAttrib = sortAttrib[1] ?? sortAttrib[0].split('.')[1]

                // if has no alias and column name for sort
                sortAttrib = sortAttrib ?? 'invalid'

                sort = sort ?? `${sortAttrib.trim()}`
            }

            excludedParametersArray = ['fields', 'distinctOn']

            if (req.body.occurrence_id != null) {
                excludedParametersArray.push('occurrence_id')

                let filter = req.body.occurrence_id
                let occurrenceIdsArray = []
                let condition=''

                for (let value of filter.split('|')) {

                    if (value.includes('equals')) {
                        value = value.replace(/equals/gi, '')

                        value  = `$$`+value.trim()+`$$`
                        occurrenceIdsArray.push(value)
                    } else {
                        let tempCondition

                        if (condition != ''){ condition += ` OR `}
                        tempCondition =  `(
                                occurrence_id ilike $$`+value+`$$
                            )`
                        condition += tempCondition
                    }
                }

                if (occurrenceIdsArray.length > 0) {
                    if (condition != ''){ condition += ` OR `}
                    condition += `(
                        occurrence_id IN (${occurrenceIdsArray.toString()})
                    )`
                }

                addedConditionString = `AND (${condition})`
            }

            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ?  conditionStringArr.mainQuery : ''
            tableAliases = conditionStringArr.tableAliases
            columnAliases = conditionStringArr.columnAliases

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn

                sort = `"${parameters['distinctOn']}" plotDbId`

                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 400057)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        else{
            orderString = `ORDER BY plot.id`
        }

        let plotsQuery = null
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(
                        parameters['fields'],
                    )

                let selectString
                    = knex.raw(`${addedDistinctString} ${fieldsString}`)
                plotsQuery = knex.select(selectString)
            } else {
                plotsQuery = knex.column(parameters['fields'].split('|'))
            }

            plotsQuery += `
            FROM
                experiment.plot plot
                JOIN experiment.occurrence occurrence 
                    ON occurrence.id = plot.occurrence_id AND occurrence.is_void = FALSE
                JOIN experiment.entry entry 
                    ON entry.id = plot.entry_id ${voidedEntriesString}
                JOIN germplasm.germplasm germplasm 
                    ON germplasm.id = entry.germplasm_id AND germplasm.is_void = FALSE
                JOIN tenant.crop crop 
                    ON crop.id = germplasm.crop_id AND crop.is_void = FALSE
                JOIN tenant.person creator 
                    ON plot.creator_id = creator.id
                LEFT JOIN tenant.person modifier 
                    ON plot.modifier_id = modifier.id
            WHERE
                plot.is_void = FALSE
                ${addedConditionString}
            ${orderString}
            `
        } else {
            plotsQuery = `
                SELECT
                    ${addedDistinctString}
                    ${responseColString}
                FROM
                    experiment.plot plot
                    JOIN experiment.occurrence occurrence 
                        ON occurrence.id = plot.occurrence_id AND occurrence.is_void = FALSE
                    JOIN experiment.entry entry 
                        ON entry.id = plot.entry_id ${voidedEntriesString}
                    JOIN germplasm.germplasm germplasm 
                        ON germplasm.id = entry.germplasm_id AND germplasm.is_void = FALSE
                    JOIN tenant.crop crop 
                        ON crop.id = germplasm.crop_id AND crop.is_void = FALSE
                    JOIN tenant.person creator 
                        ON plot.creator_id = creator.id
                    LEFT JOIN tenant.person modifier 
                        ON plot.modifier_id = modifier.id
                WHERE
                    plot.is_void = FALSE
                ${orderString}
                `
        }

        // Generate the final SQL query
        let plotFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plotsQuery,
            conditionString
        )

        // Retrieve experiments from the database
        let plots = await sequelize
            .query(plotFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                limit: limit,
                offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        if (plots == undefined || plots.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }
        
        let tableJoins = {
            'occurrence' : ' JOIN experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id AND occurrence.is_void = FALSE ',
            'entry' :  ` JOIN experiment.entry entry ON entry.id = plot.entry_id ${voidedEntriesString} `,
            'germplasm': ' JOIN germplasm.germplasm germplasm ON germplasm.id = entry.germplasm_id AND germplasm.is_void = FALSE ',
            'crop': ' JOIN tenant.crop crop ON crop.id = germplasm.crop_id AND crop.is_void = FALSE ',
            'creator': ' JOIN tenant.person creator ON plot.creator_id = creator.id',
            'modifier':' LEFT JOIN tenant.person modifier ON plot.modifier_id = modifier.id'
        }

        // retrieval of totalCount values will be processed separately
        let plotCountFinalSqlQuery = await processQueryHelper
            .getTotalCountQuery(
                req.body,
                excludedParametersArray,
                conditionString, //filter string
                addedDistinctString, //distinct string used in the main query
                tableJoins, //join statements used in the resource
                'experiment.plot plot',
                tableAliases, //table aliases involved in the join statements
                'plot.id', //main column
                columnAliases, //columns used in the filter condition
                orderString,
                responseColString
            )
        
        let plotCount = await sequelize
            .query(
                plotCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = plotCount[0].count

        res.send(200, {
        rows: plots,
        count: count
        })
        return
    }
}
