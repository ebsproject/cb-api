/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token.js')
let occurrenceHelper = require('../../../helpers/occurrence/index.js')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let userValidator = require('../../../helpers/person/validator.js')
const logger = require('../../../helpers/logger')

let excludedParametersArray = []
let tableAliases = {}
let columnAliases = {}
let responseColString = ``
let responseColStringFull = `
    occurrence.id AS "occurrenceDbId",
    occurrence.occurrence_code AS "occurrenceCode",
    occurrence.occurrence_name AS "occurrenceName",
    occurrence.occurrence_number AS "occurrenceNumber",
    occurrence.occurrence_design_type AS "occurrenceDesignType",
    location.id AS "locationDbId",
    location.location_name AS "location",
    location.location_code AS "locationCode",
    location_steward.person_name AS "locationSteward",
    program.id AS "programDbId",
    program.program_code AS "programCode",
    program.program_name AS "program",
    project.id AS "projectDbId",
    project.project_code AS "projectCode",
    project.project_name AS "project",
    site.id AS "siteDbId",
    site.geospatial_object_code AS "siteCode",
    site.geospatial_object_name AS "site",
    field.id AS "fieldDbId",
    field.geospatial_object_code AS "fieldCode",
    field.geospatial_object_name AS "field",
    occurrence.geospatial_object_id AS "geospatialObjectId",				
    goi.geospatial_object_code AS "geospatialObjectCode",
    goi.geospatial_object_name AS "geospatialObject",
    goi.geospatial_object_type AS "geospatialObjectType",
    occurrence.experiment_id AS "experimentDbId",
    experiment.experiment_code AS "experimentCode",
    experiment.experiment_name AS "experiment",
    experiment.experiment_type AS "experimentType",
    experiment.experiment_status AS "experimentStatus",
    stage.id AS "experimentStageDbId",
    stage.stage_code AS "experimentStageCode",
    stage.stage_name AS "experimentStage",
    experiment.experiment_year AS "experimentYear",
    season.id AS "experimentSeasonDbId",
    season.season_code AS "experimentSeasonCode",
    season.season_name AS "experimentSeason",
    experiment.experiment_design_type AS "experimentDesignType",
    experiment.steward_id AS "experimentStewardDbId",
    steward.person_name AS "experimentSteward",
    experiment.data_process_id AS "dataProcessDbId",
    item.abbrev AS "dataProcessAbbrev",
    occurrence.rep_count AS "repCount",
    occurrence.entry_count AS "entryCount",
    occurrence.plot_count AS "plotCount",
    occurrence.occurrence_status AS "occurrenceStatus",
    occurrence.description,
    occurrence.remarks,
    occurrence.creation_timestamp AS "creationTimestamp",
    creator.id AS "creatorDbId",
    creator.person_name AS "creator",
    occurrence.modification_timestamp AS "modificationTimestamp",
    modifier.id AS "modifierDbId",
    modifier.person_name AS "modifier",
    occurrence.occurrence_document AS "occurrenceDocument",
    occurrence.access_data AS "accessData"
`
let responseColStringBasic = `
    occurrence.id AS "occurrenceDbId",
    occurrence.occurrence_name AS "occurrenceName",
    occurrence.occurrence_design_type AS "occurrenceDesignType",
    location.id AS "locationDbId",
    location.location_name AS "location",
    location.location_code AS "locationCode",
    program.id AS "programDbId",
    site.id AS "siteDbId",
    site.geospatial_object_code AS "siteCode",
    field.id AS "fieldDbId",
    field.geospatial_object_code AS "fieldCode",
    field.geospatial_object_name AS "field",
    occurrence.experiment_id AS "experimentDbId",
    experiment.experiment_code AS "experimentCode",
    experiment.experiment_name AS "experiment",
    experiment.experiment_type AS "experimentType",
    experiment.experiment_status AS "experimentStatus",
    stage.id AS "experimentStageDbId",
    stage.stage_code AS "experimentStageCode",
    experiment.experiment_year AS "experimentYear",
    season.id AS "experimentSeasonDbId",
    season.season_code AS "experimentSeasonCode",
    experiment.experiment_design_type AS "experimentDesignType",
    occurrence.entry_count AS "entryCount",
    occurrence.plot_count AS "plotCount",
    occurrence.occurrence_status AS "occurrenceStatus",
    occurrence.access_data AS "accessData"
`
let tableJoins = {}
let tableJoinsBasic = {
    'experiment': 'JOIN experiment.experiment experiment ON experiment.id = occurrence.experiment_id',
    'creator': 'JOIN tenant.person creator ON creator.id = occurrence.creator_id',
    'log': 'LEFT JOIN experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id',
    'location': 'LEFT JOIN experiment.location location ON location.id = log.location_id',
    'site': 'JOIN place.geospatial_object site ON site.id = occurrence.site_id',
    'field': 'LEFT JOIN place.geospatial_object field ON field.id = occurrence.field_id',
    'stage': 'JOIN tenant.stage stage on stage.id = experiment.stage_id AND stage.is_void = FALSE',
    'season': 'JOIN tenant.season season on season.id = experiment.season_id AND season.is_void = FALSE',
    'program': 'JOIN tenant.program program ON program.id = experiment.program_id AND program.is_void = FALSE',
    'steward': 'JOIN tenant.person steward ON steward.id = experiment.steward_id',
}
let tableJoinsFull = {
    'log': 'LEFT JOIN experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id',
    'location': 'LEFT JOIN experiment.location location ON location.id = log.location_id',
    'location_steward': 'JOIN tenant.person location_steward ON location_steward.id = location.steward_id',
    'goi': 'LEFT JOIN place.geospatial_object goi ON goi.id = occurrence.geospatial_object_id',
    'site': 'JOIN place.geospatial_object site ON site.id = occurrence.site_id',
    'field': 'LEFT JOIN place.geospatial_object field ON field.id = occurrence.field_id',
    'creator': 'JOIN tenant.person creator ON creator.id = occurrence.creator_id',
    'modifier': 'LEFT JOIN tenant.person modifier ON modifier.id = occurrence.modifier_id',
    'experiment': 'JOIN experiment.experiment experiment ON experiment.id = occurrence.experiment_id',
    'project': 'LEFT JOIN tenant.project project on project.id = experiment.project_id',
    'stage': 'JOIN tenant.stage stage on stage.id = experiment.stage_id AND stage.is_void = FALSE',
    'season': 'JOIN tenant.season season on season.id = experiment.season_id AND season.is_void = FALSE',
    'program': 'JOIN tenant.program program ON program.id = experiment.program_id AND program.is_void = FALSE',
    'steward': 'JOIN tenant.person steward ON steward.id = experiment.steward_id',
    'item': 'LEFT JOIN master.item item ON item.id = experiment.data_process_id'
}
const endpoint = 'occurrences-search'
const operation = 'SELECT'

module.exports = {
    // Advanced search functionality for occurrences
    post: async function (req, res, next){
        // set default
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ``
        let permission = ``
        let programCond = ``
        let conditionString = ''
        let distinctString = ''
        let fields = null
        let distinctOn = ''
        let orderQuery = `ORDER BY "occurrence".id`
        let ownedConditionString = ''
        let accessDataQuery = ''
        let programIdsString = 0

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req);

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))

            return
        }

        // check if user is admin or not
        let isAdmin = await userValidator.isAdmin(userId)

        // if not admin, retrieve only occurrences user have access to
        if(!isAdmin){

            // if collaborator program code is set, filter occurrences shared to the collaborator program
            if(params.collaboratorProgramCode != null){
                programCond = ` AND program.program_code = '${params.collaboratorProgramCode}'`
            }

            // get all programs that user belongs to
            let getProgramsQuery =  `
            SELECT 
                program.id 
            FROM 
                tenant.program program,
                tenant.program_team pt,
                tenant.team_member tm
            WHERE
                tm.person_id = ${userId}
                AND pt.team_id = tm.team_id
                AND program.id = pt.program_id
                AND tm.is_void = FALSE
                AND pt.is_void = FALSE
                AND program.is_void = FALSE
                ${programCond}
            `
            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // if program does not exist
            if(programIds.length == 0){
                accessDataQuery = `occurrence.access_data @> $$\{"program": {"0":{}}}$$`
            }

            // check if there is a permission parameter
            if (params.permission != undefined  && params.permission !== undefined) {
                permission = params.permission.trim().toLowerCase();

                // validate permission
                if (!["read","write"].includes(permission)) {
                    let errMsg = '404: The permission you have provided is not valid. Please use "read" or "write"'
                    res.send(new errors.NotFoundError(errMsg))

                    return
                }

                permission = `"permission": "${permission}"`
            }

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                programIdsString += ` , ${programObj.id}`

                accessDataQuery += `occurrence.access_data @> $$\{"program": {"${programObj.id}":{${permission}}}}$$`

            }

            // include occurrences shared to the user
            let userQuery = `occurrence.access_data @> $$\{"person": {"${userId}":{${permission}}}}$$`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            }

            if(accessDataQuery != ``){
                accessDataQuery = ` AND ( (${accessDataQuery}) OR experiment.program_id in (${programIdsString}) )`
            }
        }

        let isBasic = false
        tableJoins = tableJoinsFull
        responseColString = responseColStringFull

        if (params.isBasic !== undefined && params.isBasic == 'true') {
            isBasic = true
            tableJoins = tableJoinsBasic
            responseColString = responseColStringBasic
        }

        // Default values
        if (req.body != undefined) {
            // Set the columns to be excluded in parameters for filtering
            excludedParametersArray = [
                'fields',
                'distinctOn'
            ]

            if (req.body.fields != null) {
                fields = req.body.fields
            }

            // Get filter condition
            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body, 
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''
            tableAliases = conditionStringArr.tableAliases
            columnAliases = conditionStringArr.columnAliases

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))

                return
            }

            if (req.body.distinctOn !== undefined) {
                distinctOn = req.body.distinctOn
                distinctString = await processQueryHelper
                    .getDistinctString(distinctOn)

                distinctOn = `"${distinctOn}"`
            }
        }

        // Start building the query
        let occurrenceQuery = null

        // if filter isOwned is set, retrieve occurrences where user is the creator or steward
        if (params.isOwned !== undefined) {
            ownedConditionString = ` AND (creator.id = ${userId} OR steward.id = ${userId}) `
        }

        // Check if client specifies field
        if (fields != null) {
            if (distinctOn) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(fields)
                let selectString = knex.raw(`${fieldsString}`)
                occurrenceQuery = knex.select(selectString)
            } else {
                occurrenceQuery = knex.column(fields.split('|'))
            }

            // retrieve only basic attributes
            if (isBasic) {
                occurrenceQuery += `
                    FROM
                        experiment.occurrence occurrence
                    JOIN
                        experiment.experiment experiment ON occurrence.experiment_id = experiment.id
                    LEFT JOIN
                        experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                    LEFT JOIN
                        experiment.location location ON location.id = log.location_id
                    LEFT JOIN
                        place.geospatial_object site ON site.id = occurrence.site_id
                    LEFT JOIN
                        place.geospatial_object field ON field.id = occurrence.field_id
                    JOIN
                        tenant.stage stage on stage.id = experiment.stage_id AND stage.is_void = FALSE
                    JOIN
                        tenant.season season on season.id = experiment.season_id AND season.is_void = FALSE
                    JOIN 
                        tenant.program program ON program.id = experiment.program_id AND program.is_void = FALSE
                    WHERE
                        occurrence.is_void = FALSE
                            ${accessDataQuery}
                            ${ownedConditionString}
                    ${orderQuery}
                `
            } else{
                if(conditionString.includes("contactPerson")){
                    occurrenceQuery += `,(
                        SELECT
                            data_value
                        FROM
                            experiment.occurrence_data occurrenceData
                        WHERE
                            occurrenceData.occurrence_id = occurrence.id
                            AND variable_id = (
                                SELECT
                                    id
                                FROM master.variable
                                WHERE abbrev = 'CONTCT_PERSON_CONT'
                            )
                            AND occurrenceData.is_void = false
                        LIMIT 1
                    ) AS "contactPerson"`
                }
                occurrenceQuery += `
                    FROM
                        experiment.occurrence "occurrence"
                    JOIN
                        experiment.experiment experiment ON "occurrence".experiment_id = experiment.id
                    LEFT JOIN
                        experiment.location_occurrence_group log ON log.occurrence_id = "occurrence".id
                    LEFT JOIN
                        experiment.location location ON location.id = log.location_id
                    LEFT JOIN
                        tenant.person location_steward ON location_steward.id = location.steward_id
                    LEFT JOIN 
                        place.geospatial_object goi ON goi.id = "occurrence".geospatial_object_id
                    LEFT JOIN
                        place.geospatial_object site ON site.id = "occurrence".site_id AND site.is_void = FALSE
                    LEFT JOIN
                        place.geospatial_object field ON field.id = "occurrence".field_id
                    JOIN 
                        tenant.person creator ON creator.id = "occurrence".creator_id
                    LEFT JOIN
                        tenant.person modifier ON modifier.id = "occurrence".modifier_id
                    LEFT JOIN
                        tenant.project project on project.id = experiment.project_id
                    JOIN
                        tenant.stage stage on stage.id = experiment.stage_id AND stage.is_void = FALSE
                    JOIN
                        tenant.season season on season.id = experiment.season_id AND season.is_void = FALSE
                    JOIN 
                        tenant.program program ON program.id = experiment.program_id AND program.is_void = FALSE
                    LEFT JOIN 
                        tenant.person steward ON steward.id = experiment.steward_id
                    LEFT JOIN
                        master.item item ON item.id = experiment.data_process_id
                    WHERE
                        "occurrence".is_void = FALSE
                        AND experiment.is_void = FALSE
                            ${accessDataQuery}
                            ${ownedConditionString}
                    ${orderQuery}
                `
            }

        } else {
            occurrenceQuery = await occurrenceHelper.getOccurrenceQuerySql(
                'AND experiment.is_void = FALSE',
                '',
                '',
                distinctString,
                ownedConditionString,
                isBasic,
                accessDataQuery,
                responseColString
            )
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400057)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Generate the final sql query 
        let occurrenceFinalSqlQuery = await processQueryHelper.getOccurrenceFinalSqlQueryWoLimit(
            occurrenceQuery,
            conditionString,
            orderString,
            distinctString
        )

        occurrenceFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            occurrenceFinalSqlQuery,
            orderString
        )

        // Retrieve occurrences from the database   
        let occurrences = await sequelize.query(occurrenceFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        }).catch(async err => {
            await logger.logFailingQuery(endpoint, operation, err)
        })

        if (occurrences === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        } else if (occurrences.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })

            return
        }

        // Get count 
        count = occurrences[0].totalCount

        res.send(200, {
            rows: occurrences,
            count: count
        })

        return
    }
}