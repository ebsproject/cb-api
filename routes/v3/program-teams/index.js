/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let userValidator = require('../../../helpers/person/validator.js')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let logger = require('../../../helpers/logger')

module.exports = {
    // Endpoint for creating a new program teams record
    // POST /v3/program-teams
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []

        let programTeamDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        //check if user is Admin
        let isAdmin = await userValidator.isAdmin(personDbId)

         if (!isAdmin) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401028)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let programTeamUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/program-teams'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let programTeamValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Set defaults
                let programDbId = null
                let teamDbId = null
                let orderNumber = null

                // Check if required columns are in the request body
                if (
                    !record.programDbId || !record.teamDbId || !record.orderNumber
                ) {
                    let errMsg = `Required parameters are missing. Ensure that programDbId, teamDbId, and orderNumber fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (record.programDbId !== undefined) {
                    programDbId = record.programDbId

                    if (!validator.isInt(programDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400051)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate program ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if program is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.program program
                        WHERE 
                            program.is_void = FALSE AND
                            program.id = ${programDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.teamDbId !== undefined) {
                    teamDbId = record.teamDbId

                    if (!validator.isInt(teamDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400051)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate team ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if team is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.team team
                        WHERE 
                            team.is_void = FALSE AND
                            team.id = ${teamDbId}
                        )
                    `
                    validateCount += 1
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Set defaults
                programDbId = record.programDbId
                teamDbId = record.teamDbId
                orderNumber = (record.orderNumber !== undefined) ? record.orderNumber : null

                // get values
                let tempArray = [
                    programDbId, teamDbId, orderNumber, personDbId
                ]

                programTeamValuesArray.push(tempArray)
            }

            // Create programTeam record
            let programTeamsQuery = format(`
                INSERT INTO
                    tenant.program_team (
                        program_id, team_id, order_number, creator_id
                    )
                VALUES
                    %L
                RETURNING id`, programTeamValuesArray
            )

            let programTeams = await sequelize.query(programTeamsQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            for (let programTeam of programTeams[0]) {
                programTeamDbId = programTeam.id

                let programTeamRecord = {
                    programTeamDbId: programTeamDbId,
                    recordCont: 1,
                    href: programTeamUrlString + '/' + programTeamDbId
                }

                resultArray.push(programTeamRecord)
            }
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, 'POST program_teams: ' + JSON.stringify(err), 'error')

            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}