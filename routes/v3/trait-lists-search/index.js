/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize, Variable } = require('../../../config/sequelize')
let errors = require('restify-errors')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Implementation of advanced search functionality for trait lists
    post: async function (req, res, next) {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let sort = req.query.sort

        let count = 0

        let orderString = ''

        let conditionString = ''

        let addedConditionString = ''

        let experimentIdString = ''

        let experimentLocationIdString = ''

        let parameters = {}
        parameters['experimentDbId'] = null
        parameters['experimentLocationDbId'] = null
        parameters['fields'] = null

        if (req.body != null) {
            // Retrieve parameters

            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.experimentDbId != null) {
                parameters['experimentDbId'] = req.body.experimentDbId
            }

            if (req.body.experimentLocationDbId != null) {
                parameters['experimentLocationDbId'] = req.body.experimentLocationDbId
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', 'experimentDbId', 'experimentLocationDbId']

            // Get filter condition
            conditionString = await processQueryHelper.getFilterString(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        if (parameters['experimentDbId'] != null) {

            let experimentIds = parameters['experimentDbId']

            // Parse the value
            for (experimentId of experimentIds.split('|')) {
                if (experimentIdString != '') {
                    experimentIdString += ','
                }
                experimentIdString += experimentId
            }
        }

        if (parameters['experimentLocationDbId'] != null) {

            let experimentLocationIds = parameters['experimentLocationDbId']

            // Parse the value
            for (experimentLocationId of experimentLocationIds.split('|')) {
                if (experimentLocationIdString != '') {
                    experimentLocationIdString += ','
                }
                experimentLocationIdString += experimentLocationId
            }
        }

        if (experimentIdString != '' || experimentLocationIdString != '') {

            let addedColumnSource = ''
            let addedConditionSource = ''

            if (experimentIdString == '') {
                addedColumnSource += `, operational.study "experimentLocation" `
                addedConditionSource += ` 
                    AND "plotData".study_id = "experimentLocation".id
                    AND "experimentLocation".is_void = FALSE
                    AND "experimentLocation".id IN (
                        ` + experimentLocationIdString + `
                    )`
            } else {
                addedColumnSource += `, operational.study "experimentLocation", operational.experiment experiment `
                addedConditionSource += ` 
                    AND "plotData".study_id = "experimentLocation".id
                    AND "experimentLocation".is_void = FALSE
                    AND "experimentLocation".experiment_id = experiment.id
                    AND experiment.is_void = FALSE `

                if (experimentLocationIdString != '') {
                    addedConditionSource += `AND "experimentLocation".id IN (
                        ` + experimentLocationIdString + `
                    )`
                }

                if (experimentIdString != '') {
                    addedConditionSource += ` AND experiment.id IN (
                        ` + experimentIdString + `
                    )`
                }
            }

            addedConditionString += `
                AND variable.id IN (
                    SELECT 
                        "plotData".variable_id
                    FROM 
                        operational.plot_data "plotData"
                    ` + addedColumnSource + `
                    WHERE "plotData".is_void = FALSE
                    ` + addedConditionSource + `
                )
            `
        } else {
            addedConditionString += ` AND variable.type = 'observation' `
        }

        // Build query
        let traitListsQuery = ``

        // Check if client specifies field
        if (parameters['fields'] != null) {
            traitListsQuery = knex.column(parameters['fields'].split('|'))

        } else {
            traitListsQuery = `
                SELECT
                    variable.id AS "variableDbId",
                    variable.abbrev,
                    variable.label,
                    variable.name,
                    variable.data_type AS "dataType",
                    variable.not_null AS "notNull",
                    variable.type,
                    variable.status,
                    variable.display_name AS "displayName",
                    variable.variable_set AS "variableSet",
                    variable.synonym,
                    variable.remarks,
                    variable.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    variable.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier,
                    variable.description,
                    variable.default_value AS "defaultValue",
                    variable.usage,
                    variable.data_level AS "dataLevel",
                    variable.is_computed AS "isComputed"
                `
        }

        traitListsQuery += `
            FROM 
                master.variable variable
            LEFT JOIN 
                master.user creator ON variable.creator_id = creator.id
            LEFT JOIN 
                master.user modifier ON variable.modifier_id = modifier.id
            WHERE 
                variable.is_void = FALSE
            ` + addedConditionString

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        traitListsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            traitListsQuery,
            conditionString,
            orderString
        )

        // Retrieve traits from the database   
        let traitLists = await sequelize.query(traitListsFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (traitLists === undefined || traitLists.length < 1) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            // Get count 
            traitListsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(traitListsQuery, conditionString, orderString)

            traitListsCount = await sequelize.query(traitListsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = traitListsCount[0].count
        }

        res.send(200, {
            rows: traitLists,
            count: count
        })
        return
    }
}