/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the list of of data of a transaction
    // GET /v3/transactions/:id/data
    get: async function (req, res, next) {

        // Retrieve the transaction ID

        let transactionDbId = req.params.id

        if (!validator.isInt(transactionDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400075)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {

            let transactionQuery = `
                SELECT
                    transaction.id AS "transactionDbId",
                    transaction.status,
                    transaction.study_name AS "studyName",
                    transaction.dataset,
                    transaction.dataset_name AS "datasetName",
                    transaction.data_level AS "dataLevel",
                    transaction.action,
                    transaction.remarks,
                    transaction.record_count AS "recordCount",
                    transaction.invalid_record_count AS "invalidRecordCount",
                    transaction.start_action_timestamp AS "startActionTimestamp",
                    transaction.end_action_timestamp AS "endActionTimestamp",
                    transaction.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    transaction.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier
                FROM
                    operational_data_terminal.transaction2 transaction
                    LEFT JOIN 
                        master.user creator on transaction.creator_id = creator.id
                    LEFT JOIN 
                        master.user modifier on transaction.modifier_id = modifier.id
                WHERE 	
                    transaction.id = :transactionDbId
            `

            // Retrieve transaction from the database   
            let transactions = await sequelize.query(transactionQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    transactionDbId: transactionDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await transactions == undefined || await transactions.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404026)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (transaction of transactions) {
                let terminalDataQuery = `
                    SELECT
                        terminal.id AS "terminalDbId",
                        terminal.identifier,
                        terminal.identifier_key AS "identifierKey",
                        terminal.key,
                        terminal.variable_id AS "variableDbId",
                        terminal.value AS "value",
                        terminal.data_unit AS "dataUnit",
                        terminal.is_data_type_valid AS "isDataTypeValid",
                        terminal.is_data_value_valid AS "isDataValueValid",
                        terminal.remarks,
                        terminal.is_committed_operational AS "isCommittedOperational",
                        terminal.is_generated AS "isGenerated",
                        terminal.is_suppress AS "isSuppress",
                        terminal.suppress_remarks AS "suppressRemarks",
                        terminal.is_operational AS "isOperational",
                        terminal.creation_timestamp AS "creationTimestamp",
                        collector.id AS "creatorDbId",
                        collector.display_name AS creator,
                        terminal.collection_timestamp AS "collectionTimestamp",
                        creator.id AS "creatorDbId",
                        creator.display_name AS creator,
                        terminal.modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.display_name AS modifier
                    FROM
                        operational_data_terminal.terminal terminal
                        LEFT JOIN 
                            master.user creator on terminal.creator_id = creator.id
                        LEFT JOIN 
                            master.user modifier on terminal.modifier_id = modifier.id
                        LEFT JOIN 
                            master.user collector on terminal.collector_id = collector.id
                    WHERE 	
                        terminal.is_void = FALSE
                        AND terminal.transaction_id = :transactionDbId
                `

                // Retrieve terminal data from the database   
                let data = await sequelize.query(terminalDataQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        transactionDbId: transaction.transactionDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                transaction["data"] = data
            }

            res.send(200, {
                rows: transactions
            })
            return
        }
    }
}