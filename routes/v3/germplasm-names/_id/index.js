/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize, germplasm } = require('../../../../config/sequelize')
let errors = require('restify-errors')

let validator = require('validator')
let forwarded = require('forwarded-for')

let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator')
let germplasmHelper = require('../../../../helpers/germplasm/index')

module.exports = {
 
  // Implementation of delete call for /v3/germplasm-names/{id}
    delete: async function (req, res, next) {

        let userDbId = await tokenHelper.getUserId(req)

            if (userDbId == null) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401002)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

      // Defaults
        let germplasmNameDbId = req.params.id
        let isSecure = forwarded(req, req.headers).secure

      // Set the URL for response
        let germplasmNameUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/germplasm-names"

        if (!validator.isInt(germplasmNameDbId)) {
            let errMsg = "Invalid format, germplasm name ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }
      // Start transaction
        let transaction

        try {
          // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
          // Build query
            let germplasmNameQuery = `
                SELECT
                    count(1)
                FROM 
                    germplasm.germplasm_name germplasm_name
                WHERE
                    germplasm_name.is_void = FALSE AND
                    germplasm_name.id = ${germplasmNameDbId}
            `

          // Retrieve germpalsm name from the database   
            let hasGermplasmName = await sequelize.query(germplasmNameQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            // Check if germplasm name is non-existent in database
            if (hasGermplasmName == undefined || parseInt(hasGermplasmName[0].count) === 0) {
                let errMsg = "The germplasm name you have requested does not exist."
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Build query for voiding
            let deleteGermplasmNameQuery = `
                UPDATE
                    germplasm.germplasm_name germplasm_name
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    germplasm_name.id = ${germplasmNameDbId}
            `

              await sequelize.query(deleteGermplasmNameQuery, {
                  type: sequelize.QueryTypes.UPDATE
              })

            // Commit the transaction
              await transaction.commit()

              res.send(200, {
                  rows: {
                      germplasmNameDbId: germplasmNameDbId,
                      recordCount: 1,
                      href: germplasmNameUrlString + '/' + germplasmNameDbId
                  }
              })
              return
        } catch (err) {
          // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },
    
    /**
    * Update germplasm name record
    * 
    * @param {*} req Request request
    * @param {*} res response
    * @param {*} next 
    */
    put: async function (req, res, next) {
        let germplasmDbId = null
        let nameValue = ''
        let germplasmNameType = ''
        let germplasmNameStatus = ''
  
        // Retrieve user ID
        let userDbId = await tokenHelper.getUserId(req)
  
        let errMsg = ''
        if(userDbId == null){
            errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
  
        // Check if user is ADMIN
        let isAdmin = await userValidator.isAdmin(userDbId)
        if(!isAdmin){
            errMsg = await errorBuilder.getError(req.headers.host, 401024)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
  
        if(!req.params){
            return
        }

        if(!req.body){
            errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
  
        // Check if germplasm_name record is defined
        if(req.params.id == undefined || req.params.id == null){
            errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        // Check if germplasm_name record ID is integer
        germplasmNameDbId = req.params.id ?? null
        if(germplasmNameDbId != null && !validator.isInt(germplasmNameDbId)){
            errMsg = 'Invalid format. Germplasm name ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }
  
        // Build germplasm name record retrieval query
        let germplasmNameRecordQuery = `
            SELECT
                germplasm_name.id AS "germplasmDbId",
                germplasm_name.name_value AS "nameValue",
                germplasm_name.germplasm_name_type AS "germplasmNameType",
                germplasm_name.germplasm_name_status AS "germplasmNameStatus"
            FROM
                germplasm.germplasm_name
            WHERE
                germplasm_name.is_void = FALSE
                AND germplasm_name.id = ${germplasmNameDbId}
        `

        // Retrieve the germplasm name record
        let germplasmName = await sequelize.query(germplasmNameRecordQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (await germplasmName == undefined || await germplasmName.length == 0) {
            let errMsg = 'Invalid request. Ensure that the germplasm name record exists. '
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let germplasmNameRecord = germplasmName[0] ?? []

        let gnGermplasmDbId = germplasmNameRecord.germplasmDbId ?? null
        let gnNameValue = germplasmNameRecord.nameValue ?? ''
        let gnStatus = germplasmNameRecord.germplasmNameStatus ?? ''
        let gnType = germplasmNameRecord.germplasmNameType ?? ''
        
        // Set URL for response
        let isSecure = forwarded(req, req.headers).secure
        let germplasmNameUrlString = (isSecure ? 'https' : 'http')
            + "://" + req.headers.host + "/v3/germplasm-names"
  
        let data = req.body
        let transaction
        let resultArray = []

        try {
            let setQuery = ''

            // Validate body parameters
            let validateQuery = ''
            let validateCount = 0

            // validate germplasmDbId
            if(data.germplasmDbId != undefined){
                let newGermplasmDbId = data.germplasmDbId

                if(newGermplasmDbId == ''){
                    errMsg = 'Invalid parameters. Parameter values should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(newGermplasmDbId != null && !validator.isInt(newGermplasmDbId)){
                    errMsg = 'Invalid format. Germplasm ID must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        germplasm.germplasm g
                    WHERE
                        g.is_void = FALSE
                        AND g.id = $$${newGermplasmDbId}$$
                )
                `
                validateCount += 1

                if(newGermplasmDbId != gnGermplasmDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_id = ${newGermplasmDbId}`
                }
            }

            // validate nameValue
            if(data.nameValue != undefined){
                let newNameValue = data.nameValue

                if(newNameValue == ''){
                    errMsg = 'Invalid parameters. Parameter values should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(newNameValue != gnNameValue){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        name_value = $$${newNameValue}$$`
                }
            }

            if(data.germplasmNameStatus != undefined){
                let newNermplasmNameStatus = data.germplasmNameStatus

                if(newNermplasmNameStatus == ''){
                    errMsg = 'Invalid parameters. Parameter values should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(newNermplasmNameStatus != gnStatus){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_name_status = $$${newNermplasmNameStatus}$$`
                }
            }

            if(data.germplasmNameType != undefined){
                let newGermplasmNameType = data.germplasmNameType

                if(newGermplasmNameType == ''){
                    errMsg = 'Invalid parameters. Parameter values should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let germplasmNameTypeScaleValues = await germplasmHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE')

                if( newGermplasmNameType != '' && germplasmNameTypeScaleValues 
                    && germplasmNameTypeScaleValues.length > 0){
                    
                    if(!germplasmNameTypeScaleValues.includes(newGermplasmNameType)){
                        errMsg = 'Invalid parameters. germplasmNameType values should be a valid scale value.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if(newGermplasmNameType != gnType){
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            germplasm_name_type = $$${newGermplasmNameType}$$`
                    }
                }
            }

            if (validateCount > 0){
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
                
                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            let updateGermplasmNameQuery = `
                UPDATE
                    germplasm.germplasm_name
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = $$${userDbId}$$
                WHERE
                    id = $$${germplasmNameDbId}$$
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            // Update germplasm_name record
            await sequelize.query(updateGermplasmNameQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let array = {
                germplasmNameDbId: germplasmNameDbId,
                recordCount: 1,
                href: germplasmNameUrlString + '/' + germplasmNameDbId
            }

            resultArray.push(array)

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch (e) {
            // Rollback transaction (if still open)
            if (transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return   
        }
      }
  }
