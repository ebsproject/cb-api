/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../config/sequelize")
let tokenHelper = require("../../../helpers/auth/token")
let errors = require("restify-errors")
let errorBuilder = require("../../../helpers/error-builder")
let forwarded = require("forwarded-for")
let format = require("pg-format")
let validator = require("validator")

module.exports = {
    // Endpoint for creating a new germplasm name
    // POST /v3/germplasm-names
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []
        let recordCount = 0

        let germplasmNameDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let germplasmNameUrlString =
            (isSecure ? "https" : "http") +
            "://" +
            req.headers.host +
            "/v3/germplasm-names"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(
                    req.headers.host,
                    400005
                )
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let germplasmNameValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                let nameValue = null
                let germplasmNameType = null
                let germplasmNameStatus = null
                let germplasmNormalizedName = null

                let germplasmDbId = null

                // Check if required columns are in the request body
                if (
                    record.nameValue==undefined ||
                    record.germplasmNameType==undefined ||
                    record.germplasmNameStatus==undefined ||
                    record.germplasmDbId==undefined
                ) {
                    let errMsg = ` Required parameters are missing. Ensure that nameValue, 
                        germplasmNameType, germplasmNameStatus, germplasmNormalizedName and germplasmDbId
                        fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */

                nameValue = record.nameValue
                germplasmNameType = record.germplasmNameType
                germplasmNameStatus = record.germplasmNameStatus
                germplasmNormalizedName = record.germplasmNormalizedName
                germplasmDbId = record.germplasmDbId

                // database validation for seed ID
                if (!validator.isInt(germplasmDbId)) {
                    let errMsg = `Invalid format, germplsm ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += validateQuery != "" ? " + " : ""
                validateQuery += `
                    (
                      --- Check if germlasnm is existing return 1
                      SELECT 
                        count(1)
                      FROM 
                        germplasm.germplasm germplasm
                      WHERE 
                        germplasm.is_void = FALSE AND
                        germplasm.id = ${germplasmDbId}
                    )`
                validateCount += 1

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                      ${validateQuery}
                    AS count`

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT,
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(
                        req.headers.host,
                        400015
                    )
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [
                    germplasmDbId,
                    nameValue,
                    germplasmNameType,
                    germplasmNameStatus,
                    germplasmNormalizedName,
                    personDbId,
                ]

                germplasmNameValuesArray.push(tempArray)
            }

            // Create germplasmName record/s
            let germplasmNameQuery = format(
                `
                INSERT INTO
                  germplasm.germplasm_name (
                    germplasm_id, name_value, germplasm_name_type,germplasm_name_status, germplasm_normalized_name,creator_id
                  )
                VALUES 
                  %L
                RETURNING id`,
                germplasmNameValuesArray
            )

            let germplasmNames = await sequelize.query(germplasmNameQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction,
            })

            for (germplasmName of germplasmNames[0]) {
                germplasmNameDbId = germplasmName.id

                // Return the germplasmName info
                let array = {
                    germplasmNameDbId: germplasmNameDbId,
                    recordCount: 1,
                    href: germplasmNameUrlString + "/" + germplasmNameDbId,
                }
                resultArray.push(array)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray,
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },
}
