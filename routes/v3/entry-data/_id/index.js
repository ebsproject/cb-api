/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator.js')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

  // PUT /v3/entry-data/:id
  put: async function (req, res, next) {
    // Retrieve entry data ID
    let entryDataDbId = req.params.id

    // Check if experiment data ID is an integer
    if (!validator.isInt(entryDataDbId)) {
      let errMsg = `Invalid format, entry data ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Checks if there is a request body
    if (!req.body) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400009)
        res.send(new errors.BadRequestError(errMsg))
        return
    }

    // Get userDbId
    let personDbId = await tokenHelper.getUserId(req, res)

    // If userDbId is undefined, terminate
    if (personDbId === undefined) {
        return
    }
    // If personDbId is null, return 401: User not found error
    else if (personDbId === null) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401002)
        res.send(new errors.BadRequestError(errMsg))
        return
    }

    let isAdmin = await userValidator.isAdmin(personDbId)

    // Set defaults
    let dataValue = null
    let dataQcCode = null
    let transactionDbId = null

    // Build query
    let entryDataQuery = `
      SELECT 
        entryData.data_value AS "dataValue",
        entryData.data_qc_code AS "dataQcCode",
        entryData.transaction_id AS "transactionDbId",
        entryData.entry_id AS "entryDbId",
        creator.id AS "creatorDbId"
      FROM
        experiment.entry_data entryData
        LEFT JOIN
          tenant.person creator ON creator.id = entryData.creator_id
      WHERE
        entryData.is_void = FALSE AND
        entryData.id = ${entryDataDbId}
    `

    // Retrieve entry data record from database
    let entryData = await sequelize.query(entryDataQuery,{
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (entryData === undefined || entryData.length < 1) {
      let errMsg = `The entry data record you have requested does not exist.`
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Get values from database, record = the current value in the database
    let recordDataValue = entryData[0].dataValue
    let recordDataQcCode = entryData[0].dataQcCode
    let recordTransactionDbId = entryData[0].transactionDbId
    let recordEntryDbId = entryData[0].entryDbId
    let recordCreatorDbId = entryData[0].creatorDbId

    if(!isAdmin && (personDbId != recordCreatorDbId)) {
      let programMemberQuery = `
        SELECT 
          person.id,
          person.person_role_id AS "role"
        FROM 
          tenant.person person
          LEFT JOIN 
            tenant.team_member teamMember ON teamMember.person_id = person.id
          LEFT JOIN 
            tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
          LEFT JOIN 
            tenant.program program ON program.id = programTeam.program_id 
          LEFT JOIN
            experiment.experiment experiment ON experiment.program_id = program.id
          LEFT JOIN 
            experiment.entry_list entryList ON entryList.experiment_id = entryList.id
          LEFT JOIN 
            experiment.entry entry ON entry.entry_list_id = entryList.id
          LEFT JOIN
            experiment.entry_data entryData ON entryData.entry_id = entry.id
        WHERE 
          entry.id = ${recordEntryDbId} AND
          person.id = ${personDbId} AND
          person.is_void = FALSE
      `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
    }

    let isSecure = forwarded(req, req.header).secure

    // Set URL for response
    let entryDataUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/entry-data/'
      + entryDataDbId

    let data = req.body
    let transaction

    try {
      let setQuery = ``

      if (data.dataValue !== undefined) {
        dataValue = data.dataValue
        dataValue = dataValue.trim()

        // Check if user input is same with the current value in the database
        if (dataValue != recordDataValue) {
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_value = $$${dataValue}$$
          `
        }
      }

      if (data.dataQcCode !== undefined) {
        dataQcCode = data.dataQcCode
        dataQcCode = dataQcCode.trim()

        // Check if user input is same with the current value in the database
        if (dataQcCode != recordDataQcCode) {

          let validEntryDataQcCodes = ['N', 'G', 'Q', 'S', 'M', 'B']

          // Validate data QC code
          if (!validEntryDataQcCodes.includes(dataQcCode.toLowerCase())) {
            let errMsg = `Invalid request, you have provided an invalid
              value for data qc code.`
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_qc_code = $$${dataQcCode}$$
          `
        }
      }

      if (data.transactionDbId !== undefined) {
        transactionDbId = data.transactionDbId

        // Check if user input is same with the current value in the database
        if (transactionDbId != recordTransactionDbId) {
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            transaction_id = $$${transactionDbId}$$
          `
        }
      }

      if(setQuery.length > 0) setQuery += `,`

      let updateEntryDataQuery = `
        UPDATE
          experiment.entry_data entryData
        SET
          ${setQuery}
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          entryData.id = ${entryDataDbId}
      `

      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      await sequelize.query(updateEntryDataQuery, {
        type: sequelize.QueryTypes.UPDATE,
        transaction: transaction
      })

      // Return transaction info
      let resultArray = {
        entryDataDbId: entryDataDbId,
        recordCount: 1,
        href: entryDataUrlString
      }

      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      res.send(new errors.InternalError(errMsg))
      return
    }    
  }, 

  // DELETE /v3/entry-data/:id
  delete: async function (req, res, next) {
    // Retrieve entry data ID
    let entryDataDbId = req.params.id

    // Retrieve user ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    let isAdmin = await userValidator.isAdmin(personDbId)
    
    if (!validator.isInt(entryDataDbId)) {
      let errMsg = `Invalid format, entry data ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let entryDataQuery = `
        SELECT 
          entryData.entry_id AS "entryDbId",
          creator.id AS "creatorDbId"
        FROM
          experiment.entry_data entryData
          LEFT JOIN 
            tenant.person creator ON creator.id = entryData.creator_id
        WHERE
          entryData.is_void = FALSE AND
          entryData.id = ${entryDataDbId}
      `

      let entryData = await sequelize.query(entryDataQuery, {
        type: sequelize.QueryTypes.SELECT
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      if (entryData == undefined || entryData.length < 1) {
        let errMsg = `The entry data you have requested does not exist.`
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      let recordEntryDbId = entryData[0].entryDbId
      let recordCreatorDbId = entryData[0].creatorDbId

      if(!isAdmin && (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
          SELECT 
            person.id,
            person.person_role_id AS "role"
          FROM 
            tenant.person person
            LEFT JOIN 
              tenant.team_member teamMember ON teamMember.person_id = person.id
            LEFT JOIN 
              tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
            LEFT JOIN 
              tenant.program program ON program.id = programTeam.program_id 
            LEFT JOIN
              experiment.experiment experiment ON experiment.program_id = program.id
            LEFT JOIN 
              experiment.entry_list entryList ON entryList.experiment_id = entryList.id
            LEFT JOIN 
              experiment.entry entry ON entry.entry_list_id = entryList.id
            LEFT JOIN
              experiment.entry_data entryData ON entryData.entry_id = entry.id
          WHERE 
            entry.id = ${recordEntryDbId} AND
            person.id = ${personDbId} AND
            person.is_void = FALSE
        `
  
        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
      }

      let deleteEntryDataQuery = format(`
        UPDATE
          experiment.entry_data entryData
        SET
          is_void = TRUE,
          notes = CONCAT('VOIDED-', $$${entryDataDbId}$$),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          entryData.id = ${entryDataDbId}
      `)

      await sequelize.query(deleteEntryDataQuery, {
        type: sequelize.QueryTypes.UPDATE
      })

      await transaction.commit()
      res.send(200, {
        rows: { entryDataDbId: entryDataDbId }
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}