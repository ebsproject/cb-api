/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

module.exports = {

  // Implementation of POST call for /v3/entry-data
  post: async (req, res, next) => {

    // Set defaults
    let resultArray = []

    let entryDataDbId = null
    let entryDbId = null
    let variableDbId = null
    let dataValue = null
    let transactionDbId = null

    // Get person ID from token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure

    // Set URL for response
    let entryDataUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/entry-data'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      records = data.records

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction

    try {
      transaction = await sequelize.transaction({ autocommit: false })

      let entryDataValuesArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        // Check if required columns are in the input
        if (
          !record.entryDbId || !record.variableDbId || !record.dataValue
        ) {
          let errMsg = `Required parameters are missing. Ensure that entryDbId,
            variableDbId, and dataValuefields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        /** Validation of required parameters and set values */

        if (record.entryDbId !== undefined) {
          entryDbId = record.entryDbId

          // Validate entry ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if entry is existing return 1
              SELECT 
                count(1)
              FROM 
                experiment.entry entry 
              WHERE 
                entry.is_void = FALSE AND
                entry.id = ${entryDbId}
            )
          `
          validateCount += 1
        }

        if (record.variableDbId !== undefined) {
          variableDbId = record.variableDbId

          // Validate entry ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if variable is existing return 1
              SELECT 
                count(1)
              FROM 
                master.variable variable
              WHERE 
                variable.is_void = FALSE AND
                variable.id = ${variableDbId}
            )
          `
          validateCount += 1
        }

        dataValue = record.dataValue

        /** Validation of non-required parameters and set values */

        transactionDbId = (record.transactionDbId !== undefined) ? 
          record.transactionDbId : null

        /** Validation of input in database */
        // count must be equal to validateCount if all conditions are met
        let checkInputQuery = `
          SELECT
            ${validateQuery}
          AS count
        `

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        dataValue = dataValue.trim()

        // Get values
        let tempArray = [
          entryDbId, variableDbId, dataValue, 'N', transactionDbId, personDbId
        ]

        entryDataValuesArray.push(tempArray)
      }

      // Create entry data record
      let entryDataQuery = format(`
        INSERT INTO
          experiment.entry_data (
            entry_id, variable_id, data_value, data_qc_code, 
            transaction_id, creator_id
          )
        VALUES
          %L
        RETURNING id`, entryDataValuesArray
      )

      let entryData = await sequelize.query(entryDataQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      for (entryDatum of entryData[0]) {
        entryDataDbId = entryDatum.id

        let entryDataRecord = {
          entryDataDbId: entryDataDbId,
          recordCount: 1,
          href: entryDataUrlString + '/' + entryDataDbId
        }

        resultArray.push(entryDataRecord)
      }

      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}