/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}

        if (req.body) {
            if(req.body.fields != undefined) {
                parameters['fields'] = req.body.fields
            }
        }

        // Set columns to be excluded in parameters for filtering
        let excludedParametersArray = ['fields']

        // Get filter condition
        conditionString = await processQueryHelper
            .getFilterString(req.body, excludedParametersArray)

        if(conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build base retrieval query
        let sampleMeasurementsQuery = null

        // Check if client specified values for fields
        if (parameters['fields']) {
            sampleMeasurementsQuery = knex.column(parameters['fields'].split('|'))
            sampleMeasurementsQuery += `
                FROM
                    experiment.sample_measurement sample_measurement
                LEFT JOIN
                    tenant.person creator ON sample_measurement.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON sample_measurement.modifier_id = modifier.id
                LEFT JOIN
                    experiment.sample sample ON sample_measurement.sample_id = sample.id
                LEFT JOIN
                    master.variable variable ON sample_measurement.variable_id = variable.id
                WHERE
                    sample_measurement.is_void = FALSE
                ORDER BY
                    sample_measurement.id
            `
        } else {
            sampleMeasurementsQuery = `
                SELECT
                    sample_measurement.id AS "sampleMeasurementDbId",
                    sample.id AS "sampleDbId",
                    variable.id AS "variableDbId",
                    sample_measurement.data_value AS "dataValue",
                    sample_measurement.data_qc_code AS "dataQcCode",
                    sample_measurement.collection_timestamp AS "collectionTimestamp",
                    sample_measurement.measurement_type AS "measurementType",
                    sample_measurement.transaction_id AS "transactionDbId",
                    sample_measurement.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    sample_measurement.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    experiment.sample_measurement sample_measurement
                LEFT JOIN
                    tenant.person creator ON sample_measurement.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON sample_measurement.modifier_id = modifier.id
                LEFT JOIN
                    experiment.sample sample ON sample_measurement.sample_id = sample.id
                LEFT JOIN
                    master.variable variable ON sample_measurement.variable_id = variable.id
                WHERE
                    sample_measurement.is_void = FALSE
                ORDER BY
                    sample_measurement.id
            `
        }

        // Parse sort parameters
        if (sort != undefined) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let sampleMeasurementsFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                sampleMeasurementsQuery,
                conditionString,
                orderString
            )

        // Retrieve the sample measurement records
        let sampleMeasurements = await sequelize
            .query(sampleMeasurementsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT, 
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch (async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await sampleMeasurements == undefined || await sampleMeasurements.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } 
        
        let sampleMeasurementsCountFinalSqlQuery =  await processQueryHelper
            .getCountFinalSqlQuery(
                sampleMeasurementsQuery,
                conditionString,
                orderString
            )

        let sampleMeasurementsCount = await sequelize
            .query(sampleMeasurementsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = sampleMeasurementsCount[0].count

        res.send(200, {
            rows: sampleMeasurements,
            count: count
        })
        return
    }
}