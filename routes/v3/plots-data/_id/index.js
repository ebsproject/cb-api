/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let responseHelper = require('../../../../helpers/responses')
let forwarded = require('forwarded-for')

module.exports = {

    // Endpoint for retrieving a specific plot data
    // GET /v3/plots-data/:id
    get: async function (req, res, next) {

        // Retrieve the plot data ID

        let plotDataDbId = req.params.id

        if (!validator.isInt(plotDataDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400040)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let plotsDataQuery = `
                SELECT
                    "plotData".id AS "plotDataDbId",
                    "plotData".study_id AS "experimentLocationDbId",
                    "plotData".entry_id AS "entryDbId",
                    "plotData".plot_id AS "plotDbId",
                    "plotData".variable_id AS "variableDbId",
                    variable.abbrev AS "variableAbbrev",
                    variable.label AS "variableLabel",
                    "plotData".value AS "variableValue",
                    "plotData".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    "plotData".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier,
                    "plotData".collection_timestamp AS "collectionTimestamp",
                    collector.id AS "collectorDbId",
                    collector.display_name AS collector,
                    "plotData".is_suppress AS "isSuppress",
                    "plotData".suppress_remarks AS "suppressRemarks"
                FROM 
                    operational.plot_data "plotData"
                LEFT JOIN 
                    master.variable variable ON variable.id = "plotData".variable_id
                LEFT JOIN 
                    master.user creator ON "plotData".creator_id = creator.id
                LEFT JOIN 
                    master.user modifier ON "plotData".modifier_id = modifier.id
                LEFT JOIN 
                    master.user collector ON "plotData".collector_id = collector.id
                WHERE 
                    "plotData".is_void = FALSE
                    AND "plotData".id = (:plotDataDbId)
                ORDER BY variable.abbrev
            `

            // Retrieve plots data from the database   
            let plotsData = await sequelize.query(plotsDataQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    plotDataDbId: plotDataDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await plotsData == undefined || await plotsData.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404012)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: plotsData
            })
            return
        }
  },
  /**
 * Update a specific record in experiment.plot_data
 * PUT /v3/plots-data/:id
 * @param {*} req Request parameters
 * @param {*} res Response
 */
  put: async function (req, res) {

    let dataQCCodeList = ['N', 'G', 'Q', 'S', 'M', 'B']
    let resultArray
    let setQuery = []
    let plotDataDbId = req.params.id
    let params = req.body
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).secure

    // Set URL for response
    let plotDataUrl = (isSecure ? 'https' : 'http')
      + '://' + req.headers.host + '/v3/plots-data/'
      + plotDataDbId

    // check if plotDbId is integer
    if (!validator.isInt(plotDataDbId)) {
      let errMsg = 'Invalid format, plotDbId must be an integer.'
      errorCode = await responseHelper.getErrorCodebyMessage(
        400,
        errMsg)
      errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if plotDataDbId exists
    let plotDataQuery = `
      SELECT *
      FROM 
        experiment.plot_data 
      WHERE 
        id=${plotDataDbId}
    `
    let plotDataRecord = await sequelize.query(plotDataQuery, {
      type: sequelize.QueryTypes.SELECT
    })
    if (await plotDataRecord == undefined || await plotDataRecord.length == 0) {
      let errMsg = 'The plotDbId does not exist.'
      errorCode = await responseHelper.getErrorCodebyMessage(
        404,
        errMsg)
      errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    if (params.dataQCCode !== undefined) {

      // check if the parameter dataQCCode is not the same with the existing dataQCCode
      if (plotDataRecord[0]['data_qc_code'] != params.dataQCCode) {

        // check if dataQCCode
        if (dataQCCodeList.indexOf(params.dataQCCode) == -1) {
          let errMsg = 'The dataQCCode is invalid. dataQCCode should be one '
            + 'of the following: ' + dataQCCodeList.toString()
          errorCode = await responseHelper.getErrorCodebyMessage(
            400,
            errMsg)
          errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
          res.send(new errors.BadRequestError(errMsg))
          return
        } else {
          setQuery.push(`data_qc_code = '${params.dataQCCode}'`)
        }
      }
    }

    if (params.dataValue !== undefined) {

      // check if the parameter dataValue is not the same with the existing dataValue
      if (plotDataRecord[0]['data_value'] != params.dataValue) {
        setQuery.push(`data_value = '${params.dataValue}'`)
      }
    }

    // Cannot update the transactionDbId alone without dataValue or dataQCCode
    if (params.transactionDbId !== undefined && setQuery.length > 0) {

      /* Check if the parameter transactionDbId is not the same with the existing 
        transaction_id.
      */
      if (plotDataRecord[0]['transaction_id'] != params.transactionDbId) {

        // Check if transaction ID exist
        transactionQuery = `
          SELECT exists(
            SELECT 1
            FROM 
              data_terminal.transaction 
            WHERE 
              id = ${params.transactionDbId} 
          )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        if (!transactionResult[0]['exists']) {
          let errMsg = 'The terminal transaction ID does not exist.'
          errorCode = await responseHelper.getErrorCodebyMessage(
            404,
            errMsg)
          errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
          res.send(new errors.NotFoundError(errMsg))
          return
        } else {
          setQuery.push(`transaction_id = ${params.transactionDbId}`)
        }
      }
    } else if (params.transactionDbId !== undefined && setQuery.length == 0) {
      let errMsg = 'The transactionDbId cannot be updated without dataValue or dataQCCode.'
      errorCode = await responseHelper.getErrorCodebyMessage(
        400,
        errMsg)
      errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    if (setQuery.length == 0) {
      resultArray = {
        plotDataDbId: plotDataDbId,
        recordCount: 0,
        href: plotDataUrl
      }
      res.send(200, {
        rows: resultArray
      })
      return
    }

    try {
      setQuery = setQuery.toString()
      let updateQuery = `
        UPDATE
          experiment.plot_data
        SET
          ${setQuery},
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          id = ${plotDataDbId}
      `

      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      await sequelize.query(updateQuery, {
        type: sequelize.QueryTypes.UPDATE,
        transaction: transaction
      })

      let resultArray = {
        plotDataDbId: plotDataDbId,
        recordCount: 1,
        href: plotDataUrl
      }
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return

    } catch (err) {

      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}