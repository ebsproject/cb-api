/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, Plot } = require('../../../config/sequelize')
let errors = require('restify-errors')
let jwtDecode = require('jwt-decode')
let aclStudy = require('../../../helpers/acl/study.js')
let userValidator = require('../../../helpers/person/validator.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let validator = require('validator')
var url = require('url')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {


    // Endpoint for adding a plot data record
    post: async function (req, res, next) {

        let plotDataIdArray = []

        // Get the url
        var plotDataUrlString = (req.connection && req.connection.encrypted ? 'https' : 'http')
            + "://"
            + req.headers.host
            + (url.parse(req.url)).pathname

        // Check for parameters
        if (req.body != null) {

            // Parse the input
            let data = req.body
            let records = []
            try {
                records = data.records

                if (records == undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            } catch (e) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            // Start transaction
            let transaction

            try {
                // get transaction
                transaction = await sequelize.transaction({ autocommit: false });

                for (var item in records) {
                    if (!records[item].variableDbId || !records[item].variableValue || !records[item].collectorDbId || !records[item].plotDbId) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400039)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    let plotDataQuery = `
                        INSERT INTO
                            operational.plot_data (
                                study_id,
                                entry_id,
                                plot_id,
                                variable_id,
                                value,
                                creator_id,
                                creation_timestamp,
                                is_void,
                                notes
                            )
                        SELECT
                            study_id, 
                            entry_id, 
                            id, `
                        + records[item].variableDbId + `,'`
                        + records[item].variableValue + `',`
                        + records[item].collectorDbId + `,
                            NOW(),
                            FALSE,
                            'created via CB API'
                        FROM
                            operational.plot
                        WHERE
                            id = ` + records[item].plotDbId + `
                        RETURNING 
                            id
                    `

                    await sequelize.query(plotDataQuery, {
                        type: sequelize.QueryTypes.INSERT,
                        transaction: transaction
                    })
                        .catch(async err => {
                            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                            res.send(new errors.InternalError(errMsg))
                            return
                        })
                        .then(function (plotDataArray) {

                            let plotDataDbId = plotDataArray[0][0]["id"]
                            let returnArray = {
                                plotDataDbId: plotDataDbId,
                                href: plotDataUrlString + "/" + plotDataDbId
                            }
                            plotDataIdArray.push(returnArray)
                        })
                }
                // Commit the transaction
                await transaction.commit()

                res.send(200, {
                    rows: plotDataIdArray
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback();
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            }
        }
        else {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
    }
}