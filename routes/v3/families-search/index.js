/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(req.body, excludedParametersArray)
      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let familiesQuery = null
    // Check if the client specified values for the value/s
    if (parameters['fields']) {
      familiesQuery = knex.column(parameters['fields'].split('|'))
      familiesQuery += `
        FROM
          master.family "family"
        LEFT JOIN
          master.user creator ON family.creator_id = creator.id
        LEFT JOIN
          master.user modifier ON family.modifier_id = modifier.id
        LEFT JOIN
          operational.cross "cross" ON family.cross_id = "cross".id
        LEFT JOIN
          master.product parent ON family.parent_product_id = parent.id
        LEFT JOIN
          master.product child ON family.child_product_id = child.id
        LEFT JOIN
          operational.study study ON family.source_study_id = study.id
        LEFT JOIN
          operational.plot plot ON family.source_plot_id = plot.id
        LEFT JOIN
          operational.entry entry ON family.source_entry_id = entry.id
        WHERE
          family.is_void = FALSE
        ` + addedConditionString + `
        ORDER BY
          family.id
        `
    } else {
      familiesQuery = `
        SELECT
          family.id AS "familyDbId",
          family.selection_number AS "selectionNumber",
          "cross".id AS "crossDbId",
          "cross".cross_name AS "crossName",
          parent.id AS "parentProductDbId",
          parent.display_name AS "parentProduct",
          child.id AS "childProductDbId",
          child.display_name AS "childProduct",
          plot.id As "sourcePlotDbId",
          plot.code AS "plotCode",
          entry.id AS "sourceEntryDbId",
          entry.entcode AS "entryCode",
          study.id AS "sourceStudyDbId",
          study.name AS "study",
          family.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.display_name AS "creator",
          family.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.display_name AS "modifier"
        FROM
          master.family "family"
        LEFT JOIN
          master.user creator ON family.creator_id = creator.id
        LEFT JOIN
          master.user modifier ON family.modifier_id = modifier.id
        LEFT JOIN
          operational.cross "cross" ON family.cross_id = "cross".id
        LEFT JOIN
          master.product parent ON family.parent_product_id = parent.id
        LEFT JOIN
          master.product child ON family.child_product_id = child.id
        LEFT JOIN
          operational.study study ON family.source_study_id = study.id
        LEFT JOIN
          operational.plot plot ON family.source_plot_id = plot.id
        LEFT JOIN
          operational.entry entry ON family.source_entry_id = entry.id
        WHERE
          family.is_void = FALSE
        ` + addedConditionString + `
        ORDER BY
          family.id
        `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    let familiesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      familiesQuery,
      conditionString,
      orderString
    )
    // Retrieve the family records
    let families = await sequelize
      .query(familiesFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset:offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await families == undefined || await families.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get the final count of the entry records
      let familiiesCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          familiesQuery,
          conditionString,
          orderString
        )

      let familyCount = await sequelize
        .query(familiiesCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = familyCount[0].count

      res.send(200, {
        rows: families,
        count: count
      })
      return
    }
  }
}