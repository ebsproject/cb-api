/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let experimentHelper = require('../../../../helpers/experiment/index.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for retrieving the list of data for a experiment
    // Implementation of GET call for /v3/experiments/:id/data
    get: async (req, res, next) => {

        // Retrieve the experiment ID

        let experimentDbId = req.params.id

        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {

            let addedConditionString = `AND experiment.id = ${experimentDbId}`

            // Build query
            let experimentsQuery = await experimentHelper.getExperimentQuerySql(addedConditionString)

            // Retrieve experiments from the database
            let experiments = await sequelize.query(experimentsQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (experiments === undefined || experiments.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404008)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (experiment of experiments) {

                let experimentDataQuery = `
                    SELECT
                        "experimentData".id AS "experimentDataDbId",
                        "experimentData".variable_id AS "variableDbId",
                        variable.abbrev AS "variableAbbrev",
                        variable.label AS "variableLabel",
                        "experimentData".data_value AS "dataValue",
                        "experimentData".data_qc_code AS "dataQcCode",
                        "experimentData".protocol_id AS "protocolDbId",
                        protocol.protocol_code AS "protocolCode",
                        protocol.protocol_name AS "protocolName",
                        protocol.protocol_type AS "protocolType",
                        protocol.description AS "protocolDescription",
                        "experimentData".creation_timestamp AS "creationTimestamp",
                        creator.id AS "creatorDbId",
                        creator.person_name AS creator,
                        "experimentData".modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.person_name AS modifier
                    FROM
                        experiment.experiment_data "experimentData"
                    LEFT JOIN 
                        master.variable variable ON variable.id = "experimentData".variable_id
                    LEFT JOIN
                        tenant.protocol protocol ON protocol.id = "experimentData".protocol_id
                    LEFT JOIN 
                        tenant.person creator on creator.id = "experimentData".creator_id
                    LEFT JOIN 
                        tenant.person modifier on modifier.id = "experimentData".modifier_id
                    WHERE
                        "experimentData".is_void = FALSE
                        AND "experimentData".experiment_id = ${experimentDbId}
                    ORDER BY 
                        variable.abbrev
                `

                // Retrieve experiment data from the database
                let experimentData = await sequelize.query(experimentDataQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                experiment["data"] = experimentData
            }

            res.send(200, {
                rows: experiments
            })
            return
        }
    },

    // Endpoint for adding an experiment data record
    post: async function (req, res, next) {
        // Retrieve the experiment ID
        let experimentDbId = req.params.id
        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        // Retrieve the person ID via access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let addedConditionString = `AND experiment.id = (${experimentDbId})`

        // Build the query
        let experimentQuery = await experimentHelper.getExperimentQuerySql(
            addedConditionString,
        )

        // Retrieve experiment from the db
        let experiments = await sequelize.query(experimentQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (experiments === undefined || experiments.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404008)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let resultArray = []
        
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let experimentDataUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/experiments/' + experimentDbId + '/data'

        // Check for parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records == undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Start transaction
        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let experimentDataValueArray = []
            
            for (let record of records) {
                let validateQuery = ''
                let validateCount = 0

                let variableDbId = null
                let dataValue = null
                let dataQcCode = null
                let protocolDbId = null

                if (record.variableDbId == undefined || record.dataValue == undefined) {
                    let errMsg = 'Required parameters are missing. Make sure that the variable ID and data value fields are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                variableDbId = record.variableDbId
                if (!validator.isInt(variableDbId)) {
                    let errMsg = 'Invalid request, variable ID must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if variableDbId exists
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if variable exists, return 1
                        SELECT
                            count(1)
                        FROM
                            master.variable variable
                        WHERE
                            variable.is_void = FALSE AND
                            variable.id = ${variableDbId}
                    )
                `
                validateCount += 1

                // Check if experimentDbId-variableDbId combination is unique
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if variable exists, return 1
                        SELECT
                            CASE WHEN (count(1) = 0)
                                THEN 1
                                ELSE 0
                            END AS count
                        FROM
                            experiment.experiment_data ed
                        WHERE
                            ed.is_void = FALSE AND
                            ed.experiment_id = ${experimentDbId} AND
                            ed.variable_id = ${variableDbId}
                    )
                `
                validateCount += 1

                dataValue = (record.dataValue != undefined) ? record.dataValue : ''
                dataQcCode = (record.dataQcCode != undefined) ? record.dataQcCode : 'N'
                
                if (record.protocolDbId != undefined) {
                    protocolDbId = record.protocolDbId
                    if (!validator.isInt(protocolDbId)) {
                        let errMsg = 'Invalid request, protocol ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    
                    // Check if it actually exists
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if protocol exists, return 1
                            SELECT
                                count(1)
                            FROM
                                experiment.experiment_protocol "experimentProtocol"
                            WHERE
                                "experimentProtocol".is_void = FALSE AND
                                "experimentProtocol".protocol_id = ${protocolDbId}
                        )
                    `
                    validateCount += 1
                }
                // Validate the input
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get the values
                let tempArray = [
                    experimentDbId,
                    variableDbId,
                    dataValue,
                    dataQcCode,
                    protocolDbId,
                    personDbId,
                ]

                experimentDataValueArray.push(tempArray)
            }

            // Create an experiment data record
            let experimentDataQuery = format(`
                INSERT INTO
                    experiment.experiment_data (
                        experiment_id,
                        variable_id,
                        data_value,
                        data_qc_code,
                        protocol_id,
                        creator_id
                    )
                VALUES
                    %L
                RETURNING
                    id
                `, experimentDataValueArray
            )

            let experimentData = await sequelize.query(experimentDataQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            for (e of experimentData[0]) {
                experimentDataDbId = e.id
                let edObj = {
                    experimentDataDbId: experimentDataDbId,
                    recordCount: 1,
                    href: experimentDataUrlString + '/' + experimentDataDbId
                }

                resultArray.push(edObj)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}