/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let plotCodeGenerator = require('../../../../helpers/plotCodeGenerator')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')

module.exports = {
    post: async function (req, res, next) {
        let experimentDbId = req.params.id

        if (!validator.isInt(experimentDbId)) {
            let errMsg = 'Invalid request, experiment ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        /**
         *  Check if the request body exists
         */
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction
        let plotCount

        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let identifier = Object.keys(req.body)[0]

            if (identifier !== undefined) {
                if (
                    identifier !== 'offset' &&
                    !validator.isInt(identifier)
                ) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400240)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (identifier !== 'offset') {
                // Validate if key is integer
                if (!validator.isInt(identifier)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                plotCount = await plotCodeGenerator
                    .generateOccurrencePlotCode(
                        req.body,
                        experimentDbId,
                        res
                    )

                if (typeof plotCount === 'boolean') {
                    await transaction.rollback()
                    return
                }
            } else {
                let query = `
                    SELECT
                        data_value::json
                    FROM
                        experiment.experiment_data
                    WHERE
                        variable_id IN (
                            SELECT
                                id
                            FROM
                                master.variable
                            WHERE
                                abbrev = 'PLOT_CODE_PATTERN'
                        ) AND
                        is_void = FALSE AND
                        experiment_id = ${experimentDbId}
                    LIMIT
                        1
                `

                let experimentQuery = await sequelize.query(query, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (experimentQuery.length == 0) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400242)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                plotCount = await plotCodeGenerator
                    .generateExperimentPlotCode(
                        req.body,
                        experimentDbId,
                        experimentQuery,
                        res
                    )
                if (typeof plotCount === 'boolean') {
                    await transaction.rollback()
                    return
                }
            }

            let result = {
                experimentDbId: experimentDbId,
                recordCount: plotCount
            }

            await transaction.commit()

            res.send(200, {
                rows: result
            })
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}