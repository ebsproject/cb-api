/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let experimentHelper = require('../../../../helpers/experiment/index')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let errors = require('restify-errors')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}
    let usage = req.query.usage

    let experimentDbId = req.params.id
    
    if (!validator.isInt(experimentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400025)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    addedConditionString = `AND experiment.id = ${experimentDbId}`

    // Build the query to retrieve the said experiment
    let experimentsQuery = await experimentHelper
      .getExperimentQuerySql(addedConditionString)


    let experiments = await sequelize
      .query(experimentsQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (experiments == undefined || experiments.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404008)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
     // Set the columns to be excluded in the parameters for filtering
      let excludedParametersArray = ['fields']
      // Get the filter condition
      conditionString = await processQueryHelper
        .getFilter(req.body, excludedParametersArray)
      
      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req. headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let usageCond = '';
    // if usage is set
    if (usage != null) {
      usageCond = `AND variable.usage ilike '%` + usage + `%'`
    }

    // Build the base retrieval query
    let experimentDataQuery = null
    // Check if the client specified values in fields
    if (parameters['fields']) {
      experimentDataQuery = knex.column(parameters['fields'].split('|'))
      experimentDataQuery += `
        FROM
          master.variable,
          experiment.experiment_data AS experimentData
        LEFT JOIN
          experiment.experiment experiment ON experimentData.experiment_id = experiment.id
        LEFT JOIN
          tenant.protocol ON experimentData.protocol_id = protocol.id
        LEFT JOIN
          tenant.person creator ON experimentData.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON experimentData.modifier_id = modifier.id
        WHERE
          experimentData.is_void = FALSE AND
          experimentData.experiment_id = ${experimentDbId} AND
          experimentData.variable_id = variable.id
          ${usageCond}
        ORDER BY
          experimentData.id
      `
    } else {
      experimentDataQuery = `
        SELECT
          experimentData.id AS "experimentDataDbId",
          experimentData.data_value AS "dataValue",
          experimentData.data_qc_code AS "dataQcCode",
          variable.id AS "variableDbId",
          variable.name AS "variable",
          variable.label,
          variable.abbrev,
          protocol.id AS "protocolDbId",
          protocol.protocol_name AS "protocol",
          experimentData.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          experimentData.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          master.variable,
          experiment.experiment_data AS experimentData
        LEFT JOIN
          experiment.experiment experiment ON experimentData.experiment_id = experiment.id
        LEFT JOIN
          tenant.protocol ON experimentData.protocol_id = protocol.id
        LEFT JOIN
          tenant.person creator ON experimentData.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON experimentData.modifier_id = modifier.id
        WHERE
          experimentData.is_void = FALSE AND
          experimentData.experiment_id = ${experimentDbId} AND
          experimentData.variable_id = variable.id
          ${usageCond}
        ORDER BY
          experimentData.id
      `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }


    // Generate the final SQL query
    let experimentDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      experimentDataQuery,
      conditionString,
      orderString
    )
    
    let experimentData = await sequelize
      .query(experimentDataFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.BadRequestError(errMsg))
        return
      })

    if (await experimentData === undefined || await experimentData.length < 1) {
      experimentData = []
    }

    experiments[0]['data'] = experimentData

    res.send(200, {
      rows: experiments
    })
    return
  }
}