/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let distinctString = ''
        let distinctOn = null
        let parameters = {}

        let experimentDbId = req.params.id

        if (!validator.isInt(experimentDbId)) {
            let errMsg = 'Invalid request, experiment ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let experimentQuery = `
            SELECT
                experiment.id AS "experimentDbId"
            FROM
                experiment.experiment experiment
            WHERE
                experiment.is_void = FALSE AND
                experiment.id = ${experimentDbId}
        `

        // Retrieve the experiment using the ID
        let experiment = await sequelize
            .query(experimentQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await experiment == undefined || await experiment.length < 1) {
            let errMsg = 'Resource not found, the experiment you have provided does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinctOn condition is set
            if (req.body.distinctOn != undefined) {
                distinctOn = req.body.distinctOn
                distinctString = await processQueryHelper
                    .getDistinctString(distinctOn)
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the entry data retrieval query
        let entryDataQuery = null

        if (parameters['fields'] != null) {
            entryDataQuery = knex.column(parameters['fields'].split('|'))
            
            entryDataQuery += `
                FROM
                    experiment.entry_data "entryData"
                LEFT JOIN
                    experiment.entry entry ON entry.id = "entryData".entry_id
                LEFT JOIN
                    experiment.entry_list "entryList" ON "entryList".id = entry.entry_list_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = "entryList".experiment_id
                LEFT JOIN
                    master.variable variable ON variable.id = "entryData".variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = "entryData".creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = "entryData".modifier_id
                WHERE
                    "entryData".is_void = FALSE AND
                    entry.is_void = FALSE AND
                    entry.id = "entryData".entry_id AND
                    "entryList".experiment_id = ${experimentDbId}
                ORDER BY
                    "entryData".id
            `
        } else {
            entryDataQuery = `
                SELECT
                    "entryData".id AS "entryDataDbId",
                    entry.id AS "entryDbId",
                    variable.id AS "variableDbId",
                    variable.abbrev AS "variableAbbrev",
                    "entryData".data_value AS "dataValue",
                    "entryData".data_qc_code AS "dataQcCode",
                    "entryData".transaction_id AS "transactionDbId",
                    "entryData".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    "entryData".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    experiment.entry_data "entryData"
                LEFT JOIN
                    experiment.entry entry ON entry.id = "entryData".entry_id
                LEFT JOIN
                    experiment.entry_list "entryList" ON "entryList".id = entry.entry_list_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = "entryList".experiment_id
                LEFT JOIN
                    master.variable variable ON variable.id = "entryData".variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = "entryData".creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = "entryData".modifier_id
                WHERE
                    "entryData".is_void = FALSE AND
                    entry.is_void = FALSE AND
                    entry.id = "entryData".entry_id AND
                    "entryList".experiment_id = ${experimentDbId}
                ORDER BY
                    "entryData".id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let entryDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            entryDataQuery,
            conditionString,
            orderString,
            distinctString,
        )

        let entryData = await sequelize
            .query(entryDataFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await entryData == undefined || await entryData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let entryDataCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                entryDataQuery,
                conditionString,
                orderString,
                distinctString,
            )

        let entryDataCount = await sequelize
            .query(entryDataCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        count = entryDataCount[0].count

        res.send(200, {
            rows: entryData,
            count: count
        })

        return
    }
}