/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let searchHelper = require('../../../../helpers/queryTool/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')
let userValidator = require('../../../../helpers/person/validator.js')
let validator = require('validator')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let occurrenceCond = ``
        let parameters = {}
        let condition = ''
        let experimentDbId = req.params.id
        let usage = req.query.usage ?? 'experiment_manager'
        let accessDataQuery = ``
        let responseColString = `
            -- Retrieve Occurrence info
            occurrence.id AS "occurrenceDbId",
            occurrence.occurrence_name AS "occurrenceName",
            occurrence.occurrence_status AS "occurrenceStatus",
            occurrence.description AS "description",
            occurrence.remarks AS "remarks",

            -- Retrieve Parent Experiment info
            experiment.id AS "experimentDbId",
            experiment.experiment_code AS "experimentCode",
            experiment.experiment_name AS "experimentName",
            experiment.experiment_design_type AS "experimentDesignType",

            -- Retrieve Site code of this Occurrence
            site.id AS "siteDbId",
            site.geospatial_object_code AS "siteCode",
            site.geospatial_object_name AS "site",

            -- Retrieve Field code of this Occurrence
            field.id AS "fieldDbId",
            field.geospatial_object_code AS "fieldCode",
            field.geospatial_object_name AS "field",

            -- Retrieve Location info of this Occurrence
            location.id AS "locationDbId",
            location.location_name AS "locationName",
            location.location_code AS "locationCode",

            occurrence.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            occurrence.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier
        `
        const endpoint = 'experiments/:id/occurrence-data-table-search'
        const operation = 'SELECT'

        // Check if Experiment ID is a valid ID
        if (!validator.isInt(experimentDbId)) {
            let errMsg = 'Invalid request, experimentDbId must be an integer'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req);

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if experiment exists
        let experimentQuery = `
            SELECT
                count(1)
            FROM
                experiment.experiment experiment
            WHERE
                id = ${experimentDbId} AND
                is_void = FALSE
        `

        let experiment = await sequelize.query(experimentQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (await experiment == undefined || await experiment.length < 0) {
            let errMsg = 'The experiment ID you have provided does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if user is admin or not
        let isAdmin = await userValidator.isAdmin(userId)

        // If not admin, retrieve only occurrences user have access to
        if(!isAdmin){

            // Get all programs that user belongs to
            let getProgramsQuery =  `
                SELECT 
                    program.id 
                FROM 
                    tenant.program program,
                    tenant.program_team pt,
                    tenant.team_member tm
                WHERE
                    tm.person_id = ${userId}
                    AND pt.team_id = tm.team_id
                    AND program.id = pt.program_id
                    AND tm.is_void = FALSE
                    AND pt.is_void = FALSE
                    AND program.is_void = FALSE
                `

            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // if program does not exist
            if(programIds.length == 0){
                accessDataQuery = `occurrence.access_data #> $$\{program,0}$$ is not null`
            }

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                accessDataQuery += `occurrence.access_data #> $$\{program,${programObj.id}}$$ is not null`

            }

            // include occurrences shared to the user
            let userQuery = `occurrence.access_data #> $$\{person,${userId}}$$ is not null`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            }

            if(accessDataQuery != ``){
                accessDataQuery = ` AND (${accessDataQuery})`
            }

            // Get all Occurrences that user has access to
            let occurrenceQuery = `
                SELECT
                    id
                FROM
                    experiment.occurrence occurrence
                WHERE
                    occurrence.experiment_id = ${experimentDbId}
                    AND occurrence.is_void = FALSE
                    ${accessDataQuery}
            `

            let occurrenceIds = await sequelize.query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // Build occurrence id condition
            let occurrenceIdsCond = ``
            for (let occurrenceId of occurrenceIds) {
                if(occurrenceIdsCond == ``){
                    occurrenceIdsCond = occurrenceId['id']
                } else {
                    occurrenceIdsCond += `,` + occurrenceId['id'] 
                }
            }

            occurrenceIdsCond = (occurrenceIdsCond == ``) ? '0' : occurrenceIdsCond

            occurrenceCond = ` AND occurrence.id in (` + occurrenceIdsCond + `)`
        }

        // Get all the Occurrence Data variables under the Experiment
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') AS "variableId",
                STRING_AGG( '"' || abbrev || '"', ',') AS "selectColumns",
                STRING_AGG( abbrev, ',') AS "variableAbbrev",
                STRING_AGG( '"' || abbrev || '"' || ' text', ',') AS "crosstabColumns"
            FROM(
                SELECT 
                    distinct (occurrence_data.variable_id) AS variable_id,
                    v.abbrev
                FROM
                    experiment.experiment,
                    experiment.occurrence,
                    experiment.occurrence_data,
                    master.variable v
                WHERE
                    experiment.id = ${experimentDbId} AND
                    occurrence.experiment_id = experiment.id AND
                    occurrence_data.occurrence_id = occurrence.id AND
                    occurrence_data.is_void = FALSE AND
                    occurrence.id = occurrence_data.occurrence_id AND
                    occurrence.is_void = FALSE AND
                    v.id = occurrence_data.variable_id AND
                    v.is_void = FALSE AND
                    v.data_level ilike '%occurrence%' AND
                    v.usage ilike '%${usage}%'
                    ${occurrenceCond}
                ORDER BY
                    v.abbrev
            )a
            `

        let variables = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        // Process the variable information
        let variableId = variables[0]['variableId'] == null ? [0] : variables[0]['variableId']
        let crosstabColumns = variables[0]['crosstabColumns']
        let selectColumns = variables[0]['selectColumns']

        let variableAbbrev = variables[0]['variableAbbrev'] == null ? [] : variables[0]['variableAbbrev'].split(',')


        if (crosstabColumns != "" && crosstabColumns != null) {
            crosstabColumns = ',' + crosstabColumns
        } else if (crosstabColumns == null) {
            crosstabColumns = ', "null" text';
        }
        if (selectColumns != "" && selectColumns != null) {
            selectColumns = ',' + selectColumns
        } else if (selectColumns == null) {
            selectColumns = ''
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set columns to be excluded in parameters for filtering
            Array.prototype.diff = function (a) {
                return this.filter(function(i) {
                    return a.indexOf(i) < 0
                })
            }

            // Exclude all measurement variables
            let excludedParametersArray = []

            excludedParametersArray.push(
                'fields',
                'dataQcCode',
                'dataValue',
            )

            for (abbrev of variableAbbrev) {
                excludedParametersArray.push(abbrev)
            }

            // Get the filter condition
            let conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''
            conditionArray = []

            // Get filters for Occurrence Data with values that is in JSON format
            for (metadata in req.body) {
                if (variableAbbrev.includes(metadata)) {
                    for (field in req.body[metadata]) {
                        condition = ''
                        let variableQuery = `
                            SELECT 
                                data_type
                            FROM
                                master.variable v
                            WHERE
                                v.abbrev='${metadata}'
                        `

                        let variableList = await sequelize.query(variableQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        let variableDataType = variableList[0]['data_type']
                        value = req.body[metadata]

                        column = '("' + metadata + '")::' + variableDataType
                        condition = searchHelper.getFilterCondition(value, column, columnCast = true)

                        if (condition != '') {
                            conditionArray.push(`(` + condition + ')')
                        }

                    }
                }
            }

            if (conditionArray.length > 0) {
                condition = conditionArray.join(' AND ')
                if (conditionString == '') {
                    conditionString = conditionString +
                        ` WHERE `
                        + condition
                } else {
                    conditionString = conditionString +
                        ` AND `
                        + condition
                }
            }

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let occurrenceDataQuery = null
        let fromQuery = `
            FROM
                experiment.experiment experiment,
                experiment.occurrence occurrence
            LEFT JOIN
                (
                    SELECT 
                        "occurrenceDbId" 
                        ${selectColumns}
                    FROM
                        CROSSTAB(
                            '
                                SELECT 
                                    occurrence.id AS "occurrenceDbId",
                                    occurrence_data.variable_id,
                                    occurrence_data.data_value
                                FROM
                                    experiment.experiment,
                                    experiment.occurrence,
                                    experiment.occurrence_data
                                WHERE
                                    experiment.id = ${experimentDbId} AND
                                    occurrence.experiment_id = experiment.id AND
                                    occurrence.id = occurrence_data.occurrence_id AND
                                    occurrence_data.is_void = FALSE AND
                                    occurrence.is_void = FALSE
                                    ${occurrenceCond}
                                ORDER BY
                                    occurrence.id
                            ',
                            '
                                SELECT UNNEST(ARRAY[${variableId}])
                            '
                        ) as (
                            "occurrenceDbId" integer
                            ${crosstabColumns}
                        ) 
                )crosstab ON crosstab."occurrenceDbId" = occurrence.id
            LEFT JOIN
                place.geospatial_object site
            ON
                site.id = occurrence.site_id
            LEFT JOIN
                place.geospatial_object field
            ON
                field.id = occurrence.field_id
            LEFT JOIN
                experiment.location_occurrence_group log
            ON
                log.occurrence_id = occurrence.id
            LEFT JOIN
                experiment.location location
            ON
                location.id = log.location_id
            LEFT JOIN 
                tenant.person creator
            ON
                creator.id = occurrence.creator_id::integer
            LEFT JOIN 
                tenant.person modifier
            ON
                modifier.id = occurrence.modifier_id::integer
            WHERE
                experiment.id = ${experimentDbId} AND
                occurrence.experiment_id = experiment.id AND
                occurrence.is_void = FALSE AND
                experiment.is_void = FALSE
                ${occurrenceCond}
            ORDER BY
                occurrence.id
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            occurrenceDataQuery = knex.column(parameters['fields'].split('|'))
            occurrenceDataQuery += fromQuery
        } else {
            occurrenceDataQuery = `
                SELECT
                    ${responseColString}
                    ${selectColumns}
                ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 'You have provided an invalid format for orderString.')
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let occurrenceDataFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            occurrenceDataQuery,
            conditionString,
            orderString,
        )
        
        occurrenceDataFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            occurrenceDataFinalSqlQuery,
            orderString,
        )

        // Retrieve the occurrenceData records
        let occurrenceData = await sequelize
            .query(occurrenceDataFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)

                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrenceData == undefined || await occurrenceData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = ( occurrenceData[0] ) ? occurrenceData[0].totalCount : 0

        res.send(200, {
            rows: occurrenceData,
            count: count
        })
    }
}