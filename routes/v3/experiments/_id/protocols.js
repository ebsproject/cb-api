/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

module.exports = {

    get: async (req, res, next) => {
        
        // Retrieve the experiment ID
        let experimentDbId = req.params.id

        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let experimentProtocolsQuery = `
            SELECT
                ep.id AS "experimentProtocolDbId",
                experiment.id AS "experimentDbId",
                protocol.id AS "protocolDbId",
                ep.order_number AS "orderNo",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                ep.creation_timestamp AS "creationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier",
                ep.modification_timestamp AS "modificationTimestamp",
                ep.notes
            FROM
                experiment.experiment_protocol ep
            LEFT JOIN
                tenant.person creator ON ep.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON ep.modifier_id = modifier.id
            LEFT JOIN
                experiment.experiment ON ep.experiment_id = experiment.id
            LEFT JOIN
                tenant.protocol ON ep.protocol_id = protocol.id
            WHERE
                ep.is_void = FALSE AND
                ep.experiment_id = ${experimentDbId}
        `

        // Retrieve experiment protocol
        let experimentProtocols = await sequelize
            .query(programsQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await experimentProtocols == undefined ||
            experimentProtocols.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404016)
            res.send(new errors.NotFoundError(errMsg))
            return    
        }

        res.send(200, {
            rows: experimentProtocols
        })

        return
  }
}