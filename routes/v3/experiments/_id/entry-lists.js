/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let experimentHelper = require('../../../../helpers/experiment/index.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the list of entryLists for a experiment
    // Implementation of GET call for /v3/experiments/:id/entry-lists
    get: async (req, res, next) => {

        // Retrieve the experiment ID

        let experimentDbId = req.params.id

        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {

            // Retrieve the user ID of the client from the access token
            let userId = await tokenHelper.getUserId(req);

            if (userId == null) {
              let errMsg = await errorBuilder.getError(req.headers.host, 401002)
              res.send(new errors.BadRequestError(errMsg))
              return
            }

            let addedConditionString = `AND experiment.id = (:experimentDbId)`

            // Build query
            let experimentsQuery = await experimentHelper.getExperimentQuerySql(addedConditionString)

            // Retrieve experiments from the database   
            let experiments = await sequelize.query(experimentsQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    experimentDbId: experimentDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (experiments.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404008)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (experiment of experiments) {

                let entryListsQuery = `
                    SELECT
                        "entryList".id AS "entryListDbId",
                        "entryList".entry_class AS "entryClass",
                        "entryList".entry_status AS "entryStatus",
                        product.id AS "productDbId",
                        "entryList".entlistno AS "entryListNo",
                        "entryList".entry_type AS "entryType",
                        author.id AS "authorDbId",
                        author.display_name AS "author",
                        "seedStorage".id AS "seedStorageDbId",
                        "seedStorage".key_type AS "keyType",
                        "seedStorage".label AS "seedStorageLabel",
                        "seedStorage".seed_manager AS "seedManager",
                        "seedStorage".unit AS "seedStorageVolumeUnit",
                        "entryList".gid,
                        "entryList".product_name AS "productName",
                        "entryList".remarks,
                        "entryList".creation_timestamp AS "creationTimestamp",
                        creator.id AS "creatorDbId",
                        creator.display_name AS creator,
                        "entryList".modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.display_name AS modifier
                    FROM
                        operational.entry_list "entryList"
                    LEFT JOIN 
                        master.product product ON product.id = "entryList".product_id
                    LEFT JOIN 
                        master.user author ON author.id =  "entryList".author_id::integer
                    LEFT JOIN 
                        operational.seed_storage_log AS "seedStorageLog" ON "seedStorageLog".id = "entryList".seed_storage_log_id
                    LEFT JOIN 
                        operational.seed_storage AS "seedStorage" ON "seedStorage".id = "seedStorageLog".seed_storage_id
                    LEFT JOIN 
                        master.user creator on creator.id = "entryList".creator_id
                    LEFT JOIN 
                        master.user modifier on modifier.id = "entryList".modifier_id 
                    WHERE
                        "entryList".is_void = FALSE
                        AND "entryList".experiment_id = (:experimentDbId)
                    ORDER BY 
                        "entryList".entlistno
                `

                // Retrieve entry lists from the database   
                let entryLists = await sequelize.query(entryListsQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        experimentDbId: experiment.experimentDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                experiment["entryLists"] = entryLists
            }

            res.send(200, {
                rows: experiments
            })
            return
        }
    }
}