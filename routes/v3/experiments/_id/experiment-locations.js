/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let experimentHelper = require('../../../../helpers/experiment/index.js')
let studyHelper = require('../../../../helpers/study/index.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the list of studies for a experiment
    // Implementation of GET call for /v3/experiments/:id/experiment-locations
    get: async (req, res, next) => {

        // Retrieve the experiment ID

        let experimentDbId = req.params.id

        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {

            let addedConditionString = `AND experiment.id = (:experimentDbId)`

            // Build query
            let experimentsQuery = await experimentHelper.getExperimentQuerySql(addedConditionString)

            // Retrieve experiments from the database   
            let experiments = await sequelize.query(experimentsQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    experimentDbId: experimentDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (experiments.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404008)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (experiment of experiments) {

                let addedConditionString = `AND study.experiment_id = (:experimentDbId)`

                // Build Query
                let experimentLocationsQuery = await studyHelper.getStudyQuerySql(addedConditionString, '', true)

                // Retrieve study/experiment location from the database   
                let experimentLocations = await sequelize.query(experimentLocationsQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        experimentDbId: experimentDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                experiment["experimentLocations"] = experimentLocations
            }

            res.send(200, {
                rows: experiments
            })
            return
        }
    }
}