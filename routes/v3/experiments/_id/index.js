/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let experimentHelper = require('../../../../helpers/experiment/index.js')
let userValidator = require('../../../../helpers/person/validator.js')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
const logger = require('../../../../helpers/logger/index.js')
const endpoint = 'experiments/:id'

module.exports = {

  get: async (req, res, next) => {

    // Retrieve experiment ID
    let experimentDbId = req.params.id

    addedConditionString = `AND experiment.id = ${experimentDbId}`;

    // Check if ID is an integer
    if (!validator.isInt(experimentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400025)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Build query
    let experimentQuery = await experimentHelper.getExperimentQuerySql(addedConditionString)

    // Retrieve experiment from the database   
    let experiment = await sequelize.query(experimentQuery, {
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
      })

    if (experiment === undefined || experiment.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404008)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
        rows: experiment
    })
    return
  },

    // PUT /v3/experiments/:id
    put: async function (req, res, next) {
        // Retrieve experiment ID
        let experimentDbId = req.params.id

        // Check if ID is an integer
        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Set defaults
        let projectDbId = null
        let pipelineDbId = null
        let experimentName = null
        let stageDbId = null
        let experimentObjective = null
        let experimentSubType = null
        let experimentSubSubType = null
        let dataProcessDbId = null
        let experimentYear = null
        let seasonDbId = null
        let stewardDbId = null
        let description = null
        let remarks = null
        let programDbId = null
        let notes = null
        let generateExperimentNameFlag = null

        let experimentYearRegex = /^\d{4}$/

        // Build query
        let experimentQuery = `
            SELECT
                project.id AS "projectDbId",
                pipeline.id AS "pipelineDbId",
                experiment.experiment_name AS "experimentName",
                stage.id AS "stageDbId",
                experiment.experiment_objective AS "experimentObjective",
                experiment.experiment_sub_type AS "experimentSubType",
                experiment.experiment_sub_sub_type AS "experimentSubSubType",
                experiment.experiment_year AS "experimentYear",
                experiment.experiment_status AS "experimentStatus",
                experiment.data_process_id AS "dataProcessDbId",
                experiment.experiment_design_type AS "experimentDesignType",
                experiment.notes,
                season.id AS "seasonDbId",
                steward.id AS "stewardDbId",
                program.id AS "programDbId",
                experiment.description,
                experiment.remarks,
                creator.id AS "creatorDbId"
            FROM
                experiment.experiment experiment
                LEFT JOIN
                tenant.project project ON project.id = experiment.project_id
                LEFT JOIN
                tenant.pipeline pipeline ON pipeline.id = experiment.pipeline_id
                LEFT JOIN
                tenant.stage stage ON stage.id = experiment.stage_id
                LEFT JOIN
                tenant.program program ON program.id = experiment.program_id
                LEFT JOIN
                tenant.season season ON season.id = experiment.season_id
                LEFT JOIN
                tenant.person steward ON steward.id = experiment.steward_id
                LEFT JOIN
                tenant.person creator ON creator.id = experiment.creator_id
            WHERE
                experiment.is_void = FALSE AND
                experiment.id = ${experimentDbId}
        `

        // Retrieve experiment record from database
        let experiment = await sequelize.query(experimentQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            logger.logFailingQuery(endpoint, 'SELECT', err)
            return undefined
        })

        if (experiment == undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))

            return
        }

        if (experiment.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404008)
            if (!res.headersSent) res.send(new errors.NotFoundError(errMsg))
            
            return
        }
        
        // Get values from database, record = the current value in the database
        let recordCreatorDbId = experiment[0].creatorDbId

        let recordProjectDbId = experiment[0].projectDbId
        let recordPipelineDbId = experiment[0].pipelineDbId
        let recordStageDbId = experiment[0].stageDbId
        let recordExperimentObjective = experiment[0].experimentObjective
        let recordExperimentSubType = experiment[0].experimentSubType
        let recordExperimentSubSubType = experiment[0].experimentSubSubType
        let recordExperimentYear = experiment[0].experimentYear
        let recordSeasonDbId = experiment[0].seasonDbId
        let recordProgramDbId = experiment[0].programDbId
        let recordStewardDbId = experiment[0].stewardDbId
        let recordDescription = experiment[0].description
        let recordRemarks = experiment[0].remarks
        let recordPlantingSeason = experiment[0].plantingSeason
        let recordExperimentStatus = experiment[0].experimentStatus
        let recordExperimentDesignType = experiment[0].experimentDesignType
        let recordDataProcessDbId = experiment[0].dataProcessDbId
        let recordNotes = experiment[0].notes
        let recordExperimentName = experiment[0].experimentName

        // Check if user is an admin or an owner of the experiment
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${experimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                if (!res.headersSent) res.send(new errors.UnauthorizedError(errMsg))

                return
            }

            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                if (!res.headersSent) res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }
        
        let isSecure = forwarded(req, req.header).secure

        // Set URL for response
        let experimentUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/experiments/'
            + experimentDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let transaction

        try {
            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0
            let changeFlag = 0

        /** Validation of parameters and set values */
            if (data.dataProcessDbId !== undefined) {
                dataProcessDbId = data.dataProcessDbId

                if (!validator.isInt(dataProcessDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400257)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is the same with the current value in the database
                if (dataProcessDbId != recordDataProcessDbId) {
                    // Validate data process ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if data process ID is existing, return 1
                        SELECT
                            count(1)
                        FROM
                            master.item item
                        WHERE
                            item.is_void = FALSE AND
                            item.id = ${dataProcessDbId}
                        )
                    `

                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        data_process_id = $$${dataProcessDbId}$$
                    `
                }
            }

            if (data.stageDbId !== undefined) {
                stageDbId = data.stageDbId

                if (!validator.isInt(stageDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400251)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (stageDbId != recordStageDbId) {

                    // Validate stage ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if stage is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.stage stage
                        WHERE 
                            stage.is_void = FALSE AND
                            stage.id = ${stageDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        stage_id = $$${stageDbId}$$
                    `

                    changeFlag = 1
                }
            } else {
                stageDbId = recordStageDbId
            }

            if (data.projectDbId !== undefined) {
                projectDbId = data.projectDbId

                // Check if user input is same with the current value in the database
                if (projectDbId != recordProjectDbId) {

                    if (projectDbId !== "null") {

                        if (!validator.isInt(projectDbId)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400056)
                            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                        // Validate project ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                            --- Check if project is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.project project
                            WHERE 
                                project.is_void = FALSE AND
                                project.id = ${projectDbId}
                            )
                        `
                        validateCount += 1
                    } else {
                        projectDbId = null
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        project_id = ${projectDbId}
                    `
                }
            }

            if (data.pipelineDbId !== undefined) {
                pipelineDbId = data.pipelineDbId

                // Check if user input is same with the current value in the database
                if (pipelineDbId != recordPipelineDbId) {

                    if (pipelineDbId !== "null") {

                        if (!validator.isInt(pipelineDbId)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400250)
                            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                        // Validate pipeline ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                            --- Check if pipeline is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.pipeline pipeline
                            WHERE 
                                pipeline.is_void = FALSE AND
                                pipeline.id = ${pipelineDbId}
                            )
                        `
                        validateCount += 1
                    } else {
                        pipelineDbId = null
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        pipeline_id = ${pipelineDbId}
                    `
                }
            }

            if (data.experimentYear !== undefined) {
                experimentYear = data.experimentYear

                if (!validator.isInt(experimentYear)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400252)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (!experimentYear.match(experimentYearRegex)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400254)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (experimentYear != recordExperimentYear) {

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_year = $$${experimentYear}$$
                    `

                    changeFlag = 1
                }
            } else {
                experimentYear = recordExperimentYear
            }

            if (data.seasonDbId !== undefined) {
                seasonDbId = data.seasonDbId

                if (!validator.isInt(seasonDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400063)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (seasonDbId != recordSeasonDbId) {

                    // Validate season ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if season is existing return 1
                        SELECT 
                            count(1)
                        FROM
                            tenant.season season
                        WHERE 
                            season.is_void = FALSE AND
                            season.id = ${seasonDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        season_id = $$${seasonDbId}$$
                    `

                    changeFlag = 1
                }
            } else {
                seasonDbId = recordSeasonDbId
            }

            if (data.programDbId !== undefined) {
                programDbId = data.programDbId

                if (!validator.isInt(programDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400125)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (programDbId != recordProgramDbId) {

                    // Validate season ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if program is existing return 1
                        SELECT 
                            count(1)
                        FROM
                            tenant.program program
                        WHERE 
                            program.is_void = FALSE AND
                            program.id = ${programDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        program_id = $$${programDbId}$$
                    `
                }
            } else {
                programDbId = recordProgramDbId
            }

            if (data.stewardDbId !== undefined) {
                stewardDbId = data.stewardDbId

                if (!validator.isInt(stewardDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400253)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (stewardDbId != recordStewardDbId) {

                // Validate steward ID
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                    --- Check if steward is existing return 1
                    SELECT 
                        count(1)
                    FROM 
                        tenant.person steward
                    WHERE 
                        steward.is_void = FALSE AND
                        steward.id = ${stewardDbId}
                    )
                `
                validateCount += 1

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    steward_id = $$${stewardDbId}$$
                `
                }
            }

            if (data.experimentObjective !== undefined) {
                experimentObjective = data.experimentObjective

                // Check if user input is same with the current value in the database
                if (experimentObjective != recordExperimentObjective) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_objective = $$${experimentObjective}$$
                    `
                }
            }

            if (data.experimentDesignType !== undefined) {
                experimentDesignType = data.experimentDesignType

                // Check if user input is same with the current value in the database
                if (experimentDesignType != recordExperimentDesignType) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_design_type = $$${experimentDesignType}$$
                    `
                }
            }

            if (data.experimentSubType !== undefined) {
                experimentSubType = data.experimentSubType

                // Check if user input is same with the current value in the database
                if (experimentSubType != recordExperimentSubType) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_sub_type = $$${experimentSubType}$$
                    `
                }
            }
            
            if (data.experimentSubSubType !== undefined) {
                experimentSubSubType = data.experimentSubSubType

                // Check if user input is same with the current value in the database
                if (experimentSubSubType != recordExperimentSubSubType) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_sub_sub_type = $$${experimentSubSubType}$$
                    `
                }
            }

            if (data.description !== undefined) {
                description = data.description

                // Check if user input is same with the current value in the database
                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        description = $$${description}$$
                    `
                }
            }

            if (data.remarks !== undefined) {
                remarks = data.remarks

                // Check if user input is same with the current value in the database
                if (remarks != recordRemarks) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        remarks = $$${remarks}$$
                    `
                }
            }
            
            if (data.plantingSeason !== undefined) {
                plantingSeason = data.plantingSeason

                // Check if user input is same with the current value in the database
                if (plantingSeason != recordPlantingSeason) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        planting_season = $$${plantingSeason}$$
                    `
                }
            }
            
            if (data.experimentStatus !== undefined) {
                
                experimentStatus = data.experimentStatus
                
                // Check if user input is same with the current value in the database
                if (experimentStatus != recordExperimentStatus) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_status = $$${experimentStatus}$$
                    `
                }
            }

            if (data.notes !== undefined) {
                notes = data.notes

                if(notes == null){
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    notes = ${notes}
                    `
                }else{
                    // Check if user input is same with the current value in the database
                    if (notes != recordNotes) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                        notes = $$${notes}$$
                        `
                    }
                }
        
            }

            if (data.generateExperimentName !== undefined) {
                if (changeFlag == 0) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400258)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }
                
                generateExperimentNameFlag = data.generateExperimentName

                if (generateExperimentNameFlag == 'true') {
                    pattern = await experimentHelper.getPattern("EXPERIMENT_NAME_CONFIG")
                    experimentName = await experimentHelper
                        .generateCode(
                        pattern,
                        programDbId,
                        stageDbId,
                        experimentYear,
                        seasonDbId
                        )
                    
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_name = $$${experimentName}$$
                    `
                }
            }else{
                if (data.experimentName !== undefined) {
                
                    experimentName = data.experimentName
                    
                    // Check if user input is same with the current value in the database
                    if (experimentName != recordExperimentName) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            experiment_name = $$${experimentName}$$
                        `
                    }
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }
    
            if(setQuery.length > 0) setQuery += `,`

            // Update the experiment record
            let updateExperimentQuery = `
                UPDATE
                    experiment.experiment
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    experiment.id = ${experimentDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction(async transaction => {
                return await sequelize.query(updateExperimentQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return transaction info
            let resultArray = {
                experimentDbId: experimentDbId,
                recordCount: 1,
                href: experimentUrlString
            }

            res.send(200, {
                rows: resultArray
            })

            return
        } catch (err) {
            logger.logMessage(__filename, err, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

  // DELETE /v3/experiments/:id
  delete: async function (req, res, next) {
    // Retrieve experiment ID
    let experimentDbId = req.params.id

    // Check if ID is an integer
    if (!validator.isInt(experimentDbId)) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400025)
        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
        return
    }

    // Retrieve user ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if user is an administrator. If not, no access.
    let isAdmin = await userValidator.isAdmin(personDbId)

    let transaction
    try {
        // Check if experiment exists
      let experimentQuery = `
        SELECT 
          experiment.creator_id AS "creatorDbId"
        FROM
          experiment.experiment experiment
        WHERE
          experiment.is_void = FALSE AND
          experiment.id = ${experimentDbId}
      `

      let experiment = await sequelize.query(experimentQuery, {
          type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            logger.logFailingQuery(endpoint, 'DELETE', err)
            return undefined
        })

    if (experiment == undefined) {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        if (!res.headersSent) res.send(new errors.InternalError(errMsg))
        return
      }

      if (experiment.length < 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404008)
        if (!res.headersSent) res.send(new errors.NotFoundError(errMsg))
        return
      }

      // Store the creator ID of the experiment that was retrieved
      let recordCreatorDbId = experiment[0].creatorDbId

      // Check if user is an admin or an owner of the experiment
      if (!isAdmin && (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
            SELECT 
                person.id,
                person.person_role_id AS "role"
            FROM 
                tenant.person person
            LEFT JOIN 
                tenant.team_member teamMember ON teamMember.person_id = person.id
            LEFT JOIN 
                tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
            LEFT JOIN 
                tenant.program program ON program.id = programTeam.program_id 
            LEFT JOIN
                experiment.experiment experiment ON experiment.program_id = program.id
            WHERE 
                experiment.id = ${experimentDbId} AND
                person.id = ${personDbId} AND
                person.is_void = FALSE
        `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            if (!res.headersSent) res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            if (!res.headersSent) res.send(new errors.UnauthorizedError(errMsg))
            return
        }
    }

    let deleteExperimentQuery = format(`
        UPDATE
          experiment.experiment experiment
        SET
          is_void = TRUE,
          experiment_code = CONCAT('VOIDED-', ${experimentDbId}),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          experiment.id = ${experimentDbId}
      `)

      transaction = await sequelize.transaction(async transaction => {
            return await sequelize.query(deleteExperimentQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction,
                raw: true,
            }).catch(err => {
                logger.logFailingQuery(endpoint, 'DELETE', err)
                throw new Error(err)
            })
      })

      res.send(200, {
        rows: { experimentDbId: experimentDbId }
      })
      return
    } catch (err) {
        logger.logMessage(__filename, err, 'error')
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      if (!res.headersSent) res.send(new errors.InternalError(errMsg))
      return
    }
  }
}