/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, Entry } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of advanced search functionality for entry lists of an experiment
    post: async function (req, res, next) {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let sort = req.query.sort

        let count = 0

        let orderString = ''

        let conditionString = ''

        // Retrieve the experiment ID
        let experimentDbId = req.params.id

        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Default values

        let parameters = {}
        parameters['includeData'] = false
        parameters['fields'] = null
        parameters['entryListDbId'] = null

        if (req.body != null) {
            // Retrieve parameters
            if (req.body.includeData != null) {
                if (req.body.includeData.toUpperCase() == 'TRUE') {
                    parameters['includeData'] = true
                }
            }

            if (req.body.entryListDbId != null) {
                parameters['entryListDbId'] = req.body.entryListDbId.replace(/(^\s*,)|(,\s*$)/g, '')
            }

            if (req.body.data != null) {
                parameters['data'] = req.body.data
            }

            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'includeData', 'data', 'fields'
            ]

            // Get filter condition
            conditionString = await processQueryHelper.getFilterString(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Get the filtered entry list IDs
        let entryListIdString = ''

        // Search by entry list ID
        if (parameters['entryListDbId'] != null) {
            let entryListIDs = parameters['entryListDbId'].split('|')
            // Parse the value
            for (entryListId of entryListIDs) {
                if (entryListIdString != '') {
                    entryListIdString = entryListIdString + ','
                }
                entryListIdString = entryListIdString + entryListId
            }
        }

        // Start building the query

        let entryListQuery = null

        if (parameters['fields'] != null) {
            entryListQuery = knex.column(parameters['fields'].split('|'))
        } else {
            let columnString = `
                "entryList".id AS "entryListDbId",
                "entryList".entry_class AS "entryClass",
                "entryList".entry_status AS "entryStatus",
                product.id AS "productDbId",
                "entryList".entlistno AS "entryListNo",
                "entryList".entry_type AS "entryType",
                author.id AS "authorDbId",
                author.display_name AS "author",
                "seedStorage".id AS "seedStorageDbId",
                "seedStorage".key_type AS "keyType",
                "seedStorage".label AS "seedStorageLabel",
                "seedStorage".seed_manager AS "seedManager",
                "seedStorage".unit AS "seedStorageVolumeUnit",
                "entryList".gid,
                "entryList".product_name AS "productName",
                "entryList".remarks,
                "entryList".creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.display_name AS creator,
                "entryList".modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.display_name AS modifier
            `

            let columns = knex.raw(columnString)

            entryListQuery = knex.select(columns)
        }

        // If client is asking for data
        if (parameters['includeData']) {
            // Check if the fields has entry list ID
            if (parameters['fields'] != null && !parameters['fields'].includes('entryList.id')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400029)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            // Add condition
            let addedConditionString = ` AND 
                "tableNameData".entry_list_id IN (
                    SELECT
                        id
                    FROM
                        operational.entry_list
                    WHERE
                        is_void = FALSE
                        AND experiment_id = ` + experimentDbId + `
                )`

            if (entryListIdString != '') {
                addedConditionString += ` AND "tableNameData".entry_list_id IN (` + entryListIdString + `)`
            }

            let variableConditionString = ``

            // Search by variable data abbreviation
            if (parameters['data'] != null) {

                let abbrevList = ''

                for (abbrev of parameters['data'].split('|')) {
                    abbrevList += "$$" + (abbrev.toUpperCase()).trim() + "$$,"
                }

                abbrevList = abbrevList.replace(/(^,)|(,$)/g, "")

                variableConditionString += ` AND variable.abbrev IN (` + abbrevList + `)`
            }

            if (variableConditionString != '') {
                addedConditionString += variableConditionString
            }

            // Check if there are data
            let variableList = await processQueryHelper.getVariableList('entry_list', 'entry_list_data', addedConditionString)

            if (variableList != null) {
                // Generate the query to retrieve the data and values
                for (variable of variableList) {
                    let metadataQuery = await processQueryHelper.getMetadataQuery('entry_list', 'entry_list_data', variable.abbrev)

                    entryListQuery += ',' + metadataQuery
                }
            }
        }

        entryListQuery += `
            FROM
                operational.entry_list "entryList"
            LEFT JOIN 
                master.product product ON product.id = "entryList".product_id
            LEFT JOIN 
                master.user author ON author.id =  "entryList".author_id::integer
            LEFT JOIN 
                operational.seed_storage_log AS "seedStorageLog" ON "seedStorageLog".id = "entryList".seed_storage_log_id
            LEFT JOIN 
                operational.seed_storage AS "seedStorage" ON "seedStorage".id = "seedStorageLog".seed_storage_id
            LEFT JOIN 
                master.user creator on creator.id = "entryList".creator_id
            LEFT JOIN 
                master.user modifier on modifier.id = "entryList".modifier_id 
            WHERE
                "entryList".is_void = FALSE
                AND "entryList".experiment_id = ` + experimentDbId

        // Add condition for the entry IDs
        if (entryListIdString != '') {
            entryListQuery += `
                AND "entryList".id IN (
                ` + entryListIdString + `
                )
            `
        }

        // Set the default order
        entryListQuery += ` ORDER BY "entryList".entlistno`

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        entryListFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            entryListQuery,
            conditionString,
            orderString
        )

        // Retrieve entry lists from the database   
        let entryLists = await sequelize.query(entryListFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await entryLists === undefined || await entryLists.length < 1) {
            res.send(200, {
              rows: entryLists,
              count: 0
            })
            return
        } else {
            // Get count 
            entryListCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(entryListQuery, conditionString, orderString)

            entryListCount = await sequelize.query(entryListCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = entryListCount[0].count
        }

        res.send(200, {
            rows: entryLists,
            count: count
        })
        return
    }
}