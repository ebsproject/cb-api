/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let experimentHelper = require('../../../../helpers/experiment/index.js')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')


module.exports = {


    // POST /v3/experiments/:id/entry-code-generations
    post: async function (req, res, next) {
        // Retrieve experiment ID
        let experimentDbId = req.params.id

        // Check if ID is an integer
        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction

        try {
            transaction = await sequelize.transaction({ autocommit: false })

            // Store the first key of the object
            let identifier = Object.keys(req.body)[0]

            if (identifier !== undefined) {
                if (
                    identifier !== 'offset' &&
                    !validator.isInt(identifier)
                ) {
                    await transaction.rollback()
                    let errMsg = await errorBuilder.getError(req.headers.host, 400240)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (identifier !== 'offset') {
                // Validate if key is integer
                if (!validator.isInt(identifier)) {
                    await transaction.rollback()
                    let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let entryCount = await experimentHelper.generateOccurrenceEntryCode(req.body, experimentDbId, res)

                if (typeof entryCount === 'boolean') {
                    await transaction.rollback()
                    return
                }

                resultArray = {
                    experimentDbId: experimentDbId,
                    recordCount: entryCount
                }

            } else {
                // Experiment Level

                let query = `
                    SELECT 
                    data_value::json
                    FROM experiment.experiment_data
                    WHERE
                        variable_id IN (SELECT id from master.variable WHERE abbrev = 'ENTRY_CODE_PATTERN')
                        AND is_void = FALSE
                        AND experiment_id = ${experimentDbId}
                    LIMIT 1
                `

                let experimentQuery = await sequelize.query(query, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (experimentQuery.length == 0) {
                    await transaction.rollback()
                    let errMsg = await errorBuilder.getError(req.headers.host, 400242)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let entryCount = await experimentHelper.generateExperimentEntryCode(req.body, experimentDbId, experimentQuery, res)

                if (typeof entryCount === 'boolean') {
                    await transaction.rollback()
                    return
                }

                resultArray = {
                    experimentDbId: experimentDbId,
                    recordCount: entryCount
                }
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })

            return

        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

}