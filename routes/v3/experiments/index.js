/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let userValidator = require('../../../helpers/person/validator.js')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let experimentHelper = require('../../../helpers/experiment/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let logger = require('../../../helpers/logger/index.js')
const endpoint = 'experiments'

module.exports = {

    // Implementation of GET call for /v3/experiments
    get: async (req, res, next) => {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''

        let orderString = ''

        let experimentIds = null

        let addedConditionString = ''

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req);

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let experimentsQuery = await experimentHelper.getExperimentQuerySql(addedConditionString)

        // Get filter condition for name, experimentCode, year, programDbId and seasonDbId only
        if (
            params.name !== undefined
            || params.experimentCode !== undefined
            || params.year !== undefined
            || params.programDbId !== undefined
            || params.seasonDbId !== undefined
        ) {
            let parameters = []

            if (params.name !== undefined) parameters['name'] = params.name
            if (params.experimentCode !== undefined) parameters['experimentCode'] = params.experimentCode
            if (params.year !== undefined) parameters['year'] = params.year
            if (params.programDbId !== undefined) parameters['programDbId'] = params.programDbId
            if (params.seasonDbId !== undefined) parameters['seasonDbId'] = params.seasonDbId

            conditionString = await processQueryHelper.getFilter(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        experimentFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            experimentsQuery,
            conditionString,
            orderString
        )

        // Retrieve experiments from the database   
        let experiments = await sequelize.query(experimentFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (experiments === undefined || experiments.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get count 
            experimentCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(experimentsQuery, conditionString, orderString)

            experimentCount = await sequelize.query(experimentCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = experimentCount[0].count
        }

        res.send(200, {
            rows: experiments,
            count: count
        })
        return
    },

    // Implementation of POST call for /v3/experiments
    post: async (req, res, next) => {

        // Set defaults
        let resultArray = []
        let recordCount = 0

        let experimentDbId = null
        let programDbId = null
        let pipelineDbId = null
        let stageDbId = null
        let projectDbId = null
        let experimentYear = null
        let seasonDbId = null
        let stewardDbId = null
        let experimentPlanDbId = null
        let dataProcessDbId = null
        let cropDbId = null

        let plantingSeason = null
        let experimentCode = null
        let experimentName = null
        let experimentObjective = null
        let experimentType = null
        let experimentSubType = null
        let experimentSubSubType = null
        let experimentDesignType = null
        let experimentStatus = null
        let description = null

        let experimentYearRegex = /^\d{4}$/

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let experimentUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/experiments'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        records = data.records

        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        recordCount = records.length

        try {
            let experimentValuesArray = []
            let visitedArray = []

            for (let record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Check if required columns are in the input
                if (
                    !record.programDbId ||
                    !record.stageDbId ||
                    !record.experimentYear ||
                    !record.experimentType ||
                    !record.seasonDbId ||
                    !record.experimentStatus ||
                    !record.stewardDbId ||
                    !record.cropDbId
                ) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400255)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */

                if (record.programDbId !== undefined) {
                    programDbId = record.programDbId

                    if (!validator.isInt(programDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400051)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate program ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if program is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.program program
                        WHERE 
                            program.is_void = FALSE AND
                            program.id = ${programDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.stageDbId !== undefined) {
                    stageDbId = record.stageDbId

                    if (!validator.isInt(stageDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400251)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate stage ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if stage is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.stage stage
                            WHERE 
                                stage.is_void = FALSE AND
                                stage.id = ${stageDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.experimentYear !== undefined) {
                    experimentYear = record.experimentYear

                    if (!validator.isInt(experimentYear)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400252)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (!experimentYear.match(experimentYearRegex)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400254)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.seasonDbId !== undefined) {
                    seasonDbId = record.seasonDbId

                    if (!validator.isInt(seasonDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400063)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate season ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if season is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.season season
                            WHERE 
                                season.is_void = FALSE AND
                                season.id = ${seasonDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.stewardDbId !== undefined) {
                    stewardDbId = record.stewardDbId

                    if (!validator.isInt(stewardDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400253)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate steward ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if steward is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.person steward
                            WHERE 
                                steward.is_void = FALSE AND
                                steward.id = ${stewardDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.cropDbId !== undefined) {
                    cropDbId = record.cropDbId

                    if (!validator.isInt(cropDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400182)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate crop ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if crop is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.crop crop
                            WHERE 
                                crop.is_void = FALSE AND
                                crop.id = ${cropDbId}
                        )
                    `
                    validateCount += 1
                }

                experimentObjective = record.experimentObjective
                experimentType = record.experimentType
                experimentStatus = record.experimentStatus
                plantingSeason = record.plantingSeason

                /** Validation of non-required parameters and set values */

                if (record.pipelineDbId !== undefined) {
                    pipelineDbId = record.pipelineDbId

                    if (!validator.isInt(pipelineDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400250)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate pipeline ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if pipeline is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.pipeline pipeline
                            WHERE 
                                pipeline.is_void = FALSE AND
                                pipeline.id = ${pipelineDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.experimentPlanDbId !== undefined) {
                    experimentPlanDbId = record.experimentPlanDbId

                    if (!validator.isInt(experimentPlanDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400256)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate experiment plan ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment plan is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.experiment_plan experimentPlan
                            WHERE 
                                experimentPlan.is_void = FALSE AND
                                experimentPlan.id = ${experimentPlanDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.projectDbId !== undefined) {
                    projectDbId = record.projectDbId

                    if (!validator.isInt(projectDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400056)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate project ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if project is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.project project
                            WHERE 
                                project.is_void = FALSE AND
                                project.id = ${projectDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.dataProcessDbId !== undefined) {
                    dataProcessDbId = record.dataProcessDbId

                    if (!validator.isInt(dataProcessDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400257)
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate data process ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment plan is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.item item
                            WHERE 
                                item.id = ${dataProcessDbId}
                        )
                    `
                    validateCount += 1
                }

                experimentSubType = (record.experimentSubType !== undefined) ?
                    record.experimentSubType : null

                experimentSubSubType = (record.experimentSubSubType !== undefined) ?
                    record.experimentSubSubType : null

                experimentDesignType = (record.experimentDesignType !== undefined) ?
                    record.experimentDesignType : null

                description = (record.description !== undefined) ?
                    record.description : null

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Generation of experiment code = EXP + (last experiment ID + 1) */
                let experimentInfoQuery = `
                    SELECT 
                        CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE MAX(experiment.id)+1 
                        END AS "code",
                        count(*) AS "experimentCount"
                    FROM
                        experiment.experiment experiment
                    WHERE
                        experiment.is_void = FALSE
                `

                let experimentInfo = await sequelize.query(experimentInfoQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                let strCode

                if (experimentInfo[0].experimentCount == 0) {
                    strCode = 1
                } else {
                    strCode = parseInt(experimentInfo[0].code)

                    if (records.indexOf(record) > 0) {
                        strCode = visitedArray[visitedArray.length - 1] + 1
                    }
                }

                // Check duplicates if recordCount > 1
                if (recordCount > 1) {
                    if (visitedArray.includes(strCode)) {
                        strCode += 1
                    } else {
                        visitedArray.push(strCode)
                    }
                }

                strCode = strCode.toString()
                strCode = (strCode.length < 6) ? strCode.padStart(7, '0') : strCode
                experimentCode = 'EXP' + strCode

                // if experimentName is not specified, generate experimentName
                if (record.experimentName == undefined) {
                    const pattern = await experimentHelper.getPattern("EXPERIMENT_NAME_CONFIG")
                    experimentName = await experimentHelper.generateCode(pattern, record.programDbId, record.stageDbId, record.experimentYear, record.seasonDbId)
                } else {
                    experimentName = record.experimentName
                }
                // Get values 
                let tempArray = [
                    programDbId, pipelineDbId, stageDbId, experimentYear, experimentName,
                    experimentCode, experimentObjective, experimentType, seasonDbId, plantingSeason,
                    experimentStatus, stewardDbId, experimentPlanDbId, projectDbId, dataProcessDbId,
                    experimentSubType, experimentSubSubType, experimentDesignType, description,
                    personDbId, cropDbId
                ]

                experimentValuesArray.push(tempArray)
            }

            // Create experiment record
            let experimentsQuery = format(`
                INSERT INTO
                    experiment.experiment (
                        program_id, pipeline_id, stage_id, experiment_year, experiment_name, 
                        experiment_code, experiment_objective, experiment_type, season_id, 
                        planting_season, experiment_status, steward_id, experiment_plan_id, project_id,
                        data_process_id, experiment_sub_type, experiment_sub_sub_type, 
                        experiment_design_type, description, creator_id, crop_id
                    )
                VALUES
                    %L
                RETURNING id`, experimentValuesArray
            )

            const experiments = await sequelize.transaction(async transaction => {
                return await sequelize.query(experimentsQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (const experiment of experiments[0]) {
                experimentDbId = experiment.id

                let experimentRecord = {
                    experimentDbId: experimentDbId,
                    recordCount: 1,
                    href: experimentUrlString + '/' + experimentDbId
                }

                resultArray.push(experimentRecord)
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}