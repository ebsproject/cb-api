/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY "crossParent".id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn',
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray,
        )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }
    // Build the base retrieval query
    let crossParentQuery = null
    // Check if the client specified values for fields
    if (parameters['fields']) {
      crossParentQuery = knex.column(parameters['fields'].split('|'))
      crossParentQuery += `
        FROM
          germplasm.cross_parent "crossParent"
        LEFT JOIN
          germplasm.germplasm germplasm ON "crossParent".germplasm_id = germplasm.id
        LEFT JOIN
          germplasm.cross crs ON "crossParent".cross_id = crs.id
        LEFT JOIN
          germplasm.seed ON "crossParent".seed_id = seed.id
        LEFT JOIN
          experiment.experiment experiment ON "crossParent".experiment_id = experiment.id
        LEFT JOIN
          experiment.entry entry ON "crossParent".entry_id = entry.id
        LEFT JOIN
          tenant.person creator ON "crossParent".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "crossParent".modifier_id = modifier.id
        LEFT JOIN
          experiment.plot plot ON "crossParent".plot_id = plot.id
        LEFT JOIN
          experiment.occurrence occ ON "crossParent".occurrence_id = occ.id
        WHERE
          "crossParent".is_void = FALSE
        ${addedOrderString}
      `
    } else {
      crossParentQuery = `
        SELECT
          "crossParent".id AS "crossParentDbId",
          "crossParent".parent_role AS "crossParentRole",
          "crossParent".order_number AS "crossParentOrderNumber",
          crs.id AS "crossDbId",
          crs.cross_name AS "crossName",
          crs.cross_method AS "crossMethod",
          seed.seed_code AS "seedCode",
          seed.seed_name AS "seedName",
          germplasm.id AS "germplasmDbId",
          germplasm.designation AS "germplasmDesignation",
          germplasm.parentage AS "germplasmParentage",
          germplasm.generation AS "germplasmGeneration",
          entry.id AS "entryDbId",
          entry.entry_number AS "entryNo",
          entry.entry_name AS "entryName",
          entry.entry_role AS "entryRole",
          experiment.id AS "experimentDbId",
          experiment.data_process_id AS "experimentDataProcessId",
          (
            SELECT
              item.display_name
            FROM
              master.item item
            WHERE
              item.is_void = FALSE AND
              item.id = experiment.data_process_id
          ) AS "experimentTemplate",
          plot.id AS "plotDbId",
          occ.id AS "occurrenceDbId",
          "crossParent".creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          "crossParent".modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          germplasm.cross_parent "crossParent"
        LEFT JOIN
          germplasm.germplasm germplasm ON "crossParent".germplasm_id = germplasm.id
        LEFT JOIN
          germplasm.cross crs ON "crossParent".cross_id = crs.id
        LEFT JOIN
          germplasm.seed ON "crossParent".seed_id = seed.id
        LEFT JOIN
          experiment.experiment experiment ON "crossParent".experiment_id = experiment.id
        LEFT JOIN
          experiment.entry entry ON "crossParent".entry_id = entry.id
        LEFT JOIN
          tenant.person creator ON "crossParent".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "crossParent".modifier_id = modifier.id
        LEFT JOIN
          experiment.plot plot ON "crossParent".plot_id = plot.id
        LEFT JOIN
          experiment.occurrence occ ON "crossParent".occurrence_id = occ.id
        WHERE
          "crossParent".is_void = FALSE
        ${addedOrderString}
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    let crossParentFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        crossParentQuery,
        conditionString,
        orderString,
        addedDistinctString,
      )

    let crossParent = await sequelize
      .query(crossParentFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (crossParent == undefined || crossParent.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let crossParentCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        crossParentQuery,
        conditionString,
        orderString,
        addedDistinctString,
      )

    let crossParentCount = await sequelize
      .query(crossParentCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
    
    count = crossParentCount[0].count

    res.send(200, {
      rows: crossParent,
      count: count
    })
    return
  }
}