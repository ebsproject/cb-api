/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let logger = require('../../../../helpers/logger/index.js')
const endpoint = 'transaction-datasets/:id'

module.exports = {

    /**
     * Update records in data_terminal.transaction_dataset
     * PUT v3/transaction-datasets/:id
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    put: async function (req, res) {

        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let datasetDbId = req.params.id
        let newRecord = req.body
        let setQuery = []

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let transactionDatasetUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/transaction-datasets/'
            + datasetDbId

        // check if datasetDbId is integer
        if (!validator.isInt(datasetDbId)) {
            let errMsg = 'Invalid format, transaction dataset ID must be an integer.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction dataset ID exists
        let transactionQuery = `
            SELECT 
                is_suppressed,
                is_void
            FROM 
                data_terminal.transaction_dataset
            WHERE 
                id = ${datasetDbId}
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await transactionRecord == undefined || await transactionRecord.length == 0) {
            let errMsg = 'The transaction dataset record you have requested does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        let isSuppressed = transactionRecord[0]['is_suppressed'];
        let isVoid = transactionRecord[0]['is_void'];

        if (newRecord.remarks !== undefined) {
            setQuery.push(`remarks = '${newRecord.remarks}'`)
        }

        // check if isSuppressed is valid
        if (newRecord.isSuppressed !== undefined && !validator.isBoolean(newRecord.isSuppressed)) {

            let errMsg = 'Invalid format, isSuppressed must be boolean.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else if (newRecord.isSuppressed !== undefined && isSuppressed.toString() != newRecord.isSuppressed) {
            setQuery.push(`is_suppressed = ${newRecord.isSuppressed}`)
            
            setQuery.push(`suppress_remarks = '${newRecord.suppressRemarks}'`)
        }

        // check if isVoid is valid
        if (newRecord.isVoid !== undefined && !validator.isBoolean(newRecord.isVoid)) {

            let errMsg = 'Invalid format, isVoid must be boolean.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else if (newRecord.isVoid !== undefined && isVoid.toString() != newRecord.isVoid) {
            setQuery.push(`is_void = ${newRecord.isVoid}`)
        }

        try {
            await sequelize.transaction(async transaction => {
                setQuery = setQuery.toString()
                if (setQuery.length > 0) {
                    let updateListQuery = `
                        UPDATE
                            data_terminal.transaction_dataset
                        SET
                            ${setQuery},
                            modification_timestamp = NOW(),
                            modifier_id = ${userDbId}
                        WHERE
                            id = ${datasetDbId}
                    `
                    updateListQuery = updateListQuery.trim()

                    await sequelize.query(updateListQuery, {
                        type: sequelize.QueryTypes.UPDATE,
                        transaction: transaction,
                        raw: true
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'UPDATE', err)
                        throw new Error(err)
                    })
                    recordCount = 1

                } else {

                    recordCount = 0
                }
            })

            res.send(200, {
                rows: {
                    datasetDbId: datasetDbId,
                    recordCount: recordCount,
                    href: transactionDatasetUrl
                }
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },
    /**
     * Deletes a transaction dataset record
     * DELETE v3/transaction-datasets/:id
     * @param {*} req 
     * @param {*} res 
     */
    delete: async function (req, res) {
        let datasetDbId = req.params.id

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let transactionDatasetUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/transaction-datasets/'
            + datasetDbId

        // check if datasetDbId is integer
        if (!validator.isInt(datasetDbId)) {
            let errMsg = 'Invalid format, transaction dataset ID must be an integer.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction dataset ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction_dataset 
                WHERE 
                    id=${datasetDbId}
                    AND is_void = FALSE
            )
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (!transactionRecord[0]['exists']) {
            let errMsg = 'The transaction dataset record you have requested does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        try {
            let resultArray
            await sequelize.transaction(async transaction => {
                let deleteQuery = `
                    DELETE FROM
                        data_terminal.transaction_dataset
                    WHERE
                        id = ${datasetDbId}
                `
                await sequelize.query(deleteQuery, {
                    type: sequelize.QueryTypes.DELETE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })

                resultArray = {
                    datasetDbId: datasetDbId,
                    recordCount: 1,
                    href: transactionDatasetUrl
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}