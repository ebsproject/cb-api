/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger/index')

module.exports = {
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}
        let endpoint = 'items-search'

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields']
            // Get filter condition
            conditionString = await processQueryHelper.getFilter(
                    req.body,
                    excludedParametersArray
                )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Build the base retrieval query
        let itemsQuery = null
        // Check if the client specified values for the field/s
        if (parameters['fields']) {
            itemsQuery = knex.column(parameters['fields'].split('|'))
            itemsQuery += `
                FROM
                    master.item item
                LEFT JOIN
                    tenant.person creator ON item.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON item.modifier_id = modifier.id
                LEFT JOIN
                    master.entity entity ON item.subject_entity_id = entity.id
                WHERE
                    item.is_void = FALSE
            ` + addedConditionString +
            `
                ORDER BY
                    item.id
            `
        } else {
            itemsQuery = `
                SELECT
                    item.id AS "itemDbId",
                    item.abbrev,
                    item.name,
                    item.type,
                    item.process_type AS "processType",
                    item.description,
                    item.display_name AS "displayName",
                    item.remarks,
                    item.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    item.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier",
                    item.notes,
                    item.item_url AS "itemUrl",
                    item.item_status AS "itemStatus",
                    item.item_class AS "itemClass",
                    item.subject_entity_id AS "subjectEntityDbId",
                    item.item_icon AS "itemIcon",
                    item.item_group AS "itemGroup",
                    item.item_usage AS "itemUsage",
                    item.item_qr_code AS "itemQrCode"
                FROM
                    master.item item
                LEFT JOIN
                    tenant.person creator ON item.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON item.modifier_id = modifier.id
                WHERE
                    item.is_void = FALSE
            ` + addedConditionString +
            `
                ORDER BY
                    item.id
            `
        }
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Generate the final SQL query
        itemsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            itemsQuery,
            conditionString,
            orderString
        )
        // Retrieve the place records
        let items = await sequelize.query(itemsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                // Log error when query fails
                await logger.logFailingQuery(endpoint, 'SELECT', err)
            })

        // Return error when query fails
        if(await items === undefined){
            errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // If query returns empty, return empty values
        if (await items.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get the final count of the place records
        let itemsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            itemsQuery,
            conditionString,
            orderString
        )

        let itemsCount = await sequelize.query(itemsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        count = await itemsCount[0].count
        
        res.send(200, {
            rows: items,
            count: count
        })
    }
}