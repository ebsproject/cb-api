/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let logger = require('../../../../helpers/logger')
const endpoint = 'occurrence-data/:id'

module.exports = {
  // Implementation of DELETE call for /v3/occurrence-data/:id
  delete: async function (req, res, next) {
  // Set defaults
    let occurrenceDataDbId = req.params.id
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    if (!validator.isInt(occurrenceDataDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400063)
      res.send(new errors.BadRequestError(errMsg))
      return
    } else {

      try {
        // Validate the occurrence data
        // Check if occurrence data is existing
        // Build query
        let occurrenceDataQuery = `
          SELECT
            count(1)
          FROM 
            experiment.occurrence_data
          WHERE
            is_void = FALSE AND
            id = ${occurrenceDataDbId}
        `

        // Retrieve occurrence datas from the database   
        let hasOccurrenceData = await sequelize.query(occurrenceDataQuery, {
          type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

        // Check if occurrence data is non-existent in database
        if (parseInt(hasOccurrenceData[0].count) === 0) {
          let errMsg = `The occurrence data you have requested does not exist.`
          res.send(new errors.NotFoundError(errMsg))
          return
        }

        let transaction = await sequelize.transaction(async transaction => {
            // Build query for voiding
            let deleteOccurrenceDataQuery = `
                UPDATE
                    experiment.occurrence_data
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    id = ${occurrenceDataDbId}
            `

            await sequelize.query(deleteOccurrenceDataQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction,
                raw: true
            }).catch(async err => {
                logger.logFailingQuery(endpoint, 'UPDATE', err)
                throw new Error(err)
            })
        })

        res.send(200, {
          rows: {
            occurrenceDataDbId: occurrenceDataDbId
          }
        })
        return
      } catch (err) {
        logger.logMessage(__filename, err.stack, 'error')
        let errMsg = await errorBuilder.getError(req.headers.host, 500002)
        if(!res.headersSent) res.send(new errors.InternalError(errMsg))
        return
      }
    }
  },

    // Implementation of PUT call for /v3/occurrence-data/:id
    put: async function (req, res, next) {
        // Retrieve occurrence data ID
        let occurrenceDataDbId = req.params.id
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!validator.isInt(occurrenceDataDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400063)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let dataValue = null
        let dataQcCode = null
        let protocolDbId = null

        // Build query
        let occurrenceDataQuery = `
            SELECT
                data_value AS "dataValue",
                data_qc_code AS "dataQcCode",
                occurrence_id AS "occurrenceDbId",
                protocol_id AS "protocolDbId",
                creator_id AS "creatorDbId"
            FROM 
                experiment.occurrence_data
            WHERE
                is_void = FALSE AND
                id = ${occurrenceDataDbId}
        `

        // Retrieve occurrence data record from database
        let occurrenceData = await sequelize.query(occurrenceDataQuery,{
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (
            await occurrenceData == undefined ||
            await occurrenceData.length < 1
        ) {
            let errMsg = `The occurrence data record you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordDataValue = occurrenceData[0].dataValue
        let recordDataQcCode = occurrenceData[0].dataQcCode
        let recordOccurrenceDbId = occurrenceData[0].occurrenceDbId
        let recordProtocolDbId = occurrenceData[0].protocolDbId
        let recordCreatorDbId = occurrenceData[0].creatorDbId

        let isSecure = forwarded(req, req.header).secure

        // Set URL for response
        let occurrenceDataUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/occurrence-data/'
            + occurrenceDataDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let transaction

        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            if (data.dataValue !== undefined) {
                dataValue = data.dataValue

                // Check if user input is same with the current value in the database
                if (dataValue != recordDataValue) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    data_value = $$${dataValue}$$
                    `
                }
            }

            if (data.dataQcCode !== undefined) {
                dataQcCode = data.dataQcCode

                // Check if user input is same with the current value in the database
                if (dataQcCode != recordDataQcCode) {

                let validDataQcCodes = ['N', 'G', 'Q', 'S', 'M', 'B']

                // Validate data QC code
                if (!validDataQcCodes.includes(dataQcCode.toUpperCase())) {
                    let errMsg = `Invalid request, you have provided an invalid value for data qc code.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                data_qc_code = $$${dataQcCode}$$
                `
                }
            }

            if (data.protocolDbId !== undefined) {
                protocolDbId = data.protocolDbId

                if (!validator.isInt(protocolDbId)) {
                    let errMsg = 'Invalid request, protocolDbId must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the user input is the same with the current value
                if (protocolDbId != recordProtocolDbId) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        -- Check if protocol ID exists, return 1
                        SELECT
                            count(1)
                        FROM
                            tenant.protocol p
                        WHERE
                            p.is_void = FALSE AND
                            p.id = ${protocolDbId}
                        )
                    `

                    validateCount += 1
                    

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        protocol_id = $$${protocolDbId}$$
                    `
                }
            }

            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS
                        count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if(setQuery.length > 0) setQuery += `,`

            let resultArray
            transaction = await sequelize.transaction(async transaction => {
                let updateOccurrenceDataQuery = `
                    UPDATE
                        experiment.occurrence_data
                    SET
                        ${setQuery}
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE
                        id = ${occurrenceDataDbId}
                `

                // Return transaction info
                resultArray = {
                    occurrenceDataDbId: occurrenceDataDbId,
                    recordCount: 1,
                    href: occurrenceDataUrlString
                }

                await sequelize.query(updateOccurrenceDataQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}