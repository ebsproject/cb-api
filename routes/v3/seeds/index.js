/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize, Seasons } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let responseHelper = require('../../../helpers/responses')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token.js')
let userValidator = require('../../../helpers/person/validator.js')
let format = require('pg-format')
let forwarded = require('forwarded-for')
let validator = require('validator')

const logger = require('../../../helpers/logger/index.js')
const endpoint = 'seeds'

module.exports = {

	// Implementation of POST call for /v3/seeds
	post: async function (req, res, next) {
		let creatorId = await tokenHelper.getUserId(req, res)

		// If personDbId is undefined, terminate
		if (creatorId === undefined) {
			return
		}

		if (creatorId == null) {
			let errMsg = await errorBuilder.getError(req.headers.host, 401002)
			res.send(new errors.BadRequestError(errMsg))
			return
		}

		// Check if the connection is secure or not (http or https)
		let isSecure = forwarded(req, req.headers).isSecure
		// Set URL for response
		let seedsUrlString = (isSecure ? 'https' : 'http')
			+ '://'
			+ req.headers.host
			+ '/v3/seeds'

		// Check if the request body does not exist
		if (!req.body) {
			let errMsg = await errorBuilder.getError(req.headers.host, 400009)
			res.send(new errors.BadRequestError(errMsg))
			return
		}

		let data = req.body
		let records = []


		try {
			// Records = [{}...]
			records = data.records

			// Check if the input data is empty
			if (records == undefined || records.length == 0) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400005)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		} catch (e) {
			logger.logMessage(endpoint, e.stack, 'error')

			let errMsg = await errorBuilder.getError(req.headers.host, 500004)
			res.send(new errors.InternalError(errMsg))
			return
		}

		// Start transaction
		let transaction

		try {
			// Get transaction
			transaction = await sequelize.transaction({ autocommit: false })

			let seedsValuesArray = []

			for (var record of records) {

				let validateQuery = ``
				let validateCount = 0

				let seedCode = null
				let seedName = null
				let germplasmDbId = null
				let harvestDate = null
				let harvestMethod = null
				let programDbId = null
				let sourceExperimentDbId = null
				let sourceEntryDbId = null
				let sourcePlotDbId = null
				let sourceOccurrenceDbId = null
				let sourceLocationDbId = null
				let description = null
				let notes = null

				// Check if the required colums are in the input
				if (record.germplasmDbId === undefined) {
					let errMsg = `Required parameter germplasmDbId is missing. Ensure that germplasmDbId field is not empty.`
					res.send(new errors.BadRequestError(errMsg))
					return
				}

				// validate germplasmDbId 
				if (record.germplasmDbId !== undefined) {
					germplasmDbId = record.germplasmDbId

					if (!validator.isInt(germplasmDbId)) {
						let errMsg = await errorBuilder.getError(req.headers.host, 400238)
						res.send(new errors.BadRequestError(errMsg))
						return
					}

					/* Validate and set values */
					// validate if the germplasmDbId is in the database
					let germplasmQuery = `
						SELECT id
						FROM
							germplasm.germplasm
						WHERE id=${record.germplasmDbId} and is_void = false
					`

					let germplasmArray = await sequelize.query(germplasmQuery, {
						type: sequelize.QueryTypes.SELECT
					})

					if (await germplasmArray == undefined || await germplasmArray.length == 0) {
						let errMsg = 'The provided germplasmDbId does not exist.'
						let errorCode = await responseHelper.getErrorCodebyMessage(
							400,
							errMsg)
						errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
						res.send(new errors.BadRequestError(errMsg))
						return
					}
				}

				// validate seedCode 
				if (record.seedCode !== undefined) {
					seedCode = record.seedCode

					// Validate if the seedCode has duplicate in the database
					let seedsQuery = `
						SELECT seed_code
						FROM
							germplasm.seed
						WHERE seed_code='${seedCode}' and is_void = false
					`

					let seedArray = await sequelize.query(seedsQuery, {
						type: sequelize.QueryTypes.SELECT
					})

					if (await seedArray == undefined || await seedArray.length > 0) {
						let errMsg = 'The provided seedCode has duplicates.'
						let errorCode = await responseHelper.getErrorCodebyMessage(
							400,
							errMsg)
						errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
						res.send(new errors.BadRequestError(errMsg))
						return
					}
				} else {
					let generateSeedCodeQuery = `
						SELECT
							germplasm.generate_code('seed')
						AS seedcode
					`

					let seed = await sequelize.query(generateSeedCodeQuery, {
						type: sequelize.QueryTypes.SELECT
					})

					if (seed == undefined || seed.length == 0) {
						let errMsg = await errorBuilder.getError(req.headers.host, 500004)
						res.send(new errors.BadRequestError(errMsg))
						return
					}

					seedCode = seed[0].seedcode
				}

				if (record.programDbId !== undefined && record.programDbId !== null) {
					programDbId = record.programDbId

					if (!validator.isInt(programDbId)) {
						let errMsg = `Invalid format, program ID must be an integer.`
						res.send(new errors.BadRequestError(errMsg))
						return
					}
					/* Validate and set values */
					// validate if the germplasmDbId is in the database
					let programQuery = `
						SELECT id
						FROM
							tenant.program
						WHERE id=${programDbId} and is_void = false
					`

					let programArray = await sequelize.query(programQuery, {
						type: sequelize.QueryTypes.SELECT
					})

					if (await programArray == undefined || await programArray.length == 0) {
						let errMsg = 'The provided programDbId does not exist.'
						res.send(new errors.BadRequestError(errMsg))
						return
					}


				}

				if (record.sourceExperimentDbId !== undefined) {
					sourceExperimentDbId = record.sourceExperimentDbId

					if (!validator.isInt(sourceExperimentDbId)) {
						let errMsg = `Invalid format, Source Experiment ID must be an integer.`
						res.send(new errors.BadRequestError(errMsg))
						return
					}
				}

				if (record.sourceEntryDbId !== undefined) {
					sourceEntryDbId = record.sourceEntryDbId

					if (!validator.isInt(sourceEntryDbId)) {
						let errMsg = `Invalid format, Source Entry ID must be an integer.`
						res.send(new errors.BadRequestError(errMsg))
						return
					}

					// Validate Entry ID
					validateQuery += (validateQuery != '') ? ' + ' : ''
					validateQuery += `
						(
						--- Check if entry id is existing return 1
						SELECT 
						count(1)
						FROM 
						experiment.entry entry
						WHERE 
						entry.is_void = FALSE AND
						entry.id = ${sourceEntryDbId}
						)
					`
					validateCount += 1
				}

				if (record.sourcePlotDbId !== undefined) {
					sourcePlotDbId = record.sourcePlotDbId

					if (!validator.isInt(sourcePlotDbId)) {
						let errMsg = `Invalid format, Source Plot ID must be an integer.`
						res.send(new errors.BadRequestError(errMsg))
						return
					}
					// Validate Plot ID
					validateQuery += (validateQuery != '') ? ' + ' : ''
					validateQuery += `
						(
						--- Check if plot id is existing return 1
						SELECT 
						count(1)
						FROM 
						experiment.plot plot
						WHERE 
						plot.is_void = FALSE AND
						plot.id = ${sourcePlotDbId}
						)
					`
					validateCount += 1

				}


				if (record.sourceOccurrenceDbId !== undefined) {
					sourceOccurrenceDbId = record.sourceOccurrenceDbId

					if (!validator.isInt(sourceOccurrenceDbId)) {
						let errMsg = `Invalid format, Source occurrence ID must be an integer.`
						res.send(new errors.BadRequestError(errMsg))
						return
					}

					// Validate Occurrence ID
					validateQuery += (validateQuery != '') ? ' + ' : ''
					validateQuery += `
						(
						--- Check if Occurrence id is existing return 1
						SELECT 
						count(1)
						FROM 
						experiment.occurrence occurrence
						WHERE 
						occurrence.is_void = FALSE AND
						occurrence.id = ${sourceOccurrenceDbId}
						)
					`
					validateCount += 1
				}

				if (record.sourceLocationDbId !== undefined) {
					sourceLocationDbId = record.sourceLocationDbId

					if (!validator.isInt(sourceLocationDbId)) {
						let errMsg = `Invalid format, Source Location ID must be an integer.`
						res.send(new errors.BadRequestError(errMsg))
						return
					}
					// Validate Location ID
					validateQuery += (validateQuery != '') ? ' + ' : ''
					validateQuery += `
						(
						--- Check if Location id is existing return 1
						SELECT 
						count(1)
						FROM 
						experiment.location location
						WHERE 
						location.is_void = FALSE AND
						location.id = ${sourceLocationDbId}
						)
					`
					validateCount += 1

				}

				if (record.harvestDate !== undefined) {
					harvestDate = record.harvestDate
					// validate
					var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
					if (record.harvestDate !== null && !harvestDate.match(validDateFormat)) {  // Invalid format
						let errMsg = 'Invalid format, harvest date must be in format YYYY-MM-DD.'
						let errorCode = await responseHelper.getErrorCodebyMessage(
							400,
							errMsg)
						errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
						res.send(new errors.BadRequestError(errMsg))
						return
					}

				}

				//  fields to set value
				if (record.seedName !== undefined) {
					seedName = record.seedName
				} else {
					let generateSeedNameQuery = `
						SELECT
							nextval('germplasm.gid_seq')
						AS seedname

					`

					let seed = await sequelize.query(generateSeedNameQuery, {
						type: sequelize.QueryTypes.SELECT
					})

					if (await seed == undefined || await seed.length == 0) {
						let errMsg = await errorBuilder.getError(req.headers.host, 500004)
						res.send(new errors.BadRequestError(errMsg))
						return
					}

					seedName = seed[0].seedname
				}
				if (record.harvestMethod !== undefined) harvestMethod = record.harvestMethod
				if (record.description !== undefined) description = record.description
				if (record.notes !== undefined) notes = record.notes

				if (validateCount > 0) {
					// count must be equal to validateCount if all conditions are met
					let checkInputQuery = `
						SELECT
						${validateQuery}
						AS count
					`
					let inputCount = await sequelize.query(checkInputQuery, {
						type: sequelize.QueryTypes.SELECT
					})

					if (inputCount[0].count != validateCount) {
						let errMsg = await errorBuilder.getError(req.headers.host, 400015)
						res.send(new errors.BadRequestError(errMsg))
						return
					}
				}

				// Get values
				let tempArray = [
					seedCode, seedName, harvestDate, harvestMethod,
					germplasmDbId, programDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId,
					sourceOccurrenceDbId, sourceLocationDbId, description, creatorId, notes
				]

				seedsValuesArray.push(tempArray)
			}
			// Create seed record 
			let insertSeedsQuery = format(`
				INSERT INTO
					germplasm.seed (
					seed_code, seed_name, harvest_date,harvest_method,
					germplasm_id,program_id,source_experiment_id,
					source_entry_id,source_plot_id,source_occurrence_id,source_location_id,
					description,creator_id,notes) 
				VALUES 
				%L
				RETURNING id`, seedsValuesArray
			)

			let seeds = await sequelize.query(insertSeedsQuery, {
				type: sequelize.QueryTypes.INSERT,
				transaction: transaction
			}).catch(async error => {
				logger.logFailingQuery(endpoint, 'INSERT', error)
				throw new Error(error)
			})

			let resultArray = []

			for (let seed of seeds[0]) {
				let seedDbId = seed.id

				// Return the seed info to Client
				let array = {
					seedDbId: seedDbId,
					recordCount: 1,
					href: seedsUrlString + '/' + seedDbId
				}

				resultArray.push(array)
			}

			await transaction.commit()

			res.send(200, {
				rows: resultArray,
				count: resultArray.length
			})
			return
		} catch (err) {

			await logger.logMessage(endpoint, err.stack, 'error')

			// Rollback transaction if any errors were encountered
			if (err) await transaction.rollback()
			let errMsg = await errorBuilder.getError(req.headers.host, 500001)
			res.send(new errors.InternalError(errMsg))
			return
		}
	}
}
