/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let tokenHelper = require('../../../../helpers/auth/token')
const logger = require('../../../../helpers/logger/index.js')
const endpoint = 'seeds/:id'

module.exports = {

    // Endpoint for retrieving a seeds
    // GET call /v3/seeds/:id
    get: async function (req, res, next) {
        // Retrieve the seed ID
        let seedDbId = req.params.id

        if (!validator.isInt(seedDbId)) {
            let errMsg = `Invalid format, seed ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        } 

        let seedsQuery = `
            SELECT
                seed.id as "seedDbId",
                seed.seed_code as "seedCode",
                seed.seed_name as "seedName",
                seed.harvest_date as "harvestDate",
                seed.harvest_method as "harvestMethod",
                seed.germplasm_id as "germplasmDbId",
                seed.program_id as "programDbId",
                seed.source_experiment_id as "experimentDbId",
                seed.source_entry_id as "entryDbId",
                seed.source_occurrence_id as "occurrenceDbId",
                seed.source_location_id as "locationDbId",
                seed.source_plot_id as "plotDbId",
                seed.description as "description",
                seed.notes as "notes",
                seed.creation_timestamp as "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name as creator,
                seed.modification_timestamp as "modificationTimestamp",
                modifier.id as "modifierDbId",
                modifier.person_name as modifier
            FROM
                germplasm.seed seed
            LEFT JOIN
                tenant.person creator ON seed.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON seed.modifier_id = modifier.id
            WHERE
                seed.is_void = FALSE AND
                seed.id = (:seedDbId)
            `

            // Retrieve seed from the database
            let seeds = await sequelize.query(seedsQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    seedDbId: seedDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
    
            //Return error if resource is not found
            if (await seeds == undefined || await seeds.length < 1) {
                let errMsg = `The seed you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: seeds
            })
            return
    },

    // Endpoint for updating a seed record
    // PUT call /v3/seeds/:id
    put: async function (req, res, next) {

        let seedDbId = req.params.id

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let seedUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/seeds/'
            + seedDbId

        if (!validator.isInt(seedDbId)) {
            let errMsg = `Invalid format, seed ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let seedCode = null 
        let seedName= null 
        let germplasmDbId = null 
        let harvestDate = null 
        let harvestMethod = null 
        let programDbId = null
        let sourceExperimentDbId = null
        let sourceEntryDbId = null
        let sourcePlotDbId = null
        let sourceOccurrenceDdId = null
        let sourceLocationDbId = null
        let description =    null
        let notes = null

        let validateSeedQuery = `
            SELECT 
                count(1)
            FROM
                germplasm.seed seed
            WHERE
                seed.is_void = FALSE AND
                seed.id = '${seedDbId}'
            `
        let validateSeed = await sequelize.query(validateSeedQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (validateSeed[0]['count'] != 1) {
            let errMsg = `The seed you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let userDbId = await tokenHelper.getUserId(req, res)

        // If userDbId is undefined, terminate
        if(userDbId === undefined){
            return
        }

        if (userDbId == null) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401002)
                res.send(new errors.BadRequestError(errMsg))
                return
        }

        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0
            
            if (data.seedCode !== undefined) {
                seedCode=data.seedCode
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                seed_code = $$${seedCode}$$`
            }

            if (data.seedName !== undefined) {
                seedName=data.seedName
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                seed_name = $$${seedName}$$`
            } 

            if (data.harvestDate !== undefined) {
                harvestDate=data.harvestDate

                var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
                if (harvestDate !== "null" && !harvestDate.match(validDateFormat)) {    // Invalid format
                    let errMsg = 'Invalid format, harvest date must be in format YYYY-MM-DD.'
                    errorCode = await responseHelper.getErrorCodebyMessage(400,errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input null
                if (harvestDate === "null") {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        harvest_date = NULL
                    `
                }
                else {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    harvest_date = $$${harvestDate}$$`
                }
            } 

            if (data.harvestMethod !== undefined) {
                harvestMethod=data.harvestMethod
                // Check if user input null
                if (harvestMethod === "null") {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        harvest_method = NULL
                    `
                }
                else {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    harvest_method = $$${harvestMethod}$$`
                }
            } 

            if (data.germplasmDbId !== undefined) {
                germplasmDbId = data.germplasmDbId

                if (!validator.isInt(germplasmDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400238)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate germplasm ID
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if seed is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            germplasm.germplasm germplasm
                        WHERE 
                            germplasm.is_void = FALSE AND
                            germplasm.id = ${germplasmDbId}
                    )
                `
                validateCount += 1

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                germplasm_id = $$${germplasmDbId}$$`
            }


            if (data.programDbId !== undefined) {
                programDbId = data.programDbId

                if (programDbId !== "null" && !validator.isInt(programDbId)) {
                    let errMsg = `Invalid format, program Id must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input null
                if (programDbId === "null") {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        program_id = NULL
                    `
                }
                else {
                    // Validate program ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if program is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.program program
                            WHERE 
                                program.is_void = FALSE AND
                                program.id = ${programDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    program_id = $$${programDbId}$$`
                }
            } 

            if (data.sourceExperimentDbId !== undefined) {
                sourceExperimentDbId=data.sourceExperimentDbId

                if (sourceExperimentDbId !== "null" && !validator.isInt(sourceExperimentDbId)) {
                    let errMsg = `Invalid format, experiment Id must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input null
                if (sourceExperimentDbId === "null") {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        source_experiment_id = NULL
                    `
                }
                else {
                    // Validate program ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.experiment experiment
                            WHERE 
                            experiment.is_void = FALSE AND
                            experiment.id = ${sourceExperimentDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    source_experiment_id = $$${sourceExperimentDbId}$$`
                }
            } 

            if (data.entryDbId !== undefined) {
                sourceEntryDbId=data.entryDbId

                if (!validator.isInt(sourceEntryDbId)) {
                    let errMsg = `Invalid format, Entry Id must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                                // Validate program ID
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            experiment.entry entry
                        WHERE 
                            entry.is_void = FALSE AND
                            entry.id = ${sourceEntryDbId}
                    )
                `
                validateCount += 1

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                source_entry_id = $$${sourceEntryDbId}$$`
            } 

            if (data.sourceOccurrenceDdId !== undefined) {
                sourceOccurrenceDdId=data.sourceOccurrenceDdId

                if (!validator.isInt(occurrenceDbId)) {
                    let errMsg = `Invalid format, Occurrence Id must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate program ID
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if occurrence is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            experiment.occurrence occurrence
                        WHERE 
                            occurrence.is_void = FALSE AND
                            occurrence.id = ${sourceOccurrenceDdId}
                    )
                `
                validateCount += 1

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                source_occurrence_id = $$${sourceOccurrenceDbId}$$`
            } 

            if (data.sourceLocationDbId !== undefined) {
                sourceLocationDbId=data.sourceLocationDbId

                if (!validator.isInt(sourceLocationDbId)) {
                    let errMsg = `Invalid format, Location Id must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate program ID
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if location is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            experiment.location location
                        WHERE 
                            location.is_void = FALSE AND
                            location.id = ${sourceLocationDbId}
                    )
                `
                validateCount += 1
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                source_location_id = $$${sourceLocationDbId}$$`
            } 

            if (data.sourcePlotDbId !== undefined) {
                sourcePlotDbId=data.sourcePlotDbId

                if (!validator.isInt(plotDbId)) {
                    let errMsg = `Invalid format, plot Id must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate program ID
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if plot is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            experiment.plot plot
                        WHERE 
                            plot.is_void = FALSE AND
                            plot.id = ${sourcePlotDbId}
                    )
                `
                validateCount += 1

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                source_plot_id = $$${sourcePlotDbId}$$`
            } 

            if (data.description !== undefined) {
                description=data.description
                // Check if description input null
                if (sourceExperimentDbId === "null") {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    description = NULL
                    `
                }
                else {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    description = $$${description}$$`
                }
            }

            if (data.notes !== undefined) {
                notes=data.notes
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                notes = $$${notes}$$`
            } 

                        /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if(setQuery.length > 0) setQuery += `,`

            let updateSeedQuery = `
                UPDATE
                    germplasm.seed seed
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    seed.id = ${seedDbId}
            `

            // Update seed record
            await sequelize.transaction(async transaction => {
                return await sequelize.query(updateSeedQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            let resultArray = {
                seedDbId: seedDbId,
                recordCount: 1,
                href: seedUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Implementation of delete call for /v3/seeds/{id}
    delete: async function (req, res, next) {

        let userDbId = await tokenHelper.getUserId(req, res)

        // If userDbId is undefined, terminate
        if(userDbId === undefined){
            return
        }

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
    
        // Defaults
        let seedDbId = req.params.id
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let seedUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/seeds"

        if (!validator.isInt(seedDbId)) {
            let errMsg = "Invalid format, seed ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            // Build query
            let seedQuery = `
                SELECT
                    count(1)
                FROM 
                    germplasm.seed seed
                WHERE
                    seed.is_void = FALSE AND
                    seed.id = ${seedDbId}
            `

            // Retrieve seed from the database     
            let hasSeed = await sequelize.query(seedQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Check if seed is non-existent in database
            if (hasSeed === undefined || 
                (hasSeed[0] !== undefined && hasSeed[0]['count'] !== undefined && parseInt(hasSeed[0]['count']) == 0)){
                let errMsg = `The seed you requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Build query for voiding
            let deleteSeedQuery = `
                UPDATE
                    germplasm.seed seed
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    seed.id = ${seedDbId}
                `

            // Delete (void) seed record
            await sequelize.transaction(async transaction => {
                return await sequelize.query(deleteSeedQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            let resultsArray = {
                seedDbId: seedDbId,
                recordCount: 1,
                href: seedUrlString + '/' + seedDbId
            }

            res.send(200, {
                rows: resultsArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}