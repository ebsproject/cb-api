/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let format = require('pg-format')

module.exports = {

    /**
     * POST /v3/seeds/:id/delete-packages 
     * 
     * Deletes packages given the seed ID
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */

    post: async function (req, res, next) {

        // Retrieve seed ID
        let seedDbId = req.params.id

        if (!validator.isInt(seedDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400248)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Check if seed exists
        let seedQuery = `
            SELECT EXISTS(
                SELECT 1 
                FROM 
                    germplasm.seed s
                WHERE 
                    s.id = ${seedDbId}
                    AND s.is_void= FALSE
            )
        `

        // Retrieve seed record
        let seed = await sequelize.query(seedQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        // If seed does not exist, return error
        if (await seed === undefined || await !seed[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404044)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if seed is referenced by non-voided entries
        let seedEntryQuery = `
            SELECT EXISTS (
                SELECT 1
                FROM
                    germplasm.seed s
                    LEFT JOIN experiment.entry e ON e.seed_id = s.id
                WHERE
                    s.id = ${seedDbId}
                    AND e.id IS NOT NULL
                    AND e.is_void = FALSE
            )
        `

        // Retrieve seed entries
        let seedEntry = await sequelize.query(seedEntryQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        // If entries exist, return error
        if (await seedEntry === undefined || await seedEntry[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 403006)
            res.send(new errors.ForbiddenError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })
            let resultArray = []
            let packagesObj = {}

            let deletePackagesDataQuery = format(`
                UPDATE
                    germplasm.package package
                SET
                    is_void = TRUE,
                    package_code = CONCAT('VOIDED-',package.package_code,'-',package.id),
                    package_status = 'inactive',
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                FROM 
                    germplasm.seed s
                WHERE 
                    s.id = package.seed_id
                    AND s.id =${seedDbId}
                    AND package.is_void = FALSE
                RETURNING package.id
            `)

            let packageRecords = await sequelize.query(deletePackagesDataQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            await transaction.commit()

            for (packages of packageRecords[0]) {
                packagesObj = {
                    packageDbId: packages["id"]
                }
                resultArray.push(packagesObj)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}