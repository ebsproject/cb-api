/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    post: async function(req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let addedOrderString = `
            ORDER BY
                package.id
        `
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        let seedDbId = req.params.id

        if (!validator.isInt(seedDbId)) {
            let errMsg = 'Invalid request, seedDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let seedQuery = `
            SELECT
                seed.id AS "seedDbId",
                seed.seed_code AS "seedCode",
                seed.seed_name AS "seedName",
                seed.harvest_date AS "harvestDate",
                seed.harvest_method AS "harvestMethod",
                germplasm.id AS "germplasmDbId",
                germplasm.designation AS "designation",
                germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                gn.germplasm_other_names AS "germplasmOtherNames",
                seed.program_id AS "programDbId",
                program.program_code AS "seedManager",
                seed.source_experiment_id AS "sourceExperimentDbId",
                experiment.experiment_name AS "sourceExperiment",
                experiment.experiment_year AS "sourceExperimentYear",
                experiment.experiment_type AS "sourceExperimentType",
                stage.id AS "experimentStageDbId",
                stage.stage_name AS "experimentStage",
                stage.stage_code AS "experimentStageCode",
                season.id AS "sourceExperimentSeasonDbId",
                season.season_code AS "sourceExperimentSeason",
                seed.source_occurrence_id AS "sourceOccurrenceDbId",
                occurrence.occurrence_name AS "sourceOccurrenceName",
                seed.source_location_id AS "sourceLocationDbId",
                "location".location_code AS "sourceLocationCode",
                "location".location_name AS "sourceLocationName",
                seed.source_entry_id AS "sourceEntryDbId",
                "entry".entry_code AS "sourceEntryCode",
                "entry".entry_number AS "seedSourceEntryNumber",
                seed.source_plot_id AS "sourcePlotDbId",
                plot.plot_code AS "seedSourcePlotCode",
                plot.plot_number AS "seedSourcePlotNumber",
                plot.rep AS "replication",
                "germplasmCross".id AS "crossDbId",
                "germplasmCross".cross_name AS "crossName",
                seed.harvest_date AS "harvestDate",
                (EXTRACT (YEAR FROM seed.harvest_date)) AS "sourceHarvestYear",
                seed.description AS "description",
                seed.notes AS "notes",
                seed.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                seed.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                germplasm.seed seed
            LEFT JOIN
                tenant.person creator ON seed.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON seed.modifier_id = modifier.id
                JOIN
                germplasm.germplasm germplasm ON germplasm.id = seed.germplasm_id
                AND germplasm.is_void = FALSE
            LEFT JOIN (
                SELECT
                    STRING_AGG(gn.name_value, '|'),
                    gn.germplasm_id
                FROM
                    germplasm.germplasm_name gn
                WHERE
                    gn.is_void = FALSE
                GROUP BY
                    gn.germplasm_id
            ) AS gn (germplasm_other_names, germplasm_id)
                ON gn.germplasm_id = germplasm.id
            LEFT JOIN
                tenant.program program ON program.id = seed.program_id
                AND program.is_void = FALSE
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = seed.source_experiment_id
                AND experiment.is_void = FALSE 
            LEFT JOIN
                tenant.stage stage ON stage.id = experiment.stage_id
                AND stage.is_void = FALSE
            LEFT JOIN
                tenant.season season ON season.id = experiment.season_id
                AND season.is_void = FALSE
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id
                AND occurrence.is_void = FALSE
            LEFT JOIN
                experiment.location "location" ON "location".id = seed.source_location_id
                AND location.is_void = FALSE   
            LEFT JOIN
                experiment.entry "entry" ON "entry".id = seed.source_entry_id
                AND entry.is_void = false
            LEFT JOIN
                experiment.plot plot ON plot.id = seed.source_plot_id
                AND plot.is_void = FALSE
            LEFT JOIN
                germplasm.cross "germplasmCross" ON "germplasmCross".id = seed.cross_id
            WHERE
                seed.is_void = FALSE AND
                seed.id = ${seedDbId}
        `

        let seed = await sequelize
            .query(seedQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await seed == undefined || await seed.length < 1) {
            let errMsg = 'Resource not found, the seed you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                        ${parameters['distinctOn']}
                `
            }
            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the entries value retrieval query
        let entriesQuery = null
        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                entriesQuery = knex.select(selectString)
            } else {
                entriesQuery = knex.column(parameters['fields'].split('|'))
            }

            entriesQuery += `
            FROM
                experiment.entry entry
            LEFT JOIN
                tenant.person creator ON entry.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON entry.modifier_id = modifier.id
            LEFT JOIN
                experiment.entry_list entry_list ON entry_list.id = entry.entry_list_id
            WHERE
                entry.is_void = FALSE AND
                entry.seed_id = ${seedDbId}
            ` + addedConditionString +`
            ORDER BY
                entry.id
            `
        } else {
            entriesQuery = `
            SELECT
                ${addedDistinctString}
                entry.id AS "entryDbId",
                entry.entry_number AS "entryNumber",
                entry.entry_code AS "entryCode",
                entry.entry_name AS "entryName",
                entry.entry_type AS "entryType",
                entry.entry_role AS "entryRole",
                entry.entry_class AS "entryClass",
                entry.entry_status AS "entryStatus",
                entry.description,
                entry.entry_list_id AS "entryListDbId",
                entry_list.experiment_id AS "experimentDbId",
                entry.notes,
                entry.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                entry.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbID",
                modifier.person_name AS "modifier"
            FROM
                experiment.entry entry
            LEFT JOIN
                tenant.person creator ON entry.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON entry.modifier_id = modifier.id
            LEFT JOIN
                experiment.entry_list entry_list ON entry_list.id = entry.entry_list_id
            WHERE
                entry.is_void = FALSE AND
                entry.seed_id = ${seedDbId}
            ` + addedConditionString +`
            ORDER BY
                entry.id
            `
        }
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let entriesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            entriesQuery,
            conditionString,
            orderString,
        )
        
        let entries = await sequelize
            .query(entriesFinalSqlQuery, {
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (entries === undefined || entries.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let entriesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                entriesQuery,
                conditionString,
                orderString,
            )
        
        let entriesCount = await sequelize
            .query(
                entriesCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        seed[0]['entries'] = entries[0]
        count = entriesCount[0].count

        res.send(200, {
            rows: seed,
            count: count
        })
        return
    }
}