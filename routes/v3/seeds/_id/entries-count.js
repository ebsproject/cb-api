/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the number of entries using the seed
    // POST /v3/seeds/:id/entries-count
    post: async function(req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let addedOrderString = `
            ORDER BY
                package.id
        `
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        let seedDbId = req.params.id

        if (!validator.isInt(seedDbId)) {
            let errMsg = 'Invalid request, seedDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let seedQuery = `
            SELECT
                seed.id AS "seedDbId",
                seed.seed_code AS "seedCode",
                seed.seed_name AS "seedName",
                seed.germplasm_id AS "germplasmDbId",
                seed.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                seed.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                germplasm.seed seed
            LEFT JOIN
                tenant.person creator ON seed.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON seed.modifier_id = modifier.id
            WHERE
                seed.is_void = FALSE AND
                seed.id = ${seedDbId}
        `

        // Retrieve the seed record using the ID
        let seed = await sequelize
            .query(seedQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await seed == undefined || await seed.length < 1) {
            let errMsg = 'Resource not found, the seed you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                        ${parameters['distinctOn']}
                `
            }
            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the entries value retrieval query
        let entriesQuery = null
        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                entriesQuery = knex.select(selectString)
            } else {
                entriesQuery = knex.column(parameters['fields'].split('|'))
            }

            entriesQuery += `
            FROM
                experiment.entry entry
            LEFT JOIN
                tenant.person creator ON entry.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON entry.modifier_id = modifier.id
            WHERE
                entry.is_void = FALSE AND
                entry.seed_id = ${seedDbId}
            ` + addedConditionString +`
            ORDER BY
                entry.id
            `
        } else {
            entriesQuery = `
            SELECT
                ${addedDistinctString}
                entry.id AS "entryDbId",
                entry.description,
                entry.notes,
                entry.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                entry.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbID",
                modifier.person_name AS "modifier"
            FROM
                experiment.entry entry
            LEFT JOIN
                tenant.person creator ON entry.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON entry.modifier_id = modifier.id
            WHERE
                entry.is_void = FALSE AND
                entry.seed_id = ${seedDbId}
            ` + addedConditionString +`
            ORDER BY
                entry.id
            `
        }
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let entriesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                entriesQuery,
                conditionString,
                orderString,
            )
        
        let entriesCount = await sequelize
            .query(
                entriesCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
                
        count = entriesCount[0].count
        seed[0]['entriesCount'] = count

        res.send(200, {
            rows: seed,
            count: count
        })
        return
    }
}