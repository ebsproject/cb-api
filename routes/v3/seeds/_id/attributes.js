/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

    // Implementation of GET call for /v3/seed/:id/attributes
    get: async function (req, res, next) {
        // retrieve the seed Id
        let seedDbId = req.params.id

        if (!validator.isInt(seedDbId)) {
            let errMsg = "Invalid format, seed ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let seedQuery = `
            SELECT
                seed.id AS "seedDbId"
            FROM
                germplasm.seed seed
            WHERE
                seed.is_void = FALSE
                AND seed.id = ${seedDbId}
        `
        let seed = await sequelize.query(seedQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await seed == undefined || await seed.length < 1) {
            let errMsg = "The seed you have requested does not exist"
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        //build query
        let seedAttributesQuery = `
            SELECT
                attribute.id AS "seedAttributeDbId",
                attribute.data_value AS "attributeDataValue",
                attribute.creation_timestamp AS "creationTimestamp",
                attribute.creator_id AS "creatorDbId",
                creator.person_name AS "creator",
                variable.id AS "variableDbId",
                variable.abbrev AS "variableAbbrev",
                variable.display_name AS "variableDisplayName"
            FROM
                germplasm.seed_attribute attribute
            JOIN
                master.variable variable ON attribute.variable_id = variable.id
            JOIN
	            tenant.person creator ON creator.id = attribute.creator_id
            WHERE
                attribute.seed_id = ${seedDbId}
                AND attribute.is_void = FALSE
        `

        // retrieve the attributes from the database
        let seedAttributes = await sequelize.query(seedAttributesQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch (async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        seed[0]['attributes'] = seedAttributes

        // get count
        let seedAttributesCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(seedAttributesQuery)
        let seedAttributesCount = await sequelize.query(seedAttributesCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await seedAttributesCount == undefined || await seedAttributesCount.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        count = seedAttributesCount[0].count
        res.send(200, {
            rows: seed,
            count: count
        })
        return
    }
}