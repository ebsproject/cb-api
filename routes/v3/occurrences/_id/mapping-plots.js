/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
const { sequelize } = require('../../../../config/sequelize')
const errors = require('restify-errors')
const validator = require('validator')
const errorBuilder = require('../../../../helpers/error-builder')
const processQueryHelper = require('../../../../helpers/processQuery/index.js')
const logger = require('../../../../helpers/logger')
const responseHelper = require('../../../../helpers/responses')

module.exports = {
    // Endpoint for retrieving an existing mapping plot records of an occurrence
    // GET /v3/occurrrences/:id/mapping-plots
    get: async function (req, res, next) {
        // Retrieve occurrence Id
        let occurrenceDbId = req.params.id
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let responseColString = `
            occurrence.id AS "occurrenceDbId",
            occurrence.occurrence_name AS "occurrenceName",
            site.id AS "siteDbId",
            site.geospatial_object_name AS "site",
            field.id AS "fieldDbId",
            field.geospatial_object_name AS "field",
            location.id AS "locationDbId",
            location.location_code AS "locationCode",
            season.season_code AS "experimentSeasonCode",
            season.season_name AS "seasonName",
            experiment.experiment_name AS "experiment",
            experiment.experiment_year AS "experimentYear",
            experiment.experiment_type AS "experimentType",
            experiment.experiment_design_type AS "experimentDesignType",
            plot.id AS "plotDbId",
            pi.entry_number AS "entryNumber",
            pi.entry_name AS "entryName",
            pi.entry_type AS "entryType",
            germplasm.germplasm_code AS "germplasmCode",
            germplasm.parentage AS "parentage",
            seed.seed_name AS "seedName",
            plot.rep,
            plot.plot_code AS "plotCode",
            plot.plot_number AS "plotNumber",
            plot.block_number AS "blockNumber",
            plot.design_x AS "designX",
            plot.design_y AS "designY",
            plot.pa_x AS "paX",
            plot.pa_y AS "paY",
            plot.field_x AS "fieldX",
            plot.field_y AS "fieldY",
            pi.creation_timestamp AS "creationTimestamp",
            germplasm.designation AS "designation",
            program.program_code AS "programAbbrev",
            cp.contact_person AS "contactPerson",
            creator.id AS "creatorDbId",
            creator.person_name AS "creator",
            plot.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS "modifier"`
        const endpoint = 'occurrences/:id/mapping-plots'
        const operation = 'SELECT'

        // Check if ID is an integer
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = `Invalid format. Occurrence ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))

            return
        }

        // Check if occurrenceDbId exists
        let transactionQuery = `
            SELECT count(1)
            FROM 
                experiment.occurrence
            WHERE 
                id = ${occurrenceDbId} AND
                is_void = FALSE
        `
        let occurrenceRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (await occurrenceRecord == undefined || await occurrenceRecord[0]['count'] == 0) {
            let errMsg = 'The occurrenceDbId does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Build query
        let mappingPlotsQuery = `
            SELECT
                ${responseColString}
            FROM
                experiment.plot plot
            LEFT JOIN
                experiment.occurrence occurrence
            ON
                occurrence.id = plot.occurrence_id
            LEFT JOIN
                experiment.location_occurrence_group log
            ON
                log.occurrence_id = occurrence.id
            LEFT JOIN
                experiment.experiment experiment
            ON
                experiment.id = occurrence.experiment_id
            LEFT JOIN
                place.geospatial_object field
            ON
                field.id = occurrence.field_id
            LEFT JOIN
                place.geospatial_object site
            ON
                site.id = occurrence.site_id
            LEFT JOIN
                experiment.location location
            ON
                location.id = log.location_id
            LEFT JOIN
                tenant.season season
            ON
                season.id = experiment.season_id
            LEFT JOIN
                tenant.program program
            ON
                program.id = experiment.program_id
            LEFT JOIN
                experiment.planting_instruction pi
            ON
                pi.plot_id = plot.id
            LEFT JOIN
                germplasm.seed seed
            ON
                seed.id = pi.seed_id
            LEFT JOIN
                germplasm.germplasm germplasm
            ON
                germplasm.id = pi.germplasm_id
            JOIN
                tenant.person creator
            ON
                creator.id = plot.creator_id::integer
            LEFT JOIN 
                tenant.person modifier
            ON
                modifier.id = plot.modifier_id::integer
            LEFT JOIN
                (
                    SELECT
                        occurrence_data.occurrence_id,
                        occurrence_data.data_value AS "contact_person"
                    FROM
                        experiment.occurrence_data occurrence_data
                    LEFT JOIN
                        master.variable variable
                    ON
                        variable.id = occurrence_data.variable_id
                    WHERE
                        variable.abbrev = 'CONTCT_PERSON_CONT'
                        AND variable.is_void = FALSE
                        AND occurrence_data.is_void = FALSE
                        AND occurrence_data.occurrence_id = ${occurrenceDbId}
                ) AS cp
            ON
                cp.occurrence_id = occurrence.id
            WHERE
                plot.occurrence_id = ${occurrenceDbId}
                AND plot.is_void = FALSE
            ORDER BY
                plot.plot_number
        `

        // Generate the final sql query 
        let mappingPlotsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(mappingPlotsQuery)

        mappingPlotsFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(mappingPlotsFinalSqlQuery)

        // Retrieve the plotData records AND the totalCount
        let mappingPlots = await sequelize
            .query(mappingPlotsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                return undefined
            })

        // If sequelize error was discovered
        if (mappingPlots === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        // Return error if resource is not found
        if (mappingPlots.length < 1) {
            let errMsg = 'Resource not found. The mapping plot records you have requested for do not exist.'
            res.send(new errors.NotFoundError(errMsg))

            return
        }

        let count = mappingPlots[0].totalCount

        res.send(200, {
            rows: mappingPlots,
            count: count
        })

        return
    }
}
