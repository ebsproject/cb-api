/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let occurrenceHelper = require('../../../../helpers/occurrence/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')

module.exports = {
    // Endpoint for retrieving plots and plot data
    // POST /v3/occurrences/:id/harvest-plot-data-search
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = params.sort
        let count = 0
        let orderString = ''
        let addedOrderString = ``
        let conditionString = ''
        let orderNumberConditionString = ``
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let isBasic = params.isBasic != null && params.isBasic.toLowerCase() == 'true'
        let attributes = ["plotDbId", "locationDbId", "occurrenceDbId",
            "experimentDbId", "entryDbId", "germplasmDbId", "plotCode", "plotNumber",
            "rep", "harvestStatus", "designation", "parentage", "state", "type",
            "cropDbId", "cropCode", "harvestDate", "harvestMethod", "noOfPlant",
            "noOfPanicle", "specificPlantNo", "noOfEar", "terminalHarvestDate",
            "terminalHarvestMethod", "terminalNoOfPlant", "terminalNoOfPanicle",
            "terminalSpecificPlantNo", "terminalNoOfEar", "creatorDbId", "creator",
            "modifierDbId", "modifier"]

        let creatorId = await tokenHelper.getUserId(req)

        // Get plot data creator ID
        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceDbId = req.params.id

        // Check if occurrence ID is an integer
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.occurrence_status AS "occurrenceStatus"
            FROM
                experiment.occurrence occurrence
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
        `

        // Find occurrence in the database
        let occurrence = await sequelize
            .query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        // If the occurrence does not exist, return an error
        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = 'Resource not found. The occurrence you have requested for does not exist.'
            if (!res.headersSent) res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields

                // add occurrenceDbId and plotDbId in fields parameter
                parameters['fields'] += '|occurrence.id AS occurrenceDbId'
                if (!parameters['fields'].includes('plotDbId')) {
                    parameters['fields'] += '|plot.id AS plotDbId'
                }
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                let distinctOrder = await processQueryHelper.getDistinctOrderString(parameters['distinctOn']) 
                addedOrderString = `
                    ORDER BY
                        ${distinctOrder}
                `
            }

            if (req.body.sort != undefined) {
                if (typeof req.body.sort == 'object') {
                    if (
                        req.body.sort.attribute != undefined
                        && req.body.sort.sortValue != undefined
                        && req.body.sort.attribute.trim() != ""
                        && req.body.sort.sortValue.trim() != ""
                        && attributes.includes(req.body.sort.attribute.trim())
                    ) {
                        orderString = await processQueryHelper.getOrderStringBySortValue(req.body.sort)
                    }
                }
            }
            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'orderNumber',
                'fields',
                'distinctOn',
                'sort'
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            // Build order number condition string
            if (req.body.orderNumber != null) {
                // Get the filter condition
                orderNumberConditionString = await processQueryHelper.getFilter(
                    {
                        orderNumber: req.body.orderNumber
                    },
                    [],
                )

                if (orderNumberConditionString.includes('invalid')) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }
        }

        // Build basic harvest data CTE
        let harvestDataCte = `
            withHarvestData AS (
                select
                    baseQuery.*
                from
                    baseQuery
            ),
        `
        // If isBasic flag is set to FALSE, rebuild harvest data CTE
        if (!isBasic) {
            // Get supported variables
            let variables = await occurrenceHelper.getHMSupportedVariables("plot")

            // Rebuild harvest data CTE
            harvestDataCte = `
                withHarvestData AS (
                    select
                        baseQuery.*,
                        ${variables.committedDataSelect.join(',')},
                        ${variables.terminalDataSelect.join(',')}
                    from
                        baseQuery
                    left join (
                        select
                            "plotDbId",
                            ${variables.committedData.join(',')}
                        from
                            crosstab(
                                '
                                    SELECT
                                        DISTINCT ON (plot.id, pd.variable_id)
                                        plot.id AS "plotDbId",
                                        pd.variable_id,
                                        pd.data_value
                                    FROM
                                        experiment.plot plot
                                    LEFT JOIN
                                        experiment.plot_data pd ON plot.id = pd.plot_id
                                        AND pd.is_void = FALSE
                                    WHERE
                                        plot.occurrence_id = ${occurrenceDbId}
                                    GROUP BY
                                        plot.id,
										pd.variable_id,
                                        pd.data_value,
                                        pd.id
                                    ORDER BY
                                        plot.id ASC, pd.variable_id ASC, pd.id DESC;
                                ',
                                'SELECT UNNEST(ARRAY[${variables.variableIds}]);'
                            ) as (
                                "plotDbId" integer,
                                ${variables.committedDataAs.join(',')}
                            ) 
                    ) committedData ON committedData."plotDbId" = baseQuery."plotDbId"
                    left join (
                        select
                            "plotDbId",
                            ${variables.terminalData.join(',')}
                        from
                            crosstab(
                                '
                                    SELECT
                                        plot.id AS "plotDbId",
                                        td.variable_id,
                                        td.value
                                    FROM
                                        experiment.plot plot
                                    LEFT JOIN
                                        data_terminal.transaction_dataset td 
                                        ON td.data_unit = ''plot''
                                        AND plot.id = td.entity_id
                                        AND td.is_void = FALSE
                                    WHERE
                                        plot.occurrence_id = ${occurrenceDbId}
                                        AND td.creator_id = ${creatorId}
                                    ORDER BY
                                        plot.id;
                                ',
                                'SELECT UNNEST(ARRAY[${variables.variableIds}]);'
                            ) as (
                                "plotDbId" integer,
                                ${variables.terminalDataAs.join(',')}
                            ) 
                    ) terminalData ON terminalData."plotDbId" = baseQuery."plotDbId"
                ),

            `
        }
            

        // Build the plots value retrieval query
        let plotsQuery = null
        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            plotsQuery = knex.column(parameters['fields'].split('|'))

            plotsQuery += `
                FROM
                    experiment.plot plot
                LEFT JOIN experiment.planting_instruction pi
                    ON pi.plot_id = plot.id
                    AND pi.is_void = FALSE
                JOIN germplasm.germplasm germplasm
                    ON germplasm.id = pi.germplasm_id
                    AND germplasm.is_void = FALSE
                JOIN experiment.occurrence occurrence
                    ON plot.occurrence_id = occurrence.id
                    AND occurrence.is_void = FALSE
                JOIN experiment.experiment experiment
                    ON occurrence.experiment_id = experiment.id
                    AND experiment.is_void = FALSE
                JOIN tenant.stage stage 
                    ON experiment.stage_id = stage.id AND stage.is_void = FALSE
                LEFT JOIN tenant.crop crop
                    ON crop.id = germplasm.crop_id
                    AND crop.is_void = FALSE
                JOIN tenant.person creator ON plot.creator_id = creator.id
                LEFT JOIN tenant.person modifier ON plot.modifier_id = modifier.id
                WHERE
                    plot.occurrence_id = ${occurrenceDbId} AND
                    plot.is_void = FALSE
            `
        } else {
            plotsQuery = `
                SELECT
                    plot.id AS "plotDbId",
                    plot.location_id AS "locationDbId",
                    plot.occurrence_id AS "occurrenceDbId",
                    plot.plot_code AS "plotCode",
                    plot.plot_number AS "plotNumber",
                    plot.rep AS "rep",
                    plot.harvest_status as "harvestStatus",
                    pi.entry_id AS "entryDbId",
                    pi.entry_number AS "entryNumber",
                    pi.entry_code AS "entryCode",
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation AS "designation",
                    germplasm.parentage AS "parentage",
                    germplasm.germplasm_state AS "state",
                    germplasm.germplasm_type AS "type",
                    experiment.id AS "experimentDbId",
                    stage.stage_name AS "stageName",
                    stage.stage_code AS "stageCode",
                    crop.id AS "cropDbId",
                    crop.crop_code AS "cropCode",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    experiment.plot plot
                LEFT JOIN experiment.planting_instruction pi
                    ON pi.plot_id = plot.id
                    AND pi.is_void = FALSE
                JOIN germplasm.germplasm germplasm
                    ON germplasm.id = pi.germplasm_id
                    AND germplasm.is_void = FALSE
                JOIN experiment.occurrence occurrence
                    ON plot.occurrence_id = occurrence.id
                    AND occurrence.is_void = FALSE
                JOIN experiment.experiment experiment
                    ON occurrence.experiment_id = experiment.id
                    AND experiment.is_void = FALSE
                JOIN tenant.stage stage 
                    ON experiment.stage_id = stage.id AND stage.is_void = FALSE
                LEFT JOIN tenant.crop crop
                    ON crop.id = germplasm.crop_id
                    AND crop.is_void = FALSE
                JOIN tenant.person creator ON plot.creator_id = creator.id
                LEFT JOIN tenant.person modifier ON plot.modifier_id = modifier.id
                WHERE
                    plot.occurrence_id = ${occurrenceDbId} AND
                    plot.is_void = FALSE
            `
        }
        // Parse the sort parameters
        if (sort != null && addedOrderString === '') {

            if (orderString != '') {
                orderString = orderString + (await processQueryHelper.getOrderString(sort)).replace("ORDER BY", ', ')
            } else {
                orderString = await processQueryHelper.getOrderString(sort)
            }

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        else if (addedOrderString !== '') {
            orderString = addedOrderString
        }

        let correctingOrderString = orderString
        if (parameters['fields'] && !parameters['fields'].includes("plotDbId")) {
            correctingOrderString = ''
        }

        // Build final query
        let newFinalQuery = `
            WITH baseQuery AS (
                ${plotsQuery}
            ),
            ${harvestDataCte}
            withFilters AS (
                select  
                    ROW_NUMBER() OVER(${orderString}) AS "orderNumber",
                    withHarvestData.*
                from
                    withHarvestData
                ${conditionString}
            ),
            withOrderNumberFilter AS (
                select
                    withFilters.*
                from
                    withFilters
                ${orderNumberConditionString}
            ),
            withDistinct AS (
                select  
                    ${addedDistinctString}
                    *
                from
                    withOrderNumberFilter
                ${addedOrderString}
            ),
            countQuery as (
                SELECT
                    withDistinct."occurrenceDbId",
                    count (1) as "totalCount"
                FROM
                    withDistinct
                GROUP BY withDistinct."occurrenceDbId"
            ),
            withLimits AS (
                select  
                    *
                from
                    withDistinct 
                LIMIT (:limit) OFFSET (:offset)
            )
            select
                cq."totalCount",
                withLimits.*
            from
                withLimits
                JOIN countQuery cq on cq."occurrenceDbId" = withLimits."occurrenceDbId"
                ${correctingOrderString}
        `

        // Generate the final SQL query
        let plotsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plotsQuery,
            conditionString,
            orderString,
        )

        let plots = await sequelize
            .query(newFinalQuery, {
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (plots === undefined || plots.length < 1) {  
            if (!res.headersSent) res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // // DISABLED. Count is incorporated in new final query.
        // let plotsCountFinalSqlQuery = await processQueryHelper
        //     .getCountFinalSqlQuery(
        //         plotsQuery,
        //         conditionString,
        //         orderString,
        //     )
        
        // let plotsCount = await sequelize
        //     .query(
        //         plotsCountFinalSqlQuery, {
        //         type: sequelize.QueryTypes.SELECT,
        //     }
        //     )
        //     .catch(async err => {
        //         let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        //         res.send(new errors.InternalError(errMsg))
        //         return
        //     })

        rows = plots[0]
        if (rows[0] != null && rows[0]['totalCount'] != null) count = parseInt(rows[0]['totalCount'])

        res.send(200, {
            rows: rows,
            count: count
        })
        return
    }
}
