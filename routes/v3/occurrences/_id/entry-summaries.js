/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let aclStudy = require('../../../../helpers/acl/study.js')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let validator = require('validator')
let studyHelper = require('../../../../helpers/study/index.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the entry summary information of the occurrence
    // GET /v3/studies/:id/entry-summaries
    get: async function (req, res, next) {

        // Retrieve the study ID

        let studyDbId = req.params.id

        if (!validator.isInt(studyDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400065)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            // Retrieve the user ID of the client from the access token
            let userId = await tokenHelper.getUserId(req)

            if (userId == null) {
              let errMsg = await errorBuilder.getError(req.headers.host, 401002)
              res.send(new errors.BadRequestError(errMsg))
              return
            }

            let applyAcl = true

            // Check if user is an administrator. If yes, do not apply ACL.
            if (await userValidator.isAdmin(userId)) {
                applyAcl = false
            }

            if (applyAcl == true) {

                // Check if the user has access to the study.
                let canAccess = await aclStudy.isStudyAccessible('study', '' + studyDbId, 'user', '' + userId)

                if (!canAccess) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401005)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let addedConditionString = `AND study.id = (:studyDbId)`

            // Build Query
            let studyQuery = await studyHelper.getStudyQuerySql(addedConditionString)

            // Retrieve study from the database   
            let studies = await sequelize.query(studyQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    studyDbId: studyDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (studies.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404002)
                res.send(new errors.NotFoundError(errMsg))
                return
            }
            
            // Format results from query into an array
            for (study of studies) {
            
                let numericQuery = `
                    SELECT 
                        v.id as "variableDbId",
                        v.abbrev as "variableAbbrev",
                        v.name as "variableName",
                        v.label as "variableLabel",
                        v.data_type as "variableDataType",
                        (
                            SELECT
                                count(pd.value)::float 
                            FROM 
                                operational.entry_data pd 
                            WHERE 
                                pd.variable_id = v.id AND 
                                pd.study_id IN (:studyDbId) AND 
                                pd.is_void = FALSE AND 
                                (
                                    pd.value IS NOT NULL AND 
                                    pd.value != ''
                                )
                        ) as "count",
                        (
                            SELECT
                                min(pd.value)::float 
                            FROM 
                                operational.entry_data pd 
                            WHERE 
                                pd.variable_id = v.id AND
                                pd.study_id IN (:studyDbId) AND 
                                pd.is_void = FALSE AND
                                (
                                    pd.value IS NOT NULL AND 
                                    pd.value != ''
                                )
                            ) as "min",
                        (
                            SELECT 
                                max(pd.value)::float 
                            FROM 
                                operational.entry_data pd 
                            WHERE
                                pd.variable_id = v.id AND
                                pd.study_id IN (:studyDbId) AND 
                                pd.is_void = FALSE AND 
                                (
                                    pd.value IS NOT NULL AND 
                                    pd.value != ''
                                )
                        ) as "max"
                    FROM
                        master.variable v
                    WHERE
                        v.id IN 
                        (
                            SELECT
                                id
                            FROM
                                (
                                    SELECT 
                                        distinct ed.variable_id as id 
                                    FROM 
                                        operational.entry_data ed 
                                    WHERE 
                                        ed.study_id = (:studyDbId) AND
                                        ed.is_void = false
                                ) AS query_1
                            FULL JOIN 
                                (
                                    SELECT 
                                        distinct ed.variable_id as id 
                                    FROM 
                                        operational.entry_metadata ed 
                                    WHERE 
                                        ed.study_id = (:studyDbId) AND
                                        ed.is_void = false
                                ) AS query_2 
                            USING (id)
                        )
                        AND v.data_type IN ('smallint','float','integer','numeric')
                        ORDER by abbrev ASC;
                `

                // Retrieve entry information from the database   
                let numericData = await sequelize.query(numericQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        studyDbId: study.studyDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })
                
                study["entryNumericSummaries"] = numericData
                
                let stringQuery = `
                    SELECT 
                        v.id as "variableDbId",
                        v.abbrev as "variableAbbrev",
                        v.name as "variableName",
                        v.label as "variableLabel",
                        v.data_type as "variableDataType"
                    FROM
                        master.variable v
                    WHERE 
                        v.id IN
                        (
                            SELECT
                                id
                            FROM
                                (
                                    SELECT 
                                        distinct ed.variable_id as id
                                    FROM 
                                        operational.entry_data ed 
                                    WHERE 
                                        ed.study_id = (:studyDbId) AND
                                        ed.is_void = false
                                ) AS query_1
                            FULL JOIN 
                                (
                                    SELECT 
                                        distinct ed.variable_id as id 
                                    FROM 
                                        operational.entry_metadata ed 
                                    WHERE 
                                        ed.study_id = (:studyDbId) AND 
                                        ed.is_void = false
                                ) AS query_2 
                            USING (id)
                        )
                        AND v.data_type NOT IN ('smallint','float','integer','numeric')
                        ORDER by abbrev ASC;
                `
             
                // Retrieve entry summary from the database   
                let stringData = await sequelize.query(stringQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        studyDbId: study.studyDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })
                    
                study["entryStringSummaries"] = stringData
                
            }

            res.send(200, {
                rows: studies
            })
            return
        }
    },
}
