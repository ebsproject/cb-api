/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let searchHelper = require('../../../../helpers/queryTool/index.js')
let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
const logger = require('../../../../helpers/logger')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}
        let condition = ''
        let occurrenceDbId = req.params.id
        let blockColumnsSelectQuery = ''
        let blockColumnsFromQuery = ''
        let variableId = [0]
        let crosstabColumns = null
        let selectColumns = null
        let variableAbbrev = []
        let params = req.query
        let accessDataQuery = ''
        let programCond = ``
        let conditionStringArr = []
        let excludedParametersArray = []
        let responseColString = `
            plot.id::text AS "plotDbId",
            plot.entry_id::text AS "entryDbId",
            pi.entry_code AS "entryCode",
            pi.entry_number AS "entryNumber",
            pi.entry_name AS "entryName",
            pi.entry_type AS "entryType",
            pi.entry_role AS "entryRole",
            pi.entry_class AS "entryClass",
            pi.entry_status AS "entryStatus",
            pi.germplasm_id AS "germplasmDbId",
            g.germplasm_code AS "germplasmCode",
            g.parentage,
            g.generation,
            g.germplasm_state AS "germplasmState",
            g.germplasm_type AS "germplasmType",
            pi.seed_id AS "seedDbId",
            seed.seed_code AS "seedCode",
            seed.seed_name AS "seedName",
            pi.package_id AS "packageDbId",
            package.package_code AS "packageCode",
            package.package_label AS "packageLabel",
            plot_code AS "plotCode",
            plot_number AS "plotNumber",
            plot_type AS "plotType",
            rep,
            design_x AS "designX",
            design_y AS "designY",
            plot_order_number AS "plotOrderNumber",
            pa_x AS "paX",
            pa_y AS "paY",
            field_x AS "fieldX",
            field_y AS "fieldY",
            plot_status AS "plotStatus",
            plot_qc_code AS "plotQcCode",
            pi.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            plot.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier`
        const endpoint = 'occurrences/:id/plot-data-table-search'
        const operation = 'SELECT'

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req, res)

        // If userId is null or undefined, return error
        if (userId === null || userId === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if occurrenceDbId is valid
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid format, occurrenceDbId must be an integer'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if occurrenceDbId exists
        let transactionQuery = `
            SELECT count(1)
            FROM 
                experiment.occurrence
            WHERE 
                id = ${occurrenceDbId} AND
                is_void = FALSE
        `
        let occurrenceRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (await occurrenceRecord == undefined || await occurrenceRecord[0]['count'] == 0) {
            let errMsg = 'The occurrenceDbId does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // check if user is admin or not
        let isAdmin = await userValidator.isAdmin(userId)

        // if not admin, retrieve only occurrences user have access to
        if (!isAdmin && params?.ownershipType && params.ownershipType == 'shared') {
            if (params.collaboratorProgramCode != null) {
                programCond = ` AND program.program_code = '${params.collaboratorProgramCode}'`
            }

            // get all programs that user belongs to
            let getProgramsQuery = `
                SELECT
                    program.id 
                FROM 
                    tenant.program program
                WHERE
                    program.is_void = FALSE
                    AND EXISTS (
                        SELECT 1
                        FROM tenant.program_team pt
                        WHERE
                            pt.is_void = FALSE
                            AND EXISTS (
                                SELECT 1
                                FROM tenant.team_member tm
                                WHERE
                                    tm.is_void = FALSE
                                    AND tm.team_id = pt.team_id
                                    AND tm.person_id = ${userId}
                            )
                            AND pt.program_id = program.id
                    )
                ${programCond}
            `
            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                accessDataQuery += `occ.access_data #> $$\{program,${programObj.id}}$$ is not null`

            }

            // include occurrences shared to the user
            let userQuery = `occ.access_data #> $$\{person,${userId}}$$ is not null`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            } else {
                accessDataQuery += userQuery                
            }

            if (accessDataQuery != ``) {
                accessDataQuery = ` AND (${accessDataQuery})`
            }
        }

        // Get experiment design type
        let expDesignQuery =   `
            SELECT
                exp.experiment_design_type,
                exp.experiment_type
            FROM
                experiment.experiment exp
            WHERE
                exp.is_void = false
                AND EXISTS (
                    SELECT 
                        1
                    FROM
                        experiment.occurrence
                    WHERE
                        is_void = FALSE
                        AND id = ${occurrenceDbId}
                        AND experiment_id = exp.id
                )
        `

        let expDesign = await sequelize.query(expDesignQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        // Extract experiment type and experiment design
        let expType = ''
        if (expDesign != null && expDesign[0] != null) {
            expType = expDesign[0]['experiment_type'] != null ? 
                expDesign[0]['experiment_type'].toLowerCase() : ''
            expDesign = expDesign[0]['experiment_design_type'] != null ? 
                expDesign[0]['experiment_design_type'].toLowerCase() : ''
        }

        // Get occurrence design type
        let occDesignQuery =   `
            SELECT
                occ.occurrence_design_type
            FROM
                experiment.occurrence occ
            WHERE
                occ.is_void = false
                AND occ.id = ${occurrenceDbId}
                
        `

        let occDesign = await sequelize.query(occDesignQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (occDesign != null && occDesign[0] != null) {
            occDesign = occDesign[0]['occurrence_design_type'] != null ? 
                occDesign[0]['occurrence_design_type'].toLowerCase() : ''
        }

        if(['alpha-lattice', 'augmented rcbd', 'unspecified'].includes(expDesign) || ['alpha-lattice', 'augmented rcbd', 'unspecified'].includes(occDesign) || await expType == 'observation') {
            blockColumnsSelectQuery = `, block_number AS "blockNumber"`
        } else if (await expDesign == 'Row-Column' || await occDesign == 'Row-Column') {
            blockColumnsSelectQuery = `, descol1.block_value AS "rowBlockNumber", descol2.block_value AS "colBlockNumber"`
            blockColumnsFromQuery = `
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'RowBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol1
                    ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'ColBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol2
                    ON TRUE
            `
        }

        // Get all plot data variables
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') as "variableId",
                STRING_AGG( '"' || abbrev || '"', ',') as "selectColumns",
                STRING_AGG( abbrev, ',') as "variableAbbrev",
                STRING_AGG( '"' || abbrev || '"' || ' jsonb', ',') as "crosstabColumns"
            FROM(
                SELECT 
                    distinct (plot_data.variable_id) as variable_id,
                    v.abbrev
                    FROM
                        experiment.plot_data plot_data
                            LEFT JOIN experiment.plot ON plot.id = plot_data.plot_id AND plot.is_void = FALSE
                            LEFT JOIN master.variable v ON v.id = plot_data.variable_id
                    WHERE
                        plot.occurrence_id = ${occurrenceDbId}
                        AND plot_data.is_void = FALSE
                ORDER BY v.abbrev
            )a
        `

        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (variableList) {
            variableId = variableList[0]['variableId'] == null ? [0] : variableList[0]['variableId']
            crosstabColumns = variableList[0]['crosstabColumns']
            selectColumns = variableList[0]['selectColumns']

            variableAbbrev = variableList[0]['variableAbbrev'] == null ? [] : variableList[0]['variableAbbrev'].split(',')
        }

        if (crosstabColumns != "" && crosstabColumns != null) {
            crosstabColumns = ',' + crosstabColumns
        } else if (crosstabColumns == null) {
            crosstabColumns = ', "null" text';
        }
        if (selectColumns != "" && selectColumns != null) {
            selectColumns = ',' + selectColumns
        } else if (selectColumns == null) {
            selectColumns = ''
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set columns to be excluded in parameters for filtering
            Array.prototype.diff = function (a) {
                return this.filter(function (i) { return a.indexOf(i) < 0; });
            };

            // Exclude all measurement variables
            excludedParametersArray.push('fields', 'dataQCCode', "dataValue", "plotDataDbId")
            for (abbrev of variableAbbrev) {
                excludedParametersArray.push(abbrev)
            }

            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''

            let conditionArray = []

            // Get filters for plot data with values that is in JSON format
            for (trait in req.body) {

                if (variableAbbrev.includes(trait)) {

                    for (field in req.body[trait]) {
                        condition = ''
                        if (field == 'dataValue') {
                            let variableQuery = `
                                SELECT 
                                    data_type
                                    FROM
                                        master.variable v
                                    WHERE
                                        v.abbrev='${trait}'
                            `
                            let variableList = await sequelize.query(variableQuery, {
                                type: sequelize.QueryTypes.SELECT
                            })
                            let variableDataType = variableList[0]['data_type']
                            value = req.body[trait]['dataValue'];
                            column = '("' + trait + '"->>' + "'dataValue')::" + variableDataType
                            condition = searchHelper.getFilterCondition(value, column, columnCast = true)
                        }

                        if (condition != '') {
                            conditionArray.push(`(` + condition + ')')
                        }
                    }
                }
            }

            if (conditionArray.length > 0) {
                condition = conditionArray.join(' AND ')
                if (conditionString == '') {

                    conditionString = conditionString +
                        ` WHERE `
                        + condition
                } else {
                    conditionString = conditionString +
                        ` AND `
                        + condition
                }
            }

            if (conditionString.includes('You have provided invalid values for filter.')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // build the base query
        let plotDataQuery = null
        let fromQuery = `
            FROM
                experiment.plot
                    LEFT JOIN
                    (
                        SELECT 
                            "plotDbId" 
                            ${selectColumns}
                        FROM
                            CROSSTAB(
                            '
                                SELECT 
                                    plot.id AS "plotDbId",
                                    plot_data.variable_id,
                                    jsonb_build_object(
                                        ''plotDataDbId'', plot_data.id,
                                        ''dataValue'', plot_data.data_value,
                                        ''dataQCCode'', plot_data.data_qc_code
                                    )
                                FROM
                                    experiment.plot plot
                                        LEFT JOIN
                                            experiment.plot_data ON plot.id = plot_data.plot_id
                                WHERE
                                    plot.occurrence_id = ${occurrenceDbId}
                                    AND plot_data.is_void=false 
                                    AND plot.is_void=false
                                ORDER BY
                                    plot.id
                            ',
                            '
                            SELECT UNNEST(ARRAY[${variableId}])
                        '             
                        ) as (
                            "plotDbId" integer
                            ${crosstabColumns}
                        ) 
                    )crosstab ON crosstab."plotDbId" = plot.id
                LEFT JOIN
                    experiment.planting_instruction pi
                ON 
                    plot.id = pi.plot_id
                    and pi.is_void = false
                ${blockColumnsFromQuery}
                LEFT JOIN
                    germplasm.germplasm g
                ON
                    g.id = pi.germplasm_id
                LEFT JOIN
                    germplasm.seed seed
                ON
                    seed.id = pi.seed_id
                LEFT JOIN
                    experiment.occurrence occ
                ON
                    plot.occurrence_id = occ.id
                    AND occ.is_void = FALSE
                LEFT JOIN
                    germplasm.package package
                ON
                    package.id = pi.package_id
                JOIN 
                    tenant.person creator ON creator.id = pi.creator_id::integer
                LEFT JOIN 
                    tenant.person modifier ON modifier.id = pi.modifier_id::integer
            WHERE
                plot.is_void = false 
                AND plot.occurrence_id = ${occurrenceDbId}
                AND g.id = pi.germplasm_id
                ${accessDataQuery}
            ORDER BY
                plot.plot_number
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            plotDataQuery = knex.column(parameters['fields'].split('|'))
            plotDataQuery += fromQuery
        } else {
            plotDataQuery = `
                SELECT
                    ${responseColString}
                    ${blockColumnsSelectQuery}
                    ${selectColumns}
                ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderStringTrait(
                sort, 
                selectColumns.replaceAll('"', '').split(',')
            )
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let plotDataFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            plotDataQuery,
            conditionString,
            orderString
        )

        plotDataFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            plotDataFinalSqlQuery,
            orderString
        )


        // Retrieve the plotData records AND the totalCount
        let plotData = await sequelize
            .query(plotDataFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                const routine = err.parent?.routine ?? ''

                let errMsg = await errorBuilder.getErrorResponse(req.headers.host, routine)
                res.send(new errors.InternalError(errMsg))

                return undefined
            })

        if (await plotData == undefined || await plotData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = plotData[0].totalCount

        res.send(200, {
            rows: plotData,
            count: count
        })
        return
    }
}