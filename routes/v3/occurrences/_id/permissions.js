/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let format = require('pg-format')
let logger = require('../../../../helpers/logger')
let endpoint = ('occurrences/_id/permissions')

module.exports = {
    // Endpoint for creating a new occurrence permission
    // Implementation of POST call for /v3/occurrences/:id/permissions
    post: async function (req, res, next) {
        // Set defaults
        let currentAccess = {}
        let occurrenceDbId = req.params.id

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let occurrencePermissionUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/occurrences/'
            + occurrenceDbId
            + '/permissions'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            let validateOccurrenceQuery = `
                (
                SELECT
                    id,
                    access_data AS "accessData"
                FROM
                    experiment.occurrence occurrence
                WHERE
                    occurrence.is_void = FALSE AND
                    occurrence.id = '${occurrenceDbId}'
                )
            `
            validateOccurrence = await sequelize.query(validateOccurrenceQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (validateOccurrence.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404031)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            currentAccess = validateOccurrence[0]['accessData']

            if (currentAccess == null) {
                currentAccess = {}
            }
            // Records = [{}...]
            records = data.records
            if (data.records == undefined || data.records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        
        // Retrieve the ID of the client via the access token
        let userDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if (userDbId === undefined) {
            return
        }
        // If personDbId is null, return 401: User not found error
        else if (userDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // default access data
        let dateQuery = format(`
            SELECT NOW()`
        )

        let date = await sequelize.query(dateQuery, {
            type: sequelize.QueryTypes.SELECT,
            transaction: transaction
        })
        date = date[0].now

        try {
            for (var record of records) {
                // validate if id and permission exists
                if (
                    !record.entityDbId ||
                    !record.entity ||
                    !record.permission
                ) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400170)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let entity = record.entity
                let entityDbId = record.entityDbId
                let permission = record.permission

                if (permission !== 'read' && permission !== 'write') {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400171)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (entity == 'program' || entity == 'person') {
                    // validate entity
                    let validateEntityQuery = `
                        SELECT
                            id
                        FROM
                            tenant.${entity}
                        WHERE
                            id = ${entityDbId} AND
                            is_void = FALSE
                    `
                    let validateEntity = await sequelize.query(validateEntityQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (validateEntity.length == 0) {
                        // Capitalize first letter of entity
                        const capitalizedEntity = entity.replace(/^./, entity[0].toUpperCase())
                        let errMsg = `${capitalizedEntity} ID ${entityDbId} does not exist`
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    if (currentAccess[entity] == undefined) {
                        currentAccess[entity] = {}
                    }

                    // Write/Overwrite permission
                    currentAccess[entity][entityDbId] = {
                        "addedBy": userDbId,
                        "addedOn": date,
                        "permission": permission
                    }
                } else {
                    let errMsg = 'Invalid entity. Value should be program or person'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            currentAccess = JSON.stringify(currentAccess)

            let permissionTransaction = []
            let resultArray

            transaction = await sequelize.transaction(async transaction => {
                let updateOccurrenceQuery = `
                    UPDATE
                        experiment.occurrence occurrence
                    SET
                        access_data = '${currentAccess}',
                        modification_timestamp = NOW(),
                        modifier_id = ${userDbId}
                    WHERE
                        occurrence.id = ${occurrenceDbId}
                `

                updateOccurrenceQuery = updateOccurrenceQuery.trim()

                permissionTransaction = await sequelize.query(updateOccurrenceQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })

                resultArray = {
                    occurrenceDbId: occurrenceDbId,
                    recordCount: 1,
                    href: occurrencePermissionUrlString
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Log error
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headerSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}