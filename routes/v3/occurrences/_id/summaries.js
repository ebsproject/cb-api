/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let aclStudy = require('../../../../helpers/acl/study.js')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let validator = require('validator')
let studyHelper = require('../../../../helpers/study/index.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving summary information of the occurrence
    // GET /v3/studies/:id/summaries
    get: async function (req, res, next) {

        // Retrieve the occurrence ID

        let studyDbId = req.params.id

        if (!validator.isInt(studyDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400065)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            // Retrieve the user ID of the client from the access token
            let userId = await tokenHelper.getUserId(req)

            if (userId == null) {
              let errMsg = await errorBuilder.getError(req.headers.host, 401002)
              res.send(new errors.BadRequestError(errMsg))
              return
            }

            let applyAcl = true

            // Check if user is an administrator. If yes, do not apply ACL.
            if (await userValidator.isAdmin(userId)) {
                applyAcl = false
            }

            if (applyAcl == true) {

                // Check if the user has access to the occurrence.
                let canAccess = await aclStudy.isStudyAccessible('study', '' + studyDbId, 'user', '' + userId)

                if (!canAccess) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401005)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let addedConditionString = `AND study.id = (:studyDbId)`

            // Build Query
            let studyQuery = await studyHelper.getStudyQuerySql(addedConditionString)

            // Retrieve study from the database   
            let studies = await sequelize.query(studyQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    studyDbId: studyDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (studies.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404002)
                res.send(new errors.NotFoundError(errMsg))
                return
            }
            
            // Format results from query into an array
            for (study of studies) {
                let summaryQuery = `
                    SELECT 
                        (select count(e.id)::int from operational.entry e where e.study_id = "occurrence".id and e.is_void = false) as "entryCount",
                        (select count(p.id)::int from operational.plot p where p.study_id = "occurrence".id and p.is_void = false) as "plotCount",
                        (select count(distinct e.product_id)::int from operational.entry e where  "occurrence".id = e.study_id and e.is_void = false) as "productCount",
                        (select count(distinct ed.variable_id)::int from operational.entry_data ed where "occurrence".id = ed.study_id and ed.is_void = false) as "entryDataCount",
                        (select count(distinct ed.variable_id)::int from operational.plot_metadata ed where "occurrence".id = ed.study_id and ed.is_void = false) as "plotMetadataCount",
                        (select count(distinct ed.variable_id)::int from operational.plot_data ed where "occurrence".id = ed.study_id and ed.is_void = false) as "plotDataCount"
                    FROM 
                        operational.study "occurrence"
                    WHERE 
                        "occurrence".id = (:studyDbId)
                        AND "occurrence".is_void = FALSE 
                `

                // Retrieve summary from the database   
                let summaries = await sequelize.query(summaryQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        studyDbId: study.studyDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })
                    
                study["summaries"] = summaries
            }

            res.send(200, {
                rows: studies
            })
            return
        }
    },
}
