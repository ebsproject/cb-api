/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

    // Endpoint for retrieving an existing harvest summary record of an occurrence
    // GET /v3/occurrrences/:id/harvest-summaries
    get: async function (req, res, next) {

        // Retrieve occurrence Id
        let occurrenceDbId = req.params.id

        // Check if ID is an integer
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = `Invalid format, Occurrence ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let harvestSummaryQuery = `
            WITH 
                plot AS (
                    SELECT
                        occurrence_id,
                        count(plot) AS total_plot,
                        max(rep)::varchar(4) AS total_rep
                    FROM
                        experiment.plot plot
                    LEFT JOIN experiment.occurrence occurrence on plot.occurrence_id = occurrence.id
                    WHERE
                        plot.occurrence_id = ${occurrenceDbId}
                        AND plot.is_void = FALSE
                    GROUP BY occurrence_id
                ),
                entry AS (
                    SELECT
                        occurrence.id AS occurrence_id,
                        count(entry) total_entry
                    FROM
                        experiment.occurrence occurrence
                    LEFT JOIN
                        experiment.entry_list entry_list ON entry_list.experiment_id=occurrence.experiment_id
                    LEFT JOIN
                        experiment.entry entry ON entry.entry_list_id=entry_list.id
                    WHERE
                        occurrence.id=${occurrenceDbId}
                        AND occurrence.is_void = FALSE
                        AND entry_list.is_void = FALSE
                        AND entry.is_void = FALSE
                    GROUP BY occurrence.id
                ),
                package AS (
                    SELECT
                        seed.source_occurrence_id AS occurrence_id,
                        count(package) AS total_package
                    FROM
                        germplasm.package AS package
                    LEFT JOIN
                        germplasm.seed AS seed ON package.seed_id=seed.id
                    WHERE
                        seed.source_occurrence_id=${occurrenceDbId}
                        AND package.is_void=FALSE
                        AND seed.is_void=FALSE
                        AND package.package_code not ilike 'VOIDED-%'
                    GROUP BY source_occurrence_id
                ),
                germplasmCross AS (
                    SELECT
                        occurrence.id AS occurrence_id,
                        count(germplasmCross) total_cross
                    FROM
                        germplasm.cross germplasmCross
                    LEFT JOIN
                        experiment.occurrence occurrence ON germplasmCross.occurrence_id = occurrence.id
                    WHERE
                        occurrence.id = ${occurrenceDbId} AND
                        germplasmCross.is_void = FALSE
                    GROUP BY occurrence.id
                ),
                seedPlot AS (
                    SELECT
                        source_occurrence_id AS occurrence_id,
                        COUNT(seed) AS total_seed_from_plots
                    FROM
                        germplasm.seed seed
                    WHERE
                        seed.source_occurrence_id = ${occurrenceDbId}
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'plot'
                    GROUP BY source_occurrence_id
                ),
                packagePlot AS (
                    SELECT
                        seed.source_occurrence_id AS occurrence_id,
                        COUNT(package) AS total_package_from_plots
                    FROM
                        germplasm.package package
                    LEFT JOIN
                        germplasm.seed seed ON seed.id = package.seed_id
                    WHERE
                        seed.source_occurrence_id = ${occurrenceDbId}
                        AND package.program_id = seed.program_id
                        AND package.is_void = FALSE
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'plot'
                    GROUP BY seed.source_occurrence_id
                ),
                seedCross AS (
                    SELECT
                        source_occurrence_id AS occurrence_id,
                        COUNT(seed) AS total_seed_from_crosses
                    FROM
                        germplasm.seed seed
                    WHERE
                        seed.source_occurrence_id = ${occurrenceDbId}
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'cross'
                    GROUP BY source_occurrence_id
                ),
                packageCross AS (
                    SELECT
                        seed.source_occurrence_id AS occurrence_id,
                        COUNT(package) AS total_package_from_crosses
                    FROM
                        germplasm.package package
                    LEFT JOIN
                        germplasm.seed seed ON seed.id = package.seed_id
                    WHERE
                        seed.source_occurrence_id = ${occurrenceDbId}
                        AND package.program_id = seed.program_id
                        AND package.is_void = FALSE
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'cross'
                    GROUP BY seed.source_occurrence_id
                ),
                crossWithSeed AS (
                    SELECT
                        seed.source_occurrence_id AS occurrence_id,
                        COUNT(DISTINCT(seed.cross_id)) AS cross_with_seed
                    FROM
                        germplasm.seed seed
                    LEFT JOIN
                        germplasm.cross c ON c.experiment_id = seed.source_experiment_id
                    WHERE
                        seed.source_occurrence_id = ${occurrenceDbId}
                        AND seed.harvest_source = 'cross'
                        AND seed.is_void = FALSE
                        AND c.is_void = FALSE
                    GROUP BY source_occurrence_id
                ),
                plotWithSeed AS (
                    SELECT
                        plot.occurrence_id,
                        COUNT(DISTINCT(seed.source_plot_id)) AS plot_with_seed
                    FROM
                        germplasm.seed seed
                    LEFT JOIN
                        experiment.plot plot ON plot.id = seed.source_plot_id
                    WHERE
                        plot.occurrence_id = ${occurrenceDbId}
                        AND plot.is_void = FALSE
                        AND seed.source_occurrence_id = ${occurrenceDbId}
                        AND seed.is_void = FALSE
                        AND seed.harvest_source = 'plot'
                    GROUP BY plot.occurrence_id
                )
                SELECT
                    COALESCE(NULL, total_plot, 0) AS "totalPlotCount",
                    COALESCE(NULL, total_entry, 0) AS "totalEntryCount",
                    total_rep AS "totalRepCount",
                    COALESCE(NULL, total_cross, 0) AS "totalCrossCount",
                    COALESCE(NULL, total_package,0) AS "totalPackageCount",
                    COALESCE(NULL, plot_with_seed,0) AS "harvestedPlots",
                    COALESCE(NULL, total_seed_from_plots, 0) AS "totalSeedsFromPlots",
                    COALESCE(NULL, total_package_from_plots, 0) AS "totalPackagesFromPlots",
                    COALESCE(NULL, cross_with_seed,0) AS "harvestedCrosses",
                    COALESCE(NULL, total_seed_from_crosses, 0) AS "totalSeedsFromCrosses",
                    COALESCE(NULL, total_package_from_crosses, 0) AS "totalPackagesFromCrosses"
                FROM
                    entry e
                LEFT JOIN
                    plot p ON e.occurrence_id = p.occurrence_id
                LEFT JOIN
                    package AS package ON package.occurrence_id=e.occurrence_id
                LEFT JOIN
                    germplasmCross AS gc ON gc.occurrence_id = e.occurrence_id
                LEFT JOIN
                    seedPlot AS sp ON sp.occurrence_id = e.occurrence_id
                LEFT JOIN
                    packagePlot AS pp ON pp.occurrence_id = e.occurrence_id
                LEFT JOIN
                    seedCross AS sc ON sc.occurrence_id = e.occurrence_id
                LEFT JOIN
                    packageCross AS pc ON pc.occurrence_id = e.occurrence_id
                LEFT JOIN
                    crossWithSeed AS cws ON cws.occurrence_id = e.occurrence_id
                LEFT JOIN
                    plotWithSeed AS pws ON pws.occurrence_id = e.occurrence_id

        `

        // Retrieve plot/cross harvest FROM the database
        let harvestSummary = await sequelize.query(harvestSummaryQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (await harvestSummary == undefined || await harvestSummary.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get count
            let harvestSummaryCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(harvestSummaryQuery)

            let harvestSummaryCount = await sequelize.query(harvestSummaryCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            let count = harvestSummaryCount[0].count

            res.send(200, {
                rows: harvestSummary,
                count: count
            })
            return
        }
    }
}
