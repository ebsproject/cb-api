/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let occurrencePlotsHelper = require('../../../../helpers/occurrencePlots/index.js')
let logger = require('../../../../helpers/logger')

module.exports = {
    // Implementation of advanced search functionality for entries of an occurrence
    post: async function (req, res, next) {

        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let responseColString = `
            plot.id AS "plotDbId",
            plot.entry_id AS "entryDbId",
            entry.entry_code AS "entryCode",
            entry.entry_number AS "entryNumber",
            entry.entry_name AS "entryName",
            entry.entry_type AS "entryType",
            entry.entry_role AS "entryRole",
            entry.entry_class AS "entryClass",
            entry.entry_status AS "entryStatus",
            entry.germplasm_id AS "germplasmDbId",
            g.germplasm_code AS "germplasmCode",
            g.germplasm_state AS "germplasmState",
            g.germplasm_type AS "germplasmType",
            g.parentage AS "parentage",
            entry.seed_id AS "seedDbId",
            seed.seed_code AS "seedCode",
            seed.seed_name AS "seedName",
            entry.package_id AS "packageDbId",
            package.package_code AS "packageCode",
            package.package_label AS "packageLabel",
            plot_code AS "plotCode",
            plot_number AS "plotNumber",
            plot_type AS "plotType",
            rep,
            design_x AS "designX",
            design_y AS "designY",
            plot_order_number AS "plotOrderNumber",
            pa_x AS "paX",
            pa_y AS "paY",
            field_x AS "fieldX",
            field_y AS "fieldY",
            block_number AS "blockNumber",
            descol1.block_value AS "rowBlockNumber",
            descol2.block_value AS "colBlockNumber",
            harvest_status AS "harvestStatus",
            plot_status AS "plotStatus",
            plot_qc_code AS "plotQcCode",
            entry.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            plot.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier
        `

        const endpoint = 'occurrences/:id/plots-search'
        const operation = 'SELECT'

        // Retrieve the occurrence ID
        let occurrenceDbId = req.params.id

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'You have provided an invalid format for the occurrence ID.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.occurrence_number AS "occurrenceNumber"
            FROM
                experiment.occurrence occurrence
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
        `

        let occurrence = await sequelize
            .query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = "Resource not found, the occurrence you hve requested for does not exist."
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Defaut values

        let parameters = {}
        parameters['fields'] = null
        parameters['distinctOn'] = ''

        if (req.body != undefined) {
            // Retrieve parameters

            // If fields condition is set
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn'
            ]

            // Get filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Start building the query

        let plotQuery = null

        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(
                        parameters['fields'],
                    )

                let selectString
                    = knex.raw(`${fieldsString}`)
                plotQuery = knex.select(selectString)
            } else {
                plotQuery = knex.column(parameters['fields'].split('|'))
            }

            plotQuery += `
                FROM
                    experiment.plot plot
                LEFT JOIN
                    experiment.planting_instruction pi
                ON 
                    plot.id = pi.plot_id
                    and pi.is_void = false
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'RowBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol1
                    ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'ColBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol2
                    ON TRUE
                JOIN
                    germplasm.germplasm g
                ON
                    pi.germplasm_id = g.id
                LEFT JOIN
                    germplasm.seed seed
                ON
                    seed.id = pi.seed_id
                LEFT JOIN
                    germplasm.package package
                ON
                    package.id = pi.package_id
                JOIN 
                    tenant.person creator 
                ON 
                    creator.id = plot.creator_id::integer
                LEFT JOIN 
                    tenant.person modifier
                ON
                    modifier.id = plot.modifier_id::integer
                WHERE
                    plot.occurrence_id = ` + occurrenceDbId +`
                    AND plot.is_void = false
                ORDER BY
                    ${(parameters['distinctOn']) ? parameters['distinctOn'] : ''}
                    plot.plot_number
                `
        } else {
            plotQuery = await occurrencePlotsHelper.getOccurrencePlotsQuerySql(occurrenceDbId, addedDistinctString, parameters['distinctOn'], responseColString)
        }     

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        let plotFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            plotQuery,
            conditionString,
            orderString,
            addedDistinctString,
        )

        // Get count 
        plotFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            plotFinalSqlQuery,
            orderString
        )

        // Retrieve entries from the database     
        let plots = await sequelize.query(plotFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            await logger.logFailingQuery(endpoint, operation, err)

            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await plots == undefined || await plots.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        occurrence[0]['plots'] = plots

        count = plots[0].totalCount

        res.send(200, {
            rows: occurrence,
            count: count
        })
        return
    }
}