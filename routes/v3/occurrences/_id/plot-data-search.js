/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
const logger = require('../../../../helpers/logger')

module.exports = {
    post: async function(req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let responseColString = `
            location.id AS "locationDbId",
            location.location_code AS "locationCode",
            location.location_name AS "locationName",
            plot.id AS "plotDbId",
            plot.plot_code AS "plotCode",
            plot.plot_number AS "plotNumber",
            plot.plot_type AS "plotType",
            plot.harvest_status AS "harvestStatus",
            plot_data.id AS "plotDataDbId",
            plot_data.variable_id AS "variableDbId",
            variable.abbrev AS "variableAbbrev",
            variable.label AS "variableLabel",
            plot_data.data_value AS "dataValue",
            plot_data.data_qc_code AS "dataQcCode",
            plot_data.transaction_id::text AS "transactionDbId",
            plot_data.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            plot_data.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier,
            plot_data.collection_timestamp AS "collectionTimestamp",
            plot_data.remarks
        `
        const endpoint = 'occurrences/:id/plot-data-search'
        const operation = 'SELECT'

        let occurrenceDbId = req.params.id

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                occurrence.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier"
            FROM
                experiment.occurrence occurrence
            LEFT JOIN
                experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
            LEFT JOIN
                experiment.location location ON location.id = log.location_id
            LEFT JOIN
                tenant.person creator ON occurrence.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON occurrence.modifier_id = modifier.id
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
        `

        let occurrence = await sequelize.query(occurrenceQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = `Resource not found. The occurrence you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            let conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''
            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

        }

        // Build the plot data value retrieval query
        let plotDataQuery = null
        
        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                plotDataQuery = knex.select(selectString)
            } else {
                plotDataQuery = knex.column(parameters['fields'].split('|'))
            }

            plotDataQuery += `
                FROM
                    experiment.plot_data plot_data
                JOIN
                    experiment.plot plot ON plot.id = plot_data.plot_id
                LEFT JOIN
                    experiment.location location ON location.id = plot.location_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                JOIN
                    master.variable variable ON variable.id = plot_data.variable_id
                JOIN
                    tenant.person creator ON creator.id = plot_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = plot_data.modifier_id
                WHERE
                    plot_data.is_void = FALSE AND
                    occurrence.id = ${occurrenceDbId}
            `
        } else {
            plotDataQuery = `
                SELECT
                    ${addedDistinctString}
                    ${responseColString}
                FROM
                    experiment.plot_data plot_data
                JOIN
                    experiment.plot plot ON plot.id = plot_data.plot_id
                LEFT JOIN
                    experiment.location location ON location.id = plot.location_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                JOIN
                    master.variable variable ON variable.id = plot_data.variable_id
                JOIN
                    tenant.person creator ON creator.id = plot_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = plot_data.modifier_id
                WHERE
                    plot_data.is_void = FALSE AND
                    occurrence.id = ${occurrenceDbId}
            `
        }
        
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let plotDataFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            plotDataQuery,
            conditionString,
            orderString,
        )
        
        plotDataFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            plotDataFinalSqlQuery,
            orderString,
        )

        let plotData = await sequelize.query(plotDataFinalSqlQuery, {
            replacements: {
                limit: limit,
                offset: offset,
            }
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = `Resource not found. The occurrence you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        occurrence[0]['plotData'] = plotData[0]
        
        count = (plotData[0][0]) ? plotData[0][0].totalCount : 0

        res.send(200, {
            rows: occurrence,
            count: count
        })
        return
    }
}
