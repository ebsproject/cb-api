/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let logger = require('../../../../helpers/logger')

const endpoint = 'occurrences/:id/plot-count-generations'

module.exports = {

    // POST /v3/occurrences/:id/plot-count-generations
    post: async function (req, res, next) {
        // Retrieve occurrence ID
        let occurrenceDbId = req.params.id

        // Check if occurrence ID is an integer
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = `You have provided an invalid format for the occurrence ID.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if occurrence is existing
        let occurrenceQuery = `
            SELECT
                id
            FROM
                experiment.occurrence
            WHERE
                id = ${occurrenceDbId}
        `

        // Retrieve occurrence from the database
        let occurrence = await sequelize.query(occurrenceQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = `The occurrence you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction

        try {
            // Update plot count of the occurrence
            let query = `
                UPDATE
                    experiment.occurrence AS occ
                SET
                    plot_count = t.plot_count
                FROM (
                    SELECT
                        occ.id AS occurrence_id,
                        (
                            SELECT
                                count(*)
                            FROM
                                experiment.plot AS plot
                                JOIN 
                                    experiment.planting_instruction AS plantinst
                                ON 
                                    plantinst.plot_id = plot.id
                                JOIN 
                                    experiment.entry AS ent
                                ON 
                                    ent.id = plot.entry_id
                            WHERE
                                plot.occurrence_id = ${occurrenceDbId}
                                AND plot.is_void = FALSE
                                AND plantinst.is_void = FALSE
                                AND ent.is_void = FALSE
                        ) AS plot_count
                    FROM
                        experiment.occurrence AS occ
                    WHERE
                        occ.id = ${occurrenceDbId}
                ) AS t
                WHERE
                    occ.id = t.occurrence_id
            `

            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(query, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            resultArray = {
                occurrenceDbId: occurrenceDbId,
                recordCount: 1
            }

            res.send(200, {
                rows: resultArray
            })

            return

        } catch (err) {
            // Show error log messages
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}