/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the number of plot data of the occurrence
    // GET /v3/occurrences/:id/plot-data-count
    get: async function(req, res, next) {
        let count = 0

        // Get occurrence ID from params
        let occurrenceDbId = req.params.id

        // Validate format of occurrence ID
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400241)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build germplasm query
        let occurrenceQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName"
            FROM
                experiment.occurrence
            WHERE
                occurrence.id = ${occurrenceDbId}
        `

        // Retrieve the occurrence record using the ID
        let occurrence = await sequelize
            .query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (occurrence == undefined) return 
        if (occurrence == null || occurrence.length < 1) {
            let errMsg = `The occurrence you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Build the plot data retrieval query
        let plotDataQuery = `
        SELECT
            pd.id AS "plotDataDbId",
            pd.data_value AS "plotDataValue"
        FROM
            experiment.occurrence
        LEFT JOIN
            experiment.plot ON plot.occurrence_id = occurrence.id AND plot.is_void = FALSE
        LEFT JOIN
            experiment.plot_data pd ON pd.plot_id = plot.id AND pd.is_void = FALSE
        WHERE
            occurrence.id = ${occurrenceDbId}
            and pd.id IS NOT NULL
        `

        // Build final seeds count query
        let plotDataCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(plotDataQuery)
        
        // Retrieve seeds count
        let plotDataCount = await sequelize
            .query(
                plotDataCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
                
        // If plotDataCount is undefined, return
        if (plotDataCount === undefined) {
            return
        }
        
        // Add plot data count count to the germplasm object
        count = occurrence.count
        occurrence[0]['plotDataCount'] = parseInt(plotDataCount[0].count)

        res.send(200, {
            rows: occurrence,
            count: count
        })
        return
    }

}