/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let occurrenceEntriesHelper = require('../../../../helpers/occurrenceEntries/index.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
const logger = require('../../../../helpers/logger')

module.exports = {
    // Implementation of advanced search functionality for entries of an occurrence
    post: async function (req, res, next) {

        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let excludedParametersArray = {}
        let responseColString = `
            pi.entry_id AS "entryDbId",
            pi.entry_code AS "entryCode",
            pi.entry_number AS "entryNumber",
            pi.entry_name AS "entryName",
            pi.entry_type AS "entryType",
            pi.entry_role AS "entryRole",
            pi.entry_class AS "entryClass",
            pi.entry_status AS "entryStatus",
            e.description,
            pi.germplasm_id AS "germplasmDbId",
            g.germplasm_code AS "germplasmCode",
            g.designation,
            g.parentage,
            g.generation,
            g.germplasm_state AS "germplasmState",
            g.germplasm_type AS "germplasmType",
            pi.seed_id AS "seedDbId",
            seed.seed_code AS "seedCode",
            seed.seed_name AS "seedName",
            pi.package_id AS "packageDbId",
            package.package_code AS "packageCode",
            package.package_label AS "packageLabel",
            package.package_quantity AS "packageQuantity",
            package.package_unit AS "packageUnit",
            occurrence.id AS "occurrenceDbId",
            occurrence.occurrence_name AS "occurrenceName",
            occurrence.occurrence_code AS "occurrenceCode",
            pi.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            pi.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier
        `

        const endpoint = "occurrences/:id/entries-search"
        const operation = "SELECT"

        // Retrieve the occurrence ID
        let occurrenceDbId = req.params.id

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'You have provided an invalid format for the occurrence ID.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Defaut values

        let parameters = {}
        parameters['fields'] = null
        parameters['distinctOn'] = ''

        let entryQuery = null

        if (req.body != undefined) {
            // Retrieve parameters

            // If fields condition is set
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                parameters['distinctOn'] = 'entryDbId|' + parameters['distinctOn']

                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])
                let distinctString = ''
                let columns = parameters['distinctOn'].split('|');
                for (col of columns) {
                    distinctString += `"${col}",`
                }
                distinctString = distinctString.replace(/,\s*$/g, "") + ','
                parameters['distinctOn'] = distinctString

                if (parameters['fields'] != null) {
                    let fieldsString = await processQueryHelper
                        .getFieldValuesString(
                            parameters['fields'],
                        )
                    fieldsString = fieldsString.replace(/(\s*\w+\.*\w+\s+AS\s+)(\w+)(\s*\,*)/gi, '$1"$2"$3')

                    let selectString
                        = knex.raw(`${fieldsString}`)
                    entryQuery = knex.select(selectString)
                }

            } else {
                if (parameters['fields'] != null) { // fields is specified, no distinctOn
                    // If there is any empty fields due to split, remove
                    let tempHolder = []
                    let tempEntryQuery = parameters['fields'].split('|')
                    for(let item of tempEntryQuery){
                        item = item.trim()
                        if(item !=''){
                            tempHolder.push(item)
                        }
                    }

                    // Build fields query
                    entryQuery = knex.column(tempHolder)
                    entryQuery = entryQuery.toString()
                    entryQuery = entryQuery.replace(/(SELECT)(.+)/gi, '$1 DISTINCT ON (pi.entry_id) $2')

                } else { // no distinctOn, no fields specified
                    addedDistinctString = 'DISTINCT ON (pi.entry_id)'
                }

            }

            // Set the columns to be excluded in parameters for filtering
            excludedParametersArray = [
                'fields',
                'distinctOn',
                'userId'
            ]

            // Get filter condition
            let conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body, 
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''

            if (conditionString && conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

        } else { // no request body parameters
            addedDistinctString = 'DISTINCT ON (pi.entry_id)'
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString && orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let userId = await tokenHelper.getUserId(req, res)

        // If userId is undefined, terminate
        if(userId === undefined){
            return
        }
        // If userId is null, return error
        else if(userId === null){
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Start building the query
        if (parameters['fields'] != null) {

            entryQuery += `
                FROM
                    experiment.plot plot
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                JOIN
                    experiment.planting_instruction pi ON pi.plot_id = plot.id
                JOIN
                    experiment.entry e ON pi.entry_id = e.id
                JOIN
                    germplasm.germplasm g ON g.id = pi.germplasm_id AND g.id = pi.germplasm_id
                LEFT JOIN
                    germplasm.seed seed ON seed.id = pi.seed_id
                LEFT JOIN
                    germplasm.package package ON package.id = pi.package_id
                JOIN 
                    tenant.person creator ON creator.id = pi.creator_id::integer
                LEFT JOIN 
                    tenant.person modifier ON modifier.id = pi.modifier_id::integer
                WHERE
                    plot.occurrence_id = ` + occurrenceDbId + `
                    AND plot.is_void = false
                    AND e.is_void = false
                    AND pi.is_void = false
                    AND plot.occurrence_id = occurrence.id
                `
        } else {

            entryQuery = await occurrenceEntriesHelper.getOccurrenceEntriesQuerySql(occurrenceDbId, addedDistinctString, parameters['distinctOn'], responseColString)
            addedDistinctString = ''
        }

        // Generate the final sql query 
        let entryFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            entryQuery,
            conditionString,
            orderString,
            addedDistinctString,
        )

        // Get count 
        entryFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            entryFinalSqlQuery,
            orderString
        )

        // Retrieve entries from the database     
        let entries = await sequelize.query(entryFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)

                return undefined
            })

        if (entries === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        if (await entries.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = (entries[0]) ? entries[0].totalCount : 0

        res.send(200, {
            rows: entries,
            count: count
        })
        return
    }
}