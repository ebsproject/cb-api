/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let logger = require('../../../../helpers/logger')

const endpoint = 'occurrences/:id/reorder-plots'

module.exports = {

    /**
     * POST /v3/occurrences/:id/reorder-plots reorders a specific and existing occurrence
     * 
     * @param occurrenceDbId integer as a path parameter 
     * @param token string token
     * 
     * @return object response data
     */

    post: async function (req, res, next) {
        
        let occurrenceDbId = req.params.id

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let transaction
        try {            
            let reorderPlotsQuery = `
                UPDATE
                    experiment.plot p
                SET
                    plot_number = t.num,
                    plot_code = t.num,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                FROM
                    (
                        SELECT
                            p.id,
                            p.plot_number,
                            p.modification_timestamp,
                            ROW_NUMBER() OVER(
                                ORDER BY 
                                    p.plot_number, 
                                    p.modification_timestamp
                            ) AS num
                        FROM
                            experiment.plot p
                        WHERE
                            p.is_void = FALSE AND
                            p.occurrence_id = ${occurrenceDbId}
                    ) AS t
                WHERE
                    p.is_void = FALSE AND
                    p.id = t.id
            `

            //Start transaction
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(reorderPlotsQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })
            
            let resultArray = {
                occurrenceDbId: occurrenceDbId
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Show error log messages
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}