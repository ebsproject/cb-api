/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    // POST /v3/occurrences/:id/packages-search
    post: async function(req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        let occurrenceDbId = req.params.id

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceQuery = `
            SELECT
                id AS "occurrenceDbId"
            FROM
                experiment.occurrence
            WHERE
                is_void = FALSE
                AND id = ${occurrenceDbId}
        `

        let occurrence = await sequelize.query(occurrenceQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = `Resource not found, the occurrence you have requested for does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the package record retrieval query
        let packageQuery = null
        
        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                packageQuery = knex.select(selectString)
            } else {
                packageQuery = knex.column(parameters['fields'].split('|'))
            }

            packageQuery += `
                FROM
                germplasm.package package
            LEFT JOIN 
                germplasm.seed seed on seed.id = package.seed_id
            LEFT JOIN
                germplasm.cross "germplasmCross" on "germplasmCross".id = seed.cross_id
            LEFT JOIN
                germplasm.cross_parent "femaleCrossParent" on "femaleCrossParent".cross_id = "germplasmCross".id and "femaleCrossParent".parent_role in ('female', 'female-and-male')
            LEFT JOIN
                experiment.entry "femaleEntry" on "femaleEntry".id = "femaleCrossParent".entry_id
            LEFT JOIN LATERAL (
                SELECT
                    plot.id AS "plotDbId",
                    plot.plot_code AS "plotCode",
                    pi.entry_code AS "entryCode",
                    plot.plot_number AS "plotNumber"
                FROM
                    experiment.plot
                LEFT JOIN
                    experiment.planting_instruction pi ON pi.plot_id = plot.id AND pi.is_void = FALSE
                WHERE
                    pi.entry_id = "femaleEntry".id
                ORDER BY plot.id LIMIT 1
            ) "femalePlot" ON TRUE
            LEFT JOIN
                germplasm.germplasm "femaleGermplasm" on "femaleGermplasm".id = "femaleCrossParent".germplasm_id
            LEFT JOIN
                germplasm.cross_parent "maleCrossParent" on "maleCrossParent".cross_id = "germplasmCross".id and "maleCrossParent".parent_role in ('male')
            LEFT JOIN
                germplasm.germplasm "maleGermplasm" on "maleGermplasm".id = "maleCrossParent".germplasm_id
            LEFT JOIN 
                germplasm.germplasm germplasm on germplasm.id = seed.germplasm_id
            LEFT JOIN 
                experiment.plot plot on plot.id = seed.source_plot_id
            LEFT JOIN 
                experiment.entry entry on entry.id = seed.source_entry_id
            LEFT JOIN
                experiment.occurrence occurrence on occurrence.id = seed.source_occurrence_id AND occurrence.is_void = FALSE
            LEFT JOIN
                tenant.program program ON program.id = seed.program_id AND program.is_void = FALSE
            WHERE
                seed.source_occurrence_id = ${occurrenceDbId}
                AND package.program_id = seed.program_id
                AND seed.is_void = FALSE 
                AND package.is_void = FALSE
                AND germplasm.is_void = FALSE
            `
        } else {
            packageQuery = `
                SELECT 
                    package.id AS "packageDbId",
                    package.package_code AS "packageCode",
                    package.package_label AS label,
                    package.package_quantity AS "quantity",
                    package.package_unit AS unit,
                    seed.id AS "seedDbId",
                    seed.seed_code AS "seedCode",
                    seed.seed_name AS "seedName",
                    seed.harvest_date AS "harvestDate",
                    seed.harvest_method AS "harvestMethod",
                    seed.harvest_source AS "harvestSource",
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation AS "designation",
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.generation AS "generation",
                    germplasm.parentage AS "parentage",
                    plot.plot_number AS "seedSourcePlotNumber",
                    plot.plot_code AS "seedSourcePlotCode",
                    plot.rep AS "replication",
                    entry.entry_code AS "sourceEntryCode",
                    "femalePlot"."plotDbId" AS "femalePlotDbId",
                    "femalePlot"."plotCode" AS "femalePlotCode",
                    "femalePlot"."entryCode" AS "femaleEntryCode"
                FROM
                    germplasm.package package
                LEFT JOIN 
                    germplasm.seed seed on seed.id = package.seed_id
                LEFT JOIN
                    germplasm.cross "germplasmCross" on "germplasmCross".id = seed.cross_id
                LEFT JOIN
                    germplasm.cross_parent "femaleCrossParent" on "femaleCrossParent".cross_id = "germplasmCross".id and "femaleCrossParent".parent_role in ('female', 'female-and-male')
                LEFT JOIN
                    experiment.entry "femaleEntry" on "femaleEntry".id = "femaleCrossParent".entry_id
                LEFT JOIN LATERAL (
                    SELECT
                        plot.id AS "plotDbId",
                        plot.plot_code AS "plotCode",
                        pi.entry_code AS "entryCode",
                        plot.plot_number AS "plotNumber"
                    FROM
                        experiment.plot
                    LEFT JOIN
                        experiment.planting_instruction pi ON pi.plot_id = plot.id AND pi.is_void = FALSE
                    WHERE
                        pi.entry_id = "femaleEntry".id
                    ORDER BY plot.id LIMIT 1
                ) "femalePlot" ON TRUE
                LEFT JOIN
                    germplasm.germplasm "femaleGermplasm" on "femaleGermplasm".id = "femaleCrossParent".germplasm_id
                LEFT JOIN
                    germplasm.cross_parent "maleCrossParent" on "maleCrossParent".cross_id = "germplasmCross".id and "maleCrossParent".parent_role in ('male')
                LEFT JOIN
                    germplasm.germplasm "maleGermplasm" on "maleGermplasm".id = "maleCrossParent".germplasm_id
                LEFT JOIN 
                    germplasm.germplasm germplasm on germplasm.id = seed.germplasm_id
                LEFT JOIN 
                    experiment.plot plot on plot.id = seed.source_plot_id
                LEFT JOIN 
                    experiment.entry entry on entry.id = seed.source_entry_id
                LEFT JOIN
                    experiment.occurrence occurrence on occurrence.id = seed.source_occurrence_id AND occurrence.is_void = FALSE
                LEFT JOIN
                    tenant.program program ON program.id = seed.program_id AND program.is_void = FALSE
                WHERE
                    seed.source_occurrence_id = ${occurrenceDbId}
                    AND package.program_id = seed.program_id
                    AND seed.is_void = FALSE 
                    AND package.is_void = FALSE
                    AND germplasm.is_void = FALSE
                ORDER BY "seedSourcePlotNumber"
            `
        }
        
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        packageFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            packageQuery,
            conditionString,
            orderString
        )

        // Retrieve package record from database
        let packageRecord = await sequelize.query(packageFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await packageRecord == undefined || await packageRecord.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get count 
        packageCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            packageQuery,
            conditionString,
            orderString
        )

        packageCount = await sequelize.query(packageCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = packageCount != undefined ? packageCount[0].count : 0

        res.send(200, {
            rows: packageRecord,
            count: count
        })
        return
    }
}
