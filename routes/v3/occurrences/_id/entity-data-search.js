/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let validator = require('validator')

let errorBuilder = require('../../../../helpers/error-builder')
let occurrenceHelper = require('../../../../helpers/occurrence/index')
let logger = require('../../../../helpers/logger/index')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = 'ORDER BY "occurrenceNumber" ASC'
        let conditionString = ''
        let parameters = {}
        let condition = ''

        let occurrenceDataQueryFields = '*'

        let occurrenceDbId = req.params.id
        let experimentDbId;

        let replaceTraitDataValue = false
        let includePlot = false

        const endpoint = "occurrences/:id/entity-data-search"
        const operation = "SELECT"

        // check if Occurrence ID is in valid format
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if Occurrence exists
        let occurrenceQuery = `
            SELECT
                occurrence.experiment_id
            FROM
                experiment.occurrence occurrence
            WHERE
                is_void = FALSE
                AND id = ${occurrenceDbId}
        `

        let occurrence = await sequelize.query(
            occurrenceQuery,
            {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrence == undefined || await occurrence.length < 0) {
            let errMsg = 'The occurrence ID you have provided does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        experimentDbId = occurrence[0].experiment_id ?? 0

        let selectedEntities
        let hasSelectedEntities = false

        // entitiesToBeRetrieved
        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.entities != undefined) {
                selectedEntities = req.body.entities

                let validEntries = ['experiment', 'occurrence', 'planting_protocol',
                    'management_protocol', 'entry', 'germplasm', 'plot', 'trait']

                let selectedEntitiesArr = selectedEntities.split("|")

                let diffEntries = selectedEntitiesArr.filter(sample => {
                    return validEntries.indexOf(sample) < 0
                })

                if (diffEntries.length > 0) {
                    const diffEntriesString = diffEntries.join(', ')
                    const entityString = (diffEntries.length > 1) ? 'entities' : 'entity'
                    let errMsg = `You have provided invalid input ${entityString}: ${diffEntriesString}`
                    res.send(new errors.BadRequestError(errMsg))

                    return
                }

                selectedEntities = selectedEntitiesArr
                hasSelectedEntities = true
            }

            if (req.body.replaceTraitDataValue != undefined) {
                let inputVal = req.body.replaceTraitDataValue;
                if(typeof replaceTraitDataValue == 'boolean'){
                    inputVal = inputVal.toString();
                }

                if(validator.isBoolean(inputVal)){ 
                    replaceTraitDataValue = inputVal;
                }
                else {
                    let errMsg = `You have provided invalid input value for replaceTraitDataValue! Boolean value expected.`
                    res.send(new errors.BadRequestError(errMsg))
                }
            }
        }

        let dataVariableInfo = null
        let variableDbIds = null
        let variableCrossTabCols = null
        let variableSelectCols = ''

        let occurrenceInfoQuery = `
                SELECT
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_code AS "occurrenceCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    occurrence.occurrence_number AS "occurrenceNumber",
                    occurrence.description AS "occurrenceDescription",
                    occurrence.experiment_id AS "experimentDbId",
                    site."siteCode",
                    site."siteName",
                    field."fieldCode",
                    field."field",
                    location.id AS "locationDbId",
                    location.location_code AS "locationCode",
                    location.location_name AS "locationName",
                    location.description AS "locationDescription"
                FROM
                    experiment.occurrence occurrence
                    LEFT JOIN LATERAL (
                        SELECT
                            site.geospatial_object_code AS "siteCode",
                            site.geospatial_object_name AS "siteName"
                        FROM
                            place.geospatial_object site
                        WHERE
                            is_void = FALSE
                            AND site.id = occurrence.site_id
                        LIMIT 1
                    ) site ON TRUE
                    LEFT JOIN LATERAL (
                        SELECT
                            log.location_id
                        FROM
                            experiment.location_occurrence_group log
                        WHERE
                            log.occurrence_id = occurrence.id
                        LIMIT 1
                    ) log ON TRUE
                    LEFT JOIN experiment.location location 
                        ON location.id = log.location_id
                        AND location.is_void = FALSE
                    LEFT JOIN LATERAL (
                        SELECT
                            geo.geospatial_coordinates
                        FROM
                            place.geospatial_object geo
                        WHERE
                            geo.is_void = FALSE
                            AND geo.id = occurrence.geospatial_object_id
                        LIMIT 1
                    ) geospatial ON TRUE
                    LEFT JOIN LATERAL (
                        SELECT
                            occurrence_data.data_value AS "contactPerson"
                        FROM
                            experiment.occurrence_data occurrence_data
                        LEFT JOIN
                            master.variable variable
                        ON
                            variable.id = occurrence_data.variable_id
                        WHERE
                            variable.abbrev = 'CONTCT_PERSON_CONT'
                            AND variable.is_void = FALSE
                            AND occurrence_data.is_void = FALSE
                            AND occurrence_data.occurrence_id = occurrence.id
                    ) cp ON TRUE
                    LEFT JOIN LATERAL (
                        SELECT
                            field.geospatial_object_code AS "fieldCode",
                            field.geospatial_object_name AS "field"
                        FROM
                            place.geospatial_object field
                        WHERE
                            is_void = FALSE
                            AND field.id = occurrence.field_id
                        LIMIT 1
                    ) field ON TRUE                    
                WHERE
                    occurrence.id = ${occurrenceDbId}
        `

        let additionCtrSource = ''
        let additionalSource = ''
        let entityCountAlias = 'occurrence'

        // Get all occurrence data-specific variables
        let conditionStr = `
        WHERE 
            occurrence.id = ${occurrenceDbId} AND
            variable.usage IN ('experiment_manager', 'occurrence') AND
            variable.abbrev NOT IN ('MANAGEMENT_PROTOCOL_LIST_ID','TRAIT_PROTOCOL_LIST_ID')
        ORDER BY
            variable.abbrev`

        dataVariableInfo = await occurrenceHelper.getCrossTabColumns(
            'occurrence',
            conditionStr,
            res
        )

        occurrenceDataQueryFields = 'occurrence.*'

        variableDbIds = dataVariableInfo.ids
        variableCrossTabCols = dataVariableInfo.columns

        // If this occurrence has management protocol data
        if (variableDbIds && variableCrossTabCols) {
            // Rename to OCCURRENCE_ECOSYSTEM to avoid overlapping with another ECOSYSTEM i.e. EXPERIMENT_ECOSYSTEM
            if (variableCrossTabCols.includes('ECOSYSTEM')) variableCrossTabCols = variableCrossTabCols.replace('ECOSYSTEM', 'OCCURRENCE_ECOSYSTEM')

            // Append to query
            additionCtrSource += `
                , occurrence_data AS (
                    SELECT 
                        *
                    FROM
                        CROSSTAB(
                        '
                            SELECT 
                                occurrence_data.occurrence_id AS "occurrenceDbId",
                                occurrence_data.variable_id,
                                occurrence_data.data_value
                            FROM
                                experiment.occurrence_data occurrence_data
                            WHERE
                                occurrence_data.occurrence_id = ${occurrenceDbId}
                                AND occurrence_data.is_void = FALSE
                            ORDER BY
                                occurrence_data.id
                        ',
                        ' SELECT UNNEST(ARRAY[${variableDbIds}]) '             
                    ) AS (
                        occurrence_id integer,
                        ${variableCrossTabCols}
                    )
                )
            `
            additionalSource += `
                LEFT JOIN occurrence_data ON occurrence_data.occurrence_id = occurrence."occurrenceDbId"
            `

            occurrenceDataQueryFields += ', occurrence_data.*' 
        }

        if (hasSelectedEntities && selectedEntities.indexOf('experiment') > -1) {
            let experimentQuery = await occurrenceHelper.getExperimentInfoQuery(
                'occurrence',
                ` AND experiment.id = occurrence."experimentDbId" `
            )

            additionCtrSource += `
                , experiment AS (
                    ${experimentQuery}                    
                )
            `
            additionalSource += `
                JOIN experiment ON experiment."experimentDbId" = occurrence."experimentDbId"
            `
            if(occurrenceDataQueryFields != '') {
                occurrenceDataQueryFields += `,`
            }

            occurrenceDataQueryFields += 'experiment.*'

            // Get all experiment data-specific variables
            const conditionStr = `
                WHERE 
                    experiment.id = (SELECT experiment_id FROM experiment.occurrence WHERE id = ${occurrenceDbId}) AND
                    variable.usage = 'experiment_manager' AND
                    variable.abbrev NOT IN ('MANAGEMENT_PROTOCOL_LIST_ID','TRAIT_PROTOCOL_LIST_ID')
                ORDER BY
                    variable.abbrev`

            dataVariableInfo = await occurrenceHelper.getCrossTabColumns(
                'experiment',
                conditionStr,
                res
            )

            variableDbIds = dataVariableInfo.ids
            variableCrossTabCols = dataVariableInfo.columns

            // If this occurrence has management protocol data
            if (variableDbIds && variableCrossTabCols) {
                // Rename to EXPERIMENT_ECOSYSTEM to avoid overlapping with another ECOSYSTEM i.e. OCCURRENCE_ECOSYSTEM
                if (variableCrossTabCols.includes('ECOSYSTEM')) variableCrossTabCols = variableCrossTabCols.replace('ECOSYSTEM', 'EXPERIMENT_ECOSYSTEM')

                // Append to query
                additionCtrSource += `
                    , experiment_data AS (
                        SELECT 
                            *
                        FROM
                            CROSSTAB(
                            '
                                SELECT 
                                    experiment.id AS "experimentDbId",
                                    experiment_data.variable_id,
                                    experiment_data.data_value
                                FROM
                                    experiment.experiment_data experiment_data
                                LEFT JOIN
                                    experiment.experiment ON experiment.id = experiment_data.experiment_id
                                JOIN
                                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                                WHERE
                                    experiment_data.experiment_id = (SELECT experiment_id FROM experiment.occurrence WHERE id = ${occurrenceDbId})
                                    AND experiment_data.is_void = FALSE
                                ORDER BY
                                    experiment_data.id
                            ',
                            ' SELECT UNNEST(ARRAY[${variableDbIds}]) '             
                        ) AS (
                            "experimentDbId" integer,
                            ${variableCrossTabCols}
                        )
                    )
                `
                additionalSource += `
                    LEFT JOIN experiment_data ON experiment_data."experimentDbId" = experiment."experimentDbId"
                `

                occurrenceDataQueryFields += `, experiment_data.*`
            }
        }

        if (hasSelectedEntities && selectedEntities.indexOf('planting_protocol') > -1) {
            /* BEGIN Retrieval of protocolDbId 
            *  This is to retrieve only planting protocol-specific variables in this code block
            */

            // Retrieve experiment code
            const experimentCodeQuery = `
                SELECT
                    experiment_code AS "experimentCode"
                FROM
                    experiment.experiment experiment
                JOIN
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                WHERE
                    occurrence.is_void = FALSE AND
                    occurrence.id = ${occurrenceDbId}`

            let queryResult = await sequelize.query(
                experimentCodeQuery,
                {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            const protocolCode = `PLANTING_PROTOCOL_${queryResult[0]['experimentCode']}`

            // Retrieve protocol ID
            const protocolDbIdQuery = `
                SELECT
                    id AS "protocolDbId"
                FROM
                    tenant.protocol
                WHERE 
                    protocol_code = $$${protocolCode}$$`


            queryResult = await sequelize.query(
                protocolDbIdQuery,
                {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            const protocolDbId = (queryResult.length > 0) ? queryResult[0]['protocolDbId'] : null
            /* END Retrieval of protocolDbId */

            if (protocolDbId) {
                // Get all planting protocol-specific variables
                const conditionStr = `
                    WHERE 
                        occurrence.id = ${occurrenceDbId} AND
                        occurrence_data.protocol_id = ${protocolDbId} AND
                        variable.abbrev NOT IN ('MANAGEMENT_PROTOCOL_LIST_ID','TRAIT_PROTOCOL_LIST_ID')
                    ORDER BY
                        variable.abbrev`
    
                dataVariableInfo = await occurrenceHelper.getCrossTabColumns(
                    'occurrence',
                    conditionStr,
                    res
                )
    
                variableDbIds = dataVariableInfo.ids
                variableCrossTabCols = dataVariableInfo.columns
    
                // If this occurrence has planting protocol data
                if (variableDbIds && variableCrossTabCols) {
                    // Append to query
                    additionCtrSource += `
                        , planting_protocol AS (
                            SELECT 
                                *
                            FROM
                                CROSSTAB(
                                '
                                    SELECT 
                                        occurrence_data.occurrence_id AS "occurrenceDbId",
                                        occurrence_data.variable_id,
                                        occurrence_data.data_value
                                    FROM
                                        experiment.occurrence_data occurrence_data
                                    WHERE
                                        occurrence_data.occurrence_id = ${occurrenceDbId}
                                        AND occurrence_data.is_void = FALSE
                                    ORDER BY
                                        occurrence_data.id
                                ',
                                ' SELECT UNNEST(ARRAY[${variableDbIds}]) '
                            ) AS (
                                "occurrenceDbId" integer,
                                ${variableCrossTabCols}
                            )
                        )
                    `
                    additionalSource += `
                        LEFT JOIN planting_protocol ON planting_protocol."occurrenceDbId" = occurrence."occurrenceDbId"
                    `

                    if(occurrenceDataQueryFields != '') {
                        occurrenceDataQueryFields += `,`
                    }
                    
                    occurrenceDataQueryFields += `planting_protocol.*`
                }
            }
        }

        if (hasSelectedEntities && selectedEntities.indexOf('management_protocol') > -1) {
            // Get all management protocol-/data-specific variables
            const conditionStr = `
                WHERE 
                    occurrence.id = ${occurrenceDbId} AND
                    variable.usage = 'management' AND
                    variable.abbrev NOT IN ('MANAGEMENT_PROTOCOL_LIST_ID','TRAIT_PROTOCOL_LIST_ID')
                ORDER BY
                    variable.abbrev`

            dataVariableInfo = await occurrenceHelper.getCrossTabColumns(
                'occurrence',
                conditionStr,
                res
            )

            variableDbIds = dataVariableInfo.ids
            variableCrossTabCols = dataVariableInfo.columns

            // If this occurrence has management protocol data
            if (variableDbIds && variableCrossTabCols) {
                // Append to query
                additionCtrSource += `
                    , management_protocol AS (
                        SELECT 
                            *
                        FROM
                            CROSSTAB(
                            '
                                SELECT 
                                    occurrence_data.occurrence_id AS "occurrenceDbId",
                                    occurrence_data.variable_id,
                                    occurrence_data.data_value
                                FROM
                                    experiment.occurrence_data occurrence_data
                                WHERE
                                    occurrence_data.occurrence_id = ${occurrenceDbId}
                                    AND occurrence_data.is_void = FALSE
                                ORDER BY
                                    occurrence_data.id
                            ',
                            ' SELECT UNNEST(ARRAY[${variableDbIds}]) '             
                        ) AS (
                            "occurrenceDbId" integer,
                            ${variableCrossTabCols}
                        )
                    )
                `
                additionalSource += `
                    LEFT JOIN management_protocol ON management_protocol."occurrenceDbId" = occurrence."occurrenceDbId"
                `

                if(occurrenceDataQueryFields != '') {
                    occurrenceDataQueryFields += `,`
                }
                
                occurrenceDataQueryFields += `management_protocol.*`
            }
        }

        if (hasSelectedEntities && (
            selectedEntities.indexOf('entry') > -1 || 
            selectedEntities.indexOf('germplasm') > -1 || 
            selectedEntities.indexOf('plot') > -1 ||
            selectedEntities.indexOf('trait') > -1)) {
            
            let entryQuery = await occurrenceHelper.getEntryInfoQuery(
                'occurrence',
                occurrenceDbId
            )

            additionCtrSource += `
                , entry AS (
                    ${entryQuery}                    
                )
            `
            
            additionalSource += `
                JOIN entry ON entry."occurrenceDbId" = occurrence."occurrenceDbId"
            `
        
            entityCountAlias = 'entry'

            if(selectedEntities.indexOf('entry') > -1){
                if(occurrenceDataQueryFields != '') {
                    occurrenceDataQueryFields += `,`
                }
                
                occurrenceDataQueryFields += `entry.*`
            }
        }

        if (hasSelectedEntities && selectedEntities.indexOf('germplasm') > -1) {
            let germplasmQuery = await occurrenceHelper.getGermplasmInfoQuery('entry')

            additionCtrSource += `
                , germplasm AS (
                    ${germplasmQuery}                    
                )
            `
            additionalSource += `
                JOIN germplasm ON germplasm."germplasmDbId" = entry."germplasmDbId"
            `

            if(occurrenceDataQueryFields != '') {
                occurrenceDataQueryFields += `,`
            }
            
            occurrenceDataQueryFields += `germplasm.*`

            /// Retrieve cross tab columns
            if(experimentDbId != 0){
                conditionStr = `
                        JOIN experiment.entry entry 
                            ON entry.germplasm_id = germplasm.id
                            AND entry.is_void = FALSE
                        JOIN experiment.entry_list entry_list 
                            ON entry_list.id = entry.entry_list_id
                            AND entry_list.experiment_id = ${experimentDbId}
                            AND entry_list.is_void = FALSE
                `;

                let schema = 'germplasm';
                let ext = 'attribute';
                dataVariableInfo = await occurrenceHelper.getCrossTabColumns('germplasm',conditionStr,res,schema,ext);

                variableDbIds = dataVariableInfo.ids ?? null;
                variableCrossTabCols = dataVariableInfo.columns ?? null;
                variableSelectCols = dataVariableInfo.selectColumns ?? null;

                if (variableDbIds != null && variableCrossTabCols != null){
                    // retrieve DATA_QC_CODE values
                    dataQcCols = await occurrenceHelper.getVarQcCodeColumns(
                        variableDbIds,
                        variableSelectCols,
                        'germplasm_data'
                    )

                    dataQcSelect = dataQcCols.select ?? ""
                    dataQcQuery = dataQcCols.query ?? ""
                    dataQcStm = dataQcCols.condition ?? ""

                    let sourceQuery = await occurrenceHelper.getGermplasmDataInfo(
                        dataQcSelect,
                        (`${variableSelectCols},${dataQcSelect}`),
                        variableDbIds,
                        variableCrossTabCols,
                        dataQcStm,
                        experimentDbId)

                    // Append to query
                    additionCtrSource += sourceQuery;

                    additionalSource += `
                        LEFT JOIN germplasm_trait_data ON germplasm_trait_data."germplasmDbId" = germplasm."germplasmDbId"`

                    entityCountAlias = 'germplasm'

                    if(occurrenceDataQueryFields != '') {
                        occurrenceDataQueryFields += `
                        ,`
                    }
                    
                    occurrenceDataQueryFields += `germplasm_trait_data.*`
                }

            }
        }

        if (hasSelectedEntities && (selectedEntities.indexOf('plot') > -1 || selectedEntities.indexOf('trait') > -1)) {
            dataVariableInfo = null
            variableDbIds = null
            variableCrossTabCols = ''
            variableSelectCols = ''
            includePlot = true // If plot is included in entity, set to true
            
            let dataQcCols = []

            let dataQcSelect = ''
            let dataQcQuery = ''
            let dataQcStm = ''

            let plotQuery = await occurrenceHelper.getPlotInfoQuery(
                'entry',
                ` AND plot.entry_id = entry."entryDbId" 
                WHERE plot.occurrence_id = ${occurrenceDbId}`
            )

            additionCtrSource += `
                , plot AS (
                    ${plotQuery}                    
                )
            `
            additionalSource += `
                LEFT JOIN plot ON plot."entryDbId" = entry."entryDbId"
            `

            entityCountAlias = 'plot'

            if(occurrenceDataQueryFields != '') {
                occurrenceDataQueryFields += `,`
            }
            
            occurrenceDataQueryFields += `plot.*`

            if (selectedEntities.indexOf('trait') > -1) {
                // Get all trait protocol-/data-specific variables
                const conditionStr = `
                    WHERE 
                        plot.occurrence_id = ${occurrenceDbId} AND
                        variable.abbrev NOT IN ('MANAGEMENT_PROTOCOL_LIST_ID','TRAIT_PROTOCOL_LIST_ID')`
    
                dataVariableInfo = await occurrenceHelper.getCrossTabColumns(
                    'plot',
                    conditionStr,
                    res
                )

                variableDbIds = dataVariableInfo.ids
                variableCrossTabCols = dataVariableInfo.columns
                variableSelectCols = dataVariableInfo.selectColumns

                dataQcCols = await occurrenceHelper.getVarQcCodeColumns(
                    variableDbIds,
                    variableSelectCols
                )
                
                dataQcSelect = dataQcCols.select ? (',' + dataQcCols.select) : ''
                dataQcQuery = dataQcCols.query ?? ''
                dataQcStm = dataQcCols.condition
            }

            let traitDataValueQuery = `plot_data.data_value`;
            if(replaceTraitDataValue == "true"){
                traitDataValueQuery = `
                    ( CASE 
                        WHEN plot_data.data_qc_code IN ('M','B','S') THEN 'NA' ELSE plot_data.data_value                                         
                    END )
                `;
            }

            // If this occurrence has trait data collected
            if (variableDbIds && variableCrossTabCols) {
                
                let plotSelectColumns = `
                    plot.entry_id AS "entryDbId",
                    plot.plot_number AS "plotNumber",
                    plot.plot_code AS "plotCode",
                    plot.plot_type AS "plotType",
                    plot.rep AS "rep",
                    plot.pa_x AS "paX",
                    plot.pa_y AS "paY",
                    plot.block_number AS "blockNumber",`

                // Append to query
                let plotTaitDataCTE = await occurrenceHelper.getPlotTraitDataAndQc(
                    occurrenceDbId,
                    `
                    ${variableSelectCols}
                    ${dataQcSelect}
                    `,
                    dataQcStm,
                    variableDbIds,
                    variableCrossTabCols,
                    traitDataValueQuery);

                additionCtrSource += `, ${plotTaitDataCTE}`;

                additionalSource += `
                    LEFT JOIN plot_trait_data ON plot_trait_data."traitPlotDbId" = plot."plotDbId"`

                entityCountAlias = 'plot'

                if(occurrenceDataQueryFields != '') {
                    occurrenceDataQueryFields += `,`
                }
                
                occurrenceDataQueryFields += `plot_trait_data.*`
            } 
        }

        // If plot is included in entity, add it as secondary sort parameter
        if(includePlot){
            orderString += `, "plotNumber" ASC`
        }

        // Addition for next sprint 23.11
        if(parameters['fields'] != null){
            occurrenceDataQueryFields = knex.column(parameters['fields'].split('|'))
        }
        else{
            occurrenceDataQueryFields = `
                ,all_data AS (
                    SELECT
                        ${occurrenceDataQueryFields}
                    FROM
                        occurrence
                        ${additionalSource}
                    ${orderString}
                )
            `
        }

        let countQuery = await occurrenceHelper.getResultTotalCount('all_data')

        let baseQuery = `
                WITH occurrence AS (
                    ${occurrenceInfoQuery}
                )
                ${additionCtrSource}
                ${occurrenceDataQueryFields}
                ${countQuery}
                SELECT 
					all_data.*,
					total_count.* 
				FROM 
					all_data,
					total_count
                LIMIT (:limit) OFFSET (:offset)
            `

        let occurrenceData = await sequelize
            .query(
                baseQuery,
                {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        limit: limit,
                        offset: offset,
                    }
                })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)

                return undefined
            })

        if (await occurrenceData == undefined || await occurrenceData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = occurrenceData[0].totalCount ?? 0

        res.send(200, {
            rows: occurrenceData,
            count: count
        })
    }
}