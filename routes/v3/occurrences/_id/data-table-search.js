/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let searchHelper = require('../../../../helpers/queryTool/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let validator = require('validator')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}
        let condition = ''
        let occurrenceDbId = req.params.id
        
        // Check if Occurrence ID is a valid ID
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if occurrence exists
        let occurrenceQuery = `
            SELECT
                count(1)
            FROM
                experiment.occurrence occurrence
            WHERE
                id = ${occurrenceDbId} AND
                is_void = FALSE
        `

        let occurrence = await sequelize.query(occurrenceQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (await occurrence == undefined || await occurrence.length < 0) {
            let errMsg = 'The occurrence ID you have provided does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get all the Occurrence Data variables
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') AS "variableId",
                STRING_AGG( '"' || abbrev || '"', ',') AS "selectColumns",
                STRING_AGG( abbrev, ',') AS "variableAbbrev",
                STRING_AGG( '"' || abbrev || '"' || ' jsonb', ',') AS "crosstabColumns"
            FROM(
                SELECT 
                    distinct (occurrence_data.variable_id) AS variable_id,
                    v.abbrev
                FROM
                    experiment.occurrence_data occurrence_data
                LEFT JOIN
                    experiment.occurrence
                ON
                    occurrence.id = occurrence_data.occurrence_id AND
                    occurrence.is_void = FALSE
                LEFT JOIN
                    master.variable v
                ON
                    v.id = occurrence_data.variable_id
                WHERE
                    occurrence_data.occurrence_id = ${occurrenceDbId} AND
                    occurrence_data.is_void = FALSE
                ORDER BY
                    v.abbrev
            )a
        `

        let variables = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        // Process the variable information
        let variableId = variables[0]['variableId'] == null ? [0] : variables[0]['variableId']
        let crosstabColumns = variables[0]['crosstabColumns']
        let selectColumns = variables[0]['selectColumns']

        let variableAbbrev = variables[0]['variableAbbrev'] == null ? [] : variables[0]['variableAbbrev'].split(',')

        if (crosstabColumns != "" && crosstabColumns != null) {
            crosstabColumns = ',' + crosstabColumns
        } else if (crosstabColumns == null) {
            crosstabColumns = ', "null" text';
        }
        if (selectColumns != "" && selectColumns != null) {
            selectColumns = ',' + selectColumns
        } else if (selectColumns == null) {
            selectColumns = ''
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set columns to be excluded in parameters for filtering
            Array.prototype.diff = function (a) {
                return this.filter(function(i) {
                    return a.indexOf(i) < 0
                })
            }

            // Exclude all measurement variables
            let excludedParametersArray = []

            excludedParametersArray.push(
                'fields',
                'dataQcCode',
                'dataValue',
            )

            for (abbrev of variableAbbrev) {
                excludedParametersArray.push(abbrev)
            }

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            conditionArray = []

            // Get filters for Occurrence Data with values that is in JSON format
            for (metadata in req.body) {
                if (variableAbbrev.includes(metadata)) {
                    for (field in req.body[metadata]) {
                        condition = ''
                        if (field == 'dataValue') {
                            let variableQuery = `
                                SELECT 
                                    data_type
                                FROM
                                    master.variable v
                                WHERE
                                    v.abbrev='${metadata}'
                            `
                            let variableList = await sequelize.query(variableQuery, {
                                type: sequelize.QueryTypes.SELECT
                            })
                            let variableDataType = variableList[0]['data_type']
                            value = req.body[metadata]['dataValue'];
                            column = '("' + metadata + '"->>' + "'dataValue')::" + variableDataType
                            condition = searchHelper.getFilterCondition(value, column, columnCast = true)
                        }

                        if (condition != '') {
                            conditionArray.push(`(` + condition + ')')
                        }
                    }
                }
            }

            if (conditionArray.length > 0) {
                condition = conditionArray.join(' AND ')
                if (conditionString == '') {
                    conditionString = conditionString +
                        ` WHERE `
                        + condition
                } else {
                    conditionString = conditionString +
                        ` AND `
                        + condition
                }
            }

            if (conditionString.includes('You have provided invalid values for filter.')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let occurrenceDataQuery = null
        let fromQuery = `
            FROM
                experiment.experiment experiment,
                experiment.occurrence occurrence
            LEFT JOIN
                (
                    SELECT 
                        "occurrenceDbId" 
                        ${selectColumns}
                    FROM
                        CROSSTAB(
                            '
                                SELECT 
                                    occurrence.id AS "occurrenceDbId",
                                    occurrence_data.variable_id,
                                    jsonb_build_object(
                                        ''dataValue'', occurrence_data.data_value,
                                        ''dataQCCode'', occurrence_data.data_qc_code,
                                        -- This is specific to Occurrence Data tables search
                                        ''protocolDbId'', occurrence_data.protocol_id
                                    )
                                FROM
                                    experiment.occurrence occurrence
                                LEFT JOIN
                                    experiment.occurrence_data
                                ON
                                    occurrence.id = occurrence_data.occurrence_id
                                WHERE
                                    occurrence.id = ${occurrenceDbId} AND
                                    occurrence_data.is_void = FALSE AND
                                    occurrence.is_void = FALSE
                                ORDER BY
                                    occurrence.id
                            ',
                            '
                                SELECT UNNEST(ARRAY[${variableId}])
                            '
                        ) as (
                            "occurrenceDbId" integer
                            ${crosstabColumns}
                        ) 
                )crosstab ON crosstab."occurrenceDbId" = occurrence.id
            LEFT JOIN
                experiment.location_occurrence_group log
            ON
                log.occurrence_id = occurrence.id
            LEFT JOIN
                experiment.location location
            ON
                location.id = log.location_id
            LEFT JOIN
                place.geospatial_object site
            ON
                site.id = occurrence.site_id
            LEFT JOIN
                place.geospatial_object field
            ON
                field.id = occurrence.field_id
            LEFT JOIN 
                tenant.person creator
            ON
                creator.id = occurrence.creator_id::integer
            LEFT JOIN 
                tenant.person modifier
            ON
                modifier.id = occurrence.modifier_id::integer
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId} AND
                experiment.id = occurrence.experiment_id AND
                experiment.is_void = FALSE
            ORDER BY
                occurrence.id
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            occurrenceDataQuery = knex.column(parameters['fields'].split('|'))
            occurrenceDataQuery += fromQuery
        } else {
            occurrenceDataQuery = `
                SELECT
                    -- Retrieve Occurrence info
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_name AS "occurrence",
                    occurrence.occurrence_status AS "occurrenceStatus",

                    -- Retrieve Parent Experiment info
                    experiment.id AS "experimentDbId",
                    experiment.experiment_code AS "experimentCode",
                    experiment.experiment_name AS "experiment",

                    -- Retrieve Site code of this Occurrence
                    site.id AS "siteDbId",
                    site.geospatial_object_code AS "siteCode",

                    -- Retrieve Field code of this Occurrence
                    field.id AS "fieldDbId",
                    field.geospatial_object_code AS "fieldCode",

                    -- Retrieve Location name of this Occurrence
                    location.id AS "locationDbId",
                    location.location_name AS "location",

                    occurrence.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    occurrence.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier
                    ${selectColumns}
                ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 'You have provided an invalid format for orderString.')
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let occurrenceDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            occurrenceDataQuery,
            conditionString,
            orderString,
        )

        // Retrieve the occurrenceData records
        let occurrenceData = await sequelize
            .query(occurrenceDataFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return     
            })

        if (await occurrenceData == undefined || await occurrenceData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }
        // Get the final count of the occurrenceData records retrieved
        let occurrenceDataCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                occurrenceDataQuery,
                conditionString,
                orderString,
            )

        let occurrenceDataCount = await sequelize
            .query(
                occurrenceDataCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = occurrenceDataCount[0].count

        res.send(200, {
            rows: occurrenceData,
            count: count
        })
    }
}