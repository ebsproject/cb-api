/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let occurrenceHelper = require('../../../../helpers/occurrence/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')

module.exports = {
    // Endpoint for retrieving crosses and cross data
    // POST /v3/occurrences/:id/harvest-cross-data-search
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0

        let orderString = ''
        let addedOrderString = ` ORDER BY "crossDbId" `
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let isBasic = req.params.isBasic != null && req.params.isBasic.toLowerCase() == 'true'

        let attributes = ["crossDbId", "crossName", "crossMethod",
            "crossMethodAbbrev", "remarks", "isMethodAutofilled", "harvestStatus",
            "germplasmDbId", "germplasmDesignation", "germplasmParentage",
            "germplasmGeneration", "germplasmType", "cropDbId", "cropCode",
            "entryDbId", "entryNo", "entryName", "entryRole", "experimentDbId",
            "experimentName", "experimentCode", "experimentDataProcessId",
            "crossFemaleParent", "femaleParentSeedSource", "crossMaleParent",
            "maleParentSeedSource", "femaleSourceEntryDbId", "femaleSourceEntry",
            "femaleSourceSeedName", "femaleSourcePlotDbId", "femaleSourcePlot",
            "femaleParentage", "maleSourceEntryDbId", "maleSourceEntry",
            "maleSourceSeedName", "maleSourcePlotDbId", "maleSourcePlot",
            "maleParentage", "harvestDate", "harvestMethod", "crossingDate",
            "noOfSeed", "noOfPlants", "specificPlantNo", "noOfPanicle", "noOfEar",
            "terminalHarvestDate", "terminalHarvestMethod", "terminalCrossingDate",
            "terminalNoOfSeed", "terminalNoOfPlants", "terminalSpecificPlantNo",
            "terminalNoOfPanicle", "terminalNoOfEar", "creationTimestamp",
            "creatorDbId", "creator", "modificationTimestamp", "modifierDbId",
            "modifier",]
        
        let creatorId = await tokenHelper.getUserId(req)
        
        // Get plot data creator ID
        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            return res.send(new errors.BadRequestError(errMsg))
        }

        let occurrenceDbId = req.params.id

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer.'
            return res.send(new errors.BadRequestError(errMsg))
        }

        let occurrenceQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.occurrence_status AS "occurrenceStatus"
            FROM
                experiment.occurrence occurrence
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
        `

        // Find occurrence in the database
        let occurrence = await sequelize
            .query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        // If the occurrence does not exist, return an error
        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = 'Resource not found. The occurrence you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                let distinctOrder = await processQueryHelper.getDistinctOrderString(parameters['distinctOn']) 
                addedOrderString = `
                    ORDER BY
                        ${distinctOrder}
                `
            }

            if (req.body.sort != undefined) {
                if (typeof req.body.sort == 'object') {
                    if (
                        req.body.sort.attribute != undefined
                        && req.body.sort.sortValue != undefined
                        && req.body.sort.attribute.trim() != ""
                        && req.body.sort.sortValue.trim() != ""
                        && attributes.includes(req.body.sort.attribute.trim())
                    ) {
                        orderString = await processQueryHelper.getOrderStringBySortValue(req.body.sort)
                    }
                }
            }
            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
                'sort'
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
                "||"
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                return res.send(new errors.BadRequestError(errMsg))
            }
        }

        // Build base query
        let crossesQuery = null

        // test for helper functions
        let selectStr = `
            "germplasmCross".id AS "crossDbId",
            "germplasmCross".cross_name AS "crossName",
            "germplasmCross".cross_method AS "crossMethod",
            "germplasmCross".remarks,
            "germplasmCross".is_method_autofilled AS "isMethodAutofilled",
            "germplasmCross".harvest_status AS "harvestStatus",
            "crossMethodAbbrev".abbrev AS "crossMethodAbbrev",
            germplasm.id AS "germplasmDbId",
            germplasm.designation AS "germplasmDesignation",
            germplasm.parentage AS "germplasmParentage",
            germplasm.generation AS "germplasmGeneration",
            "crossFemaleAndMaleParent".germplasm_state AS "state",
            "crossFemaleAndMaleParent".germplasm_type AS "type",
            crop.id AS "cropDbId",
            crop.crop_code AS "cropCode",
            entry.id AS "entryDbId",
            entry.entry_number AS "entryNo",
            entry.entry_name AS "entryName",
            entry.entry_role AS "entryRole",
            experiment.id AS "experimentDbId",
            experiment.experiment_name AS "experimentName",
            experiment.experiment_code AS "experimentCode",
            experiment.data_process_id AS "experimentDataProcessId",
            occurrence.id AS "occurrenceDbId",
            occurrence.occurrence_name AS "occurrenceName",
            occurrence.occurrence_code AS "occurrenceCode", 
            "germplasmCross".creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS "creator",
            "germplasmCross".modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS "modifier"
        `
        let allResAlias = 'baseQuery'
        let filteredResAlias = 'withFilters'

        // base query column aliases
        let columnAliasRefTable = [
            'crossDbId','crossName','crossMethod','remarks','isMethodAutofilled','harvestStatus',
            'crossMethodAbbrev', 'germplasmDbId', 'germplasmDesignation', 'germplasmParentage',
            'germplasmGeneration','state', 'type', 'cropDbId', 'cropCode', 'entryDbId', 'entryNo',
            'entryName','entryRole', 'experimentDbId', 'experimentName', 'experimentCode', 'modifier',
            'experimentDataProcessId', 'occurrenceDbId', 'occurrenceName', 'occurrenceCode',
            'creationTimestamp', 'creatorDbId', 'creator', 'modificationTimestamp', 'modifierDbId'
        ]

        // add occurrenceDbId in fields parameter
        if(parameters['fields']){
            parameters['fields'] += '|occurrence.id AS occurrenceDbId'
        }

        allResAlias = 'withOrderNumber'
        let finalSelectStr = `${selectStr}`

        // Build basic harvest data CTE
        let harvestDataCte = `
            ,withHarvestData AS (
                select
                    baseQuery.*
                from
                    baseQuery
            )
        `
        // If isBasic flag is set to FALSE, rebuild harvest data CTE
        if (!isBasic) {
            // Get supported variables
            let variables = await occurrenceHelper.getHMSupportedVariables("cross")

            // Rebuild harvest data CTE
            harvestDataCte = `
                ,withHarvestData AS (
                    select
                        baseQuery.*,
                        ${variables.committedDataSelect.join(',')},
                        ${variables.terminalDataSelect.join(',')}
                    from
                        baseQuery
                    left join (
                        select
                            "crossDbId",
                            ${variables.committedData.join(',')}
                        from
                            crosstab(
                                '
                                    SELECT
                                        DISTINCT ON (gc.id, cd.variable_id)
                                        gc.id AS "crossDbId",
                                        cd.variable_id,
                                        cd.data_value
                                    FROM
                                        germplasm.cross gc
                                    LEFT JOIN
                                        germplasm.cross_data cd ON gc.id = cd.cross_id
                                        AND cd.is_void = FALSE
                                    WHERE
                                        gc.occurrence_id = ${occurrenceDbId}
                                    GROUP BY
                                        gc.id,
                                        cd.variable_id,
                                        cd.data_value,
                                        cd.id
                                    ORDER BY
                                        gc.id ASC, cd.variable_id ASC, cd.id DESC;
                                ',
                                'SELECT UNNEST(ARRAY[${variables.variableIds}]);'
                            ) as (
                                "crossDbId" integer,
                                ${variables.committedDataAs.join(',')}
                            ) 
                    ) committedData ON committedData."crossDbId" = baseQuery."crossDbId"
                    left join (
                        select
                            "crossDbId",
                            ${variables.terminalData.join(',')}
                        from
                            crosstab(
                                '
                                    SELECT
                                        gc.id AS "crossDbId",
                                        td.variable_id,
                                        td.value
                                    FROM
                                        germplasm.cross gc
                                    LEFT JOIN
                                        data_terminal.transaction_dataset td 
                                        ON td.data_unit = ''cross''
                                        AND gc.id = td.entity_id
                                        AND td.is_void = FALSE
                                    WHERE
                                        gc.occurrence_id = ${occurrenceDbId}
                                        AND td.creator_id = ${creatorId}
                                    ORDER BY
                                        gc.id;
                                ',
                                'SELECT UNNEST(ARRAY[${variables.variableIds}]);'
                            ) as (
                                "crossDbId" integer,
                                ${variables.terminalDataAs.join(',')}
                            ) 
                    ) terminalData ON terminalData."crossDbId" = baseQuery."crossDbId"
                )

            `
        }

        // add female parent query
        let crossFemaleParentInfo = await occurrenceHelper.getCrossParentInfoQuery(
            'withHarvestData','withHarvestData','crossDbId','Female',['female','female-and-male'],parameters)

        let crossFemaleParentInfoQuery = crossFemaleParentInfo.sqlQuery
        let crossFemaleParentSelectStr = crossFemaleParentInfo.selectStr
        finalSelectStr = `${finalSelectStr},${crossFemaleParentSelectStr}`
        
        let crossFemaleParentInfoCte = `
            ,withFemaleParentInfo AS (
                ${crossFemaleParentInfoQuery}
            )
        `

        // add male parent query
        let crossMaleParentInfo = await occurrenceHelper.getCrossParentInfoQuery(
            'withFemaleParentInfo','withFemaleParentInfo','crossDbId','Male',['male','female-and-male'],parameters)

        let crossMaleParentInfoQuery = crossMaleParentInfo.sqlQuery
        let crossMaleParentSelectStr = crossMaleParentInfo.selectStr
        finalSelectStr = `${finalSelectStr},${crossMaleParentSelectStr}`

        let crossMaleParentInfoCte = `
            ,withMaleParentInfo AS (
                ${crossMaleParentInfoQuery}
            )
        `

        // add count query to retrieve total records count
        let countQueryCte = `
            ,countQuery as (
                SELECT
                    ${filteredResAlias}."occurrenceDbId",
                    count (1) as "totalCount"
                FROM
                    ${filteredResAlias}
                GROUP BY ${filteredResAlias}."occurrenceDbId"
            )
        `

        // build new query
        let newBaseQuery = `
            SELECT
                ${selectStr}
            FROM
                experiment.occurrence occurrence
                JOIN germplasm.cross "germplasmCross" 
                    ON occurrence.id = "germplasmCross".occurrence_id AND "germplasmCross".is_void = FALSE
                INNER JOIN LATERAL (
                    SELECT
                        log.id,
                        log.location_id,
                        location.id
                    FROM
                        experiment.location_occurrence_group log
                        JOIN experiment.location location
                            ON location.id = log.location_id AND location.is_void = FALSE
                    WHERE
                        log.is_void = FALSE AND log.occurrence_id = occurrence.id
                    LIMIT 1
                ) log ON TRUE
                INNER JOIN LATERAL (
                    SELECT
                        experiment.id,
                        experiment.experiment_name,
                        experiment.experiment_code,
                        experiment.data_process_id,
                        experiment.program_id
                    FROM
                        experiment.experiment experiment
                    WHERE
                        experiment.is_void = FALSE
                        AND occurrence.experiment_id = experiment.id
                    LIMIT 1
                ) experiment ON TRUE
                JOIN tenant.program program 
                    ON program.id = experiment.program_id AND program.is_void = FALSE
                LEFT JOIN LATERAL (
                    SELECT
                        crop_program.crop_id
                    FROM
                        tenant.crop_program crop_program
                    WHERE
                        crop_program.is_void = FALSE
                        AND crop_program.id = program.crop_program_id
                    LIMIT 1
                ) cp ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        crop.id,
                        crop.crop_code
                    FROM
                        tenant.crop crop
                    WHERE
                        crop.is_void = FALSE
                        AND crop.id = cp.crop_id
                    LIMIT 1
                ) crop ON TRUE
                LEFT JOIN LATERAL (
                    SELECT 
                        sv.abbrev
                    FROM 
                        master.variable variable
                        JOIN master.scale_value sv 
                            ON sv.scale_id = variable.scale_id AND variable.abbrev = 'CROSS_METHOD' 
                            AND LOWER(sv.value) = LOWER("germplasmCross".cross_method)
                    LIMIT 1
                ) "crossMethodAbbrev" ON TRUE
                LEFT JOIN LATERAL ( -- for female-and-male parent
                    SELECT
                        germplasm.germplasm_state,
                        germplasm.germplasm_type
                    FROM
                        germplasm.cross_parent cross_parent
                        JOIN germplasm.germplasm germplasm 
                            ON germplasm.id = cross_parent.germplasm_id AND germplasm.is_void = FALSE
                    WHERE
                        cross_parent.is_void = FALSE
                        AND cross_parent.cross_id = "germplasmCross".id 
                        AND cross_parent.parent_role = 'female-and-male'
                ) "crossFemaleAndMaleParent" ON TRUE
                LEFT JOIN germplasm.germplasm germplasm 
                    ON "germplasmCross".germplasm_id = germplasm.id AND germplasm.is_void = FALSE
                LEFT JOIN germplasm.seed seed 
                    ON "germplasmCross".seed_id = seed.id AND seed.is_void = FALSE
                LEFT JOIN experiment.entry entry 
                    ON "germplasmCross".entry_id = entry.id AND entry.is_void = FALSE
                JOIN tenant.person creator ON "germplasmCross".creator_id = creator.id
                LEFT JOIN tenant.person modifier ON "germplasmCross".modifier_id = modifier.id
            WHERE
                occurrence.is_void = FALSE 
                AND occurrence.id = ${occurrenceDbId}
            ORDER BY "crossDbId"
        `

        // filter all accumulated results
        let filteredResSelectStr = `${allResAlias}.*`

        let referenceTable = []
        columnAliasRefTable = []
        // retrieve refTable for all CTE returns
        for(var col of finalSelectStr.split(",")){
            placeholder = col.split('AS')
            if(placeholder.length > 1){
                referenceTable[placeholder[0].replaceAll(`"`,'').trim()] = placeholder[1].replaceAll(`"`,'').trim()
                columnAliasRefTable.push(placeholder[1].replaceAll(`"`,'').trim())
            }
        }

        // Apply fields to filtered results query
        if(parameters['fields']){
            // retrieve final SELECT statements for returned values
            let newSelectStr = await occurrenceHelper.getColumnsParameter(parameters,referenceTable,allResAlias)
            filteredResSelectStr = newSelectStr != '' ? newSelectStr : filteredResSelectStr
        }

        // Apply DISTINCT ON clause
        let dinstintCteStr = ''
        if(parameters['distinctOn']){
            dinstintCteStr = await occurrenceHelper.getDistictParameter(parameters['distinctOn'],columnAliasRefTable)
        }

        let filteredResQueryCte = `
            ,${filteredResAlias} AS (
                SELECT  
                    ${addedDistinctString}
                    ${filteredResSelectStr}
                FROM
                    ${allResAlias}
                ${conditionString}
                ${addedOrderString}
            )
        `
        // previous query build process
        let crossParentsSourceQuery = `
            (
                SELECT g.designation
                FROM germplasm.germplasm g, germplasm.cross_parent cp
                WHERE cp.germplasm_id = g.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 1 AND
                    cp.is_void = FALSE AND 
                    g.is_void = FALSE 
            ) AS "crossFemaleParent",
            (
                SELECT g.id
                FROM germplasm.germplasm g, germplasm.cross_parent cp
                WHERE cp.germplasm_id = g.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 1 AND
                    cp.is_void = FALSE AND 
                    g.is_void = FALSE 
            ) AS "femaleGermplasmDbId",
            (
                SELECT s.seed_code 
                FROM germplasm.seed s, germplasm.cross_parent cp 
                WHERE cp.seed_id = s.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 1 AND
                    cp.is_void = FALSE AND 
                    s.is_void = FALSE 
            ) AS "femaleParentSeedSource",
            (
                SELECT g.designation
                FROM germplasm.germplasm g, germplasm.cross_parent cp
                WHERE cp.germplasm_id = g.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 2 AND
                    cp.is_void = FALSE AND 
                    g.is_void = FALSE
            ) AS "crossMaleParent",
            (
                SELECT g.id
                FROM germplasm.germplasm g, germplasm.cross_parent cp
                WHERE cp.germplasm_id = g.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 2 AND
                    cp.is_void = FALSE AND 
                    g.is_void = FALSE
            ) AS "maleGermplasmDbId",
            (
                SELECT s.seed_code 
                FROM germplasm.seed s, germplasm.cross_parent cp 
                WHERE cp.seed_id = s.id AND 
                    cp.cross_id = "germplasmCross".id AND 
                    cp.order_number = 2 AND
                    cp.is_void = FALSE AND 
                    s.is_void = FALSE
            ) AS "maleParentSeedSource",
        `

        let seedSourceQuery = `
            femaleEntryTbl."femaleSourceEntryDbId",
            femaleEntryTbl."femaleSourceEntry",
            femaleEntryTbl."femaleSourceSeedName",
            femalePlotTbl."femaleSourcePlotDbId",
            femalePlotTbl."femaleSourcePlot",
            femaleParentage."femaleParentage",
            femaleParentage."femaleGermplasmState",
            maleEntryTbl."maleSourceEntryDbId",
            maleEntryTbl."maleSourceEntry",
            maleEntryTbl."maleSourceSeedName",
            malePlotTbl."maleSourcePlotDbId",
            malePlotTbl."maleSourcePlot",
            maleParentage."maleParentage",
            maleParentage."maleGermplasmState",
        `

        let seedSourceInfoFromQuery = `
            LEFT JOIN
                (
                    SELECT
                        e.id AS "femaleSourceEntryDbId",
                        e.entry_code AS "femaleSourceEntry",
                        s.seed_name AS "femaleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role = 'female' AND
                        cross_parent.is_void = false AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS femaleEntryTbl ON femaleEntryTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        e.id AS "maleSourceEntryDbId",
                        e.entry_code AS "maleSourceEntry",
                        s.seed_name AS "maleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role = 'male' AND
                        cross_parent.is_void = FALSE AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS maleEntryTbl ON maleEntryTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        p.id AS "femaleSourcePlotDbId",
                        p.plot_code AS "femaleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role = 'female' AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE AND 
                        s.is_void = FALSE
                ) AS femalePlotTbl ON femalePlotTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        p.id AS "maleSourcePlotDbId",
                        p.plot_code AS "maleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role = 'male' AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE AND 
                        s.is_void = FALSE
                ) AS malePlotTbl ON malePlotTbl."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        g.parentage AS "femaleParentage",
                        cross_parent.cross_id AS "crossDbId",
                        g.germplasm_state AS "femaleGermplasmState"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        (cross_parent.parent_role = 'female' 
                            OR cross_parent.parent_role = 'female-and-male'
                        ) AND
                        g.is_void = FALSE
                ) AS femaleParentage ON femaleParentage."crossDbId" = "germplasmCross".id
            LEFT JOIN
                (
                    SELECT
                        g.parentage AS "maleParentage",
                        cross_parent.cross_id AS "crossDbId",
                        g.germplasm_state AS "maleGermplasmState"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role = 'male' AND
                        g.is_void = FALSE
                ) AS maleParentage ON maleParentage."crossDbId" = "germplasmCross".id
        `

        let fromQuery = `
            FROM
                germplasm.cross "germplasmCross"
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = "germplasmCross".occurrence_id AND occurrence.is_void = FALSE
            LEFT JOIN
                experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id AND log.is_void = FALSE
            LEFT JOIN
                experiment.experiment experiment ON occurrence.experiment_id = experiment.id AND experiment.is_void = FALSE
            LEFT JOIN
                tenant.program program ON program.id = experiment.program_id AND program.is_void = FALSE
            LEFT JOIN
                tenant.crop_program cp ON cp.id = program.crop_program_id AND cp.is_void = FALSE
            LEFT JOIN
                tenant.crop crop ON crop.id = cp.crop_id AND crop.is_void = FALSE
            LEFT JOIN
                germplasm.cross_parent ON cross_parent.cross_id = "germplasmCross".id AND cross_parent.parent_role = 'female-and-male' AND cross_parent.is_void = FALSE
            LEFT JOIN
                germplasm.germplasm g2 ON g2.id = cross_parent.germplasm_id AND g2.is_void = FALSE
            LEFT JOIN
                germplasm.germplasm germplasm ON "germplasmCross".germplasm_id = germplasm.id AND germplasm.is_void = FALSE
            LEFT JOIN
                germplasm.seed seed ON "germplasmCross".seed_id = seed.id AND seed.is_void = FALSE
            LEFT JOIN
                experiment.location location ON location.id = log.location_id AND location.is_void = FALSE
            LEFT JOIN
                experiment.entry entry ON "germplasmCross".entry_id = entry.id AND entry.is_void = FALSE
            LEFT JOIN
                tenant.person creator ON "germplasmCross".creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON "germplasmCross".modifier_id = modifier.id
            ${seedSourceInfoFromQuery}
            WHERE
                "germplasmCross".is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
            ${addedOrderString}
        `

        // Check if user specified values for fields parameter
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(parameters['fields'])
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                crossesQuery = knex.select(selectString)
            } else {
                crossesQuery = knex.column(parameters['fields'].split('|'))
            }

            crossesQuery += fromQuery
        } else {
            crossesQuery = `
                SELECT
                    ${addedDistinctString}
                    "germplasmCross".id AS "crossDbId",
                    "germplasmCross".cross_name AS "crossName",
                    "germplasmCross".cross_method AS "crossMethod",
                    (
                        SELECT 
                            abbrev
                        FROM 
                            master.scale_value
                        WHERE 
                            scale_id = (
                                SELECT 
                                    scale_id 
                                FROM 
                                    master.variable 
                                WHERE 
                                    abbrev = 'CROSS_METHOD'
                            ) 
                            AND LOWER(value) = LOWER(cross_method)
                    ) AS "crossMethodAbbrev",
                    "germplasmCross".remarks,
                    "germplasmCross".is_method_autofilled AS "isMethodAutofilled",
                    "germplasmCross".harvest_status AS "harvestStatus",
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation AS "germplasmDesignation",
                    germplasm.parentage AS "germplasmParentage",
                    germplasm.generation AS "germplasmGeneration",
                    g2.germplasm_state AS "state",
                    g2.germplasm_type AS "type",
                    crop.id AS "cropDbId",
                    crop.crop_code AS "cropCode",
                    entry.id AS "entryDbId",
                    entry.entry_number AS "entryNo",
                    entry.entry_name AS "entryName",
                    entry.entry_role AS "entryRole",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_code AS "experimentCode",
                    experiment.data_process_id AS "experimentDataProcessId",
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_name AS "occurrenceName",
                    occurrence.occurrence_code AS "occurrenceCode",
                    ${crossParentsSourceQuery}
                    ${seedSourceQuery}
                    "germplasmCross".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    "germplasmCross".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            if (orderString != '') {
                orderString = orderString + (await processQueryHelper.getOrderString(sort)).replace("ORDER BY", ', ')
            } else {
                orderString = await processQueryHelper.getOrderString(sort)
            }
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                return res.send(new errors.BadRequestError(errMsg))
            }
        }
        else if(addedOrderString != ''){
            orderString = addedOrderString
        }
 
        let newFinalQuery = `
            WITH baseQuery AS (
                ${newBaseQuery}
            )
            ${harvestDataCte}
            ${crossFemaleParentInfoCte}
            ${crossMaleParentInfoCte}
            ,withOrderNumber AS (
                SELECT
                    ROW_NUMBER() OVER(${orderString}) AS "orderNumber",
                    withMaleParentInfo.*
                FROM
                    withMaleParentInfo
            )
            ${filteredResQueryCte}
            ${countQueryCte}
            ,withLimits AS (
                SELECT  
                    *
                FROM
                    ${filteredResAlias}
                LIMIT (:limit) OFFSET (:offset)
            )
        `

        let correctingOrderString = orderString
        if (parameters['fields'] && !parameters['fields'].includes("crossDbId")) {
            correctingOrderString = ''
        }

        if(countQueryCte != ''){ // with count
            newFinalQuery += `
                SELECT
                    cq."totalCount",
                    withLimits.*
                FROM
                    withLimits
                    JOIN countQuery cq on cq."occurrenceDbId" = withLimits."occurrenceDbId"
                    ${correctingOrderString}
            `
        }
        else{ // without count
            newFinalQuery += `
                SELECT
                    withLimits.*
                FROM
                    withLimits
            `
        }

        // Generate final SQL query
        let crossesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            crossesQuery,
            conditionString,
            orderString,
        )

        let crosses = await sequelize.query(newFinalQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await crosses == undefined || await crosses.length < 1) {
            return res.send(200, {
                rows: [],
                count: 0
            })
        }

        count = crosses != undefined ? crosses[0].totalCount : 0

        return res.send(200, {
            rows: crosses,
            count: count
        })
    }
}