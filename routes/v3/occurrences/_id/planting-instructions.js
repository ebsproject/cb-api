/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
const logger = require('../../../../helpers/logger')

module.exports = {
    // Endpoint for retrieving an existing planting instructions record
    // GET /v3/occurrences/:id/planting-instructions
    get: async function (req, res, next) {
        // Set defaults 
        // Retrieve occurrence ID
        let occurrenceDbId = req.params.id
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let responseColString = `
            -- planting_instruction
            plantinst.id AS "plantingInstructionDbId",
            plantinst.entry_id AS "entryDbId",
            plantinst.germplasm_id AS "germplasDbId",
            germplasm.germplasm_code AS "germplasmCode",
            plantinst.entry_code AS "entryCode",
            plantinst.entry_number AS "entryNumber",
            plantinst.entry_name AS "entryName",
            plantinst.entry_type AS "entryType",
            plantinst.entry_role AS "entryRole",
            -- experiment
            expt.id AS "experimentDbId",
            prog.program_code AS "programCode",
            stage.stage_code AS "stageCode",
            expt.experiment_year AS "experimentYear",
            season.season_code AS "seasonCode",
            expt.experiment_code AS "experimentCode",
            expt.experiment_name AS "experimentName",
            expt.experiment_type AS "experimentType",
            expt.experiment_design_type AS "experimentDesignType",
            steward.person_name AS "experimentSteward",
            -- occurrence
            occ.id AS "occurrenceDbId",
            occ.occurrence_code AS "occurrenceCode",
            occ.occurrence_name AS "occurrenceName",
            siteocc.geospatial_object_code AS "occurrenceSiteCode",
            fieldocc.geospatial_object_code AS "occurrenceFieldCode",
            -- plot
            plot.id AS "plotDbId",
            plot.plot_code AS "plotCode",
            plot.plot_number AS "plotNumber",
            plot.plot_type AS "plotType",
            plot.design_x AS "designX",
            plot.design_y AS "designY",
            plot.pa_x AS "paX",
            plot.pa_y AS "paY",
            -- package
            package.id AS "packageDbId",
            package.package_code AS "packageCode",
            package.package_label AS "packageLabel",
            -- pipeline
            pipeline.id AS "pipelineDbId",
            pipeline.pipeline_code AS "pipelineCode",
            pipeline.pipeline_name AS "pipelineName"
        `
        const endpoint = 'occurrences/:id/planting-instructions'
        const operation = 'SELECT'
        let retrieveExperimentData = (req.params.retrieveExperimentData == 'true') ? req.params.retrieveExperimentData : false

        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceQuery = `
            SELECT
                id AS "occurrenceDbId"
            FROM
                experiment.occurrence
            WHERE
                is_void = FALSE
                AND id = ${occurrenceDbId}
        `

        let occurrence = await sequelize
            .query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = 'Resource not found, the occurrence you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let variableId = ''
        let crosstabColumns = ''
        let selectColumns = ''
        let leftJoinCrossTabClause = ''

        if (retrieveExperimentData) {
            // Get all the plot data variables
            let variableQuery = `
                SELECT 
                    STRING_AGG( variable_id::text, ',') AS "variableId",
                    STRING_AGG( '"' || abbrev || '"', ',') AS "selectColumns",
                    STRING_AGG( '"' || abbrev || '"' || ' varchar', ',') AS "crosstabColumns"
                FROM(
                    SELECT 
                        distinct (experiment_data.variable_id) AS variable_id,
                        v.label as label,
                        v.abbrev
                        FROM
                            experiment.experiment_data experiment_data
                            LEFT JOIN experiment.experiment ON experiment.id = experiment_data.experiment_id AND experiment.is_void = FALSE
                            LEFT JOIN master.variable v ON v.id = experiment_data.variable_id
                            LEFT JOIN experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                        WHERE
                            occurrence.id = ${occurrenceDbId} AND
                            occurrence.is_void = FALSE AND
                            experiment.is_void = FALSE AND
                            experiment_data.is_void = FALSE
                    ORDER BY v.abbrev
                )a
            `

            let variables = await sequelize.query(variableQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            variableId = variables[0]['variableId'] == null ? [0] : variables[0]['variableId']
            crosstabColumns = variables[0]['crosstabColumns']
            selectColumns = variables[0]['selectColumns']

            if (crosstabColumns != "" && crosstabColumns != null) {
                crosstabColumns = ',' + crosstabColumns
            } else if (crosstabColumns == null) {
                crosstabColumns = ', "null" text';
            }
            if (selectColumns != "" && selectColumns != null) {
                selectColumns = ',' + selectColumns
            } else if (selectColumns == null) {
                selectColumns = ''
            }

            leftJoinCrossTabClause = `LEFT JOIN
            (
                SELECT 
                    "experimentDbId" 
                    ${selectColumns}
                FROM
                    CROSSTAB(
                    '
                        SELECT 
                            experiment.id AS "experimentDbId",
                            experiment_data.variable_id,
                            experiment_data.data_value
                        FROM
                            experiment.experiment experiment
                        LEFT JOIN
                            experiment.experiment_data ON experiment.id = experiment_data.experiment_id
                        LEFT JOIN
                            experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                        WHERE
                            occurrence.id = ${occurrenceDbId} AND
                            occurrence.is_void = FALSE AND
                            experiment.is_void = FALSE AND
                            experiment_data.is_void = FALSE
                        ORDER BY
                            experiment.id
                    ',
                    '
                    SELECT UNNEST(ARRAY[${variableId}])
                '             
                ) AS (
                    "experimentDbId" integer
                    ${crosstabColumns}
                ) 
            )crosstab ON crosstab."experimentDbId" = expt.id`
        }

        // Build query
        let plantInsQuery = `
            SELECT
                ${responseColString}
                ${selectColumns}
            FROM
                experiment.experiment AS expt
                JOIN tenant.crop AS crop
                    ON expt.crop_id = crop.id
                JOIN tenant.PROGRAM AS prog
                    ON expt.program_id = prog.id
                LEFT JOIN tenant.pipeline AS pipeline
                    ON expt.pipeline_id = pipeline.id
                JOIN tenant.stage AS stage
                    ON expt.stage_id = stage.id
                LEFT JOIN tenant.project AS proj
                    ON expt.project_id = proj.id
                JOIN tenant.season AS season
                    ON expt.season_id = season.id
                JOIN tenant.person AS steward
                    ON expt.steward_id = steward.id
                INNER JOIN experiment.entry_list AS entlist
                    ON expt.id = entlist.experiment_id
                    AND entlist.is_void = FALSE
                INNER JOIN experiment.entry AS entry
                    ON entlist.id = entry.entry_list_id
                    AND entry.is_void = FALSE
                JOIN experiment.occurrence AS occ
                    ON occ.experiment_id = expt.id
                    AND occ.is_void = FALSE
                INNER JOIN place.geospatial_object AS siteocc
                    ON occ.site_id = siteocc.id
                    AND siteocc.is_void = FALSE
                LEFT JOIN place.geospatial_object AS fieldocc
                    ON occ.field_id = fieldocc.id
                    AND fieldocc.is_void = FALSE
                LEFT JOIN experiment.location_occurrence_group AS logrp
                    ON logrp.occurrence_id = occ.id
                    AND logrp.is_void = FALSE
                LEFT JOIN experiment.LOCATION AS loc
                    ON loc.id = logrp.location_id
                    AND loc.is_void = FALSE
                LEFT JOIN place.geospatial_object AS siteloc
                    ON loc.site_id = siteloc.id
                    AND siteloc.is_void = FALSE
                LEFT JOIN place.geospatial_object AS fieldloc
                    ON loc.field_id = fieldloc.id
                    AND fieldloc.is_void = FALSE
                INNER JOIN experiment.plot AS plot
                    ON plot.occurrence_id = occ.id
                    AND plot.is_void = FALSE
                INNER JOIN experiment.planting_instruction AS plantinst
                    ON plot.id = plantinst.plot_id
                    AND plantinst.entry_id = entry.id
                    AND plantinst.is_void = FALSE
                LEFT JOIN germplasm.germplasm
                    ON plantinst.germplasm_id = germplasm.id
                LEFT JOIN germplasm.package
                    ON package.id = entry.package_id
                    AND package.is_void = FALSE
                ${leftJoinCrossTabClause}
            WHERE
                expt.is_void = FALSE
                AND occ.id = ${occurrenceDbId}
            ORDER BY
                plot.plot_number
        `

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        let plantInsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            plantInsQuery,
            conditionString,
            orderString
        )

        // Get count
        plantInsFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            plantInsFinalSqlQuery,
            orderString
        )
 
        // Retrieve planting instructions record from database
        let plantIns = await sequelize.query(plantInsFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)

                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError('err: ' + err))
                // res.send(new errors.InternalError(errMsg))
                return
            })

        if (await plantIns == undefined || await plantIns.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = plantIns[0] ? plantIns[0].totalCount : 0

        res.send(200, {
            rows: plantIns,
            count: count
        })
        return
    }
}