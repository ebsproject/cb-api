/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let aclStudy = require('../../../../helpers/acl/study.js')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
  get: async function (req, res,next) {

    // Retrieve the study ID
    let studyDbId = req.params.id

    // Check if ID is valid and existing
    if (!validator.isInt(studyDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400065)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let studyRecordQuery = `
      SELECT study.id
      FROM operational.study study
      WHERE
        study.is_void = FALSE
        AND study.id = ${studyDbId}
    `
      
    // Validate if study ID exists in the database
    let studyRecord = await sequelize
      .query(studyRecordQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (studyRecord == undefined || studyRecord.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404002)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Retrieve the user ID of the client from the access token
    let userId = await tokenHelper.getUserId(req)

    if (userId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let applyAcl = true

    // Check if user is an administrator. If yes, do not apply ACL.
    if (await userValidator.isAdmin(userId)) {
      applyAcl = false
    }

    if (applyAcl == true) {
      // Check if the user has access to the study
      let canAccess = await aclStudy
        .isStudyAccessible('study', '' + studyDbId, 'user', '' + userId)

      if (!canAccess) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401005)
        res.send(new errors.UnauthorizedError(errMsg))
        return
      }
    }

    // build string query for dynamic columns
    let dynamicColumnsString = ``

    // Retreive all entry data variables under a study
    let entryDataVariablesQuery = `
      SELECT 
        variable.id,
        variable.abbrev
      FROM
        master.variable variable
      WHERE
        variable.is_void = FALSE
        AND variable.abbrev != 'MAP_X'
        AND variable.abbrev != 'MAP_Y'
        AND variable.id IN (
          SELECT id
          FROM (
            SELECT 
              DISTINCT entryData.variable_id AS id
            FROM 
              operational.entry_data entryData
            WHERE
              entryData.study_id = ${studyDbId}
              AND entryData.is_void = FALSE
          ) AS variable_ids
        )
    `

    let entryDataVariables = await sequelize
      .query(entryDataVariablesQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (entryDataVariables != undefined || entryDataVariables.length > 0) {
      for (entryDataVariable of entryDataVariables) {
        let entryDataVariableDbId = entryDataVariable.id
        let entryDataVariableAbbrev = entryDataVariable.abbrev
        let columnString = `
          (
            SELECT
              entryData.value
            FROM
              operational.entry_data entryData
            WHERE
              entryData.is_void = FALSE
              AND entryData.entry_id = entry.id
              AND entryData.variable_id = ${entryDataVariableDbId}
            LIMIT 1
          ) AS ${entryDataVariableAbbrev}
        `

        dynamicColumnsString += (dynamicColumnsString != ``) ? `,` : ``
        dynamicColumnsString += columnString
      }
    } 

    // Retrieve all entry metadata variables under a study
    let entryMetadataVariablesQuery = `
      SELECT 
        variable.id,
        variable.abbrev
      FROM
        master.variable variable
      WHERE
        variable.is_void = FALSE
        AND variable.abbrev != 'MAP_X'
        AND variable.abbrev != 'MAP_Y'
        AND variable.id IN (
          SELECT id
          FROM (
            SELECT 
              DISTINCT entryMetadata.variable_id AS id
            FROM 
              operational.entry_metadata entryMetadata
            WHERE
              entryMetadata.study_id = ${studyDbId}
              AND entryMetadata.is_void = FALSE
          ) AS variable_ids
        )
    `

    let entryMetadataVariables = await sequelize
      .query(entryMetadataVariablesQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (
      entryMetadataVariables != undefined ||
      entryMetadataVariables.length > 0
    ) {
      for (entryMetadataVariable of entryMetadataVariables) {
        let entryMetadataVariableDbId = entryMetadataVariable.id
        let entryMetadataVariableAbbrev = entryMetadataVariable.abbrev

        let columnString = `
          (
            SELECT
              entryMetadata.value
            FROM
              operational.entry_metadata entryMetadata
            WHERE
              entryMetadata.is_void = FALSE
              AND entryMetadata.entry_id = entry.id
              AND entryMetadata.variable_id = ${entryMetadataVariableDbId}
            LIMIT 1
          ) AS ${entryMetadataVariableAbbrev}
        `

        dynamicColumnsString += (dynamicColumnsString != ``) ? `,` : ``
        dynamicColumnsString += columnString
      }
    }

    dynamicColumnsString = 
      (dynamicColumnsString == ``) ? `` : `, ${dynamicColumnsString}`

    let entryReportsQuery = `
      SELECT 
        "study".id AS "studyDbId",
        "study".name AS "studyName",
        (
          SELECT 
            place.name 
          FROM 
            master.place place 
          WHERE 
            place.is_void = FALSE 
            AND "study".place_id = place.id 
          LIMIT 1
        ) AS "location",
        "study".study,
        "study".design,
        (
          SELECT 
            sm.value 
          FROM 
            operational.study_metadata sm 
          WHERE 
            sm.is_void = FALSE 
            AND sm.study_id = "study".id 
            AND sm.variable_id = 83
          LIMIT 1
        ) AS "dist_bet_rows",
        (
          SELECT 
            sm.value 
          FROM 
            operational.study_metadata sm 
          WHERE 
            sm.is_void = FALSE 
            AND sm.study_id = "study".id 
            AND sm.variable_id = 188 
          LIMIT 1
        ) AS "rows_per_plot_cont",
        (
          SELECT 
            sm.value 
          FROM 
            operational.study_metadata sm 
          WHERE 
            sm.is_void = FALSE 
            AND sm.study_id = "study".id 
            AND sm.variable_id = 380 
          LIMIT 1
        ) AS "hills_per_row_cont",
        (
          SELECT 
            sm.value 
          FROM 
            operational.study_metadata sm 
          WHERE 
            sm.is_void = FALSE 
            AND sm.study_id = "study".id 
            AND sm.variable_id = 381 
          LIMIT 1
        ) AS "dist_bet_hills",
        (
          SELECT 
            sm.value 
          FROM 
            operational.study_metadata sm 
          WHERE 
            sm.is_void = FALSE 
            AND sm.study_id = "study".id 
            AND sm.variable_id = 382 
          LIMIT 1
        ) AS "plot_area_sqm_cont",
        (
          SELECT 
            sm.value 
          FROM 
            operational.study_metadata sm 
          WHERE 
            sm.is_void = FALSE 
            AND sm.study_id = "study".id 
            AND sm.variable_id = 383 
          LIMIT 1
        ) AS "plot_area_ha_cont",
        "entry".product_id,
        "entry".id AS "entry_id",
        "entry".entno AS "entry_no",
        "entry".entcode AS "entry_code",
        (
          SELECT 
            product.designation 
          FROM
            master.product product 
          WHERE 
            product.is_void = FALSE 
            AND product.id = "entry".product_id 
          LIMIT 1
        ) AS "product_preferred_name",
        "entry".product_name AS "designation",
        "entry".product_gid AS "gid", 
        (
          SELECT 
            pn.value 
          FROM 
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='line_name' 
          LIMIT 1
        ) AS "line_name", 
        (
          SELECT
            pn.value 
          FROM
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='elite_lines' 
          LIMIT 1
        ) AS "elite_lines", 
        (
          SELECT 
            pn.value 
          FROM 
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='derivative_name' 
          LIMIT 1
        ) AS "derivative_name",
        (
          SELECT
            pn.value 
          FROM
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='alternative_cultivar_name' 
          LIMIT 1
        ) AS "alternative_cultivar_name", 
        (
          SELECT 
            pn.value 
          FROM 
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='cultivar_name' 
          LIMIT 1
        ) AS "cultivar_name", 
        (
          SELECT 
            pn.value 
          FROM 
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='international_testing_number'
          LIMIT 1
        ) AS "international_testing_number", 
        (
          SELECT 
            pn.value
          FROM 
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='local_common_name' 
          LIMIT 1
        ) AS "local_common_name",
        (
          SELECT 
            pn.value 
          FROM 
            master.product_name pn 
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='release_name' 
          LIMIT 1
        ) AS "release_name", 
        (
          SELECT 
            pn.value 
          FROM 
            master.product_name pn
          WHERE 
            pn.is_void = FALSE 
            AND "entry".product_id = pn.product_id 
            AND pn.name_type='product_preferred_name' 
          LIMIT 1
        ) AS "product_preferred_name",
        "plot".id AS "plot_id",
        "plot".plotno,
        "plot".code AS "plot_code",
        "plot".rep,
        "plot".design_y,
        "plot".design_x,
        "plot".map_x,
        "plot".map_y 
      -- dynamic columns
        ${dynamicColumnsString}
      -- main FROM clause
      FROM
        operational.study "study",
        operational.entry "entry",
        operational.plot "plot"
      WHERE 
        "study".id = ${studyDbId}
        AND "plot".is_void = FALSE
        AND "study".is_void = FALSE
        AND "entry".is_void = FALSE
        AND "entry".study_id = "study".id
        AND "entry".id = "plot".entry_id  
      ORDER BY 
        "study".year DESC, 
        "study".season_id DESC, 
        "study".id ASC, 
        "plot".plotno
      LIMIT 1000
    `

    // Retrieve plot reports from database
    let entryReports = await sequelize
      .query(entryReportsQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return empty array if entry reports are not found
    if (entryReports == undefined || entryReports.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    res.send(200, {
      rows: entryReports,
      count: entryReports.length
    })
    return
  }
}