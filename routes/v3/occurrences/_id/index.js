/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
let logger = require('../../../../helpers/logger/index')
const endpoint = 'occurrences/:id'

module.exports = {

    // Endpoint for updating an existing occurrence record
    // PUT /v3/occurrences/:id
    put: async function (req, res, next) {

        // Retrieve the occurrence ID
        let occurrenceDbId = req.params.id

        if(!validator.isInt(occurrenceDbId)) {
            let errMsg = `Invalid format, occurrence ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let occurrenceCode = null
        let occurrenceName = null
        let occurrenceStatus = null
        let description = null
        let remarks = null
        let geospatialObjectDbId = null
        let siteDbId = null
        let fieldDbId = null
        let repCount = null
        let occurrenceDesignType = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)
 
        // Check if occurrence is existing
        // Build query
        let occurrenceQuery = `
            SELECT
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.occurrence_status AS "occurrenceStatus",
                occurrence.occurrence_number AS "occurrenceNumber",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.description,
                occurrence.remarks,
                occurrence.occurrence_design_type AS "occurrenceDesignType",
                program.program_code AS "programCode",
                site.geospatial_object_code AS "siteCode",
                stage.stage_code AS "stageCode",
                experiment.id AS "experimentDbId",
                experiment.experiment_year AS "experimentYear",
                season.season_code AS "seasonCode",
                go.id AS "geospatialObjectDbId",
                occurrence.rep_count AS "repCount",
                creator.id AS "creatorDbId"
            FROM
                experiment.occurrence occurrence
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = occurrence.experiment_id
            LEFT JOIN
                place.geospatial_object go ON go.id = occurrence.geospatial_object_id
            LEFT JOIN
                place.geospatial_object site ON site.id = occurrence.site_id
            LEFT JOIN
                tenant.stage stage on stage.id = experiment.stage_id
            LEFT JOIN
                tenant.season season on season.id = experiment.season_id
            LEFT JOIN 
                tenant.program program ON program.id = experiment.program_id
            LEFT JOIN
                tenant.person creator ON creator.id = occurrence.creator_id
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
        `

        // Retrieve occurrence from the database
        let occurrence = await sequelize.query(occurrenceQuery, {
            type: sequelize.QueryTypes.SELECT,
        })

        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = `The occurrence you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordOccurrenceName = occurrence[0].occurrenceName
        let recordOccurrenceStatus = occurrence[0].occurrenceStatus
        let recordDescription = occurrence[0].description
        let recordRemarks = occurrence[0].remarks
        let recordExperimentDbId = occurrence[0].experimentDbId
        let recordGeospatialObjectDbId = occurrence[0].geospatialObjectDbId
        let recordSiteDbId = occurrence[0].siteDbId
        let recordFieldDbId = occurrence[0].fieldDbId
        let recordRepCount = occurrence[0].repCount
        let recordCreatorDbId = occurrence[0].creatorDbId

        let recordProgramCode = occurrence[0].programCode
        let recordSiteCode = occurrence[0].siteCode
        let recordStageCode = occurrence[0].stageCode
        let recordExperimentYear = occurrence[0].experimentYear
        let recordSeasonCode = occurrence[0].seasonCode
        let recordOccurrenceCode = occurrence[0].occurrenceCode
        let recordOccurrenceNumber = occurrence[0].occurrenceNumber
        let recordOccurrenceDesignType = occurrence[0].occurrenceDesignType

        // Check if user is an admin or an owner of the cross
        // Remove checking of permission to enable collaborator role update during 22.05
        /**
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${recordExperimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }*/

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let occurrenceUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/occurrences/"
            + occurrenceDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body        

        // Start transaction
        let transaction
        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/

            if (data.occurrenceStatus !== undefined) {
                occurrenceStatus = data.occurrenceStatus.trim()

                // Check if user input is same with the current value in the database
                if (occurrenceStatus != recordOccurrenceStatus) {
                    // get status to be appended
                    let statusArr = occurrenceStatus.split(";");
                    let newStatus = statusArr.slice(-1)[0].trim();

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                CASE WHEN
                                    count(1) > 0 
                                THEN
                                    1
                                ELSE
                                    0
                                END AS count
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev = 'OCCURRENCE_STATUS' AND
                                scaleValue.value ILIKE $$%${newStatus}%$$
                        )
                    `
                    validateCount += 1
                    
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_status = $$${occurrenceStatus}$$
                    `
                }
            }

            // update site ID
            if (data.siteDbId !== undefined) {
                siteDbId = data.siteDbId

                if (!validator.isInt(siteDbId)) {
                    let errMsg = `Invalid format, site ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (siteDbId != recordSiteDbId) {

                    // Validate site ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if site is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object go
                            WHERE 
                                go.is_void = FALSE AND
                                go.id = ${siteDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        site_id = $$${siteDbId}$$
                    `
                }
            }

            // update field ID
            if (data.fieldDbId !== undefined) {
                fieldDbId = data.fieldDbId

                // Check if user input is same with the current value in the database
                if (fieldDbId != recordFieldDbId) {

                    if (fieldDbId !== "null") {
                        if (!validator.isInt(fieldDbId)) {
                            let errMsg = `Invalid format, field ID must be an integer.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                        
                        // Validate field ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if site is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    place.geospatial_object go
                                WHERE 
                                    go.is_void = FALSE AND
                                    go.id = ${fieldDbId}
                            )
                        `
                        validateCount += 1
                    } else {
                        fieldDbId = null
                    }
                    
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        field_id = ${fieldDbId}
                    `
                }
            }

            if (data.geospatialObjectDbId !== undefined) {
                geospatialObjectDbId = data.geospatialObjectDbId

                if (!validator.isInt(geospatialObjectDbId)) {
                    let errMsg = `Invalid format, geospatial object ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (geospatialObjectDbId != recordGeospatialObjectDbId) {

                    // Validate geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if geospatial object is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object go
                            WHERE 
                                go.is_void = FALSE AND
                                go.id = ${geospatialObjectDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        geospatial_object_id = $$${geospatialObjectDbId}$$
                    `
                }
            }

            if (data.repCount !== undefined) {
                repCount = data.repCount

                if (!validator.isInt(repCount)) {
                    let errMsg = `Invalid format, rep count must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (repCount != recordRepCount) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        rep_count = $$${repCount}$$
                    `
                }
            }

            if (data.occurrenceName !== undefined) {
                occurrenceName = data.occurrenceName

                if (occurrenceName != recordOccurrenceName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_name = $$${occurrenceName}$$
                    `
                }
            }

            if (data.description !== undefined) {
                description = data.description

                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        description = $$${description}$$
                    `
                }
            }

            if (data.remarks !== undefined) {
                remarks = data.remarks

                if (remarks != recordRemarks) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        remarks = $$${remarks}$$
                    `
                }
            }

            if (data.accessData !== undefined) {
                let convertAd = JSON.stringify(data.accessData)
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                access_data = $$${convertAd}$$`
            }

            if (data.occurrenceDesignType !== undefined) {
                occurrenceDesignType = data.occurrenceDesignType

                if (occurrenceDesignType != recordOccurrenceDesignType) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_design_type = $$${occurrenceDesignType}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            } 

            if (setQuery.length > 0) setQuery += `,`

            // Update the occurrence record
            let updateOccurrenceQuery = `
                UPDATE
                    experiment.occurrence    
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${occurrenceDbId}
            `
            // Get transaction
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateOccurrenceQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction
                }).then(async result => {
                        // Retrieve existing experiment_status and count
                        let checkExperimentStatusQuery = `
                            SELECT
                                id,
                                experiment_status as "experimentStatus",
                                (select count(id) from experiment.occurrence where experiment_id = ${recordExperimentDbId}) as "occurrenceCount",
                                (select count(id) from experiment.occurrence where occurrence_status like '%planted%' and experiment_id = ${recordExperimentDbId}) as "plantedOccurrencesCount"
                            FROM experiment.experiment
                            where
                                id = ${recordExperimentDbId}
                        `
                        let checkExperimentStatus = await sequelize.query(checkExperimentStatusQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                        let currentExperimentStatus = checkExperimentStatus[0].experimentStatus
                        let currentOccurrenceCount = checkExperimentStatus[0].occurrenceCount
                        let currentPlantedOccurrencesCount = checkExperimentStatus[0].plantedOccurrencesCount

                        if (currentExperimentStatus != 'planted' &&
                        (currentPlantedOccurrencesCount === currentOccurrenceCount)) {

                            const updateExperimentStatus = `UPDATE
                                    experiment.experiment
                                SET
                                    experiment_status = 'planted',
                                    modification_timestamp = NOW(),
                                    modifier_id = ${personDbId}
                                WHERE
                                    id = ${recordExperimentDbId}`

                            await sequelize.query(updateExperimentStatus, {
                                type: sequelize.QueryTypes.UPDATE,
                                transaction: transaction
                            }).then(() => {
                                logger.logCompletion('occurrences',
                                    'UPDATE',
                                    {
                                        recordExperimentDbId,
                                        'message': 'Successfully updated experiment_status column.'
                                    }
                                )
                            }).catch(async err => {
                                logger.logFailingQuery(endpoint, 'UPDATE', err)
                                throw new Error(err)
                            })
                        }
                    }
                ).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return the transaction info
            let resultArray = {
                occurrenceDbId: occurrenceDbId,
                recordCount: 1,
                href: occurrenceUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing occurrence record
    // DELETE /v3/occurrences/:id
    delete: async function (req, res, next) {
         // Retrieve the occurrence ID
         let occurrenceDbId = req.params.id

         if(!validator.isInt(occurrenceDbId)) {
             let errMsg = `Invalid format, occurrence ID must be an integer.`
             res.send(new errors.BadRequestError(errMsg))
             return
         }
 
         // Retrieve person ID of client from access token
         let personDbId = await tokenHelper.getUserId(req)

         if (personDbId == null) {
           let errMsg = await errorBuilder.getError(req.headers.host, 401002)
           res.send(new errors.BadRequestError(errMsg))
           return
         }

         let isAdmin = await userValidator.isAdmin(personDbId)
 
         let transaction
         try {
            let occurrenceQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    experiment.id AS "experimentDbId"
                FROM
                    experiment.occurrence occurrence
                    LEFT JOIN
                        tenant.person creator ON creator.id = occurrence.creator_id
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.id = occurrence.experiment_id
                WHERE
                    occurrence.is_void = FALSE AND
                    occurrence.id = ${occurrenceDbId}
            `

            let occurrence = await sequelize.query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (await occurrence == undefined || await occurrence.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404005)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = occurrence[0].creatorDbId
            let recordExperimentDbId = occurrence[0].experimentDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                        LEFT JOIN 
                            tenant.team_member teamMember ON teamMember.person_id = person.id
                        LEFT JOIN 
                            tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                        LEFT JOIN 
                            tenant.program program ON program.id = programTeam.program_id 
                        LEFT JOIN
                            experiment.experiment experiment ON experiment.program_id = program.id
                    WHERE 
                        experiment.id = ${recordExperimentDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteOccurrenceQuery = format(`
                UPDATE
                    experiment.occurrence occurrence
                SET
                    occurrence_number = CONCAT('-', $$${occurrenceDbId}$$)::int,
                    occurrence_name = CONCAT('VOIDED-', $$${occurrenceDbId}$$),
                    occurrence_code = CONCAT('VOIDED-', $$${occurrenceDbId}$$),
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${occurrenceDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    occurrence.id = ${occurrenceDbId}
            `)

            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(deleteOccurrenceQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            res.send(200, {
                rows: { occurrenceDbId: occurrenceDbId }
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}