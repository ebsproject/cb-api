/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
const logger = require('../../../../helpers/logger')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}
        let condition = ''
        let occurrenceDbId = req.params.id
        let blockColumnsSelectQuery = ''
        let blockColumnsFromQuery = ''
        let params = req.query
        let accessDataQuery = ''
        let programCond = ``
        let conditionStringArr = []
        let excludedParametersArray = []
        let responseColString = `
            plot.id::text AS "plotDbId",
            plot.entry_id::text AS "entryDbId",
            pi.entry_code AS "entryCode",
            pi.entry_number AS "entryNumber",
            pi.entry_name AS "entryName",
            pi.entry_type AS "entryType",
            pi.entry_role AS "entryRole",
            pi.entry_class AS "entryClass",
            pi.entry_status AS "entryStatus",
            pi.germplasm_id AS "germplasmDbId",
            g.germplasm_code AS "germplasmCode",
            g.germplasm_state AS "germplasmState",
            g.germplasm_type AS "germplasmType",
            g.parentage AS "parentage",
            pi.seed_id AS "seedDbId",
            seed.seed_code AS "seedCode",
            seed.seed_name AS "seedName",
            pi.package_id AS "packageDbId",
            package.package_code AS "packageCode",
            package.package_label AS "packageLabel",
            plot_code AS "plotCode",
            plot_number AS "plotNumber",
            plot_type AS "plotType",
            rep,
            design_x AS "designX",
            design_y AS "designY",
            plot_order_number AS "plotOrderNumber",
            pa_x AS "paX",
            pa_y AS "paY",
            field_x AS "fieldX",
            field_y AS "fieldY",
            block_number AS "blockNumber",
            plot_status AS "plotStatus",
            plot_qc_code AS "plotQcCode",
            pi.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            plot.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier`
        const endpoint = 'occurrences/:id/plot-data-files-search'
        const operation = 'SELECT'

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req, res)

        // If userId is undefined, terminate
        if (userId === undefined) {
            return
        }
        // If userId is null, return error
        else if(userId === null){
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if occurrenceDbId is valid
        if (!validator.isInt(occurrenceDbId)) {
            let errMsg = 'Invalid format, occurrenceDbId must be an integer'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if occurrenceDbId exists
        let transactionQuery = `
            SELECT count(1)
            FROM 
                experiment.occurrence
            WHERE 
                id = ${occurrenceDbId} AND
                is_void = FALSE
        `
        let occurrenceRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (await occurrenceRecord == undefined || await occurrenceRecord.length == 0) {
            let errMsg = 'The occurrenceDbId does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if there is a request body
        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set columns to be excluded in parameters for filtering
            Array.prototype.diff = function (a) {
                return this.filter(function (i) { return a.indexOf(i) < 0; });
            };

            // Exclude all measurement variables
            excludedParametersArray.push('fields', 'dataQCCode', "dataValue", "userId")

            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''

            let conditionArray = []

            if (conditionArray.length > 0) {
                condition = conditionArray.join(' AND ')
                if (conditionString == '') {

                    conditionString = conditionString +
                        ` WHERE `
                        + condition
                } else {
                    conditionString = conditionString +
                        ` AND `
                        + condition
                }
            }

            if (conditionString.includes('You have provided invalid values for filter.')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // check if user is admin or not
        let isAdmin = await userValidator.isAdmin(userId)

        // if not admin, retrieve only occurrences user have access to
        if (!isAdmin && params?.ownershipType && params.ownershipType == 'shared') {
            if (params.collaboratorProgramCode != null) {
                programCond = ` AND program.program_code = '${params.collaboratorProgramCode}'`
            }

            // get all programs that user belongs to
            let getProgramsQuery = `
                SELECT
                    program.id 
                FROM 
                    tenant.program program
                WHERE
                    program.is_void = FALSE
                    AND EXISTS (
                        SELECT 1
                        FROM tenant.program_team pt
                        WHERE
                            pt.is_void = FALSE
                            AND EXISTS (
                                SELECT 1
                                FROM tenant.team_member tm
                                WHERE
                                    tm.is_void = FALSE
                                    AND tm.team_id = pt.team_id
                                    AND tm.person_id = ${userId}
                            )
                            AND pt.program_id = program.id
                    )
                    ${programCond}
            `
            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                accessDataQuery += `occ.access_data #> $$\{program,${programObj.id}}$$ is not null`

            }

            // include occurrences shared to the user
            let userQuery = `occ.access_data #> $$\{person,${userId}}$$ is not null`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            } else {
                accessDataQuery += userQuery                
            }

            if (accessDataQuery != ``) {
                accessDataQuery = ` AND (${accessDataQuery})`
            }
        }

        // Get experiment design type
        let expDesignQuery =   `
            SELECT
                exp.experiment_design_type,
                exp.experiment_status,
                o.occurrence_design_type
            FROM
                experiment.experiment exp
            LEFT JOIN
                experiment.occurrence o ON o.experiment_id = exp.id
            WHERE
                    exp.is_void = false
                    AND o.id = ${occurrenceDbId}
                    AND exp.id = o.experiment_id
                    AND o.is_void = false
        `

        let expDesign = await sequelize.query(expDesignQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let expStatus = expDesign[0]['experiment_status']
        expDesign = expDesign[0]['experiment_design_type'] != null ?  expDesign[0]['experiment_design_type'] : expDesign[0]['occurrence_design_type']
        
        let entrySource = `
            experiment.planting_instruction pi
            ON 
            plot.id = pi.plot_id
            and pi.is_void = false`
        if(!(expStatus.toLowerCase() == 'created') && !(expStatus.toLowerCase().includes('planted')) && !(expStatus.toLowerCase().includes('mapped'))){
            entrySource = `
                experiment.entry pi
                ON 
                plot.entry_id = pi.id
                and pi.is_void = false`
            accessDataQuery = '' //reset accessDataQuery since the experiment is not yet finalized
        }
        if(['alpha-lattice', 'augmented rcbd', 'unspecified'].includes(expDesign.toLowerCase())) {
            blockColumnsSelectQuery = `,block_number AS "blockNumber"`
        } else if (await (expDesign.toLowerCase()) == 'row-column') {
            blockColumnsSelectQuery = `,descol1.block_value AS "rowBlockNumber", descol2.block_value AS "colBlockNumber"`
            blockColumnsFromQuery = `
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'RowBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol1
                    ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        exptdes.*
                    FROM
                        experiment.experiment_design AS exptdes
                    WHERE
                        exptdes.plot_id = plot.id
                        AND exptdes.block_name = 'ColBlock'
                        AND exptdes.is_void = FALSE
                ) AS descol2
                    ON TRUE
            `
        }

        // Get all plot data variables
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') AS "variableId",
                STRING_AGG( '"' || abbrev || '"', ',') AS "selectColumns",
                STRING_AGG( '"' || abbrev || '"' || ' varchar', ',') AS "crosstabColumns"
            FROM(
                SELECT 
                    distinct (plot_data.variable_id) AS variable_id,
                    v.label as label,
                    v.abbrev
                    FROM
                        experiment.plot_data plot_data
                            LEFT JOIN experiment.plot ON plot.id = plot_data.plot_id AND plot.is_void = FALSE
                            LEFT JOIN master.variable v ON v.id = plot_data.variable_id
                    WHERE
                        plot.occurrence_id = ${occurrenceDbId}
                        AND plot_data.is_void = FALSE
                ORDER BY v.abbrev
            )a
        `
        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        let variableId = variableList[0]['variableId'] == null ? [0] : variableList[0]['variableId']
        let crosstabColumns = variableList[0]['crosstabColumns']
        let selectColumns = variableList[0]['selectColumns']

        if (crosstabColumns != "" && crosstabColumns != null) {
            crosstabColumns = ',' + crosstabColumns
        } else if (crosstabColumns == null) {
            crosstabColumns = ', "null" text';
        }
        if (selectColumns != "" && selectColumns != null) {
            selectColumns = ',' + selectColumns
        } else if (selectColumns == null) {
            selectColumns = ''
        }

        // build the base query
        let plotDataQuery = null
        let fromQuery = `
            FROM
                experiment.plot
            LEFT JOIN
                (
                    SELECT 
                        "plotDbId" 
                        ${selectColumns}
                    FROM
                        CROSSTAB(
                        '
                            SELECT 
                                plot.id AS "plotDbId",
                                plot_data.variable_id,
                                plot_data.data_value
                            FROM
                                experiment.plot plot
                                    LEFT JOIN
                                        experiment.plot_data ON plot.id = plot_data.plot_id
                            WHERE
                                plot.occurrence_id = ${occurrenceDbId}
                                AND plot_data.is_void=false 
                                AND plot.is_void=false
                            ORDER BY
                                plot.id
                        ',
                        '
                        SELECT UNNEST(ARRAY[${variableId}])
                    '             
                    ) AS (
                        "plotDbId" integer
                        ${crosstabColumns}
                    ) 
                )crosstab ON crosstab."plotDbId" = plot.id
            LEFT JOIN
                ${entrySource}
            ${blockColumnsFromQuery}
            LEFT JOIN
                germplasm.germplasm g
            ON
                g.id = pi.germplasm_id
            LEFT JOIN
                germplasm.seed seed
            ON
                seed.id = pi.seed_id
            LEFT JOIN
                experiment.occurrence occ
            ON
                plot.occurrence_id = occ.id
                AND occ.is_void = FALSE
            LEFT JOIN
                germplasm.package package
            ON
                package.id = pi.package_id
            JOIN 
                tenant.person creator ON creator.id = pi.creator_id::integer
            LEFT JOIN 
                tenant.person modifier ON modifier.id = pi.modifier_id::integer
            WHERE
                plot.is_void = false 
                AND plot.occurrence_id = ${occurrenceDbId}
                ${accessDataQuery}
            ORDER BY
                plot.plot_number
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            plotDataQuery = knex.column(parameters['fields'].split('|'))
            plotDataQuery += fromQuery
        } else {
            plotDataQuery = `
                SELECT
                    ${responseColString}
                    ${blockColumnsSelectQuery}
                    ${selectColumns}
                ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let plotDataFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            plotDataQuery,
            conditionString,
            orderString
        )

        plotDataFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            plotDataFinalSqlQuery,
            orderString
        )

        // Retrieve the plotData records AND the totalCount
        let plotData = await sequelize
            .query(plotDataFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await plotData == undefined || await plotData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = plotData[0].totalCount

        res.send(200, {
            rows: plotData,
            count: count
        })
        return
    }
}