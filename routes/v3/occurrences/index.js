/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let occurrenceHelper = require('../../../helpers/occurrence/index.js')
let patternHelper = require('../../../helpers/patternGenerator/index.js')
let logger = require('../../../helpers/logger')

const endpoint = 'occurrences'

module.exports = {

    // Endpoint for creating a new occurrence record
    // POST /v3/occurrences
    post: async function (req, res, next) {

        // Set defaults
        let resultArray = []
        let recordCount = 0

        let occurrenceDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let occurrenceUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/occurrences'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = data.records

        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction
        try {
            let occurrenceValuesArray = []
            let occurrenceNumber = 0
            let counter = 0
            let occurrenceCode = null

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Set defaults
                let occurrenceName = null
                let occurrenceStatus = null
                let description = null
                let experimentDbId = null
                let geospatialObjectDbId = null
                let occurrenceDocument = null
                let repCount = null
                let siteDbId = null
                let fieldDbId = null

                // Check if required columns are in the request body
                if (
                    !record.occurrenceStatus || !record.experimentDbId
                ) {
                    let errMsg = `Required parameters are missing. Ensure that occurrenceStatus and experimentDbId fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */

                occurrenceName = record.occurrenceName
                occurrenceStatus = record.occurrenceStatus.trim()
                experimentDbId = record.experimentDbId

                // validation for occurrence status
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'OCCURRENCE_STATUS' AND
                            scaleValue.value ILIKE $$${occurrenceStatus}$$
                    )
                `
                validateCount += 1

                // database validation for experiment ID
                if (!validator.isInt(experimentDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400025)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if existing is existing return 1
                        SELECT 
                        count(1)
                        FROM 
                        experiment.experiment experiment
                        WHERE 
                        experiment.is_void = FALSE AND
                        experiment.id = ${experimentDbId}
                    )
                `
                validateCount += 1

                /** Validation of non-required parameters and set values */
                if (record.geospatialObjectDbId !== undefined) {
                    geospatialObjectDbId = record.geospatialObjectDbId

                    if (!validator.isInt(geospatialObjectDbId)) {
                        let errMsg = `Invalid format, geospatial object ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if geospatial object is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object geospatialObject
                            WHERE 
                                geospatialObject.is_void = FALSE AND
                                geospatialObject.id = ${geospatialObjectDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.siteDbId !== undefined) {
                    siteDbId = record.siteDbId

                    if (!validator.isInt(siteDbId)) {
                        let errMsg = `Invalid format, site ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate site ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                            (
                            --- Check if site is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object site
                            WHERE 
                                site.is_void = FALSE AND
                                site.id = ${siteDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.fieldDbId !== undefined) {
                    fieldDbId = record.fieldDbId

                    if (!validator.isInt(fieldDbId)) {
                        let errMsg = `Invalid format, field ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate field ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if field is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object field
                            WHERE 
                                field.is_void = FALSE AND
                                field.id = ${fieldDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.repCount !== undefined) {
                    repCount = record.repCount

                    if (!validator.isInt(repCount)) {
                        let errMsg = `Invalid format, rep count must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                description = (record.description !== undefined) ? record.description : null
                occurrenceDocument =
                    (record.occurrenceDocument !== undefined) ? record.occurrenceDocument : null

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                counter++;

                //set occurenceNumber
                occurrenceNumber = await patternHelper.getCounter('experiment', 'occurrence',
                    'experiment_id= ' + experimentDbId + ' AND is_void=FALSE', "experiment_id", counter)

                //get experimentName
                let value = await patternHelper.getEntityValue('experiment',
                    'experiment', 'experiment_code', experimentDbId)
                let experimentCode = value["experiment_code"]

                // get padded occurenceNumber
                let paddedOccurenceNumber = await patternHelper.getPaddedNumber(occurrenceNumber, 'leading', 3)

                // set occurrenceName
                occurrenceCode = `${experimentCode}-${paddedOccurenceNumber}`

                // if occurrenceName is not specified, generate occurrenceName
                if (record.occurrenceName == undefined) {
                    // set occurrenceCode
                    pattern = await patternHelper.getPattern("OCCURRENCE_NAME_CONFIG")
                    occurrenceName = await occurrenceHelper.generateCode(pattern, experimentDbId, counter)
                }

                // Get values
                let tempArray = [
                    occurrenceCode, occurrenceName, occurrenceNumber, occurrenceStatus, description, experimentDbId,
                    geospatialObjectDbId, occurrenceDocument, repCount, personDbId, siteDbId, fieldDbId
                ]

                occurrenceValuesArray.push(tempArray)
            }

            // Create occurrence record
            let occurrences = []
            transaction = await sequelize.transaction(async transaction => {
                let occurrencesQuery = format(`
                    INSERT INTO experiment.occurrence (
                        occurrence_code, occurrence_name, occurrence_number, occurrence_status, description, experiment_id,
                        geospatial_object_id, occurrence_document, rep_count, creator_id, site_id, field_id
                    )
                    VALUES 
                        %L
                    RETURNING id`, occurrenceValuesArray
                )
                occurrences = await sequelize.query(occurrencesQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })

                for (occurrence of occurrences[0]) {
                    occurrenceDbId = occurrence.id

                    // Return the occurrence info
                    let array = {
                        occurrenceDbId: occurrenceDbId,
                        recordCount: 1,
                        href: occurrenceUrlString + '/' + occurrenceDbId
                    }
                    resultArray.push(array)
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}