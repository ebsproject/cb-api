/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token.js')
let occurrenceHelper = require('../../../helpers/occurrence/index.js')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let userValidator = require('../../../helpers/person/validator.js')

module.exports = {

    /**
     * Retrieve occurrence information and
     * the collected plot data variables
     * POST /v3/occurrence-traits-search
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    post: async function (req, res, next){
        // set default
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ``
        let conditionString = ''
        let distinctString = ''
        let fields = null
        let distinctOn = ''
        let orderQuery = `ORDER BY "occurrence".id`
        let ownedConditionString = ''
        let accessDataQuery = ''

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req);

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if user is admin or not
        let isAdmin = await userValidator.isAdmin(userId)

        // if not admin, retrieve only occurrences user have access to
        if(!isAdmin && params.ownershipType == 'shared'){
            // get all programs that user belongs to
            let getProgramsQuery =  `
            SELECT 
                program.id 
            FROM 
                tenant.program program,
                tenant.program_team pt,
                tenant.team_member tm
            WHERE
                tm.person_id = ${userId}
                AND pt.team_id = tm.team_id
                AND program.id = pt.program_id
                AND tm.is_void = FALSE
                AND pt.is_void = FALSE
                AND program.is_void = FALSE
            `
            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                accessDataQuery += `occurrence.access_data #> $$\{program,${programObj.id}}$$ is not null`

            }

            // include occurrences shared to the user
            let userQuery = `occurrence.access_data #> $$\{person,${userId}}$$ is not null`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            }

            if(accessDataQuery != ``){
                accessDataQuery = ` AND (${accessDataQuery})`
            }
        }

        // Default values
        if (req.body != undefined) {
            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn'
            ]

            if (req.body.fields != null) {
                fields = req.body.fields
            }

            // Get filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body, 
                excludedParametersArray
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn !== undefined) {
                distinctOn = req.body.distinctOn
                distinctString = await processQueryHelper
                    .getDistinctString(distinctOn)

                distinctOn = `"${distinctOn}"`
            }
        }

        // Start building the query
        let occurrenceQuery = null

        if (params.isOwned !== undefined) {
            ownedConditionString = ` AND (creator.id = ${userId} OR steward.id = ${userId}) `
        }

        let isBasic = false
        if (params.isBasic !== undefined && params.isBasic == 'true') {
            isBasic = true
        }

        // Check if client specifies field
        if (fields != null) {
            if (distinctOn) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(fields)
                let selectString = knex.raw(`${fieldsString}`)
                occurrenceQuery = knex.select(selectString)
            } else {
                occurrenceQuery = knex.column(fields.split('|'))
            }

            // retrieve only basic attributes
            if (isBasic) {
                occurrenceQuery += `
                    FROM
                        experiment.occurrence occurrence
                    LEFT JOIN
                        experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                    LEFT JOIN
                        experiment.location location ON location.id = log.location_id
                    LEFT JOIN
                        place.geospatial_object site ON site.id = occurrence.site_id
                    LEFT JOIN
                        place.geospatial_object field ON field.id = occurrence.field_id,
                        experiment.experiment experiment
                    LEFT JOIN
                        tenant.stage stage on stage.id = experiment.stage_id
                    LEFT JOIN
                        tenant.season season on season.id = experiment.season_id
                    LEFT JOIN 
                        tenant.program program ON program.id = experiment.program_id
                    WHERE
                        occurrence.experiment_id = experiment.id
                        AND occurrence.is_void = FALSE
                            ${accessDataQuery}
                            ${ownedConditionString}
                `
            } else{

                occurrenceQuery += `
                    FROM
                    experiment.occurrence occurrence
                LEFT JOIN
                    experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                LEFT JOIN
                    experiment.location location ON location.id = log.location_id
                LEFT JOIN 
                    tenant.person location_steward ON location_steward.id = location.steward_id
                JOIN
                    place.geospatial_object site ON site.id = occurrence.site_id
                LEFT JOIN
                    place.geospatial_object field ON field.id = occurrence.field_id
                JOIN 
                    tenant.person creator ON creator.id = occurrence.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = occurrence.modifier_id
                JOIN
                    experiment.experiment experiment ON occurrence.experiment_id = experiment.id
                JOIN
                    tenant.stage stage on stage.id = experiment.stage_id
                JOIN
                    tenant.season season on season.id = experiment.season_id
                JOIN 
                    tenant.program program ON program.id = experiment.program_id
                LEFT JOIN 
                    tenant.person steward ON steward.id = experiment.steward_id
                LEFT JOIN LATERAL (
                        SELECT
                            array_agg(
                                json_build_object(
                                    'variableDbId',var.id,
                                    'variableAbbrev',var.abbrev,
                                    'variableDataType',var.data_type
                                )
                            )
                        FROM (
                                SELECT
                                    pd.variable_id
                                FROM
                                    experiment.plot AS p
                                    INNER JOIN experiment.plot_data pd
                                        ON pd.plot_id = p.id
                                WHERE
                                    p.occurrence_id = occurrence.id
                                    AND p.is_void = FALSE
                                    AND pd.is_void = FALSE
                                GROUP BY
                                    pd.variable_id
                            ) AS t (
                                variable_id
                            )
                            INNER JOIN master.variable AS var
                                ON var.id = t.variable_id
                    ) AS t (
                        collected_traits
                    ) ON TRUE
                WHERE
                    occurrence.experiment_id = experiment.id
                    AND occurrence.is_void = FALSE
                    ${accessDataQuery}
                    ${ownedConditionString}
                `
            }

        } else {
            occurrenceQuery = await occurrenceHelper.getOccurrenceTraitQuerySql(
                '',
                '',
                '',
                distinctString,
                ownedConditionString,
                isBasic,
                accessDataQuery
            )
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400057)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        let occurrenceFinalSqlQuery = await processQueryHelper.getOccurrenceFinalSqlQuery(
            occurrenceQuery,
            conditionString,
            orderString,
            distinctString
        )

        // Retrieve occurrences from the database   
        let occurrences = await sequelize.query(occurrenceFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrences === undefined || await occurrences.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get count 
        let occurrenceCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            occurrenceQuery, 
            conditionString, 
            orderString,
            distinctString
        )

        let occurrenceCount = await sequelize.query(occurrenceCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = occurrenceCount[0].count

        res.send(200, {
            rows: occurrences,
            count: count
        })
        return
    }
}