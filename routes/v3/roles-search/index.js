/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(req.body, excludedParametersArray)
        
      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let rolesQuery = null
    // Check if the client specified values for the field/s
    if (parameters['fields']) {
      rolesQuery = knex.column(parameters['fields'].split('|'))
      rolesQuery += `
        FROM
          tenant.person_role role
        LEFT JOIN
          tenant.person creator ON role.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON role.modifier_id = modifier.id
        WHERE
          role.is_void = FALSE
        ` + addedConditionString + `
        ORDER BY
          role.id
      `
    } else {
      rolesQuery = `
        SELECT 
          role.id AS "roleDbId",
          role.person_role_code AS "personRoleCode",
          role.person_role_name AS "personRoleName",
          role.description,
          role.notes,
          role.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          role.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          tenant.person_role role
        LEFT JOIN
          tenant.person creator ON role.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON role.modifier_id = modifier.id
        WHERE
          role.is_void = FALSE
        ` + addedConditionString + `
        ORDER BY
          role.id
        `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    let rolesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      rolesQuery,
      conditionString,
      orderString
    )
    // Retrieve the entry records
    let roles = await sequelize
      .query(rolesFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })
    

    if (await roles == undefined || await roles.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      let rolesCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          rolesQuery,
          conditionString,
          orderString
        )
      let rolesCount = await sequelize
        .query(rolesCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })
      count = rolesCount[0].count
    }

    res.send(200, {
      rows: roles,
      count: count
    })
    return
  }
}