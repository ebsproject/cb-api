/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require("../../../config/sequelize")
let errors = require("restify-errors")
let errorBuilder = require("../../../helpers/error-builder")
let tokenHelper = require("../../../helpers/auth/token.js")
let format = require("pg-format")
let forwarded = require("forwarded-for")
let validator = require("validator")

module.exports = {
    // Implementation of POST call for /v3/program-configurations
    post: async function (req, res, next) {
        let creatorId = await tokenHelper.getUserId(req)

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let programConfigurationsUrlString =
            (isSecure ? "https" : "http") +
            "://" +
            req.headers.host +
            "/v3/program-configurations"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            // Records = [{}...]
            records = data.records
            // Check if the input data is empty
            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Start transaction
        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let programConfigurationsValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0
                let programDbId = null
                let configDbId = null
                let notes = null

                // Check if the required colums are in the input
                if (
                    record.programDbId === undefined ||
                    record.configDbId === undefined
                ) {
                    let errMsg = `Required parameters are missing. Ensure that Program ID and Config Id fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // validate program id
                if (record.programDbId !== undefined) {
                    programDbId = record.programDbId

                    if (!validator.isInt(programDbId)) {
                        let errMsg = `Invalid format, program ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate ProgramId ID
                    validateQuery += validateQuery != "" ? " + " : ""
                    validateQuery += `
                        (
                            --- Check if Program Id is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.program program
                            WHERE 
                                program.is_void = FALSE AND
                                program.id = ${programDbId}
                        )
                    `
                    validateCount += 1
                }

                // validate config id
                if (record.configDbId !== undefined) {
                    configDbId = record.configDbId

                    if (!validator.isInt(configDbId)) {
                        let errMsg = `Invalid format, program ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    /* Validate and set values */
                    // validate if the configDbId is in the database
                    validateQuery += validateQuery != "" ? " + " : ""
                    validateQuery += `
                        (
                            --- Check if Config Id is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                platform.config config
                            WHERE 
                                config.is_void = FALSE AND
                                config.id = ${configDbId}
                        )
                    `
                    validateCount += 1
                }

                //  fields to set value
                if (record.notes !== undefined) notes = record.notes

                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                    
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT,
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [programDbId, configDbId, creatorId, notes]

                programConfigurationsValuesArray.push(tempArray)
            }
            // Create programConfiguration record
            let insertProgramConfigurationsQuery = format(`
                INSERT INTO tenant.program_config (
                    program_id, config_id,creator_id,notes
                ) 
                VALUES 
                    %L
                RETURNING id`, programConfigurationsValuesArray
            )

            let programConfigurations = await sequelize
                .query(insertProgramConfigurationsQuery,{
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                })

            let resultArray = []

            for (let programConfiguration of programConfigurations[0]) {
                let programConfigurationDbId = programConfiguration.id

                // Return the program configuration info to Client
                let array = {
                    programConfigurationDbId: programConfigurationDbId,
                    recordCount: 1,
                    href: programConfigurationsUrlString + "/" + programConfigurationDbId
                }

                resultArray.push(array)
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray,
                count: resultArray.length,
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },
}