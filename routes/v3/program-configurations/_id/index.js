/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize")
let errors = require("restify-errors")
let validator = require("validator")
let errorBuilder = require("../../../../helpers/error-builder")
let tokenHelper = require("../../../../helpers/auth/token")

module.exports = {
    // Implementation of delete call for /v3/program-configurations/{id}
    delete: async function (req, res, next) {
        let userDbId = await tokenHelper.getUserId(req)

        // Defaults
        let programConfigDbId = req.params.id

        if (!validator.isInt(programConfigDbId)) {
            let errMsg = "Invalid format, program configuration ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Start transaction
        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            // Build query
            let programConfigurationQuery = `
                SELECT
                    count(1)
                FROM 
                    tenant.program_config programConfig
                WHERE
                    programConfig.is_void = FALSE AND
                    programConfig.id = ${programConfigDbId}
            `

            // Retrieve program configuration from the database
            let hasProgramConfiguration = await sequelize
                .query(programConfigurationQuery, {
                    type: sequelize.QueryTypes.SELECT,
                })
                .catch(async (err) => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Check if program configuration is non-existent in database
            if (
                hasProgramConfiguration == undefined ||
                hasProgramConfiguration[0].count == 0
            ) {
                let errMsg = `The program configuration you requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Build query for voiding
            let deleteprogramConfigurationQuery = `
                UPDATE
                    tenant.program_config programConfig
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    programConfig.id = ${programConfigDbId}
            `

            await sequelize.query(deleteprogramConfigurationQuery, {
                type: sequelize.QueryTypes.UPDATE,
            })

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: {
                    programConfigDbId: programConfigDbId,
                }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
