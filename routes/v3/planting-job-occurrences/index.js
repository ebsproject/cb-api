/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let logger = require('../../../helpers/logger')

const enpoint = 'planting-job-occurrences'

module.exports = {
    post: async function (req, res, next) {
        /**
         *  Retrieve the user ID via access token in the request body
         *  and verify if the user that the ID points to exists
         */

        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isSecure = forwarded(req, req.headers).isSecure

        let plantingJobOccurrenceUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/planting-job-occurrences'

        /**
         *  Check if the request body exists since it is
         *  required in POST creation resources; else, end
         *  the function immediately and return an error
         */

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        /**
         * Set default values to the variables
         */

        let resultArray = []
        let recordCount = 0
        let data = req.body
        let records = []

        /**
         * Check if the records array exists
         * and if it has elements
         */
        try {
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Retrieve the valid units for planting job occurrences
        let validUnitsQuery = `
            SELECT
                array_agg(value)
            FROM
                master.scale_value
            WHERE
                is_void = FALSE AND
                scale_id = (
                    SELECT
                        scale_id
                    FROM
                        master.variable
                    WHERE
                        abbrev = 'PACKAGE_UNIT'
                )
        `
        
        let units = await sequelize
            .query(validUnitsQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500006)
                res.send(new errors.InternalError(errMsg))
                return
            })

        let transaction
        try {
            let plantingJobOccurrenceValuesArray = []

            for (let record of records) {
                /**
                 * @validateQuery the validation query which returns the count of valid checks
                 * @validateCount a local counter that is counter-checked with the return value of the validateQuery
                 */
                let validateQuery = ''
                let validateCount = 0

                let plantingJobDbId = null // required
                let occurrenceDbId = null //required
                let seedsPerEnvelope = 1 // non-required, default is 1
                let packageUnit = 'g' // non-required, default is g
                let envelopesPerPlot = 1 // non-required, default is 1
                let notes = null

                // Check if required columns exist in the request body
                if (
                    !record.plantingJobDbId ||
                    !record.occurrenceDbId
                ) {
                    let errMsg = 'Required parameters are missing. Ensure that the following attributes: plantingJobDbId, occurrenceDbId are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /**
                 * Perform column validations here
                 */

                plantingJobDbId = record.plantingJobDbId
                if (!validator.isInt(plantingJobDbId)) {
                    let errMsg = 'Invalid request, plantingJobDbId must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                // Create validation query clause for plantingJobDbId
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        -- Check if planting job exists, return 1
                        SELECT
                            count(1)
                        FROM
                            experiment.planting_job
                        WHERE
                            is_void = FALSE AND
                            id = ${plantingJobDbId}
                    )
                `

                validateCount += 1

                occurrenceDbId = record.occurrenceDbId
                if (!validator.isInt(occurrenceDbId)) {
                    let errMsg = 'Invalid request, occurrenceDbId must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                // Create validation query clause for occurrenceDbId
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        -- Check if occurrence exists, return 1
                        SELECT
                            count(1)
                        FROM
                            experiment.occurrence
                        WHERE
                            is_void = FALSE AND
                            id = ${occurrenceDbId}
                    )
                `
                
                validateCount += 1

                if (record.seedsPerEnvelope !== undefined) {
                    seedsPerEnvelope = record.seedsPerEnvelope
                    if (!validator.isInt(seedsPerEnvelope)) {
                        let errMsg = 'Invalid request, seedsPerEnvelope must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.packageUnit !== undefined) {
                    packageUnit = record.packageUnit
                    if (!units[0]["array_agg"].includes(packageUnit.toLowerCase())) {
                        let errMsg = 'Invalid request, unit value must be one of the following: kg, seeds, pan, g, panicles, plant'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.envelopesPerPlot !== undefined) {
                    envelopesPerPlot = record.envelopesPerPlot
                    if (!validator.isInt(envelopesPerPlot)) {
                        let errMsg = 'Invalid request, envelopesPerPlot must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                notes = (record.notes !== undefined) ? record.notes : ''

                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS
                        count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            
                let temp = [
                    plantingJobDbId,
                    occurrenceDbId,
                    seedsPerEnvelope,
                    packageUnit,
                    envelopesPerPlot,
                    notes,
                    personDbId
                ]

                plantingJobOccurrenceValuesArray.push(temp)
            }

            // Create a planting job occurrence insertion query
            let plantingJobOccurrenceQuery = format(`
                INSERT INTO
                    experiment.planting_job_occurrence (
                        planting_job_id,
                        occurrence_id,
                        seeds_per_envelope,
                        package_unit,
                        envelopes_per_plot,
                        notes,
                        creator_id
                    )
                VALUES
                    %L
                RETURNING id
            `, plantingJobOccurrenceValuesArray)

            let plantingJobOccurrences

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                plantingJobOccurrences = await sequelize.query(plantingJobOccurrenceQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(enpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (let plantingJobOccurrence of plantingJobOccurrences[0]) {
                let plantingJobOccurrenceDbId = plantingJobOccurrence.id

                let plantingJobOccurrenceRecord = {
                    plantingJobOccurrenceDbId: plantingJobOccurrenceDbId,
                    recordCount: 1,
                    href: plantingJobOccurrenceUrlString + '/' + plantingJobOccurrenceDbId
                }

                resultArray.push(plantingJobOccurrenceRecord)
            }

            res.send(200, {
                rows: resultArray
            })
        }
        catch (err) {
            // Show logs when error encountered
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}