/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
let logger = require('../../../../helpers/logger')

const enpoint = 'planting-job-occurrences/:id'

module.exports = {

    /**
     * Update planting job occurrence record
     * PUT /v3/planting-job-occurrences/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    put: async function (req, res, next) {
        let plantingJobOccurrenceDbId = req.params.id

        if (!validator.isInt(plantingJobOccurrenceDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400229)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let seedsPerEnvelope = null
        let envelopesPerPlot = null
        let packageUnit = null
        let notes = null

        // Retrieve personDbId of the client from the access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plantingJobOccurrenceQuery = `
            SELECT
                p.seeds_per_envelope AS "seedsPerEnvelope",
                p.envelopes_per_plot AS "envelopesPerPlot",
                p.package_unit AS "packageUnit",
                p.notes AS "notes"
            FROM
                experiment.planting_job_occurrence AS p
            WHERE
                p.is_void = FALSE AND
                p.id = ${plantingJobOccurrenceDbId}
        `

        let plantingJobOccurrence = await sequelize
            .query(plantingJobOccurrenceQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJobOccurrence == undefined ||
            await plantingJobOccurrence.length < 1
        ) {
            let errMsg = 'The planting job occurrence you have requested does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let recordSeedsPerEnvelope = plantingJobOccurrence[0].seedsPerEnvelope
        let recordEnvelopesPerPlot = plantingJobOccurrence[0].envelopesPerPlot
        let recordPackageUnit = plantingJobOccurrence[0].packageUnit
        let recordNotes = plantingJobOccurrence[0].notes

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let plantingJobOccurrenceUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + "/v3/planting-job-occurrences/"
            + plantingJobOccurrenceDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        // Retrieve the valid units for planting job occurrences
        let validUnitsQuery = `
            SELECT
                array_agg(value)
            FROM
                master.scale_value
            WHERE
                is_void = FALSE AND
                scale_id = (
                    SELECT
                        scale_id
                    FROM
                        master.variable
                    WHERE
                        abbrev = 'PACKAGE_UNIT'
                )
        `

        let units = await sequelize
            .query(validUnitsQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500006)
                res.send(new errors.InternalError(errMsg))
                return
            })


        let transaction
        try {

            let setQuery = ``

            if (data.seedsPerEnvelope !== undefined) {
                seedsPerEnvelope = data.seedsPerEnvelope
                if (seedsPerEnvelope != recordSeedsPerEnvelope) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        seeds_per_envelope = $$${seedsPerEnvelope}$$
                    `
                }
            }

            if (data.envelopesPerPlot !== undefined) {
                envelopesPerPlot = data.envelopesPerPlot
                if (envelopesPerPlot != recordEnvelopesPerPlot) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        envelopes_per_plot = $$${envelopesPerPlot}$$
                    `
                }
            }

            if (data.packageUnit !== undefined) {
                packageUnit = data.packageUnit
                if (packageUnit != recordPackageUnit) {
                    if (!units[0]["array_agg"].includes(packageUnit)) {
                        let errMsg = 'Invalid request, unit value must be one of the following: kg, seeds, pan, g, panicles, plant'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_unit = $$${packageUnit}$$
                    `
                }
            }

            if (data.notes !== undefined) {
                notes = data.notes
                if (notes != recordNotes) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        notes = $$${notes}$$
                    `
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the plantingJob occurrence record
            let updatePlantingJobOccurrenceQuery = `
                UPDATE
                    experiment.planting_job_occurrence
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${plantingJobOccurrenceDbId}
            `

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updatePlantingJobOccurrenceQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(enpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return the transaction info
            let result = {
                plantingJobOccurrenceDbId: plantingJobOccurrenceDbId,
                recordCount: 1,
                href: plantingJobOccurrenceUrlString
            }

            res.send(200, {
                rows: result
            })

            return
        } catch (err) {
            // Log error message
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Delete planting job occurrence record
     * DELETE /v3/planting-job-occurrences/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    delete: async function (req, res, next) {
        let plantingJobOccurrenceDbId = req.params.id

        if (!validator.isInt(plantingJobOccurrenceDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400229)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plantingJobOccurrenceQuery = `
            SELECT
                p.planting_job_status AS "plantingJobStatus"
            FROM
                experiment.planting_job_occurrence o
            LEFT JOIN
                experiment.planting_job p ON o.planting_job_id = p.id
            WHERE
                o.id = ${plantingJobOccurrenceDbId} AND
                o.is_void = FALSE
        `

        let plantingJobOccurrence = await sequelize
            .query(plantingJobOccurrenceQuery, {
                type: sequelize.QueryTypes.SELECT
            })

        if (
            await plantingJobOccurrence == undefined ||
            await plantingJobOccurrence.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404052)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let plantingJobStatus = plantingJobOccurrence[0].plantingJobStatus

        if (plantingJobStatus == 'packed') {
            let errMsg = await errorBuilder.getError(req.headers.host, 403012)
            res.send(new errors.ForbiddenError(errMsg))
            return
        }

        let transaction
        try {

            let deletePlantingJobOccurrenceQuery = format(`
                UPDATE
                    experiment.planting_job_occurrence
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${plantingJobOccurrenceDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${plantingJobOccurrenceDbId}
            `)

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(deletePlantingJobOccurrenceQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(enpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            res.send(200, {
                rows: {
                    plantingJobOccurrenceDbId: plantingJobOccurrenceDbId
                }
            })

            return
        } catch (err) {
            // Log message when error encountered
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}