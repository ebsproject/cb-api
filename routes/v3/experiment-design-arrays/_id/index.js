/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

async function getExperiments(experimentDbId) {
    // Retrieve the information of a experiment
    let experimentQuery = `
        SELECT
            experiment.id AS "experimentId",
            experiment.experiment_name AS "experimentName",
            experiment.experiment_code as "experimentCode",
            experiment.steward_id AS "experimentStewardId",
            experiment.program_id AS "programId",
            experiment.project_id AS "projectId",
            experiment.experiment_design_type AS "statDesignType",
            experiment.experiment_type AS "experimentType",
            experiment.experiment_sub_type AS "experimentSubType",
            experiment.experiment_sub_sub_type AS "experimentSubSubType",
            experiment.season_id AS "seasonId",
            stage.stage_name AS stage,
            experiment.experiment_year AS year
        FROM 
            experiment.experiment experiment
            LEFT JOIN
                tenant.stage stage
                ON stage.id = experiment.stage_id
        WHERE 
            experiment.is_void = FALSE
            AND experiment.id = :experimentDbId
    `

    let experiments = await sequelize.query(experimentQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            experimentDbId: experimentDbId
        }
    })

    if (await experiments.length > 0) {
        return await experiments
    } else {
        return []
    }
}

async function getEntryLists(experimentDbId) {
    // Retrieve entry lists in a experiment
    let entryListQuery = `
        SELECT
            entry.id AS "entryId",
            entry.entry_number AS "entryNumber",
            entry.germplasm_id AS "germplasmId",
            germplasm.designation AS "germplasmName",
            germplasm.parentage AS "parentage",
            seed.id AS "seedId",
            entry.entry_type AS "entryType",
            entry.entry_role AS "entryRole"
        FROM
            experiment.entry_list "entryList",
            experiment.entry entry
            LEFT JOIN 
                germplasm.germplasm germplasm
                ON germplasm.id = entry.germplasm_id
                AND germplasm.is_void = FALSE
            LEFT JOIN
                germplasm.seed seed
                ON seed.id = entry.seed_id
                AND seed.is_void = FALSE
        WHERE
            "entryList".id = entry.entry_list_id
            AND "entryList".is_void = FALSE
            AND entry.is_void = FALSE
            AND "entryList".experiment_id = :experimentDbId
    `

    let entryLists = await sequelize.query(entryListQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            experimentDbId: experimentDbId
        }
    })

    if (await entryLists.length > 0) {
        return await entryLists
    } else {
        return []
    }
}

async function getTraitLists(experimentDbId) {
    // Retrieve trait lists in a experiment
    let traitListQuery = `
        SELECT
            "listMember".data_id AS "traitId",
            protocol.id AS "traitProtocolId"
        FROM
            experiment.experiment_protocol "experimentProtocol",
            tenant.protocol protocol,
            tenant.protocol_data "protocolData",
            platform.list list,
            platform.list_member "listMember"
        WHERE
            "experimentProtocol".experiment_id = :experimentDbId
            AND "experimentProtocol".protocol_id = protocol.id
            AND protocol.id = "protocolData".protocol_id
            AND "protocolData".is_void = FALSE
            AND protocol.protocol_type = 'trait protocol'
            AND "protocolData".data_value::int = list.id
            AND list.type = 'trait_protocol'
            AND list.id = "listMember".list_id
    `

    let traitLists = await sequelize.query(traitListQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            experimentDbId: experimentDbId
        }
    })

    if (await traitLists.length > 0) {
        return await traitLists
    } else {
        return []
    }
}

async function getOccurrences(experimentDbId) {
    // Retrieve occurrences in a experiment
    let occurrenceQuery = `
        SELECT
            occurrence.id AS "occurrenceId",
            "experimentGroup".id AS "experimentGroupId",
            "experimentGroup".experiment_group_name AS "experimentGroupName",
            occurrence.occurrence_name AS "occurrenceName",
            occurrence.description AS "occurrenceRemarks",
            occurrence.site_id AS "siteId",
            occurrence.field_id AS "fieldId",
            location.id AS "locationId",
            location.geospatial_object_id AS "plantingAreaId",
            null AS "occurrenceManagementId",
            null AS "plotTypeName",
            location.location_planting_date AS "plantingDate"
        FROM
            experiment.experiment experiment
            LEFT JOIN 
                experiment.occurrence occurrence
                ON occurrence.experiment_id = experiment.id
                AND occurrence.is_void = FALSE
            LEFT JOIN
                experiment.experiment_group_member "experimentGroupMember"
                ON "experimentGroupMember".experiment_id = occurrence.experiment_id
                AND "experimentGroupMember".is_void = FALSE
            LEFT JOIN
                experiment.experiment_group "experimentGroup"
                ON "experimentGroup".id =  "experimentGroupMember".experiment_group_id
                AND "experimentGroup".is_void = FALSE
            LEFT JOIN 
                experiment.location_occurrence_group "locationOccurrenceGroup"
                ON "locationOccurrenceGroup".occurrence_id = occurrence.id
                AND "locationOccurrenceGroup".is_void = FALSE
            LEFT JOIN 
                experiment.location location
                ON location.id = "locationOccurrenceGroup".location_id
                AND location.is_void = FALSE
            LEFT JOIN
                place.geospatial_object "geospatialObject"
                ON "geospatialObject".id = location.geospatial_object_id
                AND "geospatialObject".is_void = FALSE
        WHERE
            experiment.id =  :experimentDbId
    `

    let occurrences = await sequelize.query(occurrenceQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            experimentDbId: experimentDbId
        }
    })

    if (await occurrences.length > 0) {
        return await occurrences
    } else {
        return []
    }
}

async function getPlotParameters(experimentDbId) {
    // Retrieve the plot information of an occurence in an experiment
    let plotParameterQuery = `
        SELECT
        (
            SELECT
                "protocolData".data_value
            FROM
                tenant.protocol_data "protocolData",
                master.variable variable
            WHERE 
                protocol.id = "protocolData".protocol_id
                AND "protocolData".is_void = FALSE
                AND "protocolData".variable_id = variable.id
                AND variable.is_void = FALSE
                AND variable.abbrev = 'PLOT_LN'
            LIMIT 1
        ) AS plotLength,
        (
            SELECT
                unit
            FROM
                master.variable variable,
                master.scale scale
            WHERE
                variable.abbrev = 'PLOT_LN' 
                AND variable.is_void = FALSE
                AND variable.scale_id = scale.id
                AND scale.is_void = FALSE
        ) AS plotLengthUnit,
        (
            SELECT
                "protocolData".data_value
            FROM
                tenant.protocol_data "protocolData",
                master.variable variable
            WHERE 
                protocol.id = "protocolData".protocol_id
                AND "protocolData".is_void = FALSE
                AND "protocolData".variable_id = variable.id
                AND variable.is_void = FALSE
                AND variable.abbrev = 'PLOT_WIDTH'
            LIMIT 1
        ) AS plotWidth,
        (
            SELECT
                unit
            FROM
                master.variable variable,
                master.scale scale
            WHERE
                variable.abbrev = 'PLOT_WIDTH' 
                AND variable.is_void = FALSE
                AND variable.scale_id = scale.id
                AND scale.is_void = FALSE
        ) AS plotWidthUnit,
        (
            SELECT
                "protocolData".data_value
            FROM
                tenant.protocol_data "protocolData",
                master.variable variable
            WHERE 
                protocol.id = "protocolData".protocol_id
                AND "protocolData".is_void = FALSE
                AND "protocolData".variable_id = variable.id
                AND variable.is_void = FALSE
                AND variable.abbrev = 'ALLEY_LENGTH'
            LIMIT 1
        ) AS alleyLength,
        (
            SELECT
                unit
            FROM
                master.variable variable,
                master.scale scale
            WHERE
                variable.abbrev = 'ALLEY_LENGTH' 
                AND variable.is_void = FALSE
                AND variable.scale_id = scale.id
                AND scale.is_void = FALSE
        ) AS alleyLengthUnit,
        null AS plantingSurfaceCode,
        (
            SELECT
                "protocolData".data_value
            FROM
                tenant.protocol_data "protocolData",
                master.variable variable
            WHERE 
                protocol.id = "protocolData".protocol_id
                AND "protocolData".is_void = FALSE
                AND "protocolData".variable_id = variable.id
                AND variable.is_void = FALSE
                AND variable.abbrev = 'ROWS_PER_PLOT_CONT'
            LIMIT 1
        ) AS rowsPerPlot
    FROM
        experiment.experiment_protocol "experimentProtocol",
        tenant.protocol protocol
    WHERE
        protocol.id = "experimentProtocol".protocol_id
        AND protocol.is_void = FALSE
        AND protocol.protocol_type = 'planting protocol'
        AND "experimentProtocol".experiment_id = :experimentDbId
    `

    let plotParameters = await sequelize.query(plotParameterQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            experimentDbId: experimentDbId
        }
    })

    if (await plotParameters.length > 0) {
        return await plotParameters
    } else {
        return []
    }
}

async function getPlots(occurrenceDbId) {
    // Retrieve the plots of an occurrence
    let plotQuery = `
        SELECT
            plot.id AS "plotId",
            plot.entry_id AS "entryId",
            plot.plot_number AS "plotNumber",
            plot.rep,
            plot.block_number block,
            (
                SELECT
                    JSON_AGG(
                        json_build_object(
                        'block_type', experimentDesign.block_type,
                        'blockValue', experimentDesign.block_value,
                        'blockLevelNumber', experimentDesign.block_level_number
                        )
                    )
                FROM
                    experiment.experiment_design experimentDesign
                WHERE
                    experimentDesign.plot_id = plot.id
            ) AS designBlocks,
            plot.design_x AS "designX",
            plot.design_y AS "designY",
            plot.pa_x AS "paX",
            plot.pa_y AS "paY",
            plot.field_x AS "fieldX",
            plot.field_y AS "fieldY",
            null AS barcode
        FROM
            experiment.plot plot
        WHERE 
            plot.is_void = FALSE
            AND plot.occurrence_id = :occurrenceDbId
    `

    let plots = await sequelize.query(plotQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            occurrenceDbId: occurrenceDbId
        }
    })

    if (await plots.length > 0) {
        return await plots
    } else {
        return []
    }
}

async function getSites(siteDbIdStr) {
    // Retrieve site information
    let siteQuery = `
        SELECT
            "geospatialObject".id AS "siteId",
            "geospatialObject".geospatial_object_code AS "siteCode",
            "geospatialObject".geospatial_object_name AS "siteName",
            "geospatialObject".geospatial_object_type AS type
        FROM
            place.geospatial_object "geospatialObject"
        WHERE
            "geospatialObject".is_void = FALSE
            AND "geospatialObject".id IN (
                ${siteDbIdStr}    
            )
    `

    let sites = await sequelize.query(siteQuery, {
        type: sequelize.QueryTypes.SELECT,
    })

    if (await sites.length > 0) {
        return await sites
    } else {
        return []
    }
}

async function getCountries(siteDbId) {
    // Retrieve country of a site
    let countryQuery = `
        SELECT
            geospatial_object_code AS code
        FROM 
            place.geospatial_object
        WHERE
            id IN
            (
                with RECURSIVE parentsearch AS (
                    SELECT 
                        id, 
                        parent_geospatial_object_id AS "parentId" 
                    FROM 
                        place.geospatial_object 
                    WHERE 
                        id = :siteDbId
                    UNION
                    SELECT 
                        t.id, 
                        t.parent_geospatial_object_id AS "parentId" 
                    FROM 
                        place.geospatial_object t 
                        JOIN parentsearch ps 
                        ON t.id = ps."parentId" 
                    )
                    SELECT 
                        "parentId" 
                    FROM parentsearch where "parentId" IS NULL
            ) 
            AND geospatial_object_subtype = 'country'
        `

    let countries = await sequelize.query(countryQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            siteDbId: siteDbId
        }
    })

    if (await countries.length > 0) {
        return await countries
    } else {
        return []
    }
}

async function getFields(siteDbId) {
    // Retrieve field information of a site
    let fieldQuery = `
        SELECT
            facility.id AS "fieldId",
            facility.facility_name AS "fieldName"
        FROM
            place.facility facility,
            place.geospatial_object "geospatialObject"
        WHERE
            "geospatialObject".id = :siteDbId
            AND "geospatialObject".id = facility.geospatial_object_id
            AND facility.is_void = FALSE
            AND "geospatialObject".is_void = FALSE
    `

    let fields = await sequelize.query(fieldQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            siteDbId: siteDbId
        }
    })

    if (await fields.length > 0) {
        return await fields
    } else {
        return []
    }
}

async function getPrograms(programDbId) {
    // Retrieve programs
    let programQuery = `
        SELECT
            program.id AS "programId",
            program.program_code AS "programCode",
            program.program_name AS "programName",
            program.description
        FROM
            tenant.program program
        WHERE
            program.is_void = FALSE
            AND program.id = :programDbId
    `

    let programs = await sequelize.query(programQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            programDbId: programDbId
        }
    })

    if (await programs.length > 0) {
        return await programs
    } else {
        return []
    }
}

async function getProjects(projectDbId) {
    // Retrieve projects
    let projectQuery = `
        SELECT
            project.id AS "projectId",
            project.project_code AS "projectCode",
            project.project_name AS "projectName",
            project.description
        FROM
            tenant.project project
        WHERE
            project.is_void = FALSE
            AND project.id = :projectDbId
    `

    let projects = await sequelize.query(projectQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            projectDbId: projectDbId
        }
    })

    if (await projects.length > 0) {
        return await projects
    } else {
        return []
    }
}

async function getSeasons(seasonDbId) {
    // Retrieve season
    let seasonQuery = `
        SELECT
            season.id AS "seasonId",
            season.season_code AS "seasonCode",
            season.season_name AS "seasonName"
        FROM
            tenant.season season
        WHERE 
            season.is_void = FALSE
            AND season.id = :seasonDbId
        `

    let seasons = await sequelize.query(seasonQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            seasonDbId: seasonDbId
        }
    })

    if (await seasons.length > 0) {
        return await seasons
    } else {
        return []
    }
}

async function getTraits(traitDbId) {
    //  Retrieve traits
    let traitQuery = `
        SELECT
            trait.id AS "traitId",
            trait.name,
            trait.abbrev AS code,
            trait.data_level AS level,
            trait.description,
            trait.data_type AS "dataType"
        FROM
            master.variable trait
        WHERE
            trait.is_void = FALSE
            AND trait.id = :traitDbId
    `

    traits = await sequelize.query(traitQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            traitDbId: traitDbId
        }
    })

    if (await traits.length > 0) {
        return await traits
    } else {
        return []
    }
}

async function getPersons(stewardDbId) {
    // Retrieve persons
    let personQuery = `
        SELECT
            person.id AS "personId",
            person.first_name AS "firstName",
            person.last_name AS "lastName",
            "programTeam".program_id AS "programId"
        FROM
            tenant.person person
            LEFT JOIN
                tenant.team_member "teamMember"
                ON "teamMember".person_id = person.id
                AND "teamMember".is_void = FALSE
            LEFT JOIN
                tenant.program_team "programTeam"
                ON "programTeam".team_id = "teamMember".team_id
                AND "programTeam".is_void = FALSE
        WHERE
            person.id = :stewardDbId
            AND person.is_void = FALSE
    `

    let persons = await sequelize.query(personQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            stewardDbId: stewardDbId
        }
    })

    if (await persons.length > 0) {
        return await persons
    } else {
        return []
    }
}

module.exports = {

    // Implementatio for GET /v3/experiment-design-arrays/:id
    get: async function (req, res, next) {
        // Retrieve the experiment ID
        let experimentDbId = req.params.id

        try {
            // Check if ID is valid
            if (!validator.isInt(experimentDbId)) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400025)
                res.send(new errors.BadRequestError(errMsg))
                return
            } else {

                // Initialize
                let experimentDesignArrays = {}

                let references = {}

                let siteDbIdStr = ''

                // Retrieve experiments
                let experiments = await getExperiments(experimentDbId)

                /**
                 * ---------------------------------------
                 * Set the information for experiments
                 * ---------------------------------------
                 */
                if (await experiments.length > 0) {
                        // Retrieve entry lists
                    let entryLists = await getEntryLists(experimentDbId)

                    experiments[0]['entryLists'] = await entryLists

                    // Retrieve trait lists
                    let traitLists = await getTraitLists(experimentDbId)

                    experiments[0]['traitLists'] = await traitLists

                    // Get occurrences
                    let occurrences = await getOccurrences(experimentDbId)

                   // let occurrenceDbId = null
                    if (await occurrences.length > 0) {
                        for await (occurrence of occurrences) {
                            // Retrieve occurrence Db IDs
                            let occurrenceDbId = await occurrence['occurrenceId']

                            // Retrieve site Db IDs
                            let siteDbId = await occurrence['siteId']
                            siteDbIdStr = (siteDbIdStr == '') ? siteDbId : siteDbIdStr + ', ' + siteDbId

                            // Retrieve plot parameters
                            let plotParameters = await getPlotParameters(experimentDbId)

                            occurrence['plotParameters'] = plotParameters

                            // Retrieve plots
                            let plots = await getPlots(occurrenceDbId)

                            occurrence['plots'] = await plots
                        }
                    }

                    experiments[0]['occurrences'] = await occurrences

                    /**
                     * ---------------------------------------
                     * Set the information for the reference
                     * ---------------------------------------
                     */
                    if (await occurrences.length > 0) {
                        // Retrieve sites
                        let sites = await getSites(siteDbIdStr)

                        if(await sites.length > 0) {
                            // Retrieve country of a site
                            let siteDbId = await sites[0]['siteId']

                            let countries = await getCountries(siteDbId)

                            sites[0]['countryCode'] = (await countries.length > 0) ? await countries[0]['code'] : null

                            // Retrieve fields
                            let fields = await getFields(siteDbId)
                            
                            sites[0]['fields'] = (await fields.length > 0) ? await fields : []

                            references["sites"] = await sites
                        } else {
                            references["sites"] = []
                        }
                    }

                    // Retrieve programs
                    let programDbId = await experiments[0]['programId']

                    let programs = await getPrograms(programDbId)

                    references["programs"] = await programs

                    // Retrieve projects
                    let projectDbId = await experiments[0]['projectId']

                    let projects = await getProjects(projectDbId)

                    references["projects"] = await projects

                    // Retrieve season
                    let seasonDbId = await experiments[0]['seasonId']

                    let seasons = await getSeasons(seasonDbId)

                    references["seasons"] = await seasons

                    // Retrieve trait info
                    let traitDbId = (await traitLists.length > 0) ? await traitLists[0]['traitId'] : null
                
                    if(traitDbId != null) {
                        let traits = await getTraits(traitDbId)

                        references["traits"] = await traits
                    } else {
                        references["traits"] = []
                    }

                    // Retrieve persons
                    let stewardDbId = await experiments[0]['experimentStewardId']

                    let persons = await getPersons(stewardDbId)

                    references["persons"] = await persons

                    references["locations"] = []

                    references["occurrenceManagements"] = []
                } else{
                    // Return error if resource is not found
                    let errMsg = await errorBuilder.getError(req.headers.host, 404008)
                    await res.send(new errors.NotFoundError(errMsg))
                    return
                }       

                experimentDesignArrays["experiments"] = await experiments

                experimentDesignArrays["locationPlots"] = []

                experimentDesignArrays["references"] = references

                await res.send(200, {
                    rows: experimentDesignArrays
                })
                return
            }
        } catch (err) {
            console.log('Error:')
            console.log(err)
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            await res.send(new errors.InternalError(errMsg))
            return
        }
    }
}