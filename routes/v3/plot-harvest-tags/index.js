/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let validator = require('validator')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    /**
     * Retrieve plots for harvest tags
     * GET /v3/plot-harvest-tags
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = []
        let occurrenceDbId

        // Retrieve the occurrence ID
        occurrenceDbId = params.occurrenceDbId

        // Check if occurrenceDbId is an integer
        if(!validator.isInt(occurrenceDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400241)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.occurrence_status AS "occurrenceStatus"
            FROM
                experiment.occurrence occurrence
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
        `

        // Find occurrence in the database
        let occurrence = await sequelize
            .query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If the occurrence does not exist, return an error
        if (await occurrence == undefined || await occurrence.length < 1) {
            let errMsg = 'Resource not found. The occurrence you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Build query
        let plotsQuery = `
        SELECT
			plots."plotDbId",
			plots."plotCode",
			plots."plotNumber",
			plots."entryDbId",
			plots."entryCode",
			plots."entryNumber",
			plots."germplasmName",
			plots."germplasmParentage",
			plots."germplasmState",
			plots."germplasmType",
			plots."seedName",
			plots."seedCode",
			plots."packageLabel",
			plots."packageCode",
			plots."locationDbId",
			plots."locationCode",
			plots."locationName",
			plots."occurrenceDbId",
			plots."occurrenceCode",
			plots."occurrenceName",
			plots."experimentDbId",
			plots."experimentCode",
			plots."experimentName",
			plots."experimentYear",
			plots."stageDbId",
			plots."stageName",
			plots."stageCode",
			plots."seasonDbId",
			plots."seasonName",
			plots."seasonCode",
			plots."siteDbId",
			plots."siteName",
			plots."siteCode",
			plots."programDbId",
			plots."programCode",
			plots."programName",
			plots."harvestMethod",
			plots."numericVariable",
			plots."cropName",
			plots."cropCode",
			plots."fieldX",
			plots."fieldY",
			plots."paX",
			plots."paY",
			plots."designX",
			plots."designY",
            CASE
                WHEN "arr"["key"] <> ' ' THEN "arr"["key"]
                ELSE null
            END AS "selection",
            GENERATE_SERIES(
                1,
                CASE
                    WHEN pd.data_value IS NULL THEN 1
                    ELSE pd.data_value::INTEGER
                END
            ) AS "bagNumber"
        FROM (
            SELECT
                plot.id AS "plotDbId",
                plot.plot_code AS "plotCode",
                plot.plot_number AS "plotNumber",
                entry.id AS "entryDbId",
                entry.entry_code AS "entryCode",
                entry.entry_number AS "entryNumber",
                germplasm.designation AS "germplasmName",
                germplasm.parentage AS "germplasmParentage",
                germplasm.germplasm_state AS "germplasmState",
                germplasm.germplasm_type AS "germplasmType",
                seed.seed_name AS "seedName",
                seed.seed_code AS "seedCode",
                package.package_label AS "packageLabel",
                package.package_code AS "packageCode",
                location.id AS "locationDbId",
                location.location_code AS "locationCode",
                location.location_name AS "locationName",
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                experiment.id AS "experimentDbId",
                experiment.experiment_code AS "experimentCode",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_year AS "experimentYear",
                stage.id AS "stageDbId",
                stage.stage_name AS "stageName",
                stage.stage_code AS "stageCode",
                season.id AS "seasonDbId",
                season.season_name AS "seasonName",
                season.season_code AS "seasonCode",
                site.id AS "siteDbId",
                site.geospatial_object_name AS "siteName",
                site.geospatial_object_code AS "siteCode",
                program.id AS "programDbId",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                harvest_method.data_value AS "harvestMethod",
                numeric_var.data_value AS "numericVariable",
                crop.crop_name AS "cropName",
                crop.crop_code AS "cropCode",
                plot.field_x AS "fieldX",
                plot.field_y AS "fieldY",
                plot.pa_x AS "paX",
                plot.pa_y AS "paY",
                plot.design_x AS "designX",
                plot.design_y AS "designY",
                generate_subscripts(
                    CASE
                        WHEN LOWER(crop.crop_code) <> 'rice'
                            THEN string_to_array(' ',',')
                        WHEN LOWER(harvest_method.data_value) in ('bulk')
                            THEN string_to_array('B',',')
                        WHEN LOWER(harvest_method.data_value) in ('plant-specific')
 							AND numeric_var.data_value IS NOT NULL
                            THEN string_to_array(numeric_var.data_value,',')
						WHEN LOWER(harvest_method.data_value) in ('plant-specific and bulk')
 							AND numeric_var.data_value IS NOT NULL
                            THEN string_to_array(numeric_var.data_value || ',B',',')
                        WHEN LOWER(harvest_method.data_value) in ('panicle selection', 'single plant selection')
							AND numeric_var.data_value IS NOT NULL
                            THEN ARRAY(SELECT generate_series(1,numeric_var.data_value::integer)::text)
						WHEN LOWER(harvest_method.data_value) in ('single plant selection and bulk')
							AND numeric_var.data_value IS NOT NULL
							THEN ARRAY_APPEND(ARRAY(SELECT generate_series(1,numeric_var.data_value::integer)::text),'B')
                        ELSE string_to_array(' ',',')
                    END,
                    1)
                AS "key",
                CASE
                    WHEN LOWER(crop.crop_code) <> 'rice'
                        THEN string_to_array(' ',',')
                    WHEN LOWER(harvest_method.data_value) in ('bulk')
                        THEN string_to_array('B',',')
                    WHEN LOWER(harvest_method.data_value) in ('plant-specific')
						AND numeric_var.data_value IS NOT NULL
                        THEN string_to_array(numeric_var.data_value,',')
					WHEN LOWER(harvest_method.data_value) in ('plant-specific and bulk')
						AND numeric_var.data_value IS NOT NULL
						THEN string_to_array(numeric_var.data_value || ',B',',')
                    WHEN LOWER(harvest_method.data_value) in ('panicle selection', 'single plant selection')
						AND numeric_var.data_value IS NOT NULL
                        THEN ARRAY(SELECT generate_series(1,numeric_var.data_value::integer)::text)
					WHEN LOWER(harvest_method.data_value) in ('single plant selection and bulk')
						AND numeric_var.data_value IS NOT NULL
                        THEN ARRAY_APPEND(ARRAY(SELECT generate_series(1,numeric_var.data_value::integer)::text),'B')
                    ELSE string_to_array(' ',',')
                END AS "arr"
            FROM
                experiment.plot
            LEFT JOIN
                experiment.entry entry ON plot.entry_id = entry.id AND entry.is_void = FALSE
            LEFT JOIN
                experiment.planting_instruction pi ON plot.id = pi.plot_id AND pi.is_void = FALSE
            LEFT JOIN
                germplasm.germplasm germplasm ON pi.germplasm_id = germplasm.id AND germplasm.is_void = FALSE
            LEFT JOIN
                tenant.crop crop ON germplasm.crop_id = crop.id AND crop.is_void = FALSE
            LEFT JOIN
                germplasm.seed seed ON pi.seed_id = seed.id AND seed.is_void = FALSE
            LEFT JOIN
                germplasm.package package ON pi.package_id = package.id AND package.is_void = FALSE
            LEFT JOIN
                experiment.location location ON plot.location_id = location.id AND location.is_void = FALSE
            LEFT JOIN
                experiment.occurrence occurrence ON plot.occurrence_id = occurrence.id AND occurrence.is_void = FALSE
            LEFT JOIN
                experiment.experiment experiment ON occurrence.experiment_id = experiment.id AND experiment.is_void = FALSE
            LEFT JOIN
                tenant.program program ON experiment.program_id = program.id AND program.is_void = FALSE
            LEFT JOIN
                tenant.stage stage ON experiment.stage_id = stage.id AND stage.is_void = FALSE
            LEFT JOIN
                tenant.season season ON experiment.season_id = season.id AND season.is_void = FALSE
            LEFT JOIN
                place.geospatial_object site ON occurrence.site_id = site.id AND site.is_void = FALSE
            LEFT JOIN
                experiment.plot_data harvest_method ON plot.id = harvest_method.plot_id AND harvest_method.is_void = FALSE AND harvest_method.variable_id =
                    (
                        SELECT
                            id
                        FROM
                            master.variable
                        WHERE
                            abbrev = 'HV_METH_DISC'
                        LIMIT 1
                    )
            LEFT JOIN
                experiment.plot_data numeric_var ON plot.id = numeric_var.plot_id AND numeric_var.is_void = FALSE AND numeric_var.variable_id =
                    (
                        SELECT
                            id
                        FROM
                            master.variable
                        WHERE
                            abbrev = CASE
                                WHEN LOWER(harvest_method.data_value) IN ('single plant selection', 'single plant selection and bulk')
                                    THEN 'NO_OF_PLANTS'
                                WHEN LOWER(harvest_method.data_value) IN ('panicle selection')
                                    THEN 'PANNO_SEL'
                                WHEN LOWER(harvest_method.data_value) IN ('plant-specific', 'plant-specific and bulk')
                                    THEN 'SPECIFIC_PLANT'
                            END
                        LIMIT 1
                    )
            WHERE
                plot.occurrence_id = ${occurrenceDbId}
                AND plot.is_void = FALSE
        ) plots
        LEFT JOIN
            experiment.plot_data pd ON plots."plotDbId" = pd.plot_id
            AND pd.variable_id = (
                SELECT
                    id
                FROM
                    master.variable
                WHERE
                    variable.abbrev = 'NO_OF_BAGS'
                    AND variable.is_void = FALSE
                    LIMIT 1
                )
            AND pd.is_void = FALSE
        `

        // Get the filter condition
        conditionString = await processQueryHelper.getFilter(
            searchParams,
            []
        )

        if (conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let plotsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plotsQuery,
            conditionString,
            orderString,
        )

        // Retrieve plots from the database   
        let plots = await sequelize
            .query(plotsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await plots == undefined || await plots.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get the final count of the plot records
        let plotsCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                plotsQuery,
                conditionString,
                orderString
            )

        let plotsCount = await sequelize
            .query(plotsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = plotsCount === undefined ? 0 : plotsCount[0].count

        res.send(200, {
            rows: plots,
            count: count
        })
        return
    }
}
