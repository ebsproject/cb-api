/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let productSearchHelper = require('../../../helpers/queryTool/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let userValidator = require('../../../helpers/person/validator.js')
let tokenHelper = require('../../../helpers/auth/token.js')
const logger = require('../../../helpers/logger')

module.exports = {
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let programCond = ``
        let conditionString = ''
        let addedConditionString = ''
        let additionalParams = {}
        let parameters = {}
        let searchQuery = ''
        let excludedParametersArray = []
        let accessDataQuery = ''
        let programIdsString = 0
        const endpoint = 'terminal-transactions-search'
        const operation = 'SELECT'

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set columns to be excluded in parameters for filtering
            excludedParametersArray = ['fields', 'occurrenceName', 'occurrenceDbId']

            let bodyParameters = req.body
            let condition = ``
            for (column in bodyParameters) {
                if (excludedParametersArray.includes(column))
                    continue

                let filters = bodyParameters[column]
                condition = ``
                condition = productSearchHelper.getFilterCondition(filters, column)
                if (condition !== '') {
                    condition = `(` + condition + ')'
                    if (searchQuery == '') {
                        searchQuery = searchQuery +
                            ` WHERE `
                            + condition
                    } else {
                        searchQuery = searchQuery +
                            ` AND `
                            + condition
                    }
                }
            }

            // Get filter condition
            conditionString = searchQuery

            // filter by occurrence name; this is a special case since the occurrence is in json format
            if (req.body.occurrenceName != null) {
                additionalParams['occurrence.occurrence_name'] = req.body.occurrenceName
                addedConditionString = await processQueryHelper.getFilter(
                    additionalParams,
                    []
                )
                addedConditionString = addedConditionString.replace('WHERE', 'AND').trim()
                addedConditionString = addedConditionString.replace('"occurrence.occurrence_name"', 'occurrence.occurrence_name').trim()
            }

            // filter by occurrence id; this is a special case since the occurrence is in json format
            if (req.body.occurrenceDbId != null) {
                additionalParams['t.occurrenceDbId'] = req.body.occurrenceDbId
                addedConditionString = await processQueryHelper.getFilter(
                    additionalParams,
                    []
                )
                addedConditionString = addedConditionString.replace('WHERE', 'AND').trim()
                addedConditionString = addedConditionString.replace('"t.occurrenceDbId"', 't."occurrenceDbId"').trim()
            }

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)
        
        let isAdmin = await userValidator.isAdmin(personDbId)
        
        if(!isAdmin){
            // if collaborator program code is set, filter occurrences shared to the collaborator program
            if(params.collaboratorProgramCode != null){
                programCond = ` AND program.program_code = '${params.collaboratorProgramCode}'`
            }

            // get all programs that user belongs to
            let getProgramsQuery =  `
                SELECT 
                    program.id 
                FROM 
                    tenant.program program,
                    tenant.program_team pt,
                    tenant.team_member tm,
                    tenant.team t
                WHERE
                    tm.person_id = ${personDbId}
                    AND tm.is_void = FALSE
                    AND pt.team_id = tm.team_id
                    AND pt.is_void = FALSE
                    AND program.id = pt.program_id
                    AND program.is_void = FALSE
                    AND t.id = pt.team_id
                    AND t.is_void = FALSE
                    ${programCond}
            `
            let programIds = await sequelize.query(getProgramsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            // if program does not exist
            if(programIds.length == 0){
                accessDataQuery = `occurrence.access_data @> $$\{"program": {"0":{}}}$$`
            }

            // build program permission query
            for (let programObj of programIds) {
                if (accessDataQuery != ``) {
                    accessDataQuery += ` OR `
                }

                programIdsString += ` , ${programObj.id}`

                accessDataQuery += `occurrence.access_data #> $$\{program,${programObj.id}}$$ is not null`

            }

            // include occurrences shared to the user
            let userQuery = `occurrence.access_data #> $$\{person,${personDbId}}$$ is not null`
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR ` + userQuery
            }

            if(accessDataQuery != ``){
                accessDataQuery = ` AND ( transaction.creator_id = ${personDbId} OR (${accessDataQuery}) OR experiment.program_id in (${programIdsString}) )`
            }
        }

        // build the base query
        let transactionsQuery = null
        let fromQuery = `
            FROM
                data_terminal.transaction transaction
            LEFT JOIN LATERAL
                jsonb_to_recordset(CASE WHEN jsonb_typeof(transaction.occurrence) = 'array' THEN transaction.occurrence ELSE '[]' END) as t("dataUnit" varchar, "dataUnitCount" integer, "variableDbIds" jsonb, "occurrenceDbId" integer, "formulaDbId" jsonb)
            ON TRUE
            LEFT JOIN
                experiment.occurrence occurrence
            ON
                occurrence.id = t."occurrenceDbId"
                AND occurrence.is_void = FALSE
            LEFT JOIN 
                experiment.experiment experiment 
            ON 
                experiment.id = occurrence.experiment_id
            JOIN
                experiment.location location
            ON
                location.id = transaction.location_id
            JOIN
                tenant.person creator
            ON
                creator.id = transaction.creator_id
            LEFT JOIN
                tenant.person committer
            ON
                committer.id = transaction.committer_id
                AND committer.is_void = FALSE
            LEFT JOIN
                tenant.person modifier
            ON
                modifier.id = transaction.modifier_id
            LEFT JOIN
                tenant.program program
            ON
                program.id = experiment.program_id
            WHERE
                transaction.is_void = FALSE
                ${addedConditionString} 
                ${accessDataQuery}
            GROUP BY transaction.id, location.id, creator.id, committer.id, modifier.id, experiment.id, program.program_code
            ORDER BY transaction.modification_timestamp DESC NULLS LAST, transaction.creation_timestamp DESC
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            transactionsQuery = knex.column(parameters['fields'].split('|'))
            transactionsQuery += fromQuery
        } else {
            transactionsQuery = `
                SELECT
                    transaction.id AS "transactionDbId",
                    location.location_name AS "locationName",
                    transaction.location_id AS "locationDbId",
                    CASE
                        WHEN jsonb_typeof(transaction.occurrence) = 'array' THEN
                            json_agg(json_build_object(
                                'dataUnit', t."dataUnit",
                                'dataUnitCount', t."dataUnitCount",
                                'variableDbIds', t."variableDbIds",
                                'formulaDbId', t."formulaDbId",
                                'occurrenceName', occurrence.occurrence_name,
                                'occurrenceDbId', t."occurrenceDbId"
                            ))
                        ELSE
                            json_build_object(
                                'dataUnit', NULL,
                                'dataUnitCount', NULL,
                                'variableDbIds', NULL,
                                'formulaDbId', NULL,
                                'occurrenceName', NULL,
                                'occurrenceDbId', NULL
                            )
                    END AS occurrence,
                    transaction.experiment_id AS "experimentDbId",
                    transaction.checksum,
                    transaction.action,
                    transaction.status,
                    transaction.remarks,
                    experiment.program_id AS "programDbId",
                    program.program_code AS "programCode",
                    transaction.creator_id AS "creatorDbId",
                    transaction.creation_timestamp AS "creationTimestamp",
                    creator.person_name AS creator,
                    transaction.committer_id AS "committerDbId",
                    transaction.committed_timestamp AS "committedTimestamp",
                    committer.person_name AS committer,
                    transaction.modifier_id AS "modifierDbId",
                    transaction.modification_timestamp AS "modificationTimestamp",
                    modifier.person_name AS modifier
                    ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let transactionsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            transactionsQuery,
            conditionString,
            orderString
        )

        let finalDataCountQuery = await processQueryHelper
        .getFinalTotalCountQuery(transactionsFinalSqlQuery, orderString)

        // Retrieve the transaction records
        let transactions = await sequelize
            .query(finalDataCountQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            }).catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
            })

        if (transactions === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
           
            return
        } else if(transactions.length < 1) {
            res.send(200, {
                rows: [],
                count:0
            })
            return
        }

        count = transactions[0].totalCount

        res.send(200, {
            rows: transactions,
            count: count
        })
        return
    }
}