/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }

      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']

      // Get filter condition
      conditionString = await processQueryHelper.getFilter(req.body, excludedParametersArray)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Build the base retrieval query
    let applicationsQuery = null

    // Check if the client specified values for the field/s
    if (parameters['fields']) {
      applicationsQuery = knex.column(parameters['fields'].split('|'))
      applicationsQuery += `
        FROM
          platform.application application
        LEFT JOIN
          tenant.person creator ON application.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON application.modifier_id = modifier.id
        WHERE
          application.is_void = FALSE
        ` + addedConditionString +
        `ORDER BY
          application.id
        `
    } else {
      applicationsQuery = `
        SELECT
          application.id as "applicationDbId",
          application.abbrev,
          application.label,
          application.action_label AS "actionLabel",
          application.icon,
          application.description,
          application.is_public AS "isPublic",
          application.remarks,
          application.notes,
          application.application_document AS "applicationDocument",
          application.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          application.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          platform.application application
        LEFT JOIN
          tenant.person creator ON application.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON application.modifier_id = modifier.id
        WHERE
          application.is_void = FALSE
        ` + addedConditionString +
        `ORDER BY
          application.id
        `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final SQL Query
    applicationsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      applicationsQuery,
      conditionString,
      orderString
    )

    // Retrieve the application records
    let applications = await sequelize
      .query(applicationsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await applications == undefined || await applications.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return

    } else {
      // Get the final count of the application records retrieved
      let applicationCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          applicationsQuery,
          conditionString,
          orderString
        )

      let applicationCount = await sequelize
        .query(applicationCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = applicationCount[0].count
    }

    res.send(200, {
      rows: applications,
      count: count
    })
    return
  }
}