/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let userValidator = require('../../../helpers/person/validator.js')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let logger = require('../../../helpers/logger')

module.exports = {

    // Endpoint for creating a new organization record
    // POST /v3/organizations
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []

        let organizationDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        //check if user is Admin
        let isAdmin = await userValidator.isAdmin(personDbId)

         if (!isAdmin) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401028)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let organizationUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/organizations'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        try {
            let organizationValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Set defaults
                let organizationCode = null
                let organizationName = null
                let description = null

                // Check if required columns are in the request body
                if (
                    !record.organizationCode || !record.organizationName 
                ) {
                    let errMsg = `Required parameters are missing. Ensure that organizationCode and organizationName fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                organizationCode = record.organizationCode
                organizationName = record.organizationName

                // Validate organizationCode if unique
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if the organizationCode is unique
                        SELECT
                            CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE 0
                            END AS count
                        FROM
                            tenant.organization org
                        WHERE
                            org.is_void = FALSE AND
                            org.organization_code = $$${organizationCode}$$
                    )
                `
                validateCount += 1


                // Validate organizationName if unique
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if the organizationName is unique
                        SELECT
                            CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE 0
                            END AS count
                        FROM
                            tenant.organization org
                        WHERE
                            org.is_void = FALSE AND
                            org.organization_name = $$${organizationName}$$
                    )
                `
                validateCount += 1
            
                description = (record.description !== undefined) ? record.description : null

                if (validateQuery.length != 0) {
                    let checkInputQuery = `
                        SELECT ${validateQuery} AS count
                    `
                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (inputCount[0]['count'] != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // get values
                let tempArray = [
                    organizationCode, organizationName, description, personDbId
                ]

                organizationValuesArray.push(tempArray)
            }

            // Create organization record
            let organizationsQuery = format(`
                INSERT INTO
                    tenant.organization (
                        organization_code, organization_name, description,creator_id
                    )
                VALUES
                    %L
                RETURNING id`, organizationValuesArray
            )

            const organizations = await sequelize.transaction(async transaction => {
                return await sequelize.query(organizationsQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (organization of organizations[0]) {
                organizationDbId = organization.id

                let organizationRecord = {
                    organizationDbId: organizationDbId,
                    recordCount: 1,
                    href: organizationUrlString + '/' + organizationDbId
                }

                resultArray.push(organizationRecord)
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, 'POST organizations: ' + JSON.stringify(err), 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}