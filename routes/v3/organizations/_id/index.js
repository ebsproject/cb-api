/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

  // Endpoint for retrieving organization information given a specific organization id
  // GET /v3/organizations/:id

  get: async function (req, res, next) {
    // Retrieve the organization id
    let organizationDbId = req.params.id

    if (!validator.isInt(organizationDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400182)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let organizationsQuery = `
      SELECT
        org.id AS "organizationDbId",
        org.organization_code AS "organizationCode",
        org.organization_name AS "organizationName",
        org.description,
        org.notes,
        org.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS creator,
        org.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS modifier
      FROM
        tenant.organization org
      LEFT JOIN
        tenant.person creator ON org.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON org.modifier_id = modifier.id
      WHERE
        org.id = ${organizationDbId}
        AND org.is_void = FALSE
    `
    // Retrieve organizations from the database
    let organizations = await sequelize
      .query(organizationsQuery, { 
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (await organizations === undefined || await organizations.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404001)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: organizations
    })
    return
  }
}
