/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errorBuilder = require('../../../../helpers/error-builder')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {
    /**
     * Update package log records
     * PUT /v3/package-logs/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    put: async function (req, res, next) {

        const packageTransactionFilter = [
            'deposit',
            'reserve',
            'withdraw'
        ]

        // Retrieve the package log ID
        let packageLogDbId = req.params.id
        if (!validator.isInt(packageLogDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400081)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)
        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if request body exists
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let packageTransactionType = null

        // Check if package log is existing
        // Build query
        let packageLogQuery = `
            SELECT
                pl.package_id as "packageDbId",
                pl.package_transaction_type AS "packageTransactionType"
            FROM
                germplasm.package_log "pl"
            WHERE
                "pl".is_void = FALSE AND
                "pl".id = ${packageLogDbId}
        `

        // Retrieve package log from the database
        let packageLog = await sequelize.query(packageLogQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            
        if (await packageLog == undefined || await packageLog.length < 1) {
            let errMsg = `The package log you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordpackageTransactionType = packageLog[0].packageTransactionType

        let isSecure = forwarded(req, req.headers).secure
        // Set the URL for response
        let packageLogUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/package-logs/"
            + packageLogDbId

        // Check if request body value is included in filters
        if (!packageTransactionFilter.includes(req.body.packageTransactionType)) {
            let errMsg = "You have provided an invalid parameter value for package transaction type"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body

        let transaction
        try {
            let setQuery = ``

            // update package transaction type
            if (data.packageTransactionType !== undefined) {
                packageTransactionType = data.packageTransactionType

                // Check if user input is same with the current value in the database
                if (packageTransactionType != recordpackageTransactionType) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_transaction_type = $$${packageTransactionType}$$
                    `
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the package log record
            let updatePackageLogQuery = `
                UPDATE
                    germplasm.package_log  
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${packageLogDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updatePackageLogQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let resultArray = {
                packageLogDbId: packageLogDbId,
                recordCount: 1,
                href: packageLogUrlString
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return

        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Delete package log records
     * DELETE /v3/package-logs/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    delete: async function (req, res, next) {
        // Retrieve the package log ID
        let packageLogDbId = req.params.id

        if (!validator.isInt(packageLogDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400081)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if package log is existing
        // Build query
        let packageLogQuery = `
            SELECT
                pl.id,
                pl.package_id as "packageDbId"
            FROM
                germplasm.package_log "pl"
            WHERE
                "pl".is_void = FALSE AND
                "pl".id = ${packageLogDbId}
        `

        // Retrieve package log from the database
        let packageLog = await sequelize.query(packageLogQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If file upload retrieval failed, end execution
        if (packageLog === undefined) {
            return
        }

        // If no record was retrieved, return NotFoundError
        if (await packageLog == undefined || await packageLog.length < 1) {
            let errMsg = `The package log you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        
        let transaction
        try {
             // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            let deletePackageLogQuery = format(`
                UPDATE
                    germplasm.package_log "pl"
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${packageLogDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    "pl".id = ${packageLogDbId}
            `)

            await sequelize.query(deletePackageLogQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { packageLogDbId: packageLogDbId }
            })
            return

        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}