/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errorBuilder = require('../../../helpers/error-builder')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {
    // Endpoint for creating package logs
    // POST /v3/package-logs
    post: async function (req, res, next) {
        // Set defaults
        let packageDbId = null
        let packageTransactionType = null
        let packageQuantity = null
        let packageUnit = null
        let entityAbbrev = null
        // Will be based on current entityAbbrev in the iteration
        let entityDbId = null
        let dataDbId = null
        let creatorDbId

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)
        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if request body exists
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let packageLogUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/package-logs'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            /*
                Records = [
                {},
                {},
                ...
                ]
            */
            records = data.records
            recordCount = records.length
            // Check if the input data is empty
            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Start transaction
        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let packageLogValuesArray = []
            let queryResult = null

            for (var record of records) {
                // Check if the required colums are in the input
                if (
                    !record.packageDbId ||
                    !record.packageTransactionType ||
                    !record.packageQuantity ||
                    !record.packageUnit ||
                    !record.entityAbbrev ||
                    !record.dataDbId
                ) {
                    let errMsg = '400: Required parameters are missing. Ensure that the packageDbId, packageTransactionType, packageQuantity, packageUnit, entityAbbrev, and dataDbId fields are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /* Validate and set values */

                // Validate first if packageDbId value is valid
                if (!Number.isInteger(record.packageDbId) && !validator.isInt(record.packageDbId)) {
                    let errMsg = '400: You have provided an invalid value for packageDbId.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                packageDbId = record.packageDbId

                // Validate if the packageDbId exists in the DB
                // packageDbId should exist to be valid
                validatePackageDbIdQuery = `SELECT
                        CASE WHEN (count(1) > 0)
                        THEN 1
                        ELSE 0
                        END
                        AS count
                    FROM
                        germplasm.package package
                    WHERE
                        package.is_void = FALSE AND
                        package.id = '${packageDbId}'`

                queryResult = await sequelize.query(validatePackageDbIdQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                // Get boolean value to determine whether package ID exists in the database or not
                packageDbIdExists = !!queryResult[0]['count']

                if (!packageDbIdExists) {
                    let errMsg = '404: The packageDbId you have provided does not exist.'
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                // Validate first if packageTransactionType is valid
                packageTransactionType = record.packageTransactionType

                // Validate if the packageTransactionType exists in the DB
                // packageTransactionType should exist to be valid
                validatePackageTransactionTypeQuery = `SELECT
                        CASE WHEN (count(1) > 0)
                        THEN 1
                        ELSE 0
                        END
                        AS count
                    FROM
                        master.scale_value scale_value
                    WHERE
                        scale_value.is_void = FALSE AND
                        scale_value.value = '${packageTransactionType}'`

                queryResult = await sequelize.query(validatePackageTransactionTypeQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                // Get boolean value to determine whether package transaction type is valid or invalid
                packageTransactionTypeExists = !!queryResult[0]['count']

                if (!packageTransactionTypeExists) {
                    let errMsg = '400: You have provided an invalid value for packageTransactionType.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate first if packageQuantity value is valid
                if (!Number.isInteger(record.packageQuantity) && !validator.isInt(record.packageQuantity)) {
                    let errMsg = '400: You have provided an invalid value for packageQuantity.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                packageQuantity = record.packageQuantity

                // Validate first if packageUnit is valid
                packageUnit = record.packageUnit
                abbrev = `PACKAGE_UNIT_${packageUnit.toUpperCase()}`

                // Validate if the packageUnit exists in the DB
                // packageUnit should exist to be valid
                validatePackageUnitQuery = `SELECT
                        CASE WHEN (count(1) > 0)
                        THEN 1
                        ELSE 0
                        END
                        AS count
                    FROM
                        master.scale_value scale_value
                    WHERE
                        scale_value.is_void = FALSE AND
                        scale_value.abbrev = '${abbrev}' AND
                        scale_value.value = '${packageUnit}'`

                queryResult = await sequelize.query(validatePackageUnitQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                // Get boolean value to determine whether package transaction type is valid or invalid
                packageUnitExists = !!queryResult[0]['count']

                if (!packageUnitExists) {
                    let errMsg = '400: You have provided an invalid value for packageUnit.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate first if entityAbbrev is valid
                entityAbbrev = record.entityAbbrev.toUpperCase()

                let validateEntityAbbrevQuery = `SELECT id FROM dictionary.entity WHERE abbrev = '${entityAbbrev}'`

                queryResult = await sequelize.query(validateEntityAbbrevQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (typeof queryResult === 'object' && queryResult.length === 1) {
                    entityDbId = queryResult[0]['id']
                } else {
                    entityDbId = null
                }

                if (!entityDbId) {
                    let errMsg = '404: The entityAbbrev you have provided does not exist in the database.'
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                // Validate first if dataDbId value is valid
                if (!Number.isInteger(record.dataDbId) && !validator.isInt(record.dataDbId)) {
                    let errMsg = '400: You have provided an invalid value for dataDbId.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                dataDbId = record.dataDbId
                let table = ''
                let alias = ''

                if (entityAbbrev === 'ENTRY') {
                    table = 'experiment.entry'
                    alias = 'entry'
                } else if (entityAbbrev === 'PLOT') {
                    table = 'experiment.plot'
                    alias = 'plot'
                }

                // Validate if the dataDbId exists in the DB
                // dataDbId should exist to be valid
                validateDataDbIdQuery = `SELECT
                        CASE WHEN (count(1) > 0)
                        THEN 1
                        ELSE 0
                        END
                        AS count
                    FROM
                        ${table} ${alias}
                    WHERE
                        ${alias}.is_void = FALSE AND
                        ${alias}.id = '${dataDbId}'`

                queryResult = await sequelize.query(validateDataDbIdQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                // Get boolean value to determine whether package transaction type is valid or invalid
                dataDbIdExists = !!queryResult[0]['count']

                if (!dataDbIdExists) {
                    let errMsg = '404: The dataDbId you have provided does not exist in the database.'
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                // Set creator ID for this new record
                creatorDbId = personDbId

                let tempArray = [
                    packageDbId,
                    packageTransactionType,
                    packageQuantity,
                    packageUnit,
                    entityDbId,
                    dataDbId,
                    creatorDbId,
                ]

                packageLogValuesArray.push(tempArray)
            }

            // Create package log record
            let packageLogQuery = format(`
                INSERT INTO germplasm.package_log
                    (
                        package_id,
                        package_transaction_type,
                        package_quantity,
                        package_unit,
                        entity_id,
                        data_id,
                        creator_id
                    )
                VALUES
                    %L
                RETURNING id`, packageLogValuesArray
            )

            packageLogQuery = packageLogQuery.trim()

            let packageLogs = await sequelize.query(packageLogQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            let resultArray = []

            for (let packageLog of packageLogs[0]) {
                packageLogDbId = packageLog.id

                // Return the user info to Client
                let array = {
                    userDbId: creatorDbId,
                    packageLogDbId,
                    recordCount: 1,
                    href: packageLogUrlString + '/' + packageLogDbId
                }

                resultArray.push(array)
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}