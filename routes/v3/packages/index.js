/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let { createPackageLog } = require('../../../helpers/package/index')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

module.exports = {

  // Endpoint for creating a new package record
  // POST /v3/packages
  post: async function (req, res, next) {

    // Set defaults
    let resultArray = []
    let recordCount = 0

    let packageDbId = null

    // Get person ID from token
    let personDbId = await tokenHelper.getUserId(req, res)

    // If personDbId is undefined, terminate
    if(personDbId === undefined){
      return
    }

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure

    // Set URL for response
    let packageUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/packages'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      records = data.records
      recordCount = records.length

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    try{ 
      transaction = await sequelize.transaction({ autocommit: false })

      let packageValuesArray = []
      let visitedArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        let packageCode = null
        let packageLabel = null
        let packageQuantity = null
        let packageUnit = null
        let packageStatus = null

        let seedDbId = null
        let programDbId = null
        let geospatialObjectDbId = null
        let facilityDbId = null

        // Check if required columns are in the request body
        if(
          !record.packageLabel || !record.packageQuantity ||
          !record.packageUnit || !record.packageStatus || !record.seedDbId
        ) {
          let errMsg = ` Required parameters are missing. Ensure that packageLabel, packageQuantity, packageUnit, packageStatus and seedDbId
            fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        /** Validation of required parameters and set values */

        packageCode = record.packageCode
        packageLabel = record.packageLabel
        packageQuantity = record.packageQuantity
        packageUnit = record.packageUnit
        packageStatus = record.packageStatus
        seedDbId = record.seedDbId
        
        // database validation for package code
        if (packageCode !== undefined) {

          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              SELECT
                CASE WHEN
                  (count(1) = 0)
                  THEN 1
                  ELSE 0
                END AS count
              FROM
                germplasm.package package
              WHERE
                package.is_void = FALSE AND
                package.package_code = $$${packageCode}$$
            )
          `
          validateCount += 1

        } else {
          let generatePackageCodeQuery = `
            SELECT
              germplasm.generate_code('package')
            AS packagecode
              
          `
          
          let package = await sequelize.query(generatePackageCodeQuery, {
            type: sequelize.QueryTypes.SELECT
          })
          
          if (await package ==  undefined || await package.length == 0 ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.NotFoundError(errMsg))
            return
          }
        
          packageCode = package[0].packagecode
        }

        // validation for package quantity format
        if (!validator.isFloat(packageQuantity)) {
          let errMsg = `Invalid format, package quantity must be a float.`
          res.send(new errors.BadRequestError(errMsg))
          return 
        }

        // validation for package quantity value (must be at least 0)
        if (parseFloat(packageQuantity) < 0) {
          let errMsg = `Invalid format, negative values are not allowed for the package quantity.`
          res.send(new errors.BadRequestError(errMsg))
          return 
        }

        // validation for package unit
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if package unit is existing return 1
            SELECT 
              count(1)
            FROM 
              master.scale_value scaleValue
              LEFT JOIN
                master.variable variable ON scaleValue.scale_id = variable.scale_id
            WHERE
              variable.abbrev LIKE 'UNIT' AND
              scaleValue.value ILIKE $$${packageUnit}$$
          )
        `
        validateCount += 1
        
        // validation for package status
        packageStatus = packageStatus.trim().toLowerCase()
        validateQuery += (validateQuery !== '') ? ' + ' : ''
        validateQuery += `
          (
          --- Check if PACKAGE_STATUS is existing return 1
          SELECT 
            count(1)
          FROM 
            master.scale_value scaleValue
          LEFT JOIN
            master.variable variable ON scaleValue.scale_id = variable.scale_id
          WHERE
            variable.abbrev LIKE 'PACKAGE_STATUS' AND
            scaleValue.value ILIKE $$${packageStatus}$$
          )
        `
        validateCount += 1

        // database validation for seed ID
        if (!validator.isInt(seedDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400236)
          res.send(new errors.BadRequestError(errMsg))
          return 
        }

        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if seed is existing return 1
            SELECT 
              count(1)
            FROM 
              germplasm.seed seed
            WHERE 
              seed.is_void = FALSE AND
              seed.id = ${seedDbId}
          )
        `
        validateCount += 1

        /** Validation of non-required parameters and set values */

        if (record.programDbId !== undefined) {
          programDbId = record.programDbId

          if (!validator.isInt(programDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400051)
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          // Validate program ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if program is existing return 1
              SELECT 
                count(1)
              FROM 
                tenant.program program
              WHERE 
                program.is_void = FALSE AND
                program.id = ${programDbId}
            )
          `
          validateCount += 1
        }

        if (record.geospatialObjectDbId !== undefined) {
          geospatialObjectDbId = record.geospatialObjectDbId

          if (!validator.isInt(geospatialObjectDbId)) {
            let errMsg = `Invalid format, geospatial object ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          // Validate geospatial object ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if geospatial object is existing return 1
              SELECT 
                count(1)
              FROM 
                place.geospatial_object geospatialObject
              WHERE 
                geospatialObject.is_void = FALSE AND
                geospatialObject.id = ${geospatialObjectDbId}
            )
          `
          validateCount += 1
        }

        if (record.facilityDbId !== undefined) {
          facilityDbId = record.facilityDbId

          if (!validator.isInt(facilityDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400130)
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          // Validate facility ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if facility is existing return 1
              SELECT 
                count(1)
              FROM 
                place.facility facility
              WHERE 
                facility.is_void = FALSE AND
                facility.id = ${facilityDbId}
            )
          `
          validateCount += 1
        }

        /** Validation of input in database */
        // count must be equal to validateCount if all conditions are met
        let checkInputQuery = `
          SELECT
            ${validateQuery}
          AS count
        `
        
        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check duplicates if the record count is  > 1
        if (recordCount > 1) {
          // Check if package code is already used
          if (visitedArray.includes(packageCode)) {
            let errMsg = `You have provided duplicate package code.`
            res.send(new errors.BadRequestError(errMsg))
            return
          } else {
            visitedArray.push(packageCode)
          }
        }

        // Get values
        let tempArray = [
          packageCode, packageLabel, packageQuantity, packageUnit, packageStatus,
          seedDbId, programDbId, geospatialObjectDbId, facilityDbId, personDbId
        ]

        packageValuesArray.push(tempArray)
      }

      // Create package record/s
      let packagesQuery = format(`
        INSERT INTO
          germplasm.package (
            package_code, package_label, package_quantity, package_unit,
            package_status, seed_id, program_id, geospatial_object_id,
            facility_id, creator_id
          )
        VALUES 
          %L
        RETURNING id, package_quantity, package_unit`, packageValuesArray
      )

      let packages = await sequelize.query(packagesQuery,{
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      // Commit the transaction
      await transaction.commit()
      
      for (package of packages[0]) {
        packageDbId = package.id

        // Return the package info
        let array = {
          packageDbId: packageDbId,
          recordCount: 1,
          href: packageUrlString + '/' + packageDbId
        }
        resultArray.push(array)

        // Get quantity and unit
        let packageQuantity = package.package_quantity
        let packageUnit = package.package_unit
        // Create package log
        await createPackageLog(
          packageDbId,
          packageQuantity,
          packageUnit,
          personDbId
        )
      }

      res.send(200, {
          rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}
