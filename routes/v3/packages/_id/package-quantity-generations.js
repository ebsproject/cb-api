/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
const logger = require('../../../../helpers/logger')
const packageHelper = require('../../../../helpers/package/index')
const scaleConversionHelper = require('../../../../helpers/scaleConversion/index')

module.exports = {
    // POST /v3/packages/:id/package-quantity-generations
    post: async function (req, res, next) {
        const endpoint = 'packages/:id/package-quantity-generations'

        // Retrieve package ID
        let packageDbId = req.params.id
        let originalPackageUnit = ''
        let newPackageQuantity = 0
        let cropDbId = null
        
        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if package ID is an integer
        if (!validator.isInt(packageDbId)) {
            let errMsg = `You have provided an invalid format for the package ID.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if package is existing
        const packageQuery = `
            SELECT
                package.id AS "packageDbId",
                package.package_quantity AS "packageQuantity",
                package.package_unit AS "packageUnit",
                crop_program.id AS "cropDbId"
            FROM
                germplasm.package package
            LEFT JOIN
                tenant.program program ON package.program_id = program.id
            LEFT JOIN
                tenant.crop_program crop_program ON program.crop_program_id = crop_program.id
            WHERE
                package.id = ${packageDbId} AND
                package.is_void = FALSE
        `

        // Retrieve package from the database
        const package = await sequelize.query(packageQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await package == undefined || await package.length < 1) {
            let errMsg = `The package you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        cropDbId = package[0]['cropDbId'] ?? null
        originalPackageUnit = package[0]['packageUnit']

        // Get all package logs with type 'deposit' and 'withdraw' linked to current package
        const packageLogQuery = `
            SELECT
                id AS "packageLogDbId",
                package_id AS "packageDbId",
                package_quantity AS "packageQuantity",
                package_unit AS "packageUnit",
                package_transaction_type AS "packageTransactionType"
            FROM
                germplasm.package_log
            WHERE
                package_id = ${packageDbId} AND
                (package_transaction_type = 'deposit' OR
                package_transaction_type = 'withdraw') AND
                is_void = FALSE
        `

        // Retrieve package from the database
        const packageLogs = await sequelize.query(packageLogQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })


        if (packageLogs.length > 0) { // Update package_quantity
            // Convert some if not all package logs' quantities to grams if not already
            // Then add all package logs' standardized quantities to the...
            // ...standardized package quantity
            for (const packageLog of packageLogs) {
                // Convert this log's package unit if it is not in grams
                let {
                    packageQuantity,
                    packageUnit,
                    packageTransactionType,
                } = packageLog

                if (packageUnit != 'g') {
                    // Retrieve conversion rules for given crop ID
                    let conversionArray = await scaleConversionHelper.getScaleConversionValues(cropDbId)

                    packageQuantity = await packageHelper.convertPackageQuantity(packageQuantity, packageUnit, 'g', conversionArray)
                }

                if (packageTransactionType === 'deposit') {
                    newPackageQuantity += packageQuantity
                } else if (packageTransactionType === 'withdraw') {
                    newPackageQuantity -= packageQuantity
                }
            }

            // Convert value back to original unit
            if (originalPackageUnit != 'g') {
                // Retrieve conversion rules for given crop ID
                let conversionArray = await scaleConversionHelper.getScaleConversionValues(cropDbId)
            
                newPackageQuantity = await packageHelper.convertPackageQuantity(newPackageQuantity, 'g', originalPackageUnit, conversionArray)
            }

            // Update package_quantity of the package
            try {
                // Begin transaction
                let transaction = await sequelize.transaction({ autocommit: false })
    
                // Build UPDATE query
                let updatePackageQuantityquery = `
                    UPDATE
                        germplasm.package AS package
                    SET
                        package_quantity = ${newPackageQuantity}
                    WHERE
                        id = ${packageDbId}
                `
    
                // Run UPDATE query
                await sequelize.query(updatePackageQuantityquery, {
                    type: sequelize.QueryTypes.UPDATE
                })
    
                resultArray = {
                    packageDbId: packageDbId,
                    recordCount: 1
                }
    
                // Commit the transaction
                await transaction.commit()
    
                res.send(200, {
                    rows: resultArray
                })
    
                return
            } catch (err) {
                await logger.logFailingQuery(endpoint, 'UPDATE', err)
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500003)
                res.send(new errors.InternalError(errMsg))
                return
            }
        } else { // No update on package_quantity
            res.send(200, {
                rows: { message: 'Package currently has no deposit or withdraw logs to calculate the package quantity with.' }
            })
        }
    }
}