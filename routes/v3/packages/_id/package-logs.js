/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

  // Endpoint for retrieving an existing package log record
  // GET /v3/packages/:id/package-logs
  get: async function (req, res, next) {

    // Retrieve package ID
    let packageDbId = req.params.id

    // Check if ID is an integer
    if (!validator.isInt(packageDbId)) {
      let errMsg = `Invalid format, package ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    //Build query
    let packageLogsQuery = `
    SELECT
      entry.entry_code AS "entryCode",
      package.package_label AS "packageLabel",
      pl.id AS "packageLogDbId",
      pl.package_quantity AS "packageQuantity",
      pl.package_unit AS "packageUnit",
      pl.package_transaction_type AS "packageTransactionType",
      pl.entity_id AS "entityDbId",
      pl.data_id AS "dataDbId",
      pl.remarks,
      creator.person_name AS "creator",
      pl.creation_timestamp AS "creationTimestamp",
      modifier.person_name AS "modifier",
      pl.modification_timestamp AS "modificationTimestamp",
      pl.notes,
      pl.is_void AS "isVoid",
      pl.event_log AS "eventLog"
    FROM
      germplasm.package_log pl
    LEFT JOIN
      germplasm.package package ON package.id = pl.package_id
    LEFT JOIN
      dictionary.entity entity ON entity.id = pl.entity_id
    LEFT JOIN
      experiment.entry entry ON entry.id = pl.data_id
    INNER JOIN tenant.person AS creator
      ON creator.id = pl.creator_id
    LEFT JOIN tenant.person AS modifier
      ON modifier.id = pl.modifier_id
    WHERE
      package.is_void = FALSE
      AND pl.package_id = ${packageDbId}
    `

    // Retrieve package-logs from the database
    let packageLogs = await sequelize.query(packageLogsQuery, {
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (await packageLogs == undefined || await packageLogs.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get count
      let packageLogsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(packageLogsQuery)
      let packageLogsCount = await sequelize.query(packageLogsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      count = packageLogsCount[0].count

      res.send(200, {
        rows: packageLogs,
        count: count
      })
      return
    }
  },

  // Endpoint for creating a package log record for the given package id
  // POST /v3/packages/:id/package-logs
  post: async function (req, res, next) {

    // Set defaults
    let resultArray = []

    // Retrieve package ID
    let packageDbId = req.params.id

    // Check if ID is an integer
    if (!validator.isInt(packageDbId)) {
      let errMsg = `Invalid format, package ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Build query
    let packageQuery = `
      SELECT
        package.id AS "packageDbId",
        package.package_code AS "packageCode",
        package.package_label AS "label",
        package.package_quantity AS "quantity",
        package.package_unit AS "unit"
      FROM
        germplasm.package package
      WHERE
        package.is_void = FALSE AND
        package.id = ${packageDbId}
    `

    // Retrieve package record from database
    let package = await sequelize.query(packageQuery, {
        type: sequelize.QueryTypes.SELECT
    })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

    if (await package == undefined || await package.length < 1) {
        let errMsg = `The package you have requested does not exist.`
        res.send(new errors.NotFoundError(errMsg))
        return
    }

    // Retrieve user ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)
    
    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure

    // Set URL for response
    let packageLogsUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + `/v3/packages/${packageDbId}/package-logs`

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      records = data.records

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    try {
      transaction = await sequelize.transaction({ autocommit: false })

      let packageLogValuesArray = []

      let allowedTransactionTypes = ['deposit','withdraw','reserve'];

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        let packageQuantity = null
        let packageUnit = null
        let packageTransactionType = null

        // Check if required columns are in the request body
        if(
          !record.packageQuantity || !record.packageUnit || !record.packageTransactionType
        ) {
          let errMsg = ` Required parameters are missing. Ensure that packageQuantity, packageUnit, and packageTransactionType
            fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        /** Validation of required parameters and set values */

        packageQuantity = record.packageQuantity
        packageUnit = record.packageUnit
        packageTransactionType = record.packageTransactionType

        // validation for package quantity format
        if (!validator.isFloat(packageQuantity)) {
          let errMsg = `Invalid format, package quantity must be a float.`
          res.send(new errors.BadRequestError(errMsg))
          return 
        }

        // validation for package quantity value (must be at least 0)
        if (parseFloat(packageQuantity) < 0) {
          let errMsg = `Invalid format, negative values are not allowed for the package quantity.`
          res.send(new errors.BadRequestError(errMsg))
          return 
        }

        // validation for package unit
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if package unit is existing return 1
            SELECT 
              count(1)
            FROM 
              master.scale_value scaleValue
              LEFT JOIN
                master.variable variable ON scaleValue.scale_id = variable.scale_id
            WHERE
              variable.abbrev LIKE 'PACKAGE_UNIT' AND
              scaleValue.value ILIKE $$${packageUnit}$$
          )
        `
        validateCount += 1
        
        // validation for package transaction type
        if (!allowedTransactionTypes.includes(packageTransactionType)) {
          let errMsg = `Invalid input, package transaction type must be: ${allowedTransactionTypes.join(',')}.`
          res.send(new errors.BadRequestError(errMsg))
          return 
        }

        /** Validation of input in database */
        // count must be equal to validateCount if all conditions are met
        let checkInputQuery = `
          SELECT
            ${validateQuery}
          AS count
        `
        
        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Get values
        let tempArray = [
          packageDbId, packageQuantity, packageUnit, packageTransactionType, personDbId
        ]

        packageLogValuesArray.push(tempArray)

      }

      // Create package log record/s
      let packageLogsQuery = format(`
        INSERT INTO
          germplasm.package_log (
            package_id, package_quantity, package_unit,
            package_transaction_type, creator_id
          )
        VALUES 
          %L
        RETURNING id`, packageLogValuesArray
      )

      let packageLogs = await sequelize.query(packageLogsQuery,{
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      // Commit the transaction
      await transaction.commit()
      
      for (packageLog of packageLogs[0]) {
        packageLogDbId = packageLog.id

        // Return the package log info
        let array = {
          packageLogDbId: packageLogDbId,
          recordCount: 1,
          href: packageLogsUrlString + '/' + packageLogDbId
        }
        resultArray.push(array)
      }

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}