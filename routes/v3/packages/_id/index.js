/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
let { createPackageLog, getPackageLogTransactionType } = require('../../../../helpers/package/index')
let advancedSearchHelper = require('../../../../helpers/advancedSearch/index')
let endpoint = 'packages/_id'

module.exports = {

    // Endpoint for retrieving an existing package record
    // GET /v3/packages/:id
    get: async function (req, res, next) {
        // Retrieve package ID
        let packageDbId = req.params.id

        // Check if ID is an integer
        if (!validator.isInt(packageDbId)) {
            let errMsg = `Invalid format, package ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let packageQuery = `
            SELECT
                experiment.id AS "experimentDbId",
                experiment.experiment_name AS "experiment",
                experiment.experiment_year AS "experimentYear",
                experiment.experiment_type AS "experimentType",
                stage.id AS "experimentStage",
                stage.stage_code AS "experimentStageCode",
                occurrence.occurrence_name AS "experimentOccurrence",
                program.id AS "programDbId",
                program.program_code AS "seedManager",
                seed.id AS "seedDbId",
                seed.seed_name AS "seedName",
                seed.seed_code AS "GID",
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                germplasm.designation,
                "location".id AS "locationDbId",
                "location".location_code AS "location",
                "location".location_name AS "sourceStudyName",
                season.season_code AS "sourceStudySeason",
                "entry".entry_code AS "sourceEntryCode",
                "entry".entry_number AS "seedSourceEntryNumber",
                plot.plot_code AS "seedSourcePlotCode",
                plot.plot_number AS "seedSourcePlotNumber",
                plot.rep AS "replication",
                seed.harvest_date AS "harvestDate",
                package.id AS "packageDbId",
                package.package_code AS "packageCode",
                package.package_label AS "label",
                package.package_quantity AS "quantity",
                package.package_unit AS "unit",
                package.package_document AS "packageDocument",
                facility.facility_name AS "facility",
                container.facility_name AS "container",
                subFacility.facility_name AS "subFacility",
                creator.person_name AS "creator",
                modifier.person_name AS "modifier"
            FROM
                germplasm.package package
            LEFT JOIN
                tenant.program program ON program.id = package.program_id
            LEFT JOIN
                place.facility container ON container.id = package.facility_id
            LEFT JOIN
                place.facility subFacility ON subFacility.id = container.parent_facility_id
            LEFT JOIN
                place.facility facility ON facility.id = container.root_facility_id
            LEFT JOIN
                germplasm.seed seed ON seed.id = package.seed_id
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = seed.germplasm_id
            LEFT JOIN
                experiment.location "location" ON "location".id = seed.source_location_id
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = seed.source_experiment_id
            LEFT JOIN
                experiment.plot plot ON plot.id = seed.source_plot_id
            LEFT JOIN
                experiment.entry "entry" ON "entry".id = seed.source_entry_id
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id
            LEFT JOIN
                tenant.season season ON season.id = experiment.season_id
            LEFT JOIN
                tenant.stage stage ON stage.id = experiment.stage_id
            LEFT JOIN
                tenant.person creator ON creator.id = package.creator_id
            LEFT JOIN
                tenant.person modifier ON modifier.id = package.modifier_id
            WHERE
                package.is_void = FALSE AND
                package.id = ${packageDbId}
        `

        // Retrieve package record from database
        let package = await sequelize.query(packageQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await package == undefined || await package.length < 1) {
            let errMsg = `The package you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        res.send(200, {
            rows: package
        })
        return
    },

    // Endpoint for updating an existing package record
    // PUT /v3/packages/:id
    put: async function (req, res, next) {

        // Retrieve package ID
        let packageDbId = req.params.id

        // Check if ID is an integer
        if (!validator.isInt(packageDbId)) {
            let errMsg = `Invalid format, package ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Set defaults
        let packageQuantity = null
        let packageUnit = null
        let packageStatus = null
        let geospatialObjectDbId = null
        let programDbId = null
        let facilityDbId = null
        let notes = null
        let logQuantityChange = false
        let quantityUpdated = false

        // Build query
        let packageQuery = `
            SELECT
                package.package_quantity AS "packageQuantity",
                package.package_unit AS "packageUnit",
                package.package_status AS "packageStatus",
                package.program_id AS "programDbId",
                package.notes,
                geospatialObject.id AS "geospatialObjectDbId",
                facility.id AS "facilityDbId"
            FROM
                germplasm.package package
            LEFT JOIN
                place.geospatial_object geospatialObject ON geospatialObject.id = package.geospatial_object_id 
            LEFT JOIN
                place.facility facility ON facility.id = package.facility_id 
            WHERE
                package.is_void = FALSE AND
                package.id = ${packageDbId}
        `

        // Retrieve package record from database
        let package = await sequelize.query(packageQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await package == undefined || await package.length < 1) {
            let errMsg = `The package you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordPackageQuantity = package[0].packageQuantity
        let recordPackageUnit = package[0].packageUnit
        let recordPackageStatus = package[0].packageStatus
        let recordProgramDbId = package[0].programDbId
        let recordPackageNotes = package[0].notes
        let recordGeospatialObjectDbId = package[0].geospatialObjectDbId
        let recordFacilityDbId = package[0].facilityDbId

        let isSecure = forwarded(req, req.header).secure

        // Set URL for response
        let packageUrlString = (isSecure ? 'https' : 'http')
        + '://'
        + req.headers.host
        + '/v3/packages/'
        + packageDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        try {
            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values */

            if (data.packageQuantity !== undefined) {
                packageQuantity = data.packageQuantity

                if (!validator.isFloat(packageQuantity)) {
                    let errMsg = `Invalid format, package quantity must be a float.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }

                // validation for package quantity value (must be at least 0)
                if (parseFloat(packageQuantity) < 0) {
                    let errMsg = `Invalid format, negative values are not allowed for the package quantity.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }

                // Check if logQuantityChange is supplied
                if (data.logQuantityChange !== undefined) {
                    logQuantityChange = data.logQuantityChange

                    // Check if logQuantityChange is boolean.
                    // If not, generate BadRequestError.
                    if (typeof logQuantityChange !== 'boolean') {
                        let errMsg = `Invalid format, logQuantityChange must be a boolean value.`
                        res.send(new errors.BadRequestError(errMsg))
                        return 
                    }
                }

                // Check if user input is same with the current value in the database
                if (packageQuantity != recordPackageQuantity) {
                    quantityUpdated = true

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_quantity = $$${packageQuantity}$$
                    `
                }
            }

            if (data.packageUnit !== undefined) {
                packageUnit = data.packageUnit

                // Check if user input is same with the current value in the database
                if (packageUnit != recordPackageUnit) {
                
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if package unit is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                            LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'PACKAGE_UNIT' AND
                            scaleValue.value ILIKE $$${packageUnit}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_unit = $$${packageUnit}$$
                    `
                }
            }

            if (data.packageStatus !== undefined) {
                packageStatus = data.packageStatus.trim().toLowerCase()

                // Check if user input is same with the current value in the database
                if (packageStatus != recordPackageStatus) {
                
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if package status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                            LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'PACKAGE_STATUS' AND
                            scaleValue.value ILIKE $$${packageStatus}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_status = $$${packageStatus}$$
                    `
                }
            }

            if (data.geospatialObjectDbId !== undefined) {
                geospatialObjectDbId = data.geospatialObjectDbId

                if (!validator.isInt(geospatialObjectDbId)) {
                    let errMsg = `Invalid format, geospatial object ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (geospatialObjectDbId != recordGeospatialObjectDbId) {

                    // Validate geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if geospatial object is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            place.geospatial_object geospatialObject
                        WHERE 
                            geospatialObject.is_void = FALSE AND
                            geospatialObject.id = ${geospatialObjectDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        geospatial_object_id = $$${geospatialObjectDbId}$$
                    `
                }
            }

            if (data.programDbId !== undefined) {
                programDbId = data.programDbId
        
                if (programDbId !== "null" && !validator.isInt(programDbId)) {
                  let errMsg = `Invalid format, program Id must be an integer.`
                  res.send(new errors.BadRequestError(errMsg))
                  return
                }

                // Check if user input null
                if (programDbId === "null") {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        program_id = NULL
                    `
                }
                // Check if user input is same with the current value in the database
                else if (programDbId != recordProgramDbId) {
                    // Validate program ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if program is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.program program
                            WHERE 
                                program.is_void = FALSE AND
                                program.id = ${programDbId}
                        )
                    `
                    validateCount += 1
            
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        program_id = $$${programDbId}$$
                    `
                }
            } 

            if (data.facilityDbId !== undefined) {
                facilityDbId = data.facilityDbId

                if (facilityDbId !== "null" && !validator.isInt(facilityDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400130)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input null
                if (facilityDbId === "null") {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        facility_id = NULL
                    `
                }
                // Check if user input is same with the current value in the database
                else if (facilityDbId != recordFacilityDbId) {

                    // Validate facility ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if facility is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            place.facility facility
                        WHERE 
                            facility.is_void = FALSE AND
                            facility.id = ${facilityDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        facility_id = $$${facilityDbId}$$
                    `
                }
            }

            if (data.notes !== undefined) {
                notes = data.notes

                // Check if user input is same with the current value in the database
                if (notes != recordPackageNotes) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        notes = $$${notes}$$
                    `
                }
                
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if(setQuery.length > 0) setQuery += `,`

            // Update the package record
            let updatePackageQuery = `
                UPDATE
                    germplasm.package package
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    package.id = ${packageDbId}
            `

            // Update package record
            await sequelize.transaction(async transaction => {
                return await sequelize.query(updatePackageQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return transaction info
            let resultArray = {
                packageDbId: packageDbId,
                recordCount: 1,
                href: packageUrlString
            }

            // If package quantity was updated
            // and logQuantityChange flag is set to TRUE,
            // then create a new package log record.
            if (packageQuantity !== null) {
                if (quantityUpdated && logQuantityChange) {
                    // Get transaction type and package log quantity
                    let result = await getPackageLogTransactionType(
                        parseFloat(recordPackageQuantity),
                        parseFloat(packageQuantity)
                    )

                    // If the result is not null, insert the package log record
                    if (result !== null) {
                        // Insert package log quantity
                        await createPackageLog(
                            packageDbId,
                            result.pqValue,
                            recordPackageUnit,
                            personDbId,
                            result.transactionType
                        )
                    }
                }
            }

            // TODO: return to this one day
            // Temporarily disable advanced search single update
            // advancedSearchHelper.triggerAdvancedSearchUpdate(endpoint, 'PUT', `packageDbId:${req.params.id}`, req.body)

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for deleting an existing package record
    // DELETE /v3/packages/:id
    delete: async function (req, res, next) {
        // Retrieve package ID
        let packageDbId = req.params.id

        // Check if ID is an integer
        if (!validator.isInt(packageDbId)) {
            let errMsg = `Invalid format, package ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let packageQuery = `
                SELECT
                    package.package_code AS "packageCode"
                FROM
                    germplasm.package package
                LEFT JOIN
                    tenant.person creator ON creator.id = package.creator_id
                WHERE
                    package.is_void = FALSE AND
                    package.id = ${packageDbId}
            `
            
            let package = await sequelize.query(packageQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await package == undefined || await package.length < 1) {
                let errMsg = `The package you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordPackageCode = package[0].packageCode

            let deletePackageQuery = format(`
                UPDATE
                    germplasm.package package
                SET
                    is_void = TRUE,
                    package_code = 'VOIDED-${recordPackageCode}-${packageDbId}',
                    package_status = 'inactive',
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    package.id = ${packageDbId}
            `)
            
            await sequelize.query(deletePackageQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { 
                    packageDbId: packageDbId
                }
            })
            return

        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
