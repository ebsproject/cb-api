/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for updating an existing experiment design record
    // PUT /v3/experiment-designs/:id
    put: async function (req, res, next) {

        // Retrieve the experiment design ID
        let experimentDesignDbId = req.params.id

        if(!validator.isInt(experimentDesignDbId)) {
            let errMsg = `Invalid format, experiment design ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let occurrenceDbId = null
        let designDbId = null
        let plotDbId = null
        let blockType = null
        let blockValue = null
        let blockLevelNumber = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)
    
        // Check if experiment design is existing
        // Build query
        let experimentDesignQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                ed.design_id AS "designDbId",
                plot.id AS "plotDbId",
                ed.block_type AS "blockType",
                ed.block_value AS "blockValue",
                ed.block_type AS "blockType",
                ed.block_level_number AS "blockLevelNumber",
                creator.id AS "creatorDbId"
            FROM
                experiment.experiment_design ed
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = ed.occurrence_id
            LEFT JOIN
                experiment.plot plot ON plot.id = ed.plot_id
            LEFT JOIN
                tenant.person creator ON creator.id = ed.creator_id
            WHERE
                ed.is_void = FALSE AND
                ed.id = ${experimentDesignDbId}
        `

        // Retrieve occurrence from the database
        let experimentDesign = await sequelize.query(experimentDesignQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (experimentDesign === undefined || experimentDesign.length < 1) {
            let errMsg = `The experiment design you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database

        let recordOccurrenceDbId = experimentDesign[0].occurrenceDbId
        let recordDesignDbId = experimentDesign[0].designDbId
        let recordPlotDbId = experimentDesign[0].plotDbId
        let recordBlockType = experimentDesign[0].blockType
        let recordBlockValue = experimentDesign[0].blockValue
        let recordBlockLevelNumber = experimentDesign[0].blockLevelNumber
        let recordCreatorDbId = experimentDesign[0].creatorDbId

        // Check if user is an admin or an owner of the cross
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                WHERE 
                    occurrence.id = ${recordOccurrenceDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let experimentDesignUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/experiment-designs/"
            + experimentDesignDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body    

        // Start transaction
        let transaction
        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/

            if (data.occurrenceDbId !== undefined) {
                occurrenceDbId = data.occurrenceDbId

                if (!validator.isInt(occurrenceDbId)) {
                    let errMsg = `Invalid format, occurrence ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (occurrenceDbId != recordOccurrenceDbId) {

                    // Validate occurrence ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if occurrence is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.occurrence occurrence
                            WHERE 
                                occurrence.is_void = FALSE AND
                                occurrence.id = ${occurrenceDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_id = $$${occurrenceDbId}$$
                    `
                }
            }

            if (data.designDbId !== undefined) {
                designDbId = data.designDbId

                if (!validator.isInt(designDbId)) {
                    let errMsg = `Invalid format, design ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (designDbId != recordDesignDbId) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        design_id = $$${designDbId}$$
                    `
                }
            }

            if (data.plotDbId !== undefined) {
                plotDbId = data.plotDbId

                if (!validator.isInt(plotDbId)) {
                    let errMsg = `Invalid format, plot ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (plotDbId != recordPlotDbId) {

                    // Validate plot ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if plot is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.plot plot
                            WHERE 
                                plot.is_void = FALSE AND
                                plot.id = ${plotDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        plot_id = $$${plotDbId}$$
                    `
                }
            }

            if (data.blockType !== undefined) {
                blockType = data.blockType.trim()

                // Check if user input is same with the current value in the database
                if (blockType != recordBlockType) {
                    
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'EXPERIMENT_DESIGN_BLOCK_TYPE' AND
                                scaleValue.value ILIKE $$${blockType}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        block_type = $$${blockType}$$
                    `
                }
            }

            if (data.blockValue !== undefined) {
                blockValue = data.blockValue

                if (!validator.isInt(blockValue)) {
                    let errMsg = `Invalid format, block value must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (blockValue != recordBlockValue) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        block_value = $$${blockValue}$$
                    `
                }
            }

            if (data.blockLevelNumber !== undefined) {
                blockLevelNumber = data.blockLevelNumber

                if (!validator.isInt(blockLevelNumber)) {
                    let errMsg = `Invalid format, block level number must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (blockLevelNumber != recordBlockLevelNumber) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        block_level_number = $$${blockLevelNumber}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the occurrence record
            let updateExperimentDesignQuery = `
                UPDATE
                    experiment.experiment_design
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${experimentDesignDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            
            await sequelize.query(updateExperimentDesignQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let resultArray = {
                experimentDesignDbId: experimentDesignDbId,
                recordCount: 1,
                href: experimentDesignUrlString
            }
            
            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing experiment design record
    // DELETE /v3/experiment-designs/:id
    delete: async function (req, res, next) {

        // Retrieve the experiment design ID
        let experimentDesignDbId = req.params.id

        if(!validator.isInt(experimentDesignDbId)) {
            let errMsg = `Invalid format, experiment design ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let experimentDesignQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    occurrence.id AS "occurrenceDbId"
                FROM
                    experiment.experiment_design ed
                LEFT JOIN
                    tenant.person creator ON creator.id = ed.creator_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = ed.occurrence_id
                WHERE
                    ed.is_void = FALSE AND
                    ed.id = ${experimentDesignDbId}
            `

            let experimentDesign = await sequelize.query(experimentDesignQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (experimentDesign == undefined || experimentDesign.length < 1) {
                let errMsg = `The experiment design you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = experimentDesign[0].creatorDbId
            let recordOccurrenceDbId = experimentDesign[0].occurrenceDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                    WHERE 
                        occurrence.id = ${recordOccurrenceDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `
                
                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteExperimentDesignQuery = format(`
                UPDATE
                    experiment.experiment_design
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${experimentDesignDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${experimentDesignDbId}
            `)
            
            await sequelize.query(deleteExperimentDesignQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { experimentDesignDbId: experimentDesignDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }    
}