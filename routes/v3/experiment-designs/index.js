/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

module.exports = {

    // Endpoint for creating experiment design record/s
    // POST /v3/experiment-designs
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []

        let experimentDesignDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let experimentDesignUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/experiment-designs'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let experimentDesignValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Set defaults
                let occurrenceDbId = null
                let designDbId = null
                let plotDbId = null
                let blockType = null
                let blockValue = null
                let blockLevelNumber = null
                let blockName = null

                if (
                    record.occurrenceDbId == undefined ||
                    record.designDbId == undefined ||
                    record.plotDbId == undefined ||
                    record.blockType == undefined ||
                    record.blockLevelNumber == undefined
                ) {
                    let errMsg = `Required parameters are missing. Ensure that occurrenceDbId,
                    designDbId, plotDbId, blockType, and blockLevelNumber fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */
                
                occurrenceDbId = record.occurrenceDbId
                designDbId = record.designDbId
                plotDbId = record.plotDbId
                blockType = record.blockType.trim()
                blockLevelNumber = record.blockLevelNumber

                // database validation for occurrence ID
                if (!validator.isInt(occurrenceDbId)) {
                    let errMsg =  `Invalid format, occurrence ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }
                
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if occurrence is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            experiment.occurrence occurrence
                        WHERE 
                            occurrence.is_void = FALSE AND
                            occurrence.id = ${occurrenceDbId}
                    )
                `
                validateCount += 1

                // validation for design ID
                if (!validator.isInt(designDbId)) {
                    let errMsg =  `Invalid format, design ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }
                
                // database validation for plot ID
                if (!validator.isInt(plotDbId)) {
                    let errMsg =  await errorBuilder.getError(req.headers.host, 400038)
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if plot is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            experiment.plot plot
                        WHERE 
                            plot.is_void = FALSE AND
                            plot.id = ${plotDbId}
                    )
                `
                validateCount += 1
                
                // validation for blockType
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if block type is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'EXPERIMENT_DESIGN_BLOCK_TYPE' AND
                            scaleValue.value ILIKE $$${blockType}$$
                    )
                `
                validateCount += 1
                
                // validation for block level number
                if (!validator.isInt(blockLevelNumber)) {
                    let errMsg =  `Invalid format, block level number must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }
                
                /** Validation of non-required parameters and set values */
                if (record.blockValue !== undefined) {
                    blockValue = record.blockValue
                    // validation for block value
                    if (!validator.isInt(blockValue)) {
                        let errMsg =  `Invalid format, block value must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return 
                    }
                }

                blockName = (record.blockName != undefined) ? record.blockName : null

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [
                    occurrenceDbId, designDbId, plotDbId, blockType, blockValue,
                    blockLevelNumber, personDbId, blockName
                ]

                experimentDesignValuesArray.push(tempArray)
            }

            // Create experiment design record
            let experimentDesignsQuery = format(`
                INSERT INTO
                    experiment.experiment_design (
                        occurrence_id, design_id, plot_id, block_type, block_value,
                        block_level_number, creator_id, block_name
                    )
                VALUES 
                    %L
                RETURNING id`, experimentDesignValuesArray
            )
            
            let experimentDesigns = await sequelize.query(experimentDesignsQuery,{
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })
            
            for (experimentDesign of experimentDesigns[0]) {
                experimentDesignDbId = experimentDesign.id

                // Return the occurrence info
                let array = {
                    experimentDesignDbId: experimentDesignDbId,
                    recordCount: 1,
                    href: experimentDesignUrlString + '/' + experimentDesignDbId
                }
                resultArray.push(array)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}