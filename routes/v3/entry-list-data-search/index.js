/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger/index')
let endpoint = 'entry-list-data-search'

module.exports = {  
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}
        
        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields']

            // Get filter condition
            conditionString = await processQueryHelper.getFilter(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let entryListDataQuery = null

        // Check if the client specified values for the field/s
        if (parameters['fields'] != null) {
            entryListDataQuery = knex.column(parameters['fields'].split('|'))

            entryListDataQuery += `
                FROM
                    experiment.entry_list_data entry_list_data
                LEFT JOIN
                    experiment.entry_list entryList ON entry_list_data.entry_list_id = entryList.id
                LEFT JOIN 
                    master.variable variable ON variable.id = entry_list_data.variable_id
                LEFT JOIN 
                    tenant.person creator ON creator.id = entry_list_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = entry_list_data.modifier_id
                WHERE
                    entry_list_data.is_void = FALSE
                    ${addedConditionString}
                ORDER BY
                    entry_list_data.id
            `
        } else {
            entryListDataQuery = `
                SELECT
                    entry_list_data.id AS "entryListDataDbId",
                    entry_list_data.entry_list_id AS "entryListDbId",
                    entryList.entry_list_name AS "entryListName",
                    entryList.description,
                    entry_list_data.variable_id AS "variableDbId",
                    variable.abbrev AS "variableAbbrev",
                    variable.label AS "variableLabel",
                    entry_list_data.protocol_id AS "protocolDbId",
                    entry_list_data.data_value AS "dataValue",
                    entry_list_data.data_qc_code AS "dataQcCode",
                    entry_list_data.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    entry_list_data.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    experiment.entry_list_data entry_list_data
                LEFT JOIN
                    experiment.entry_list entryList ON entry_list_data.entry_list_id = entryList.id
                LEFT JOIN 
                    master.variable variable ON variable.id = entry_list_data.variable_id
                LEFT JOIN 
                    tenant.person creator ON creator.id = entry_list_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = entry_list_data.modifier_id
                WHERE
                    entry_list_data.is_void = FALSE
                    ${addedConditionString}
                ORDER BY
                    entry_list_data.id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        entryListFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            entryListDataQuery,
            conditionString,
            orderString
        )

        // Retrieve the entry list data records
        let entryListData = await sequelize.query(entryListFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                // Log error when query fails
                await logger.logFailingQuery(endpoint, 'SELECT', err)
            })

        // Return error when query fails
        if (await entryListData === undefined) {
            errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
        
        // If query returns empty, return empty values
        if( await entryListData.length < 1){
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }
        
        // Get the final count of the entry list data records
        let entryListDataCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            entryListDataQuery,
            conditionString,
            orderString
        )
        
        let entryListDataCount = await sequelize.query(entryListDataCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = entryListDataCount[0].count

        res.send(200, {
            rows: entryListData,
            count: count
        })

        return
    }
}