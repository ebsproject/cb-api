/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token.js')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedDistinctString = ''
    let parameters = {}
    let ownedConditionString = ''
    let userId = await tokenHelper.getUserId(req);

    if (userId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    parameters['distinctOn'] = ''

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }

      let excludedParametersArray = ['fields', 'distinctOn']

      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray,
      )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(
            parameters['distinctOn']
          )
        parameters['distinctOn'] = `"${parameters['distinctOn']}",`
      }
    }

    let locationsQuery = null

    if (req.query.isOwned !== undefined) {
      ownedConditionString = ` AND (creator.id = ${userId} OR steward.id = ${userId}) `
    }

    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters['fields'],
        )
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        locationsQuery = knex.select(selectString)
      } else {
        locationsQuery = knex.column(parameters['fields'].split('|'))
      }

      locationsQuery += `
        FROM
          experiment.location location
        LEFT JOIN
          tenant.person steward ON location.steward_id = steward.id
        LEFT JOIN
          place.geospatial_object geospatial_object ON location.geospatial_object_id = geospatial_object.id
        LEFT JOIN
          place.geospatial_object site ON site.id = location.site_id
        LEFT JOIN
          place.geospatial_object field ON field.id = location.field_id
        LEFT JOIN
          tenant.season season on season.id = location.season_id
        LEFT JOIN
          tenant.person creator ON location.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON location.modifier_id = modifier.id
        ORDER BY
          ${parameters['distinctOn']}
          location.id
      `
    } else {
      locationsQuery = `
        SELECT
          ${addedDistinctString}
          location.id AS "locationDbId",
          location.location_code AS "locationCode",
          location.location_name AS "locationName",
          location.location_status AS "locationStatus",
          location.location_type AS "locationType",
          location.description,
          location.plot_count AS "plotCount",
          (
            SELECT
              count(log.id)
            FROM
              experiment.location_occurrence_group log
            WHERE
              log.location_id = location.id AND
             log.is_void = FALSE
          )::int AS "occurrenceCount",
          location.location_document AS "locationDocument",
          location.location_planting_date AS "plantingDate",
          location.location_harvest_date AS "harvestDate",
          site.id AS "siteDbId",
          site.geospatial_object_code AS "siteCode",
          site.geospatial_object_name AS "site",
          field.id AS "fieldDbId",
          field.geospatial_object_code AS "fieldCode",
          field.geospatial_object_name AS "field",
          geospatial_object.id AS "geospatialObjectDbId",
          geospatial_object.geospatial_object_code AS "geospatialObjectCode",
          geospatial_object.geospatial_object_name AS "geospatialObjectName",
          geospatial_object.geospatial_object_type AS "geospatialObjectType",
          location.location_year AS "year",
          season.id AS "seasonDbId",
          season.season_code AS "seasonCode",
          season.season_name AS "season",
          steward.id AS "stewardDbId",
          steward.person_name AS "steward",
          location.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          location.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          experiment.location location
        LEFT JOIN
          tenant.person steward ON location.steward_id = steward.id
        LEFT JOIN
          place.geospatial_object geospatial_object ON location.geospatial_object_id = geospatial_object.id
        LEFT JOIN
          place.geospatial_object site ON site.id = location.site_id
        LEFT JOIN
          place.geospatial_object field ON field.id = location.field_id
        LEFT JOIN
          tenant.season season on season.id = location.season_id
        LEFT JOIN
          tenant.person creator ON location.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON location.modifier_id = modifier.id
        WHERE
          location.is_void = FALSE
          ${ownedConditionString}
        ORDER BY
          ${parameters['distinctOn']}
          location.id
      `
    }

    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400057)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    locationFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      locationsQuery,
      conditionString,
      orderString,
    )
    // Retrieve experiments from the database
    let locations = await sequelize
      .query(locationFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await locations == undefined || await locations.length < 1) {
      locations = []
      count = 0
    } else {
      let locationCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          locationsQuery,
          conditionString,
          orderString,
        )

      let locationCount = await sequelize
        .query(
          locationCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
          }
        )
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = locationCount[0].count
    }

    res.send(200, {
      rows: locations,
      count: count
    })
    return
  }
}