/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let validator = require('validator')
let errors = require('restify-errors')

module.exports = {
    /**
     * Retrieve cross data for harvest labels
     * GET /v3/cross-harvest-labels
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function(req, res, next) {
        // Initialize variables
        let params = req.query
        let sort = req.query.sort
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = []
        let occurrenceDbId

        // Check if occurrence IDs are provided as parameters
        if(typeof params.occurrenceDbId == 'undefined'){
            //  If not provided, return error
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve occurrence ID
        occurrenceDbId = params.occurrenceDbId

        // Validate if occurrence ID is valid integer value
        if(!validator.isInt(occurrenceDbId)){
            //  If not valid integer, return error
            let errMsg = await errorBuilder.getError(req.headers.host, 400241)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve occurence record using occurrence ID
        let occurrenceDataQuery = `
            SELECT
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.occurrence_status AS "occurrenceStatus"
            FROM
                experiment.occurrence occurrence
            WHERE
                occurrence.is_void = FALSE AND
                occurrence.id = ${occurrenceDbId}
        `

        let occurrenceData = await sequelize
            .query(occurrenceDataQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // if occurrence data is undefined
        if(await occurrenceData == undefined){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // if occurrence data is empty, return error
        if (await occurrenceData.length === 0) {
            let errMsg = 'Resource not found. The occurrence you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // get filter condition
        conditionString = await processQueryHelper.getFilter(
            searchParams,
            []
        )

        if (conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // get sort condition
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // build final query
        let crossDataQuery = `
            SELECT
                crosses.*,
                GENERATE_SERIES(
                    1,
                    CASE
                        WHEN cd.data_value IS NULL THEN 1
                        ELSE cd.data_value::INTEGER
                    END
                ) AS "bagNumber"
            FROM
                (
                    SELECT 
		                "cross".id AS "crossDbId",
		                "cross".cross_name AS "crossName",
		                (SELECT parentage AS "parentage" FROM germplasm.germplasm WHERE id = "seed".germplasm_id AND is_void = FALSE),
		                (SELECT designation AS "femaleParent" FROM germplasm.germplasm WHERE id = "femaleParent".germplasm_id AND is_void = FALSE),
		                (SELECT designation AS "maleParent" FROM germplasm.germplasm WHERE id = "maleParent".germplasm_id AND is_void = FALSE),
		                "femaleParent".plot_id AS "femaleParentPlotDbId",
		                "femaleEntry".id AS "femaleParentEntryDbId",
		                "femalePlot"."entryCode" AS "femaleParentEntryCode",
		                "femaleEntry".entry_number AS "femaleParentEntryNumber",
		                "femalePlot"."plotDbId" AS "femaleParentPlotDbId",
		                "femalePlot"."plotCode" AS "femaleParentPlotCode",
		                "femalePlot"."plotNumber" AS "femaleParentPlotNumber",
		                "maleParent".plot_id AS "maleParentPlotDbId",
		                "maleEntry".id AS "maleParentEntryDbId",
		                "malePlot"."entryCode" AS "maleParentEntryCode",
		                "maleEntry".entry_number AS "maleParentEntryNumber",
                        "malePlot"."plotDbId" AS "maleParentPlotDbId",
                        "malePlot"."plotCode" AS "maleParentPlotCode",
                        "malePlot"."plotNumber" AS "maleParentPlotNumber",
		                "crossData".data_value AS "noOfSeeds",
		                "seed".harvest_date AS "harvestDate",
		                "germplasm".id AS "germplasmDbId",
		                "germplasm".designation AS "germplasmName",
		                "germplasm".germplasm_code AS "germplasmCode",
		                "harvestedPackage".id AS "harvestedPackageDbId",
		                "harvestedPackage".package_code AS "harvestedPackageCode",
		                "harvestedPackage".package_label AS "harvestedPackageLabel",
		                exp.id AS "experimentDbId",
		                exp.experiment_code AS "experimentCode",
		                exp.experiment_name AS "experimentName",
		                exp.experiment_year AS "experimentYear",
		                "occurrence".id AS "occurrenceDbId",
		                "occurrence".occurrence_code AS "occurrenceCode",
		                "occurrence".occurrence_name AS "occurrenceName",
		                "season".id AS "seasonDbId",
		                "season".season_code AS "seasonCode",
		                "season".season_name AS "seasonName",
		                "program".id AS "programDbId",
		                "program".program_code AS "programCode",
		                "program".program_name AS "programName",
		                "site".id AS "siteDbId",
		                "site".geospatial_object_code AS "siteCode",
		                "site".geospatial_object_name AS "siteName",
                        "femalePlot"."plotDbId" AS "plotDbId",
		                "femalePlot"."plotCode" AS "plotCode",
		                "femalePlot"."plotNumber" AS "plotNumber"
		            FROM 
		                experiment.occurrence "occurrence"
		                    LEFT JOIN 
		                        experiment.experiment exp ON exp.id = "occurrence".experiment_id AND exp.is_void = FALSE
		                    LEFT JOIN 
		                        germplasm.seed "seed" ON "seed".source_occurrence_id = "occurrence".id AND "seed".is_void = FALSE 	
		                    LEFT JOIN
		                        germplasm.package "harvestedPackage" ON "harvestedPackage".seed_id = "seed".id AND "harvestedPackage".is_void = FALSE
		                    LEFT JOIN
		                        germplasm.germplasm "germplasm" ON "germplasm".id = "seed".germplasm_id AND "germplasm".is_void = FALSE
		                    LEFT JOIN 
		                        germplasm.cross "cross" ON "cross".id = "seed".cross_id AND "cross".is_void = FALSE
		                    LEFT JOIN LATERAL (
								SELECT
									"crossData".data_value
								FROM
									germplasm.cross_data "crossData"
									LEFT JOIN master.variable mv ON "crossData".variable_id = mv.id AND mv.is_void = FALSE 
								WHERE
									"crossData".cross_id = "cross".id AND mv.abbrev = 'NO_OF_SEED' AND "crossData".is_void = FALSE
		                        LIMIT 1
							) "crossData" ON TRUE	
		                    LEFT JOIN 
		                        germplasm.cross_parent "femaleParent" ON "femaleParent".cross_id = "cross".id AND "femaleParent".parent_role = 'female' AND "femaleParent".is_void = FALSE
		                    LEFT JOIN
		                        experiment.entry "femaleEntry" ON "femaleEntry".id = "femaleParent".entry_id AND "femaleEntry".is_void = FALSE
		                    LEFT JOIN LATERAL (
                                SELECT
                                    plot.id AS "plotDbId",
                                    plot.plot_code AS "plotCode",
                                    pi.entry_code AS "entryCode",
                                    plot.plot_number AS "plotNumber"
                                FROM
                                    experiment.plot
                                LEFT JOIN
                                    experiment.planting_instruction pi ON pi.plot_id = plot.id AND pi.is_void = FALSE
                                WHERE
                                    pi.entry_id = "femaleEntry".id
                                ORDER BY plot.id LIMIT 1
                            ) "femalePlot" ON TRUE
		                    LEFT JOIN 
		                        germplasm.cross_parent "maleParent" ON "maleParent".cross_id = "cross".id AND "maleParent".parent_role = 'male' AND "maleParent".is_void = FALSE
		                    LEFT JOIN
		                        experiment.entry "maleEntry" ON "maleEntry".id = "maleParent".entry_id AND "maleEntry".is_void = FALSE
		                    LEFT JOIN LATERAL (
                                SELECT
                                    plot.id AS "plotDbId",
                                    plot.plot_code AS "plotCode",
                                    pi.entry_code AS "entryCode",
                                    plot.plot_number AS "plotNumber"
                                FROM
                                    experiment.plot
                                LEFT JOIN
                                    experiment.planting_instruction pi ON pi.plot_id = plot.id AND pi.is_void = FALSE
                                WHERE
                                    pi.entry_id = "maleEntry".id
                                ORDER BY plot.id LIMIT 1
                            ) "malePlot" ON TRUE
		                    LEFT JOIN 
		                        tenant.season "season" ON "season".id = exp.season_id AND "season".is_void = FALSE
		                    LEFT JOIN 
		                        tenant.program "program" ON "program".id = exp.program_id AND "program".is_void = FALSE
		                    LEFT JOIN
		                        place.geospatial_object "site" ON "site".id = "occurrence".site_id AND "site".is_void = FALSE
		            WHERE
		                "occurrence".id = ${occurrenceDbId} AND
		                "occurrence".is_void = FALSE AND
		                "cross".id IS NOT NULL AND
		                "seed".harvest_source = 'cross'
            ) crosses
            LEFT JOIN
                germplasm.cross_data cd ON crosses."crossDbId" = cd.cross_id
                AND cd.variable_id = (
                    SELECT
                        id
                    FROM
                        master.variable
                    WHERE
                        variable.abbrev = 'NO_OF_BAGS'
                        AND variable.is_void = FALSE
                        LIMIT 1
                    )
                AND cd.is_void = FALSE
        `

        // Generate final SQL query
        let crossDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            crossDataQuery,
            conditionString,
            orderString,
        )

        // Retrieve cross data
        let crossData = await sequelize
            .query(crossDataFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Check if cross data is undefined, return error
        if (await crossData == undefined || typeof crossData == 'undefined') {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Check if cross data is empty, return empty
        if (await crossData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Retrieve final count
        let crossCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                crossDataQuery,
                conditionString,
                orderString
            )

        // Check if cross count is undefined, return error
        let crossDataCount = await sequelize
            .query(crossCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Set count value
        count = crossDataCount === undefined ? 0 : crossDataCount[0].count

        // return cross data
        res.send(200, {
            rows: crossData,
            count: count
        })
        return
    }
}
