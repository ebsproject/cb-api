/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let { performance } = require('perf_hooks')

module.exports = {

    get: async (req, res, next) => {

        let responseTime
        let statusString = ''
        let databaseStatusString = ''
        let startTime = performance.now()
        let statusTimestamp = ''
        let httpStatusCode = 200
        let result = {}

        let statusQuery = `
            SELECT 
                DISTINCT (id),
                LOCALTIMESTAMP(0) AS "timestamp"
            FROM 
                master.item
            WHERE 
                is_void = FALSE
            LIMIT 1
        `
        
        let item = await sequelize.query(statusQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .then(function(item){
            let endTime = performance.now()

            databaseStatusString = 'OK'
            statusString = 'OK'
            responseTime = (endTime-startTime)/1000
            responseTime = responseTime.toFixed(6)
            statusTimestamp = item[0].timestamp.toDateString() + ' ' + item[0].timestamp.toLocaleTimeString()

            result = {
                status: statusString,
                responseTime: responseTime + 's',
                timestamp: statusTimestamp,
                checks: {
                    database: databaseStatusString
                }
            }

            res.send(httpStatusCode, {
                rows: result
            })
            return
        })
        .catch(async err => {
            await setTimeout(async () => {

            // Set values for response 
            httpStatusCode = 500
            statusString = 'FAILED'
            databaseStatusString = 'FAILED'

            // Compute for response time
            let endTime = performance.now()
            responseTime = (endTime-startTime)/1000
            responseTime = responseTime.toFixed(6)
            statusTimestamp = new Date().toDateString() + ' ' + new Date().toLocaleTimeString()

            res.send(httpStatusCode, {
                rows: {
                    status: statusString,
                    responseTime: responseTime + 's',
                    timestamp: statusTimestamp,
                    checks: {
                        database: databaseStatusString
                    }
                }
            })
            return
            }, 15000)
        })
    }
}