/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
  /**
   * Retrieve event logs for specific experiment.plot_data record
   * GET v3/plot-data/:id/event-logs
   * @param {*} req Request parameters
   * @param {*} res Response
   */
  get: async function (req, res, next) {

    // Retrieve the plot data ID
    let plotDataDbId = req.params.id

    // Validate plotDbId
    if (!validator.isInt(plotDataDbId)) {
      let errMsg = "Invalid format, plot data ID must be an integer"
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Build query
    let eventLogsQuery = `
      WITH records (jsonbrecords) AS (
        SELECT
          event_log
        FROM 
          experiment.plot_data
        WHERE
          id = ${plotDataDbId} AND
          is_void = FALSE
      )
      SELECT 
        r.*,
        person.person_name AS actor
      FROM 
        records, 
        jsonb_array_elements(jsonbrecords) AS t(doc),
        jsonb_to_record(t.doc) AS r(actor_id int, new_data json, row_data json, action_type varchar, log_timestamp timestamp, transaction_id int),
        tenant.person person
      WHERE
        r.actor_id = person.id AND
        person.is_void = FALSE
      ORDER BY r.log_timestamp DESC
    `

    // Retrieve plot data eventLogs from the database 
    let eventLogs = await sequelize.query(eventLogsQuery, {
      type: sequelize.QueryTypes.SELECT
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    // Return error if resource is not found
    if (await eventLogs === undefined || await eventLogs.length < 1) {
      let errMsg = "The plot data event log/s you have requested does not exist/s."
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: eventLogs
    })
    
    return
  }
}