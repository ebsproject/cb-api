/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {

  /**
  * Update a specific record in experiment.plot_data
  * PUT /v3/plot-data/:id
  * @param {*} req Request parameters
  * @param {*} res Response
  */
  put: async function (req, res, next) {

    let plotDataDbId = req.params.id

    // Check if plotDataDbId is integer
    if (!validator.isInt(plotDataDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400040)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Set defaults
    let dataValue = null
    let dataQcCode = null
    let collectionTimestamp = null
    let transactionDbId = null

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(personDbId)

    // Check if plot data record is existing
    // Build query
    let plotDataQuery = `
      SELECT 
        pd.data_value AS "dataValue",
        pd.data_qc_code AS "dataQcCode",
        pd.collection_timestamp AS "collectionTimestamp",
        pd.transaction_id AS "transactionDbId",
        creator.id AS "creatorDbId",
        occurrence.id AS "occurrenceDbId"
      FROM 
        experiment.plot_data pd
      LEFT JOIN
        tenant.person creator ON creator.id = pd.creator_id
      LEFT JOIN
        experiment.plot plot ON plot.id = pd.plot_id
      LEFT JOIN
        experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
      WHERE 
        pd.is_void = FALSE AND 
        pd.id = ${plotDataDbId}
    `

    let plotDataRecord = await sequelize.query(plotDataQuery, {
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })
    
    if (await plotDataRecord === undefined || await plotDataRecord.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404012)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Get values from database, record = the current value in the database
    let recordDataValue = plotDataRecord[0].dataValue
    let recordDataQcCode = plotDataRecord[0].dataQcCode
    let recordCollectionTimestamp = plotDataRecord[0].collectionTimestamp
    let recordTransactionDbId = plotDataRecord[0].transactionDbId
    let recordCreatorDbId = plotDataRecord[0].creatorDbId
    let recordOccurrenceDbId = plotDataRecord[0].occurrenceDbId

    // Check if user is an admin or an owner of the plot
    if(!isAdmin && (personDbId != recordCreatorDbId)) {
      let programMemberQuery = `
        SELECT 
          person.id,
          person.person_role_id AS "role"
        FROM 
          tenant.person person
        LEFT JOIN 
          tenant.team_member teamMember ON teamMember.person_id = person.id
        LEFT JOIN 
          tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
        LEFT JOIN 
          tenant.program program ON program.id = programTeam.program_id 
        LEFT JOIN
          experiment.experiment experiment ON experiment.program_id = program.id
        LEFT JOIN
          experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
        WHERE 
          occurrence.id = ${recordOccurrenceDbId} AND
          person.id = ${personDbId} AND
          person.is_void = FALSE
      `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
    }
    
    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).secure

    // Set URL for response
    let plotDataUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host 
      + '/v3/plot-data/'
      + plotDataDbId
  
    // Check is req.body has parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Parse the input
    let data = req.body    

    // Start transaction
    let transaction
    try {
      let setQuery = ``

      /** Validation of parameters and set values **/
      if (data.dataValue !== undefined) {
        dataValue = data.dataValue

        if (dataValue != recordDataValue) {
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_value = $$${dataValue}$$
          `
        }
      }

      if (data.dataQcCode !== undefined) {
        dataQcCode = data.dataQcCode

        // Check if user input is same with the current value in the database
        if (dataQcCode != recordDataQcCode) {

          let validDataQcCodeValues = ['N', 'G', 'Q', 'S', 'M', 'B']

          // Validate data qc code
          if(!validDataQcCodeValues.includes(dataQcCode)) {
            let errMsg = `Invalid request, you have provided an invalid value for data QC code.`
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_qc_code = $$${dataQcCode}$$
          `
        }
      }

      if (data.collectionTimestamp !== undefined) {
        collectionTimestamp = data.collectionTimestamp

        // Check if user input is same with the current value in the database
        if (collectionTimestamp != recordCollectionTimestamp) {

          let validTimestamp = /^\d{4}-[01]\d-[0-3]\d [0-2]\d:[0-5]\d:[0-5]\d\.\d\d\d$/
          if (!collectionTimestamp.match(validTimestamp)) {
            let errMsg = 'Invalid format, collectionTimestamp must be in this format YYYY-MM-DD hh:mm:ss.sss'
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            collection_timestamp = $$${collectionTimestamp}$$
          `
        }
      }

      if (data.transactionDbId !== undefined) {
        transactionDbId = data.transactionDbId

        if (dataValue == null || dataQcCode == null) {
          let errMsg = 'The transactionDbId cannot be updated without dataValue or dataQCCode.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        if (!validator.isInt(transactionDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400075)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        if (transactionDbId != recordTransactionDbId) {

          // Validate transaction ID
          let transactionQuery = `
            --- Check if transaction is existing return 1
            SELECT 
              count(1)
            FROM 
              data_terminal.transaction transaction
            WHERE 
              transaction.is_void = FALSE AND
              transaction.id = ${transactionDbId}
          `

          let transaction = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
          })
            .catch(async err => {
              let errMsg = await errorBuilder.getError(req.headers.host, 500004)
              res.send(new errors.InternalError(errMsg))
              return
            })

          if (transaction[0].count == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404026)
            res.send(new errors.NotFoundError(errMsg))
            return
          } else {
            setQuery += (setQuery != '') ? ',' : ''
            setQuery += `
              transaction_id = $$${transactionDbId}$$
            `
          }
        }
      }

      if (setQuery.length > 0) setQuery += `,`

      // Update the plot data record
      let updatePlotDataQuery = `
        UPDATE
          experiment.plot_data
        SET
          ${setQuery}
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          id = ${plotDataDbId}
      `
      
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      await sequelize.query(updatePlotDataQuery, {
        type: sequelize.QueryTypes.UPDATE,
        transaction: transaction
      })

      // Return the transaction info
      let resultArray = {
        plotDataDbId: plotDataDbId,
        recordCount: 1,
        href: plotDataUrlString
      }
      
      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      res.send(new errors.InternalError(errMsg))
      return
    }
  },

  /**
  * Deletes a specific record in experiment.plot_data
  * DELETE /v3/plot-data/:id
  * @param {*} req Request parameters
  * @param {*} res Response
  */
  delete: async function (req, res, next) {

    let plotDataDbId = req.params.id

    // Check if plotDataDbId is integer
    if (!validator.isInt(plotDataDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400040)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req, res)

    // If personDbId is undefined, terminate
    if(personDbId === undefined){
      return
    }

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    let isAdmin = await userValidator.isAdmin(personDbId)

    let transaction

    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let plotDataQuery = `
        SELECT
          creator.id AS "creatorDbId", 
          occurrence.id AS "occurrenceDbId"
        FROM 
          experiment.plot_data pd
        LEFT JOIN
          tenant.person creator ON creator.id = pd.creator_id
        LEFT JOIN
          experiment.plot plot ON plot.id = pd.plot_id
        LEFT JOIN
          experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
        WHERE 
          pd.is_void = FALSE AND 
          pd.id = ${plotDataDbId}
      `

      let plotDataRecord = await sequelize.query(plotDataQuery, {
        type: sequelize.QueryTypes.SELECT
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })
      
      if (await plotDataRecord === undefined || await plotDataRecord.length < 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404012)
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      let recordCreatorDbId = plotDataRecord[0].creatorDbId
      let recordOccurrenceDbId = plotDataRecord[0].occurrenceDbId

      // Check if user is an admin or an owner of the plot
      if(!isAdmin && (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
          SELECT 
            person.id,
            person.person_role_id AS "role"
          FROM 
            tenant.person person
          LEFT JOIN 
            tenant.team_member teamMember ON teamMember.person_id = person.id
          LEFT JOIN 
            tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
          LEFT JOIN 
            tenant.program program ON program.id = programTeam.program_id 
          LEFT JOIN
            experiment.experiment experiment ON experiment.program_id = program.id
          LEFT JOIN
            experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
          WHERE 
            occurrence.id = ${recordOccurrenceDbId} AND
            person.id = ${personDbId} AND
            person.is_void = FALSE
        `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
      }

      let deletePlotDataQuery = format(`
        UPDATE
          experiment.plot_data
        SET
          is_void = TRUE,
          notes = CONCAT('VOIDED-', $$${plotDataDbId}$$),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          id = ${plotDataDbId}
      `)

      await sequelize.query(deletePlotDataQuery, {
        type: sequelize.QueryTypes.UPDATE
      })

      await transaction.commit()
      res.send(200, {
        rows: { plotDataDbId: plotDataDbId }
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}