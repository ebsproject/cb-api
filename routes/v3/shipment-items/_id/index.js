/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let validator = require("validator");
let tokenHelper = require("../../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let projectHelper = require("../../../../helpers/project/index.js");
let errorBuilder = require("../../../../helpers/error-builder");

module.exports = {
  // Endpoint for retrieving the list of shipmentItems
  // GET /v3/shipmentItems/{id}
  get: async function (req, res, next) {
    let shipmentItemDbId = req.params.id;
    let count = 0;

    if (!validator.isInt(shipmentItemDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400056);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Build query
    let shipmentItemsQuery = `
      SELECT
        shipment_item.id AS "shipmentItemDbId",
        shipment_item.shipment_id AS "shipmentDbId" , 
        shipment_item.germplasm_id  AS "germplasmDbId",
        shipment_item.seed_id AS "seedDbId",
        shipment_item.package_id AS "packageDbId",
        shipment_item.shipment_item_number AS "shipmentItemNumber",
        shipment_item.shipment_item_code AS "shipmentItemCode",
        shipment_item.shipment_item_status AS "shipmentItemStatus",
        shipment_item.shipment_item_weight AS "shipmentItemWeight",
        shipment_item.package_unit AS "packageUnit",
        shipment_item.package_count  AS "packageCount",
        shipment_item.test_code AS "testCode",
        shipment_item.mta_status AS "mtaStatus",
        shipment_item.availability AS "availability",
        shipment_item.use AS "use",
        shipment_item.mls_ancestors AS "mlsAncestors",
        shipment_item.genetic_stock AS "geneticStock",
        shipment_item.remarks AS "remarks",
        shipment_item.creator_id AS "creatorDbId",
        shipment_item.modifier_id AS "modifierDbId",
        shipment_item.notes  AS "notes",
        germplasm.designation AS "germplasmDesignation",
        germplasm.parentage AS "germplasmParentage",
        germplasm.germplasm_state AS "germplasmState",
        germplasm.germplasm_state AS "germplasmNormalizedName",
        germplasm.germplasm_state AS "germplasmType",
        germplasm.generation AS "germplasmGeneration",
        seed.seed_code as "seedCode",
        seed.seed_name as "seedName",
        seed.harvest_date as "harvestDate",
        seed.source_experiment_id as "sourceExperimentDbId",
        seed.source_occurrence_id as "seedOccurrenceDbId",
        seed.source_location_id as "seedLocationDbId",
        package.package_code as "packageCode",
        package.package_label as "packageLabel"
      FROM
        inventory.shipment_item shipment_item
        LEFT JOIN germplasm.germplasm as germplasm on germplasm.id=shipment_item.germplasm_id
				LEFT JOIN germplasm.seed as seed on seed.id=shipment_item.seed_id
				LEFT JOIN germplasm.package as package on package.id=shipment_item.package_id
        WHERE
          shipment_item.id = (:shipmentItemDbId) AND
          shipment_item.is_void = FALSE 
        `;

    // Retrieve project from the database
    let shipmentItems = await sequelize
      .query(shipmentItemsQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          shipmentItemDbId: shipmentItemDbId,
        },
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    // Return error if resource is not found
    if (
      (await shipmentItems) === undefined ||
      (await shipmentItems.length) < 1
    ) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404017);
      res.send(new errors.NotFoundError(errMsg));
      return;
    }

    res.send(200, {
      rows: shipmentItems,
    });
    return;
  },

  // Endpoint for updating an existing shipment item record
  // PUT /v3/shipment-item/:id
  put: async function (req, res, next) {
    // Retrieve the shipment item ID
    let shipmentItemDbId = req.params.id;

    if (!validator.isInt(shipmentItemDbId)) {
      let errMsg = `Invalid format, shipment Item ID must be an integer.`;
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Set defaults
    let shipmentDbId = null;
    let germplasmDbId = null;
    let seedDbId = null;
    let packageDbId = null;
    let shipmentItemNumber = null;
    let shipmentItemCode = null;
    let shipmentItemStatus = null;
    let shipmentItemWeight = null;
    let packageCount = null;
    let packageUnit = null;
    let testCode = null;
    let mtaStatus = null;
    let availability = null;
    let use = null;
    let mlsAncestors = null;
    let geneticStock = null;
    let remarks = null;
    let creatorId = null;
    let modifierId = null;
    let notes = null;

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req);

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Check if shipment item is existing
    // Build query
    let shipmentItemsQuery = `
      SELECT
        shipment_item.id AS "shipmentItemDbId",
        shipment_item.shipment_id AS "shipmentDbId" , 
        shipment_item.germplasm_id  AS "germplasmDbId",
        shipment_item.seed_id AS "seedDbId",
        shipment_item.package_id AS "packageDbId",
        shipment_item.shipment_item_number AS "shipmentItemNumber",
        shipment_item.shipment_item_code AS "shipmentItemCode",
        shipment_item.shipment_item_status AS "shipmentItemStatus",
        shipment_item.shipment_item_weight AS "shipmentItemWeight",
        shipment_item.package_unit AS "packageUnit",
        shipment_item.package_count  AS "packageCount",
        shipment_item.test_code AS "testCode",
        shipment_item.mta_status AS "mtaStatus",
        shipment_item.availability AS "availability",
        shipment_item.use AS "use",
        shipment_item.mls_ancestors AS "mlsAncestors",
        shipment_item.genetic_stock AS "geneticStock",
        shipment_item.remarks AS "remarks",
        shipment_item.creator_id AS "creatorDbId",
        shipment_item.modifier_id AS "modifierDbId",
        shipment_item.notes  AS "notes"
      FROM
        inventory.shipment_item shipment_item
      WHERE
        shipment_item.is_void = FALSE AND
        id = ${shipmentItemDbId}
        `;

    // Retrieve shipment from the database
    let shipmentItem = await sequelize
      .query(shipmentItemsQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    if ((await shipmentItem) === undefined || (await shipmentItem.length) < 1) {
      let errMsg = `The shipment Item you have requested does not exist.`;
      res.send(new errors.NotFoundError(errMsg));
      return;
    }

    // Get values from database, record = the current value in the database
    let recordShipmentItemDbId = shipmentItem[0].shipmentItemDbId;
    let recordShipmentDbId = shipmentItem[0].shipmentDbId;
    let recordGermplasmDbId = shipmentItem[0].germplasmDbId;
    let recordSeedDbId = shipmentItem[0].seedDbId;
    let recordShipmentItemNumber = shipmentItem[0].shipmentItemNumber;
    let recordShipmentItemCode = shipmentItem[0].shipmentItemCode;
    let recordShipmentItemStatus = shipmentItem[0].shipmentItemStatus;
    let recordShipmentItemWeight = shipmentItem[0].shipmentItemWeight;
    let recordPackageUnit = shipmentItem[0].packageUnit;
    let recordPackageCount = shipmentItem[0].packageCount;
    let recordTestCode = shipmentItem[0].testCode;
    let recordMtaStatus = shipmentItem[0].mtaStatus;
    let recordAvailability = shipmentItem[0].availability;
    let recordUse = shipmentItem[0].use;
    let recordMlsAncestors = shipmentItem[0].mlsAncestors;
    let recordGeneticStock = shipmentItem[0].geneticStock;
    let recordRemarks = shipmentItem[0].remarks;
    let recordCreatorId = shipmentItem[0].creatorId;
    let recordModifierId = shipmentItem[0].modifierId;
    let recordNotes = shipmentItem[0].notes;

    // add additional fields

    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipment-items/" +
      shipmentItemDbId;

    // Check is req.body has parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Parse the input
    let data = req.body;

    // Start transaction
    let transaction;
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false });

      let setQuery = ``;
      let validateQuery = ``;
      let validateCount = 0;

      /** Validation of parameters and set values **/

      if (data.shipmentDbId !== undefined) {
        shipmentDbId = data.shipmentDbId;

        if (!validator.isInt(shipmentDbId)) {
          let errMsg = "Invalid request shipment ID must be an integer.";
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (shipmentDbId != recordShipmentDbId) {
          // Validate shipment ID
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            --- Check if shipment is existing return 1
            SELECT 
              count(1)
            FROM
              inventory.shipment shipment
            WHERE 
              shipment.is_void = FALSE AND
              shipment.id = ${shipmentDbId}
            )
        `;
          validateCount += 1;

          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            program_id = $$${shipmentDbId}$$
            `;
        }
      } else {
        shipmentDbId = recordShipmentDbId;
      }

      if (data.germplasmDbId !== undefined) {
        germplasmDbId = data.germplasmDbId;

        if (!validator.isInt(germplasmDbId)) {
          let errMsg = "Invalid request germplasm ID must be an integer.";
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (germplasmDbId != recordGermplasmDbId) {
          // Validate germpaslm ID
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
                (
                --- Check if germplasm is existing return 1
                SELECT 
                    count(1)
                FROM
                    germplasm.germplasm germplasm
                WHERE 
                    germplasm.is_void = FALSE AND
                    germplasm.id = ${germplasmDbId}
                )
            `;
          validateCount += 1;

          setQuery += setQuery != "" ? "," : "";
          setQuery += `
                germplasm_id = $$${germplasmDbId}$$
            `;
        }
      } else {
        germplasmDbId = recordGermplasmDbId;
      }

      if (data.seedDbId !== undefined) {
        seedDbId = data.seedDbId;

        if (!validator.isInt(seedDbId)) {
          let errMsg = "Invalid request seed ID must be an integer.";
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (seedDbId != recordSeedDbId) {
          // Validate seed ID
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            --- Check if seed is existing return 1
            SELECT 
                count(1)
            FROM
                germplasm.seed seed
            WHERE 
                seed.is_void = FALSE AND
                seed.id = ${seedDbId}
            )
            `;
          validateCount += 1;

          setQuery += setQuery != "" ? "," : "";
          setQuery += `seed_id = $$${seedDbId}$$`;
        }
      } else {
        seedDbId = recordSeedDbId;
      }

      //validate shipment Item Number
      if (data.shipmentItemNumber !== undefined) {
        shipmentItemNumber = data.shipmentItemNumber;

        if (!validator.isInt(shipmentItemNumber)) {
          let errMsg =
            "Invalid request shipment Item Number must be an integer.";
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (shipmentItemNumber != recordShipmentItemNumber) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `shipment_item_number = $$${shipmentItemNumber}$$`;
        }
      }

      // validate ShipmentItemCode
      if (data.shipmentItemCode !== undefined) {
        shipmentItemCode = data.shipmentItemCode.trim();

        // Check if user input is same with the current value in the database
        if (shipmentItemCode != recordShipmentItemCode) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `shipment_item_code = $$${shipmentItemCode}$$`;
        }
      }

      // validate recordShipmentItemStatus
      if (data.shipmentItemStatus !== undefined) {
        shipmentItemStatus = data.shipmentItemStatus.trim();

        // Check if user input is same with the current value in the database
        if (shipmentItemStatus != recordShipmentItemStatus) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `shipment_item_status = $$${shipmentItemStatus}$$`;
        }
      }

      // validate shipmentItemWeight
      if (data.shipmentItemWeight !== undefined) {
        shipmentItemWeight = data.shipmentItemWeight;

        // Check if user input is same with the current value in the database
        if (shipmentItemWeight != recordShipmentItemWeight) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `shipment_item_weight = $$${shipmentItemWeight}$$`;
        }
      }

      // validate packageUnit
      if (data.packageUnit !== undefined) {
        packageUnit = data.packageUnit;

        // Check if user input is same with the current value in the database
        if (packageUnit != recordPackageUnit) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `package_unit = $$${packageUnit}$$`;
        }
      }

      // validate recordPackageCount
      if (data.packageCount !== undefined) {
        packageCount = data.packageCount;

        if (!validator.isInt(packageCount)) {
          let errMsg =
            "Invalid request shipment item package count  must be an integer.";
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (packageCount != recordPackageCount) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `package_count = $$${packageCount}$$`;
        }
      }

      // validate testCode
      if (data.testCode !== undefined) {
        testCode = data.testCode;

        // Check if user input is same with the current value in the database
        if (testCode != recordTestCode) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `test_code = $$${testCode}$$`;
        }
      }

      // validate mtaStatus
      if (data.mtaStatus !== undefined) {
        mtaStatus = data.mtaStatus;

        // Check if user input is same with the current value in the database
        if (mtaStatus != recordMtaStatus) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `mta_status = $$${mtaStatus}$$`;
        }
      }

      // validate availability
      if (data.availability !== undefined) {
        availability = data.availability;

        // Check if user input is same with the current value in the database
        if (availability != recordAvailability) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `availability = $$${availability}$$`;
        }
      }

      // validate use
      if (data.use !== undefined) {
        use = data.use;

        // Check if user input is same with the current value in the database
        if (use != recordUse) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `use = $$${use}$$`;
        }
      }

      // validate mlsAncestors
      if (data.mlsAncestors !== undefined) {
        mlsAncestors = data.mlsAncestors;

        // Check if user input is same with the current value in the database
        if (mlsAncestors != recordMlsAncestors) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `mls_ancestors = $$${mlsAncestors}$$`;
        }
      }

      // validate geneticStock
      if (data.geneticStock !== undefined) {
        geneticStock = data.geneticStock;

        // Check if user input is same with the current value in the database
        if (geneticStock != recordGeneticStock) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `genetic_stock = $$${geneticStock}$$`;
        }
      }

      // validate remarks
      if (data.remarks !== undefined) {
        remarks = data.remarks.trim();

        // Check if user input is same with the current value in the database
        if (remarks != recordRemarks) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `remarks = $$${remarks}$$`;
        }
      }

      // validate creator_id
      if (data.creator_id !== undefined) {
        creatorId = data.creatorId;

        // Check if user input is same with the current value in the database
        if (creatorId != recordCreatorId) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `creator_id = $$${creatorId}$$`;
        }
      }

      // validate modifierId
      if (data.modifierId !== undefined) {
        modifierId = data.modifierId;

        // Check if user input is same with the current value in the database
        if (modifierId != recordModifierId) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `modifier_id = $$${modifierId}$$`;
        }
      }

      // validate notes
      if (data.notes !== undefined) {
        notes = data.notes.trim();

        // Check if user input is same with the current value in the database
        if (notes != recordNotes) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `notes = $$${notes}$$`;
        }
      }

      /** Validation of input in database */
      // count must be equal to validateCount if all conditions are met
      if (validateCount > 0) {
        let checkInputQuery = `
          SELECT
              ${validateQuery}
          AS count`;

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT,
        });

        if (inputCount[0]["count"] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015);
          res.send(new errors.BadRequestError(errMsg));
          return;
        }
      }

      if (setQuery.length > 0) setQuery += `,`;

      // Update the shipment item record
      let updateshipmentItemsQuery = `
        UPDATE
            inventory.shipment_item  
        SET
            ${setQuery}
            modification_timestamp = NOW(),
            modifier_id = ${personDbId}
        WHERE
            id = ${shipmentItemDbId}
        `;

      await sequelize.query(updateshipmentItemsQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      // Return the transaction info
      let resultArray = {
        shipmentItemDbId: shipmentItemDbId,
        recordCount: 1,
        href: shipmentUrlString,
      };

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: resultArray,
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500003);
      res.send(new errors.InternalError(err));
      return;
    }
  },

  // Implementation of delete call for /v3/shipmentItems/{id}
  delete: async function (req, res, next) {
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Defaults
    let shipmentItemDbId = req.params.id;
    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipment-items";

    if (!validator.isInt(shipmentItemDbId)) {
      let errMsg = "Invalid format, shipmentItems ID must be an integer";
      res.send(new errors.BadRequestError(errMsg));
      return;
    }
    // Start transaction
    let transaction;

    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false });
      // Build query
      let shipmentItemsQuery = `
        SELECT
          count(1)
        FROM 
          inventory.shipment_item shipment_item
        WHERE
          shipment_item.is_void = FALSE AND
          shipment_item.id = ${shipmentItemDbId}
      `;

      // Retrieve shipment from the database
      let hasShipmentItems = await sequelize
        .query(shipmentItemsQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      // Check if shipment is non-existent in database
      if (hasShipmentItems == undefined || hasShipmentItems < 1) {
        let errMsg = `The shipment item you requested does not exist.`;
        res.send(new errors.NotFoundError(errMsg));
        return;
      }

      // Build query for voiding
      let deleteshipmentItemsQuery = `
        UPDATE
          inventory.shipment_item shipment_item
        SET
          is_void = TRUE,
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          shipment_item.id = ${shipmentItemDbId}
        `;

      await sequelize.query(deleteshipmentItemsQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: {
          shipmentItemDbId: shipmentItemDbId,
          recordCount: 1,
          href: shipmentUrlString + "/" + shipmentItemDbId,
        },
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500002);
      res.send(new errors.InternalError(errMsg));
      return;
    }
  },
};
