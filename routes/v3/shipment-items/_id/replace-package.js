/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let errorBuilder = require("../../../../helpers/error-builder");
let tokenHelper = require("../../../../helpers/auth/token");
let validator = require("validator");
let userValidator = require("../../../../helpers/person/validator.js");
let forwarded = require("forwarded-for");
let format = require("pg-format");

module.exports = {
  /**
   * POST /v3/shipment-items/:id/replace-package
   *
   * @param packageCode string as a path parameter
   * @param token string token
   *
   * @return object response data
   */

  post: async function (req, res, next) {
    // Retrieve package by packageCode
    let shipmentItemDbId = req.params.id;
    let packageCode = req.params.packageCode;

    let isSecure = forwarded(req, req.headers).secure;
    // Set the URL for response
    let shipmentUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipment-items/" +
      shipmentItemDbId;

    if (!validator.isInt(shipmentItemDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400025);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Retrieve user ID of client from access token
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.UnauthorizedError(errMsg));
      return;
    }

    let isAdmin = await userValidator.isAdmin(userDbId);

    let transaction;
    try {
      transaction = await sequelize.transaction({ autocommit: false });
      let setQuery = ``;
      let validateQuery = ``;
      let validateCount = 0;

      let packageRecordQuery = `
        SELECT
            package.id as "package_id",
            seed.id as "seed_id",
            germplasm.id as "germplasm_id",
            germplasm_mta.mta_status as "mta_status",
            germplasm_mta.use as "use",
            germplasm_mta.mls_ancestors as "mls_ancestors",
            germplasm_mta.genetic_stock as "genetic_stock"
        FROM germplasm.package as package
        LEFT JOIN germplasm.seed as seed on package.seed_id=seed.id
        LEFT JOIN germplasm.germplasm as germplasm on seed.germplasm_id=germplasm.id
        LEFT JOIN germplasm.germplasm_mta as germplasm_mta on germplasm.id=germplasm_mta.germplasm_id
        WHERE 
          package_code='${packageCode}'
        `;

      // Retrieve list member from the database
      let packageRecord = await sequelize
        .query(packageRecordQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      if (
        (await packageRecord) === undefined ||
        (await packageRecord.length) < 1
      ) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404008);
        res.send(new errors.NotFoundError(errMsg));
        return;
      }

      let recordGermplasmDbId = packageRecord[0].germplasm_id;
      let recordSeedDbId = packageRecord[0].seed_id;
      let recordPackageDbId = packageRecord[0].package_id;
      let recordMtaStatus = packageRecord[0].mta_status;
      let recordUse = packageRecord[0].use;
      let recordMlsAncestors = packageRecord[0].mls_ancestors;
      let recordGeneticStock = packageRecord[0].genetic_stock;

      // Update the shipment item record
      let updateshipmentItemsQuery = `
        UPDATE
            inventory.shipment_item  
        SET
            ${setQuery}
            germplasm_id=${recordGermplasmDbId},
            seed_id=${recordSeedDbId},
            package_id=${recordPackageDbId},
            mta_status=$$${recordMtaStatus}$$,
            use=$$${recordUse}$$,
            mls_ancestors=$$${recordMlsAncestors}$$,
            genetic_stock = $$${recordGeneticStock}$$,
            modification_timestamp = NOW(),
            modifier_id = ${userDbId}
        WHERE
            id = ${shipmentItemDbId}
        `;

      await sequelize.query(updateshipmentItemsQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      // Return the transaction info
      let resultArray = {
        shipmentItemDbId: shipmentItemDbId,
        recordCount: 1,
        href: shipmentUrlString,
      };

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: resultArray,
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500003);
      res.send(new errors.InternalError(err));
      return;
    }
  },
};
