/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../config/sequelize");
let errors = require("restify-errors");
let tokenHelper = require("../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let format = require("pg-format");
let errorBuilder = require("../../../helpers/error-builder");

module.exports = {
  // Endpoint for adding a shipment item record
  post: async function (req, res, next) {
    let resultArray = [];
    // Retrieve the ID of the client via the access token
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentItemsUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipment-items";

    // Check for parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Parse the input
    let data = req.body;
    let records = [];

    try {
      records = data.records;
      recordCount = records.length;
      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004);
      // res.send(new errors.InternalError(errMsg));
      res.send(new errors.InternalError(err));
      return;
    }

    // Start transaction
    let transaction;

    try {
      transaction = await sequelize.transaction({ autocommit: false });

      let shipmentItemValuesArray = [];
      let visitedArray = [];

      for (var record of records) {
        let shipmentDbId = null;
        let germplasmDbId = null;
        let seedDbId = null;
        let packageDbId = null;
        let shipmentItemNumber = null;
        let shipmentItemCode = null;
        let shipmentItemStatus = null;
        let shipmentItemWeight = null;
        let packageCount = null;
        let testCode = null;
        let mtaStatus = null;
        let availability = null;
        let use = null;
        let mlsAncestors = null;
        let geneticStock = null;
        let remarks = null;
        let creatorId = null;
        let modifierId = null;
        let notes = null;

        let validateQuery = ``;
        let validateCount = 0;

        // Check if the required columns are in the input
        if (
          record.shipmentDbId == undefined ||
          record.germplasmDbId == undefined ||
          record.seedDbId == undefined ||
          record.packageDbId == undefined
        ) {
          let errMsg =
            "Invalid request. Required parameters are missing.Ensure that   are not empty" +
            record.shipmentDbId +
            " " +
            record.germplasmDbId +
            " " +
            record.seedDbId +
            " " +
            record.packageDbId;
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Validate and set values

        if (record.shipmentDbId !== undefined) {
          shipmentDbId = record.shipmentDbId;
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            -- Check if shipmentDbId is existing, return 1
            SELECT
                count(1)
            FROM
                inventory.shipment shipment
            WHERE
                shipment.is_void = FALSE AND
                shipment.id = ${shipmentDbId}
            )`;

          validateCount += 1;
        }

        if (record.germplasmDbId !== undefined) {
          germplasmDbId = record.germplasmDbId;
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            -- Check if germplasmDbId is existing, return 1
            SELECT
                count(1)
            FROM
                germplasm.germplasm germplasm
            WHERE
                germplasm.is_void = FALSE AND
                germplasm.id = ${germplasmDbId}
            )`;

          validateCount += 1;
        }

        if (record.seedDbId !== undefined) {
          seedDbId = record.seedDbId;
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            -- Check if seedDbId is existing, return 1
            SELECT
                count(1)
            FROM
                germplasm.seed seed
            WHERE
                seed.is_void = FALSE AND
                seed.id = ${seedDbId}
            )`;

          validateCount += 1;
        }

        if (record.packageDbId !== undefined) {
          packageDbId = record.packageDbId;
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            -- Check if packageDbId is existing, return 1
            SELECT
                count(1)
            FROM
                germplasm.package package
            WHERE
                package.is_void = FALSE AND
                package.id = ${packageDbId}
            )`;

          validateCount += 1;
        }

        shipmentItemNumber =
          record.shipmentItemNumber != undefined
            ? record.shipmentItemNumber
            : null;
        shipmentItemCode =
          record.shipmentItemCode != undefined ? record.shipmentItemCode : null;
        shipmentItemStatus =
          record.shipmentItemStatus != undefined
            ? record.shipmentItemStatus
            : null;
        shipmentItemWeight =
          record.shipmentItemWeight != undefined
            ? record.shipmentItemWeight
            : null;
        packageUnit =
          record.packageUnit != undefined ? record.packageUnit : null;
        packageCount =
          record.packageCount != undefined ? record.packageCount : null;
        testCode = record.testCode != undefined ? record.testCode : null;
        mtaStatus = record.mtaStatus != undefined ? record.mtaStatus : null;
        availability =
          record.availability != undefined ? record.availability : null;
        use = record.use != undefined ? record.use : null;
        mlsAncestors =
          record.mlsAncestors != undefined ? record.mlsAncestors : null;
        geneticStock =
          record.geneticStock != undefined ? record.geneticStock : null;
        remarks = record.remarks != undefined ? record.remarks : null;
        creatorId = record.creatorId != undefined ? record.creatorId : null;
        modifierId = record.modifierId != undefined ? record.modifierId : null;
        notes = record.notes != undefined ? record.notes : null;

        // count must be equal to validateCount if all conditions are met
        let checkInputQuery = `SELECT ${validateQuery} AS count`;

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT,
        });

        if (inputCount[0]["count"] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015);
          res.send(
            new errors.BadRequestError("checkInputQuery: " + checkInputQuery)
          );
          return;
        }

        // Get values
        let tempArray = [
          shipmentDbId,
          germplasmDbId,
          seedDbId,
          packageDbId,
          shipmentItemNumber,
          shipmentItemCode,
          shipmentItemStatus,
          shipmentItemWeight,
          packageUnit,
          packageCount,
          testCode,
          mtaStatus,
          availability,
          use,
          mlsAncestors,
          geneticStock,
          remarks,
          userDbId,
          modifierId,
          notes,
        ];

        shipmentItemValuesArray.push(tempArray);
      }

      // Create shipment item record
      let shipmentItemQuery = format(
        `
        INSERT INTO inventory.shipment_item (
          shipment_id,
          germplasm_id,
          seed_id,
          package_id,
          shipment_item_number,
          shipment_item_code,
          shipment_item_status,
          shipment_item_weight,
          package_unit,
          package_count,
          test_code,
          mta_status,
          availability,
          use,
          mls_ancestors,
          genetic_stock,
          remarks,
          creator_id,
          modifier_id,
          notes
        )
        VALUES 
          %L
        RETURNING 
          id`,
        shipmentItemValuesArray
      );

      let shipmentItems = await sequelize.query(shipmentItemQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction,
      });

      for (shipmentItem of shipmentItems[0]) {
        shipmentItemDbId = shipmentItem.id;

        // Return the shipment info
        let shipmentItemsObj = {
          shipmentItemDbId: shipmentItemDbId,
          recordCount: 1,
          href: shipmentItemsUrlString + "/" + shipmentItemDbId,
        };

        resultArray.push(shipmentItemsObj);
      }

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: resultArray,
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500001);
      res.send(new errors.InternalError("err: " + errMsg));
      return;
    }
  },
};
