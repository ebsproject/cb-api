/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let {sequelize} = require('../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let format = require('pg-format')

module.exports = {
    /**
     * Create germplasm relation records
     * POST /v3/germplasm-relations
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res, next) {
        let parentGermplasmDbId
        let childGermplasmDbId
        let orderNumber
        
        // Get user ID
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let germplasmRelationsUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/germplasm-relations'
        
        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = [] 
        let recordCount = 0

        try {
            records = data.records
            recordCount = records.length
            // Check if the input data is empty
            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } 
        catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let germplasmRelationsArray = []
            for (var record of records) {

                // check if the required columns are in the input
                if (
                    record.parentGermplasmDbId === undefined 
                    || record.childGermplasmDbId === undefined 
                    || record.orderNumber === undefined
                ) {
                    let errMsg = 'Required parameters are missing. ' +
                        'Ensure that the parentGermplasmDbId, childGermplasmDbId, ' +
                        'and orderNumber fields are not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                parentGermplasmDbId = record.parentGermplasmDbId
                childGermplasmDbId = record.childGermplasmDbId
                orderNumber = record.orderNumber

                // check if parent germplasm id is an integer
                if (!validator.isInt(parentGermplasmDbId)) {
                    let errMsg = "Invalid format, parent germplasm ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if parent germplasm exists
                let parentGermplasmQuery = `
                    SELECT
                        count(1)
                    FROM
                        germplasm.germplasm
                    WHERE
                        id = ${parentGermplasmDbId}
                        AND is_void = FALSE
                `

                let parentGermplasm = await sequelize.query(parentGermplasmQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
                if (await parentGermplasm == undefined){
                    return
                }

                if (parentGermplasm[0] !== undefined && parentGermplasm[0] !== null 
                    && parentGermplasm[0]['count'] !== undefined && parentGermplasm[0]['count'] !== null
                    && parseInt(parentGermplasm[0]['count']) === 0
                ) {
                    let errMsg = "The parent germplasm record does not exist."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if child germplasm id is an integer
                if (!validator.isInt(childGermplasmDbId)) {
                    let errMsg = "Invalid format, child germplasm ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if child germplasm exists
                let childGermplasmQuery = `
                    SELECT
                        count(1)
                    FROM
                        germplasm.germplasm
                    WHERE
                        id = ${childGermplasmDbId}
                        AND is_void = FALSE
                `

                let childGermplasm = await sequelize.query(childGermplasmQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
                if (await childGermplasm == undefined){
                    return
                }

                if (childGermplasm[0] !== undefined && childGermplasm[0] !== null 
                    && childGermplasm[0]['count'] !== undefined && childGermplasm[0]['count'] !== null
                    && parseInt(childGermplasm[0]['count']) === 0
                ) {
                    let errMsg = "The child germplasm record does not exist."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if order number is an integer
                if (!validator.isInt(orderNumber)) {
                    let errMsg = "Invalid format, order number must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if parent germplasm id = child germplasm id
                if (parentGermplasmDbId === childGermplasmDbId) {
                    let errMsg = "The parent germplasm ID cannot be the same as the child germplasm ID."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if child germplasm-order number combination already exists
                let childRelationOrderNumberQuery = `
                    SELECT
                        count(1)
                    FROM
                        germplasm.germplasm_relation
                    WHERE
                        child_germplasm_id = ${childGermplasmDbId}
                        AND order_number = ${orderNumber}
                        AND is_void = FALSE
                `

                let childRelationOrderNumber = await sequelize.query(childRelationOrderNumberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
                if (childRelationOrderNumber === undefined) {
                    return
                }

                if (childRelationOrderNumber !== null && await childRelationOrderNumber[0] !== undefined
                    && await childRelationOrderNumber[0] !== null
                    && childRelationOrderNumber[0]['count'] > 0
                ){
                    let errMsg = "The order number already exists for the child germplasm."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if child germplasm-parent germplasm combination already exists
                let childRelationParentQuery = `
                    SELECT
                        count(1)
                    FROM
                        germplasm.germplasm_relation
                    WHERE
                        (
                            (
                                child_germplasm_id = ${childGermplasmDbId}
                                AND parent_germplasm_id = ${parentGermplasmDbId}
                            )
                            OR
                            (
                                child_germplasm_id = ${parentGermplasmDbId}
                                AND parent_germplasm_id = ${childGermplasmDbId}
                            )
                        )
                        AND is_void = FALSE
                `

                let childRelationParent = await sequelize.query(childRelationParentQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
                if (childRelationParent === undefined) {
                    return
                }

                if (childRelationParent !== null && await childRelationParent[0] !== undefined
                    && await childRelationParent[0] !== null
                    && childRelationParent[0]['count'] > 0
                ){
                    let errMsg = "The parent-child combination already exists."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Add data to insertion array
                let tempArray = [
                    parentGermplasmDbId,
                    childGermplasmDbId,
                    orderNumber,
                    userDbId
                ]

                germplasmRelationsArray.push(tempArray)
            }

            // Create germplasm relation
            let germplasmRelationQuery = format (`
                INSERT INTO germplasm.germplasm_relation
                    (parent_germplasm_id, child_germplasm_id, order_number, creator_id)
                VALUES 
                    %L
                RETURNING id`, germplasmRelationsArray
            )

            let germplasmRelations = await sequelize.query(germplasmRelationQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })
                .catch(async err => {
                    return
                })

            // Check if insertion query returned results
            if (germplasmRelations == undefined || germplasmRelations[0].length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            let resultArray = []
            for (let germplasmRelation of germplasmRelations[0]) {
                germplasmRelationDbId = germplasmRelation.id
                // Return the germplasm file upload info to Client
                let array = {
                    germplasmRelationDbId: germplasmRelationDbId,
                    recordCount: 1,
                    href: germplasmRelationsUrlString + '/' + germplasmRelationDbId
                }
                resultArray.push(array)
            }

            await transaction.commit()
            
            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch (e) {
            // Rollback transaction (if still open)
            if (transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return   
        }
    }
}