/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let validator = require('validator')

module.exports = {
  // POST /v3/location-data
  post: async function (req, res, next) {
    let resultArray = []
    
    // Retrieve the ID of the client via access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isSecure = forwarded(req, req.headers).secure

    // Set the URL for response
    let locationDataUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/location-data'

    // Check for parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Parse the input
    let data = req.body
    let records = []

    try {
      records = data.records
      recordCount = records.length

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let locationDataValuesArray = []

      for (var record of records) {
        let validateQuery = ''
        let validateCount = 0

        // Declare the variables
        let locationDbId = null
        let variableDbId = null
        let dataValue = null
        let dataQcCode = null

        // Check if the required parameters exist
        if (
          record.locationDbId == undefined ||
          record.variableDbId == undefined ||
          record.dataValue == undefined ||
          record.dataQcCode == undefined
        ) {
          let errMsg =
            `Required parameters/s are missing. Ensure that the location ID, variable ID, data value, and data QC code fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        locationDbId = record.locationDbId
        if (!validator.isInt(locationDbId)) {
          let errMsg = 'Invalid request, location ID must be an integer.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }
        
        // Check if it exists
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if the location exists, return 1
            SELECT
              count(1)
            FROM
              experiment.location location
            WHERE
              location.is_void = FALSE AND
              location.id = ${locationDbId}
          )
        `

        validateCount += 1

        variableDbId = record.variableDbId
        if (!validator.isInt(variableDbId)) {
          let errMsg = 'Invalid request, variable ID must be an integer.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if it exists
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if the variable exists, return 1
            SELECT
              count(1)
            FROM
              master.variable variable
            WHERE
              variable.is_void = FALSE AND
              variable.id = ${variableDbId}
          )
        `

        validateCount += 1

        let validQcCodes = [
          'N',
          'G',
          'Q',
          'S',
          'M',
          'B',
        ]

        // Check if location data qc code is valid
        dataQcCode = record.dataQcCode
        if (!validQcCodes.includes(dataQcCode)) {
          let errMsg =
            'Invalid request, you have entered an invalid value for dataQcCode. The valid values are: N, G, Q, S, M, and B.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }
        
        dataValue = record.dataValue

        // Validate the input
        let checkInputQuery = `
          SELECT ${validateQuery} AS count
        `

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Get values
        let tempArray = [
          locationDbId,
          variableDbId,
          dataValue,
          dataQcCode,
          personDbId,
        ]

        locationDataValuesArray.push(tempArray)
      }

      // Create a location data record
      let locationDataQuery = format(`
        INSERT INTO
          experiment.location_data (
            location_id,
            variable_id,
            data_value,
            data_qc_code,
            creator_id
          )
        VALUES
          %L
        RETURNING
          id
        `, locationDataValuesArray
      )

      let locationData = await sequelize.query(locationDataQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      for (l of locationData[0]) {
        locationDataDbId = l.id
        // Return the location data info
        let ldObj = {
          locationDataDbId: locationDataDbId,
          recordCount: 1,
          href: locationDataUrlString + '/' + locationDataDbId
        }

        resultArray.push(ldObj)
      }

      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}