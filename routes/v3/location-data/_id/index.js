/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

  // Endpoint for updating an existing location data record
  // PUT /v3/location-data/:id
  put: async function (req, res, next) {
    // Retrieve location data ID
    let locationDataDbId = req.params.id

    if(!validator.isInt(locationDataDbId)) {
      let errMsg = `Invalid format, location data ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Set defaults
    let dataValue = null
    let dataQcCode = null

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(personDbId)

    // Check if occurrence is existing
    // Build query
    let locationDataQuery = `
      SELECT
        ld.data_value AS "dataValue",
        ld.data_qc_code AS "dataQcCode",
        occurrence.id As "occurrenceDbId",
        creator.id AS "creatorDbId"
      FROM
        experiment.location_data ld
      LEFT JOIN
        experiment.location_occurrence_group log ON ld.location_id = log.location_id
      LEFT JOIN
        experiment.occurrence occurrence ON log.occurrence_id = occurrence.id
      LEFT JOIN
        tenant.person creator ON creator.id = ld.creator_id
      WHERE
        ld.is_void = FALSE AND
        ld.id = ${locationDataDbId}
    `

    let locationData = await sequelize.query(locationDataQuery, {
      type: sequelize.QueryTypes.SELECT,
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await locationData === undefined || await locationData.length < 1) {
      let errMsg = `The location data you have requested does not exist.`
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Get values from database, record = the current value in the database
    let recordDataValue = locationData[0].dataValue
    let recordDataQcCode = locationData[0].dataQcCode
    let recordOccurrenceDbId = locationData[0].occurrenceDbId
    let recordCreatorDbId = locationData[0].recordCreatorDbId

    // Check if user is an admin or an owner of the plot
    if(!isAdmin && (personDbId != recordCreatorDbId)) {
      let programMemberQuery = `
        SELECT 
          person.id,
          person.person_role_id AS "role"
        FROM 
          tenant.person person
        LEFT JOIN 
          tenant.team_member teamMember ON teamMember.person_id = person.id
        LEFT JOIN 
          tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
        LEFT JOIN 
          tenant.program program ON program.id = programTeam.program_id 
        LEFT JOIN
          experiment.experiment experiment ON experiment.program_id = program.id
        LEFT JOIN
          experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
        WHERE 
          occurrence.id = ${recordOccurrenceDbId} AND
          person.id = ${personDbId} AND
          person.is_void = FALSE
      `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
    }

    let isSecure = forwarded(req, req.headers).secure

    // Set the URL for response
    let locationDataUrlString = (isSecure ? 'https' : 'http')
      + "://"
      + req.headers.host
      + "/v3/location-data/"
      + locationDataDbId

    // Check is req.body has parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Parse the input
    let data = req.body    

    // Start transaction
    let transaction
    try {

      let setQuery = ``

      if (data.dataQcCode !== undefined) {
        dataQcCode = data.dataQcCode

        // Check if user input is same with the current value in the database
        if (dataQcCode != recordDataQcCode) {

          let validDataQcCodeValues = ['N', 'G', 'Q', 'S', 'M', 'B']

          // Validate data QC code
          if(!validDataQcCodeValues.includes(dataQcCode)) {
            let errMsg = `Invalid request, you have provided an invalid
              value for data QC code.`
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_qc_code = $$${dataQcCode}$$
          `
        }
      }

      if (data.dataValue !== undefined) {
        dataValue = data.dataValue

        if (dataValue != recordDataValue) {
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_value = $$${dataValue}$$
          `
        }
      }

      if (setQuery.length > 0) setQuery += `,`

      // Update the location data record
      let updateLocationDataQuery = `
        UPDATE
          experiment.location_data
        SET
          ${setQuery}
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          id = ${locationDataDbId}
      `

      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })
      
      await sequelize.query(updateLocationDataQuery, {
        type: sequelize.QueryTypes.UPDATE,
        transaction: transaction
      })

      // Return the transaction info
      let resultArray = {
        locationDataDbId: locationDataDbId,
        recordCount: 1,
        href: locationDataUrlString
      }
      
      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      res.send(new errors.InternalError(errMsg))
      return
    }
  },

  // Endpoint for deleting an existing location data record
  // DELETE /v3/location-data/:id
  delete: async function (req, res, next) {
    // Retrieve location data ID
    let locationDataDbId = req.params.id

    if(!validator.isInt(locationDataDbId)) {
      let errMsg = `Invalid format, location data ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    let isAdmin = await userValidator.isAdmin(personDbId)

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let locationDataQuery = `
        SELECT
          occurrence.id As "occurrenceDbId",
          creator.id AS "creatorDbId"
        FROM
          experiment.location_data ld
        LEFT JOIN
          experiment.location_occurrence_group log ON ld.location_id = log.location_id
        LEFT JOIN
          experiment.occurrence occurrence ON log.occurrence_id = occurrence.id
        LEFT JOIN
          tenant.person creator ON creator.id = ld.creator_id
        WHERE
          ld.is_void = FALSE AND
          ld.id = ${locationDataDbId}
      `
      
      let locationData = await sequelize.query(locationDataQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })
  
      if (locationData === undefined || locationData.length < 1) {
        let errMsg = `The location data you have requested does not exist.`
        res.send(new errors.NotFoundError(errMsg))
        return
      }
  
      let recordOccurrenceDbId = locationData[0].occurrenceDbId
      let recordCreatorDbId = locationData[0].recordCreatorDbId
  
      // Check if user is an admin or an owner of the plot
      if(!isAdmin && (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
          SELECT 
            person.id,
            person.person_role_id AS "role"
          FROM 
            tenant.person person
          LEFT JOIN 
            tenant.team_member teamMember ON teamMember.person_id = person.id
          LEFT JOIN 
            tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
          LEFT JOIN 
            tenant.program program ON program.id = programTeam.program_id 
          LEFT JOIN
            experiment.experiment experiment ON experiment.program_id = program.id
          LEFT JOIN
            experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
          WHERE 
            occurrence.id = ${recordOccurrenceDbId} AND
            person.id = ${personDbId} AND
            person.is_void = FALSE
        `
  
        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        
        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
        
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
      }

      let deleteLocationDataQuery = format(`
        UPDATE
          experiment.location_data
        SET
          is_void = TRUE,
          notes = CONCAT('VOIDED-', $$${locationDataDbId}$$),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          id = ${locationDataDbId}
      `)

      await sequelize.query(deleteLocationDataQuery, {
        type: sequelize.QueryTypes.UPDATE
      })

      await transaction.commit()
      res.send(200, {
        rows: { locationDataDbId: locationDataDbId }
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}