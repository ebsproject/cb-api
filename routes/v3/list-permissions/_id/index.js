/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let logger = require('../../../../helpers/logger')

const endpoint = 'list-permissions/:id'

module.exports = {
  // Endpoint for updating a list permission
  // Implementation of PUT call for /v3/list-permissions/:id
  put: async function (req, res, next) {
    let listPermissionDbId = req.params.id
    if (!validator.isInt(listPermissionDbId)) {
      let errMsg = 'You have provided an invalid format for the list permission ID.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).secure

    // Set URL for response
    let listPermissionUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/list-permissions'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body

    try {
      let validateListPermissionQuery = `
        (
          SELECT
          CASE WHEN
            (count(1) = 1)
            THEN 1
            ELSE 0
          END AS count
          FROM
            platform.list_access la
          WHERE
            la.is_void = FALSE AND
            la.id = '${listPermissionDbId}'
        )
      `
      validateListPermission = await sequelize
        .query(validateListPermissionQuery, {
          type: sequelize.QueryTypes.SELECT
        })

      if (validateListPermission[0]['count'] != 1) {
        let errMsg = 'The list permission you have requested does not exist.'
        res.send(new errors.NotFoundError(errMsg))
        return
      }
    } catch (e) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    try {
      let permission = data.permission

      if (permission !== 'read' && permission !== 'read_write') {
        let errMsg = 'You have provided an invalid permission value.'
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      let updateListPermissionQuery = `
        UPDATE
          platform.list_access
        SET
          permission = '${permission}',
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          id = ${listPermissionDbId}
      `
      updateListPermissionQuery = updateListPermissionQuery.trim()

      // Start transactions
      transaction = await sequelize.transaction(async transaction => {
        await sequelize.query(updateListPermissionQuery, {
          type: sequelize.QueryTypes.INSERT,
          transaction: transaction,
          raw: true
        }).catch(async err => {
          logger.logFailingQuery(endpoint, 'INSERT', err)
          throw new Error(err)
        })
      })

      let resultArray = {
        listPermissionDbId: listPermissionDbId,
        recordCount: 1,
        href: listPermissionUrlString + '/' + listPermissionDbId
      }

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Show error log message
      logger.logMessage(__filename, err.stack, 'error')
      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      if(!res.headersSent) res.send(new errors.InternalError(errMsg))
      return
    }
  },

  // Endpoint for deleting a list permission
  // Implementation of DELETE call for /v3/list-permissions/:id
  delete: async function (req, res, next) {
    // Set defaults
    let listPermissionDbId = req.params.id
    let listDbId = null

    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).secure

    // Set URL for response
    let listPermissionUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/list-permissions'

    if (!validator.isInt(listPermissionDbId)) {
      let errMsg = 'You have provided an invalid format for the list permission ID.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let transaction

    try {
      let validateListPermissionQuery = `
        SELECT
        CASE WHEN
          (count(1) = 1)
          THEN 1
          ELSE 0
        END AS count
        FROM
          platform.list_access lm
        WHERE
          lm.is_void = false AND
          lm.id = '${listPermissionDbId}'
      `
      validateListPermissionQuery = await sequelize
          .query(validateListPermissionQuery, {
            type: sequelize.QueryTypes.SELECT
          })

      if (validateListPermissionQuery[0]['count'] != 1) {
        let errMsg = 'The list permission you have requested does not exist.'
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      let getListIdQuery = `
        SELECT
          lm.list_id as "listDbId"
        FROM
          platform.list_access lm
        WHERE
          lm.is_void = FALSE AND
          lm.id = '${listPermissionDbId}'
      `
      list = await sequelize.query(getListIdQuery, {
        type: sequelize.QueryTypes.SELECT
      })

      listDbId = list[0]['listDbId']

      let validateListQuery = `
        SELECT
        CASE WHEN
          (count(1) = 1)
          THEN 1
          ELSE 0
        END AS count
        FROM
          platform.list list
        WHERE
          list.is_void = FALSE AND
          list.id = '${listDbId}'
      `
      validateList = await sequelize.query(validateListQuery, {
        type: sequelize.QueryTypes.SELECT
      })

      if (validateList[0]['count'] != 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404031)
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      let validateListCreatorQuery = `
        SELECT
        CASE WHEN
          (count(1) > 0)
          THEN 1
          ELSE 0
        END AS count
        FROM
          platform.list list
        WHERE
          list.is_void = FALSE AND
          list.id = '${listDbId}' AND
          list.creator_id = ${userDbId}
      `
      validateListCreator = await sequelize.query(validateListCreatorQuery, {
        type: sequelize.QueryTypes.SELECT
      })

      if (validateListCreator[0]['count'] != 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 403005)
        res.send(new errors.ForbiddenError(errMsg))
        return
      }

      let deleteListPermissionQuery = `
        UPDATE
          platform.list_access lm
        SET
          is_void = TRUE,
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          lm.id = '${listPermissionDbId}'
      `
      deleteListPermissionQuery = deleteListPermissionQuery.trim()

      // Start transactions
      transaction = await sequelize.transaction(async transaction => {
        await sequelize.query(deleteListPermissionQuery, {
          type: sequelize.QueryTypes.UPDATE,
          transaction: transaction,
          raw: true
        }).catch(async err => {
          logger.logFailingQuery(endpoint, 'DELETE', err)
          throw new Error(err)
        })
      })

      let resultArray = {
        listPermissionDbId: listPermissionDbId,
        recordCount: 1,
        href: listPermissionUrlString + '/' + listPermissionDbId
      }

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Log error message
      logger.logMessage(__filename, err.stack, 'error')
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      if(!res.headersSent) res.send(new errors.InternalError(errMsg))
      return
    }
  }
}