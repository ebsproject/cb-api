/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  // Endpoint for retrieving all plan-configurations
  // GET /v3/plan-configurations
  get: async function (req, res, next) {
    
    // Set defaults 
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let count = 0
    let conditionString = ''
    let orderString = ''

    // Build query
    let planConfigQuery = `
      SELECT
        planConfig.id AS "planConfigDbId",
        planConfig.abbrev,
        planConfig.name,
        planConfig.config_value as "configValue",
        planConfig.rank,
        planConfig.usage,
        planConfig.remarks,
        planConfig.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.display_name AS creator,
        planConfig.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.display_name AS modifier   
      FROM 
        platform.plan_config planConfig
      LEFT JOIN 
        master.user creator ON planConfig.creator_id = creator.id
      LEFT JOIN 
        master.user modifier ON planConfig.modifier_id = modifier.id
      WHERE
        planConfig.is_void = FALSE
      ORDER BY
        planConfig.id
    `

    // Get filter condition for abbrev only
    if (params.abbrev !== undefined) {
      let parameters = {
        abbrev: params.abbrev
      }
      conditionString = await processQueryHelper.getFilterString(parameters)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400003)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)

      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final sql query 
    planConfigFinalQuery = await processQueryHelper.getFinalSqlQuery(
      planConfigQuery,
      conditionString,
      orderString
    )
    
    // Retrieve planConfigs from the database   
    let planConfigs = await sequelize.query(planConfigFinalQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        limit: limit,
        offset: offset
      }
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })
    
    if (await planConfigs == undefined || await planConfigs.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get count 
      planConfigCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(planConfigQuery, conditionString, orderString)

      planConfigCount = await sequelize.query(planConfigCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      count = planConfigCount[0].count
    }

    res.send(200, {
      rows: planConfigs,
      count: count
    })
    return
 }
}
