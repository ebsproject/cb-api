/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize, germplasm } = require('../../../../config/sequelize')
let errors = require('restify-errors')

let validator = require('validator')
let forwarded = require('forwarded-for')
let format = require('pg-format')

let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator')
let germplasmHelper = require('../../../../helpers/germplasm/index')

module.exports = {
    /**
     * Update a germplasm attribute record
     * 
     * @param {*} req Request request
     * @param {*} res 
     * @param {*} next 
     */
    put: async function (req, res, next) {
        let germplasmDbId = null
        let variableDbId = null
        let dataValue = ''
        let dataQcCode = ''

        let errMsg = ''

        // Retrieve user ID
        let userDbId = await tokenHelper.getUserId(req)
  
        if(userDbId == null){
            errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
  
        // Check if user is ADMIN
        let isAdmin = await userValidator.isAdmin(userDbId)
        if(!isAdmin){
            errMsg = await errorBuilder.getError(req.headers.host, 401024)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        if(!req.params){
            return
        }
    
        if(req.body == undefined || req.body == null){
            errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if germplasm_name record is defined
        if(req.params.id == undefined || req.params.id == null){
            errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        // Check if germplasm_name record ID is integer
        geAttributeDbId = req.params.id ?? null
        if(geAttributeDbId != null && !validator.isInt(geAttributeDbId)){
            errMsg = 'Invalid format. Germplasm atribute ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build germplasm attribute query
        let getAttributeQuery = `
            SELECT
                germplasm_attribute.id AS "germplasmAttributeDbId",
                germplasm_attribute.germplasm_id AS "germplasmDbId",
                germplasm_attribute.variable_id AS "variableDbId",
                germplasm_attribute.data_value AS "dataValue",
                germplasm_attribute.data_qc_code AS "dataQcCode"
            FROM
                germplasm.germplasm_attribute germplasm_attribute
            WHERE
                germplasm_attribute.is_void = FALSE
                AND germplasm_attribute.id = ${geAttributeDbId}
        `

        // Retrieve the germplasm name record
        let germplasmAttribute = await sequelize.query(getAttributeQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        // Return error if resource is not found
        if (germplasmAttribute == undefined || germplasmAttribute.length == 0) {
            let errMsg = 'Invalid request. Ensure that the germplasm attribute upload record exists. '
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let germplasmAttribRecord = germplasmAttribute[0] ?? []

        let germplasmAttributeDbId = germplasmAttribRecord.germplasmAttributeDbId ?? null
        let attribGermplasmDbId = germplasmAttribRecord.germplasmDbId ?? ''
        let attribVariableDbId = germplasmAttribRecord.variableDbId ?? ''
        let attribDataValue = germplasmAttribRecord.dataValue ?? ''
        let attribDataQcCode = germplasmAttribRecord.dataQcCode ?? ''
        
        // Set URL for response
        let isSecure = forwarded(req, req.headers).secure
        let germplasmAttribUrlString = (isSecure ? 'https' : 'http')
            + "://" + req.headers.host + "/v3/germplasm-attributes"
  
        let data = req.body
        let transaction
        let resultArray = []

        try{
            let setQuery = ''

            // validate input parameters
            let validateQuery = ''
            let validateCount = 0

            let newGermplasmDbId
            if(data.germplasmDbId != undefined){
                if(data.germplasmDbId == ''){
                    errMsg = 'Invalid parameters. Germplasm ID value should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(data.germplasmDbId != null && !validator.isInt(data.germplasmDbId)){
                    errMsg = 'Invalid format. Germplasm ID must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                newGermplasmDbId = data.germplasmDbId

                if(newGermplasmDbId != attribGermplasmDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_id = ${newGermplasmDbId}`
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        germplasm.germplasm g
                    WHERE
                        g.is_void = FALSE
                        AND g.id = $$${newGermplasmDbId}$$
                )
                `

                validateCount += 1
            }

            let newVariableDbId
            if(data.variableDbId != undefined){
                if(data.variableDbId == ''){
                    errMsg = 'Invalid parameters. Variable ID value should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(data.variableDbId != null && !validator.isInt(data.variableDbId)){
                    errMsg = 'Invalid format. Variable ID must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                newVariableDbId = data.variableDbId

                if(newVariableDbId != attribVariableDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        variable_id = ${newVariableDbId}`
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        master.variable v
                    WHERE
                        v.is_void = FALSE
                        AND v.id = $$${newVariableDbId}$$
                )
                `

                validateCount += 1
            }

            let newDataValue
            if(data.dataValue != undefined){

                if(data.dataValue == ''){
                    errMsg = 'Invalid parameters. Data Value value should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                newDataValue = data.dataValue

                if(newDataValue != attribDataValue){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        data_value = $$${newDataValue}$$`
                }
            }

            let newQcCode
            if(data.dataQcCode != undefined){

                if(data.dataQcCode == ''){
                    errMsg = 'Invalid parameters. Data QC Code value should not be empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                newQcCode = data.dataQcCode

                if(newQcCode != attribDataQcCode){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        data_qc_code = $$${newQcCode}$$`
                }
            }

            if (validateCount > 0){
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (inputCount != undefined && inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0){
                setQuery += `,`
            }

            let updateGermplasmAttribQuery = `
            UPDATE
                    germplasm.germplasm_attribute
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = $$${userDbId}$$
                WHERE
                    id = $$${geAttributeDbId}$$
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            // Update germplasm_name record
            await sequelize.query(updateGermplasmAttribQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let array = {
                germplasmAttributeDbId: geAttributeDbId,
                recordCount: 1,
                href: germplasmAttribUrlString + '/' + geAttributeDbId
            }

            resultArray.push(array)

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch(e){
            // Rollback transaction (if still open)
            if (transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return  
        }
    },

    /**
     * Delete germplasm attribute record
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    delete: async function (req, res, next) {
        let errMsg = ''

        let germplasmAttributeDbId = req.params.id

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if user is an admin
        if(!isAdmin) {
            errMsg = await errorBuilder.getError(req.headers.host, 401027)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Check if germplasm attribute ID is an integer
        if (!validator.isInt(germplasmAttributeDbId)) {
            errMsg = await errorBuilder.getError(req.headers.host, 400249)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve germplasm attribute record
        let germplasmAttributeQuery = `
            SELECT
                ga.id AS "germplasmAttributeDbId"
            FROM 
                germplasm.germplasm_attribute ga
            WHERE 
                ga.is_void = FALSE AND 
                ga.id = ${germplasmAttributeDbId}
        `
    
        let germplasmAttributeRecord = await sequelize.query(
            germplasmAttributeQuery, 
            {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If germplasm attribute retrieval failed, end execution
        if (germplasmAttributeRecord === undefined) {
            return
        }

        // If no record was retrieved, return NotFoundError
        if (germplasmAttributeRecord !== null && germplasmAttributeRecord.length < 1) {
            errMsg = await errorBuilder.getError(req.headers.host, 404055)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
      
            let deleteGermplasmAttributeQuery = format(`
                UPDATE
                    germplasm.germplasm_attribute
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${germplasmAttributeDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${germplasmAttributeDbId}
            `)
      
            await sequelize.query(deleteGermplasmAttributeQuery, {
                type: sequelize.QueryTypes.UPDATE
            })
      
            await transaction.commit()

            res.send(200, {
                rows: { 
                    germplasmAttributeDbId: germplasmAttributeDbId 
                }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (transaction !== undefined && transaction !== null && 
                transaction.finished !== 'commit'
            ) {
                transaction.rollback()
            }
    
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}