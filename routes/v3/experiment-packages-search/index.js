/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// packages search with experiment level source data
let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let validator = require('validator')
let errors = require('restify-errors')
let germplasmHelper = require('../../../helpers/germplasm/index')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        
        let count = 0
        
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let distinctOrderString = ``
        let parameters = {}

        let excludedParametersArray = [ 'fields', 'distinctOn' ]
        let conditionStringArr
        let responseColString = ''
        
        let packageDistinctColumns = ''
        let orderOuter = ''

        parameters['distinctOn'] = ''
        responseColString = `
                experiment.id AS "experimentDbId",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_year AS "experimentYear",
                experiment.experiment_type AS "experimentType",
                stage.id AS "stageDbId",
                stage.stage_name AS "stageName",
                stage.stage_code AS "stageCode",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.id AS "occurrenceDbId",
                program.id AS "programDbId",
                program.program_code AS "programCode",
                "location".id AS "locationDbId",
                "location".location_code AS "locationCode",
                "location".location_name AS "locationName",
                season.id AS "seasonDbId",
                season.season_code AS "seasonCode",
                creator.person_name AS "creator",
                modifier.person_name AS "modifier"
            `

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', 'distinctOn', 'sortByInput', 'names', 'workingListDbId', 'multipleSelect', 'groupLimit', 'retainSelection', '__browser_filters__']

            // Get filter condition
            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ?  conditionStringArr.mainQuery : ''
            tableAliases = conditionStringArr.tableAliases
            columnAliases = conditionStringArr.columnAliases

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `${parameters['distinctOn']}`

                packageDistinctColumns = knex.raw(`${addedDistinctString}`)

                let distinctOnValue = parameters['distinctOn'];

                // Split the value of distinctOn via |
                let columns = distinctOnValue.split('|');
                let distinctOnSort = ''
                for (col of columns) {
                    distinctOnSort += `"${col}",`
                }

                distinctOnSort = distinctOnSort.replace(/,\s*$/, "")
                distinctOrderString = `ORDER BY ${distinctOnSort}`

                orderOuter = ` ORDER BY mainQuery."${parameters['distinctOn']}" `
            }
        }

        try{
            // Build base retrieval query
            let packagesQuery = null
            let fromQuery = `
                FROM
                    experiment.experiment experiment
                    JOIN tenant.program program ON 
                        program.id = experiment.program_id AND program.is_void = FALSE        
                    INNER JOIN LATERAL (
                        SELECT 
                            seed.id,
                            seed.source_location_id,
                            seed.source_occurrence_id 
                        FROM
                            germplasm.seed seed 
                        WHERE
                            seed.source_experiment_id = experiment.id
                            AND seed.is_void = FALSE
                        LIMIT 1
                    ) seed ON TRUE
                    JOIN tenant.season season ON 
                        season.id = experiment.season_id AND season.is_void = FALSE
                    JOIN tenant.stage stage ON 
                        stage.id = experiment.stage_id AND stage.is_void = FALSE
                    JOIN tenant.person creator ON 
                        creator.id = experiment.creator_id
                    JOIN experiment.location "location" ON 
                        "location".id = seed.source_location_id AND location.is_void = FALSE
                    JOIN experiment.occurrence occurrence ON 
                        occurrence.id = seed.source_occurrence_id AND occurrence.is_void = FALSE
                    LEFT JOIN
                        tenant.person modifier ON modifier.id = experiment.modifier_id
                WHERE
                    EXISTS (
                        SELECT 
                            1
                        FROM
                            germplasm.package 
                        WHERE 
                            seed_id = seed.id
                            AND is_void = FALSE
                        LIMIT 1
                    )
            `

            // Check if user specified values in fields parameter
            let addPackageCount = false
            
            if (parameters['fields']) {
                if (parameters['distinctOn']) {

                    let fieldsString = await processQueryHelper
                        .getFieldValuesString(parameters['fields'])
                    let selectString = knex.raw(`${fieldsString}`)
                    packagesQuery = knex.select(selectString)
                    // if fields is specified, surround columns in double quotes
                    packagesQuery = packagesQuery.toString().replace(/(\s+AS\s*)(\w+)(\s*)/g, "$1\"$2\"$3")

                } else {
                    packagesQuery = knex.column(parameters['fields'].split('|'))
                }
                packagesQuery += fromQuery
            } else {
                addPackageCount = true
                packagesQuery = `
                    SELECT 
                        ${responseColString}
                    ${fromQuery}
                    `
            }

            // Parse the sort parameters
            if (sort != null) {
                orderString = await processQueryHelper.getOrderString(sort)
                if (orderString.includes('invalid')) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (distinctOrderString !== '') {
                /**
                 * If distinctOn is specified, add the column to Sort condition first
                 * If fields and sort are specified, the sort columns must be specified in the field
                 */
                orderString = orderString.replace("ORDER BY", ',')
            }
            
            // Generate final SQL query
            let packagesFinalSqlQuery = `
                SELECT 
                    ${packageDistinctColumns}
                    *
                FROM
                    (
                        ${packagesQuery}
                    ) AS tbl
                `+ conditionString
            
            let finalDataCountQuery = await processQueryHelper
                .getFinalTotalCountQuery(packagesFinalSqlQuery)

            let packages = await sequelize
                .query(finalDataCountQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        limit: limit,
                        offset: offset
                    }
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    console.log(err)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await packages === undefined || await packages.length < 1) {
                res.send(200, {
                    rows: [],
                    count: 0
                })
                return
            }

            count = packages[0].totalCount ? parseInt(packages[0].totalCount) : 0

            res.send(200, {
                rows: packages,
                count: count
            })
            return
        }
        catch(error){
            console.log('Error encountered searching for packages. ',error)
        }
    }
}