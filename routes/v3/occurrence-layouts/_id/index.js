/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
let logger = require('../../../../helpers/logger')

const endpoint = 'occurrence-layouts/:id'

module.exports = {
    // Endpoint for updating an existing occurrence-layout record
    // PUT /v3/occurrence-layouts/:id
    put: async function (req, res, next) {
        // Retrieve the occurrence layout ID
        let occurrenceLayoutDbId = req.params.id

        if (!validator.isInt(occurrenceLayoutDbId)) {
            let errMsg = `Invalid format, occurrence layout ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let occurrenceLayoutWidth = null
        let occurrenceLayoutHeight = null
        let occurrenceLayoutOrder = null
        let occurrenceLayoutOrderData = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if occurrence layout record is existing
        // Build query
        let occurrenceLayoutQuery = `
            SELECT
                ol.occurrence_layout_width AS "occurrenceLayoutWidth",
                ol.occurrence_layout_height AS "occurrenceLayoutHeight",
                ol.occurrence_layout_order AS "occurrenceLayoutOrder",
                ol.occurrence_layout_order_data AS "occurrenceLayoutOrderData",
                creator.id AS "creatorDbId",
                occurrence.id AS "occurrenceDbId"
            FROM
                experiment.occurrence_layout ol
            LEFT JOIN
                tenant.person creator ON creator.id = ol.creator_id
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = ol.occurrence_id
            WHERE
                ol.is_void = FALSE AND
                ol.id = ${occurrenceLayoutDbId}
        `

        let occurrenceLayout = await sequelize.query(occurrenceLayoutQuery, {
            type: sequelize.QueryTypes.SELECT,
        }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
        })

        if (await occurrenceLayout === undefined || await occurrenceLayout.length < 1) {
            let errMsg = `The occurrence layout you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordOccurrenceLayoutWidth = occurrenceLayout[0].occurrenceLayoutWidth
        let recordOccurrenceLayoutHeight = occurrenceLayout[0].occurrenceLayoutHeight
        let recordOccurrenceLayoutOrder = occurrenceLayout[0].occurrenceLayoutOrder
        let recordOccurrenceLayoutOrderData = occurrenceLayout[0].occurrenceLayoutOrderData
        let recordOccurrenceDbId = occurrenceLayout[0].occurrenceDbId
        let recordCreatorDbId = occurrenceLayout[0].recordCreatorDbId

        // Check if user is an admin or an owner of the plot
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                WHERE 
                    occurrence.id = ${recordOccurrenceDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let occurrenceLayoutUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/occurrence-layouts/"
            + occurrenceLayoutDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body        

        // Start transaction
        let transaction
        try {
            let setQuery = ``

            /** Validation of parameters and set values **/
            if (data.occurrenceLayoutWidth !== undefined) {
                occurrenceLayoutWidth = data.occurrenceLayoutWidth

                if (!validator.isInt(occurrenceLayoutWidth)) {
                    let errMsg = `Invalid format, occurrence layout width must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (occurrenceLayoutWidth != recordOccurrenceLayoutWidth) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_layout_width = $$${occurrenceLayoutWidth}$$
                    `
                }
            }

            if (data.occurrenceLayoutHeight !== undefined) {
                occurrenceLayoutHeight = data.occurrenceLayoutHeight

                if (!validator.isInt(occurrenceLayoutHeight)) {
                    let errMsg = `Invalid format, occurrence layout height must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (occurrenceLayoutHeight != recordOccurrenceLayoutHeight) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_layout_height = $$${occurrenceLayoutHeight}$$
                    `
                }
            }

            if (data.occurrenceLayoutOrder !== undefined) {
                occurrenceLayoutOrder = data.occurrenceLayoutOrder

                if (occurrenceLayoutOrder != recordOccurrenceLayoutOrder) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_layout_order = $$${occurrenceLayoutOrder}$$
                    `
                }
            }

            if (data.occurrenceLayoutOrderData !== undefined) {
                occurrenceLayoutOrderData = data.occurrenceLayoutOrderData

                if (!validator.isJSON(occurrenceLayoutOrderData)) {
                    let errMsg = `Invalid format, occurrenceLayoutOrderData field must be a valid JSON string.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (occurrenceLayoutOrderData != recordOccurrenceLayoutOrderData) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_layout_order_data = $$${occurrenceLayoutOrderData}$$
                    `
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the occurrence layout record
            let updateOccurrenceLayoutQuery = `
                UPDATE
                    experiment.occurrence_layout
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${occurrenceLayoutDbId}
            `
            
            // Start transaction
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateOccurrenceLayoutQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return the transaction info
            let resultArray = {
                occurrenceLayoutDbId: occurrenceLayoutDbId,
                recordCount: 1,
                href: occurrenceLayoutUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Show error log messages
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for deleting an existing occurrence-layout record
    // DELETE /v3/occurrence-layouts/:id
    delete: async function (req, res, next) {
        // Retrieve the occurrence layout ID
        let occurrenceLayoutDbId = req.params.id

        if (!validator.isInt(occurrenceLayoutDbId)) {
            let errMsg = `Invalid format, occurrence layout ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            let occurrenceLayoutQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    occurrence.id AS "occurrenceDbId"
                FROM
                    experiment.occurrence_layout ol
                LEFT JOIN
                    tenant.person creator ON creator.id = ol.creator_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = ol.occurrence_id
                WHERE
                    ol.is_void = FALSE AND
                    ol.id = ${occurrenceLayoutDbId}
            `

            let occurrenceLayout = await sequelize.query(occurrenceLayoutQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
    
            if (await occurrenceLayout === undefined || await occurrenceLayout.length < 1) {
                let errMsg = `The occurrence layout you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordOccurrenceDbId = occurrenceLayout[0].occurrenceDbId
            let recordCreatorDbId = occurrenceLayout[0].recordCreatorDbId

            // Check if user is an admin or an owner of the plot
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                    WHERE 
                        occurrence.id = ${recordOccurrenceDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                        type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                        res.send(new errors.UnauthorizedError(errMsg))
                        return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                        res.send(new errors.UnauthorizedError(errMsg))
                        return
                }
            }

            let deleteOccurrenceLayoutQuery = format(`
                UPDATE
                    experiment.occurrence_layout
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${occurrenceLayoutDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${occurrenceLayoutDbId}
            `)

            // Start transaction
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(deleteOccurrenceLayoutQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            res.send(200, {
                rows: { occurrenceLayoutDbId: occurrenceLayoutDbId }
            })
            return
        } catch (err) {
            // Show error log messages
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}