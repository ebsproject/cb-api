/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  // Endpoint for adding a file cabinet record
  post: async function (req, res, next) {
    let resultArray = []

    // Retrieve the ID of the client via access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    let isSecure = forwarded(req, req.headers).secure

    // Set the URL for response
    let occurrenceLayoutUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/occurrence-layouts'

    // Check for parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Parse the input
    let data = req.body
    let records = []

    try {
      records = data.records
      recordCount = records.length

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let occurrenceLayoutValuesArray = []

      for (let record of records) {
        let validateQuery = ''
        let validateCount = 0

        // Declare variables
        let occurrenceLayoutWidth = null
        let occurrenceLayoutHeight = null
        let occurrenceLayoutOrder = null
        let occurrenceLayoutOrderData = null
        let occurrenceDbId = null

        if (
          record.occurrenceLayoutWidth == undefined ||
          record.occurrenceLayoutHeight == undefined ||
          record.occurrenceLayoutOrder == undefined ||
          record.occurrenceLayoutOrderData == undefined ||
          record.occurrenceDbId == undefined
        ) {
          let errMsg =
            `Required parameters are missing. Ensure that the occurrence layout width, height, order, order data, and the occurrence ID fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        occurrenceLayoutWidth = record.occurrenceLayoutWidth
        if (!validator.isInt(occurrenceLayoutWidth)) {
          let errMsg = 'Invalid format, occurrence layout width must be an integer.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        occurrenceLayoutHeight = record.occurrenceLayoutHeight
        if (!validator.isInt(occurrenceLayoutHeight)) {
          let errMsg = 'Invalid format, occurrence layout height must be an integer.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        occurrenceDbId = record.occurrenceDbId
        if (!validator.isInt(occurrenceDbId)) {
          let errMsg = 'Invalid format, occurrence ID must be an integer.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if occurrence exists
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if occurrence exists, return 1
            SELECT
              count(1)
            FROM
              experiment.occurrence occurrence
            WHERE
              occurrence.is_void = FALSE AND
              occurrence.id = ${occurrenceDbId}
          )
        `

        validateCount += 1

        occurrenceLayoutOrder = record.occurrenceLayoutOrder

        let checkInputQuery = `
          SELECT ${validateQuery} AS count
        `

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        try {
          occurrenceLayoutOrderData =
            JSON.parse(record.occurrenceLayoutOrderData)

          let tempArray = [
            occurrenceLayoutWidth,
            occurrenceLayoutHeight,
            occurrenceLayoutOrder,
            occurrenceLayoutOrderData,
            occurrenceDbId,
            personDbId,
          ]

          occurrenceLayoutValuesArray.push(tempArray)
        } catch (err) {
          let errMsg = 'Invalid format, occurrence layout order data must be a valid JSON string.'
          res.send(new errors.BadRequestError(errMsg))
          return
        }
      }

      // Insert the occurrence layout records
      let occurrenceLayoutsQuery = format(`
        INSERT INTO
          experiment.occurrence_layout (
            occurrence_layout_width,
            occurrence_layout_height,
            occurrence_layout_order,
            occurrence_layout_order_data,
            occurrence_id,
            creator_id
          )
        VALUES
          %L
        RETURNING
          id
        `, occurrenceLayoutValuesArray
      )

      let occurrenceLayouts = await sequelize.query(occurrenceLayoutsQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      for (occurrenceLayout of occurrenceLayouts[0]) {
        occurrenceLayoutDbId = occurrenceLayout.id
        let ol = {
          occurrenceLayoutDbId: occurrenceLayoutDbId,
          recordCount: 1,
          href: occurrenceLayoutUrlString + '/' + occurrenceLayoutDbId
        }

        resultArray.push(ol)
      }

      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}