/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let validator = require('validator')
const logger = require('../../../../helpers/logger')

module.exports = {

    /**
     * Retrieve the summary of transaction dataset
     * GET /v3/terminal-transactions/:id/dataset-summary
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    get: async function (req, res) {

        let transactionDbId = req.params.id
        let retrieveOccurrences = req.params.retrieveOccurrences == 'false' ? false : true
        let retrieveOccurrencesOnly = req.params.retrieveOccurrencesOnly == 'true' ? true : false
        const endpoint = 'terminal-transactions/:id/dataset-summary'
        const operation = 'SELECT'

        // check if transaction ID is integer
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transactionDbId must be an integer.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let occurrences = []

        if(retrieveOccurrences === true){
            let occurrenceQuery = `
                SELECT 
                    ARRAY_AGG(distinct variable_id::text) "variableDbIds", 
                    data_unit "dataUnit", 
                    count(distinct entity_id::text) "dataUnitCount",
                    occurrence_id::text "occurrenceDbId"
                FROM (
                    SELECT 
                        data.variable_id,
                        data.data_unit ,  
                        data.entity_id,
                        occ.id as occurrence_id
                    FROM 
                        data_terminal.transaction_dataset data
                            LEFT JOIN experiment.plot pd ON pd.id=data.entity_id
                            LEFT JOIN experiment.occurrence occ ON occ.id=pd.occurrence_id
                    WHERE
                        transaction_id =${transactionDbId}
                        and data_unit= 'plot'
                    GROUP BY data.variable_id,occ.id, data.data_unit ,data.entity_id
                    
                    UNION 
                        SELECT 
                            data.variable_id,
                            data.data_unit ,  
                            data.entity_id,
                            cd.occurrence_id
                        FROM 
                            data_terminal.transaction_dataset data
                        LEFT JOIN germplasm."cross" AS cd ON cd.id = data.entity_id
                        WHERE
                            transaction_id =${transactionDbId}
                            AND data_unit= 'cross'
                        GROUP BY data.variable_id, cd.occurrence_id, data.data_unit, data.entity_id
                )occurrences 
                GROUP BY occurrence_id, data_unit 
            `
            // Retrieve datasets FROM the database
            occurrences = await sequelize.query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT
            })
        }

        if(retrieveOccurrencesOnly === true){
            res.send(200, {
                rows: {
                    occurrences: occurrences
                }
            })
            return
        }

        let datasetQuery = `
            SELECT 
                v.id::text as "variableDbId",
                v.abbrev,
                data.data_unit "dataUnit",
                data.is_generated "computed",
                COUNT(*),
                COUNT(*) FILTER (WHERE data.remarks <> '' OR data.remarks IS NOT NULL) "remarksCount",
                COUNT(*) FILTER (WHERE data.status = 'invalid' OR data.status = 'invalid;updated') "invalidCount",
                COUNT(*) FILTER (WHERE data.status = 'missing' OR data.status = 'missing;updated') "missingCount",
                COUNT(*) FILTER (WHERE data.status = 'new' AND data.is_void = FALSE AND data.is_suppressed = FALSE) "newCount",
                COUNT(*) FILTER (WHERE data.is_void = TRUE) "voidedCount",
                COUNT(*) FILTER (WHERE data.status = 'updated' OR data.status = 'invalid;updated' OR data.status = 'missing;updated') "updatedCount",
                COUNT(*) FILTER (WHERE data.status = 'new' OR data.status = 'updated' AND data.is_void = FALSE) "validCount",
                COUNT(*) FILTER (WHERE data.is_void = FALSE) "commitCount",
                COUNT(*) FILTER (WHERE data.status ='updated' AND data.is_void = FALSE) "operationalCount",
                COUNT(*) FILTER (WHERE data.is_suppressed = TRUE AND data.is_void = FALSE) "suppressedCount",
                (CASE             
                    WHEN data.entity='plot_data' THEN
                        (SELECT 
                        COUNT(*) 
                    FROM
                        data_terminal.transaction_dataset datax,
                        experiment.plot_data pm 
                    WHERE 
                        datax.transaction_id = datax.transaction_id 
                        AND pm.variable_id = data.variable_id 
                        AND pm.variable_id = datax.variable_id 
                        AND pm.is_void = FALSE 
                        AND pm.plot_id = datax.entity_id 
                        AND pm.data_qc_code = 'S' 
                        AND datax.is_void = FALSE
                        AND datax.data_unit = 'plot'
                    )
                WHEN data.entity='cross_data' THEN
                    (SELECT 
                        COUNT(*) 
                    FROM
                        data_terminal.transaction_dataset datax,
                        germplasm.cross_data cd 
                        LEFT JOIN germplasm.cross "cross"
                            ON "cross".id = cd.cross_id 
                        LEFT JOIN experiment.experiment AS expt
                            ON expt.id = "cross".experiment_id
                        LEFT JOIN experiment.occurrence AS occ
                            ON occ.experiment_id = expt.id
                    WHERE 
                        datax.transaction_id = datax.transaction_id 
                        AND cd.variable_id = datax.variable_id 
                        AND cd.is_void = FALSE 
                        AND cd.data_qc_code = 'S' 
                        AND datax.is_void = FALSE
                        AND datax.data_unit = 'cross'
                    )
                END
            ) "operationalSuppressedCount",
            MIN(
            CASE 
                WHEN v.data_type NOT IN ( 'character varying','date','time','text') 
                AND (data.value ~ '^\\d+$' OR data.value ~ '^(\\d*(.\\d+)?)$')  
                AND data.is_void = FALSE
                AND data.is_suppressed = FALSE
                THEN 
                    replace(data.value,',','')::float 
                ELSE NULL 
            END) minimum,
            MAX(
                CASE 
                    WHEN v.data_type NOT IN ( 'character varying','date','time','text') 
                    AND (data.value ~ '^\\d+$' or data.value ~ '^(\\d*(.\\d+)?)$')  
                    AND data.is_void = FALSE
                    AND data.is_suppressed = FALSE 
                    THEN 
                        replace(data.value,',','')::float 
                    ELSE NULL 
                END) maximum,
            CAST(
                VARIANCE(
                    CAST(
                    (CASE 
                        WHEN v.data_type NOT IN ( 'character varying','date','time','text') 
                        AND (data.value ~ '^\\d+$' or data.value ~ '^(\\d*(.\\d+)?)$') 
                        AND data.is_void = FALSE
                        AND data.is_suppressed = FALSE 
                        THEN 
                            replace(data.value,',','') 
                        ELSE NULL 
                        END
                    )
                    AS double precision
                    )
                ) AS DECIMAL (30,2)
            ) variance,
            CAST(
            AVG(
                CAST(
                (CASE 
                    WHEN v.data_type NOT IN ( 'character varying','date','time','text') 
                    AND (data.value ~ '^\\d+$' or data.value ~ '^(\\d*(.\\d+)?)$') 
                    AND data.is_void = FALSE
                    AND data.is_suppressed = FALSE 
                    THEN 
                        replace(data.value,',','') 
                    ELSE NULL 
                    END
                ) 
                AS double precision
                )
            ) AS DECIMAL (30,2)
            ) average
        FROM 
            data_terminal.transaction_dataset data
            LEFT JOIN master.variable v ON v.id=data.variable_id 
        WHERE
            transaction_id =${transactionDbId}
        GROUP BY v.id, data.is_generated, data.data_unit , data.entity ,data.variable_id        `

        // Retrieve datasets FROM the database
        let dataset = await sequelize.query(datasetQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        res.send(200, {
            rows: {
                variables: dataset,
                occurrences: occurrences
            }
        })
        return
    }
}