/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let validator = require('validator')

module.exports = {

    /**
     * Retrieve the dataset records given the transactionDbId
     * Endpoint for data_terminal.transaction_dataset
     * GET /v3/terminal-transactions/:id/datasets
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    get: async function (req, res) {

        let transactionDbId = req.params.id

        // check if transaction ID is integer
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transactionDbId must be an integer.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let datasetQuery = `
      SELECT
        dataset.id::text as "datasetDbId",
        dataset.transaction_id::text as "transactionDbId",
        dataset.variable_id::text as "variableDbId",
        dataset.value,
        dataset.status,
        dataset.is_suppressed as "isSuppressed",
        dataset.suppress_remarks as "suppressRemarks",
        dataset.is_generated as "isComputed",
        dataset.entity,
        dataset.entity_id::text as "entityDbId",
        dataset.data_unit as "dataUnit",
        dataset.remarks,
        dataset.notes,
        dataset.creation_timestamp as "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name as creator,
        dataset.modification_timestamp as "modificationTimestamp",
        modifier.id as "modifierDbId",
        modifier.person_name as modifier
      FROM
        data_terminal.transaction_dataset dataset
      LEFT JOIN
        tenant.person creator ON dataset.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON dataset.modifier_id = modifier.id
      WHERE
        dataset.transaction_id = (:transactionDbId)
    `
        // Retrieve datasets from the database
        let dataset = await sequelize.query(datasetQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                transactionDbId: transactionDbId
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (await dataset == undefined || await dataset.length < 1) {
            let errMsg = 'The dataset records you have requested do not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        res.send(200, {
            rows: dataset
        })
        return
    }
}