/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let responseHelper = require('../../../../helpers/responses')
let forwarded = require('forwarded-for')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let logger = require('../../../../helpers/logger')
const endpoint = 'terminal-transactions/:id/files'

module.exports = {
    /**
     * Retrieve the files of a transaction
     * GET v3/terminal-transactions/:id/files
     * @param {*} req 
     * @param {*} res 
     */
    get: async function (req, res) {
        // Retrieve the file ID
        let transactionDbId = req.params.id
        let sort = req.params.sort
        let orderString = ''
        let conditionString = ''
        let count

        // validates if transactionDbId is integer
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transactionDbId must be an integer.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode,
                errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionRecord[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let filesQuery = `
            SELECT
                file.id AS "transactionFileDbId",
                file.transaction_id AS "transactionDbId",
                file.status,
                file.remarks,
                file.notes,
                file.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                file.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                data_terminal.transaction_file file
            LEFT JOIN
                tenant.person creator ON file.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON file.modifier_id = modifier.id
            WHERE
                file.is_void = FALSE AND
                file.transaction_id = ${transactionDbId}
        `
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Retrieve files from the database
        let files = await sequelize.query(filesQuery + orderString, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return recordCount=0 if there are no file records
        if (await files === undefined || await files.length < 1) {
            count = 0
        } else {
            // Get count
            fileCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                filesQuery, conditionString, orderString)

            fileCount = await sequelize.query(fileCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = fileCount[0].count
        }

        res.send(200, {
            rows: files,
            count: count
        })
        return

    },
    /**
     * Create records in data_terminal.transaction_files
     * POST call for /v3/terminal-transactions/:id/files
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {

        let transactionDbId = req.params.id
        let loadDataset = req.params.loadDataset

        // Set defaults
        let resultArray = []
        let statusList = ['uploaded plot data', 'invalid plot data', 'committed plot data',
        'uploaded cross data', 'invalid cross data', 'committed cross data', 'suppressed']

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let transactionFileUrl = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/terminal-transactions/' + transactionDbId + '/files'

        // validates if transactionDbId is integer
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transactionDbId must be an integer.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode,
                errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let records = data.records
        if (records == undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let creatorId = await tokenHelper.getUserId(req)

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            for (let record of records) {
                let status = record.status
                let dataset = record.dataset
                let remarks = null

                // validate if mandatory fields exist : status, dataset
                if (
                    status === undefined ||
                    dataset === undefined
                ) {
                    let errMsg = 'Required parameters are missing. Ensure that '
                        + 'transactionDbId, status, and dataset fields are not empty.'
                    errorCode = await responseHelper.getErrorCodebyMessage(
                        400,
                        errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode,
                        errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // check if status is valid
                if (
                    status !== undefined
                    && (!status || status.trim().length === 0)
                ) {
                    let errMsg = 'The transaction file status is invalid. Status '
                        + 'is empty.'
                    errorCode = await responseHelper.getErrorCodebyMessage(
                        400,
                        errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode,
                        errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                } else if (status !== undefined
                    && statusList.indexOf(status) == -1) {
                    let errMsg = 'The transaction file status is invalid. Status '
                        + 'should be one of the following: ' + statusList.toString()
                    errorCode = await responseHelper.getErrorCodebyMessage(
                        400,
                        errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode,
                        errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate non-mandatory parameters : remarks
                if (record.remarks !== undefined) remarks = record.remarks
                if(loadDataset == 'true'){
                    let datasetQ = `
                    SELECT
                        dataset.variable_id::text as "variableDbId",
                        dataset.value,
                        dataset.entity,
                        dataset.entity_id::text as "entityDbId"
                    FROM
                        data_terminal.transaction_dataset dataset
                    WHERE
                        dataset.transaction_id = ${transactionDbId}
                    ORDER BY id
                `
                    dataset = await sequelize.query(datasetQ, {
                        type: sequelize.QueryTypes.SELECT
                    }).catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })
                }

                await sequelize.transaction(async transaction => {
                    // Create transaction record
                    dataset = JSON.stringify(dataset);
                    let insertQuery = `
                        INSERT INTO
                            data_terminal.transaction_file (
                            status, transaction_id, dataset, creator_id, remarks, is_void)
                        VALUES(
                            '${status}', ${transactionDbId}, '${dataset}', 
                            ${creatorId}, '${remarks}' , FALSE
                        )
                        RETURNING
                            id
                    `
                    let transactionFile = await sequelize.query(insertQuery, {
                        type: sequelize.QueryTypes.INSERT,
                        transaction: transaction,
                        raw: true
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'INSERT', err)
                        throw new Error(err)
                    })

                    let array = {
                        transactionFileDbId: transactionFile[0][0]['id'],
                        recordCount: 1,
                        href: transactionFileUrl + '/' + transactionFile[0][0]['id']
                    }
                    resultArray.push(array)
                })

                res.send(200, {
                    rows: resultArray,
                    count: resultArray.length
                })
                return
            }

        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}