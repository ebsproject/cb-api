/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let processQueryHelper = require('../../../../helpers/processQuery/index')
let searchHelper = require('../../../../helpers/queryTool/index.js')
let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let validator = require('validator')
const logger = require('../../../../helpers/logger')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}
        let excludedParametersArray = []
        const endpoint = 'terminal-transactions/:id/datasets-search'
        const operation = 'SELECT'

        let transactionDbId = req.params.id

        // check if transactionDbId is valid
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transactionDbId must be an integer'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionRecord[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {

            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            excludedParametersArray = ["fields"]

            let bodyParameters = req.body
            let variableDbId
            let condition = ``
            for (column in bodyParameters) {
                columnCast = false
                if (excludedParametersArray.includes(column))
                    continue

                let filters = bodyParameters[column]
                if (column == 'variableDbId') {
                    variableDbId = filters
                }
                condition = ``
                // Cast the value to the variable's data type to allow filtering with  operators
                if (column == 'value' && variableDbId != null) {
                    let variableQuery = `
                        SELECT 
                            data_type
                        FROM
                            master.variable v
                        WHERE
                            v.id='${variableDbId}'
                            AND is_void = FALSE
                    `
                    let variableList = await sequelize.query(variableQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    let variableDataType = variableList[0]['data_type']
                    column = column + "::" + variableDataType
                    columnCast = true // allows casting of data type

                    if (variableDataType == 'date' && typeof filters === 'object') {

                        for (operator in filters) {
                            var finalOperator = '';
                            if (operator == 'less than') { finalOperator = `<` }
                            else if (operator == 'less than or equal to') { finalOperator = `<=` }
                            else if (operator == 'greater than') { finalOperator = `>` }
                            else if (operator == 'equals') { finalOperator = `=` }
                            else if (operator == 'not equals') { finalOperator = `<>` }
                            else { finalOperator = `>=` }
                            if (condition !== '') {
                                condition = condition + ' AND ' + column + finalOperator + `'` + filters[operator] + `'`
                            } else {
                                condition = column + finalOperator + `'` + filters[operator] + `'`
                            }

                        }
                    } else {
                        condition = searchHelper.getFilterCondition(filters, column, columnCast)
                    }
                } else {
                    condition = searchHelper.getFilterCondition(filters, column, columnCast)
                }

                if (condition !== '') {
                    condition = `(` + condition + ')'
                    if (conditionString == '') {
                        conditionString = conditionString +
                            ` WHERE `
                            + condition
                    } else {
                        conditionString = conditionString +
                            ` AND `
                            + condition
                    }
                }
            }

        }

        // build the base query
        let datasetsQuery = ''
        let fromQuery = `
            FROM
                data_terminal.transaction_dataset dataset
                JOIN
                    tenant.person creator ON dataset.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON dataset.modifier_id = modifier.id
            WHERE
                dataset.transaction_id=${transactionDbId}
                AND dataset.is_void = FALSE
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            datasetsQuery = knex.column(parameters['fields'].split('|'))
            datasetsQuery += fromQuery
        } else {
            datasetsQuery = `
                SELECT
                    dataset.id::text AS "datasetDbId",
                    dataset.transaction_id::text AS "transactionDbId",
                    dataset.variable_id::text AS "variableDbId",
                    dataset.value,
                    dataset.status,
                    dataset.is_suppressed AS "isSuppressed",
                    dataset.suppress_remarks AS "suppressRemarks",
                    dataset.is_generated AS "isComputed",
                    dataset.entity,
                    dataset.entity_id::text AS "entityDbId",
                    dataset.data_unit AS "dataUnit",
                    dataset.remarks,
                    dataset.notes,
                    dataset.is_void AS "isVoid",
                    dataset.collection_timestamp AS "collectionTimestamp",
                    dataset.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    dataset.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier
                    ${fromQuery}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let datasetsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            datasetsQuery,
            conditionString,
            orderString
        )

        let finalDataCountQuery = await processQueryHelper
        .getFinalTotalCountQuery(datasetsFinalSqlQuery, orderString)

        // Retrieve the datasets records
        let datasets = await sequelize
            .query(finalDataCountQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await datasets == undefined || await datasets.length < 1) {
            datasets = []
            count = 0
        } else {
            count = datasets[0].totalCount
        }

        res.send(200, {
            rows: datasets,
            count: count
        })
        return
    }
}