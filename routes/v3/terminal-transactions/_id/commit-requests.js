/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let seedlotHelper = require('../../../../helpers/seedlot/validation')
let occurrenceHelper = require('../../../../helpers/occurrence/index.js')
let fileHelper = require('../../../../helpers/terminalTransaction/file')
let responseHelper = require('../../../../helpers/responses')
let forwarded = require('forwarded-for')
let validator = require('validator')
let logger = require('../../../../helpers/logger')
const endpoint = 'terminal-transactions/:id/commit-requests'

module.exports = {

    /**
     * Commit transaction dataset records
     * POST /v3/terminal-transactions/:id/commit-requests
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {

        let transactionDbId = req.params.id
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let insertCount = 0
        let updateCount = 0
        let voidCount = 0
        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let terminalTransactionUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/terminal-transactions/'
            + transactionDbId

        // check if transactionDbId is integer
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transaction ID must be an integer.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT 
                status
            FROM 
                data_terminal.transaction
            WHERE 
                id = ${transactionDbId}
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (await transactionRecord == undefined || await transactionRecord.length == 0) {
            let errMsg = 'The transaction ID you have requested does not exist.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if transaction ID is not committed

        if (transactionRecord[0]['status'] == 'committed') {
            let errMsg = 'The transaction is already committed.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID is not committing in progress.

        if (transactionRecord[0]['status'] == 'committed') {
            let errMsg = 'The transaction is now committing in progress.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction dataset records are validated
        transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction_dataset
                WHERE 
                    transaction_id = ${transactionDbId} 
                    AND status IS NULL
                    AND is_void = FALSE
            )                
        `
        transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (transactionRecord[0]['exists']) {
            let errMsg = 'There is at least 1 transaction dataset record that is not yet validated.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        transactionQuery = `
            SELECT 
                action
            FROM 
                data_terminal.transaction
            WHERE 
                id = ${transactionDbId}
                AND is_void = FALSE
        `
        transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        try {
            //insert or update plot data records
            let entityQuery = `
                SELECT 
                    t.entity 
                FROM 
                    data_terminal.transaction_dataset t 
                WHERE 
                    t.transaction_id = ${transactionDbId}
                GROUP BY t.entity
            `
            let entityResult = await sequelize.query(entityQuery, {
                type: sequelize.QueryTypes.SELECT
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            let resultArray
            transaction = await sequelize.transaction(async transaction => {
                for (let entity of entityResult) {

                    if (entity['entity'] == 'plot_data') {

                        let deleteQuery = `
                            UPDATE 
                                experiment.plot_data  
                            SET 
                                is_void= TRUE,
                                transaction_id = ${transactionDbId},
                                modifier_id= ${userId},
                                modification_timestamp = NOW()
                            FROM 
                            (
                                SELECT
                                    entity_id,
                                    variable_id
                                FROM
                                    data_terminal.transaction_dataset 
                                WHERE 
                                    entity = 'plot_data'
                                    AND transaction_id = ${transactionDbId}
                                    AND is_void=TRUE
                            )dataset
                            WHERE
                                dataset.entity_id = plot_data.plot_id
                                AND dataset.variable_id = plot_data.variable_id
                                AND plot_data.is_void = FALSE
                        `
                        let plotDataVoided = await sequelize.query(deleteQuery, {
                            type: sequelize.QueryTypes.UPDATE,
                            transaction: transaction,
                            raw: true
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'UPDATE', err)
                            throw new Error(err)
                        })
                        voidCount += plotDataVoided[1]

                        let updateQuery = `
                            UPDATE 
                                experiment.plot_data  
                            SET 
                                data_value = dataset.value,
                                data_qc_code = dataset.status,
                                transaction_id = ${transactionDbId},
                                modifier_id= ${userId},
                                modification_timestamp = 'NOW()',
                                collection_timestamp = dataset.collection_timestamp
                            FROM 
                            (
                                SELECT
                                    entity_id,
                                    variable_id,
                                    (CASE
                                        WHEN status LIKE 'missing%' THEN null
                                        ELSE value
                                    END),
                                    (CASE 
                                        WHEN status LIKE 'missing%' THEN 'M'
                                        WHEN status IN ('new','updated') AND is_suppressed=FALSE THEN 'Q'
                                        WHEN is_suppressed = TRUE THEN 'S'
                                        WHEN status LIKE 'invalid%' THEN 'B'
                                    END) status,
                                    collection_timestamp
                                FROM
                                    data_terminal.transaction_dataset 
                                WHERE 
                                    entity = 'plot_data'
                                    AND transaction_id = ${transactionDbId}
                                    AND is_void = FALSE
                            )dataset
                            WHERE
                                dataset.entity_id = plot_data.plot_id
                                AND dataset.variable_id = plot_data.variable_id
                                AND plot_data.is_void = FALSE
                                AND (
                                    dataset.value <> plot_data.data_value
                                    OR dataset.status <> plot_data.data_qc_code
                                )
                        `
                        let plotDataUpdated = await sequelize.query(updateQuery, {
                            type: sequelize.QueryTypes.UPDATE,
                            transaction: transaction,
                            raw: true
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'UPDATE', err)
                            throw new Error(err)
                        })
                        updateCount += plotDataUpdated[1]

                        let insertQuery = `
                            INSERT INTO experiment.plot_data 
                                (plot_id, variable_id, data_value, data_qc_code, transaction_id, 
                                    collection_timestamp, creator_id, creation_timestamp)
                            SELECT
                                entity_id,
                                variable_id,
                                (CASE
                                    WHEN status LIKE 'missing%' THEN null
                                    ELSE value
                                END),
                                (CASE 
                                    WHEN status LIKE 'missing%' THEN 'M'
                                    WHEN status IN ('new','updated') AND is_suppressed=FALSE THEN 'Q'
                                    WHEN is_suppressed = TRUE THEN 'S'
                                    WHEN status LIKE 'invalid%' THEN 'B'
                                END),
                                transaction_id,
                                collection_timestamp,
                                ${userId},
                                'NOW()'
                            FROM 
                                data_terminal.transaction_dataset dataset
                            WHERE 
                                dataset.entity = 'plot_data' AND
                                dataset.transaction_id = ${transactionDbId}
                                AND dataset.is_void=FALSE
                                AND NOT EXISTS(
                                    SELECT 1 
                                    FROM 
                                    experiment.plot_data
                                    WHERE
                                    dataset.entity_id = plot_data.plot_id
                                    AND dataset.variable_id = plot_data.variable_id
                                    AND plot_data.is_void = FALSE
                                    AND plot_data.data_value = dataset.value
                                )
                        `
                        let plotDataCreated = await sequelize.query(insertQuery, {
                            type: sequelize.QueryTypes.INSERT,
                            transaction: transaction,
                            raw: true
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'INSERT', err)
                            throw new Error(err)
                        })
                        insertCount += plotDataCreated[1]
                    } else if (entity['entity'] == 'cross_data') {

                        let deleteQuery = `
                            UPDATE 
                                germplasm.cross_data  
                            SET 
                                is_void= TRUE,
                                transaction_id = ${transactionDbId},
                                modifier_id= ${userId},
                                modification_timestamp = NOW()
                            FROM 
                            (
                                SELECT
                                    entity_id,
                                    variable_id
                                FROM
                                    data_terminal.transaction_dataset 
                                WHERE 
                                    entity = 'cross_data'
                                    AND transaction_id = ${transactionDbId}
                                    AND is_void=TRUE
                            )dataset
                            WHERE
                                dataset.entity_id = cross_data.cross_id
                                AND dataset.variable_id = cross_data.variable_id
                                AND cross_data.is_void = FALSE
                        `
                        let crossDataVoided = await sequelize.query(deleteQuery, {
                            type: sequelize.QueryTypes.UPDATE,
                            transaction: transaction,
                            raw: true
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'UPDATE', err)
                            throw new Error(err)
                        })
                        voidCount += crossDataVoided[1]

                        let updateQuery = `
                            UPDATE 
                                germplasm.cross_data  
                            SET 
                                data_value = dataset.value,
                                data_qc_code = dataset.status,
                                transaction_id = ${transactionDbId},
                                modifier_id= ${userId},
                                modification_timestamp = 'NOW()',
                                collection_timestamp = dataset.collection_timestamp
                            FROM 
                            (
                                SELECT
                                    entity_id,
                                    variable_id,
                                    value,
                                    (CASE 
                                        WHEN status IN ('new','updated') AND is_suppressed=FALSE THEN 'Q'
                                        WHEN is_suppressed = TRUE THEN 'S'
                                    END) status,
                                    collection_timestamp
                                FROM
                                    data_terminal.transaction_dataset 
                                WHERE 
                                    entity = 'cross_data'
                                    AND transaction_id = ${transactionDbId}
                                    AND is_void = FALSE
                                    AND status <> 'invalid'
                                
                            )dataset
                            WHERE
                                dataset.entity_id = cross_data.cross_id
                                AND dataset.variable_id = cross_data.variable_id
                                AND cross_data.is_void = FALSE
                                AND (
                                    dataset.value <> cross_data.data_value
                                    OR dataset.status <> cross_data.data_qc_code
                                )
                        `
                        let crossDataUpdated = await sequelize.query(updateQuery, {
                            type: sequelize.QueryTypes.UPDATE,
                            transaction: transaction,
                            raw: true
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'UPDATE', err)
                            throw new Error(err)
                        })
                        updateCount += crossDataUpdated[1]

                        let insertQuery = `
                            INSERT INTO germplasm.cross_data 
                                (cross_id, variable_id, data_value, data_qc_code, transaction_id, 
                                    collection_timestamp, creator_id, creation_timestamp)
                            SELECT
                                entity_id,
                                variable_id,
                                (CASE
                                    WHEN status LIKE 'missing%' THEN null
                                    ELSE value
                                END),
                                (CASE 
                                    WHEN status LIKE 'missing%' THEN 'M'
                                    WHEN status IN ('new','updated') AND is_suppressed=FALSE THEN 'Q'
                                    WHEN is_suppressed = TRUE THEN 'S'
                                    WHEN status LIKE 'invalid%' THEN 'B'
                                END),
                                transaction_id,
                                collection_timestamp,
                                ${userId},
                                'NOW()'
                            FROM 
                                data_terminal.transaction_dataset dataset
                            WHERE 
                                dataset.transaction_id = ${transactionDbId}
                                AND dataset.is_void = FALSE
                                AND dataset.status <> 'invalid'
                                AND dataset.entity = 'cross_data' 
                                AND NOT EXISTS(
                                    SELECT 1 
                                    FROM 
                                        germplasm.cross_data
                                    WHERE
                                        dataset.entity_id = cross_data.cross_id
                                        AND dataset.variable_id = cross_data.variable_id
                                        AND cross_data.is_void = FALSE
                                        AND cross_data.data_value = dataset.value
                                )
                        `
                        let crossDataCreated = await sequelize.query(insertQuery, {
                            type: sequelize.QueryTypes.INSERT,
                            transaction: transaction,
                            raw: true
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'INSERT', err)
                            throw new Error(err)
                        })
                        insertCount += crossDataCreated[1]
                    }

                }

                // Create transaction file record for committed dataset for plots
                let committedQuery = `
                    AND status IN ('updated', 'new')
                    AND data_unit = 'plot'
                    AND dataset.is_void = FALSE    
                `
                let plotIdList = []
                let committedPlotDataset = await fileHelper.getDataset(transactionDbId, committedQuery)

                if (committedPlotDataset.length > 0) {
                    // Extract plot ids
                    committedPlotDataset.map(function(key) {
                        plotIdList.push(key['entityDbId']);
                    })

                    committedPlotDataset = JSON.stringify(committedPlotDataset)

                    committedPlotTransactionFile = await fileHelper.create(transactionDbId, 'committed plot data', committedPlotDataset, userId, transaction)
                }

                // Create transaction file record for committed dataset for crosses
                committedQuery = `
                    AND status IN ('updated', 'new')
                    AND data_unit = 'cross'
                    AND dataset.is_void = FALSE    
                `
                let crossIdList = []
                let committedCrossDataset = await fileHelper.getDataset(transactionDbId, committedQuery)

                if (committedCrossDataset.length > 0) {

                    committedCrossDataset.map(function(key) {
                        crossIdList.push(key['entityDbId']);
                    })

                    committedCrossDataset = JSON.stringify(committedCrossDataset)

                    committedCrossTransactionFile = await fileHelper.create(transactionDbId, 'committed cross data', committedCrossDataset, userId, transaction)

                    let formattedCrossIds = crossIdList.join(',')
                    await seedlotHelper.updateCrossHarvestStatus(req, res, formattedCrossIds, transaction)
                }

                // Create transaction file record for voided plot dataset
                voidedPlotQuery = `
                    AND dataset.is_void = TRUE  
                    AND data_unit = 'plot'
                `
                let voidedPlotDataset = await fileHelper.getDataset(transactionDbId, voidedPlotQuery)

                if (voidedPlotDataset.length > 0) {
                    // Extract plot ids
                    voidedPlotDataset.map(function(key) {
                        plotIdList.push(key['entityDbId']);
                    })

                    voidedPlotDataset = JSON.stringify(voidedPlotDataset)

                    await fileHelper.create(transactionDbId, 'voided plot data', voidedPlotDataset, userId, transaction)
                }

                // Create transaction file record for voided cross dataset
                voidedCrossQuery = `
                    AND dataset.is_void = TRUE  
                    AND data_unit = 'cross'
                `
                let voidedCrossDataset = await fileHelper.getDataset(transactionDbId, voidedCrossQuery)

                if (voidedCrossDataset.length > 0) {

                    voidedCrossDataset = JSON.stringify(voidedCrossDataset)

                    await fileHelper.create(transactionDbId, 'voided cross data', voidedCrossDataset, userId, transaction)
                }

                // Create transaction file record for invalid plot dataset
                invalidPlotQuery = `
                    AND ( dataset.status = 'invalid' OR dataset.status = 'invalid;updated' )
                    AND dataset.is_void = FALSE  
                    AND data_unit = 'plot'  
                `
                let invalidPlotDataset = await fileHelper.getDataset(transactionDbId, invalidPlotQuery)

                if (invalidPlotDataset.length > 0) {
                    // Extract plot ids
                    invalidPlotDataset.map(function(key) {
                        plotIdList.push(key['entityDbId']);
                    })

                    invalidPlotDataset = JSON.stringify(invalidPlotDataset)

                    await fileHelper.create(transactionDbId, 'invalid plot data', invalidPlotDataset, userId, transaction)
                }

                // Create transaction file record for invalid cross dataset
                invalidCrossQuery = `
                    AND dataset.status = 'invalid'
                    AND dataset.is_void = FALSE  
                    AND data_unit = 'cross'  
                `
                let invalidCrossDataset = await fileHelper.getDataset(transactionDbId, invalidCrossQuery)

                if (invalidCrossDataset.length > 0) {

                    invalidCrossDataset = JSON.stringify(invalidCrossDataset)

                    await fileHelper.create(transactionDbId, 'invalid cross data', invalidCrossDataset, userId, transaction)
                }

                // Update plot harvest status and occurrence status
                if (plotIdList.length > 0) {
                    let formattedPlotIds = plotIdList.join(',')
                    await seedlotHelper.updatePlotAdvancingHarvestStatus(req, res, formattedPlotIds, transaction)
                    await occurrenceHelper.updateOccurrenceStatus(formattedPlotIds)
                }

                let deleteDatasetQuery = `
                    DELETE
                    FROM
                    data_terminal.transaction_dataset 
                    where transaction_id = ${transactionDbId}
                `
                await sequelize.query(deleteDatasetQuery, {
                    type: sequelize.QueryTypes.DELETE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })

                resultArray = {
                    transactionDbId: transactionDbId,
                    insertCount: insertCount,
                    updateCount: updateCount,
                    voidCount: voidCount,
                    href: terminalTransactionUrl
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}