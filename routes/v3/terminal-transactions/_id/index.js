/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let responseHelper = require('../../../../helpers/responses')
let forwarded = require('forwarded-for')
let validator = require('validator')
let logger = require('../../../../helpers/logger')
const endpoint = 'terminal-transactions/:id'

module.exports = {

    /**
     * Update records in data_terminal.transaction
     * PUT /v3/terminal-transactions/:id
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    put: async function (req, res) {

        let transactionDbId = req.params.id
        let newRecord = req.body
        let transaction
        let setQuery = []
        // Set defaults
        let statusList = [
            'in queue',
            'error in background process',
            'to be committed',
            'committing in progress',
            'uploading in progress',
            'removing data in progress',
            'undo removing data in progress',
            'to be suppressed',
            'suppression in progress',
            'to be unsuppressed',
            'undo suppression in progress',
            'calculation in progress',
            'trait calculation failed',
            'uploaded',
            'uploaded: to be calculated',
            'committed',
        ]

        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let terminalTransactionUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/terminal-transactions/'
            + transactionDbId

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT 
                status,
                location_id
            FROM 
                data_terminal.transaction 
            WHERE 
                id = ${transactionDbId}
                AND is_void = FALSE
        `
        
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (transactionRecord.length == 0) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID is not committed
        transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                data_terminal.transaction 
                WHERE 
                id = ${transactionDbId} 
                AND status = 'committed'
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (transactionResult[0]['exists']) {
            let errMsg = 'The transaction status cannot be updated when it is ' +
                'already committed.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        let status = transactionRecord[0]['status'];
        let locationDbId = transactionRecord[0]['location_id'];

        // check if status is valid
        if (
            newRecord.status !== undefined
            && (!newRecord.status || newRecord.status.trim().length === 0)
        ) {
            let errMsg = 'The transaction status is invalid. Status is empty.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else if (
            newRecord.status !== undefined
            && statusList.indexOf(newRecord.status) == -1
        ) {
            let errMsg = 'The transaction status is invalid. Status should be one '
                + 'of the following: ' + statusList.toString()
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if ((!status.includes('uploaded') && status !== 'committing in progress' && status !== 'to be committed')
            && newRecord.status !== undefined
            && (
                newRecord.status == 'committed'
                || newRecord.status == 'committing in progress')
        ) {
            let errMsg = 'The transaction cannot be committed. The value of the '
                + 'current status should be uploaded or committing in progress to '
                + 'be able to update the status into committed.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        //check if checksum is valid
        if (newRecord.checksum !== undefined
            && (!newRecord.checksum || newRecord.checksum.trim().length === 0)) {
            let errMsg = 'The transaction checksum is invalid. Checksum is empty.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if occurrence is valid
        if (newRecord.occurrences !== undefined
            && !Array.isArray(newRecord.occurrences)
        ) {
            let errMsg = 'Invalid format, occurrences must be an array.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else if (newRecord.occurrences !== undefined) {
            // check if records in occurrences are valid
            let errMsg = ''
            let occurrenceCount = 0
            // loop occurrences
            for (let occurrence of newRecord.occurrences) {
                occurrenceCount++;
                if (occurrence.occurrenceDbId == undefined
                    || occurrence.dataUnit == undefined
                    || occurrence.dataUnitCount == undefined
                    || occurrence.variableDbIds == undefined
                ) {
                    errMsg = 'There is at least one(1) invalid record in '
                        + 'occurrences. Ensure that the fields; occurrenceDbId, '
                        + 'dataUnitCount, dataUnit, and variableDbIds are not empty.'

                } else {

                    // check if variableDbIds is valid
                    if (!Array.isArray(occurrence.variableDbIds)) {
                        errMsg = 'There is at least one(1) invalid record in '
                            + 'occurrences. Ensure that the variableDbIds is an array.'
                    } else if (Array.isArray(occurrence.variableDbIds)) {

                        // check if variable exists in variableDbIds
                        let variableCount = 0
                        for (let variable of occurrence.variableDbIds) {
                            variableCount++

                            // check if variableDbId value is integer
                            if (!validator.isInt(variable)) {
                                errMsg = 'There is at least one(1) invalid record in '
                                    + 'occurrences. Ensure that the variableDbIds is not empty.'
                                break;
                            }

                            // check if variableDbId exists in database
                            let variableQuery = `
                                SELECT exists(
                                SELECT 1
                                FROM 
                                    master.variable
                                WHERE 
                                    id=${variable}
                                    AND is_void = FALSE
                                )
                            `
                            let variableResult = await sequelize.query(variableQuery, {
                                type: sequelize.QueryTypes.SELECT
                            })
                            if (!variableResult[0]['exists']) {
                                errMsg = 'There is at least one(1) invalid record in '
                                    + 'occurrences. Ensure that the variableDbIds exist in '
                                    + 'database and must be of type metadata or observation.'
                                break;
                            }
                        }
                        if (variableCount == 0) {
                            errMsg = 'There is at least one(1) invalid record in '
                                + 'occurrences. Ensure that the variableDbIds is not empty.'
                        }
                    }

                    // check if occurrenceDbId is valid
                    if (!validator.isInt(occurrence.occurrenceDbId)) {
                        errMsg = 'There is at least one(1) invalid record in '
                            + 'occurrences. The field occurrenceDbId must be an integer.'
                    }

                    // check if dataUnitCount is valid
                    if (!validator.isInt(occurrence.dataUnitCount)) {
                        errMsg = 'There is at least one(1) invalid record in '
                            + 'occurrences. The field dataUnitCount must be an integer.'
                    }

                    // check if dataUnit is valid, API 21.04 accepted value is plot or cross only
                    if (occurrence.dataUnit !== 'plot' && occurrence.dataUnit !== 'cross') {
                        errMsg = 'There is at least one(1) invalid record in '
                            + 'occurrences. The dataUnit should be one of the following: '
                            + 'plot, cross'
                    }
                }
                if (errMsg !== '') {

                    errorCode = await responseHelper.getErrorCodebyMessage(
                        400,
                        errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode,
                        errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

            }// end loop occurrences
            if (occurrenceCount == 0) {

                let errMsg = 'Ensure that occurrences is not empty.'
                errorCode = await responseHelper.getErrorCodebyMessage(
                    400,
                    errMsg)
                errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        if (newRecord.remarks !== undefined) {
            setQuery.push(`remarks = '${newRecord.remarks}'`)
        }
        if (newRecord.status !== undefined) {
            setQuery.push(`status = '${newRecord.status}'`)
            if (newRecord.status == 'committed') {
                setQuery.push(`committer_id = ${userDbId}`)
                setQuery.push(`committed_timestamp = 'NOW()'`)
            }
        }
        if (newRecord.occurrences !== undefined) {
            newRecord.occurrences = JSON.stringify(newRecord.occurrences)
            setQuery.push(`occurrence = $$${newRecord.occurrences}$$`)
        }
        if (newRecord.checksum !== undefined) {
            setQuery.push(`checksum = $$${newRecord.checksum}$$`)
        }
        try {
            let resultArray
            transaction = await sequelize.transaction(async transaction => {
                setQuery = setQuery.toString()
                let updateListQuery = `
                    UPDATE
                        data_terminal.transaction
                    SET
                        ${setQuery},
                        modification_timestamp = NOW(),
                        modifier_id = ${userDbId}
                    WHERE
                        id = ${transactionDbId}
                `
                updateListQuery = updateListQuery.trim()
    
                await sequelize.query(updateListQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
    
                resultArray = {
                    transactionDbId: transactionDbId,
                    recordCount: 1,
                    href: terminalTransactionUrl
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },
    /**
     * Delete the transaction record
     * DELETE v3/terminal-transactions/:id
     * @param {*} req 
     * @param {*} res 
     */
    delete: async function (req, res) {
        let transactionDbId = req.params.id
        let transaction

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let terminalTransactionUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/terminal-transactions/'
            + transactionDbId

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionRecord[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        try {
            let resultArray
            transaction = await sequelize.transaction(async transaction => {
                let deleteQuery = `
                    DELETE FROM
                        data_terminal.transaction
                    WHERE
                        id = ${transactionDbId}
                `
                await sequelize.query(deleteQuery, {
                    type: sequelize.QueryTypes.DELETE,
                    transaction: transaction,
                    raw: true,
                })

                resultArray = {
                    transactionDbId: transactionDbId,
                    recordCount: 1,
                    href: terminalTransactionUrl
                }
            }).catch(async err => {
                logger.logFailingQuery(endpoint, 'DELETE', err)
                throw new Error(err)
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}