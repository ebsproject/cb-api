/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let responseHelper = require('../../../../helpers/responses')
let logger = require('../../../../helpers/logger/index.js')
const endpoint = 'terminal-transactions/:id/variable-computations'

module.exports = {

    /**
     * Create records in data_terminal.transaction_dataset
     * POST call for /v3/terminal-transactions/:id/variable-computations
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {

        let transactionDbId = req.params.id
        let resultArray = []
        let entityDbIdList = ''
        let datasetEntityDbIdList = ''

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let resultDbId = []

        if (data.variableDbId === undefined) {
            let errMsg = 'Required parameter is missing. Ensure that variableDbId is not empty.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let variableDbId = data.variableDbId
        // validate if measurementVariables is an array
        if (!Array.isArray(variableDbId)) {
            let errMsg = 'Invalid format, variableDbId must be an array.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // validate if entityDbId is an array
        if (data.entityDbId !== undefined && !Array.isArray(data.entityDbId)) {
            let errMsg = 'Invalid format, entityDbId must be an array of integers.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else if (data.entityDbId !== undefined) {
            entityDbIdList = ' AND t.entity_id IN (' + data.entityDbId.toString() + ')'
            datasetEntityDbIdList = ' AND ds.entity_id IN (' + data.entityDbId.toString() + ')'
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if transaction ID is not committed
        transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId} 
                    AND status <> 'committed'
                    AND is_void = FALSE
            )
        `
        transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The dataset cannot be created. Ensure that the status of ' +
                'the terminal transaction ID is uploaded or uploading in progress'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let creatorId = await tokenHelper.getUserId(req)

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {

            let formulaDbIdList = data.formulaDbId ?? []

            if (formulaDbIdList){

                // validate if formulaDbId is an array
                if (!Array.isArray(formulaDbIdList)) {
                    let errMsg = 'Invalid format, formulaDbId must be an array.'
                    let errorCode = await responseHelper.getErrorCodebyMessage(
                        400,
                        errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
    
            }

            //compute formula for each variable
            for (let formula of formulaDbIdList) {
                let paramVarIdArr = []  // used to find variables ids in target tables
                let paramVarAbbrevArr = []  // used in crosstab columns
                let functionCallParamsStr = []
                let parameters

                // Check if formula exists
                let variableQuery = `
                    SELECT 
                        f.data_level,
                        split_part(f.function_name, '(', 1) AS function_name,
                        v.abbrev,
                        v.data_type,
                        f.result_variable_id
                    FROM 
                        master.formula f
                    LEFT JOIN master.variable v 
                        ON v.id = f.result_variable_id
                    WHERE 
                        f.id = ${formula}
                        AND v.is_void = FALSE
                        AND f.is_void = FALSE
                `

                let variableResult = await sequelize.query(variableQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (!variableResult || variableResult.length === 0) {
                    resultArray.push({
                        resultVariableId: 0,
                        formulaDbId: formula,
                        recordCount: 0
                    })
                    continue
                }

                let variable = variableResult[0] || {}
                let functionName = variable['function_name'] ?? ''
                let resultsDataLevel = variable['data_level'] ?? ''
                let resultDataType = variable['data_type'] ?? ''
                let resultVariableAbbrev = variable['abbrev'] ?? ''
                let resultVariableId = variable['result_variable_id'] ?? ''
                resultDbId.push(resultVariableId)
                let updateCount = 0
                let insertCount = 0

                /*Get formula parameters*/
                const parameterSql = `
                    SELECT 
                        v.id,
                        v.abbrev,
                        v.data_type
                    FROM 
                        master.formula_parameter fp
                            LEFT JOIN master.variable v ON v.id = fp.param_variable_id
                    WHERE 
                        formula_id = ${formula}
                        AND fp.is_void = FALSE
                        AND v.is_void = FALSE
                    ORDER BY
                        order_number
                `
                parameters = await sequelize.query(parameterSql, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                // API 20.04 only computes plot variables
                if (resultsDataLevel == 'plot') {

                    // get all parameters of the formula
                    for (let param of parameters) {
                        let parameterVariableId = param['id']
                        let parameterVariableAbbrev = param['abbrev']
                        let parameterVariableDataType = param['data_type']

                        paramVarIdArr.push(parameterVariableId)

                        paramVarAbbrevArr.push('param_'
                            + parameterVariableAbbrev.toLowerCase() + ' ' + parameterVariableDataType)

                        functionCallParamsStr.push(parameterVariableAbbrev.toLowerCase())

                    }// end loop get parameters of formula

                    paramVarIdArr = paramVarIdArr.toString();
                    paramVarAbbrevArr = paramVarAbbrevArr.toString();

                    const functionCall = functionName
                        + '(param_' + functionCallParamsStr.join(', param_') + ')'

                    const crosstabQuery = `
                        SELECT 
                            id as plot_id,
                            ${functionCall}::${resultDataType} ${resultVariableAbbrev}
                        FROM CROSSTAB(
                            $$SELECT
                                id,
                                variable_id,
                                value
                            FROM (
                                -- 1️⃣ transaction_dataset (Priority 1)
                                SELECT
                                    t.entity_id AS id,
                                    t.variable_id,
                                    t.value
                                FROM
                                    data_terminal.transaction_dataset t
                                WHERE
                                    t.transaction_id = ${transactionDbId}
                                    AND t.status IN ('new', 'updated')
                                    AND t.variable_id IN (${paramVarIdArr})
                                    AND t.is_suppressed = FALSE
                                    AND t.is_void = FALSE
                                    AND t.data_unit = 'plot'

                                UNION ALL

                                -- 2️⃣ plot_data with NOT IN filter (Priority 2, fallback)
                                SELECT DISTINCT
                                    op.plot_id AS id,
                                    op.variable_id,
                                    op.data_value AS value
                                FROM
                                    experiment.plot_data op
                                JOIN data_terminal.transaction_dataset t
                                    ON t.entity_id = op.plot_id
                                    AND t.transaction_id = ${transactionDbId}
                                    AND t.data_unit = 'plot'
                                    ${entityDbIdList}
                                WHERE
                                    op.variable_id IN (${paramVarIdArr})
                                    AND op.is_void = FALSE
                                    AND op.data_qc_code IN ('G', 'Q')
                                    AND op.variable_id NOT IN (
                                        SELECT td.variable_id
                                        FROM data_terminal.transaction_dataset td
                                        WHERE td.transaction_id =  ${transactionDbId}
                                            AND td.variable_id IN (${paramVarIdArr})
                                            AND td.is_void = FALSE
                                            AND td.entity_id = op.plot_id
                                            AND td.data_unit = 'plot'
                                            AND td.status IN ('new', 'updated')
                                    )


                                UNION ALL

                                -- 3️⃣ occurrence_data from transaction_dataset (Priority 3, fallback)
                                SELECT DISTINCT
                                    t.entity_id AS id,
                                    od.variable_id,
                                    od.data_value AS value
                                FROM
                                    data_terminal.transaction_dataset t
                                JOIN experiment.plot p ON t.entity_id = p.id
                                JOIN experiment.occurrence_data od ON p.occurrence_id = od.occurrence_id
                                LEFT JOIN data_terminal.transaction_dataset td
                                    ON td.entity_id = t.entity_id
                                    AND td.transaction_id = ${transactionDbId}
                                    AND td.variable_id = od.variable_id
                                    AND td.status IN ('new', 'updated')
                                    AND td.is_void = FALSE
                                    AND td.is_suppressed = FALSE
                                    AND td.data_unit = 'plot'
                                LEFT JOIN experiment.plot_data op
                                    ON op.plot_id = t.entity_id
                                    AND op.variable_id = od.variable_id
                                    AND op.data_qc_code IN ('G', 'Q')
                                    AND op.is_void = FALSE
                                WHERE
                                    t.transaction_id = ${transactionDbId}
                                    AND t.status IN ('new', 'updated')
                                    AND t.variable_id IN (${paramVarIdArr})
                                    AND t.is_suppressed = FALSE
                                    AND t.is_void = FALSE
                                    AND t.data_unit = 'plot'
                                    AND od.variable_id IN (${paramVarIdArr})
                                    AND od.is_void = FALSE
                                    AND od.data_qc_code IN ('G', 'Q')
                                    AND (
                                        td.entity_id IS NULL -- Exclude if already exists in transaction_dataset
                                        AND op.plot_id IS NULL -- Exclude if already exists in plot_data
                                    )

                                UNION ALL

                                -- 4️⃣ occurrence_data from plot_data (Fallback for last source)
                                SELECT DISTINCT
                                    p.id AS id,
                                    od.variable_id,
                                    od.data_value AS value
                                FROM
                                    experiment.plot_data pd
                                JOIN experiment.plot p ON p.id = pd.plot_id
                                JOIN experiment.occurrence_data od 
                                    ON od.occurrence_id = p.occurrence_id
                                    AND od.variable_id IN (${paramVarIdArr})
                                    AND od.is_void = FALSE
                                    AND od.data_qc_code IN ('G', 'Q')
                                    ${entityDbIdList}
                                LEFT JOIN data_terminal.transaction_dataset t
                                    ON t.entity_id = p.id
                                    AND t.transaction_id = ${transactionDbId}
                                    AND t.variable_id = od.variable_id
                                    AND t.status IN ('new', 'updated')
                                    AND t.is_suppressed = FALSE
                                    AND t.is_void = FALSE
                                    AND t.data_unit = 'plot'
                                LEFT JOIN experiment.plot_data op
                                    ON op.plot_id = p.id
                                    AND op.variable_id = od.variable_id
                                    AND op.is_void = FALSE
                                    AND op.data_qc_code IN ('G', 'Q')
                                WHERE
                                    pd.variable_id IN (${paramVarIdArr})
                                    AND pd.is_void = FALSE
                                    AND p.occurrence_id = (
                                        SELECT (jsonb_array_element(occurrence, 0)->>'occurrenceDbId')::INTEGER
                                        FROM data_terminal.transaction tr
                                        WHERE tr.id = ${transactionDbId}
                                    )
                                    AND (
                                        t.entity_id IS NULL -- Exclude if already present in transaction_dataset
                                        AND op.plot_id IS NULL -- Exclude if already present in plot_data
                                    )

                                        ) combined_data
                                        ORDER BY id$$,
                                        $$SELECT unnest(array[${paramVarIdArr}])$$
                                    ) AS data(
                                        id INTEGER,
                                        ${paramVarAbbrevArr}
                                    )
                        `

                    let suppressedValConditions = `
                        WHEN NOT EXISTS (
                            SELECT 1
                            FROM data_terminal.transaction_dataset ts
                            WHERE ts.entity_id = t.entity_id
                            AND ts.transaction_id = ${transactionDbId}
                            AND ts.variable_id IN (${paramVarIdArr})
                            AND ts.is_suppressed = FALSE
                        ) THEN NULL
                    `
                    let suppressedVoidConditions = `
                        WHEN NOT EXISTS (
                            SELECT 1
                            FROM data_terminal.transaction_dataset ts
                            WHERE ts.entity_id = t.entity_id
                            AND ts.transaction_id = ${transactionDbId}
                            AND ts.variable_id IN (${paramVarIdArr})
                            AND ts.is_suppressed = FALSE
                        ) THEN TRUE
                    `
                    if(data.withoutSuppression !== undefined && data.withoutSuppression == true){
                        suppressedValConditions = `WHEN t.value IS NOT NULL AND t.is_generated = TRUE AND t.notes = 'uploaded' THEN t.value`
                        suppressedVoidConditions = ''
                    }

                    await sequelize.transaction(async transaction => {
                        let updateQuery = `
                            UPDATE data_terminal.transaction_dataset t
                            SET 
                                is_generated = TRUE,
                                value=(
                                    CASE 
                                        ${suppressedValConditions}
                                        WHEN x.${resultVariableAbbrev} IS NULL AND t.value IS NOT NULL AND t.is_generated = TRUE 
                                            AND t.notes <> 'uploaded' THEN NULL
                                        WHEN x.${resultVariableAbbrev} IS NULL OR trim(x.${resultVariableAbbrev}::text) IN ('NA', '') 
                                            THEN t.value
                                        WHEN x.${resultVariableAbbrev} IS NOT NULL AND t.value IS NULL 
                                            THEN x.${resultVariableAbbrev}::text 
                                        WHEN x.${resultVariableAbbrev} <> t.value::${resultDataType}
                                            THEN x.${resultVariableAbbrev}::text 
                                        ELSE t.value  
                                    END),
                                is_void = (
                                    CASE
                                        ${suppressedVoidConditions}
                                        WHEN (x.${resultVariableAbbrev} IS NULL OR trim(x.${resultVariableAbbrev}::text) IN ('NA', '')) AND t.value IS NOT NULL AND t.is_generated = TRUE AND t.notes <> 'uploaded'
                                        THEN TRUE
                                        ELSE 
                                            FALSE 
                                    END),
                                is_suppressed = ( 
                                    CASE 
                                        WHEN x.${resultVariableAbbrev}<>t.value::${resultDataType} THEN 
                                            FALSE ELSE t.is_suppressed
                                    END)
                            FROM 
                                data_terminal.transaction_dataset ds 
                                    LEFT JOIN 
                                    (${crosstabQuery}) x ON x.plot_id = ds.entity_id 
                            WHERE 
                                t.transaction_id = ${transactionDbId}
                                AND t.variable_id = ${resultVariableId}
                                AND t.entity_id = ds.entity_id
                                AND ds.transaction_id = t.transaction_id
                                AND ds.variable_id = ${resultVariableId}
                                AND ds.data_unit = 'plot'
                            ${datasetEntityDbIdList}
                        `

                        let updateResult = await sequelize.query(updateQuery, {
                            type: sequelize.QueryTypes.UPDATE,
                            transaction: transaction,
                            raw: true
                        }).then((updateResult) => {
                            updateCount = updateResult[1]
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'UPDATE', err)
                            throw new Error(err)
                        })
                        updateCount = 0

                        const insertQuery = `
                            INSERT INTO data_terminal.transaction_dataset
                                (transaction_id, data_unit, entity, entity_id, variable_id, value,
                                creator_id, creation_timestamp, is_void, is_generated, status, 
                                collection_timestamp, is_suppressed) 
                            SELECT 
                                ${transactionDbId}, 
                                'plot', 
                                'plot_data',
                                a.plot_id,
                                ${resultVariableId}, 
                                a.${resultVariableAbbrev}, 
                                ${creatorId}, 
                                NOW(),
                                (
                                CASE WHEN a.${resultVariableAbbrev} IS NULL OR 
                                    TRIM(a.${resultVariableAbbrev}::text) IN ('', 'NA')  
                                    THEN TRUE 
                                ELSE 
                                    FALSE 
                                END
                                ), 
                                TRUE, 
                                (SELECT
                                    CASE
                                        WHEN
                                        (
                                            (min_value is not null
                                            AND a.${resultVariableAbbrev} < min_value::numeric)
                                        OR
                                            (max_value is not null
                                            AND a.${resultVariableAbbrev} > max_value::numeric)
                                        ) THEN 'invalid'
                                        ELSE 'new'
                                    END
                                FROM master.scale WHERE id = (SELECT scale_id FROM master.variable WHERE id = ${resultVariableId})
                                ),
                                NOW(),
                                FALSE
                            FROM 
                                (${crosstabQuery})a
                            WHERE 
                                NOT exists
                                (
                                    SELECT 1
                                    FROM 
                                        data_terminal.transaction_dataset t
                                    WHERE 
                                        transaction_id=${transactionDbId}
                                        AND variable_id=${resultVariableId}
                                        AND a.plot_id=t.entity_id
                                        AND t.data_unit = 'plot'
                                )
                                AND (
                                (EXISTS
                                    (
                                    SELECT 1
                                    FROM 
                                        experiment.plot_data p
                                    WHERE 
                                        p.variable_id=${resultVariableId}
                                        AND p.is_void=FALSE
                                        AND p.plot_id=a.plot_id 
                                    )
                                    AND a.${resultVariableAbbrev} IS NULL
                                )
                                OR a.${resultVariableAbbrev} IS NOT NULL
                                ) 
                                AND a.${resultVariableAbbrev} IS NOT NULL
                        `

                        let insertResult = await sequelize.query(insertQuery, {
                            type: sequelize.QueryTypes.INSERT,
                            transaction: transaction
                        }).then((insertResult) => {
                            insertCount = insertResult[1]
                        }).catch(async err => {
                            logger.logFailingQuery(endpoint, 'INSERT', err)
                            throw new Error(err)
                        })
                        insertCount = 0

                        resultArray.push({
                            resultVariableDbId: resultVariableId,
                            formulaDbId: formula,
                            recordCount: insertCount + updateCount
                        })
                    })
                }

            } // end loop 

            await sequelize.transaction(async transaction => {
                for (let variable of resultDbId) {
                    // Check if computed variables are already in experiment.plot_data
                    let updateQuery = `
                        UPDATE data_terminal.transaction_dataset t
                        SET 
                            status = 'updated'
                        FROM 
                            experiment.plot_data pd
                        WHERE
                            t.value <> pd.data_value 
                            AND t.entity = 'plot_data'
                            AND pd.variable_id = t.variable_id
                            AND pd.plot_id = t.entity_id
                            AND pd.is_void = FALSE
                            AND t.variable_id = ${variable}
                            AND t.transaction_id= ${transactionDbId}
                            AND t.status = 'new'
                    `
                    await sequelize.query(updateQuery, {
                        type: sequelize.QueryTypes.UPDATE,
                        transaction: transaction,
                        raw: true
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'UPDATE', err)
                        throw new Error(err)
                    })
    
                    updateQuery = `
                        DELETE FROM data_terminal.transaction_dataset t
                        USING 
                            experiment.plot_data pd
                        WHERE
                            t.value = pd.data_value 
                            AND t.entity = 'plot_data'
                            AND pd.variable_id = t.variable_id
                            AND pd.plot_id = t.entity_id
                            AND pd.is_void = FALSE
                            AND t.variable_id = ${variable}
                            AND t.transaction_id= ${transactionDbId}
                    `
                    await sequelize.query(updateQuery, {
                        type: sequelize.QueryTypes.DELETE,
                        transaction: transaction,
                        raw: true
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'DELETE', err)
                        throw new Error(err)
                    })
                }
            })
            
            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}