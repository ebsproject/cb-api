/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let responseHelper = require('../../../../helpers/responses')

module.exports = {

    /**
     * Create records in data_terminal.terminal
     * POST call for /v3/terminal-transactions/:id/dataset-table
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {

        let transactionDbId = req.params.id
        let invalidVariables = []
        let errorCode

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = data.records

        //temporary remove designation for values that has " ' " e.g. FU-P'I-HUA (KAOHSIUNG)
        let checkDesignation = ''
        let recordSize = records.length

        //temporary remove designation to multiple data entries
        for(let countArr = 0; countArr <= recordSize; countArr++){
            checkDesignation = records[countArr] ?? ''
            if(checkDesignation != ''){
                delete records[countArr]['designation']
            }
        }
        
        let measurementVariables = data.measurementVariables

        // validate if mandatory field, records
        if (records == undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // validate mandatory field, measurementVariables
        if (measurementVariables === undefined) {
            let errMsg = 'Required parameters are missing. Ensure that measurementVariables fields are not empty.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // validate if measurementVariables is an array
        if (!Array.isArray(measurementVariables)) {
            let errMsg = 'Invalid format, measurementVariables must be an array.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID is not committed
        transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId} 
                    AND status <> 'committed'
                    AND is_void = FALSE
            )
        `
        transactionResult = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })
        if (!transactionResult[0]['exists']) {
            let errMsg = 'The dataset cannot be created. Ensure that the status of the terminal transaction ID is uploaded or uploading in progress.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // validate if measurementVariables exist in database
        for (let variable of measurementVariables) {
            // Check if transaction ID exists
            let variableQuery = `
                SELECT exists(
                    SELECT 1
                    FROM 
                        master.variable
                    WHERE 
                        abbrev='${variable}'
                        AND type IN ('metadata','observation')
                        AND is_void = FALSE
                )
            `
            let variableResult = await sequelize.query(variableQuery, {
                type: sequelize.QueryTypes.SELECT
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
            if (!variableResult[0]['exists']) {
                invalidVariables.push(variable)
            }
        }
        if (invalidVariables.length > 0) {
            let errMsg = 'There is at least one(1) invalid variable in measurementVariables field.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let creatorId = await tokenHelper.getUserId(req)

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            //  validate records and insert to data_terminal.transaction_dataset
            records = JSON.stringify(records).replace(/'/g, "’")
            measurementVariables = measurementVariables.toString().toLowerCase()

            let datasetQuery = `
                SELECT data_terminal.create_transaction_dataset(
                    ${transactionDbId},
                    ${creatorId},
                    '${records}'::json,
                    '{${measurementVariables}}'::text[]
                ) AS result
            `
            let datasetResult = await sequelize.query(datasetQuery, {
                type: sequelize.QueryTypes.SELECT
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            if (isNaN(parseInt(datasetResult[0]['result']))) {
                let errMsg
                if (datasetResult[0]['result'].match(/^.+PLOT_ID.+$/)) {
                    errMsg = 'There is at least one(1) plot that does not exist in the location.'
                } else if (datasetResult[0]['result'].match(/^.+CROSS_ID.+$/)) {
                    errMsg = 'There is at least one(1) cross that does not exist in the location.'
                } else {
                    errMsg = 'There is at least one(1) record that does not have cross_id and plot_id.'
                }
                errorCode = await responseHelper.getErrorCodebyMessage(
                    400,
                    errMsg)
                errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            res.send(200, {
                count: parseInt(datasetResult[0]['result'])
            })
            return

        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}