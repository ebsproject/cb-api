/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let searchHelper = require('../../../../helpers/queryTool/index.js')
let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let validator = require('validator')
const logger = require('../../../../helpers/logger')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}
        let addedConditionString = ''
        let condition = ''
        let transactionDbId = req.params.id
        //logger variables
        const endpoint = 'terminal-transactions/:id/cross-datasets-table-search'
        const operation = 'SELECT'

        // check if transactionDbId is valid
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transactionDbId must be an integer'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionRecord[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get all measurement variables in the transaction
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') AS "variableId",
                STRING_AGG( '"' || abbrev || '"', ',') AS "selectColumns",
                STRING_AGG( abbrev, ',') AS "variableAbbrev",
                STRING_AGG( '"' || abbrev || '"' || ' jsonb', ',') AS "crosstabColumns"
            FROM(
                SELECT 
                    distinct (dataset.variable_id) AS variable_id,
                    v.abbrev
                FROM
                    data_terminal.transaction_dataset dataset
                    LEFT JOIN master.variable v ON v.id = dataset.variable_id
                WHERE
                    dataset.transaction_id = ${transactionDbId}
                ORDER BY v.abbrev
            )a
        `
        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        let variableId = variableList[0]['variableId']
        let crosstabColumns = variableList[0]['crosstabColumns']
        let selectColumns = variableList[0]['selectColumns']
        let variableAbbrev = variableList[0]['variableAbbrev'].split(',')

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set columns to be excluded in parameters for filtering
            Array.prototype.diff = function (a) {
                return this.filter(function (i) { return a.indexOf(i) < 0; });
            };

            // Exclude all measurement variables
            let excludedParametersArray = variableAbbrev.diff([
                'transactionDbId',
                'entityDbId',
                'crossName',
                'crossMethod',
                'crossRemarks',
                'germplasmDesignation',
                'germplasmParentage',
                'germplasmGeneration',
                'crossFemaleParent',
                'femaleParentage',
                'femaleParentSeedSource',
                'crossMaleParent',
                'maleParentage',
                'maleParentSeedSource',
                'femaleSourceEntryDbId',
                'femaleSourceEntry',
                'femaleSourceSeedName',
                'femaleSourcePlotDbId',
                'femaleSourcePlot',
                'femaleParentEntryNumber',
                'femaleOccurrenceName',
                'femaleOccurrenceCode',
                'maleParentEntryNumber',
                'maleOccurrenceName',
                'maleOccurrenceCode',
                'maleSourceEntryDbId',
                'maleSourceEntry',
                'maleSourceSeedName',
                'maleSourcePlotDbId',
                'maleSourcePlot',
            ])

            excludedParametersArray.push('fields', 'status', "isVoid", "isSuppressed")

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray
            )
            // special case to search for values according to status
            if (req.body.status != null) {
                let status = req.body.status.trim()
                let statusValue = []

                for (value of status.split('|')) {

                    statusValue.push("''" + value + "''")
                }
                addedConditionString = " AND status in ( " + statusValue.toString() + ")"
            }

            // special case to search for values according to isVoid column
            if (req.body.isVoid != null) {
                let isVoid = req.body.isVoid
                addedConditionString += ` AND dataset.is_void= ${isVoid}`
            }

            // special case to search for values according to isSuppressed column
            if (req.body.isSuppressed != null) {
                let isSuppressed = req.body.isSuppressed
                addedConditionString += ` AND dataset.is_suppressed= ${isSuppressed}`
            }

            let conditionArray = []

            // Get filters for measurement variables with values that is in JSON format
            for (measurementVariable in req.body) {

                if (variableAbbrev.includes(measurementVariable)) {

                    for (field in req.body[measurementVariable]) {
                        condition = ''
                        if (field == 'value') {
                            value = req.body[measurementVariable]['value']
                            column = '("' + measurementVariable + '"->>' + "'value')"
                        } else if (
                            field == 'status'
                            || field == 'isSuppressed'
                            || field == 'suppressRemarks'
                            || field == 'isVoid'
                        ) {

                            value = req.body[measurementVariable][field];
                            column = '("' + measurementVariable + '"->>' + "'" + field + "')"
                        }

                        condition = searchHelper.getFilterCondition(value, column, columnCast = true)
                        if (condition != '') {
                            conditionArray.push(`(` + condition + ')')
                        }
                    }
                }
            }

            if (conditionArray.length > 0) {
                condition = conditionArray.join(' AND ')
                if (conditionString == '') {

                    conditionString = conditionString +
                        ` WHERE `
                        + condition
                } else {
                    conditionString = conditionString +
                        ` AND `
                        + condition
                }
            }
            if (conditionString.includes('You have provided invalid values for filter.')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // build the base query
        let datasetsQuery = null
        let fromQuery = `
            FROM
                CROSSTAB(
                '
                SELECT 
                    dataset.entity_id AS "entityDbId",
                    dataset.transaction_id AS "transactionDbId",
                    
                    "cross".cross_name AS "crossName",
                    "cross".cross_method AS "crossMethod",
                    "cross".remarks AS "crossRemarks",
                    germplasm.designation AS "germplasmDesignation",
                    germplasm.parentage AS "germplasmParentage",
                    germplasm.generation AS "germplasmGeneration",
                    
                    "femaleParent"."crossFemaleParent",
                    "femaleParent"."femaleParentage",
                    "femaleParent"."femaleParentSeedSource",

                    "maleParent"."crossMaleParent",
                    "maleParent"."maleParentage",
                    "maleParent"."maleParentSeedSource",

                    "femaleOccurrence"."femaleOccurrenceName",
					"femaleOccurrence"."femaleOccurrenceCode",
					
					"maleOccurrence"."maleOccurrenceName",
					"maleOccurrence"."maleOccurrenceCode",

                    "femaleEntryTbl"."femaleSourceEntryDbId",
                    "femaleEntryTbl"."femaleSourceEntry",
                    "femaleEntryTbl"."femaleSourceSeedName",
                    "femalePlotTbl"."femaleSourcePlotDbId",
                    "femalePlotTbl"."femaleSourcePlot",

                    "femaleParentEntryTbl"."femaleParentEntryNumber",
                    
                    "maleParentEntryTbl"."maleParentEntryNumber",
                    
                    "maleEntryTbl"."maleSourceEntryDbId",
                    "maleEntryTbl"."maleSourceEntry",
                    "maleEntryTbl"."maleSourceSeedName",
                    "malePlotTbl"."maleSourcePlotDbId",
                    "malePlotTbl"."maleSourcePlot",
                    
                    dataset.variable_id,
                    jsonb_build_object(
                    ''value'',dataset.value, 
                    ''status'', dataset.status, 
                    ''isSuppressed'', dataset.is_suppressed,
                    ''suppressRemarks'', dataset.suppress_remarks,
                    ''isVoid'', dataset.is_void,
                    ''crossDataValue'', cd.data_value,
                    ''crossDataQcCode'', cd.data_qc_code
                    )
                FROM  
                    data_terminal.transaction_dataset dataset
                    LEFT JOIN germplasm.cross "cross"
                        ON "cross".id = dataset.entity_id 
                    LEFT JOIN germplasm.cross_data cd 
                        ON cd.cross_id = dataset.entity_id 
                        AND cd.is_void=false 
                        AND cd.variable_id = dataset.variable_id                    
                    LEFT JOIN germplasm.germplasm germplasm
                        ON "cross".germplasm_id = germplasm.id
                    LEFT JOIN
                        (
                            SELECT
                                e.id AS "femaleSourceEntryDbId",
                                e.entry_code AS "femaleSourceEntry",
                                s.seed_name AS "femaleSourceSeedName",
                                cross_parent.cross_id AS "crossDbId",
                                germplasm.designation AS "crossFemaleParent"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.germplasm germplasm ON germplasm.id = cross_parent.germplasm_id
                                LEFT JOIN
                                    germplasm.seed s ON s.id = cross_parent.seed_id
                                LEFT JOIN
                                    experiment.entry e ON e.id = s.source_entry_id
                            WHERE
                               (cross_parent.parent_role= ''female'' OR cross_parent.parent_role = ''female-and-male'') AND
                                cross_parent.is_void = false AND
                                s.is_void = FALSE AND
                                e.is_void = FALSE
                        ) AS "femaleEntryTbl" ON "femaleEntryTbl"."crossDbId" = "cross".id
                    LEFT JOIN
                        (
                            SELECT
                                e.id AS "maleSourceEntryDbId",
                                e.entry_code AS "maleSourceEntry",
                                s.seed_name AS "maleSourceSeedName",
                                cross_parent.cross_id AS "crossDbId"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.seed s ON s.id = cross_parent.seed_id
                                LEFT JOIN
                                    experiment.entry e ON e.id = s.source_entry_id
                            WHERE
                                (cross_parent.parent_role = ''male'' OR cross_parent.parent_role = ''female-and-male'') AND
                                cross_parent.is_void = FALSE AND
                                s.is_void = FALSE AND
                                e.is_void = FALSE
                        ) AS "maleEntryTbl" ON "maleEntryTbl"."crossDbId" = "cross".id
                    LEFT JOIN
                        (
                            SELECT
                                p.id AS "femaleSourcePlotDbId",
                                p.plot_code AS "femaleSourcePlot",
                                cross_parent.cross_id AS "crossDbId"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.seed s ON s.id = cross_parent.seed_id
                                LEFT JOIN
                                    experiment.plot p ON p.id = s.source_plot_id
                            WHERE
                                (cross_parent.parent_role= ''female'' OR cross_parent.parent_role = ''female-and-male'') AND
                                cross_parent.is_void = FALSE AND
                                p.is_void = FALSE
                        ) AS "femalePlotTbl" ON "femalePlotTbl"."crossDbId" = "cross".id
                    LEFT JOIN
                        (
                            SELECT
                                p.id AS "maleSourcePlotDbId",
                                p.plot_code AS "maleSourcePlot",
                                cross_parent.cross_id AS "crossDbId"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.seed s ON s.id = cross_parent.seed_id
                                LEFT JOIN
                                    experiment.plot p ON p.id = s.source_plot_id
                            WHERE
                                (cross_parent.parent_role = ''male'' OR cross_parent.parent_role = ''female-and-male'') AND
                                cross_parent.is_void = FALSE AND
                                p.is_void = FALSE
                        ) AS "malePlotTbl" ON "malePlotTbl"."crossDbId" = "cross".id
                    LEFT JOIN
                        (
                            SELECT
                                g.parentage AS "femaleParentage",
                                cross_parent.cross_id AS "crossDbId",
                                g.designation AS "crossFemaleParent",
                                s.seed_code AS "femaleParentSeedSource"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                                LEFT JOIN
                                    germplasm.seed s ON s.id = cross_parent.seed_id
                            WHERE
                                cross_parent.is_void = FALSE AND
                                (cross_parent.parent_role= ''female'' OR cross_parent.parent_role = ''female-and-male'') AND
                                g.is_void = FALSE
                        ) AS "femaleParent" ON "femaleParent"."crossDbId" = "cross".id
                    LEFT JOIN
                        (
                            SELECT
                                g.parentage AS "maleParentage",
                                cross_parent.cross_id AS "crossDbId",
                                g.designation AS "crossMaleParent",
                                s.seed_code AS "maleParentSeedSource"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                                LEFT JOIN
                                    germplasm.seed s ON s.id = cross_parent.seed_id
                            WHERE
                                cross_parent.is_void = FALSE AND
                                (cross_parent.parent_role = ''male'' OR cross_parent.parent_role = ''female-and-male'') AND
                                g.is_void = FALSE AND
                                s.is_void = FALSE
                        ) AS "maleParent" ON "maleParent"."crossDbId" = "cross".id
                    LEFT JOIN
                        (
                            SELECT
                                e.entry_number AS "femaleParentEntryNumber",
                                cross_parent.cross_id AS "crossDbId"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.germplasm germplasm ON germplasm.id = cross_parent.germplasm_id
                                LEFT JOIN
                                    experiment.entry e ON e.id = cross_parent.entry_id
                            WHERE
                               (cross_parent.parent_role= ''female'' OR cross_parent.parent_role = ''female-and-male'') AND
                                cross_parent.is_void = false AND
                                e.is_void = FALSE
                        ) AS "femaleParentEntryTbl" ON "femaleParentEntryTbl"."crossDbId" = "cross".id
                    LEFT JOIN
                        (
                            SELECT
                                e.entry_number AS "maleParentEntryNumber",
                                cross_parent.cross_id AS "crossDbId"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    germplasm.germplasm germplasm ON germplasm.id = cross_parent.germplasm_id
                                LEFT JOIN
                                    experiment.entry e ON e.id = cross_parent.entry_id
                            WHERE
                               (cross_parent.parent_role= ''male'' OR cross_parent.parent_role = ''female-and-male'') AND
                                cross_parent.is_void = false AND
                                e.is_void = FALSE
                        ) AS "maleParentEntryTbl" ON "maleParentEntryTbl"."crossDbId" = "cross".id
                        LEFT JOIN
                        (
                            SELECT
                                cross_parent.cross_id AS "crossDbId",
                                occ.occurrence_name AS "femaleOccurrenceName",
                                occ.occurrence_code AS "femaleOccurrenceCode"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    experiment.occurrence occ ON occ.id = cross_parent.occurrence_id
                            WHERE
                                cross_parent.is_void = FALSE AND
                                (cross_parent.parent_role = ''female'' OR cross_parent.parent_role = ''female-and-male'')
                        ) AS "femaleOccurrence" ON "femaleOccurrence"."crossDbId" = "cross".id
					LEFT JOIN
                        (
                            SELECT
                                cross_parent.cross_id AS "crossDbId",
                                occ.occurrence_name AS "maleOccurrenceName",
                                occ.occurrence_code AS "maleOccurrenceCode"
                            FROM
                                germplasm.cross_parent cross_parent
                                LEFT JOIN
                                    experiment.occurrence occ ON occ.id = cross_parent.occurrence_id
                            WHERE
                                cross_parent.is_void = FALSE AND
                                (cross_parent.parent_role = ''male'' OR cross_parent.parent_role = ''female-and-male'')
                        ) AS "maleOccurrence" ON "maleOccurrence"."crossDbId" = "cross".id
                    
                WHERE
                    dataset.transaction_id = ${transactionDbId}
                    AND dataset.entity = ''cross_data''

                    ${addedConditionString}
                ORDER BY "cross".id
                ',
                '
                SELECT UNNEST(ARRAY[${variableId}])
                '       
                ) AS ("entityDbId" integer, "transactionDbId" integer, "crossName" text , "crossMethod" text ,  "crossRemarks" text,
                    "germplasmDesignation" text, "germplasmParentage" text , "germplasmGeneration" text , 
                    "crossFemaleParent" text , "femaleParentage" text , "femaleParentSeedSource" text , 
                    "crossMaleParent" text , "maleParentage" text , "maleParentSeedSource" text , 
                    "femaleOccurrenceName" text, "femaleOccurrenceCode" text, "maleOccurrenceName" text, "maleOccurrenceCode" text,
                    "femaleSourceEntryDbId" integer , "femaleSourceEntry" text , "femaleSourceSeedName" text , 
                    "femaleSourcePlotDbId" integer , "femaleSourcePlot" text , 
                    "femaleParentEntryNumber" integer, "maleParentEntryNumber" integer,
                    "maleSourceEntryDbId" integer , "maleSourceEntry" text , "maleSourceSeedName" text , 
                    "maleSourcePlotDbId" integer , "maleSourcePlot" text , 
                    ${crosstabColumns}) 
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            datasetsQuery = knex.column(parameters['fields'].split('|'))
            datasetsQuery += fromQuery
        } else {
            datasetsQuery = `
                SELECT
                    "transactionDbId",
                    "entityDbId",
                    "crossName",
                    "crossMethod",
                    "crossRemarks",
                    "germplasmDesignation",
                    "germplasmParentage",
                    "germplasmGeneration",

                    "crossFemaleParent",
                    "femaleParentage",
                    "femaleParentSeedSource",

                    "crossMaleParent",
                    "maleParentage",
                    "maleParentSeedSource",

                    "femaleSourceEntryDbId",
                    "femaleSourceEntry",
                    "femaleSourceSeedName",
                    "femaleSourcePlotDbId",
                    "femaleSourcePlot",

                    "femaleParentEntryNumber",
                    
                    "maleParentEntryNumber",

                    "femaleOccurrenceName",
                    "femaleOccurrenceCode",
				
                    "maleOccurrenceName",
                    "maleOccurrenceCode",
                    
                    "maleSourceEntryDbId",
                    "maleSourceEntry",
                    "maleSourceSeedName",
                    "maleSourcePlotDbId",
                    "maleSourcePlot",
                    ${selectColumns}
                ${fromQuery}
            `
        }

        // Parse the sort body
        if (sort != null) {
            orderString = await processQueryHelper.getOrderStringTrait(
                sort,
                selectColumns.replaceAll('"', '').split(','),
                'value'
            )
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let datasetsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            datasetsQuery,
            conditionString,
            orderString
        )

        datasetsFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            datasetsFinalSqlQuery,
            orderString
        )

        // Retrieve the datasets records
        let datasets = await sequelize
            .query(datasetsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await datasets == undefined || await datasets.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        count = datasets[0].totalCount

        res.send(200, {
            rows: datasets,
            count: count
        })
        return
    }
    
}