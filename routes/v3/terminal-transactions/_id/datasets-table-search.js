/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let searchHelper = require('../../../../helpers/queryTool/index.js')
let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let responseHelper = require('../../../../helpers/responses')
let validator = require('validator')
const logger = require('../../../../helpers/logger')

module.exports = {
    post: async (req, res) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}
        let addedConditionString = ''
        let condition = ''
        let transactionDbId = req.params.id
        let conditionStringArr = []
        let excludedParametersArray = []
        let responseColString = `"transactionDbId",
            "entityDbId",
            "locationName",
            "locationCode" ,
            "occurrenceName",
            "occurrenceCode" ,
            "plotCode",
            "plotNumber",
            "plotType",
            rep, 
            "designX",
            "designY",
            "paX",
            "paY",
            "fieldX",
            "fieldY",
            "germplasmName",
            "germplasmCode",
            "parentage",
            "entryNumber",
            "entryCode",
            "entryName",
            "entryType",`
        const endpoint = 'terminal-transactions/:id/datasets-table-search'
        const operation = 'SELECT'

        // check if transactionDbId is valid
        if (!validator.isInt(transactionDbId)) {
            let errMsg = 'Invalid format, transactionDbId must be an integer'
            let errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1
                FROM 
                    data_terminal.transaction 
                WHERE 
                    id=${transactionDbId}
                    AND is_void = FALSE
            )
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (!transactionRecord[0]['exists']) {
            let errMsg = 'The terminal transaction ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get all measurement variables in the transaction
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') AS "variableId",
                STRING_AGG( '"' || abbrev || '"', ',') AS "selectColumns",
                STRING_AGG( abbrev, ',') AS "variableAbbrev",
                STRING_AGG( '"' || abbrev || '"' || ' jsonb', ',') AS "crosstabColumns"
            FROM(
                SELECT 
                    distinct (dataset.variable_id) AS variable_id,
                    v.abbrev
                FROM
                    data_terminal.transaction_dataset dataset
                    LEFT JOIN master.variable v ON v.id = dataset.variable_id
                WHERE
                    dataset.transaction_id = ${transactionDbId}
                ORDER BY v.abbrev
            )a
        `
        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        let variableId = variableList[0]['variableId']
        let crosstabColumns = variableList[0]['crosstabColumns']
        let selectColumns = variableList[0]['selectColumns']
        let variableAbbrev = variableList[0]['variableAbbrev'].split(',')

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set columns to be excluded in parameters for filtering
            Array.prototype.diff = function (a) {
                return this.filter(function (i) { return a.indexOf(i) < 0; });
            };

            // Exclude all measurement variables
            excludedParametersArray = variableAbbrev.diff([
                'transactionDbId',
                'entityDbId',
                'locationName',
                'locationCode',
                'occurrenceName',
                'occurrenceCode',
                'plotCode',
                'plotNumber',
                'plotType',
                'rep',
                'designX',
                'designY',
                'paX',
                'paY',
                'fieldX',
                'fieldY',
                'germplasmName',
                'germplasmCode',
                'parentage',
                'entryNumber',
                'entryCode',
                'entryName',
                'entryType'
            ])

            excludedParametersArray.push('fields', 'status', "isVoid", "isSuppressed")

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray
            )

            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )
            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''

            // special case to search for values according to status
            if (req.body.status != null) {
                let status = req.body.status.trim()
                let statusValue = []

                for (value of status.split('|')) {

                    statusValue.push("''" + value + "''")
                }
                addedConditionString = " AND status in ( " + statusValue.toString() + ")"
            }

            // special case to search for values according to isVoid column
            if (req.body.isVoid != null) {
                let isVoid = req.body.isVoid
                addedConditionString += ` AND dataset.is_void= ${isVoid}`
            }

            // special case to search for values according to isSuppressed column
            if (req.body.isSuppressed != null) {
                let isSuppressed = req.body.isSuppressed
                addedConditionString += ` AND dataset.is_suppressed= ${isSuppressed}`
            }

            let conditionArray = []

            // Get filters for measurement variables with values that is in JSON format
            for (let measurementVariable in req.body) {

                if (variableAbbrev.includes(measurementVariable)) {

                    for (let field in req.body[measurementVariable]) {
                        condition = ''
                        if (field == 'value') {
                            value = req.body[measurementVariable]['value']
                            column = '("' + measurementVariable + '"->>' + "'value')"
                        } else if (
                            field == 'status'
                            || field == 'isSuppressed'
                            || field == 'suppressRemarks'
                            || field == 'isVoid'
                        ) {

                            value = req.body[measurementVariable][field];
                            column = '("' + measurementVariable + '"->>' + "'" + field + "')"
                        }

                        condition = searchHelper.getFilterCondition(value, column, columnCast = true)
                        if (condition != '') {
                            conditionArray.push(`(` + condition + ')')
                        }
                    }
                }
            }

            if (conditionArray.length > 0) {
                condition = conditionArray.join(' AND ')
                if (conditionString == '') {

                    conditionString = conditionString +
                        ` WHERE `
                        + condition
                } else {
                    conditionString = conditionString +
                        ` AND `
                        + condition
                }
            }
            if (conditionString.includes('You have provided invalid values for filter.')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // build the base query
        let datasetsQuery = null
        let fromQuery = `
            FROM
                CROSSTAB(
                '
                SELECT 
                    dataset.entity_id AS "entityDbId",
                    dataset.transaction_id AS "transactionDbId",
                    l.location_name AS "locationName",
                    l.location_code AS "locationCode",
                    o.occurrence_name AS "occurrenceName",
                    o.occurrence_code AS "occurrenceCode" ,
                    p.plot_code AS "plotCode",
                    p.plot_number AS "plotNumber",
                    p.plot_type AS "plotType",
                    p.rep, 
                    p.design_x AS "designX",
                    p.design_y AS "designY",
                    p.pa_x AS "paX",
                    p.pa_y AS "paY",
                    p.field_x AS "fieldX",
                    p.field_y AS "fieldY",
                    e.entry_name AS "germplasmName",
                    g.germplasm_code AS "germplasmCode",
                    g.parentage AS "parentage",
                    e.entry_number AS "entryNumber",
                    e.entry_code AS "entryCode",
                    e.entry_name AS "entryName",
                    e.entry_type AS "entryType",
                    dataset.variable_id,
                    jsonb_build_object(
                    ''value'',dataset.value, 
                    ''status'', dataset.status, 
                    ''isSuppressed'', dataset.is_suppressed,
                    ''suppressRemarks'', dataset.suppress_remarks,
                    ''isVoid'', dataset.is_void,
                    ''dataValue'', pd.data_value,
                    ''dataQCCode'', pd.data_qc_code,
                    ''creationTimestamp'',dataset.creation_timestamp,
                    ''modificationTimestamp'',dataset.modification_timestamp
                    )
                FROM  
                    data_terminal.transaction_dataset dataset
                    LEFT JOIN experiment.planting_instruction e ON e.plot_id = dataset.entity_id
                    LEFT JOIN experiment.plot_data pd ON pd.plot_id = dataset.entity_id 
                        AND pd.is_void=false 
                        AND pd.variable_id = dataset.variable_id
                    LEFT JOIN experiment.plot p ON p.id = e.plot_id
                    LEFT JOIN experiment.location l ON l.id= p.location_id
                    LEFT JOIN experiment.occurrence o ON o.id= p.occurrence_id
                        AND e.is_void=false
                    LEFT JOIN germplasm.germplasm g ON g.id = e.germplasm_id
                        AND g.is_void=false
                WHERE
                    dataset.transaction_id=${transactionDbId}
                        AND entity = ''plot_data''
                    ${addedConditionString}
                ORDER BY p.id
                ',
                '
                SELECT UNNEST(ARRAY[${variableId}])
                '       
                ) AS ("entityDbId" integer, "transactionDbId" integer, "locationName" text, 
                    "locationCode" text, "occurrenceName" text, "occurrenceCode" text,
                    "plotCode" text, "plotNumber" integer, "plotType" text, rep integer, "designX" integer, 
                    "designY" integer, "paX" integer, "paY" integer, "fieldX" integer, "fieldY" integer,
                    "germplasmName" text, "germplasmCode" text, "parentage" text, "entryNumber" integer, 
                    "entryCode" text, "entryName" text, "entryType" text, ${crosstabColumns}) 
        `

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            datasetsQuery = knex.column(parameters['fields'].split('|'))
            datasetsQuery += fromQuery
        } else {
            datasetsQuery = `
                SELECT
                ${responseColString}
                ${selectColumns}
                ${fromQuery}
            `
        }

      
        // Parse the sort parameter
        if (sort != null) {
            orderString = await processQueryHelper.getOrderStringTrait(
                sort,
                selectColumns.replaceAll('"', '').split(','),
                'value'
            )

            // Handle "NA" and invalid values as NULL for sorting
            orderString = orderString.replace(/"(\w+)"->>'value'/gi, `(CASE WHEN "$1"->>'value' = 'NA' OR "$1"->>'value' ~ '[a-zA-Z]' THEN NULL ELSE "$1"->>'value' END)`);

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let datasetsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
            datasetsQuery,
            conditionString,
            orderString
        )

        datasetsFinalSqlQuery = await processQueryHelper.getFinalTotalCountQuery(
            datasetsFinalSqlQuery,
            orderString
        )

        // Retrieve the datasets records
        let datasets = await sequelize
            .query(datasetsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await datasets == undefined || await datasets.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } 

        count = datasets[0].totalCount

        res.send(200, {
            rows: datasets,
            count: count
        })
        return
    }
}