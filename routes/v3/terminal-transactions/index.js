/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let responseHelper = require('../../../helpers/responses')
let validator = require('validator')
let logger = require('../../../helpers/logger')
const endpoint = 'terminal-transactions'

module.exports = {

    // Create record in data_terminal.transaction
    // POST call for /v3/terminal-transactions
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let terminalTransactionUrl = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/terminal-transactions'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let records = data.records
        if (records == undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction
        let creatorId = await tokenHelper.getUserId(req)

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {

            for (var record of records) {
                let locationDbId = null
                let action = record.action
                let remarks = null
                let errorCode = null
                let experimentDbId = null
                let occurrence = null

                // Check if occurrence is provided and it is an array
                if (record.occurrence && Array.isArray(record.occurrence)) {
                    occurrence = record.occurrence;
                }

                // validate if mandatory fields exist : action
                if ( action === undefined ) {
                    let errMsg = 'Required parameters are missing. Ensure that action field is not empty.'
                    errorCode = await responseHelper.getErrorCodebyMessage(
                        400,
                        errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (record.locationDbId !== undefined){
                    locationDbId = record.locationDbId

                    // validates if locationDbId is integer
                    if (!validator.isInt(locationDbId)) {
                        let errMsg = 'Invalid format, locationDbId must be an integer.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // validate if the locationDbId is in the database
                    let locationQuery = `
                        SELECT *
                        FROM
                            experiment.location
                        WHERE id=${locationDbId}
                    `
                    let locationArray = await sequelize.query(locationQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (await locationArray == undefined || await locationArray.length == 0) {
                        let errMsg = 'The provided locationDbId does not exist.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    
                    // Check if the user has an open transaction in the location
                    let openTransactionQuery = `
                        SELECT exists(
                            SELECT 1
                            FROM 
                                data_terminal.transaction 
                            WHERE 
                                location_id = ${locationDbId}
                                AND status <> 'committed'
                                AND creator_id = ${creatorId} 
                                AND action = '${action}'
                        )
                    `
                    let hasOpenTransaction = await sequelize.query(openTransactionQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (hasOpenTransaction[0]['exists']) {
                        let errMsg = 'User still has an open transaction on locationDbId.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                const validActions = [
                    'file upload',
                    'post harvest',
                    'harvest manager',
                    'experiment creation',
                    'save list members',
                ]

                // validate the action
                if (!validActions.includes(action)) {
                    let errMsg = 'The provided action is invalid. Action should be file upload or harvest manager.'
                    errorCode = await responseHelper.getErrorCodebyMessage(400, errMsg)
                    errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate non-mandatory parameters : remarks
                if (record.remarks !== undefined) remarks = record.remarks

                if (record.experimentDbId !== undefined){

                    experimentDbId = record.experimentDbId

                    // validates if locationDbId is integer
                    if (!validator.isInt(experimentDbId)) {
                        let errMsg = 'Invalid format, experimentDbId must be an integer.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // validate if the experimentDbId is in the database
                    let experimentQuery = `
                        SELECT *
                        FROM
                            experiment.experiment
                        WHERE id=${experimentDbId}
                    `
                    let experimentArray = await sequelize.query(experimentQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (await experimentArray == undefined || await experimentArray.length == 0) {
                        let errMsg = 'The provided experimentDbId does not exist.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if the user has an open transaction in the experiment
                    let openExptTransactionQuery = `
                        SELECT exists(
                            SELECT 1
                            FROM 
                                data_terminal.transaction 
                            WHERE 
                                experiment_id = ${experimentDbId}
                                AND status <> 'committed'
                                AND creator_id = ${creatorId} 
                                AND action = '${action}'
                        )
                    `
                    let hasOpenExptTransaction = await sequelize.query(openExptTransactionQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (hasOpenExptTransaction[0]['exists']) {
                        let errMsg = 'User still has an open transaction on experimentDbId.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                let terminalTransaction = []
                    transaction = await sequelize.transaction(async transaction => {

                    // Create transaction record
                    let insertTransactionQuery = `
                        INSERT INTO
                            data_terminal.transaction (
                                status, location_id, occurrence, action, experiment_id, creator_id, remarks, is_void)
                        VALUES(
                            'in queue', ${locationDbId}, '${JSON.stringify(occurrence)}', '${action}', ${experimentDbId},
                            ${creatorId}, '${remarks}' , FALSE
                        )
                        RETURNING
                            id
                    `
                    terminalTransaction = await sequelize.query(insertTransactionQuery, {
                        type: sequelize.QueryTypes.INSERT,
                        transaction: transaction,
                        raw: true
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'INSERT', err)
                        throw new Error(err)
                    })
                })
                
                let array = {
                    transactionDbId: terminalTransaction[0][0]['id'],
                    recordCount: 1,
                    href: terminalTransactionUrl + '/' + terminalTransaction[0][0]['id']
                }
                resultArray.push(array)

            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}