/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}
        let addedDistinctString = ''
        parameters['distinctOn'] = ''
        let includeParentCountQuery = ''
        let femaleCountQuery = ''
        let maleCountQuery = ''
        let entryCodeQuery = 'entry.entry_code'
        const endpoint = 'entries-search'

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields', 'distinctOn', 'includeParentCount']
            // Get filter condition
            conditionString = await processQueryHelper
                .getFilter(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))

                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )
                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            if (req.body.includeParentCount !== undefined || req.body.includeParentCount !== null) {
                if (req.body.includeParentCount == true) {
                    femaleCountQuery = `
                        (SELECT
                            count(1)
                        FROM germplasm.cross_parent 
                        WHERE experiment_id = experiment.id
                            AND (parent_role = 'female' or  parent_role = 'female-and-male')
                            AND entry_id = entry.id
                            AND is_void = false
                        ) AS "femaleCount"`

                    maleCountQuery = `
                        (SELECT
                            count(1)
                        FROM germplasm.cross_parent 
                        WHERE experiment_id = experiment.id
                            AND (parent_role = 'male' or  parent_role = 'female-and-male')
                            AND entry_id = entry.id
                            AND is_void = false
                        ) AS "maleCount"`
                    includeParentCountQuery = femaleCountQuery + ',' + maleCountQuery + ','
                }
            }
        }
        // Build the base retrieval query
        let entryQuery = null
        // Check if the client specified values for the field/s
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(
                        parameters['fields'],
                    )

                let selectString
                    = knex.raw(`${fieldsString}`)
                entryQuery = knex.select(selectString)
            } else {
                entryQuery = knex.column(parameters['fields'].split('|'))
            }

            entryQuery += `
                FROM
                    experiment.entry entry
                LEFT JOIN
                    experiment.entry_list entry_list ON entry_list.id = entry.entry_list_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = entry_list.experiment_id
                LEFT JOIN
                    germplasm.seed seed ON seed.id = entry.seed_id
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = entry.germplasm_id
                LEFT JOIN
                    germplasm.package package ON package.id = entry.package_id
                LEFT JOIN
                    tenant.person creator ON entry.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON entry.modifier_id = modifier.id
                WHERE
                    entry.is_void = FALSE
                ` + addedConditionString +`
                ORDER BY
                    ${parameters['distinctOn']}
                    entry.id
                `
        } else {
            entryQuery = `
                SELECT
                    ${addedDistinctString}
                    entry.id AS "entryDbId",
                    entry.entry_code AS "entryCode",
                    entry.entry_number AS "entryNumber",
                    entry.entry_name AS "entryName",
                    entry.entry_type AS "entryType",
                    entry.entry_role AS "entryRole",
                    entry.entry_class AS "entryClass",
                    entry.entry_status AS "entryStatus",
                    entry.description,
                    entry.entry_list_id AS "entryListDbId",
                    experiment.id AS "experimentDbId",
                    entry.germplasm_id AS "germplasmDbId",
                    germplasm.designation,
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.germplasm_type AS "germplasmNameType",
                    germplasm.germplasm_code AS "germplasmCode",
                    entry.seed_id AS "seedDbId",
                    seed.seed_code AS "seedCode",
                    seed.seed_name AS "seedName",
                    package.id AS "packageDbId",
                    package.package_code AS "packageCode",
                    package.package_label AS "packageLabel",
                    package.package_quantity AS "packageQuantity",
                    package.package_unit AS "packageUnit",
                    seed.source_entry_id AS "sourceEntryDbId",
                    seed.source_occurrence_id AS "sourceOccurrenceDbId",
                    ` + includeParentCountQuery +`
                    entry.notes,
                    entry.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    entry.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    experiment.entry entry
                LEFT JOIN
                    experiment.entry_list entry_list ON entry_list.id = entry.entry_list_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = entry_list.experiment_id
                LEFT JOIN
                    germplasm.seed seed ON seed.id = entry.seed_id
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = entry.germplasm_id
                LEFT JOIN
                    germplasm.package package ON package.id = entry.package_id
                LEFT JOIN
                    tenant.person creator ON entry.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON entry.modifier_id = modifier.id
                WHERE
                    entry.is_void = FALSE
                ` + addedConditionString +`
                ORDER BY
                    ${parameters['distinctOn']}
                    entry.id
                `
        }
      
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }
      
        // Generate the final SQL query
        let entryFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            entryQuery,
            conditionString,
            orderString,
            addedDistinctString,
        )

        // Check if female/male count field is included
        entryFinalSqlQuery = entryFinalSqlQuery.replace('"femaleCountField"', femaleCountQuery)
        entryFinalSqlQuery = entryFinalSqlQuery.replace('"maleCountField"', maleCountQuery)

        if (req.body.retrievePIentryCode) {
            entryCodeQuery = `
                (SELECT entry_code 
                    FROM experiment.planting_instruction 
                    WHERE entry_id = entry.id
                    AND is_void = false
                    LIMIT 1)
            `
            entryFinalSqlQuery = entryFinalSqlQuery.replace(`"entry"."entry_code"`, entryCodeQuery)
        }
        if (req.body.retrievePIseedDbId) {
            seedDbIdQuery = `
                (SELECT seed_id 
                    FROM experiment.planting_instruction 
                    WHERE entry_id = entry.id
                    AND is_void = false
                    LIMIT 1)
            `
            entryFinalSqlQuery = entryFinalSqlQuery.replace(`"entry"."seed_id"`, seedDbIdQuery)

            germplasmDbIdQuery = `
                (SELECT id
                    FROM germplasm.germplasm g
                    WHERE g.is_void = false
                        AND g.id = (SELECT germplasm_id
                            FROM experiment.planting_instruction
                            WHERE entry_id = entry.id
                            AND is_void = false
                            LIMIT 1
                        )
                    LIMIT 1
                )
            `
            entryFinalSqlQuery = entryFinalSqlQuery.replace(`"entry"."germplasm_id"`, germplasmDbIdQuery)

            germplasmCodeQuery = `
                (SELECT germplasm_code
                    FROM germplasm.germplasm g
                    WHERE g.is_void = false
                        AND g.id = (SELECT germplasm_id
                            FROM experiment.planting_instruction
                            WHERE entry_id = entry.id
                            AND is_void = false
                            LIMIT 1
                        )
                    LIMIT 1
                )
            `
            entryFinalSqlQuery = entryFinalSqlQuery.replace(`"germplasm"."germplasm_code"`, germplasmCodeQuery)

            designationQuery = `
                (SELECT designation
                    FROM germplasm.germplasm g
                    WHERE g.is_void = false
                        AND g.id = (SELECT germplasm_id
                            FROM experiment.planting_instruction
                            WHERE entry_id = entry.id
                            AND is_void = false
                            LIMIT 1
                        )
                    LIMIT 1
                )
            `
            entryFinalSqlQuery = entryFinalSqlQuery.replace(`"germplasm"."designation"`, designationQuery)
        }

        // Retrieve the entry records
        let entries = await sequelize
            .query(entryFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
            })
      
        if (entries == undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        } else if (entries.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })

            return
        } else {
            // Get the final count of the entry records
            let entryCountFinalSqlQuery = await processQueryHelper
                .getCountFinalSqlQuery(
                    entryQuery,
                    conditionString,
                    orderString,
                    addedDistinctString
                )
            
            // Check if female/male count field is included
            entryCountFinalSqlQuery = entryCountFinalSqlQuery.replace('"femaleCountField"', femaleCountQuery)
            entryCountFinalSqlQuery = entryCountFinalSqlQuery.replace('"maleCountField"', maleCountQuery)

            let entryCount = await sequelize
                .query(entryCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
            })

            if (entryCount == undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                
                return
            } else if (entryCount.length < 1) {
                res.send(200, {
                    rows: [],
                    count: 0
                })

                return
            }
        
            count = entryCount[0].count

            res.send(200, {
                rows: entries,
                count: count
            })

            return
        }
    }
}