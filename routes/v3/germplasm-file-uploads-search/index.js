/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let params = req.query
        let count = 0

        let requestBody

        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let addedOrderString = `
            ORDER BY file_upload.id
        `

        let excludeParametersArray = ['fields','distinctOn']
        let searchParams = {}
        searchParams['distinctOn'] = ''

        // set fields if body.fields is not undefined / empty
        if(req.body != undefined){
            requestBody = req.body

            if(req.body.fields != null){
                searchParams['fields'] = req.body.fields
            }

            // set distinctOn if body.distinctOn is not undefined / empty
            if(req.body.distinctOn != undefined){
                searchParams['distinctOn'] = req.body.distinctOn

                addedDistinctString = await processQueryHelper.getDistinctString(
                    searchParams['distinctOn']
                )

                searchParams['distinctOn'] = `"${searchParams['distinctOn']}"`
                addedOrderString = `
                    ORDER BY ${searchParams['distinctOn']}
                `
            }
            
            // Get the filter condition/s
            conditionString = await processQueryHelper.getFilter(
                requestBody,
                excludeParametersArray
            )

            if(conditionString.includes('invalid')){
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build base query
        let fileUploadQuery = null
        
        if(searchParams['fields'] != null){

            if(searchParams['distinctOn']){
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    searchParams['fields']
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                fileUploadQuery = knex.select(selectString)
            }
            else{
                fileUploadQuery = knex.column(searchParams['fields'].split('|'))
            }

            fileUploadQuery += `
                FROM
                    germplasm.file_upload file_upload
                LEFT JOIN
                    dictionary.entity entity ON entity.id = file_upload.entity_id
                WHERE
                    file_upload.is_void = FALSE 
                ${addedOrderString}
            `
        }
        else{
            fileUploadQuery = `
                SELECT
                    ${addedDistinctString}
                    file_upload.id AS "germplasmFileUploadDbId",
                    file_upload.program_id AS "programDbId",
                    (
                        SELECT
                            program.program_code AS "programCode"
                        FROM
                            tenant.program program
                        WHERE
                            program.is_void = FALSE AND
                            program.id = file_upload.program_id
                    ),
                    file_upload.file_name AS "fileName",
                    file_upload.file_data AS "fileData",
                    file_upload.file_status AS "fileStatus",
                    file_upload.file_upload_origin AS "fileUploadOrigin",
                    file_upload.file_upload_action AS "fileUploadAction",
                    file_upload.germplasm_count AS "germplasmCount",
                    file_upload.seed_count AS "seedCount",
                    file_upload.package_count AS "packageCount",
                    file_upload.remarks AS "remarks",
                    file_upload.error_log AS "errorLog",
					(
						SELECT
							person.person_name AS "uploader"
						FROM
							tenant.person person
						WHERE
							person.is_void = FALSE AND
							person.id = file_upload.creator_id
					),
                    file_upload.creation_timestamp AS "uploadTimestamp",
                    (
						SELECT
							person.person_name AS "modifier"
						FROM
							tenant.person person
						WHERE
							person.is_void = FALSE AND
							person.id = file_upload.modifier_id
					),
					file_upload.modification_timestamp AS "modificationTimestamp",
                    entity.id AS "entityDbId",
                    entity.abbrev AS "entity"
                FROM
                    germplasm.file_upload file_upload
                LEFT JOIN
                    dictionary.entity entity ON entity.id = file_upload.entity_id
                WHERE
                    file_upload.is_void = FALSE
                ${addedOrderString}
            `
        
            // Parse sort parameters
            if(sort != null) {
                orderString = await processQueryHelper.getOrderString(sort)

                if(orderString.includes('invalid')){
                    let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }
        }

        try{

            // Generate the final SQL query
            let fileUploadFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
                fileUploadQuery,
                conditionString,
                orderString,
            )

            let fileUploads = await sequelize.query(
                fileUploadFinalSqlQuery, 
                {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
                return
            })

            if ( await fileUploads == undefined || 
                await fileUploads.length < 1 ) {
                    res.send(200, {
                        rows: [],
                        count: 0
                    })
                    return
            }

            let fileUploadsCountSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                fileUploadQuery,
                conditionString,
                orderString,
            )

            let fileUploadsCount = await sequelize.query(
                fileUploadsCountSqlQuery,
                {
                    type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            count = fileUploadsCount[0].count
            res.send(200, {
                rows: fileUploads,
                count: count,
            })
            return
        }
        catch(error){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}