/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../helpers/person/validator.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')
const logger = require('../../../helpers/logger')
const endpoint = 'cross-parents'

module.exports = {

    // Endpoint for creating a new cross-parent record
    // POST /v3/cross-parents
    post: async function (req, res, next) {

        let resultArray = []
        let crossParentDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let crossParentUrlString = (isSecure ? 'https' : 'http')
                + "://"
                + req.headers.host
                + "/v3/cross-parents"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        try {
            let crossParentValuesArray = []

            for (let record of records) {
                let validateQuery = ``
                let validateCount = 0

                let crossDbId = null
                let germplasmDbId = null
                let seedDbId = null
                let parentRole = null
                let orderNumber = null
                let experimentDbId = null
                let entryDbId = null
                let plotDbId = null
                let notes = null
                let occurrenceDbId = null

                // Check if the required columns are in the input
                if (
                    !record.crossDbId || !record.germplasmDbId || 
                    !record.parentRole || !record.orderNumber
                ) {
                    let errMsg = `Required parameters are missing. Ensure that the crossDbId,
                        germplasmDbId, parentRole, and orderNumber fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */

                crossDbId = record.crossDbId
                germplasmDbId = record.germplasmDbId
                parentRole = record.parentRole
                orderNumber = record.orderNumber

                // database validation for cross ID
                if (!validator.isInt(crossDbId)) {
                    let errMsg = `Invalid format, cross ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if cross is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            germplasm.cross "cross"
                        WHERE 
                            "cross".is_void = FALSE AND
                            "cross".id = ${crossDbId}
                    )
                `
                validateCount += 1

                // database validation for germplasm ID
                if (!validator.isInt(germplasmDbId)) {
                    let errMsg = `Invalid format, germplasm ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if germplasm is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            germplasm.germplasm germplasm
                        WHERE 
                            germplasm.is_void = FALSE AND
                            germplasm.id = ${germplasmDbId}
                    )
                `
                validateCount += 1

                // validation for parent role

                let validParentRoleValues = ['female', 'male', 'female-and-male']

                // Validate parent role
                if(!validParentRoleValues.includes(parentRole.toLowerCase())) {
                    let errMsg = `Invalid request, you have provided an invalid
                        value for parent role.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // validation for orderNumber
                if (!validator.isInt(orderNumber)) {
                    let errMsg = `Invalid format, order number must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of non-required parameters and set values */

                if (record.seedDbId !== undefined) {
                    seedDbId = record.seedDbId

                    if (!validator.isInt(seedDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400236)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate seed ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if seed is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.seed seed
                            WHERE 
                                seed.is_void = FALSE AND
                                seed.id = ${seedDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.experimentDbId !== undefined) {
                    experimentDbId = record.experimentDbId

                    if (!validator.isInt(experimentDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400025)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate experiment ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.experiment experiment
                            WHERE 
                                experiment.is_void = FALSE AND
                                experiment.id = ${experimentDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.entryDbId !== undefined) {
                    entryDbId = record.entryDbId

                    if (!validator.isInt(entryDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400020)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate entry ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry entry
                            WHERE 
                                entry.is_void = FALSE AND
                                entry.id = ${entryDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.plotDbId !== undefined) {
                    plotDbId = record.plotDbId

                    if (!validator.isInt(plotDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400038)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate plot ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if plot is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.plot plot
                            WHERE 
                                plot.is_void = FALSE AND
                                plot.id = ${plotDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.occurrenceDbId !== undefined) {
                    occurrenceDbId = record.occurrenceDbId

                    if (!validator.isInt(occurrenceDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate occurrence ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if occurrence is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.occurrence occ
                            WHERE 
                                occ.is_void = FALSE AND
                                occ.id = ${occurrenceDbId}
                        )
                    `
                    validateCount += 1
                }

                notes = (record.notes !== undefined) ? 
                    record.notes.trim() : null

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [
                    crossDbId, germplasmDbId, seedDbId, parentRole, orderNumber,
                    experimentDbId, entryDbId, plotDbId, personDbId, notes, occurrenceDbId
                ]

                crossParentValuesArray.push(tempArray)
            }

            // Create cross parent record/s
            let crossParentsQuery = format(`
                INSERT INTO
                    germplasm.cross_parent (
                        cross_id, germplasm_id, seed_id, parent_role, order_number,
                        experiment_id, entry_id, plot_id, creator_id, notes, occurrence_id
                    )
                VALUES
                    %L
                RETURNING id`, crossParentValuesArray
            )
            
            let crossParents = await sequelize.transaction(async transaction => {
                return await sequelize.query(crossParentsQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (let crossParent of crossParents[0]) {
                crossParentDbId = crossParent.id

                // Return the package info
                let array = {
                    crossParentDbId: crossParentDbId,
                    recordCount: 1,
                    href: crossParentUrlString + '/' + crossParentDbId
                }
                resultArray.push(array)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}