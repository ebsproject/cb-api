/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
const endpoint = 'cross-parents/:id'

module.exports = {
  
  // Endpoint for updating an existing cross-parent record
  // PUT /v3/cross-parents/:id
  put: async function(req, res, next) {

    // Retrieve the cross parent ID
    let crossParentDbId = req.params.id

    if(!validator.isInt(crossParentDbId)) {
      let errMsg = `Invalid format, cross parent ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Set defaults
    let crossDbId = null
    let germplasmDbId = null
    let seedDbId = null
    let parentRole = null
    let orderNumber = null
    let entryDbId = null
    let plotDbId = null
    let occurrenceDbId = null

    // Check is req.body has parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Retrieve the ID of the client via the access token
    let personDbId = await tokenHelper.getUserId(req, res)

    // If personDbId is undefined, terminate
    if (personDbId === undefined) {
        return
    }
    // If personDbId is null, return 401: User not found error
    else if (personDbId === null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(personDbId)

    // Check if cross is existing
    // Build query
    let crossParentQuery = `
      SELECT
        "cross".id AS "crossDbId",
        germplasm.id AS "germplasmDbId",
        seed.id AS "seedDbId",
        crossParent.parent_role AS "parentRole",
        crossParent.order_number AS "orderNumber",
        experiment.id AS "experimentDbId",
        entry.id AS "entryDbId",
        plot.id AS "plotDbId",
        occ.id AS "occurrenceDbId",
        creator.id AS "creatorDbId"
      FROM
        germplasm.cross_parent crossParent
        LEFT JOIN
          germplasm.cross "cross" ON "cross".id = crossParent.cross_id
        LEFT JOIN
          germplasm.germplasm germplasm ON germplasm.id = crossParent.germplasm_id
        LEFT JOIN
          germplasm.seed seed ON seed.id = crossParent.seed_id
        LEFT JOIN
          experiment.experiment experiment ON experiment.id = crossParent.experiment_id
        LEFT JOIN
          experiment.entry entry ON entry.id = crossParent.entry_id
        LEFT JOIN
          experiment.plot plot ON plot.id = crossParent.plot_id
        LEFT JOIN
          tenant.person creator ON creator.id = crossParent.creator_id
        LEFT JOIN
          experiment.occurrence occ ON occ.id = crossParent.occurrence_id
      WHERE
        crossParent.is_void = FALSE AND
        crossParent.id = ${crossParentDbId}
    `

    // Retrieve cross parent from the database
    let crossParent = await sequelize.query(crossParentQuery, {
      type: sequelize.QueryTypes.SELECT,
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (crossParent === undefined || crossParent.length < 1) {
      let errMsg = `The cross parent you have requested does not exist.`
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Get values from database, record = the current value in the database
    let recordCreatorDbId = crossParent[0].creatorDbId
    let recordCrossDbId = crossParent[0].crossDbId
    let recordGermplasmDbId = crossParent[0].germplasmDbId
    let recordSeedDbId = crossParent[0].seedDbId
    let recordParentRole = crossParent[0].parentRole
    let recordOrderNumber = crossParent[0].orderNumber
    let recordExperimentDbId = crossParent[0].experimentDbId
    let recordEntryDbId = crossParent[0].entryDbId
    let recordPlotDbId = crossParent[0].plotDbId
    let recordOccurrenceDbId = crossParent[0].occurrenceDbId

    // Check if user is an admin or an owner of the cross
    if(!isAdmin && (personDbId != recordCreatorDbId)) {
      let programMemberQuery = `
        SELECT 
          person.id,
          person.person_role_id AS "role"
        FROM 
          tenant.person person
          LEFT JOIN 
            tenant.team_member teamMember ON teamMember.person_id = person.id
          LEFT JOIN 
            tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
          LEFT JOIN 
            tenant.program program ON program.id = programTeam.program_id 
          LEFT JOIN
            experiment.experiment experiment ON experiment.program_id = program.id
        WHERE 
          experiment.id = ${recordExperimentDbId} AND
          person.id = ${personDbId} AND
          person.is_void = FALSE
      `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (person[0].id === undefined || person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false

        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
    }

    let isSecure = forwarded(req, req.headers).secure

    // Set the URL for response
    let crossParentUrlString = (isSecure ? 'https' : 'http')
      + "://"
      + req.headers.host
      + "/v3/cross-parents/"
      + crossParentDbId

    // Parse the input
    let data = req.body

    try {
      let setQuery = ``
      let validateQuery = ``
      let validateCount = 0

      /** Validation of parameters and set values **/

      if (data.crossDbId !== undefined) {
        crossDbId = data.crossDbId

        if (!validator.isInt(crossDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400019)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user input is same with the current value in the database
        if (crossDbId != recordCrossDbId) {

          // Validate cross ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if cross is existing return 1
              SELECT 
                count(1)
              FROM 
                germplasm.cross "cross"
              WHERE 
                "cross".is_void = FALSE AND
                "cross".id = ${crossDbId}
            )
          `
          validateCount += 1

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            cross_id = $$${crossDbId}$$
          `
        }
      }

      if (data.germplasmDbId !== undefined) {
        germplasmDbId = data.germplasmDbId

        if (!validator.isInt(germplasmDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400238)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user input is same with the current value in the database
        if (germplasmDbId != recordGermplasmDbId) {

          // Validate germplasm ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if germplasm is existing return 1
              SELECT 
                count(1)
              FROM 
                germplasm.germplasm germplasm
              WHERE 
                germplasm.is_void = FALSE AND
                germplasm.id = ${germplasmDbId}
            )
          `
          validateCount += 1

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            germplasm_id = $$${germplasmDbId}$$
          `
        }
      }

      if (data.seedDbId !== undefined) {
        seedDbId = data.seedDbId

        if (!validator.isInt(seedDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400236)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user input is same with the current value in the database
        if (seedDbId != recordSeedDbId) {

          // Validate seed ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if seed is existing return 1
              SELECT 
                count(1)
              FROM 
                germplasm.seed seed
              WHERE 
                seed.is_void = FALSE AND
                seed.id = ${seedDbId}
            )
          `
          validateCount += 1

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            seed_id = $$${seedDbId}$$
          `
        }
      }

      if (data.parentRole !== undefined) {
        parentRole = data.parentRole

        // Check if user input is same with the current value in the database
        if (parentRole != recordParentRole) {

          let validParentRoleValues = ['female', 'male']

          // Validate parent role
          if(!validParentRoleValues.includes(parentRole.toLowerCase())) {
            let errMsg = `Invalid request, you have provided an invalid
              value for parent role.`
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            parent_role = $$${parentRole}$$
          `
        }
      }

      if (data.orderNumber !== undefined) {
        orderNumber = data.orderNumber

        if (!validator.isInt(orderNumber)) {
          let errMsg = `Invalid format, order number must be an integer.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user input is same with the current value in the database
        if (orderNumber != recordOrderNumber) {
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            order_number = $$${orderNumber}$$
          `
        }
      }

      if (data.entryDbId !== undefined) {
        entryDbId = data.entryDbId

        if (!validator.isInt(entryDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400020)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user input is same with the current value in the database
        if (entryDbId != recordEntryDbId) {

          // Validate entry ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if entry is existing return 1
              SELECT 
                count(1)
              FROM 
                experiment.entry entry
              WHERE 
                entry.is_void = FALSE AND
                entry.id = ${entryDbId}
            )
          `
          validateCount += 1

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            entry_id = $$${entryDbId}$$
          `
        }
      }

      if (data.plotDbId !== undefined) {
        plotDbId = data.plotDbId

        if (!validator.isInt(plotDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400038)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user input is same with the current value in the database
        if (plotDbId != recordPlotDbId) {

          // Validate plot ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if plot is existing return 1
              SELECT 
                count(1)
              FROM 
                experiment.plot plot
              WHERE 
                plot.is_void = FALSE AND
                plot.id = ${plotDbId}
            )
          `
          validateCount += 1

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            plot_id = $$${plotDbId}$$
          `
        }
      }

      if (data.occurrenceDbId !== undefined) {
        occurrenceDbId = data.occurrenceDbId

        if (!validator.isInt(occurrenceDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400241)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user input is same with the current value in the database
        if (occurrenceDbId != recordOccurrenceDbId) {

          // Validate occurrence ID
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if occurrence is existing return 1
              SELECT 
                count(1)
              FROM 
                experiment.occurrence occ
              WHERE 
                occ.is_void = FALSE AND
                occ.id = ${occurrenceDbId}
            )
          `
          validateCount += 1

          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            occurrence_id = $$${occurrenceDbId}$$
          `
        }
      }

      /** Validation of input in database */
      // count must be equal to validateCount if all conditions are met
      if (validateCount > 0) {
        let checkInputQuery = `
          SELECT
            ${validateQuery}
          AS count
        `
        
        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0]['count'] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
      }

      if (setQuery.length > 0) setQuery += `,`

      // Update the cross record
      let updateCrossParentQuery = `
        UPDATE
          germplasm.cross_parent
        SET
          ${setQuery}
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          id = ${crossParentDbId}
      `
      await sequelize.transaction(async transaction => {
          await sequelize.query(updateCrossParentQuery, {
          type: sequelize.QueryTypes.UPDATE,
          transaction: transaction,
          raw: true
        }).catch(async err => {
          logger.logFailingQuery(endpoint, 'UPDATE', err)
          throw new Error(err)
        })
      })

      // Return the transaction info
      let resultArray = {
        crossParentDbId: crossParentDbId,
        recordCount: 1,
        href: crossParentUrlString
      }

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      logger.logMessage(__filename, err, 'error')
      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      if (!res.headersSent) res.send(new errors.InternalError(errMsg))
      return
    }
  },

  // Endpoint for deleting an existing cross-parent record
  // DELETE /v3/cross-parents/:id
  delete: async function(req, res, next) {
    // Retrieve the cross parent ID
    let crossParentDbId = req.params.id

    if(!validator.isInt(crossParentDbId)) {
      let errMsg = `Invalid format, cross parent ID must be an integer.`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let isAdmin = await userValidator.isAdmin(personDbId)

    try {

      let crossParentQuery = `
        SELECT
          creator.id AS "creatorDbId",
          experiment.id AS "experimentDbId"
        FROM
          germplasm.cross_parent crossParent
          LEFT JOIN
            tenant.person creator ON creator.id = crossParent.creator_id
          LEFT JOIN
            experiment.experiment experiment ON experiment.id = crossParent.experiment_id
        WHERE
          crossParent.is_void = FALSE AND
          crossParent.id = ${crossParentDbId}
      `

      let crossParent = await sequelize.query(crossParentQuery, {
        type: sequelize.QueryTypes.SELECT
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      if (crossParent == undefined || crossParent.length < 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404005)
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      let recordCreatorDbId = crossParent[0].creatorDbId
      let recordExperimentDbId = crossParent[0].experimentDbId

      // Check if user is an admin or an owner of the cross
      if(!isAdmin && (personDbId != recordCreatorDbId)) {
        let programMemberQuery = `
          SELECT 
            person.id,
            person.person_role_id AS "role"
          FROM 
            tenant.person person
            LEFT JOIN 
              tenant.team_member teamMember ON teamMember.person_id = person.id
            LEFT JOIN 
              tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
            LEFT JOIN 
              tenant.program program ON program.id = programTeam.program_id 
            LEFT JOIN
              experiment.experiment experiment ON experiment.program_id = program.id
          WHERE 
            experiment.id = ${recordExperimentDbId} AND
            person.id = ${personDbId} AND
            person.is_void = FALSE
        `

        let person = await sequelize.query(programMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
    
        if (await person[0].id === undefined || await person[0].role === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401025)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
    
        // Checks if user is a program team member or has a producer role
        let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        let isProducer = (person[0].role == '26') ? true : false
    
        if (!isProgramTeamMember && !isProducer) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401026)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
      }

      let deleteCrossParentQuery = format(`
        UPDATE
          germplasm.cross_parent crossParent
        SET
          is_void = TRUE,
          notes = CONCAT('VOIDED-', $$${crossParentDbId}$$),
          modification_timestamp = NOW(),
          modifier_id = ${personDbId}
        WHERE
          crossParent.id = ${crossParentDbId}
      `)

      await sequelize.transaction(async transaction => {
          await sequelize.query(deleteCrossParentQuery, {
          type: sequelize.QueryTypes.UPDATE,
          transaction: transaction,
          raw: true
        }).catch(async err => {
          logger.logFailingQuery(endpoint, 'DELETE', err)
          throw new Error(err)
        })
      })

      res.send(200, {
        rows: { crossParentDbId: crossParentDbId }
      })
      return
    } catch (err) {
      logger.logMessage(__filename, err, 'error')
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      if (!res.headersSent) res.send(new errors.InternalError(errMsg))
      return
    }
  }
}