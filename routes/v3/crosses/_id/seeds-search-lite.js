/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving seeds of a cross
    // POST /v3/crosses/:id/seeds-search-lite
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let distinctString = ''
        let distinctOn = null
        let parameters = {}
        parameters['distinctOn'] = ''

        let crossDbId = req.params.id

        let crossQuery = `
            SELECT
                "cross".id AS "crossDbId",
                "cross".cross_name AS "crossName",
                "cross".creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                "cross".modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier"
            FROM
                germplasm.cross "cross"
            LEFT JOIN
                tenant.person creator ON "cross".creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON "cross".modifier_id = modifier.id
            WHERE
                "cross".is_void = FALSE AND
                "cross".id = ${crossDbId}
        `
        
        // Retrieve cross from the database
        let cross = await sequelize.query(crossQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        if (await cross === undefined || await cross.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404004)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                distinctOn = req.body.distinctOn
                distinctString = await processQueryHelper.getDistinctString(distinctOn)
            }

            // Set the excluded parameters
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the seeds retrieval query
        let seedsQuery = null

        // Check if the client specified values for fields
        if (parameters['fields'] != null) {
            seedsQuery = knex.column(parameters['fields'].split('|'))

            seedsQuery += `
                FROM
                    germplasm.seed seed
                JOIN
                    germplasm.germplasm germplasm ON seed.germplasm_id = germplasm.id AND germplasm.is_void = FALSE
                LEFT JOIN
                    tenant.person creator ON seed.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON seed.modifier_id = modifier.id
                WHERE
                    seed.is_void = FALSE AND
                    seed.cross_id = ${crossDbId}
                ORDER BY
                    seed.id
            `
        } else {
            seedsQuery = `
                SELECT
                    seed.id AS "seedDbId",
                    seed.seed_code AS "seedCode",
                    seed.seed_name AS "seedName",
                    seed.harvest_date AS "harvestDate",
                    seed.harvest_method AS "harvestMethod",
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation AS "germplasmName",
                    seed.source_entry_id AS "sourceEntryDbId",
                    seed.source_occurrence_id AS "sourceOccurrenceDbId",
                    seed.source_location_id AS "sourceLocationDbId",
                    seed.source_plot_id AS "sourcePlotDbId",
                    seed.cross_id AS "crossDbId",
                    seed.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    seed.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    germplasm.seed seed
                JOIN
                    germplasm.germplasm germplasm ON seed.germplasm_id = germplasm.id AND germplasm.is_void = FALSE
                LEFT JOIN
                    tenant.person creator ON seed.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON seed.modifier_id = modifier.id
                WHERE
                    seed.is_void = FALSE AND
                    seed.cross_id = ${crossDbId}
                ORDER BY
                    seed.id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let seedsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            seedsQuery,
            conditionString,
            orderString,
            distinctString
        )

        let seeds = await sequelize.query(seedsFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset,
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        cross[0]['seeds'] = seeds

        let seedsCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                seedsQuery,
                conditionString,
                orderString,
                distinctString
            )

        let seedsCount = await sequelize.query(seedsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        count = seedsCount[0].count

        res.send(200, {
            rows: cross,
            count: count
        })
        return
    }
}