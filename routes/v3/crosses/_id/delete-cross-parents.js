/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let format = require('pg-format')

module.exports = {

    /**
     * POST /v3/crosses/:id/delete-cross-parents allows the deletion of all cross parents 
     * under a specific cross given the cross ID
     * 
     * @param crossDbId integer as a path parameter 
     * @param token string token
     * 
     * @return object response data
     */

    post: async function (req, res, next) {
        
        // Retrieve plot ID
        let crossDbId = req.params.id

        if (!validator.isInt(crossDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)
        
        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })
            
            let crossQuery = `
                SELECT
                    experiment.id AS "experimentDbId",
                    creator.id AS "creatorDbId"
                FROM
                    germplasm.cross "cross"
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = "cross".experiment_id
                LEFT JOIN
                    tenant.person creator ON creator.id = "cross".creator_id
                WHERE
                    "cross".is_void = FALSE AND
                    "cross".id = ${crossDbId}
            `
            
            // Retrieve cross from the database
            let cross = await sequelize.query(crossQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
            
            if (await cross === undefined || await cross.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404004)
                res.send(new errors.NotFoundError(errMsg))
                return
            }
            
            let recordExperimentDbId = cross[0].experimentDbId
            let recordCreatorDbId = cross[0].creatorDbId

            // Check if user is an admin or an owner of the plot
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    WHERE 
                        experiment.id = ${recordExperimentDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `
        
                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let crossParentQuery = `
                SELECT 
                    id
                FROM
                    germplasm.cross_parent
                WHERE
                    is_void = FALSE AND
                    cross_id = ${crossDbId}
                ORDER BY
                    id
            `

            let crossParentRecords = await sequelize.query(crossParentQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            let resultArray = []
            let crossParentObj = {}

            if (crossParentRecords.length != 0) {
                let deleteCrossParentQuery = format(`
                    UPDATE
                        germplasm.cross_parent
                    SET
                        is_void = TRUE,
                        notes = 'VOIDED',
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE
                        cross_id = ${crossDbId}
                `)

                await sequelize.query(deleteCrossParentQuery, {
                    type: sequelize.QueryTypes.UPDATE
                })

                await transaction.commit()

                for (crossParent of crossParentRecords) {
                    crossParentObj = {
                        crossParentDbId: crossParent.id
                    }
                    resultArray.push(crossParentObj)
                }
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}