/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let harvestStatusHelper = require('../../../../helpers/harvestStatus/index.js')
let variableHelper = require('../../../../helpers/variable/index')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
const logger = require('../../../../helpers/logger/index.js')
const endpoint = 'crosses/:id'

module.exports = {

    // Endpoint for retrieving a cross record
    // GET /v3/crosses/:id
    get: async function (req, res, next) {

        // Retrieve the cross ID
        let crossDbId = req.params.id

        if (!validator.isInt(crossDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400019)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        // Retrieve the user ID of the client from the access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let crossQuery = `
            SELECT
                "germplasmCross".id AS "crossDbId",
                "germplasmCross".cross_name AS "crossName",
                "germplasmCross".cross_method AS "crossMethod",
                "germplasmCross".germplasm_id AS "germplasmDbId",
                germplasm.designation AS "germplasmDesignation",
                germplasm.parentage AS "germplasmParentage",
                germplasm.generation AS "germplasmGeneration",
                seed.id AS "seedDbId",
                seed.seed_code AS "seedCode",
                seed.seed_name AS "seedName",
                entry.id AS "entryDbId",
                entry.entry_number AS "entryNo",
                entry.entry_name AS "entryName",
                entry.entry_role AS "entryRole",
                "entryList".id AS "entryListDbId",
                "entryList".entry_list_name AS "entryListName",
                "entryList".entry_list_code AS "entryListCode",
                experiment.id AS "experimentDbId",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_code AS "experimentCode",
                experiment.data_process_id AS "experimentDataProcessId",
                "germplasmCross".occurrence_id AS "occurrenceDbId",
                (
                    SELECT
                        item.display_name
                    FROM
                        master.item item
                    WHERE
                        item.is_void = FALSE AND
                        item.id = experiment.data_process_id
                ) AS "experimentTemplate",
                "parentDbId",
                "germplasmCross".is_method_autofilled AS "isMethodAutofilled",
                "germplasmCross".harvest_status AS "harvestStatus",
                "germplasmCross".remarks,
                "germplasmCross".creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                "germplasmCross".modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                germplasm.cross "germplasmCross"
            LEFT JOIN
                experiment.entry entry ON "germplasmCross".entry_id = entry.id
            LEFT JOIN
                experiment.entry_list "entryList" ON "germplasmCross".entry_list_id = "entryList".id AND "entryList".is_void = FALSE
            LEFT JOIN
                germplasm.germplasm germplasm ON "germplasmCross".germplasm_id = germplasm.id
            LEFT JOIN
                germplasm.seed seed ON "germplasmCross".seed_id = seed.id
            LEFT JOIN
                experiment.experiment experiment ON "germplasmCross".experiment_id = experiment.id
            LEFT JOIN
                tenant.person creator ON "germplasmCross".creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON "germplasmCross".modifier_id = modifier.id
            WHERE
                "germplasmCross".is_void = false
                AND "germplasmCross".id = :crossDbId
        `

        crossQuery = crossQuery.replace('"parentDbId"', `
            (
                SELECT array_to_string(array_agg(entry_id order by order_number) ,',') 
                FROM germplasm.cross_parent
                WHERE cross_id = "germplasmCross".id
            ) AS "parentDbId"
        `)

        // Retrieve cross from the database   
        let crosses = await sequelize.query(crossQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                crossDbId: crossDbId
            }
        })
            .catch(async err => {
                logger.logFailingQuery(endpoint, 'SELECT', err.stack)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        if (await crosses === undefined) return

        // Return error if resource is not found
        if (await crosses == null || await crosses.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404004)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        res.send(200, {
            rows: crosses
        })
        return
        
    },

    // Endpoint for updating a cross
    // PUT /v3/crosses/:id
    put: async function (req, res, next) {

        // Retrieve the cross ID
        let crossDbId = req.params.id

        if (!validator.isInt(crossDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400019)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let crossName = null
        let crossMethod = null
        let germplasmDbId = null
        let seedDbId = null
        let entryDbId = null
        let remarks = null
        let harvestStatus = null
        let entryListDbId = null
        let validateStatus = false
        let validateStatusValues = [ 'true', 'false' ]
        let occurrenceDbId = null

        // Retrieve the user ID of the client from the access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }
        // If personDbId is null, return error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if cross is existing
        // Build query
        let crossQuery = `
            SELECT
                "cross".cross_name AS "crossName",
                "cross".cross_method AS "crossMethod",
                "cross".harvest_status AS "harvestStatus",
                "cross".experiment_id AS "experimentDbId",
                entryList.id AS "entryListDbId",
                germplasm.id AS "germplasmDbId",
                seed.id AS "seedDbId",
                entry.id AS "entryDbId",
                creator.id AS "creatorDbId",
                "cross".remarks,
                "cross".occurrence_id AS "occurrenceDbId"
            FROM
                germplasm.cross "cross"
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = "cross".germplasm_id
            LEFT JOIN
                germplasm.seed seed ON seed.id = "cross".seed_id
            LEFT JOIN
                experiment.entry entry ON entry.id = "cross".entry_id
            LEFT JOIN
                tenant.person creator ON creator.id = "cross".creator_id
            LEFT JOIN
                experiment.entry_list entryList ON entryList.id = "cross".entry_list_id
            WHERE
                "cross".is_void = FALSE AND  
                entryList.is_void = FALSE AND
                "cross".id = ${crossDbId}
        `

        // Retrieve cross from the database   
        let cross = await sequelize.query(crossQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (cross === undefined || cross.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404004)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordCreatorDbId = cross[0].creatorDbId
        let recordCrossName = cross[0].crossName
        let recordCrossMethod = cross[0].crossMethod
        let recordHarvestStatus = cross[0].harvestStatus
        let recordExperimentDbId = cross[0].experimentDbId
        let recordGermplasmDbId = cross[0].germplasmDbId
        let recordSeedDbId = cross[0].seedDbId
        let recordEntryDbId = cross[0].entryDbId
        let recordRemarks = cross[0].remarks
        let recordEntryListDbId = cross[0].entryListDbId
        let recordOccurrenceDbId = cross[0].occurrenceDbId
        
        // // TEMPORARILY REMOVED IN SUPPORT OF CORB-5719
        // // (HM: Retrieve only occurrences and experiments where permission is "write")
        // // Removing this check allows collaborators (non-admin and non-owner of cross)
        // // to update the cross harvest status to "IN QUEUE" for harvesting.
        // // Check if user is an admin or an owner of the cross
        // if(!isAdmin && (personDbId != recordCreatorDbId)) {
        //     let programMemberQuery = `
        //         SELECT 
        //             person.id,
        //             role.person_role_code AS "role"
        //         FROM 
        //             tenant.person person
        //         LEFT JOIN
        //             tenant.person_role role ON role.id = person.person_role_id
        //         LEFT JOIN 
        //             tenant.team_member teamMember ON teamMember.person_id = person.id
        //         LEFT JOIN 
        //             tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
        //         LEFT JOIN 
        //             tenant.program program ON program.id = programTeam.program_id 
        //         LEFT JOIN
        //             experiment.experiment experiment ON experiment.program_id = program.id
        //         WHERE 
        //             experiment.id = ${recordExperimentDbId} AND
        //             person.id = ${personDbId} AND
        //             person.is_void = FALSE
        //     `

        //     let person = await sequelize.query(programMemberQuery, {
        //         type: sequelize.QueryTypes.SELECT
        //     })
            
        //     if (person[0].id === undefined || person[0].role === undefined) {
        //         let errMsg = await errorBuilder.getError(req.headers.host, 401025)
        //         res.send(new errors.UnauthorizedError(errMsg))
        //         return
        //     }
            
        //     // Checks if user is a program team member or has a producer role
        //     let isProgramTeamMember = (person[0].id == personDbId) ? true : false
        //     let isProducer = (person[0].role == 'DATA_PRODUCER') ? true : false
            
        //     if (!isProgramTeamMember && !isProducer) {
        //         let errMsg = await errorBuilder.getError(req.headers.host, 401026)
        //         res.send(new errors.UnauthorizedError(errMsg))
        //         return
        //     }
        // }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let crossUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/crosses/"
            + crossDbId

        // Parse the input
        let data = req.body    

        // Get validateStatus flag value
        if (data.validateStatus !== undefined) {
            // Check if validate status value is boolean
            if (!validateStatusValues.includes(data.validateStatus.toLowerCase())) {
                let errMsg = 'Invalid value for validateStatus. Accepted values are: true or false.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            validateStatus = JSON.parse(data.validateStatus.toLowerCase())
        }

        try {
            // Check flag for running the harvest status validation
            if (validateStatus) {
                // Perform validation and apply correct harvest status
                await harvestStatusHelper.validateCrossHarvestStatus(req, res, crossDbId)
            }
            else {
                let setQuery = ``
                let validateQuery = ``
                let validateCount = 0

                /** Validation of parameters and set values **/

                if (data.crossMethod !== undefined) {
                    crossMethod = data.crossMethod

                    // Check if user input is same with the current value in the database
                    if (crossMethod != recordCrossMethod) {
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if cross method is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    master.scale_value scaleValue
                                    LEFT JOIN
                                    master.variable variable ON scaleValue.scale_id = variable.scale_id
                                WHERE
                                    variable.abbrev LIKE 'CROSS_METHOD' AND
                                    scaleValue.value ILIKE $$${crossMethod}$$
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            cross_method = $$${crossMethod}$$
                        `
                    }          
                }

                if (data.germplasmDbId !== undefined) {
                    germplasmDbId = data.germplasmDbId

                    if (!validator.isInt(germplasmDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400238)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if user input is same with the current value in the database
                    if (germplasmDbId != recordGermplasmDbId) {

                        // Validate germplasm ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if germplasm is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    germplasm.germplasm germplasm
                                WHERE 
                                    germplasm.is_void = FALSE AND
                                    germplasm.id = ${germplasmDbId}
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            germplasm_id = $$${germplasmDbId}$$
                        `
                    }
                }

                if (data.seedDbId !== undefined) {
                    seedDbId = data.seedDbId

                    if (!validator.isInt(seedDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400236)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if user input is same with the current value in the database
                    if (seedDbId != recordSeedDbId) {

                        // Validate seed ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if seed is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    germplasm.seed seed
                                WHERE 
                                    seed.is_void = FALSE AND
                                    seed.id = ${seedDbId}
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            seed_id = $$${seedDbId}$$
                        `
                    }
                }

                if (data.entryDbId !== undefined) {
                    entryDbId = data.entryDbId

                    if (!validator.isInt(entryDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400020)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if user input is same with the current value in the database
                    if (entryDbId != recordEntryDbId) {

                        // Validate entry ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if entry is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    experiment.entry entry
                                WHERE 
                                    entry.is_void = FALSE AND
                                    entry.id = ${entryDbId}
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            entry_id = $$${entryDbId}$$
                        `
                    }
                }

                if (data.harvestStatus !== undefined) {
                    harvestStatus = data.harvestStatus

                    let validHarvestStatusValues = [
                        'NO_HARVEST',
                        'READY',
                        'IN_QUEUE',
                        'IN_PROGRESS',
                        'DONE',
                        'COMPLETED',
                        'FAILED',
                        'DELETION_IN_PROGRESS',
                        'REVERT_IN_PROGRESS'
                    ]

                    // retrieve HARVEST_STATUS scale values
                    let scaleValues = await variableHelper.getScaleValues('HARVEST_STATUS')

                    // set as validHarvestStatusValues
                    if(scaleValues){
                        validHarvestStatusValues = scaleValues
                    }

                    // Validate harvest status
                    if (!harvestStatus.startsWith('INCOMPLETE') && !validHarvestStatusValues.includes(harvestStatus.toUpperCase())) {
                        let errMsg = `Invalid request, you have provided an invalid value for harvest status.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
            
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        harvest_status = $$${harvestStatus}$$
                    `
                }

                if (data.entryListDbId !== undefined) {
                    entryListDbId = data.entryListDbId
                    if (!validator.isInt(entryListDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if user input is same with the current value in the database
                    if (entryListDbId != recordEntryListDbId) {

                        // Validate entryList ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if entryList is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    experiment.entry_list entryList
                                WHERE 
                                    entryList.is_void = FALSE AND
                                    entryList.id = ${entryListDbId}
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            entry_list_id = $$${entryListDbId}$$
                        `
                    }
                }

                if (data.occurrenceDbId !== undefined) {
                    occurrenceDbId = data.occurrenceDbId
                    if (!validator.isInt(occurrenceDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400229)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if user input is same with the current value in the database
                    if (occurrenceDbId != recordOccurrenceDbId) {

                        // Validate entryList ID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if occurrence is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    experiment.occurrence occurrence
                                WHERE 
                                    occurrence.is_void = FALSE AND
                                    occurrence.id = ${occurrenceDbId}
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            occurrence_id = $$${occurrenceDbId}$$
                        `
                    }
                }

                /** Set values of parameters without validation */
                if (data.crossName !== undefined) {
                    crossName = data.crossName

                    // Check if user input is same with the current value in the database
                    if (crossName != recordCrossName) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            cross_name = $$${crossName}$$
                        `
                    }
                }

                if (data.remarks !== undefined) {
                    remarks = data.remarks

                    // Check if user input is same with the current value in the database
                    if (remarks != recordRemarks) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            remarks = $$${remarks}$$
                        `
                    }
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                if (validateCount > 0) {
                    let checkInputQuery = `
                        SELECT
                            ${validateQuery}
                        AS count
                    `

                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (inputCount[0]['count'] != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (setQuery.length > 0) setQuery += `,`

                // Update the cross record
                let updateCrossQuery = `
                    UPDATE
                        germplasm.cross
                    SET
                        ${setQuery}
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE
                        id = ${crossDbId}
                `

                // Update cross record
                await sequelize.transaction(async transaction => {
                    return await sequelize.query(updateCrossQuery, {
                        type: sequelize.QueryTypes.UPDATE,
                        transaction: transaction,
                        raw: true
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'UPDATE', err)
                        throw new Error(err)
                    })
                })
            }

            // Return the transaction info
            let resultArray = {
                crossDbId: crossDbId,
                recordCount: 1,
                href: crossUrlString
            }

            if (!res.headersSent) {
                res.send(200, {
                    rows: resultArray
                })
                return
            }
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for deleting an existing cross record
    // DELETE /v3/crosses/:id
    delete: async function (req, res, next) {
        // Retrieve the cross ID
        let crossDbId = req.params.id

        if (!validator.isInt(crossDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400019)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve the user ID of the client from the access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let isAdmin = await userValidator.isAdmin(personDbId)

        try {

            let crossQuery = `
                SELECT
                    "cross".experiment_id AS "experimentDbId",
                    creator.id AS "creatorDbId"
                FROM
                    germplasm.cross "cross"
                LEFT JOIN
                    tenant.person creator ON creator.id = "cross".creator_id
                WHERE
                    "cross".is_void = FALSE AND
                    "cross".id = ${crossDbId}
            `

            let cross = await sequelize.query(crossQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (cross == undefined || cross.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404005)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = cross[0].creatorDbId
            let recordExperimentDbId = cross[0].experimentDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                        LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                        LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                        LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                        LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    WHERE 
                        experiment.id = ${recordExperimentDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteCrossQuery = format(`
                UPDATE
                    germplasm.cross "cross"
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${crossDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    "cross".id = ${crossDbId}
            `)

            await sequelize.transaction(async transaction => {
                await sequelize.query(deleteCrossQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            res.send(200, {
                rows: { crossDbId: crossDbId }
            })
            return

        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}