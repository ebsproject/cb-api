/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
const { sequelize } = require('../../../config/sequelize')
const errors = require('restify-errors')
const validator = require('validator')
const tokenHelper = require('../../../helpers/auth/token.js')
const crossesHelper = require('../../../helpers/crosses/index.js')
const seedlotHelper = require('../../../helpers/seedlot/index')

const forwarded = require('forwarded-for')
const format = require('pg-format')
const errorBuilder = require('../../../helpers/error-builder')
const logger = require('../../../helpers/logger')
const endpoint = 'crosses'

module.exports = {

    // Endpoint for adding a cross record
    post: async function (req, res, next) {

        let resultArray = []
        let crossDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let crossUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/crosses"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let records = data.records || []

        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {

            let crossValuesArray = []
            for (let record of records) {
                let validateQuery = ``
                let validateCount = 0

                let crossName = null
                let crossMethod = null
                let germplasmDbId = null
                let seedDbId = null
                let experimentDbId = null
                let entryDbId = null
                let isMethodAutofilled = false
                let entryListDbId = null
                let occurrenceDbId = null

                // Check if the required columns are in the input
                if (record.crossName == undefined || record.crossMethod == undefined) {
                    let errMsg = `Required parameters are missing. Ensure that the crossName and crossMethod fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                crossName = record.crossName
                crossMethod = record.crossMethod

                /** Validation of non-required parameters and set values */

                if (record.germplasmDbId !== undefined) {
                    germplasmDbId = record.germplasmDbId

                    if (!validator.isInt(germplasmDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400238)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate germplasm ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if germplasm is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.germplasm germplasm
                            WHERE 
                                germplasm.is_void = FALSE AND
                                germplasm.id = ${germplasmDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.seedDbId !== undefined) {
                    seedDbId = record.seedDbId

                    if (!validator.isInt(seedDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400236)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate seed ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if seed is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.seed seed
                            WHERE 
                                seed.is_void = FALSE AND
                                seed.id = ${seedDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.experimentDbId !== undefined) {
                    experimentDbId = record.experimentDbId

                    if (!validator.isInt(experimentDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400025)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate experiment ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.experiment experiment
                            WHERE 
                                experiment.is_void = FALSE AND
                                experiment.id = ${experimentDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.entryDbId !== undefined) {
                    entryDbId = record.entryDbId

                    if (!validator.isInt(entryDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400020)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate entry ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry entry
                            WHERE 
                                entry.is_void = FALSE AND
                                entry.id = ${entryDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.isMethodAutofilled !== undefined) {
                    isMethodAutofilled = record.isMethodAutofilled
                    if (typeof isMethodAutofilled !=='boolean') {
                        let errMsg = 'Invalid format, isMethodAutofilled must be boolean.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.entryListDbId !== undefined) {
                    entryListDbId = record.entryListDbId

                    if (!validator.isInt(entryListDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate entryList ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entryList is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry_list entryList
                            WHERE 
                                entryList.is_void = FALSE AND
                                entryList.id = ${entryListDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.occurrenceDbId !== undefined) {
                    occurrenceDbId = record.occurrenceDbId

                    if (!validator.isInt(occurrenceDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate entryList ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if occurrence is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.occurrence occurrence
                            WHERE 
                                occurrence.is_void = FALSE AND
                                occurrence.id = ${occurrenceDbId}
                        )
                    `
                    validateCount += 1
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check autofilled methods
                if(crossMethod == null || crossMethod == ''){
                    // crossMethod = await crossesHelper.getAutoFilledMethod(crossName, experimentDbId)
                    let parents = crossName.split('|')
                    let femaleParent = parents[0]
                    let maleParent = parents[1]
                    
                    let female = await crossesHelper.getGermplasmInfo(femaleParent)
                    let male = await crossesHelper.getGermplasmInfo(maleParent)


                    let experimentInfo = await crossesHelper.checkBreedingExperiment(experimentDbId)

                    if(experimentInfo.isHybrid){
                        crossMethod = 'hybrid formation'
                    }else if(experimentInfo.isBreeding){
                        let backcross = await seedlotHelper.findBackcrossRecurrentParent(female.germplasmDbId,male.germplasmDbId) !== null
                        let femaleParentage = female.parentage.toUpperCase()
                        let maleParentage = male.parentage.toUpperCase()
                        let isParentageUnknown = femaleParentage !== '?/?' && femaleParentage !== 'UNKNOWN' && maleParentage !== '?/?' && maleParentage !== 'UNKNOWN'

                        if(backcross){
                            crossMethod = isParentageUnknown ? 'backcross' : ''
                        }else if(female.germplasmState == 'fixed' && female.generation !== 'F1' && male.germplasmState == 'fixed' && male.generation !== 'F1'){
                            crossMethod = 'single cross'
                        }else if(female.germplasmState !== 'fixed' && female.generation == 'F1' && male.germplasmState !== 'fixed' && male.generation == 'F1'){
                            crossMethod = 'double cross'
                        }else if((female.germplasmState == 'fixed' && female.generation !== 'F1' && male.germplasmState !== 'fixed' && male.generation == 'F1') || 
                            (male.germplasmState == 'fixed' && male.generation !== 'F1' && female.germplasmState !== 'fixed' && female.generation == 'F1')){
                            crossMethod = experimentInfo.cropCode === 'WHEAT' ? 'top cross' : 'three-way cross'
                        }
                    }
                }

                // Get values
                let tempArray = [
                    crossName, crossMethod, germplasmDbId, seedDbId, experimentDbId,
                    entryDbId, personDbId, isMethodAutofilled, entryListDbId, occurrenceDbId
                ]

                crossValuesArray.push(tempArray)
            }

            // Create cross record
            let crossesQuery = format(`
                INSERT INTO 
                    germplasm.cross (
                        cross_name, cross_method, germplasm_id, seed_id, experiment_id,
                        entry_id, creator_id, is_method_autofilled, entry_list_id, occurrence_id
                    )
                VALUES 
                    %L 
                RETURNING id`, crossValuesArray
            )

            let crosses = await sequelize.transaction(async transaction => {
                return await sequelize.query(crossesQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (let cross of crosses[0]) {
                crossDbId = cross.id
                // Return the cross info
                let array = {
                    crossDbId: cross.id,
                    recordCount: 1,
                    href: crossUrlString + '/' + crossDbId
                }
                resultArray.push(array)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}