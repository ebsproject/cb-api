/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, Plot } = require('../../../config/sequelize')
let errors = require('restify-errors')
let responseHelper = require('../../../helpers/responses')
let tokenHelper = require('../../../helpers/auth/token.js')
let validator = require('validator')
var url = require('url')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    /**
    * Create record(s) in data_terminal.suppression_rules
    * POST /v3/suppression-rules
    * @param {*} req Request parameters
    * @param {*} res Response
    */
    post: async function (req, res) {

        let resultIdArray = []
        let creatorId = await tokenHelper.getUserId(req)
        let numberOperatorList = ['<', '>', '<=', '>=', '=', '<>']
        let textOperatorList = ['equals', 'not equals', 'contains', 'does not contain']
        let conjunctionList = ['and', 'or']
        let entityList = ['plot_data']
        let statusList = ['new', 'complete', 'selected', 'all']
        let transaction
        let transactionDbId
        let factorVariableDbId
        let targetVariableDbId
        let conjunction
        let operator
        let value
        let status
        let entity

        if (creatorId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Get the url
        var urlString = (req.connection && req.connection.encrypted ? 'https' : 'http')
            + "://"
            + req.headers.host
            + (url.parse(req.url)).pathname

        // Check for parameters
        if (req.body != undefined) {

            // Parse the input
            let data = req.body
            let records = []
            try {
                records = data.records

                if (records == undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            } catch (e) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            // Start transaction
            try {
                // get transaction
                transaction = await sequelize.transaction({ autocommit: false });
                let insertColumns
                let insertValues
                for (var item of records) {
                    insertColumns = []
                    insertValues = []

                    if (
                        item.transactionDbId === undefined
                        || item.factorVariableDbId === undefined
                        || item.targetVariableDbId === undefined
                        || item.conjunction === undefined
                        || item.operator === undefined
                        || item.value === undefined
                        || item.status === undefined
                        || item.entity === undefined
                    ) {
                        let errMsg = 'Required parameters are missing. Ensure that transactionDbId, targetVariableDbId, factorVariableDbId, conjunction, operator, value, status and entity are not empty.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    transactionDbId = item.transactionDbId
                    factorVariableDbId = item.factorVariableDbId
                    targetVariableDbId = item.targetVariableDbId
                    conjunction = item.conjunction
                    operator = item.operator
                    value = item.value
                    status = item.status
                    entity = item.entity

                    // Check if transaction ID exist
                    transactionQuery = `
                        SELECT exists(
                            SELECT 1
                            FROM 
                                data_terminal.transaction 
                            WHERE 
                                id = ${transactionDbId} 
                                AND is_void = FALSE
                        )
                    `
                    let transactionResult = await sequelize.query(transactionQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (!transactionResult[0]['exists']) {
                        let errMsg = 'The terminal transaction ID does not exist.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            404,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    // Check if target variable ID exist
                    variableQuery = `
                        SELECT exists(
                            SELECT 1
                            FROM 
                                master.variable
                            WHERE 
                                id = ${targetVariableDbId} 
                                AND is_void = FALSE
                        )
                    `
                    let variableResult = await sequelize.query(variableQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (!variableResult[0]['exists']) {
                        let errMsg = 'The target variable ID does not exist.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            404,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    // Check if factor variable ID exist
                    variableQuery = `
                        SELECT *
                        FROM 
                            master.variable
                        WHERE 
                            id = ${factorVariableDbId} 
                            AND is_void = FALSE
                    `
                    variableResult = await sequelize.query(variableQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (await variableResult == undefined || await variableResult.length == 0) {
                        let errMsg = 'The factor variable ID does not exist.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            404,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    // check if value is valid
                    if (variableResult[0]['data_type'] == 'integer' && !validator.isInt(value)) {
                        let errMsg = 'Invalid format, variableDbId with dataType of an integer must have an integer value.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (
                        (variableResult[0]['data_type'] == 'float' || variableResult[0]['data_type'] == 'double precision')
                        && !validator.isFloat(value)) {
                        let errMsg = 'Invalid format, variableDbId with dataType of a float or double precision must have a float value.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (variableResult[0]['data_type'] == 'boolean' && !validator.isBoolean(value)) {
                        let errMsg = 'Invalid format, variableDbId with dataType of a boolean must have an boolean value.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (variableResult[0]['data_type'] == 'date' && validator.toDate(value) == null) {
                        let errMsg = 'Invalid format, variableDbId with dataType of a date must be a valid date in format YYYY-MM-DD.'
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else if (variableResult[0]['data_type'] == 'date' && validator.toDate(value) != null) {

                        var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
                        if (!value.match(validDateFormat)) {  // Invalid format
                            let errMsg = 'Invalid format, variableDbId with dataType of a date must be in format YYYY-MM-DD.'
                            errorCode = await responseHelper.getErrorCodebyMessage(
                                400,
                                errMsg)
                            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }

                    // check for scale values
                    if (variableResult[0]['scale_id'] != null) {
                        scaleQuery = `
                            SELECT 
                                ARRAY_AGG(value)
                            FROM 
                                master.scale_value
                            WHERE
                                scale_id = ${variableResult[0]['scale_id']}
                                AND is_void = FALSE
                        `
                        let scaleResult = await sequelize.query(scaleQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                        if (scaleResult[0]['array_agg'] != null && scaleResult[0]['array_agg'].indexOf(value) == -1) {

                            let errMsg = 'The value is invalid. value should be one '
                                + 'of the following: ' + scaleResult[0]['array_agg'].toString()
                            errorCode = await responseHelper.getErrorCodebyMessage(
                                400,
                                errMsg)
                            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }

                    // check if operator is valid
                    if (
                        (
                            variableResult[0]['data_type'] == 'integer'
                            || variableResult[0]['data_type'] == 'float'
                            || variableResult[0]['data_type'] == 'double precision'
                            || variableResult[0]['data_type'] == 'date'
                            || variableResult[0]['data_type'] == 'time'
                        ) && numberOperatorList.indexOf(operator) == -1
                    ) {
                        let errMsg = 'The operator is invalid. Operator should be one '
                            + 'of the following: ' + numberOperatorList.toString()
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else if (
                        (
                            variableResult[0]['data_type'] == 'text'
                            || variableResult[0]['data_type'] == 'character varying'
                        ) && textOperatorList.indexOf(operator) == -1) {
                        let errMsg = 'The operator is invalid. Operator should be one '
                            + 'of the following: ' + textOperatorList.toString()
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // check if status is valid
                    if (statusList.indexOf(status) == -1) {
                        let errMsg = 'The status is invalid. Status should be one '
                            + 'of the following: ' + statusList.toString()
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // check if entity is valid
                    if (entityList.indexOf(entity) == -1) {
                        let errMsg = 'The entity is invalid. Entity should be one '
                            + 'of the following: ' + entityList.toString()
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // check if conjunction is valid
                    if (conjunctionList.indexOf(conjunction) == -1) {
                        let errMsg = 'The conjunction is invalid. Conjunction should be one '
                            + 'of the following: ' + conjunctionList.toString()
                        errorCode = await responseHelper.getErrorCodebyMessage(
                            400,
                            errMsg)
                        errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (item.remarks !== undefined) {
                        insertValues.push("'" + item.remarks + "'")
                        insertColumns.push("remarks")
                    }

                    if (item.entityDbId !== undefined) {
                        insertValues.push(item.entityDbId)
                        insertColumns.push("entity_id")
                    }

                    insertValues = insertValues.toString()
                    insertColumns = insertColumns.toString()

                    if (insertValues != '') {
                        insertValues = ',' + insertValues
                    }
                    if (insertValues != '') {
                        insertColumns = ',' + insertColumns
                    }
                    let insertQuery = `
                        INSERT INTO data_terminal.suppression_rule (
                            transaction_id, 
                            target_variable_id, 
                            factor_variable_id, 
                            conjunction, 
                            "operator", 
                            value, 
                            status, 
                            entity,
                            creator_id, 
                            creation_timestamp,
                            is_void
                            ${insertColumns}
                        )
                        SELECT
                            ${transactionDbId}, 
                            ${targetVariableDbId}, 
                            ${factorVariableDbId}, 
                            '${conjunction}', 
                            '${operator}', 
                            '${value}', 
                            '${status}', 
                            '${entity}',
                            ${creatorId},
                            NOW(),
                            FALSE
                            ${insertValues}
                        WHERE
                            NOT EXISTS(
                                SELECT 1 
                                FROM 
                                    data_terminal.suppression_rule
                                WHERE 
                                    target_variable_id = ${targetVariableDbId}
                                    AND factor_variable_id = ${factorVariableDbId}
                                    AND transaction_id = ${transactionDbId}
                                    AND entity= '${entity}'
                                    AND value= '${value}'
                                    AND operator= '${operator}'
                                    AND is_void = FALSE
                            )
                            RETURNING 
                                id
                    `
                    await sequelize.query(insertQuery, {
                        type: sequelize.QueryTypes.INSERT,
                        transaction: transaction
                    })
                        .catch(async err => {
                            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                            res.send(new errors.InternalError(errMsg))
                            return
                        })
                        .then(function (resultArray) {

                            if (resultArray[1] > 0) {
                                let resultDbId = resultArray[0][0]["id"]
                                let returnArray = {
                                    suppressionRuleDbId: resultDbId,
                                    href: urlString + "/" + resultDbId
                                }
                                resultIdArray.push(returnArray)
                            }
                        })
                }
                // Commit the transaction
                await transaction.commit()

                res.send(200, {
                    rows: resultIdArray
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            }
        }
    }
}