/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let responseHelper = require('../../../../helpers/responses')
let forwarded = require('forwarded-for')
let validator = require('validator')

module.exports = {

    /**
     * Update records in data_terminal.suppression_rules
     * PUT /v3/suppression-rules/:id
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    put: async function (req, res) {

        let suppressionRuleDbId = req.params.id
        let newRecord = req.body
        let transaction
        let setQuery = []
        // Set defaults
        let statusList = ['new', 'complete', 'selected', 'all']

        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let suppressionRuleUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/suppression-rules/'
            + suppressionRuleDbId

        // check if suppression rule ID is integer
        if (!validator.isInt(suppressionRuleDbId)) {
            let errMsg = 'Invalid format, suppression rule ID must be an integer'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if suppression rule ID exists
        let suppressionQuery = `
            SELECT 
                status,
                entity_id
            FROM 
                data_terminal.suppression_rule 
            WHERE 
                id = ${suppressionRuleDbId}
                AND is_void = FALSE
        `
        let ruleRecord = await sequelize.query(suppressionQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (await ruleRecord == undefined || await ruleRecord.length == 0) {
            let errMsg = 'The suppression rule ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        let status = ruleRecord[0]['status'];

        // check if status is valid
        if (
            newRecord.status !== undefined
            && (!newRecord.status || newRecord.status.trim().length === 0)
        ) {
            let errMsg = 'The status is invalid. Status should not be empty.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else if (
            newRecord.status !== undefined
            && statusList.indexOf(newRecord.status) == -1
        ) {
            let errMsg = 'The status is invalid. Status should be one '
                + 'of the following: ' + statusList.toString()
            errorCode = await responseHelper.getErrorCodebyMessage(
                400,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else if (
            newRecord.status !== undefined
            && newRecord.status !== status
        ) {
            setQuery.push(`status = '${newRecord.status}'`)
        }

        if (newRecord.remarks !== undefined) {
            setQuery.push(`remarks = '${newRecord.remarks}'`)
        }

        if (newRecord.entityDbId !== undefined && newRecord.entityDbId != null
            && newRecord.entityDbId !== ruleRecord[0]['entity_id']
        ) {
            setQuery.push(`entity_id = '${newRecord.entityDbId}'`)
        } else if (newRecord.entityDbId != null && newRecord.entityDbId.trim() == ""
            && newRecord.entityDbId !== ruleRecord[0]['entity_id']) {
            setQuery.push(`entity_id = '${newRecord.entityDbId}'`)
        } else if (newRecord.entityDbId == null && ruleRecord[0]['entity_id'] != null) {
            setQuery.push(`entity_id = NULL`)
        }

        try {
            transaction = await sequelize.transaction({ autocommit: false })
            if (setQuery.length > 0) {
                setQuery = setQuery.toString()
                let updateListQuery = `
                    UPDATE
                        data_terminal.suppression_rule
                    SET
                        ${setQuery},
                        modification_timestamp = NOW(),
                        modifier_id = ${userDbId}
                    WHERE
                        id = ${suppressionRuleDbId}
                `
                updateListQuery = updateListQuery.trim()

                await sequelize.query(updateListQuery, {
                    type: sequelize.QueryTypes.UPDATE
                })

                resultArray = {
                    suppressionRuleDbId: suppressionRuleDbId,
                    recordCount: 1,
                    href: suppressionRuleUrl
                }
            } else {
                resultArray = {
                    suppressionRuleDbId: suppressionRuleDbId,
                    recordCount: 0,
                    href: suppressionRuleUrl
                }
            }
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {

            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },
    /**
     * Delete the suppression rule record
     * DELETE v3/suppression-rules/:id
     * @param {*} req 
     * @param {*} res 
     */
    delete: async function (req, res) {
        let suppressionRuleDbId = req.params.id
        let transaction

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let suppressionRuleUrl = (isSecure ? 'https' : 'http')
            + '://' + req.headers.host + '/v3/suppression-rules/'
            + suppressionRuleDbId

        // Check if transaction ID exists
        let transactionQuery = `
            SELECT exists(
                SELECT 1 
                FROM 
                    data_terminal.suppression_rule 
                WHERE 
                    id = ${suppressionRuleDbId}
                    AND is_void = FALSE
            )
        `
        let transactionRecord = await sequelize.query(transactionQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (!transactionRecord[0]['exists']) {
            let errMsg = 'The suppression rule ID does not exist.'
            errorCode = await responseHelper.getErrorCodebyMessage(
                404,
                errMsg)
            errMsg = await errorBuilder.getError(req.headers.host, errorCode, errMsg)
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let deleteQuery = `
                DELETE FROM
                    data_terminal.suppression_rule
                WHERE
                    id = ${suppressionRuleDbId}
            `
            await sequelize.query(deleteQuery, {
                type: sequelize.QueryTypes.DELETE
            })

            let resultArray = {
                suppressionRuleDbId: suppressionRuleDbId,
                recordCount: 1,
                href: suppressionRuleUrl
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}