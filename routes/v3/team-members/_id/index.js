/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')
let userValidator = require('../../../../helpers/person/validator')
let forwarded = require('forwarded-for')

module.exports = {

    // Implementation of DELETE call for /v3/team-members/:id
    delete: async function (req, res, next) {

        // check if user is admin
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        const isAdmin = await userValidator.isAdmin(userDbId)
        if (!isAdmin) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401019)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let teamMemberDbId = req.params.id

        if (!validator.isInt(teamMemberDbId)) {
            let errMsg = 'You have provided an invalid format for the team member ID'
            res.send(new errors.BadRequestError(errMsg))
            return
        } else {

            // start transaction
            let transaction

            try {
                // Get transaction
                transaction = await sequelize.transaction({autocommit: false})

                // validate the id
                let teamMemberQuery = `
                    SELECT
                        count(1)
                    FROM
                        tenant.team_member member
                    WHERE
                        member.is_void = FALSE
                        AND member.id = ${teamMemberDbId}
                `
                // Retrieve record from the database   
                let hasRecord = await sequelize.query(teamMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (parseInt(hasRecord[0].count) === 0) {
                    let errMsg = 'The team member you have requested does not exist'
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                // Build query for voiding
                let deleteTeamMemberQuery = `
                    UPDATE
                        tenant.team_member member
                    SET
                        is_void = TRUE,
                        modification_timestamp = NOW(),
                        modifier_id = ${userDbId}
                    WHERE
                        member.id = ${teamMemberDbId}
                `
                await sequelize.query(deleteTeamMemberQuery, {
                    type: sequelize.QueryTypes.UPDATE
                })
        
                // Commit the transaction
                await transaction.commit()
                res.send(200, {
                    rows: {
                        teamMemberDbId: teamMemberDbId
                    }
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500002)
                res.send(new errors.InternalError(errMsg))
                return
            }
        }
    }




}
