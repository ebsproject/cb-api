/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let forwarded = require('forwarded-for')
let userValidator = require('../../../helpers/person/validator')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async (req, res, next) => {
        let teamDbId = null
        let memberDbId = null
        let roleDbId = null
        let userRoleDbId = null
        let remarks = null
        let orderNumber = null
        let resultArray = []
        
        // Store the ID of the user currently logged in
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the user is an admin, end the transaction if not
        if (!await userValidator.isAdmin(userDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401016)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let teamMembersUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/team-members'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []
        let recordCount = 0

        try {
            records = data.records
            recordCount = records.length

            if (data.records == undefined || data.records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })
            let teamMembersValuesArray = []
            let existingTeamMembersArray = []
            let visitedArray  = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0
                
                if (!record.teamDbId || !record.memberDbId || !record.roleDbId) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400159)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (record.teamDbId != undefined) {
                    teamDbId = record.teamDbId
                    if (isNaN(teamDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400160)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the team exists, return 1
                            SELECT
                                count(1)
                            FROM
                                tenant.team team
                            WHERE
                                team.is_void = FALSE AND
                                team.id = ${teamDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.memberDbId != undefined) {
                    memberDbId = record.memberDbId
                    if (isNaN(memberDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400161)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the member exists, return 1
                            SELECT
                                count(1)
                            FROM
                                tenant.person member
                            WHERE
                                member.is_void = FALSE AND
                                member.id = ${memberDbId}
                        )
                    `
                    validateCount += 1

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the user is already a member in the team, return 1
                            SELECT
                                CASE WHEN
                                (count(1) = 1)
                                THEN 0
                                ELSE 1
                                END AS count
                            FROM
                            tenant.team_member member
                            WHERE
                            member.team_id = ${teamDbId}
                            AND member.person_id  = ${memberDbId}
                            AND member.is_void = FALSE
                        )
                    `
                    validateCount += 1
                }

                if (record.roleDbId != undefined) {
                    roleDbId = record.roleDbId
                    if (isNaN(roleDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400162)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the role exists, return 1
                            SELECT
                                count(1)
                            FROM
                                tenant.person_role role
                            WHERE
                                role.is_void = FALSE AND
                                role.id = ${roleDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.userRoleDbId !== undefined) {
                    userRoleDbId = record.userRoleDbId
                    if (isNaN(userRoleDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400163)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the user role exists, return 1
                            SELECT
                                count(1)
                            FROM
                                tenant.person_role role
                            WHERE
                                role.is_void = FALSE AND
                                role.id = ${userRoleDbId}
                        )
                    `
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the user is already a member in the team, return 1
                            SELECT
                                count(1)
                            FROM
                                tenant.team_member member
                            WHERE
                                member.team_id = ${teamDbId}
                                AND member.person_id  = ${memberDbId}
                        )
                    `
                }

                remarks = (record.remarks != undefined) ? record.remarks : null

                // Validate the filters in DB; Count must be equal to validateCount
                let checkInputQuery = `SELECT ${validateQuery} AS count`
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let maxOrderNumberQuery = `
                    SELECT
                        CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE MAX(order_number) 
                        END AS "maxOrderNumber",
                        count(id) AS "teamMemberCount"
                    FROM 
                        tenant.team_member
                    WHERE 
                        team_id = ${teamDbId}
                `

                let maxOrderNumber = await sequelize.query(maxOrderNumberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (maxOrderNumber[0].teamMemberCount == 0) {
                    orderNumber+=1
                } else {
                    orderNumber = parseInt(maxOrderNumber[0].maxOrderNumber)+1
        
                    if (records.indexOf(record) > 0) {
                        orderNumber = visitedArray[visitedArray.length-1]+1
                    }
                }
          
                // Check duplicates if recordCount > 1
                if (recordCount > 1) {
                    if (visitedArray.includes(orderNumber)) {
                        orderNumber+=1
                    } else {
                        visitedArray.push(orderNumber)
                    }
                }
                
                let tempArray = [
                    teamDbId, memberDbId, roleDbId, userDbId, orderNumber
                ]

                // Check if team_id and member_id combination already exists and if just voided
                // If exists, update the record to is_void = FALSE
                let validateMemberQuery = `
                    SELECT
                        CASE WHEN
                        (count(1) > 0)
                        THEN 1
                        ELSE 0
                        END AS count,
                        member.id as "teamMemberDbId"
                    FROM
                        tenant.team_member member
                    WHERE
                        member.team_id = ${teamDbId}
                        AND member.person_id  = ${memberDbId}
                        AND member.is_void = TRUE
                    GROUP by member.id
                `
                let validateMember = await sequelize.query(validateMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (validateMember.length > 0 && validateMember[0]['count'] == 1) {
                    existingTeamMembersArray.push(validateMember[0]['teamMemberDbId'])
                } else {
                    teamMembersValuesArray.push(tempArray)
                }
            }

            if (teamMembersValuesArray.length > 0) {
                // Create a team member record
                let teamMemberQuery = format(`
                    INSERT INTO
                        tenant.team_member
                            (
                            team_id, person_id, person_role_id, creator_id, order_number
                            )
                    VALUES
                        %L
                    RETURNING id`, teamMembersValuesArray 
                )

                teamMemberQuery = teamMemberQuery.trim()

                let teamMembers = await sequelize.query(teamMemberQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction
                })

                for (let teamMember of teamMembers[0]) {
                    teamMemberDbId = teamMember.id
                    let tempObject = {
                        teamMemberDbId: teamMemberDbId,
                        recordCount: 1,
                        href: teamMembersUrlString + '/' + teamMemberDbId
                    }
                    resultArray.push(tempObject)
                }
            }

            // check existing team members records that are voided
            if (existingTeamMembersArray.length > 0) {
                let teamMemberQuery = format(`
                    UPDATE
                        tenant.team_member
                    SET
                        is_void = FALSE,
                        person_role_id = ${roleDbId},
                        modification_timestamp = NOW(),
                        modifier_id = ${userDbId}
                    WHERE
                        id in (%L)
                    RETURNING id`, existingTeamMembersArray 
                )

                let teamMembers = await sequelize.query(teamMemberQuery, {
                    type: sequelize.QueryTypes.UPDATE
                })

                for (let teamMember of teamMembers[0]) {
                    teamMemberDbId = teamMember.id
                    let tempObject = {
                        teamMemberDbId: teamMemberDbId,
                        recordCount: 1,
                        href: teamMembersUrlString + '/' + teamMemberDbId
                    }
                    resultArray.push(tempObject)
                }
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}