/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

const validator = require('validator')
const tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
const logger = require('../../../helpers/logger')
const format = require('pg-format')
let userValidator = require('../../../helpers/person/validator.js')

module.exports = { 
	// Endpoint for retrieving all configurations
	// GET /v3/configurations
	get: async function (req, res, next) {
		// Set defaults 
		let limit = req.paginate.limit;
		let offset = req.paginate.offset
		let params = req.query
		let sort = req.query.sort
		let count = 0
		let orderString = ''
		let conditionString = ''

		// Build query
		let configsQuery = `
			SELECT
				config.id AS "configurationDbId",
				config.abbrev,
				config.name,
				config.config_value AS "configValue",
				config.rank,
				config.usage,
				config.remarks AS "remarks",
				config.notes,
				config.creation_timestamp AS "creationTimestamp",
				config.creator_id AS "creatorDbId",
				creator.person_name AS creator,
				config.modification_timestamp AS "modificationTimestamp",
				modifier.id AS "modifierId",
				modifier.person_name AS modifier
			FROM 
				platform.config config
			LEFT JOIN 
				tenant.person creator ON config.creator_id = creator.id
			LEFT JOIN 
				tenant.person modifier ON config.modifier_id = modifier.id
			WHERE
				config.is_void = FALSE
			ORDER BY
				config.id
		`
		// Get filter condition for abbrev only
		if (params.abbrev !== undefined) {
			let parameters = {
				abbrev: params.abbrev
			}

			conditionString = await processQueryHelper.getFilterString(parameters)

			if (conditionString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400003)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}

		// Parse the sort parameters
		if (sort != null) {
			orderString = await processQueryHelper.getOrderString(sort)

			if (orderString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400004)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}

		// Generate the final sql query 
		configsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
			configsQuery,
			conditionString,
			orderString
		)

		// Retrieve configs from the database   
		let configs = await sequelize.query(configsFinalSqlQuery, {
			type: sequelize.QueryTypes.SELECT,
			replacements: {
				limit: limit,
				offset: offset
			}
		})
		.catch(async err => {
			let errMsg = await errorBuilder.getError(req.headers.host, 500004)
			res.send(new errors.InternalError(errMsg))
			return
		})

		if (await configs === undefined || await configs.length < 1) {
			res.send(200, {
        rows: [],
        count: 0
      })
			return
		} else {
			// Get count 
			configurationCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(configsQuery, conditionString, orderString)

			configCount = await sequelize.query(configurationCountFinalSqlQuery, {
				type: sequelize.QueryTypes.SELECT
			})
			.catch(async err => {
				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

			count = configCount[0].count
		}

		res.send(200, {
			rows: configs,
			count: count
    })
    return
	},

	// Endpoint for creating new configuration
	// POST /v3/configurations
	post: async function (req, res, next) {
		let resultArray = []
		// Get person ID from token
		let personDbId = await tokenHelper.getUserId(req)
		// If person ID is null, display error
		if(personDbId == null) {
			let errMsg = await errorBuilder.getError(req.headers.host, 401002)
			res.send(new errors.BadRequestError(errMsg))
			return
		}

		//check if user is admin
		let isAdmin = await userValidator.isAdmin(personDbId)
		 if(!isAdmin) {
		    let errMsg = await errorBuilder.getError(req.headers.host, 401028)
		    res.send(new errors.BadRequestError(errMsg))
		    return
		}

		// Check if connection is secure or not (http or https)
		let isSecure = forwarded(req, req.headers).secure

		// Set the URL for response
		let configUrlString = (isSecure ? 'https' : 'http')
			+ "://"
			+ req.headers.host
			+ "/v3/configurations"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

		// If request body is empty, return error message
		let data = req.body
		let records = data.records ?? []
		if(records === undefined ||  records.length == 0) {
			let errMsg = await errorBuilder.getError(req.headers.host, 400005)
			res.send(new errors.BadRequestError(errMsg))
			return
		}		

		// Validate request body
		let transaction
		try {
			// Get transaction
			transaction = await sequelize.transaction({ autocommit: false })

			let configValuesArray = []
			for(var record of records) {
				let abbrev = null
				let name = null
				let configValue = null
				let rank = null
				let usage = null
				let remarks = null
				let notes = null

				// Validation for the required parameters
				if(record.abbrev == undefined || record.abbrev == undefined || record.usage == undefined || record.configValue == undefined){
					let errMsg = `Required parameters are missing. Ensure that the abbrev, name, configValue, and usage fields are not empty.`
					res.send(new errors.BadRequestError(errMsg))
					return
				}

				// Assign request body params to variables
				abbrev = record.abbrev
				name = record.name
				configValue = record.configValue
				rank = record.rank ?? '1' // Optional; default value to 1
				usage = record.usage
				remarks = record.remarks ?? null // Optional; default value to null
				notes = record.notes ?? null // Optional; default value to null

				if (!validator.isInt(rank)) {
					let errMsg = await errorBuilder.getError(req.headers.host, 400083)
					res.send(new errors.BadRequestError(errMsg))
					return
				}

				let tempArray = [
					abbrev, name, configValue, rank, usage, remarks, notes
				]

				configValuesArray.push(tempArray)
			}
			
			// Insert query
			let configQuery = format(`
				INSERT INTO
					platform.config (
						abbrev, name, config_value, rank, usage, remarks, notes
					)
				VALUES
					%L
				RETURNING id`, configValuesArray
			)

			let configure = await sequelize.query(configQuery, {
				type: sequelize.QueryTypes.INSERT,
				transaction: transaction
			}).catch(async err => {
				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

			for(let config of configure[0]) {
				configDbId = config.id 
				let array = {
					configDbId: config.id,
					recordCount: 1,
					href: configUrlString + '/' + configDbId
				}
				resultArray.push(array)
			}

			await transaction.commit()
			res.send(200, {
			    rows: resultArray,
			    count: resultArray.length
			})

			return
		}
		// Log error
		catch (err) {
			logger.logMessage(__filename,'POST configurations: '+ JSON.stringify(err), 'error')
			// Rollback transaction if any errors were encountered
			if (err) await transaction.rollback()
			let errMsg = await errorBuilder.getError(req.headers.host, 500001)
			res.send(new errors.InternalError(errMsg))
			return
		}
	}
}
