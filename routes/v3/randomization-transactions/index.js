/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token.js')
let userValidator = require('../../../helpers/person/validator.js')
let format = require('pg-format')
let forwarded = require('forwarded-for')

module.exports = {
  // Implementation of POST call for /v3/randomization-transactions
  post: async function (req, res, next) {
    let type = null //status
    let studyDbId = null //occurence_id
    let inputFile = null //json parameters
    let actorId = null
    let creatorId = null
    let startActionTimestamp = null
    let endActionTimestamp = null
    let randomizationOutput = null
    let dataResults = null

    // Verify if user is an admin
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401002)
        res.send(new errors.BadRequestError(errMsg))
        return
    }

    if (!await userValidator.isAdmin(userDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401016)
      res.send(new errors.UnauthorizedError(errMsg))
      return
    }

    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure
    // Set URL for response
    let randTransactionUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/randomization-transactions'


    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      // Records = [{}...]
      records = data.records
      recordCount = records.length
      // Check if the input data is empty
      if (records == undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (e) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    // Start transaction
    let transaction

    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let randomizationTransactionsArray = []
      let visitedArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        // Check if the required colums are in the input
        if (!record.studyDbId) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400085)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        /* Validate and set values */

        if (record.studyDbId != undefined) {
          studyDbId = record.studyDbId

          // Validate if the abbreviation does not yet exist in the DB
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if the study_id does not exist yet; if so, return 0
              SELECT
                CASE WHEN (count(1) > 0)
                  THEN 0
                  ELSE 1
                END
              FROM
                operational_data_terminal.randomization_transaction randomizationTransaction
              WHERE
                randomizationTransaction.is_void = FALSE AND
                randomizationTransaction.study_id = '${studyDbId}'
            )
          `
          validateCount += 1
        }

        type = (record.type != undefined) ? record.type : 'initialized' //status
        inputFile = (record.inputFile != undefined) ? record.inputFile : null //json parameters
        startActionTimestamp = (record.startActionTimestamp != undefined) ? record.startActionTimestamp : new Date().toDateString() + ' ' + new Date().toLocaleTimeString()
        endActionTimestamp = (record.endActionTimestamp != undefined) ? record.endActionTimestamp : new Date().toDateString() + ' ' + new Date().toLocaleTimeString() //json parameters
        actorId = (record.actorId != undefined) ? record.actorId : userDbId
        randomizationOutput = (record.randomizationOutput != undefined) ? record.randomizationOutput : null //json parameters
        dataResults = (record.dataResults != undefined) ? record.dataResults : null //json parameters
        creatorId = userDbId

        // Validate the filters in DB; Count must be equal to validateCount
        let checkInputQuery = `SELECT ${validateQuery} AS count`
        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        if (inputCount[0]['count'] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
        
        if (recordCount > 1) {
          // Check if studyDbId is unique among bulk inputs
          if (visitedArray.includes(studyDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400175)
            res.send(new errors.BadRequestError(errMsg))
            return
          } else {
            visitedArray.push(studyDbId)
          }
        }

        let tempArray = [
          studyDbId,
          type,
          inputFile,
          actorId,
          creatorId,
          startActionTimestamp,
          endActionTimestamp,
          randomizationOutput,
          dataResults
        ]

        randomizationTransactionsArray.push(tempArray)
      }

      // Create randomization_transaction record
      let randomizationTransactionQuery = format(`
        INSERT INTO
          operational_data_terminal.randomization_transaction
            (
              study_id, type, input_file, actor_id, creator_id, start_action_timestamp, end_action_timestamp, randomization_output, data_results
            )
        VALUES
          %L
        RETURNING id`, randomizationTransactionsArray
      )

      randomizationTransactionQuery = randomizationTransactionQuery.trim()

      let randomizationTransactions = await sequelize.query(randomizationTransactionQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      let resultArray = []

      for (let randTransaction of randomizationTransactions[0]) {
        randTransactionDbId = randTransaction.id

        // Return the randomization transaction info to Client
        let array = {
          randomizationTransactionDbId: randTransactionDbId,
          recordCount: 1,
          href: randTransactionUrlString + '/' + randTransactionDbId
        }

        resultArray.push(array)
      }

      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}
