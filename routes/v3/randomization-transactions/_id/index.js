/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')
let userValidator = require('../../../../helpers/person/validator.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {
  // Endpoint for retrieving a specific randomization transaction
  // GET /v3/randomization-transaction/:id
  get: async function (req, res, next) {
    // Retrieve the randomization transaction ID
    let randomizationTransactionId = req.params.id

    if (!validator.isInt(randomizationTransactionId)) {
      let errMsg = "You have provided an invalid format for the randomization transaction ID."
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    let randomizationTransactionQuery = `
      SELECT
        randomizationTransaction.id AS "randomizationTransactionDbId",
        randomizationTransaction.type,
        randomizationTransaction.study_id AS "studyDbId",
        randomizationTransaction.input_file AS "inputFile",
        randomizationTransaction.start_action_timestamp AS "startActionTimestamp",
        randomizationTransaction.end_action_timestamp AS "endActionTimestamp",
        actor.id AS "actorDbId",
        actor.display_name AS actor,
        randomizationTransaction.is_successful AS "isSuccessful",
        randomizationTransaction.generated_input AS "generatedInput",
        randomizationTransaction.randomization_output AS "randomizationOutput",
        randomizationTransaction.elapsed_time AS "elapsedTime",
        randomizationTransaction.data_results AS "dataResults",
        randomizationTransaction.plan_id AS "planDbId",
        randomizationTransaction.remarks,
        randomizationTransaction.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.display_name AS creator,
        randomizationTransaction.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.display_name AS modifier
      FROM 
        operational_data_terminal.randomization_transaction randomizationTransaction
      LEFT JOIN 
        master.user creator ON randomizationTransaction.creator_id = creator.id
      LEFT JOIN 
        master.user modifier ON randomizationTransaction.modifier_id = modifier.id
      LEFT JOIN
        master.user actor ON randomizationTransaction.actor_id = modifier.id
      WHERE 
        randomizationTransaction.id = ${randomizationTransactionId} AND
        randomizationTransaction.is_void = FALSE 
    `

    // Retrieve randomization transaction from the database   
    let randomizationTransactions = await sequelize.
      query(randomizationTransactionQuery, {
        type: sequelize.QueryTypes.SELECT,
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (await randomizationTransactions == undefined || await randomizationTransactions.length < 1) {
      let errMsg = "The randomization transaction you have requested does not exist"
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: randomizationTransactions
    })
    return
  },

  // DELETE /v3/randomization-transactions/:id
  delete: async function(req, res, next) {
    // Retrieve randomization transaction ID
    let randomizationTransactionDbId = req.params.id

    // Retrieve user ID of client from access token
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401002)
        res.send(new errors.BadRequestError(errMsg))
        return
    }

    // Check if user is an administrator. If not, no access.
    let isAdmin = await userValidator.isAdmin(userDbId)
    
    if (!validator.isInt(randomizationTransactionDbId)) {
      let errMsg = 
        'You have provided an invalid format for randomization transaction ID.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let randomizationTransactionQuery = `
        SELECT
          randomizationTransaction.id AS "randomizationTransactionDbId",
          randomizationTransaction.creator_id AS "creatorDbId"
        FROM
          operational_data_terminal.randomization_transaction randomizationTransaction
        WHERE
          randomizationTransaction.is_void = FALSE AND
          randomizationTransaction.id = ${randomizationTransactionDbId}
      `

      let randomizationTransaction = await sequelize
        .query(randomizationTransactionQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      if (await randomizationTransaction == undefined || await randomizationTransaction.length < 1) {
        let errMsg = 
          'The randomization transaction record you requested does not exist.'
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      // Store the creator ID of the experiment that was retrieved
      let randomizationTransactionCreatorDbId = randomizationTransaction[0]['creatorDbId']

      // Checks if user is the owner of the experiment
      if (randomizationTransactionCreatorDbId != userDbId) {
        if(!isAdmin) {
          let errMsg = 'You do not have access to delete this randomization transaction.'
          res.send(new errors.UnauthorizedError(errMsg))
          return
        }
      }

      let deleteRandomizationTransactionQuery = format(`
        UPDATE
          operational_data_terminal.randomization_transaction randomizationTransaction
        SET
          is_void = TRUE,
          notes = CONCAT('VOIDED-', ${randomizationTransactionDbId}),
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          randomizationTransaction.id = ${randomizationTransactionDbId}
      `)

      await sequelize
        .query(deleteRandomizationTransactionQuery, {
          type: sequelize.QueryTypes.UPDATE
        })

      await transaction.commit()
      res.send(200, {
        rows: { randomizationTransactionDbId: randomizationTransactionDbId }
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}