/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    /**
     * Retrieve plots for harvest labels
     * GET /v3/plot-harvest-labels
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let params = req.query
        let sort = req.query.sort
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = []
        let occurrenceDbId

        if (params.occurrenceDbId != undefined) {
            occurrenceDbId = params.occurrenceDbId.split(',').join('|')
        }

        // Get the filter condition
        conditionString = await processQueryHelper.getFilter(
            searchParams,
            []
        )

        if (conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Build query
        let plotQuery = `
            SELECT
                plots.*,
                GENERATE_SERIES(
                    1,
                    CASE
                        WHEN pd.data_value IS NULL THEN 1
                        ELSE pd.data_value::INTEGER
                    END
                ) AS "bagNumber"
            FROM
                (
                    SELECT
                        pi.plot_id "plotDbId",
                        l.id "locationDbId",
                        l.location_code AS "locationCode",
                        l.location_name AS "locationName",
                        o.id AS "occurrenceDbId",
                        o.occurrence_code AS "occurrenceCode",
                        o.occurrence_name AS "occurrenceName",
                        o.occurrence_number AS "occurrenceNumber",
                        pr.id AS "programDbId",
                        pr.program_code AS "programCode",
                        pr.program_name AS "programName",
                        plot.plot_code AS "plotCode",
                        plot.plot_number AS "plotNumber",
                        plot.field_x AS "fieldX",
                        plot.field_y AS "fieldY",
                        pi.entry_id AS "entryDbId",
                        pi.entry_code AS "entryCode",
                        pi.entry_number AS "entryNumber",  
                        hgermplasm.designation AS "germplasmName",
                        hgermplasm.id AS "germplasmDbId",
                        hgermplasm.generation AS "germplasmGeneration",
                        hseed.id AS "seedDbId",
                        hseed.seed_name AS "seedName",
                        hseed.seed_code AS "seedCode",
                        hpackage.id AS "harvestedPackageDbId",
                        hpackage.package_code AS "harvestedPackageCode",
                        hpackage.package_label AS "harvestedPackageLabel",
                        ppackage.id AS "plantedPackageDbId",
                        ppackage.package_code AS "plantedPackageCode",
                        ppackage.package_label AS "plantedPackageLabel",
                        harvest_method.data_value "harvestMethod",
                        (
                            CASE WHEN LOWER(harvest_method.data_value) IN 
                                (   'plant-specific', 'panicle selection', 
                                    'plant-specific and bulk', 'single plant selection and bulk', 
                                    'single plant selection', 'bulk'
                                )
                            THEN (SELECT (REGEXP_MATCHES(hgermplasm.designation, '.+-(.+)$'))[1]) END 
                        ) AS "selectionNumber"
                    FROM 
                        experiment.plot plot 
                        LEFT JOIN experiment.planting_instruction pi ON pi.plot_id = plot.id
                        LEFT JOIN germplasm.package ppackage ON ppackage.id = pi.package_id
                        LEFT JOIN germplasm.seed hseed on hseed.source_plot_id = plot.id
                        LEFT JOIN germplasm.germplasm hgermplasm ON hgermplasm.id = hseed.germplasm_id
                        LEFT JOIN germplasm.package hpackage ON hpackage.seed_id = hseed.id
                        LEFT JOIN experiment.location l ON l.id = plot.location_id
                        LEFT JOIN experiment.occurrence o ON o.id = plot.occurrence_id
                        LEFT JOIN experiment.experiment e ON e.id = o.experiment_id 
                        LEFT JOIN tenant.program pr ON pr.id = e.program_id 
                        LEFT JOIN experiment.plot_data harvest_method ON harvest_method.plot_id =plot.id 
                            AND harvest_method.variable_id = (
                                SELECT id FROM master.variable WHERE abbrev = 'HV_METH_DISC'
                            )
                            AND harvest_method.is_void = FALSE
                    WHERE
                        plot.occurrence_id = ${occurrenceDbId}
                        AND pi.is_void = FALSE
                        AND hgermplasm.is_void = FALSE
                        AND plot.is_void = FALSE
                        AND hseed.is_void = FALSE
            ) plots
            LEFT JOIN
                experiment.plot_data pd ON plots."plotDbId" = pd.plot_id
                AND pd.variable_id = (
                    SELECT
                        id
                    FROM
                        master.variable
                    WHERE
                        variable.abbrev = 'NO_OF_BAGS'
                        AND variable.is_void = FALSE
                        LIMIT 1
                    )
                AND pd.is_void = FALSE
        `

        // Generate final SQL query
        let plotDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plotQuery,
            conditionString,
            orderString,
        )

        // Retrieve plot data from the database   
        let plotData = await sequelize.query(plotDataFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await plotData == undefined || await plotData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get the final count of records
            let plotDataCountFinalSqlQuery = await processQueryHelper
                .getCountFinalSqlQuery(
                    plotQuery,
                    conditionString,
                    orderString
                )

            let plotDataCount = await sequelize
                .query(plotDataCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = plotDataCount != null && plotDataCount[0] != null && plotDataCount[0].count != null ? plotDataCount[0].count : 0

            res.send(200, {
                rows: plotData,
                count: count
            })
            return
        }
    }
}
