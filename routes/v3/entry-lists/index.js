/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')

let tokenHelper = require('../../../helpers/auth/token')
let patternHelper = require('../../../helpers/patternGenerator/index')
let crossesHelper = require('../../../helpers/crosses/index')

let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')

let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

const endpoint = 'entry-lists'

module.exports = {

    // Implementation of POST call for /v3/entry-lists
    post: async (req, res, next) => {

        // Set defaults
        let resultArray = []

        let entryListDbId = null
        let entryListCode = null
        let entryListName = null
        let entryListStatus = null
        let experimentDbId = null
        let entryListType = null
        let experimentYear = null
        let experimentYearRegex = /^\d{4}$/
        let seasonDbId = null
        let siteDbId = null
        let cropDbId = null
        let programDbId = null
        let description = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let entryListUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/entry-lists'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = data.records

        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction
        try {

            let entryListValuesArray = []
            let counter = 0
            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                let generatedEntryListName

                if(!record.entryListName) {
                     // use system-generated entry list name 
                    pattern = await patternHelper.getPattern("CM_NAME_PATTERN_ENTRY_LIST_NAME")
                    generatedEntryListName = await crossesHelper.generateCode(
                            pattern, 
                            record.experimentYear, 
                            record.seasonDbId,
                            record.siteDbId,
                            counter
                        )
                
                    entryListName = generatedEntryListName
                }

                // Check if required columns are in the input
                if (
                    (!record.entryListName && !generatedEntryListName && generatedEntryListName == '') || 
                    !record.entryListStatus || 
                    !record.entryListType
                ) {
                    let errMsg = `Required parameters are missing. Ensure that entryListName, entryListStatus, and entryListType fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(!generatedEntryListName){
                    // use provided entry list name
                    entryListName = record.entryListName.trim()
                }

                /** Validation of required parameters and set values */

                if (record.entryListStatus !== undefined) {
                    entryListStatus = record.entryListStatus.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_LIST_STATUS' AND
                                scaleValue.value ILIKE $$${entryListStatus}$$
                        )
                    `
                    validateCount += 1
                }

                if (record.experimentDbId !== undefined) {
                    experimentDbId = record.experimentDbId

                    if (!validator.isInt(experimentDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400025)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate experiment ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.experiment experiment
                            WHERE 
                                experiment.is_void = FALSE AND
                                experiment.id = ${experimentDbId}
                        )
                    `
                    validateCount += 1

                    
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment ID is not used
                            SELECT 
                                CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                                END AS count
                            FROM 
                                experiment.entry_list entryList
                            WHERE 
                                entryList.experiment_id = ${experimentDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.entryListType !== undefined) {
                    entryListType = record.entryListType.trim()

                    let validEntryListTypes = ['entry list', 'cross list', 'parent list']

                    // Validate entry list type
                    if (!validEntryListTypes.includes(entryListType.toLowerCase())) {
                        let errMsg = `Invalid request, you have provided an invalid
                        value for entry list type.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.experimentYear !== undefined) {
                    experimentYear = record.experimentYear

                    if (!validator.isInt(experimentYear)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400129)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (!experimentYear.match(experimentYearRegex)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400144)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                }

                if (record.seasonDbId !== undefined) {
                    seasonDbId = record.seasonDbId

                    if (!validator.isInt(seasonDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400063)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate season ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if season is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.season
                            WHERE 
                                is_void = FALSE
                            AND
                                id = ${seasonDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.siteDbId !== undefined) {
                    siteDbId = record.siteDbId

                    if (!validator.isInt(siteDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400064)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate site ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if site is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object site
                            WHERE 
                                site.is_void = FALSE
                            AND
                                site.id = ${siteDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.cropDbId !== undefined) {
                    cropDbId = record.cropDbId

                    if (!validator.isInt(cropDbId)) {
                        let errMsg = `Invalid request, cropDbId must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                
                    // Validate crop ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if crop ID is not used
                            SELECT 
                                count(1)
                            FROM
                                tenant.crop
                            WHERE
                                is_void = FALSE
                            AND
                                id = ${cropDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.programDbId !== undefined) {
                    programDbId = record.programDbId
                    
                    if (!validator.isInt(programDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400125)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate program ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if program ID is not used
                            SELECT 
                                count(1)
                            FROM 
                                tenant.program
                            WHERE 
                                is_void = FALSE
                            AND
                                id = ${programDbId}
                        )
                    `
                    validateCount += 1
                }

                description = (record.description !== undefined) ? 
                    record.description.trim() : null

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Generate entry list code: ENTLIST + (last entry list ID + 1)
                let generateEntryListCodeQuery = `
                    SELECT
                        experiment.generate_code('entry_list')
                    AS entrylistcode
                `
                let result = await sequelize.query(generateEntryListCodeQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                let entryListCode = result[0].entrylistcode

                // Get values
                let tempArray = [
                    entryListCode, entryListName, entryListStatus, experimentDbId, entryListType, description, 
                    experimentYear, seasonDbId, siteDbId, cropDbId, programDbId, personDbId
                ]

                entryListValuesArray.push(tempArray)

                counter += 1
            }

            let entryLists = []
            transaction = await sequelize.transaction(async transaction => {
                // Create entry record
                let entryListQuery = format(`
                    INSERT INTO
                        experiment.entry_list (
                            entry_list_code,
                            entry_list_name,
                            entry_list_status,
                            experiment_id,
                            entry_list_type,
                            description,
                            experiment_year,
                            season_id,
                            site_id,
                            crop_id,
                            program_id,
                            creator_id
                        )
                    VALUES
                        %L
                    RETURNING id`, entryListValuesArray
                )

                entryLists = await sequelize.query(entryListQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })

                for (var entrylist of entryLists[0]) {
                    entryListDbId = entrylist.id

                    let entryListRecord = {
                        entryListDbId: entryListDbId,
                        recordCount: 1,
                        href: entryListUrlString + '/' + entryListDbId
                    }

                    resultArray.push(entryListRecord)
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}