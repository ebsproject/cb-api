/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    //POST /v3/entry-lists/:id/cross-count-generations
    post: async function (req, res, next) {
        // Retrieve entry list ID
        let entryListDbId = req.params.id
        
        // Check if entry list ID is an integer
        if (!validator.isInt(entryListDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400021)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if entry list is existing
        let entryListQuery = `
            SELECT
                id
            FROM
                experiment.entry_list
            WHERE
                id = ${entryListDbId}
        `

        // Retrieve entry list from the database
        let entryList = await sequelize.query(entryListQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await entryList == undefined || await entryList.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404006)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction

        try{
            transaction = await sequelize.transaction({ autocommit: false })
            // Update cross count of the entry list
            let query = `
                UPDATE 
                    experiment.entry_list AS entlist
                SET 
                    cross_count = t.cross_count
                FROM (
                    SELECT 
                        entlist.id AS entry_list_id,
                        (
                            SELECT 
                                count(DISTINCT "cross".id) AS crossCount
                            FROM 
                                germplasm.cross AS "cross"
                            JOIN 
                                germplasm.cross_parent AS cp 
                            ON 
                                "cross".id = cp.cross_id
                            WHERE 
                                "cross".entry_list_id = ${entryListDbId} AND 
                                "cross".is_void = FALSE AND 
                                cp.is_void = FALSE
                        ) AS cross_count
                    FROM 
                        experiment.entry_list entlist
                    WHERE 
                        entlist.id = ${entryListDbId}
                ) AS t
                WHERE 
                    entlist.id = t.entry_list_id
                `
            let updateCrossCountQuery = await sequelize.query(query, {
            type: sequelize.QueryTypes.UPDATE
            })

            resultArray = {
                entryListDbId: entryListDbId,
                recordCount: 1
            }
            
            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })

            return
        }catch{
           // Rollback transaction if any errors were encountered
           if (err) await transaction.rollback()
           let errMsg = await errorBuilder.getError(req.headers.host, 500003)
           res.send(new errors.InternalError(errMsg))
           return
        }
    }
}