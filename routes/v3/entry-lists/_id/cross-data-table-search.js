/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let searchHelper = require('../../../../helpers/queryTool/index.js')

module.exports = {
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let orderString = ''
        let addedOrderString = `ORDER BY "germplasmCross".id`
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let includeParentSourceQuery = ''

        let variableId = [0]
        let crosstabColumns = null
        let selectColumns = null
        let variableAbbrev = []
        let retrieveDataFromPI = ''

        //get the parent id from link
        let entryListDbId = req.params.id

        //validation of parent id
        if (!validator.isInt(entryListDbId)) {
            let errMsg =
                await errorBuilder.getError(req.headers.host, 400021)
                res.send(new errors.BadRequestError(errMsg))
            return
        }

        //validation for entryList if its existing
        let entryListQuery = `
            SELECT 
                entlist.id
            FROM 
                experiment.entry_list entlist
            WHERE 
                entlist.is_void = FALSE AND 
                entlist.id = ${entryListDbId}
        `
        let entryList = await sequelize
        .query(entryListQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 404006)
            res.send(new errors.InternalError(errMsg))
            return
        })

        // Get all plot data variables
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') as "variableId",
                STRING_AGG( '"' || abbrev || '"', ',') as "selectColumns",
                STRING_AGG( abbrev, ',') as "variableAbbrev",
                STRING_AGG( '"' || abbrev || '"' || ' jsonb', ',') as "crosstabColumns"
            FROM(
                SELECT 
                    distinct (cross_data.variable_id) as variable_id,
                    v.abbrev
                    FROM
                        germplasm.cross_data cross_data
                            LEFT JOIN germplasm.cross c ON c.id = cross_data.cross_id AND c.is_void = FALSE
                            LEFT JOIN master.variable v ON v.id = cross_data.variable_id
                    WHERE
                        c.entry_list_id = ${entryListDbId}
                        AND cross_data.is_void = FALSE
                ORDER BY v.abbrev
            )a
        `

        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (variableList) {
            variableId = variableList[0]['variableId'] == null ? [0] : variableList[0]['variableId']
            crosstabColumns = variableList[0]['crosstabColumns']
            selectColumns = variableList[0]['selectColumns']

            variableAbbrev = variableList[0]['variableAbbrev'] == null ? [] : variableList[0]['variableAbbrev'].split(',')
        }

        if (crosstabColumns != "" && crosstabColumns != null) {
            crosstabColumns = ',' + crosstabColumns
        } else if (crosstabColumns == null) {
            crosstabColumns = ', "null" text';
        }
        if (selectColumns != "" && selectColumns != null) {
            selectColumns = ',' + selectColumns
        } else if (selectColumns == null) {
            selectColumns = ''
        }

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Get flag for retrieval from planting instruction
            if (req.body.retrieveDataFromPI != null) {
                retrieveDataFromPI = true
            }

            // Exclude all measurement variables
            let excludedParametersArray = []

            excludedParametersArray.push('fields', 'distinctOn', 'dataQCCode', "dataValue")
            for (abbrev of variableAbbrev) {
                excludedParametersArray.push(abbrev)
            }

            // Get filter condition
            conditionString = await processQueryHelper
            .getFilter(
                req.body,
                excludedParametersArray,
                (req.body.crossName) ? '||' : '|'
            )

            let conditionArray = []

            // Get filters for plot data with values that is in JSON format
            for (trait in req.body) {

                if (variableAbbrev.includes(trait)) {

                    for (field in req.body[trait]) {
                        condition = ''
                        if (field == 'dataValue') {
                            let variableQuery = `
                                SELECT 
                                    data_type
                                    FROM
                                        master.variable v
                                    WHERE
                                        v.abbrev='${trait}'
                            `
                            let variableList = await sequelize.query(variableQuery, {
                                type: sequelize.QueryTypes.SELECT
                            })
                            let variableDataType = variableList[0]['data_type']
                            value = req.body[trait]['dataValue'];
                            column = '("' + trait + '"->>' + "'dataValue')::" + variableDataType
                            condition = searchHelper.getFilterCondition(value, column, columnCast = true)
                        }

                        if (condition != '') {
                            conditionArray.push(`(` + condition + ')')
                        }
                    }
                }
            }

            if (conditionArray.length > 0) {
                condition = conditionArray.join(' AND ')
                if (conditionString == '') {

                    conditionString = conditionString +
                        ` WHERE `
                        + condition
                } else {
                    conditionString = conditionString +
                        ` AND `
                        + condition
                }
            }

            if (conditionString && conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                    ${parameters['distinctOn']}
                `
            }
        }

        if (retrieveDataFromPI === true) {
            // Build query for retrieval of data from Planting Instruction
            includeParentSourceQuery = `
                (
                    SELECT 
                        g.designation
                    FROM 
                        germplasm.cross_parent cp
                    LEFT JOIN 
                        experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                    LEFT JOIN
                        germplasm.germplasm g on g.id = pi.germplasm_id
                    WHERE 
                        cp.cross_id = "germplasmCross".id AND
                        cp.order_number = 1
                    LIMIT 1
                ) AS "crossFemaleParent",
                (
                    SELECT 
                        s.seed_code
                    FROM 
                        germplasm.cross_parent cp
                    LEFT JOIN 
                        experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                    LEFT JOIN
                        germplasm.seed s on s.id = pi.seed_id
                    WHERE 
                        cp.cross_id = "germplasmCross".id AND
                        cp.order_number = 1
                    LIMIT 1
                ) AS "femaleParentSeedSource",
                -- Copy "crossFemaleParent" if "crossMaleParent" is null
                CASE 
                    WHEN (
                        SELECT 
                            g.designation
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.germplasm g on g.id = pi.germplasm_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    ) = NULL THEN (
                        SELECT 
                            g.designation
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.germplasm g on g.id = pi.germplasm_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 1
                        LIMIT 1
                    )
                    WHEN (
                        SELECT 
                            g.designation
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.germplasm g on g.id = pi.germplasm_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    ) IS NULL THEN (
                        SELECT 
                            g.designation
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.germplasm g on g.id = pi.germplasm_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 1
                        LIMIT 1
                    )
                    WHEN (
                        SELECT 
                            g.designation
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.germplasm g on g.id = pi.germplasm_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    ) = '' THEN (
                        SELECT 
                            g.designation
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.germplasm g on g.id = pi.germplasm_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 1
                        LIMIT 1
                    )
                    ELSE (
                        SELECT 
                            g.designation
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.germplasm g on g.id = pi.germplasm_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    )
                END AS "crossMaleParent",
                -- Copy "femaleParentSeedSource" if "maleParentSeedSource" is null
                CASE
                    WHEN (
                        SELECT 
                            s.seed_code
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.seed s on s.id = pi.seed_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    ) = NULL THEN (
                        SELECT 
                            s.seed_code
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.seed s on s.id = pi.seed_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 1
                        LIMIT 1
                    )
                    WHEN (
                        SELECT 
                            s.seed_code
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.seed s on s.id = pi.seed_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    ) IS NULL THEN (
                        SELECT 
                            s.seed_code
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.seed s on s.id = pi.seed_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 1
                        LIMIT 1
                    )
                    WHEN (
                        SELECT 
                            s.seed_code
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.seed s on s.id = pi.seed_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    ) = '' THEN (
                        SELECT 
                            s.seed_code
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.seed s on s.id = pi.seed_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 1
                        LIMIT 1
                    )
                    ELSE (
                        SELECT 
                            s.seed_code
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        LEFT JOIN
                            germplasm.seed s on s.id = pi.seed_id
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    )
                END AS "maleParentSeedSource",
                (
                    SELECT 
                        pi.entry_number
                    FROM 
                        germplasm.cross_parent cp
                    LEFT JOIN 
                        experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                    WHERE 
                        cp.cross_id = "germplasmCross".id AND
                        cp.order_number = 1
                    LIMIT 1
                ) AS "femaleParentEntryNumber",
                -- Copy "femaleParentEntryNumber" if "crossMethod" is 'selfing'
                CASE
                    WHEN "germplasmCross".cross_method = 'selfing' THEN (
                        SELECT 
                            pi.entry_number
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 1
                        LIMIT 1
                    )
                    ELSE (
                        SELECT 
                            pi.entry_number
                        FROM 
                            germplasm.cross_parent cp
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = cp.entry_id AND p.occurrence_id = cp.occurrence_id
                        LEFT JOIN
                            experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id 
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND
                            cp.order_number = 2
                        LIMIT 1
                    )
                END AS "maleParentEntryNumber",
                (
                    SELECT 
                        entry_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.entry entry ON entry.id = crossParent.entry_id
                    WHERE 
                        (crossParent.parent_role = 'female' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                    LIMIT 1
                ) AS "femaleEntryCode",
                (
                    SELECT 
                        entry_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.entry entry ON entry.id = crossParent.entry_id
                    WHERE 
                        (crossParent.parent_role = 'male' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                    LIMIT 1
                ) AS "maleEntryCode",
                (
                    SELECT 
                        e.entry_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN 
                        experiment.plot p ON p.entry_id = crossParent.entry_id AND p.occurrence_id = crossParent.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction e ON e.entry_id = p.entry_id AND e.plot_id = p.id 
                    WHERE 
                        (crossParent.parent_role = 'female' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                        AND e.entry_code is not null
                    LIMIT 1
                ) AS "femalePIEntryCode",
                (
                    SELECT 
                        e.entry_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN 
                        experiment.plot p ON p.entry_id = crossParent.entry_id AND p.occurrence_id = crossParent.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction e ON e.entry_id = p.entry_id AND e.plot_id = p.id 
                    WHERE 
                        (crossParent.parent_role = 'male' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                        AND e.entry_code is not null
                    LIMIT 1
                ) AS "malePIEntryCode",
                (
                    SELECT 
                        occurrence_name
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'female' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                    LIMIT 1
                ) AS "femaleOccurrenceName",
                (
                    SELECT 
                        occurrence_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'female' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                    LIMIT 1
                ) AS "femaleOccurrenceCode",
                (
                    SELECT 
                        occurrence_name
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'male' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                    LIMIT 1
                ) AS "maleOccurrenceName",
                (
                    SELECT 
                        occurrence_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'male' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                    LIMIT 1
                ) AS "maleOccurrenceCode",
            `

            seedSourceInfoTables = `
                LEFT JOIN
                (
                    SELECT
                        e.id AS "femaleSourceEntryDbId",
                        e.entry_code AS "femaleSourceEntry",
                        s.seed_name AS "femaleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role in ('female', 'female-and-male') AND
                        cross_parent.is_void = false AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS femaleEntryTbl ON femaleEntryTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        e.id AS "maleSourceEntryDbId",
                        e.entry_code AS "maleSourceEntry",
                        s.seed_name AS "maleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role in ('male', 'female-and-male') AND
                        cross_parent.is_void = FALSE AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS maleEntryTbl ON maleEntryTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        p.id AS "femaleSourcePlotDbId",
                        p.plot_code AS "femaleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role in ('female', 'female-and-male') AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE
                ) AS femalePlotTbl ON femalePlotTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        p.id AS "maleSourcePlotDbId",
                        p.plot_code AS "maleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role in ('male', 'female-and-male') AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE
                ) AS malePlotTbl ON malePlotTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        g.parentage AS "femaleParentage",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role in ('female', 'female-and-male') AND
                        g.is_void = FALSE
                ) AS femaleParentage ON femaleParentage."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        g.parentage AS "maleParentage",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role in ('male', 'female-and-male') AND
                        g.is_void = FALSE
                ) AS maleParentage ON maleParentage."crossDbId" = "germplasmCross".id
            `

            seedSourceInfoQuery = `
                femaleEntryTbl."femaleSourceEntryDbId",
                femaleEntryTbl."femaleSourceEntry",
                femaleEntryTbl."femaleSourceSeedName",
                femalePlotTbl."femaleSourcePlotDbId",
                femalePlotTbl."femaleSourcePlot",
                (
                    SELECT
                        g.parentage
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        experiment.plot p ON p.entry_id = cross_parent.entry_id AND p.occurrence_id = cross_parent.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = pi.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role in ('female', 'female-and-male') AND
                        cross_parent.cross_id = "germplasmCross".id AND
                        g.is_void = FALSE
                    LIMIT 1
                ) AS "femaleParentage",
                maleEntryTbl."maleSourceEntryDbId",
                maleEntryTbl."maleSourceEntry",
                maleEntryTbl."maleSourceSeedName",
                malePlotTbl."maleSourcePlotDbId",
                malePlotTbl."maleSourcePlot",
                (
                    SELECT
                        g.parentage
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        experiment.plot p ON p.entry_id = cross_parent.entry_id AND p.occurrence_id = cross_parent.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction pi ON pi.entry_id = p.entry_id AND pi.plot_id = p.id
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = pi.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role in ('male', 'female-and-male') AND
                        cross_parent.cross_id = "germplasmCross".id AND
                        g.is_void = FALSE
                    LIMIT 1
                ) AS "maleParentage",
            `
        } else {
            // Default query
            includeParentSourceQuery = `
                (
                    SELECT g.designation
                    FROM germplasm.germplasm g, germplasm.cross_parent cp
                    WHERE cp.germplasm_id = g.id AND 
                        cp.cross_id = "germplasmCross".id AND 
                        cp.order_number = 1
                ) AS "crossFemaleParent",
                (
                    SELECT s.seed_code 
                    FROM germplasm.seed s, germplasm.cross_parent cp 
                    WHERE cp.seed_id = s.id AND 
                        cp.cross_id = "germplasmCross".id AND 
                        cp.order_number = 1
                ) AS "femaleParentSeedSource",
                -- Copy "crossFemaleParent" if "crossMaleParent" is null
                CASE
                    WHEN (
                        SELECT g.designation
                        FROM germplasm.germplasm g, germplasm.cross_parent cp
                        WHERE cp.germplasm_id = g.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 2
                    ) = NULL THEN (
                        SELECT g.designation
                        FROM germplasm.germplasm g, germplasm.cross_parent cp
                        WHERE cp.germplasm_id = g.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 1
                    )
                    WHEN (
                        SELECT g.designation
                        FROM germplasm.germplasm g, germplasm.cross_parent cp
                        WHERE cp.germplasm_id = g.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 2
                    ) IS NULL THEN (
                        SELECT g.designation
                        FROM germplasm.germplasm g, germplasm.cross_parent cp
                        WHERE cp.germplasm_id = g.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 1
                    )
                    WHEN (
                        SELECT g.designation
                        FROM germplasm.germplasm g, germplasm.cross_parent cp
                        WHERE cp.germplasm_id = g.id AND 
                        cp.cross_id = "germplasmCross".id AND 
                        cp.order_number = 2
                    ) = '' THEN (
                        SELECT g.designation
                        FROM germplasm.germplasm g, germplasm.cross_parent cp
                        WHERE cp.germplasm_id = g.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 1
                    )
                    ELSE (
                        SELECT g.designation
                        FROM germplasm.germplasm g, germplasm.cross_parent cp
                        WHERE cp.germplasm_id = g.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 2
                    )
                END AS "crossMaleParent",
                -- Copy "femaleParentSeedSource" if "maleParentSeedSource" is null
                CASE
                    WHEN (
                        SELECT s.seed_code 
                        FROM germplasm.seed s, germplasm.cross_parent cp 
                        WHERE cp.seed_id = s.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 2
                    ) = NULL THEN (
                        SELECT s.seed_code 
                        FROM germplasm.seed s, germplasm.cross_parent cp 
                        WHERE cp.seed_id = s.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 1
                    )
                    WHEN (
                        SELECT s.seed_code 
                        FROM germplasm.seed s, germplasm.cross_parent cp 
                        WHERE cp.seed_id = s.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 2
                    ) IS NULL THEN (
                        SELECT s.seed_code 
                        FROM germplasm.seed s, germplasm.cross_parent cp 
                        WHERE cp.seed_id = s.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 1
                    )
                    WHEN (
                        SELECT s.seed_code 
                        FROM germplasm.seed s, germplasm.cross_parent cp 
                        WHERE cp.seed_id = s.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 2
                    ) = '' THEN (
                        SELECT s.seed_code 
                        FROM germplasm.seed s, germplasm.cross_parent cp 
                        WHERE cp.seed_id = s.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 1
                    )
                    ELSE (
                        SELECT s.seed_code 
                        FROM germplasm.seed s, germplasm.cross_parent cp 
                        WHERE cp.seed_id = s.id AND 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.order_number = 2
                    )
                END AS "maleParentSeedSource",
                (
                    SELECT 
                        e.entry_number 
                    FROM 
                        germplasm.cross_parent cp, experiment.entry e
                    WHERE 
                        cp.cross_id = "germplasmCross".id AND 
                        cp.entry_id = e.id AND
                        cp.order_number = 1
                ) AS "femaleParentEntryNumber",
                -- Copy "femaleParentEntryNumber" if "crossMethod" is 'selfing'
                CASE
                    WHEN "germplasmCross".cross_method = 'selfing' THEN (
                        SELECT 
                            e.entry_number 
                        FROM 
                            germplasm.cross_parent cp, experiment.entry e
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.entry_id = e.id AND
                            cp.order_number = 1
                    )
                    ELSE (
                        SELECT 
                            e.entry_number 
                        FROM 
                            germplasm.cross_parent cp, experiment.entry e
                        WHERE 
                            cp.cross_id = "germplasmCross".id AND 
                            cp.entry_id = e.id AND
                            cp.order_number = 2
                    )
                END AS "maleParentEntryNumber",
                (
                    SELECT 
                        e.entry_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN 
                        experiment.plot p ON p.entry_id = crossParent.entry_id AND p.occurrence_id = crossParent.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction e ON e.entry_id = p.entry_id AND e.plot_id = p.id 
                    WHERE 
                        (crossParent.parent_role = 'female' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                        AND e.entry_code is not null
                    LIMIT 1
                ) AS "femaleEntryCode",
                (
                    SELECT 
                        e.entry_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN 
                        experiment.plot p ON p.entry_id = crossParent.entry_id AND p.occurrence_id = crossParent.occurrence_id
                    LEFT JOIN
                        experiment.planting_instruction e ON e.entry_id = p.entry_id AND e.plot_id = p.id 
                    WHERE 
                        (crossParent.parent_role = 'male' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                        AND e.entry_code is not null
                    LIMIT 1
                ) AS "maleEntryCode",
                (
                    SELECT 
                        occurrence_name
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'female' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                ) AS "femaleOccurrenceName",
                (
                    SELECT 
                        occurrence_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'female' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                ) AS "femaleOccurrenceCode",
                (
                    SELECT 
                        occurrence_name
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'male' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                ) AS "maleOccurrenceName",
                (
                    SELECT 
                        occurrence_code
                    FROM 
                        germplasm.cross_parent crossParent
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = crossParent.occurrence_id
                    WHERE 
                        (crossParent.parent_role = 'male' OR crossParent.parent_role='female-and-male') AND 
                        crossParent.cross_id = "germplasmCross".id
                ) AS "maleOccurrenceCode",
            `

            seedSourceInfoTables = `
                LEFT JOIN
                (
                    SELECT
                        e.id AS "femaleSourceEntryDbId",
                        e.entry_code AS "femaleSourceEntry",
                        s.seed_name AS "femaleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role in ('female', 'female-and-male') AND
                        cross_parent.is_void = false AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS femaleEntryTbl ON femaleEntryTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        e.id AS "maleSourceEntryDbId",
                        e.entry_code AS "maleSourceEntry",
                        s.seed_name AS "maleSourceSeedName",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.entry e ON e.id = s.source_entry_id
                    WHERE
                        cross_parent.parent_role in ('male', 'female-and-male') AND
                        cross_parent.is_void = FALSE AND
                        s.is_void = FALSE AND
                        e.is_void = FALSE
                ) AS maleEntryTbl ON maleEntryTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        p.id AS "femaleSourcePlotDbId",
                        p.plot_code AS "femaleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role in ('female', 'female-and-male') AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE
                ) AS femalePlotTbl ON femalePlotTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        p.id AS "maleSourcePlotDbId",
                        p.plot_code AS "maleSourcePlot",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.seed s ON s.id = cross_parent.seed_id
                    LEFT JOIN
                        experiment.plot p ON p.id = s.source_plot_id
                    WHERE
                        cross_parent.parent_role in ('male', 'female-and-male') AND
                        cross_parent.is_void = FALSE AND
                        p.is_void = FALSE
                ) AS malePlotTbl ON malePlotTbl."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        g.parentage AS "femaleParentage",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role in ('female', 'female-and-male') AND
                        g.is_void = FALSE
                ) AS femaleParentage ON femaleParentage."crossDbId" = "germplasmCross".id
                LEFT JOIN
                (
                    SELECT
                        g.parentage AS "maleParentage",
                        cross_parent.cross_id AS "crossDbId"
                    FROM
                        germplasm.cross_parent cross_parent
                    LEFT JOIN
                        germplasm.germplasm g ON g.id = cross_parent.germplasm_id
                    WHERE
                        cross_parent.is_void = FALSE AND
                        cross_parent.parent_role in ('male', 'female-and-male') AND
                        g.is_void = FALSE
                ) AS maleParentage ON maleParentage."crossDbId" = "germplasmCross".id
            `

            seedSourceInfoQuery = `
                femaleEntryTbl."femaleSourceEntryDbId",
                femaleEntryTbl."femaleSourceEntry",
                femaleEntryTbl."femaleSourceSeedName",
                femalePlotTbl."femaleSourcePlotDbId",
                femalePlotTbl."femaleSourcePlot",
                femaleParentage."femaleParentage",
                maleEntryTbl."maleSourceEntryDbId",
                maleEntryTbl."maleSourceEntry",
                maleEntryTbl."maleSourceSeedName",
                malePlotTbl."maleSourcePlotDbId",
                malePlotTbl."maleSourcePlot",
                maleParentage."maleParentage",
            `
        }

        varQuery = `
        LEFT JOIN
        (
            SELECT 
                "crossDbId" 
                ${selectColumns}
            FROM
                CROSSTAB(
                '
                    SELECT 
                        "cross".id AS "crossDbId",
                        cross_data.variable_id,
                        jsonb_build_object(
                            ''dataValue'', cross_data.data_value,
                            ''dataQCCode'', cross_data.data_qc_code
                        )
                    FROM
                        germplasm.cross "cross"
                            LEFT JOIN
                                germplasm.cross_data ON "cross".id = cross_data.cross_id
                    WHERE
                        "cross".entry_list_id = ${entryListDbId}
                        AND cross_data.is_void=false 
                        AND "cross".is_void=false
                    ORDER BY
                        "cross".id
                ',
                '
                    SELECT UNNEST(ARRAY[${variableId}])
                '             
            ) as (
                "crossDbId" integer
                ${crosstabColumns}
            ) 
        )crosstab ON crosstab."crossDbId" = "germplasmCross".id
        `

        // Build the base retrieval query
        let crossesQuery = null
        // Check if the client specified values for fields
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(parameters['fields'])
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                crossesQuery = knex.select(selectString)
            } else {
                crossesQuery = knex.column(parameters['fields'].split('|'))
            }
            
            crossesQuery += `
                FROM
                    germplasm.cross "germplasmCross"
                LEFT JOIN
                    germplasm.germplasm germplasm ON "germplasmCross".germplasm_id = germplasm.id
                LEFT JOIN
                    germplasm.seed seed ON "germplasmCross".seed_id = seed.id
                LEFT JOIN
                    experiment.experiment experiment ON "germplasmCross".experiment_id = experiment.id
                LEFT JOIN
                    experiment.entry entry ON "germplasmCross".entry_id = entry.id
                LEFT JOIN
                    tenant.person creator ON "germplasmCross".creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON "germplasmCross".modifier_id = modifier.id
                LEFT JOIN 
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id AND occurrence.is_void = FALSE
                LEFT JOIN
                    experiment.entry_list entryList ON "germplasmCross".entry_list_id = entryList.id AND entryList.is_void = FALSE
                LEFT JOIN 
                    experiment.location_occurrence_group location_occurrence_group ON location_occurrence_group.occurrence_id = occurrence.id AND location_occurrence_group.is_void = FALSE
                LEFT JOIN 
                    experiment.location AS location ON location.id = location_occurrence_group.location_id AND location.is_void = FALSE
                ${varQuery}
                ${seedSourceInfoTables}
                WHERE
                    "germplasmCross".is_void = FALSE AND
                    "germplasmCross".entry_list_id = ${entryListDbId}
                    ${addedOrderString}
              `
        } else {
            crossesQuery = `
                SELECT
                    "germplasmCross".id AS "crossDbId",
                    "germplasmCross".cross_name AS "crossName",
                    "germplasmCross".cross_method AS "crossMethod",
                    "germplasmCross".remarks AS "crossRemarks",
                    "germplasmCross".is_method_autofilled AS "isMethodAutofilled",
                    "germplasmCross".harvest_status AS "harvestStatus"
                    ${selectColumns},
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation AS "germplasmDesignation",
                    germplasm.parentage AS "germplasmParentage",
                    germplasm.generation AS "germplasmGeneration",
                    entry.id AS "entryDbId",
                    entry.entry_number AS "entryNo",
                    entry.entry_name AS "entryName",
                    entry.entry_role AS "entryRole",
                    entryList.id AS "entryListDbId",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_code AS "experimentCode",
                    experiment.data_process_id AS "experimentDataProcessId",
                    "germplasmCross".occurrence_id AS "occurrenceDbId",
                    (
                        SELECT
                            item.display_name
                        FROM
                            master.item item
                        WHERE
                            item.is_void = FALSE AND
                            item.id = experiment.data_process_id
                    ) AS "experimentTemplate",
                    ${includeParentSourceQuery}
                    "parentDbId",
                    ${seedSourceInfoQuery}
                    location.id AS "locationDbId",
                    location.location_code AS "locationCode",
                    "germplasmCross".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    "germplasmCross".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifierDbId"
                FROM
                    germplasm.cross "germplasmCross"
                LEFT JOIN
                    germplasm.germplasm germplasm ON "germplasmCross".germplasm_id = germplasm.id
                LEFT JOIN
                    germplasm.seed seed ON "germplasmCross".seed_id = seed.id
                LEFT JOIN
                    experiment.experiment experiment ON "germplasmCross".experiment_id = experiment.id
                LEFT JOIN
                    experiment.entry entry ON "germplasmCross".entry_id = entry.id
                LEFT JOIN
                    tenant.person creator ON "germplasmCross".creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON "germplasmCross".modifier_id = modifier.id
                LEFT JOIN
                    experiment.entry_list entryList ON "germplasmCross".entry_list_id = entryList.id AND entryList.is_void = FALSE
                LEFT JOIN 
                    experiment.location_occurrence_group location_occurrence_group ON location_occurrence_group.occurrence_id = "germplasmCross".occurrence_id
                LEFT JOIN 
                    experiment.location AS location ON location.id = location_occurrence_group.location_id AND location.is_void = FALSE
                ${varQuery}
                ${seedSourceInfoTables}
                WHERE
                    "germplasmCross".is_void = FALSE AND
                    "germplasmCross".entry_list_id = ${entryListDbId}
                    ${addedOrderString}
            `
        }   

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderStringTrait(
                sort,
                selectColumns.replaceAll('"', '').split(',')
            )

            if (orderString && orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        crossesQuery = crossesQuery.replace('"parentDbId"', `
            (
                SELECT array_to_string(array_agg(entry_id order by order_number) ,',') 
                FROM germplasm.cross_parent
                WHERE cross_id = "germplasmCross".id
            ) AS "parentDbId"
        `)
        
        // Generate final SQL query
        let crossesFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                crossesQuery,
                conditionString,
                orderString,
            )

        //retrieve parent data records
        let crossesData = await sequelize.query(crossesFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset,
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        // If no records were retrieved
        if (await crossesData.length < 1 || await crossesData == undefined) {
            res.send(200, {
                rows: crossesData,
                count: 0
            })
            return
        }

        let crossesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                crossesQuery,
                conditionString,
            )

        let crossesCount = await sequelize
            .query(crossesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                return undefined
            })

        if (crossesCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        count = crossesCount[0].count

        res.send(200, {
            rows: crossesData,
            count: count
        })

        return
    }
}