/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')

module.exports = {

    /**
     * POST /v3/entry-lists/:id/reorder-entries reorders a specific and existing entry list
     * 
     * @param entryListDbId integer as a path parameter 
     * @param token string token
     * 
     * @return object response data
     */

    post: async function (req, res, next) {
        
        let entryListDbId = req.params.id

        if (!validator.isInt(entryListDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })
            
            let reorderEntriesQuery = `
                UPDATE
                    experiment.entry ee
                SET
                    entry_number = t.num,
                    entry_code = t.num,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                FROM
                    (
                        SELECT
                            ee.id,
                            ee.entry_number,
                            ee.modification_timestamp,
                            ROW_NUMBER() OVER(
                                ORDER BY 
                                    ee.entry_number, 
                                    ee.modification_timestamp
                            ) AS num
                        FROM
                            experiment.entry ee
                        WHERE
                            ee.is_void = FALSE AND
                            ee.entry_list_id = ${entryListDbId}
                    ) AS t
                WHERE
                    ee.is_void = FALSE AND
                    ee.id = t.id
            `

            await sequelize.query(reorderEntriesQuery, {
                type: sequelize.QueryTypes.UPDATE
            })
            
            let resultArray = {
                entryListDbId: entryListDbId
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}