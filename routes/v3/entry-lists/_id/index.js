/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
let logger = require('../../../../helpers/logger')
const endpoint = 'entry-lists/:id'

module.exports = {

    // Endpoint for updating an existing entry list record
    // PUT /v3/entry-lists/:id
    put: async function (req, res, next) {
        // Retrieve entry list ID
        let entryListDbId = req.params.id

        if (!validator.isInt(entryListDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400021)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let entryListName = null
        let description = null
        let entryListStatus = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        let entryListQuery = `
            SELECT
                entryList.id AS "entryListDbId",
                entryList.experiment_id AS "experimentDbId",
                entryList.entry_list_name AS "entryListName",
                entryList.description,
                entryList.entry_list_status "entryListStatus",
                entryList.experiment_year AS "experimentYear",
                entryList.season_id AS "seasonDbId",
                entryList.site_id AS "siteDbId",
                entryList.id AS "creatorDbId"
            FROM
                experiment.entry_list entryList
            WHERE
                entryList.is_void = FALSE AND
                entryList.id = ${entryListDbId}
        `

        let entryList = await sequelize.query(entryListQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (entryList == undefined || entryList.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404006)
            res.send(new errors.NotFoundError(errMsg))
            return
        }
    
        let recordCreatorDbId = entryList[0].creatorDbId
        let recordEntryListDbId = entryList[0].entryListDbId
        let recordExperimentDbId = entryList[0].experimentDbId
        let recordEntryListName = entryList[0].entryListName
        let recordEntryListStatus = entryList[0].entryListStatus
        let recordDescription = entryList[0].description
        let recordExperimentYear = entryList[0].experimentYear
        let recordSeasonDbId = entryList[0].seasonDbId
        let recordSiteDbId = entryList[0].siteDbId
        let experimentYearRegex = /^\d{4}$/
    
        // Check if user is an admin or an owner of the entry list
        if(!isAdmin && (personDbId != recordCreatorDbId)) {

            let tableName = 'entry_list'
            let recordId = recordEntryListDbId

            if(recordExperimentDbId !== null){
                tableName = 'experiment'
                recordId = recordExperimentDbId
            }

            let programMemberQuery = `
            SELECT 
                person.id,
                person.person_role_id AS "role"
            FROM 
                tenant.person person
            LEFT JOIN 
                tenant.team_member teamMember ON teamMember.person_id = person.id
            LEFT JOIN 
                tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
            LEFT JOIN 
                tenant.program program ON program.id = programTeam.program_id 
            LEFT JOIN
                experiment.`+ tableName + ` ` + tableName + ` ON ` + tableName + `.program_id = program.id
            WHERE 
                `+ tableName + `.id = ${recordId} AND
                person.id = ${personDbId} AND
                person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let entryListUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/entry-lists/"
            + entryListDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body    

        // Start transaction
        let transaction
        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            if (data.entryListName !== undefined) {
                entryListName = data.entryListName.trim()
        
                if (entryListName != recordEntryListName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_list_name = $$${entryListName}$$
                    `
                }
            }

            if (data.entryListStatus !== undefined) {
                entryListStatus = data.entryListStatus.trim()
        
                if (entryListStatus != recordEntryListStatus) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_LIST_STATUS' AND
                                scaleValue.value ILIKE $$${entryListStatus}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_list_status = $$${entryListStatus}$$
                    `
                }
            }

            if (data.description !== undefined) {
                description = data.description.trim()
        
                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        description = $$${description}$$
                    `
                }
            }

            if (data.experimentYear !== undefined) {
                const experimentYear = data.experimentYear

                if (experimentYear != recordExperimentYear) {
                    if (!validator.isInt(experimentYear)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400129)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
    
                    if (!experimentYear.match(experimentYearRegex)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400144)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_year = $$${experimentYear}$$
                    `
                }
            }

            if (data.seasonDbId !== undefined) {
                const seasonDbId = data.seasonDbId

                if (seasonDbId != recordSeasonDbId) {
                    if (!validator.isInt(seasonDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400063)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate season ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if season is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                tenant.season
                            WHERE 
                                is_void = FALSE
                            AND
                                id = ${seasonDbId}
                        )
                    `
                    validateCount += 1
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        season_id = $$${seasonDbId}$$
                    `
                }
            }

            if (data.siteDbId !== undefined) {
                const siteDbId = data.siteDbId

                if (siteDbId != recordSiteDbId) {

                    if (!validator.isInt(siteDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400064)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate site ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if site is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object site
                            WHERE 
                                site.is_void = FALSE
                            AND
                                site.id = ${siteDbId}
                        )
                    `
                    validateCount += 1
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        site_id = $$${siteDbId}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the experiment block record
            let updateEntryListQuery = `
                UPDATE
                    experiment.entry_list  
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${entryListDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateEntryListQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return the transaction info
            let resultArray = {
                entryListDbId: entryListDbId,
                recordCount: 1,
                href: entryListUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing entry list record
    // DELETE /v3/entry-lists/:id
    delete: async function (req, res, next) {

        // Retrieve entry list ID
        let entryListDbId = req.params.id

        if (!validator.isInt(entryListDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400021)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            let entryListQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    experiment.id AS "experimentDbId",
                    entryList.entry_list_code AS "entryListCode"
                FROM
                    experiment.entry_list entryList
                LEFT JOIN
                    tenant.person creator ON creator.id = entryList.creator_id
                LEFT JOIN
                    experiment.experiment ON experiment.id = entryList.experiment_id
                WHERE
                    entryList.is_void = FALSE AND
                    entryList.id = ${entryListDbId}
            `

            let entryList = await sequelize.query(entryListQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (entryList == undefined || entryList.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404006)
                res.send(new errors.NotFoundError(errMsg))
                return
            }
        
            let recordCreatorDbId = entryList[0].creatorDbId
            let recordExperimentDbId = entryList[0].experimentDbId
            let recordEntryListCode = entryList[0].entryListCode
        
            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${recordExperimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteEntryListQuery = format(`
                UPDATE
                    experiment.entry_list
                SET
                    is_void = TRUE,
                    entry_list_code = 'VOIDED-${entryListDbId}'
                WHERE
                    id = ${entryListDbId}
            `)
            // Get transaction
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(deleteEntryListQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            res.send(200, {
                rows: { 
                    entryListDbId: entryListDbId 
                }
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}