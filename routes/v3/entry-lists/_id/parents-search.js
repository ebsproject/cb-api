/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let logger = require('../../../../helpers/logger')

module.exports = {
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort

        let orderString = ''
        let addedOrderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let includeParentCountQuery = ''

        //get the parent id from link
        let entryListDbId = req.params.id
        const endpoint = 'entry-lists/:id/parents-search'

        //validation of parent id
        if (!validator.isInt(entryListDbId)) {
          let errMsg =
            await errorBuilder.getError(req.headers.host, 400021)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        //validation for entryList if its existing
        let entryListQuery = `
            SELECT 
                entlist.id
            FROM 
                experiment.entry_list entlist
            WHERE 
                entlist.is_void = FALSE AND 
                entlist.id = ${entryListDbId}
        `
        let entryList = await sequelize.query(entryListQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 404006)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper.getDistinctString(parameters['distinctOn'])
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                        ${parameters['distinctOn']}
                `
            }

            // Set the excluded parameters
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
          
            //count how many used in crosses
            if(req.body.includeParentCount != undefined && req.body.includeParentCount == true) {
                includeParentCountQuery = `
                    (
                        SELECT 
                            COUNT (1)
                        FROM 
                            germplasm.cross_parent cp
                        JOIN 
                            germplasm.germplasm g ON cp.germplasm_id = g.id
                        JOIN 
                            germplasm.cross c ON cp.cross_id = c.id
                        WHERE 
                            cp.germplasm_id = germplasm.id AND
                            c.entry_list_id = entlist.id AND
                            g.is_void = FALSE AND
                            c.is_void = FALSE AND
                            cp.is_void = FALSE AND
                            (cp.parent_role = 'female' OR cp.parent_role = 'female-and-male')
                    ) AS "femaleCount",
                    (
                        SELECT 
                            COUNT (1)
                        FROM 
                            germplasm.cross_parent cp
                        JOIN 
                            germplasm.germplasm g ON cp.germplasm_id = g.id
                        JOIN 
                            germplasm.cross c ON cp.cross_id = c.id
                        WHERE 
                            cp.germplasm_id = germplasm.id AND
                            c.entry_list_id = entlist.id AND
                            g.is_void = FALSE AND
                            c.is_void = FALSE AND
                            cp.is_void = FALSE AND
                            (cp.parent_role = 'male' OR cp.parent_role = 'female-and-male') 
                    ) AS "maleCount",
                `
            }
        }

        let parentDataQuery = null
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )

                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                parentDataQuery = knex.select(selectString)
            } else {
                parentDataQuery = knex.column(parameters['fields'].split('|'))
            }

            parentDataQuery +=`
                FROM 
                    experiment.parent parent
                LEFT JOIN 
                    experiment.entry_list entlist ON parent.entry_list_id = entlist.id
                LEFT JOIN 
                    experiment.experiment experiment ON parent.experiment_id = experiment.id
                LEFT JOIN 
                    experiment.occurrence occ ON parent.occurrence_id = occ.id
                LEFT JOIN 
                    experiment.entry entry ON parent.entry_id = entry.id
                LEFT JOIN 
                    tenant.person creator ON parent.creator_id = creator.id
                LEFT JOIN 
                    tenant.person modifier ON parent.modifier_id = modifier.id
                LEFT JOIN 
                    germplasm.germplasm germplasm ON entry.germplasm_id = germplasm.id
                LEFT JOIN
                    germplasm.seed seed ON entry.seed_id = seed.id
                LEFT JOIN
                    germplasm.package package ON entry.package_id = package.id
                WHERE
                    entlist.is_void = FALSE AND 
                    entry.is_void = FALSE AND 
                    germplasm.is_void = FALSE AND 
                    seed.is_void = FALSE AND 
                    occ.is_void = FALSE AND 
                    parent.is_void = FALSE AND
                    experiment.is_void = FALSE AND
                    entlist.id = ${entryListDbId}
            `+ addedOrderString
        }
        else{
            parentDataQuery = `
                SELECT
                    ${addedDistinctString}
                    parent.id AS "parentDbId",
                    package.package_label AS "packageLabel",
                    germplasm.id AS "germplasmDbId",
                    germplasm.germplasm_code AS "germplasmCode",
                    (SELECT designation
                        FROM germplasm.germplasm g
                        WHERE g.is_void = false
                            AND g.id = (SELECT germplasm_id 
                                FROM experiment.planting_instruction pi
                                LEFT JOIN 
                                    experiment.plot p ON p.entry_id = pi.entry_id
                                        AND p.id = pi.plot_id
                                WHERE pi.entry_id = parent.entry_id
                                    AND p.occurrence_id = occ.id
                                    AND p.is_void = false
                                    AND pi.is_void = false
                                LIMIT 1
                            )
                        LIMIT 1
                    ),
                    ${includeParentCountQuery}
                    germplasm.parentage,
                    germplasm.germplasm_type AS "germplasmType",
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.generation,
                    (SELECT seed_id 
                        FROM experiment.planting_instruction pi
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = pi.entry_id
                                AND p.id = pi.plot_id
                        WHERE pi.entry_id = parent.entry_id
                            AND p.occurrence_id = occ.id
                            AND p.is_void = false
                            AND pi.is_void = false
                        LIMIT 1
                    ) AS "seedDbId",
                    seed.seed_name AS "seedName",
                    parent.order_number AS "orderNumber",
                    parent.parent_type AS "parentType",
                    parent.parent_role AS "parentRole",
                    parent.plot_id AS "plotDbId",
                    parent.description,
                    entlist.id AS "entryListDbId",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_code AS "experimentCode",
                    occ.id AS "occurrenceDbId",
                    occ.occurrence_name AS "occurrenceName",
                    occ.occurrence_code AS "occurrenceCode",
                    entry.id AS "entryDbId",
                    (SELECT entry_code
                        FROM experiment.planting_instruction pi
                        LEFT JOIN 
                            experiment.plot p ON p.entry_id = pi.entry_id
                                AND p.id = pi.plot_id
                        WHERE pi.entry_id = parent.entry_id
                            AND p.occurrence_id = occ.id
                            AND p.is_void = false
                            AND pi.is_void = false
                        LIMIT 1
                    ) AS "entryCode",
                    entry.entry_number AS "entryNumber",
                    parent.creation_timestamp AS "creationTimeStamp",
                    creator.id AS "creatorDbId",
                    parent.modification_timestamp AS "modificationTimeStamp",
                    modifier.id AS "modifierDbId"
                FROM 
                    experiment.parent parent
                LEFT JOIN 
                    experiment.entry_list entlist ON parent.entry_list_id = entlist.id
                LEFT JOIN 
                    experiment.experiment experiment ON parent.experiment_id = experiment.id
                LEFT JOIN 
                    experiment.occurrence occ ON parent.occurrence_id = occ.id
                LEFT JOIN 
                    experiment.entry entry ON parent.entry_id = entry.id
                LEFT JOIN 
                    tenant.person creator ON parent.creator_id = creator.id
                LEFT JOIN 
                    tenant.person modifier ON parent.modifier_id = modifier.id
                LEFT JOIN 
                    germplasm.germplasm germplasm ON entry.germplasm_id = germplasm.id
                LEFT JOIN
                    germplasm.seed seed ON entry.seed_id = seed.id
                LEFT JOIN
                    germplasm.package package ON entry.package_id = package.id
                WHERE
                    entlist.is_void = FALSE AND 
                    entry.is_void = FALSE AND 
                    germplasm.is_void = FALSE AND 
                    seed.is_void = FALSE AND
                    occ.is_void = FALSE AND 
                    parent.is_void = FALSE AND
                    experiment.is_void = FALSE AND
                    entlist.id = ${entryListDbId}
            `+ addedOrderString
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Generate the final SQL query
        let parentDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
          parentDataQuery,
          conditionString,
          orderString
        )

        //retrieve parent data records
        let parentData = await sequelize.query(parentDataFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset,
            }
        })
        .catch(async err => {
            await logger.logFailingQuery(endpoint, 'SELECT', err)
        })

        if (parentData == undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        // Get the final count of the parent data records
        let parentDataCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            parentDataQuery,
            conditionString,
            orderString
        )
        
        let parentDataCount = await sequelize.query(parentDataCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        count = parentDataCount[0].count

        res.send(200, {
          rows: parentData,
          count:count
        })
        return
    }
}