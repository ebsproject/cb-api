/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/entry-lists/:id/data
    get: async function (req, res, next) {

        // Retrieve the entry list ID
        let entryListDbId = req.params.id

        // Check if the ID is valid
        if (!validator.isInt(entryListDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400021)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {

            // Build query

            let entryListQuery = `
                SELECT
                    "entryList".id AS "entryListDbId",
                    "entryList".entry_class AS "entryClass",
                    "entryList".entry_status AS "entryStatus",
                    product.id AS "productDbId",
                    "entryList".entlistno AS "entryListNo",
                    "entryList".entry_type AS "entryType",
                    author.id AS "authorDbId",
                    author.display_name AS "author",
                    "seedStorage".id AS "seedStorageDbId",
                    "seedStorage".key_type AS "keyType",
                    "seedStorage".label AS "seedStorageLabel",
                    "seedStorage".seed_manager AS "seedManager",
                    "seedStorage".unit AS "seedStorageVolumeUnit",
                    "entryList".gid,
                    "entryList".product_name AS "productName",
                    "entryList".remarks,
                    "entryList".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    "entryList".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier
                FROM
                    operational.entry_list "entryList"
                LEFT JOIN 
                    master.product product ON product.id = "entryList".product_id
                LEFT JOIN 
                    master.user author ON author.id =  "entryList".author_id::integer
                LEFT JOIN 
                    operational.seed_storage_log AS "seedStorageLog" ON "seedStorageLog".id = "entryList".seed_storage_log_id
                LEFT JOIN 
                    operational.seed_storage AS "seedStorage" ON "seedStorage".id = "seedStorageLog".seed_storage_id
                LEFT JOIN 
                    master.user creator on creator.id = "entryList".creator_id
                LEFT JOIN 
                    master.user modifier on modifier.id = "entryList".modifier_id 
                WHERE
                    "entryList".is_void = FALSE
                    AND "entryList".id = (:entryListDbId)
            `

            // Retrieve entry list from the database
            let entryLists = await sequelize.query(entryListQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    entryListDbId: entryListDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await entryLists == undefined || await entryLists.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404006)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (entryList of entryLists) {
                let entryListDataQuery = `
                    SELECT
                        "entryListData".variable_id AS "variableDbId",
                        variable.abbrev AS "variableAbbrev",
                        variable.label AS "variableLabel",
                        "entryListData".value AS "variableValue",
                        "entryListData".creation_timestamp AS "creationTimestamp",
                        creator.id AS "creatorDbId",
                        creator.display_name AS creator,
                        "entryListData".modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.display_name AS modifier
                    FROM operational.entry_list_data "entryListData"
                    LEFT JOIN 
                        master.variable variable ON variable.id = "entryListData".variable_id
                    LEFT JOIN 
                        master.user creator ON "entryListData".creator_id = creator.id
                    LEFT JOIN 
                        master.user modifier ON "entryListData".modifier_id = modifier.id
                    WHERE 
                        "entryListData".is_void = FALSE
                        AND "entryListData".entry_list_id = (:entryListDbId)
                    ORDER BY variable.abbrev
                `

                let entryListData = await sequelize.query(entryListDataQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        entryListDbId: entryList["entryListDbId"]
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                entryList["data"] = entryListData
            }

            res.send(200, {
                rows: entryLists
            })
            return
        }
    }
}