/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async (req, res, next) => {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let params = req.query
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields', 'distinctOn']
      // Get filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(
            parameters['distinctOn']
          )
        parameters['distinctOn'] = `"${parameters['distinctOn']}",`
      }
    }
    // Build the base retrieval query
    let seasonsQuery = null
    // Check if the client specified values for the fields
    if (parameters['fields'] != null) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters['fields'],
        )
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        seasonsQuery = knex.select(selectString)
      } else {
        seasonsQuery = knex.column(parameters['fields'].split('|'))
      }
      seasonsQuery += `
        FROM
          tenant.season season
        LEFT JOIN
          tenant.person creator ON creator.id = season.creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = season.modifier_id
        WHERE 
          season.is_void = FALSE
          ` + addedConditionString + `
        ORDER BY
          ${parameters['distinctOn']}
          season.id
      `
    } else {
      let selectClause = `
        SELECT 
          ${addedDistinctString}
          season.id as "seasonDbId",
          season.season_code as "seasonCode",
          season.season_name as "seasonName",
          season.description,
          season.is_active as "isActive",
          season.creation_timestamp as "creationTimestamp",
          season.creator_id AS "creatorDbId",
          creator.person_name as creator,
          season.modification_timestamp as "modificationTimestamp",
          season.modifier_id as "modifierDbId",
          modifier.person_name as modifier
        FROM tenant.season season
        LEFT JOIN 
          tenant.person creator ON season.creator_id = creator.id
        LEFT JOIN 
          tenant.person modifier ON season.modifier_id = modifier.id
      `

      // Check whether API caller needs both active and inactive seasons
      let whereClause
      if (params.forDataBrowser !== undefined || conditionString.includes('isActive')) {
        whereClause = `
          WHERE 
            season.is_void = FALSE
        `
      } else {
        whereClause = `
          WHERE 
            season.is_void = FALSE
            AND season.is_active = TRUE
        `
      }

      let orderByClause = `
        ORDER BY
          ${parameters['distinctOn']}
          season.id
      `

      seasonsQuery = selectClause + whereClause + orderByClause
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    seasonsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      seasonsQuery,
      conditionString,
      orderString
    )
    // Retrieve the team records
    let seasons = await sequelize
      .query(seasonsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await seasons == undefined || await seasons.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get the final count of the season records retrieved
      let seasonsCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          seasonsQuery,
          conditionString,
          orderString
        )
      let seasonsCount = await sequelize
        .query(
          seasonsCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = seasonsCount[0].count
    }

    res.send(200, {
      rows: seasons,
      count: count
    })
    return
  }
}