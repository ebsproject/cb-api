/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token.js')
let validator = require('validator')
let errorBuilder = require('../../../helpers/error-builder')
let seedlotHelper = require('../../../helpers/seedlot/index.js')
let germplasmHelper = require('../../../helpers/germplasm/index.js')

let cowpeaHarvestHelper = require('../../../helpers/seedlot/harvest-cowpea.js');

const logger = require('../../../helpers/logger')

/**
 * Get the variable id of the supplied abbrev
 * @param {*} abbrev 
 */
async function getVariableId(abbrev) {
    try {
        let variableQuery = `
            SELECT 
                id AS "variableDbId" 
            FROM 
                master.variable
            WHERE 
                abbrev = '${abbrev}' AND is_void = FALSE
        `
        // Retrieve taxonomy_id for taxonomy_name = "Unknown"
        let variableDbId = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return variableDbId[0].variableDbId
    }
    catch (err){
        return false
    }
}

/**
 * Get new counter value of the given name
 * Ex. If name is IR 12345-4
 * counter value should be 5
 * @param {*} name - the last name in the sequence
 * @param {*} delimiter - delimiter of the name separating the number
 * @param {*} optional - optional values such as counter prefix and suffix
 */
async function getCounterValue(name, delimiter, optional = {}, preDelimiter = '') {
    let prefix = optional.prefix == null ? '' : optional.prefix
    let suffix = optional.suffix == null ? '' : optional.suffix

    let counter = 1

    // If no name was provided, return 1
    if(name == null) {
        return counter
    }

    let lastPiece = name
    if (delimiter != '') {
        // split name by the delimiter
        let pieces =  name.split(delimiter)

        // get the last piece
        // ex. in "IR 12345-B-2"
        // the last piece is "2"
        lastPiece = pieces[pieces.length - 1]
    }

    // check for repeating text
    if (optional.freeTextRepeater == 'yes') {
        let freeTextDelimiter = optional.freeTextDelimiter
        let freeTextValue = optional.freeTextValue
        let regexExp = new RegExp('^'+freeTextValue+'('+freeTextDelimiter+'\\d*)?$');

        // check if B<delimiter>counter is present
        if (lastPiece.match(regexExp)) {
            let repeatingText = lastPiece.match(regexExp)[0]
            
            // Split text by delimiter and parse it as integer
            let delimiterRegex = new RegExp(freeTextDelimiter);
            let currentNum = parseInt(repeatingText.split(delimiterRegex)[1])

            // if paresed number is NOT NaN, increment by 1 and return 
            if (!Number.isNaN(currentNum)) {
                return currentNum + 1
            }
            else {
                return counter + 1
            }
        } else {
            return counter
        }
    }

    // Get last number
    lastPiece = parseInt(
        lastPiece.replace(prefix,"")
            .replace(new RegExp(suffix + '$'),"")
            .replace(preDelimiter,"")
    )

    // if last piece is not an integer, return the counter
    if(lastPiece ==  NaN) {
        return counter
    }
    
    // add 1 to the last piece and set it as counter's value
    counter = lastPiece + 1

    return counter
}

/**
 * Assembles the seed info for insertion
 * @param {string} designation - designation of new germplasm record
 * @param {string} generation - generation of the new germplasm record
 * @param {object} plotRecord - plot info of the harvest
 * @param {string} germplasmNameType - germplasm name type of new germplasm record
 * @returns {object} germplasmData - assembled germplasm info for insertion
 */
async function assembleGermplasmData(designation, generation, plotRecord, germplasmNameType) {
    let germplasmData = []

    germplasmData.designation = designation
    germplasmData.generation = generation
    germplasmData.parentage = plotRecord.germplasmParentage
    germplasmData.germplasmState = plotRecord.germplasmState
    germplasmData.germplasmType = plotRecord.germplasmType
    germplasmData.germplasmNameType = germplasmNameType
    germplasmData.cropId = plotRecord.cropDbId
    germplasmData.taxonomyId = plotRecord.germplasmTaxonomyId

    return germplasmData
}

/**
 * Assembles the seed info for insertion
 * @param {string} seedName - name of the new seed record
 * @param {object} harvestData - harvest data of the harvested material
 * @param {object} plotRecord - plot info of the harvest
 * @param {object} germplasmRecord - germplasm information
 * @param {string} harvestSource - 'plot' or 'cross
 * @returns {object} seedData - assembled seed info for insertion
 */
async function assembleSeedData(seedName, harvestData, plotRecord, germplasmRecord, harvestSource) {
    let seedData = []

    seedData.germplasmDbId = plotRecord.germplasmDbId
    if(germplasmRecord != null && "germplasmDbId" in germplasmRecord && germplasmRecord.germplasmDbId !== undefined) {
        seedData.germplasmDbId = germplasmRecord.germplasmDbId
    }
    seedData.seedName = seedName
    seedData.harvestDate = harvestData.harvest_date
    seedData.harvestMethod = harvestData.harvest_method
    seedData.programDbId = plotRecord.programDbId
    seedData.sourceExperimentDbId = plotRecord.experimentDbId
    seedData.sourceEntryDbId = plotRecord.entryDbId
    seedData.sourcePlotDbId = plotRecord.plotDbId
    seedData.sourceOccurrenceDbId = plotRecord.occurrenceDbId
    seedData.sourceLocationDbId = plotRecord.locationDbId
    seedData.crossDbId = plotRecord.crossDbId
    seedData.harvestSource = harvestSource

    return seedData
}

async function assemblePackageData(packageLabel, plotRecord, seedRecord) {
    let packageData = []

    packageData.packageLabel = packageLabel
    packageData.seedDbId = seedRecord.seedDbId
    packageData.programDbId = plotRecord.programDbId

    return packageData
}

/**
 * Checks for transgenic and/or genome_edited parent
 * 
 * @param {String} fParentGermplasmType germplasm type of female parent
 * @param {String} mParentGermplasmType germplasm type of male parent
 * @returns {Object} mixed
 */
async function checkTransGenParent(fParentGermplasmType,mParentGermplasmType){
    // Additional validation for possible transgenic / genome_edited parents
    let modifiedHarvest = false
    let childGermplasmType = ''

    let optional = {
        additionalCondition: "default"
    }
    
    let transGenEdGermplasmTypeArr = ['transgenic','genome_edited']

    // If one parent is transgenic or genome-edited (germplasm_type)
    if( (transGenEdGermplasmTypeArr.includes(fParentGermplasmType) && 
        !transGenEdGermplasmTypeArr.includes(mParentGermplasmType)) || 
        (transGenEdGermplasmTypeArr.includes(mParentGermplasmType) &&
        !transGenEdGermplasmTypeArr.includes(fParentGermplasmType)) ){
        modifiedHarvest = true

        childGermplasmType = transGenEdGermplasmTypeArr.includes(mParentGermplasmType) ? mParentGermplasmType : fParentGermplasmType
    }

    // If both parents are transgenic or genome-edited, 
    // AND they have the same germplasm type
    if( transGenEdGermplasmTypeArr.includes(fParentGermplasmType) && 
        transGenEdGermplasmTypeArr.includes(mParentGermplasmType) && 
        fParentGermplasmType == mParentGermplasmType){
        modifiedHarvest = true

        childGermplasmType = fParentGermplasmType
    }

    // If both parents are transgenic or genome-edited, 
    // AND they have different germplasm types
    if( transGenEdGermplasmTypeArr.includes(fParentGermplasmType) && 
        transGenEdGermplasmTypeArr.includes(mParentGermplasmType) && 
        fParentGermplasmType != mParentGermplasmType){
        return false
    }

    if(modifiedHarvest){
        optional = {
            additionalCondition: childGermplasmType
        }
    }

    return {
        'modifiedHarvest': modifiedHarvest,
        'optional': optional,
        'childGermplasmType': childGermplasmType
    }
}

/**
 * Harvest use case for Single/Double/Three-way/Complex Cross Bulk Rice
 * Updates the cross name 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData - harvest data of the cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestSingleCrossBulkRice(crossRecord, harvestData, userId, harvestSource, prefixes, optional) {
    // if no_of_seed = 0, do not create germplasm, seeds, and packages
    if (harvestData.no_of_seed == 0) return true

    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        let newCrossName
        let newGermplasmDbId
        let existingChildGermplasmDbId
        let existingChildGermplasm

        // Check germplasm type of parents for transgenic and/or genome_edited
        let parentsCheck = await checkTransGenParent(femaleParentGermplasm.germplasmType,maleParentGermplasm.germplasmType)
        // If check failed, return false
        if(!parentsCheck){
            return false
        }
        // Unpack parentsCheck
        let modifiedHarvest = parentsCheck.modifiedHarvest != undefined ? parentsCheck.modifiedHarvest : false
        let optional = parentsCheck.optional != undefined ? parentsCheck.optional : optional
        let childGermplasmType = parentsCheck.childGermplasmType != undefined ? parentsCheck.childGermplasmType : 'progeny'

        // Check if the pair already has a child from within the same program.
        existingChildGermplasmDbId = await seedlotHelper.getChildGermplasmId(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId,
            true,
            true, 
            [
                `program_code = '${crossRecord.programCode}'`,
            ]
        )

        // Get existing child germplasm if it exists
        if (existingChildGermplasmDbId != 0) {
            existingChildGermplasm = await seedlotHelper.getGermplasmInfo(existingChildGermplasmDbId)
        }

        // 1. Update cross name
        if (existingChildGermplasm != null) {
            // If child germplasm exists, use its designation as the new cross name
            newCrossName = existingChildGermplasm.designation
        }
        else {
            // Else, build a new cross name
            newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray, optional)
        }
        let values = [
            `cross_name = '${newCrossName}'`
        ]

        result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
        if(!result) return false
        entitiesArray.cross.crossName = newCrossName

        if (existingChildGermplasm != null) {
            // If child germplasm exists, use it as the germplasm for seed and package creation
            newGermplasmDbId = existingChildGermplasmDbId
        }
        else {
            // Create new designation
            let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)

            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }

            // 2.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = await seedlotHelper.riceParentageProcess(femaleParentGermplasm, maleParentGermplasm, crossRecord.crossMethod)
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_type'] = modifiedHarvest ? childGermplasmType : "progeny"
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['germplasm_name_type'] = 'cross_name'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if(!result) return false
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmInfo(newGermplasmDbId)

            // 2.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'cross_name'
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // Create backcross derivative name
            let occurrenceDbId = entitiesArray.cross.occurrenceDbId
            let occurrenceCrossCount = await seedlotHelper.getOccurrenceCrossCount(occurrenceDbId)
            // Criteria:
            //      IF the occurrence only has one cross, AND the project code is set for the experiment
            //      THEN use the project code for the backcross derivative name
            //      ELSE use the cross name for the backcross derivative name
            let withProjectCode = occurrenceCrossCount == 1 && entitiesArray.cross.projectCode != null
            let bcDerivativeName = await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray, {
                additionalCondition: withProjectCode ? 'with_project_code' : 'without_project_code'
            })
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = bcDerivativeName
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_BACKCROSS_DERIVATIVE')
            germplasmNameInfo['germplasm_name_status'] = 'active'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            
            // 2.3 Create germplasm relation records
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // Create germplasm attribute records
            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }
            // GERMPLASM YEAR
            if(crossRecord.crossingDate != null) {
                result = await seedlotHelper.createGermplasmYear(
                    newGermplasmDbId, 
                    crossRecord, 
                    userId
                )
            }
            // MTA STATUS
            result = await seedlotHelper.insertRecord(
                'germplasm',
                'germplasm_attribute',
                {
                    germplasm_id: newGermplasmDbId,
                    variable_id: await getVariableId('MTA_STATUS'),
                    data_value: 'PUD2',
                    data_qc_code: 'G'
                },
                userId
            )
            // ORIGIN
            await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)

            // 1.4.a. Create new family
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let newFamilyCode = await seedlotHelper.generateFamilyCode()
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // 3.1 Create Seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id

        // 3.2 Create seed attribute
        let seedAttributeInfo = {}
        seedAttributeInfo['seed_id'] = newSeedDbId
        seedAttributeInfo['variable_id'] = await getVariableId('USE')
        seedAttributeInfo['data_value'] = 'Active'
        seedAttributeInfo['data_qc_code'] = 'N'
        result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)
        if(!result) return false

        // 3.3 Create seed relation
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 4 Create Package
        let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
        let newPackageCode = await seedlotHelper.generatePackageCode()
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = harvestData.no_of_seed
        packageInfo['package_unit'] = 'seeds'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id

    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Backcross Bulk Rice
 * Creates germplasm, seed, and package, and other related records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestBackcrossBulkRice(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent

        let parentsCheck = await checkTransGenParent(femaleParentGermplasm.germplasmType,maleParentGermplasm.germplasmType)

        if(!parentsCheck){
            return false
        }

        let modifiedHarvest = parentsCheck.modifiedHarvest != undefined ? parentsCheck.modifiedHarvest : false
        let optional = parentsCheck.optional != undefined ? parentsCheck.optional : optional
        let childGermplasmType = parentsCheck.childGermplasmType != undefined ? parentsCheck.childGermplasmType : 'progeny'

        // Find the recurrent parent
        let recurrentParentCheck = await seedlotHelper.findBackcrossRecurrentParent(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId
        )
        // if no recurrent parent, the pairing is not a true backcross
        // so the harvest fails
        if (recurrentParentCheck == null) return false
        
        // add recurrent and non recurrent parents to entities array
        let recurrentParentGermplasm = recurrentParentCheck.recurrentParentGermplasm
        let nonRecurrentParentGermplasm = recurrentParentCheck.nonRecurrentParentGermplasm

        entitiesArray.recurrentParentGermplasm = recurrentParentGermplasm
        entitiesArray.nonRecurrentParentGermplasm = nonRecurrentParentGermplasm

        // Build parentage based on recurrent parent
        let parentageProcess = await seedlotHelper.backcrossParentageProcess(entitiesArray, 'designation', false)
        let newParentage = parentageProcess.bcParentage
        
        // update cross name
        let newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray, optional)
        let values = [
            `cross_name = '${newCrossName}'`
        ]
        result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)

        if(!result) return false
        entitiesArray.cross.crossName = newCrossName

        // Determine taxonomy
        let taxonomyInfo = await seedlotHelper.determineTaxonomy(
            femaleParentGermplasm,
            maleParentGermplasm,
            {
                cropCode: entitiesArray.record.cropCode, 
                programCode: entitiesArray.record.programCode
            }
        )
        if (taxonomyInfo.taxonomyDbId == null) {
            return false
        }

        // Use parentage as designation and parentage of new germplasm
        let newDesignation = newCrossName
        
        // 1.1 Create germplasm
        let germplasmInfo = {}

        germplasmInfo['designation'] = newDesignation
        germplasmInfo['parentage'] = newParentage
        germplasmInfo['generation'] = await seedlotHelper.advanceGermplasmBCGeneration(nonRecurrentParentGermplasm.generation)
        germplasmInfo['germplasm_state'] = 'not_fixed'
        germplasmInfo['germplasm_name_type'] = 'cross_name'
        germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
        germplasmInfo['crop_id'] = crossRecord.cropDbId
        germplasmInfo['germplasm_type'] = modifiedHarvest ? childGermplasmType : 'progeny'
        
        result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
        if (!result) return false
        
        let newGermplasmDbId = result[0].id
        entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

        // 1.2 Create germplasm name
        let germplasmNameInfo = {}

        germplasmNameInfo['germplasm_id'] = newGermplasmDbId
        germplasmNameInfo['name_value'] = newDesignation
        germplasmNameInfo['germplasm_name_type'] = 'cross_name'
        germplasmNameInfo['germplasm_name_status'] = 'standard'
        
        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
        if(!result) return false
        
        let newGermplasmNameDbId = result[0].id

        // Create backcross derivative name
        if (entitiesArray.nonRecurrentParentGermplasm !== undefined
            && entitiesArray.nonRecurrentParentGermplasm.backcrossDerivativeName !== undefined
            && entitiesArray.nonRecurrentParentGermplasm.backcrossDerivativeName !== null
        ) {
            let bcDerivativeName = await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray)

            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = bcDerivativeName
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_BACKCROSS_DERIVATIVE')
            germplasmNameInfo['germplasm_name_status'] = 'active'
            
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
        }

        // 1.3 Create germplasm relation records (female and male)
        let germplasmRelationFemaleInfo = {}

        germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
        germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
        germplasmRelationFemaleInfo['order_number'] = 1
        
        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
        if(!result) return false
        
        let germplasmRelationMaleInfo = {}
        germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
        germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
        germplasmRelationMaleInfo['order_number'] = 2
        
        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
        if(!result) return false

        // Create germplasm attribute records
        // TAXONOMY NAME
        if (taxonomyInfo.taxonomyName != null) {
            result = await seedlotHelper.insertGermplasmAttribute(
                newGermplasmDbId,
                'TAXONOMY_NAME',
                taxonomyInfo.taxonomyName,
                userId
            )
        }
        // GERMPLASM YEAR
        if(crossRecord.crossingDate != null) {
            result = await seedlotHelper.createGermplasmYear(
                newGermplasmDbId, 
                crossRecord, 
                userId
            )
        }
        // MTA STATUS
        result = await seedlotHelper.insertRecord(
            'germplasm',
            'germplasm_attribute',
            {
                germplasm_id: newGermplasmDbId,
                variable_id: await getVariableId('MTA_STATUS'),
                data_value: 'PUD2',
                data_qc_code: 'G'
            },
            userId
        )
        // ORIGIN
        await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)

        // 1.4.a. Create new family
        let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
        let newFamilyCode = await seedlotHelper.generateFamilyCode()
        let familyInfo = {}
        familyInfo['family_name'] = newFamilyName
        familyInfo['family_code'] = newFamilyCode
        familyInfo['cross_id'] = entitiesArray.cross.crossDbId
        result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
        if(!result) return false
        let newFamilyDbId = result[0].id
        
        // 1.4.b. Add new germplasm to the family
        result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
        if(!result) return false

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
        let seedInfo = {}

        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false

        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed attribute
        let seedAttributeInfo = {}
        seedAttributeInfo['seed_id'] = newSeedDbId
        seedAttributeInfo['variable_id'] = await getVariableId('USE')
        seedAttributeInfo['data_value'] = 'Active'
        seedAttributeInfo['data_qc_code'] = 'N'
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)
        if(!result) return false

        // 2.3 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}

        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}

        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // 3.1 Create new package label
        let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
        let newPackageCode = await seedlotHelper.generatePackageCode()

        // 3.2 Create new package record
        let packageInfo = {}

        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = harvestData.no_of_seed
        packageInfo['package_unit'] = 'seeds'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId
        
        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        
        let newPackageDbId = result[0].id
    }
    catch(error) {
        return false
    }
    return true
}

/**
 * Harvest use case for Backcross Single Seed Numbering Rice
 * Creates germplasm, seed, and package, and other related records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestBackcrossSingleSeedNumberingRice(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        let existingChildGermplasm
        let newCrossName

        let parentsCheck = await checkTransGenParent(femaleParentGermplasm.germplasmType,maleParentGermplasm.germplasmType)

        if(!parentsCheck){
            return false
        }

        let modifiedHarvest = parentsCheck.modifiedHarvest != undefined ? parentsCheck.modifiedHarvest : false
        let optional = parentsCheck.optional != undefined ? parentsCheck.optional : optional
        let childGermplasmType = parentsCheck.childGermplasmType != undefined ? parentsCheck.childGermplasmType : 'progeny'

        // Find the recurrent parent
        let recurrentParentCheck = await seedlotHelper.findBackcrossRecurrentParent(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId
        )
        // if no recurrent parent, the pairing is not a true backcross
        // so the harvest fails
        if (recurrentParentCheck == null) return false
        
        // add recurrent and non recurrent parents to entities array
        let recurrentParentGermplasm = recurrentParentCheck.recurrentParentGermplasm
        let nonRecurrentParentGermplasm = recurrentParentCheck.nonRecurrentParentGermplasm

        entitiesArray.recurrentParentGermplasm = recurrentParentGermplasm
        entitiesArray.nonRecurrentParentGermplasm = nonRecurrentParentGermplasm

        // Build parentage based on recurrent parent
        let parentageProcess = await seedlotHelper.backcrossParentageProcess(entitiesArray, 'designation', false)
        let newParentage = parentageProcess.bcParentage

        // Check if the pair already has a child from within the same program.
        existingChildGermplasm = await seedlotHelper.getChildGermplasmId(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId,
            true,
            true,
            [
                `program_code = '${crossRecord.programCode}'`,
            ],
            true,
            true
        )

        // 1. Update cross name
        if (existingChildGermplasm != null) newCrossName = existingChildGermplasm.cross_name
        else newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray, optional)
        let values = [
            `cross_name = '${newCrossName}'`
        ]
        result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
        if(!result) return false
        entitiesArray.cross.crossName = newCrossName
        
        // Create germplasm/seeds/packages based on number of seeds (n)
        for (let n = 1; n <= harvestData.no_of_seed; n++) {
            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }
            
            // Create new designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)

            // 1.1 Create germplasm
            let germplasmInfo = {}

            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = newParentage
            germplasmInfo['generation'] = await seedlotHelper.advanceGermplasmBCGeneration(nonRecurrentParentGermplasm.generation)
            germplasmInfo['germplasm_state'] = 'not_fixed'
            germplasmInfo['germplasm_name_type'] = 'cross_name'
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['crop_id'] = crossRecord.cropDbId
            germplasmInfo['germplasm_type'] = modifiedHarvest ? childGermplasmType : 'progeny'
            
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            
            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}

            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'cross_name'
            germplasmNameInfo['germplasm_name_status'] = 'standard'
            
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            
            let newGermplasmNameDbId = result[0].id

            // Create backcross derivative name
            if (entitiesArray.nonRecurrentParentGermplasm !== undefined
                && entitiesArray.nonRecurrentParentGermplasm.backcrossDerivativeName !== undefined
                && entitiesArray.nonRecurrentParentGermplasm.backcrossDerivativeName !== null
            ) {
                let bcDerivativeName = await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray, {counter:n})

                germplasmNameInfo['germplasm_id'] = newGermplasmDbId
                germplasmNameInfo['name_value'] = bcDerivativeName
                germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_BACKCROSS_DERIVATIVE')
                germplasmNameInfo['germplasm_name_status'] = 'active'
                
                result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
                if(!result) return false
            }

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}

            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // Create germplasm attribute records
            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }
            // GERMPLASM YEAR
            if(crossRecord.crossingDate != null) {
                result = await seedlotHelper.createGermplasmYear(
                    newGermplasmDbId, 
                    crossRecord, 
                    userId
                )
            }
            // MTA STATUS
            result = await seedlotHelper.insertRecord(
                'germplasm',
                'germplasm_attribute',
                {
                    germplasm_id: newGermplasmDbId,
                    variable_id: await getVariableId('MTA_STATUS'),
                    data_value: 'PUD2',
                    data_qc_code: 'G'
                },
                userId
            )
            // ORIGIN
            await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)

            // 1.4.a. Create new family
            let newFamilyDbId
            if (entitiesArray.family === undefined) {
                let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
                let newFamilyCode = await seedlotHelper.generateFamilyCode()
                let familyInfo = {}
                familyInfo['family_name'] = newFamilyName
                familyInfo['family_code'] = newFamilyCode
                familyInfo['cross_id'] = entitiesArray.cross.crossDbId
                result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
                if(!result) return false
                newFamilyDbId = result[0].id

                // Add to entities array
                entitiesArray.family = await seedlotHelper.getFamilyInfo(newFamilyDbId)
            }
            else newFamilyDbId = entitiesArray.family.familyDbId
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false

            // 2.1 Create seed
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
            let seedInfo = {}

            seedInfo['seed_name'] = newSeedName
            seedInfo['seed_code'] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = crossRecord.programDbId
            seedInfo['source_experiment_id'] = crossRecord.experimentDbId
            seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
            seedInfo['source_location_id'] = crossRecord.locationDbId
            seedInfo['source_plot_id'] = null
            seedInfo['source_entry_id'] = null
            seedInfo['cross_id'] = crossRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource
            
            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if(!result) return false

            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 2.2 Create seed attribute
            let seedAttributeInfo = {}
            seedAttributeInfo['seed_id'] = newSeedDbId
            seedAttributeInfo['variable_id'] = await getVariableId('USE')
            seedAttributeInfo['data_value'] = 'Active'
            seedAttributeInfo['data_qc_code'] = 'N'
            
            result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)
            if(!result) return false

            // 2.3 Create seed relation records (female and male)
            let seedRelationFemaleInfo = {}

            seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
            seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
            seedRelationFemaleInfo['order_number'] = 1
            
            result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
            if(!result) return false
            
            let seedRelationMaleInfo = {}

            seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
            seedRelationMaleInfo['child_seed_id'] = newSeedDbId
            seedRelationMaleInfo['order_number'] = 2
            
            result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
            if(!result) return false

            // 3 Create package
            // 3.1 Create new package label
            let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
            let newPackageCode = await seedlotHelper.generatePackageCode()

            // 3.2 Create new package record
            let packageInfo = {}

            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 1 //one for each seed record
            packageInfo['package_unit'] = 'seeds'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = crossRecord.programDbId
            
            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false
            
            let newPackageDbId = result[0].id
        }
    }
    catch(error) {
        return false
    }

    return true
}

/**
 * Harvest use case for Single Cross Single Seed Numbering Rice
 * Updates the cross name 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData - harvest data of the cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestSingleCrossSingleSeedNumberingRice(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    // if no_of_seed = 0, do not create germplasm, seeds, and packages
    if (harvestData.no_of_seed == 0) return true

    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        let existingChildGermplasm
        let newCrossName

        let parentsCheck = await checkTransGenParent(femaleParentGermplasm.germplasmType,maleParentGermplasm.germplasmType)

        if(!parentsCheck){
            return false
        }

        let modifiedHarvest = parentsCheck.modifiedHarvest != undefined ? parentsCheck.modifiedHarvest : false
        let optional = parentsCheck.optional != undefined ? parentsCheck.optional : optional
        let childGermplasmType = parentsCheck.childGermplasmType != undefined ? parentsCheck.childGermplasmType : 'progeny'

        // Check if the pair already has a child from within the same program.
        existingChildGermplasm = await seedlotHelper.getChildGermplasmId(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId,
            true,
            true,
            [
                `program_code = '${crossRecord.programCode}'`,
            ],
            true,
            true
        )

        // 1. Update cross name
        if (existingChildGermplasm != null) newCrossName = existingChildGermplasm.cross_name
        else newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray, optional)
        let values = [
            `cross_name = '${newCrossName}'`
        ]

        result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
        if(!result) return false
        entitiesArray.cross.crossName = newCrossName
        
        // Create germplasm/seeds/packages based on number of seeds (n)
        for (let n = 1; n <= harvestData.no_of_seed; n++) {

            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }

            // Create new designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)

            // 2.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = await seedlotHelper.riceParentageProcess(femaleParentGermplasm, maleParentGermplasm, crossRecord.crossMethod)
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = 'not_fixed'
            germplasmInfo['germplasm_type'] = modifiedHarvest ? childGermplasmType : 'progeny'
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['germplasm_name_type'] = 'cross_name'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if(!result) return false
            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId)

            // 2.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'cross_name'
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)          
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // Create backcross derivative name
            let occurrenceDbId = entitiesArray.cross.occurrenceDbId
            let occurrenceCrossCount = await seedlotHelper.getOccurrenceCrossCount(occurrenceDbId)
            // Criteria:
            //      IF the occurrence only has one cross, AND the project code is set for the experiment
            //      THEN use the project code for the backcross derivative name
            //      ELSE use the cross name for the backcross derivative name
            let withProjectCode = occurrenceCrossCount == 1 && entitiesArray.cross.projectCode != null
            let bcDerivativeName = await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray, {
                additionalCondition: withProjectCode ? 'with_project_code' : 'without_project_code',
                counter:n
            })
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = bcDerivativeName
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_BACKCROSS_DERIVATIVE')
            germplasmNameInfo['germplasm_name_status'] = 'active'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false

            // 2.3 Create germplasm relation records
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // Create germplasm attribute records
            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }
            // GERMPLASM YEAR
            if(crossRecord.crossingDate != null) {
                result = await seedlotHelper.createGermplasmYear(
                    newGermplasmDbId, 
                    crossRecord, 
                    userId
                )
            }
            // MTA STATUS
            result = await seedlotHelper.insertRecord(
                'germplasm',
                'germplasm_attribute',
                {
                    germplasm_id: newGermplasmDbId,
                    variable_id: await getVariableId('MTA_STATUS'),
                    data_value: 'PUD2',
                    data_qc_code: 'G'
                },
                userId
            )
            // ORIGIN
            await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)

            // 1.4.a. Create new family
            let newFamilyDbId
            if (entitiesArray.family === undefined) {
                let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
                let newFamilyCode = await seedlotHelper.generateFamilyCode()
                let familyInfo = {}
                familyInfo['family_name'] = newFamilyName
                familyInfo['family_code'] = newFamilyCode
                familyInfo['cross_id'] = entitiesArray.cross.crossDbId
                result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
                if(!result) return false
                newFamilyDbId = result[0].id

                // Add to entities array
                entitiesArray.family = await seedlotHelper.getFamilyInfo(newFamilyDbId)
            }
            else newFamilyDbId = entitiesArray.family.familyDbId
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false

            // 3.1 Create Seed 
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
            let seedInfo = {}
            seedInfo['seed_name'] = newSeedName
            seedInfo['seed_code'] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = crossRecord.programDbId
            seedInfo['source_experiment_id'] = crossRecord.experimentDbId
            seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
            seedInfo['source_location_id'] = crossRecord.locationDbId
            seedInfo['cross_id'] = crossRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if(!result) return false
            let newSeedDbId = result[0].id

            // 3.2 Create seed attribute
            let seedAttributeInfo = {}
            seedAttributeInfo['seed_id'] = newSeedDbId
            seedAttributeInfo['variable_id'] = await getVariableId('USE')
            seedAttributeInfo['data_value'] = 'Active'
            seedAttributeInfo['data_qc_code'] = 'N'
            result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)           
            if(!result) return false

            // 3.3 Create seed relation
            let seedRelationFemaleInfo = {}
            seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
            seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
            seedRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)        
            if(!result) return false
            
            let seedRelationMaleInfo = {}
            seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
            seedRelationMaleInfo['child_seed_id'] = newSeedDbId
            seedRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
            if(!result) return false

            // 4 Create Package
            let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
            let newPackageCode = await seedlotHelper.generatePackageCode()
            let packageInfo = {}
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 1 //one for each seed record
            packageInfo['package_unit'] = 'seeds'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = crossRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false
            let newPackageDbId = result[0].id
        }

    } catch (err) {
        return false
    }

    return true
}

/**
 * Rice - BC derivative name process for numbered harvests.
 * Known use cases:
 *      - Single Plant Selection (not_fixed/any_type)
 *      - Panicle Selection (not_fixed/any type)
 *      - Single Plant Seed Increase (fixed/fixed_line)
 * @param {Object} entitiesArray object array of entities involved in the harvest
 * @param {Integer} newGermplasmDbId new germplasm database identifier
 * @param {Integer} userId user database identifier
 * @returns {Boolean} wether the creation of the BC Derivative name was successful or not
 */
async function riceSelfingBCDerivativeNameProcess(entitiesArray, newGermplasmDbId, userId) {
    // Create backcross derivative name
    let germplasmNameInfo = {}
    if (entitiesArray.plot !== undefined
        && entitiesArray.plot.backcrossDerivativeName !== undefined
        && entitiesArray.plot.backcrossDerivativeName !== null
    ) {
        // Check parent germplasm name
        // Get standard status name
        let result = await seedlotHelper.retrieveRecordsBasic(
            'germplasm',
            'germplasm_name',
            [],
            [
                `germplasm_id = ${entitiesArray.record.germplasmDbId}`,
                `germplasm_name_status = 'standard'`
            ]
        )

        // If retrieval fails, return fals
        if (result == null || result[0] == null) return false

        let standardName = result[0].name_value ?? null;
        let backcrossDerivativeName = entitiesArray.plot.backcrossDerivativeName

        // // Compare standard name and bc derivatve name
        // // If names are the same, use the same germplasm name
        let newBackcrossDerivativeName = ``
        if (standardName === backcrossDerivativeName) {
            newBackcrossDerivativeName = entitiesArray.germplasm.designation
        }
        // // Otherwise, build new name
        else newBackcrossDerivativeName = await buildGermplasmDesignation(entitiesArray, {}, "germplasm_name")
        
        germplasmNameInfo["germplasm_id"] = newGermplasmDbId
        germplasmNameInfo["name_value"] = newBackcrossDerivativeName
        germplasmNameInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev("GERMPLASM_NAME_TYPE_BACKCROSS_DERIVATIVE")
        germplasmNameInfo["germplasm_name_status"] = "active"

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
        if (!result) return false
    }

    return true
}

/**
 * Facilitate the harvest of materials for Rice Selfing of Fixed Materials using 
 * Single Plant Seed Increase harvest method
 * Creates germplasm, seed, and package record
 * 
 * @param {Object} record - plot record information
 * @param {Object} harvestData - harvest data information
 * @param {Integer} userId - user identifier 
 * @param {String} harvestSource - 'plot' or 'cross' 
 * @param {Object} optional - optional parameters for generic name builder 
 */
async function harvestFixedSelfingSinglePlantSeedIncrease(record, harvestData, userId, harvestSource, optional){
    // retrieve plot record information
    plotRecord = await populateCrossRecord(record,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode
    }    

    try{
        // extract starting information
        let startingGeneration = plotRecord.germplasmGeneration
        let recordsToCreate = parseInt(harvestData.no_of_plants)

        // create germplasm, seed, and package records based on numberOfSeedlotsToCreate
        for(let i = 0; i < recordsToCreate; i++){
            // 1. Create germplasm record
            // 1.a. Generate the next germplasm designation and generation
            let nextGermplasmDesignation = await buildGermplasmDesignation(entitiesArray)
            let newGermplasmGeneration = await seedlotHelper.advanceGermplasmGeneration(String(startingGeneration))

            // 1.b. Assemble germplasm data
            let gemplasmNameType = 'line_name';
            let germplasmData = await assembleGermplasmData(nextGermplasmDesignation, newGermplasmGeneration, plotRecord, gemplasmNameType)

            // 1.c Create Germplasm
            let germplasmCreated = await seedlotHelper.createGermplasmRecord(germplasmData, userId)
            let newGermplasmDbId = germplasmCreated[0].id

            germplasmData.germplasmDbId = newGermplasmDbId

            await seedlotHelper.createGermplasmName(germplasmData, plotRecord, userId)

            let germplasmRecord = await seedlotHelper.getGermplasmRecord(germplasmCreated[0].id)
            entitiesArray.germplasm = germplasmRecord

            // Create backcross derivative name
            result = await riceSelfingBCDerivativeNameProcess(entitiesArray, newGermplasmDbId, userId)
            if (!result) return false

            // 1.d Populate germplasm relation
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // Create germplasm attribute records
            let childGermplasmDbId = newGermplasmDbId
            let parentGermplasmDbId = entitiesArray.record.germplasmDbId
            
            // Populate GERMPLASM_YEAR, MTA_STATUS and ORIGIN based on parent germplasm
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'GERMPLASM_YEAR', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'MTA_STATUS', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'ORIGIN', userId)

            // 2. Create seed record
            // 2.a. Generate seed name
            let seedName = await seedlotHelper.seedNameBuilder(entitiesArray)

            // 2.b. Assemble seed data
            let seedData = await assembleSeedData(seedName, harvestData, plotRecord, germplasmRecord, harvestSource)

            // 2.c. Create Seed
            let seedCreated = await seedlotHelper.createSeedRecord(seedData, userId)
            let newSeedDbId = seedCreated[0].id

            let seedRecord = await seedlotHelper.getSeedInformation(newSeedDbId)
            entitiesArray.seed = seedRecord

            // 2.d Populate seed relation
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 3. Create package record
            // 3.a. Generate package label
            let packageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)

            // 3.b. Assemble package data
            let packageData = await assemblePackageData(packageLabel, plotRecord, seedRecord)

            // 3.c. Create package record
            let packageCreated = await seedlotHelper.createPackageRecord(packageData, userId)

            let packageRecord = await seedlotHelper.getPackageInformation(packageCreated[0].id)
            entitiesArray.package = packageRecord
        }

    }
    catch(error){
        return false
    }

    return true
}

/**
 * Harvest use case for Transgenesis and Genome editing - Single Seed Numbering - Rice
 * Updates the cross name 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData - harvest data of the cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestTransgenesisGenomeEditiongSSNRice(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    // if no_of_seed = 0, do not create germplasm, seeds, and packages
    if (harvestData.no_of_seed == 0) return true

    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let backgroundGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let constructGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        entitiesArray.backgroundGermplasm = backgroundGermplasm
        entitiesArray.constructGermplasm = constructGermplasm

        // Check if construct germplasm has type = gene_construct.
        // If requirement is not met, the harvest fails
        if (constructGermplasm != null && constructGermplasm.germplasmType != 'gene_construct') return false

        // 1. Update cross name
        let newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray)
        let values = [
            `cross_name = '${newCrossName}'`
        ]
        result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
        if(!result) return false
        entitiesArray.cross.crossName = newCrossName
        
        // Create germplasm/seeds/packages based on number of seeds (n)
        for (let n = 1; n <= harvestData.no_of_seed; n++) {

            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                backgroundGermplasm,
                constructGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }

            // Create new designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)

            // Determine type
            let germplasmType = crossRecord.crossMethod === 'transgenesis' ?
                'transgenic'
                :
                (
                    crossRecord.crossMethod === 'genome editing' ?
                    'genome_edited'
                    :
                    null
                )

            // If germplasm type is null, harvest fails         
            if (germplasmType == null) return false

            // 2.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = backgroundGermplasm.designation
            germplasmInfo['generation'] = 'T0'
            germplasmInfo['germplasm_state'] = 'not_fixed'
            germplasmInfo['germplasm_type'] = germplasmType
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['germplasm_name_type'] = 'transgenic_event_id'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if(!result) return false
            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'transgenic_event_id'
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)          
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 2.3 Create germplasm relation record for background
            let germplasmRelationBackgroundInfo = {}
            germplasmRelationBackgroundInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationBackgroundInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationBackgroundInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationBackgroundInfo, userId)
            if(!result) return false

            // Create germplasm attribute records
            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }
            // GERMPLASM YEAR
            if(crossRecord.crossingDate != null) {
                result = await seedlotHelper.createGermplasmYear(
                    newGermplasmDbId, 
                    crossRecord, 
                    userId
                )
            }
            // MTA STATUS
            result = await seedlotHelper.insertRecord(
                'germplasm',
                'germplasm_attribute',
                {
                    germplasm_id: newGermplasmDbId,
                    variable_id: await getVariableId('MTA_STATUS'),
                    data_value: 'PUD2',
                    data_qc_code: 'G'
                },
                userId
            )
            // ORIGIN
            await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)

            // 1.4.a. Create new family
            let newFamilyDbId
            if (entitiesArray.family === undefined) {
                let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
                let newFamilyCode = await seedlotHelper.generateFamilyCode()
                let familyInfo = {}
                familyInfo['family_name'] = newFamilyName
                familyInfo['family_code'] = newFamilyCode
                familyInfo['cross_id'] = entitiesArray.cross.crossDbId
                result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
                if(!result) return false
                newFamilyDbId = result[0].id

                // Add to entities array
                entitiesArray.family = await seedlotHelper.getFamilyInfo(newFamilyDbId)
            }
            else newFamilyDbId = entitiesArray.family.familyDbId
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false

            // 3.1 Create Seed 
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
            entitiesArray.seed = {}
            entitiesArray.seed.seedName = newSeedName
            let seedInfo = {}
            seedInfo['seed_name'] = newSeedName
            seedInfo['seed_code'] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = crossRecord.programDbId
            seedInfo['source_experiment_id'] = crossRecord.experimentDbId
            seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
            seedInfo['source_location_id'] = crossRecord.locationDbId
            seedInfo['cross_id'] = crossRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if(!result) return false
            let newSeedDbId = result[0].id

            // 3.2 Create seed attribute
            let seedAttributeInfo = {}
            seedAttributeInfo['seed_id'] = newSeedDbId
            seedAttributeInfo['variable_id'] = await getVariableId('USE')
            seedAttributeInfo['data_value'] = 'Active'
            seedAttributeInfo['data_qc_code'] = 'N'
            result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)           
            if(!result) return false

            // // 3.3 Create seed relation for background
            let seedRelationBackgroundInfo = {}
            seedRelationBackgroundInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
            seedRelationBackgroundInfo['child_seed_id'] = newSeedDbId
            seedRelationBackgroundInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationBackgroundInfo, userId)        
            if(!result) return false

            // // 4 Create Package
            let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
            entitiesArray.package = {}
            entitiesArray.package.packageLabel = newPackageLabel
            let newPackageCode = await seedlotHelper.generatePackageCode()
            let packageInfo = {}
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 1 //one for each seed record
            packageInfo['package_unit'] = 'seeds'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = crossRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false
            let newPackageDbId = result[0].id
        }

    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Rice Hybrid formation - H-Harvest
 * Updates the cross name 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData - harvest data of the cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestHybridFormationHHarvestRice(crossRecord, harvestData, userId, harvestSource, prefixes) {
    // if no_of_bags = 0, do not create germplasm, seeds, and packages
    if (harvestData.no_of_bags == 0) return true

    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    let germplasmType = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_HYBRID')

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)

        // Check if the pair has been made before and has an existing child
        let newGermplasmDbId = await seedlotHelper.retrieveCommonChildGermplasm(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId,
            [
                `child.germplasm_type = '${germplasmType}'`
            ])

        // If the pair has no child yet, create one
        if (newGermplasmDbId === null) {
            // 1. Update cross name
            let newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray)
            let values = [
                `cross_name = '${newCrossName}'`
            ]
            result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
            if(!result) return false
            entitiesArray.cross.crossName = newCrossName

            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }
            
            // Create new designation
            let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)

            // 2.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = await seedlotHelper.riceParentageProcess(femaleParentGermplasm, maleParentGermplasm, crossRecord.crossMethod)
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = "fixed"
            germplasmInfo['germplasm_type'] = germplasmType
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['germplasm_name_type'] = 'cross_name'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if(!result) return false
            newGermplasmDbId = result[0].id

            // 2.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'cross_name'
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 2.3 Create germplasm relation records
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // Create germplasm attribute records
            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }
            // GERMPLASM YEAR
            if(crossRecord.crossingDate != null) {
                result = await seedlotHelper.createGermplasmYear(
                    newGermplasmDbId, 
                    crossRecord, 
                    userId
                )
            }
            // MTA STATUS
            result = await seedlotHelper.insertRecord(
                'germplasm',
                'germplasm_attribute',
                {
                    germplasm_id: newGermplasmDbId,
                    variable_id: await getVariableId('MTA_STATUS'),
                    data_value: 'PUD2',
                    data_qc_code: 'G'
                },
                userId
            )
            // ORIGIN
            await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)
        } else {
            let childGermplasm = await seedlotHelper.getGermplasmInfo(newGermplasmDbId)
            // Update cross name
            let newCrossName = childGermplasm.designation
            let values = [
                `cross_name = '${newCrossName}'`
            ]
            result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
            if(!result) return false
            entitiesArray.cross.crossName = newCrossName
        }

        // 3.1 Create Seed
        let newSeedCode = await seedlotHelper.generateSeedCode()
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id

        // 3.2 Create seed attribute
        let seedAttributeInfo = {}
        seedAttributeInfo['seed_id'] = newSeedDbId
        seedAttributeInfo['variable_id'] = await getVariableId('USE')
        seedAttributeInfo['data_value'] = 'Active'
        seedAttributeInfo['data_qc_code'] = 'N'
        result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)
        if(!result) return false

        // 3.3 Create seed relation
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 4 Create Package
        let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
        let newPackageCode = await seedlotHelper.generatePackageCode()
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = '0'
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Rice CMS Multiplication - A-line Harvest
 * Updates the cross name 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData - harvest data of the cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestCMSMultALineRice(crossRecord, harvestData, userId, harvestSource, prefixes) {
    // if no_of_bags = 0, do not create germplasm, seeds, and packages
    if (harvestData.no_of_bags == 0) return true

    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    let germplasmType = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_CMS_LINE')

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)

        // Check if the pair has been made before and has an existing child
        let newGermplasmDbId = await seedlotHelper.retrieveCommonChildGermplasm(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId,
            [
                `child.germplasm_type = '${germplasmType}'`
            ])

        // Check if female parent is an A line (type = CMS_line).
        // If A line, use female parent as germplasm
        if (femaleParentGermplasm.germplasmType.trim() === germplasmType) {
            newGermplasmDbId = femaleParentGermplasm.germplasmDbId
        }

        // If the pair has no child yet, create one
        if (newGermplasmDbId === null) {
            // 1. Update cross name
            let newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray)
            let values = [
                `cross_name = '${newCrossName}'`
            ]
            result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
            if(!result) return false
            entitiesArray.cross.crossName = newCrossName

            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }
            
            // Create new designation
            let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)

            // 2.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = await seedlotHelper.riceParentageProcess(femaleParentGermplasm, maleParentGermplasm, crossRecord.crossMethod)
            germplasmInfo['generation'] = 'BC1F1'
            germplasmInfo['germplasm_state'] = "fixed"
            germplasmInfo['germplasm_type'] = germplasmType
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['germplasm_name_type'] = 'cross_name'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if(!result) return false
            newGermplasmDbId = result[0].id

            // 2.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'cross_name'
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 2.3 Create germplasm relation records
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // Create germplasm attribute records
            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }
            // GERMPLASM YEAR
            if(crossRecord.crossingDate != null) {
                result = await seedlotHelper.createGermplasmYear(
                    newGermplasmDbId, 
                    crossRecord, 
                    userId
                )
            }
            // MTA STATUS
            result = await seedlotHelper.insertRecord(
                'germplasm',
                'germplasm_attribute',
                {
                    germplasm_id: newGermplasmDbId,
                    variable_id: await getVariableId('MTA_STATUS'),
                    data_value: 'PUD2',
                    data_qc_code: 'G'
                },
                userId
            )
            // ORIGIN
            await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)
        } else {
            let childGermplasm = await seedlotHelper.getGermplasmInfo(newGermplasmDbId)
            // Update cross name
            let newCrossName = childGermplasm.designation
            let values = [
                `cross_name = '${newCrossName}'`
            ]
            result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
            if(!result) return false
            entitiesArray.cross.crossName = newCrossName
        }

        // 3.1 Create Seed
        let newSeedCode = await seedlotHelper.generateSeedCode()
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id

        // 3.2 Create seed attribute
        let seedAttributeInfo = {}
        seedAttributeInfo['seed_id'] = newSeedDbId
        seedAttributeInfo['variable_id'] = await getVariableId('USE')
        seedAttributeInfo['data_value'] = 'Active'
        seedAttributeInfo['data_qc_code'] = 'N'
        result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)
        if(!result) return false

        // 3.3 Create seed relation
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 4 Create Package
        let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
        let newPackageCode = await seedlotHelper.generatePackageCode()
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = '0'
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Rice Test cross - Test-cross harvest
 * Updates the cross name 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData - harvest data of the cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestTestCrossRice(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    // if no_of_bags = 0, do not create germplasm, seeds, and packages
    if (harvestData.no_of_bags == 0) return true

    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    let germplasmType = 'unknown'

    try {
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)

        // Check if the pair has been made before and has an existing child
        let newGermplasmDbId = await seedlotHelper.retrieveCommonChildGermplasm(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId,
            [
                `child.germplasm_type = '${germplasmType}'`,
                `child.germplasm_state = 'not_fixed'`
            ])

        // If the pair has no child yet, create one
        if (newGermplasmDbId === null) {
            // 1. Update cross name
            let newCrossName = await seedlotHelper.crossNameBuilder(entitiesArray)
            let values = [
                `cross_name = '${newCrossName}'`
            ]
            result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
            if(!result) return false
            entitiesArray.cross.crossName = newCrossName

            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }
            
            // Create new designation
            let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)

            // 2.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = await seedlotHelper.riceParentageProcess(femaleParentGermplasm, maleParentGermplasm, crossRecord.crossMethod)
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_type'] = germplasmType
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['germplasm_name_type'] = 'cross_name'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if(!result) return false
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmInfo(newGermplasmDbId)

            // 2.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'cross_name'
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 2.3 Create germplasm relation records
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // Create germplasm attribute records
            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }
            // GERMPLASM YEAR
            if(crossRecord.crossingDate != null) {
                result = await seedlotHelper.createGermplasmYear(
                    newGermplasmDbId, 
                    crossRecord, 
                    userId
                )
            }
            // MTA STATUS
            result = await seedlotHelper.insertRecord(
                'germplasm',
                'germplasm_attribute',
                {
                    germplasm_id: newGermplasmDbId,
                    variable_id: await getVariableId('MTA_STATUS'),
                    data_value: 'PUD2',
                    data_qc_code: 'G'
                },
                userId
            )
            // ORIGIN
            await seedlotHelper.createGermplasmOrigin(newGermplasmDbId, crossRecord, userId)

            // 1.4.a. Create new family
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let newFamilyCode = await seedlotHelper.generateFamilyCode()
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        } else {
            let childGermplasm = await seedlotHelper.getGermplasmInfo(newGermplasmDbId)
            // Update cross name
            let newCrossName = childGermplasm.designation
            let values = [
                `cross_name = '${newCrossName}'`
            ]
            result = await seedlotHelper.updateRecordById(crossRecord.crossDbId, 'germplasm', 'cross', values, userId)
            if(!result) return false
            entitiesArray.cross.crossName = newCrossName
        }

        // 3.1 Create Seed
        let newSeedCode = await seedlotHelper.generateSeedCode()
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id

        // 3.2 Create seed attribute
        let seedAttributeInfo = {}
        seedAttributeInfo['seed_id'] = newSeedDbId
        seedAttributeInfo['variable_id'] = await getVariableId('USE')
        seedAttributeInfo['data_value'] = 'Active'
        seedAttributeInfo['data_qc_code'] = 'N'
        result = await seedlotHelper.insertRecord('germplasm', 'seed_attribute', seedAttributeInfo, userId)
        if(!result) return false

        // 3.3 Create seed relation
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 4 Create Package
        let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
        let newPackageCode = await seedlotHelper.generatePackageCode()
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = '0'
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (err) {
        return false
    }

    return true
}

/**
 * Builds germplasm designation for numbered harvest
 * @param {object} entitiesArray - entities related to the harvest
 * @param {object} optional - optional parameters
 * @param {string} subEntity - sub-entity for germplasm (germplasm, germplasm_name)
 * @returns {string} designation - new designation
 */
async function buildGermplasmDesignation(entitiesArray, optional = {}, subEntity = 'germplasm') {
    // build designation with empty counter
    let optionalEmptyCounter = optional
    optionalEmptyCounter['counter'] = ''
    let counterRegex = '[1-9][0-9]*'

    // get designation counter delimiter from config
    let configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
    if (subEntity === 'germplasm_name') configBaseString = 'HM_NAME_PATTERN_GERMPLASM_BC_DERIV'
    let nameConfig = await seedlotHelper.getNomenclatureConfig(configBaseString, entitiesArray, optional)

    // reverse the array
    let reverseConfig = nameConfig.reverse()
    let revCLen = reverseConfig.length
    let special = optional.special !== undefined ? optional.special : []
    let delimiter = ''
    let prefix = ''
    let suffix = ''
    // Get prefix, suffix, and counter values from the config
    for (let i = 0; i < revCLen; i++) {
        let item = reverseConfig[i]
        // if counter item found, check next value
        if (item.type == 'counter') {
            // if the next item is a free-text, set next item as prefix
            // and set the item after that as the prefix
            if(reverseConfig[i+1].type == 'free-text') {
                prefix = reverseConfig[i+1].value
                delimiter = reverseConfig[i+2].value
            }
            // if the next item is a delimiter, set it as the delimiter
            else if(reverseConfig[i+1].type == 'delimiter') {
                delimiter = reverseConfig[i+1].value
            }
            // If counter has leading zeros, modify regex
            if (item.leading_zero === 'yes') counterRegex = '[0-9]*'
            break
        }
        // if free-text item found, check if special
        else if (item.type == 'free-text') {
            // if item is special, and special type is included in the special array,
            // OR if the item is NOT special, then add it to the suffix
            if(
                (item.special !== undefined && special.includes(item.special))
                || item.special === undefined
            ) {
                suffix = item.value + suffix
            }
        }
        // if field item found, get field value from the entites array
        // and add it to the suffix
        else if (item.type == 'field') {
            let entity = item.entity
            let fieldName = item.field_name
            suffix = entitiesArray[entity][fieldName] + suffix
        }
    }

    // get designation pattern
    let germplasmDesignationPattern
    if (subEntity === 'germplasm') {
        germplasmDesignationPattern = await seedlotHelper.germplasmNameBuilder(entitiesArray, optionalEmptyCounter)    
    }
    else if (subEntity === 'germplasm_name') {
        germplasmDesignationPattern = await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray, optionalEmptyCounter)    
    }
    // remove prefix and suffix
    let pcs = germplasmDesignationPattern.split(delimiter)      // split pattern by the delimiter
    let lastPc = pcs[pcs.length-1]                              // get the last part of the pattern
    lastPc = lastPc.replace(prefix,'')                          // remove instances of prefix
    lastPc = lastPc.replace(suffix,'')                          // remove instances of suffix
    pcs[pcs.length-1] = lastPc                                  // insert clean last piece back to pcs array
    germplasmDesignationPattern = pcs.join(delimiter)           // join pcs with the delimiter

    // get last in sequence
    let fieldName = 'designation'
    if (subEntity === 'germplasm_name') fieldName = 'name_value'
    let lastGermplasmDesignation = await seedlotHelper.getLastInNumberedSequence(
        'germplasm',
        subEntity,
        fieldName,
        germplasmDesignationPattern,
        counterRegex,
        {
            prefix: prefix,
            suffix: suffix
        },
        true
    )

    // get the next counter based on the last designation
    let germplasmCounter = await getCounterValue(
        lastGermplasmDesignation,
        delimiter,
        {
            prefix: prefix,
            suffix: suffix
        },
        germplasmDesignationPattern
    )
    optional.counter = germplasmCounter

    // generate the next germplasm designation and generation
    if (subEntity === 'germplasm') {
        return await seedlotHelper.germplasmNameBuilder(entitiesArray, optional)
    }
    else if (subEntity === 'germplasm_name') {
        return await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray, optional)
    }
}

/**
 * Builds germplasm designation with repeated field values
 * @param {object} entitiesArray - entities related to the harvest
 * @param {object} optional - optional parameters
 * @param {string} subEntity - sub-entity for germplasm (germplasm, germplasm_name)
 * @returns {string} designation - new designation
 */
async function buildGermplasmDesignationWithRepeater(entitiesArray, optional = {}, subEntity = 'germplasm') {
    // build designation with empty counter
    let optionalEmptyCounter = optional
    optionalEmptyCounter['counter'] = ''
    optionalEmptyCounter['skip_repeater'] = true
    let counterRegex = '[1-9][0-9]*'

    // get designation counter delimiter from config
    let configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
    if (subEntity === 'germplasm_name') configBaseString = 'HM_NAME_PATTERN_GERMPLASM_BC_DERIV'
    let nameConfig = await seedlotHelper.getNomenclatureConfig(configBaseString, entitiesArray, optional)
    // reverse the array
    let reverseConfig = nameConfig.reverse()
    let revCLen = reverseConfig.length
    let delimiter = ''
    let repeaterDelimiter = ''
    let repeatedValue = ''
    let repeaterType = ''
    let minimum = 0
    let item

    // Get delimiters, minimum values from the config
    for (let i = 0; i < revCLen; i++) {
        item = reverseConfig[i]
        // if repeater item found, extract type, delimiter, and minimum value
        if (item.type.includes('repeater')) {
            repeaterType = item.type
            repeaterDelimiter = item.delimiter
            minimum = item.minimum
            if (item.type.includes('free-text')) repeatedValue = item.value
            else if (item.type.includes('field')) repeatedValue = entitiesArray[item.entity][item.fieldName]
        }
        else if (item.type.includes('delimiter')) {
            delimiter = item.value
            break
        }
    }

    // build base pattern
    let basePattern
    if (subEntity === 'germplasm') {
        basePattern = await seedlotHelper.germplasmNameBuilder(
            entitiesArray, 
            {
                counter: 0,
                skip_repeater: false
            }
        )
    }
    else if (subEntity === 'germplasm_name') {   
        basePattern = await seedlotHelper.backcrossDerivativeNameBuilder(
            entitiesArray, 
            {
                counter: 0,
                skip_repeater: false
            }
        )
    }
    
    // If free-text repeater, remove duplicate value
    if (repeaterType === 'free-text-repeater') {
        let basePatternParts = basePattern.split(delimiter)
        let bppLength = basePatternParts.length

        if (basePatternParts[bppLength-2].includes(repeatedValue)) {
            basePatternParts.pop()
            basePattern = basePatternParts.join(delimiter)
        }
    }


    // Check if base pattern already exists
    let fieldName = 'designation'
    if (subEntity === 'germplasm_name') fieldName = 'name_value'
    let newDesignation = await seedlotHelper.getLastInNumberedSequence(
        'germplasm',
        subEntity,
        fieldName,
        basePattern,
        '',
        {},
        true
    )

    // if base pattern does not exist, use base pattern as new name
    if (newDesignation === null) return basePattern
    
    // Remove last delimiter counter, if any
    let basePatternParts = basePattern.split(repeaterDelimiter)
    let bppLength = basePatternParts.length
    if (repeaterType === 'free-text-repeater' && bppLength > 1) {
        basePatternParts.pop()
        basePattern = basePatternParts.join(repeaterDelimiter)
    }

    // get last in sequence
    let lastGermplasmDesignation = await seedlotHelper.getLastInNumberedSequence(
        'germplasm',
        'germplasm',
        'designation',
        basePattern,
        counterRegex,
        {
            delimiter:repeaterDelimiter
        },
        true
    )

    // get counter value
    let germplasmCounter = minimum
    if (lastGermplasmDesignation !== null) {
        // return minimum
        germplasmCounter = await getCounterValue(
            lastGermplasmDesignation,
            repeaterDelimiter,
            {},
            basePattern
        )
    }
    optional.counter = germplasmCounter
    optional.skip_repeater = false

    // generate the next germplasm designation and generation
    if (subEntity === 'germplasm') {
        return await seedlotHelper.germplasmNameBuilder(entitiesArray, optional)
    }
    else if (subEntity === 'germplasm_name') {
        return await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray, optional)
    }
}

/**
 * Builds germplasm designation with repeated text elements
 * @param {object} entitiesArray - entities related to the harvest
 * @param {object} optional - optional parameters
 * @param {string} subEntity - sub-entity for germplasm (germplasm, germplasm_name)
 * @returns {string} designation - new designation
 */
 async function buildGermplasmDesignationWithTextRepeater(entitiesArray, optional = {}, subEntity = 'germplasm') {
    // build designation with empty counter
    let optionalEmptyCounter = optional
    optionalEmptyCounter['counter'] = ''
    optionalEmptyCounter['skip_repeater'] = true

    // get designation counter delimiter from config
    let configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
    if (subEntity === 'germplasm_name') configBaseString = 'HM_NAME_PATTERN_GERMPLASM_BC_DERIV'
    let nameConfig = await seedlotHelper.getNomenclatureConfig(configBaseString, entitiesArray, optional)
    // reverse the array
    let reverseConfig = nameConfig.reverse()
    let revCLen = reverseConfig.length
    let delimiter = ''
    let repeaterDelimiter = ''
    let repeatedValue = ''
    let repeaterType = ''
    let minimum = 0
    let item

    // Get delimiters, minimum values from the config
    for (let i = 0; i < revCLen; i++) {
        item = reverseConfig[i]
        // if repeater item found, extract type, delimiter, and minimum value
        if (item.type.includes('repeater')) {
            repeaterType = item.type
            repeaterDelimiter = item.delimiter
            minimum = item.minimum
            if (item.type.includes('free-text')) repeatedValue = item.value
            else if (item.type.includes('field')) repeatedValue = entitiesArray[item.entity][item.fieldName]
        }
        else if (item.type.includes('delimiter')) {
            delimiter = item.value
            break
        }
    }

    // Get root value
    nameConfig = nameConfig.reverse()
    let baseConfigItem = nameConfig[0]
    let rootValue = entitiesArray[baseConfigItem.entity][baseConfigItem.field_name]

    // Split by delimiter
    let nameParts = rootValue.split(delimiter)
    // Get last part
    let last = ''
    let nameEnd = nameParts[nameParts.length-1]
    if (nameEnd.includes(repeaterDelimiter)) {
        let nameEndParts = nameEnd.split(repeaterDelimiter)
        last = nameEndParts[nameEndParts.length-1]
    }
    else if (nameEnd === repeatedValue) {
        last = nameEnd
    }
    // Determine counter value
    let counter = 1
    if (last != '' && Number.isInteger(Number(last))) {
        counter = parseInt(last) + 1
    }
    else if (last === repeatedValue) {
        counter = 2
    }

    // Build germplasm name
    optional.counter = counter
    return await seedlotHelper.germplasmNameBuilder(entitiesArray, optional)
}

/**
 * Builds seed name
 * Currently supports numbered harvests for maize and wheat
 * @param {object} entitiesArray - entities related to the harvest
 * @param {object} optional - optional parameters
 * @returns {string} seedName - new seed name
 */
async function buildSeedName(entitiesArray, optional = {}) {
    // build seed name with empty counter
    let optionalEmptyCounter = optional
    optionalEmptyCounter['counter'] = ''

    // get seed name pattern
    let seedNamePattern = await seedlotHelper.seedNameBuilder(entitiesArray, optionalEmptyCounter)

    // get last seed name using pattern
    let lastSeedName = await seedlotHelper.getLastInNumberedSequence(
        'germplasm',
        'seed',
        'seed_name',
        seedNamePattern,
        '[0-9]*'
    )

    // get seed counter delimiter from config
    let configBaseString = 'HM_NAME_PATTERN_SEED'
    let nameConfig = await seedlotHelper.getNomenclatureConfig(configBaseString, entitiesArray, optional)
    // reverse the array
    let reverseConfig = nameConfig.reverse()
    let revCLen = reverseConfig.length
    let delimiter = ''
    for (let i = 0; i < revCLen; i++) {
        let item = reverseConfig[i]
        // if counter item found, get value of next item
        if (item.type == 'counter') {
            delimiter = reverseConfig[i+1].value
            break
        }
    }

    // get the next counter based on the last seed name
    let seedCounter = await getCounterValue(lastSeedName,delimiter)
    optional['counter'] = seedCounter
    
    // generate the new seed name
    return await seedlotHelper.seedNameBuilder(entitiesArray, optional)
}

/**
 * Specialized package label builder that utilizes
 * the HARVESTED_PACKAGE_PREFIX variable value
 * stored in the occurrence_data table for the current occurrence.
 * Currently used by Maize harvest use cases
 * @param {object} dbRecord - cross or plot information
 * @param {object} entitiesArray - entities information
 * @param {string} harvestSource - plot or cross
 * @param {integer} userId - user identifier
 * @returns {object} - contains the new package label and the updated entities array
 */
async function buildPackageLabel(dbRecord, entitiesArray, harvestSource, userId) {
    let harvestedPackagePrefix = await seedlotHelper.getHarvestedPackagePrefix(dbRecord.occurrenceDbId)

    // if prefix is null, generate new one
    if(harvestedPackagePrefix == null) {
        harvestedPackagePrefix = await seedlotHelper.createHarvestedPackagePrefix(dbRecord, '-', userId)
    }

    entitiesArray[harvestSource].harvestedPackagePrefix = harvestedPackagePrefix

    // get last package label for occurrence
    let lastPackageLabel = await seedlotHelper.getLastInNumberedSequence(
        'germplasm',
        'package',
        'package_label',
        harvestedPackagePrefix,
        '[0-9]*',
        { delimiter : '-' }
    )
    // get counter for next package label
    let packageCounter = await getCounterValue(lastPackageLabel,'-')
    
    // create new package label
    let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray, {counter:packageCounter})

    return {
        newPackageLabel : newPackageLabel,
        entitiesArray : entitiesArray
    }
}

/**
 * Harvest use case for Single Cross Bulk DCP
 * 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestSingleCrossBulkDCP(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // If number of plants is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_plants !== null && harvestData.no_of_plants == 0) return true

        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }

        let newGermplasmDbId = null
        // DISABLED FOR NOW - WILL CONFIRM WITH DCP BREEDERS IF THIS IS NEEDED
        // // If BOTH parents are fixed, check if the parents have an existing F1F child
        // if (femaleParentGermplasm.germplasmState == 'fixed' && maleParentGermplasm.germplasmState == 'fixed') {
        //     let childConditions = [
        //         `child.germplasm_type = 'F1F'`
        //     ]
        //     newGermplasmDbId = await seedlotHelper.retrieveCommonChildGermplasm(
        //         femaleParentGermplasm.germplasmDbId,
        //         maleParentGermplasm.germplasmDbId,
        //         childConditions
        //     )
        // }

        // If the parents do not have a child yet, create one
        if (newGermplasmDbId == null) {
            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode,
                    includeTaxonomyName: true
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }

            // Create new designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)

            // Preparation of propagating parent attributes
            let propagateAttributeProcess = await seedlotHelper.parentAttributePropagation(crossParents)
            let heteroticGroup = propagateAttributeProcess.childHetGroup

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['parentage'] = `${femaleParentGermplasm.designation}/${maleParentGermplasm.designation}`
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_type'] = await seedlotHelper.germplasmTypeProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_CROSS_CODE')
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // HETEROTIC GROUP
            if (heteroticGroup != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'HETEROTIC_GROUP', heteroticGroup, userId)
            }

            // TAXONOMY NAME
            if (taxonomyInfo.taxonomyName != null) {
                result = await seedlotHelper.insertGermplasmAttribute(
                    newGermplasmDbId,
                    'TAXONOMY_NAME',
                    taxonomyInfo.taxonomyName,
                    userId
                )
            }

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = germplasmInfo['germplasm_name_type']
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            // Add family code number to cross info
            optional.counter = parseInt(newFamilyCode.replace(familyCodePrefix,''))
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Single Cross Bulk Maize
 * 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestSingleCrossBulkMaize(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }

        // If BOTH parents are fixed, check if the parents have an existing F1F child
        let newGermplasmDbId = null
        if (femaleParentGermplasm.germplasmState == 'fixed' && maleParentGermplasm.germplasmState == 'fixed') {
            let childConditions = [
                `child.germplasm_type = 'F1F'`
            ]
            newGermplasmDbId = await seedlotHelper.retrieveCommonChildGermplasm(
                femaleParentGermplasm.germplasmDbId,
                maleParentGermplasm.germplasmDbId,
                childConditions
            )
        }

        // If the parents do not have a child yet, create one
        if (newGermplasmDbId == null) {
            // Create new designation
            let newDesignation = await seedlotHelper.pedigreeProcess(femaleParentGermplasm, maleParentGermplasm)

            // Preparation of propagating parent attributes
            let propagateAttributeProcess = await seedlotHelper.parentAttributePropagation(crossParents)
            let grainColor = propagateAttributeProcess.childGrainColor
            let heteroticGroup = propagateAttributeProcess.childHetGroup

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['parentage'] = newDesignation
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_type'] = await seedlotHelper.germplasmTypeProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // Insert new germplasm attributes
            // GRAIN COLOR
            if (grainColor != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'GRAIN_COLOR', grainColor, userId)
            }

            // HETEROTIC GROUP
            if (heteroticGroup != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'HETEROTIC_GROUP', heteroticGroup, userId)
            }

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            // Add family code number to cross info
            optional.counter = parseInt(newFamilyCode.replace(familyCodePrefix,''))
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id

    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Three-way Cross Bulk Maize
 * 
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestThreeWayCrossBulkMaize(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }
        let femaleParentGermplasmDbId = crossParents.femaleParent.germplasmDbId
        let maleParentGermplasmDbId = crossParents.maleParent.germplasmDbId
        let childGermplasmDbId = await seedlotHelper.getChildGermplasmId(femaleParentGermplasmDbId, maleParentGermplasmDbId, false)
        let childGermplasm = await seedlotHelper.getGermplasmInfo(childGermplasmDbId)
        let isCrossed = false
        
        // Check if one parent = fixed and one non-fixed parent with germplasm type F1F
         if((femaleParentGermplasm.germplasmState == 'fixed' || maleParentGermplasm.germplasmState == 'fixed') &&
            ((femaleParentGermplasm.germplasmState == 'not_fixed' && femaleParentGermplasm.germplasmType == 'F1F') ||  
             (maleParentGermplasm.germplasmState == 'not_fixed' && maleParentGermplasm.germplasmType == 'F1F')) && 
             childGermplasmDbId > 0 && childGermplasm.germplasmType == 'F1'
        ){
            //check if there's a cross with crossMethod = 'three-way cross'
            isCrossed = await seedlotHelper.checkExistingCross(childGermplasmDbId,'three-way cross')
        }

        let newGermplasmDbId = '';
        if(!isCrossed){
            // Preparation of propagating parent attributes
            let propagateAttributeProcess = await seedlotHelper.parentAttributePropagation(crossParents)
            let grainColor = propagateAttributeProcess.childGrainColor
            let heteroticGroup = propagateAttributeProcess.childHetGroup
 
            // Create new designation
            let delimiter = '//'
            let newDesignation = await seedlotHelper.pedigreeProcess(femaleParentGermplasm, maleParentGermplasm, delimiter)

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['parentage'] = newDesignation
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_type'] = 'F1'
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // Insert new germplasm attributes
            // GRAIN COLOR
            if (grainColor != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'GRAIN_COLOR', grainColor, userId)
            }

            // HETEROTIC GROUP
            if (heteroticGroup != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'HETEROTIC_GROUP', heteroticGroup, userId)
            }
            
            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = entitiesArray.germplasm['designation']
            germplasmNameInfo['germplasm_name_type'] = entitiesArray.germplasm['germplasmNameType']
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            // Add family code number to cross info
            optional.counter = parseInt(newFamilyCode.replace(familyCodePrefix,''))
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }else{
            newGermplasmDbId = childGermplasmDbId
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)     
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)      
        if(!result) return false
        let newPackageDbId = result[0].id

    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Double Cross Bulk Maize
 * @param {object} crossRecord - cross records 
 * @param {object} harvestData - harvest data of cross
 * @param {Integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestDoubleCrossBulkMaize(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)

        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let parentNurseryStatus = 'different_nurseries'

        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }

        let hasExistingChild = false
        let germplasmDbId = ''

        let femaleParentGermplasmType = femaleParentGermplasm.germplasmType
        let maleParentGermplasmType = maleParentGermplasm.germplasmType

        // If both female and male parent germplasm_type =F1F, 
        if(femaleParentGermplasmType === 'F1F' && maleParentGermplasmType == 'F1F'){
            // check germplasm relation table
            // If there is a child_germplasm_id where parent_germplasm_id order_number=1 AND 
            // order_number=2 are equal to mother and father germplasm_id (regardless of order), 
            let existingChildDbId = await seedlotHelper.getChildGermplasmId(femaleParentGermplasm.germplasmDbId, maleParentGermplasm.germplasmDbId, false)

            // then use the child_germplasm_id and create new seed and package records linked to it
            if(existingChildDbId != 0){
                hasExistingChild = true

                let existingChild = await seedlotHelper.getGermplasmRecord(existingChildDbId)

                entitiesArray.germplasm = existingChild
                germplasmDbId = existingChildDbId
            }
        }

        if(!hasExistingChild){
            // Create new designation
            let delimiter = '//'
            let newDesignation = await seedlotHelper.pedigreeProcess(femaleParentGermplasm,maleParentGermplasm,delimiter)

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['parentage'] = newDesignation
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_type'] = 'F1'
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false

            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            germplasmDbId = newGermplasmDbId

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)

            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            // Add family code number to cross info
            optional.counter = parseInt(newFamilyCode.replace(familyCodePrefix,''))
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }
        // Preparation of propagating parent attributes
        let propagateAttributeProcess = await seedlotHelper.parentAttributePropagation(crossParents)
        let grainColor = propagateAttributeProcess.childGrainColor
        let heteroticGroup = propagateAttributeProcess.childHetGroup

        // Insert new germplasm attributes
        // GRAIN COLOR
        if (grainColor != null) {
            await seedlotHelper.insertGermplasmAttribute(germplasmDbId, 'GRAIN_COLOR', grainColor, userId)
        }

        // HETEROTIC GROUP
        if (heteroticGroup != null) {
            await seedlotHelper.insertGermplasmAttribute(germplasmDbId, 'HETEROTIC_GROUP', heteroticGroup, userId)
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}

        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = germplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false

        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}

        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}

        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel

        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)

        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)

        if(!result) return false
        let newPackageDbId = result[0].id
    }
    catch(error){
        return false
    }

    return true
}

/**
 * Harvest use case for Maternal Haploid Induction Bulk Maize
 * @param {object} crossRecord - cross records 
 * @param {object} harvestData - harvest data of cross
 * @param {Integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestMaternalHaploidInductionBulkMaize(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        entitiesArray.femaleParentGermplasm = femaleParentGermplasm
        entitiesArray.maleParentGermplasm = maleParentGermplasm
        let parentNurseryStatus = 'different_nurseries'

        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }

        let parenthesesDesignation = await seedlotHelper.encloseDesignationInParentheses(entitiesArray.femaleParentGermplasm)
        entitiesArray.femaleParentGermplasm.designation = parenthesesDesignation

        let hasExistingChild = false
        let newGermplasmDbId = ''
        
        // Check if female parent already has a haploid child
        let childConditions = [
            `child.germplasm_type = 'haploid'`
        ]
        let childGermplasmDbId = await seedlotHelper.retrieveChildGermplasm(femaleParentGermplasm.germplasmDbId, childConditions)

        // If haploid child already exists, use it at germplasm for the new seed record
        if (childGermplasmDbId != null) {
            hasExistingChild = true
            newGermplasmDbId = childGermplasmDbId
        }

        if(!hasExistingChild){
            // Create new designation
            let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            // For the parentage, copy the female parent's parentage
            germplasmInfo['parentage'] = femaleParentGermplasm.parentage
            germplasmInfo['generation'] = await seedlotHelper.getScaleValueByAbbrev('GENERATION_HAPLOID')
            germplasmInfo['germplasm_state'] = "fixed"
            germplasmInfo['germplasm_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_HAPLOID')
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm, true)
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false

            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)

            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female ONLY)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            // Populate grain color and heterotic group based on female parent
            await seedlotHelper.populateGermplasmAttribute(crossParents.femaleParent.germplasmDbId, newGermplasmDbId, "HETEROTIC_GROUP", userId)
            await seedlotHelper.populateGermplasmAttribute(crossParents.femaleParent.germplasmDbId, newGermplasmDbId, "GRAIN_COLOR", userId)

            // TO DO IN A FUTURE SPRINT
            // Add family id to getGermplasmInfo
            // //  let familyDbId = entitiesArray.femaleParentGermplasm.familyDbId
            // NOTE: RULES ARE STILL TBA
            //
            // // Add new germplasm to the family
            // let familyDbId = entitiesArray.plot.familyDbId
            // if (familyDbId !== undefined && familyDbId !== null) {
            //     result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
            //     if(!result) return false
            // }
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}

        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false

        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}

        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}

        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel

        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)

        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)

        if(!result) return false
        let newPackageDbId = result[0].id
    }
    catch(error){
        return false
    }

    return true
}

/**
 * Harvest use case for Backcross Bulk Maize
 * Creates germplasm, seed, and package, and other related records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestBackcrossBulkMaize(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // Retrieve cross parents
        let crossDbId = crossRecord.crossDbId
        let crossParents = await seedlotHelper.getCrossParents(crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }

        // Find the recurrent parent
        let recurrentParentCheck = await seedlotHelper.findBackcrossRecurrentParent(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId
        )
        // if no recurrent parent, the pairing is not a true backcross
        // so the harvest fails
        if (recurrentParentCheck == null) return false

        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // check if parent germplasm has been backcrossed before
        // also check if previous pairings are backcrosses
        let bcGermplasmDbId = 0
        let backcrossCheck = await seedlotHelper.checkBackcross(
            crossDbId, 
            femaleParentGermplasm.germplasmDbId, 
            maleParentGermplasm.germplasmDbId
        )
        // if has previous backcross, use existing child as germplasm
        if (backcrossCheck.hasPreviousBackcross) {
            bcGermplasmDbId = backcrossCheck.childGermplasmId
        }
        // if no previous backcross, create new germplasm
        else {
            // add recurrent and non recurrent parents to entities array
            let recurrentParentGermplasm = recurrentParentCheck.recurrentParentGermplasm
            let nonRecurrentParentGermplasm = recurrentParentCheck.nonRecurrentParentGermplasm
            entitiesArray.recurrentParentGermplasm = recurrentParentGermplasm
            entitiesArray.nonRecurrentParentGermplasm = nonRecurrentParentGermplasm
            // Build parentage based on recurrent parent
            let parentageProcess = await seedlotHelper.backcrossParentageProcess(entitiesArray)
            let newParentage = parentageProcess.bcParentage
            // Use parentage as designation and parentage of new germplasm
            let newDesignation = newParentage

            // Preparation of propagating parent attributes
            let propagateAttributeProcess = await seedlotHelper.parentAttributePropagation(crossParents)
            let grainColor = propagateAttributeProcess.childGrainColor
            let heteroticGroup = propagateAttributeProcess.childHetGroup

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['parentage'] = newParentage
            germplasmInfo['generation'] = await seedlotHelper.advanceGermplasmBCGeneration(nonRecurrentParentGermplasm.generation)
            germplasmInfo['germplasm_state'] = 'not_fixed'
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['crop_id'] = crossRecord.cropDbId
            germplasmInfo['germplasm_type'] = 'backcross'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            bcGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(bcGermplasmDbId);

            // Insert new germplasm attributes
            // GRAIN_COLOR
            if (grainColor != null) {
                await seedlotHelper.insertGermplasmAttribute(bcGermplasmDbId, 'GRAIN_COLOR', grainColor, userId)
            }

            // HETEROTIC GROUP
            if (heteroticGroup != null) {
                await seedlotHelper.insertGermplasmAttribute(bcGermplasmDbId, 'HETEROTIC_GROUP', heteroticGroup, userId)
            }

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = bcGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = germplasmInfo['germplasm_name_type']
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = bcGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = bcGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            // Add family code number to cross info
            optional.counter = parseInt(newFamilyCode.replace(familyCodePrefix,''))
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, bcGermplasmDbId, userId)
            if(!result) return false
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = bcGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id

    } catch (err) {
        return false
    }

    return true
 }

/**
 * Harvest use case for Hybrid Formation Bulk Maize
 * 
 * Creates germplasm(if it doesn't exist), seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestHybridFormationBulkMaize(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasmDbId = crossParents.femaleParent.germplasmDbId
        let maleParentGermplasmDbId = crossParents.maleParent.germplasmDbId
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(femaleParentGermplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(maleParentGermplasmDbId)
        let childGermplasmDbId = await seedlotHelper.getChildGermplasmId(femaleParentGermplasmDbId, maleParentGermplasmDbId)
        let childGermplasm = await seedlotHelper.getGermplasmInfo(childGermplasmDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }
        
        // Set parent designation - //replace & with forward slash if parent is hybrid 
        if(femaleParentGermplasm.germplasmType == 'hybrid'){
            femaleDesignation = femaleParentGermplasm.parentage
            femaleParentGermplasm.designation = femaleDesignation.replace('&', '/');
        }
        if(maleParentGermplasm.germplasmType == 'hybrid'){
            maleDesignation = maleParentGermplasm.parentage
            maleParentGermplasm.designation = maleDesignation.replace('&', '/');
        }

        let delimiter = '&'
        // Create new designation
        let newDesignation = await seedlotHelper.pedigreeProcess(femaleParentGermplasm, maleParentGermplasm, delimiter)

        // Preparation of propagating parent attributes
        let propagateAttributeProcess = await seedlotHelper.parentAttributePropagation(crossParents)
        let grainColor = propagateAttributeProcess.childGrainColor
        let heteroticGroup = propagateAttributeProcess.childHetGroup

        // Determine if at least one of the parents is not_fixed AND not F1F or hybrid
        let hasNotFixedNonF1FOrHybridParent = 
            (
                // Check if female parent is not_fixed AND not F1F or hybrid
                femaleParentGermplasm.germplasmState == 'not_fixed'
                && femaleParentGermplasm.germplasmType != 'F1F' 
                && (
                    femaleParentGermplasm.germplasmType == null
                    || 
                    (
                        femaleParentGermplasm.germplasmType != null 
                        && !femaleParentGermplasm.germplasmType.includes('hybrid')
                    )
                )
            )
            ||
            (
                // Check if male parent is not_fixed AND not F1F or hybrid
                maleParentGermplasm.germplasmState == 'not_fixed'
                && maleParentGermplasm.germplasmType != 'F1F' 
                && (
                    maleParentGermplasm.germplasmType == null
                    || 
                    (
                        maleParentGermplasm.germplasmType != null 
                        && !maleParentGermplasm.germplasmType.includes('hybrid')
                    )
                )
            )

        // 1.1 Create germplasm
        if(
            hasNotFixedNonF1FOrHybridParent
            || childGermplasmDbId == 0
            || childGermplasm == undefined
            || !childGermplasm.germplasmType.includes('hybrid')
        ){
            // Check for Open-pollinated variety (OPV) parents
            let germplasmTypeAbbrev = 'GERMPLASM_TYPE_HYBRID'
            let checkOPV = await seedlotHelper.checkOPVMaize(femaleParentGermplasmDbId, maleParentGermplasmDbId)
            // If at least one parent is OPV, get approriate germplasm type abbrev from the result
            if (checkOPV.hasOPV !== undefined && checkOPV.hasOPV) {
                germplasmTypeAbbrev = checkOPV.typeAbbrev !== undefined ? checkOPV.typeAbbrev : 'GERMPLASM_TYPE_HYBRID'
            }

            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['parentage'] = newDesignation
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_state'] = 'not_fixed'
            germplasmInfo['germplasm_type'] = await seedlotHelper.getScaleValueByAbbrev(germplasmTypeAbbrev)
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmInfo['crop_id'] = crossRecord.cropDbId
    
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // Insert new germplasm attributes
            // GRAIN COLOR
            if (grainColor != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'GRAIN_COLOR', grainColor, userId)
            }

            // HETEROTIC GROUP
            if (heteroticGroup != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'HETEROTIC_GROUP', heteroticGroup, userId)
            }
    
            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmNameInfo['germplasm_name_status'] = 'standard'
    
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id
    
            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false
            
            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false
        }else{//for hybrids
            newGermplasmDbId = childGermplasmDbId
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        // create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossParents.femaleParent.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Single Cross Bulk Wheat
 *
 * Creates germplasm, seed, and package records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestSingleCrossBulkWheat(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        let isBothParentsFixed = femaleParentGermplasm.germplasmState == "fixed" && maleParentGermplasm.germplasmState == "fixed"
        let F1FChild = await seedlotHelper.getF1FGermplasmChild(crossParents.femaleParent.germplasmDbId, crossParents.maleParent.germplasmDbId)
        let isNewCross = F1FChild == undefined? true: false
        let seedGermplasmDbId = null

        // Get cross number
        let crossNumberProcess =  await seedlotHelper.getChildCrossNumber(crossParents)
        // If cross number process fails, return error
        if (!crossNumberProcess.success) {
            return crossNumberProcess
        }
        let crossNumber = crossNumberProcess.crossNumber

        // If both parents' germplasm state is "fixed" or the cross is a new cross, then create new germplasm
        if(!isBothParentsFixed || isNewCross) {
            // Get growth habit
            let growthHabitResult = await seedlotHelper.growthHabitProcess(
                femaleParentGermplasm.germplasmDbId,
                maleParentGermplasm.germplasmDbId
            )
            entitiesArray.cross.growthHabit = growthHabitResult.growthHabit
            entitiesArray.cross.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue
            entitiesArray.record.growthHabit = growthHabitResult.growthHabit
            entitiesArray.record.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue

            // Build designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)
            // For Parentage Process
            let parentageProcessResult = await seedlotHelper.wheatParentageProcess(crossParents)
            // Get growth habit
            let growthHabit = entitiesArray.cross.growthHabitAttributeValue

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_name_type'] = "bcid"
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = parentageProcessResult.parentage
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_type'] = await seedlotHelper.germplasmTypeProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            let newGermplasmDbId = result[0].id
            seedGermplasmDbId = newGermplasmDbId
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // Insert growth habit of new germplasm, if any
            if (growthHabit != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'GROWTH_HABIT', growthHabit, userId)
            }

            // Last step of Parentage Process: insert new cross number attribute for new germplasm
            await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'CROSS_NUMBER', crossNumber, userId)

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = "bcid"
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false

        } else {
            // Use existing child germplasm
            seedGermplasmDbId = F1FChild.childGermplasmDbId
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }
        let seedInfo = {}
        seedInfo['germplasm_id'] = seedGermplasmDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['seed_name'] = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_entry_id'] = null  // from female cross parent and selected occ/loc
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['seed_code'] = newSeedCode
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_plot_id'] = null
        seedInfo['harvest_source'] = harvestSource
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false

        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        let packageInfo = {}
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId // = seed.source_experiment -> experiment.program_id
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_status'] = 'active'

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id

    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Top Cross Bulk Wheat
 *
 * Creates germplasm, seed, and package records
 * @param {*} crossRecord
 * @param {*} harvestData
 * @param {*} userId
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestTopCrossBulkWheat(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        let isBothParentsFixed = femaleParentGermplasm.germplasmState == "fixed" && maleParentGermplasm.germplasmState == "fixed"
        let F1FChild = await seedlotHelper.getF1FGermplasmChild(crossParents.femaleParent.germplasmDbId, crossParents.maleParent.germplasmDbId)
        let isNewCross = F1FChild == undefined? true: false
        let seedGermplasmDbId = null

        // Get cross number
        let crossNumberProcess =  await seedlotHelper.getChildCrossNumber(crossParents)
        // If cross number process fails, return error
        if (!crossNumberProcess.success) {
            return crossNumberProcess
        }
        let crossNumber = crossNumberProcess.crossNumber

        // If NOT both parents' germplasm state are "fixed" or the cross is a new cross, then create new germplasm
        if(!isBothParentsFixed || isNewCross) {
            // Get growth habit
            let growthHabitResult = await seedlotHelper.growthHabitProcess(
                femaleParentGermplasm.germplasmDbId,
                maleParentGermplasm.germplasmDbId
            )
            entitiesArray.cross.growthHabit = growthHabitResult.growthHabit
            entitiesArray.cross.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue
            entitiesArray.record.growthHabit = growthHabitResult.growthHabit
            entitiesArray.record.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue

            // Build designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)
            // For Parentage Process
            let parentageProcessResult = await seedlotHelper.wheatParentageProcess(crossParents)
            // Get growth habit
            let growthHabit = entitiesArray.cross.growthHabitAttributeValue

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['generation'] = 'F1TOP'
            germplasmInfo['germplasm_name_type'] = "bcid"
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = parentageProcessResult.parentage
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_type'] = 'F1TOP'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            let newGermplasmDbId = result[0].id
            seedGermplasmDbId = newGermplasmDbId
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // Insert growth habit of new germplasm, if any
            if (growthHabit != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'GROWTH_HABIT', growthHabit, userId)
            }

            // Last step of Parentage Process: insert new cross number attribute for new germplasm
            await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'CROSS_NUMBER', crossNumber, userId)

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = "bcid"
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        } else {
            // Use existing child germplasm
            seedGermplasmDbId = F1FChild.childGermplasmDbId
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }
        let seedInfo = {}
        seedInfo['germplasm_id'] = seedGermplasmDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['seed_name'] = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['seed_code'] = newSeedCode
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['harvest_source'] = harvestSource
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false

        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false

        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        let packageInfo = {}
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId // = seed.source_experiment -> experiment.program_id
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_status'] = 'active'

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false

        let newPackageDbId = result[0].id

    } catch (err) {
        return false
    }

    return true
}

/**
 * Harvest use case for Backcross Bulk Wheat
 * Creates germplasm, seed, and package, and other related records
 * @param {object} crossRecord - cross information
 * @param {object} harvestData -harvest data of cross
 * @param {integer} userId - user identifier
 * @param {string} harvestSource - 'plot' or 'cross
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestBackcrossBulkWheat(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        // Retrieve cross parents
        let crossDbId = crossRecord.crossDbId
        let crossParents = await seedlotHelper.getCrossParents(crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent

        // Find the recurrent parent
        let recurrentParentCheck = await seedlotHelper.findBackcrossRecurrentParent(
            femaleParentGermplasm.germplasmDbId,
            maleParentGermplasm.germplasmDbId
        )
        // if no recurrent parent, the pairing is not a true backcross
        // so the harvest fails
        if (recurrentParentCheck == null) return false

        // Get cross number
        let crossNumberProcess =  await seedlotHelper.getChildCrossNumber(crossParents, false)
        // If cross number process fails, return error
        if (!crossNumberProcess.success) {
            return crossNumberProcess
        }
        let crossNumber = crossNumberProcess.crossNumber

        // check if parent germplasm has been backcrossed before
        // also check if previous pairings are backcrosses
        let bcGermplasmDbId = 0
        let backcrossCheck = await seedlotHelper.checkBackcross(
            crossDbId, 
            femaleParentGermplasm.germplasmDbId, 
            maleParentGermplasm.germplasmDbId
        )
        // if has previous backcross, use existing child as germplasm
        if (backcrossCheck.hasPreviousBackcross) {
            bcGermplasmDbId = backcrossCheck.childGermplasmId
        }
        // if no previous backcross, create new germplasm
        else {
            // add recurrent and non recurrent parents to entities array
            let recurrentParentGermplasm = recurrentParentCheck.recurrentParentGermplasm
            let nonRecurrentParentGermplasm = recurrentParentCheck.nonRecurrentParentGermplasm
            entitiesArray.recurrentParentGermplasm = recurrentParentGermplasm
            entitiesArray.nonRecurrentParentGermplasm = nonRecurrentParentGermplasm
            // Build parentage based on recurrent parent
            let bcParentageProcess = await seedlotHelper.backcrossParentageProcess(entitiesArray, 'parentage')
            let newParentage = bcParentageProcess.bcParentage
            optional.additionalCondition = bcParentageProcess.recurrentRole

            // Get growth habit
            let growthHabitResult = await seedlotHelper.growthHabitProcess(
                femaleParentGermplasm.germplasmDbId,
                maleParentGermplasm.germplasmDbId
            )
            entitiesArray.cross.growthHabit = growthHabitResult.growthHabit
            entitiesArray.cross.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue
            entitiesArray.record.growthHabit = growthHabitResult.growthHabit
            entitiesArray.record.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue

            // Build designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray, optional)
            // Get growth habit
            let growthHabit = entitiesArray.cross.growthHabitAttributeValue

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['parentage'] = newParentage
            germplasmInfo['generation'] = await seedlotHelper.advanceGermplasmBCGeneration(nonRecurrentParentGermplasm.generation)
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_name_type'] = "bcid"
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_type'] = 'backcross'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            let newGermplasmDbId = result[0].id
            bcGermplasmDbId = newGermplasmDbId
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // Insert growth habit of new germplasm, if any
            if (growthHabit != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'GROWTH_HABIT', growthHabit, userId)
            }

            // Last step of Parentage Process: insert new cross number attribute for new germplasm
            await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'CROSS_NUMBER', crossNumber, userId)

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = "bcid"
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 1.4.a. Create new family
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }
        let seedInfo = {}
        seedInfo['germplasm_id'] = bcGermplasmDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['seed_name'] = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['seed_code'] = newSeedCode
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['harvest_source'] = harvestSource
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false

        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false

        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3 Create package
        let packageInfo = {}
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId // = seed.source_experiment -> experiment.program_id
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_status'] = 'active'

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false

        let newPackageDbId = result[0].id
    } catch (error) {
        return false
    }

    return true
 }

/**
 * Facilitate harvest for Wheat Double Cross harvest method
 * Creates germplasm, seed, package, and other related records
 * @param {object} crossRecord 
 * @param {object} harvestData 
 * @param {Integer} userId 
 * @param {string} harvestSource 
 * @param {object} prefixes 
 * @param {object} optional 
 */
async function harvestDoubleCrossBulkWheat(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try{
        // retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)

        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let parentNurseryStatus = 'different_nurseries'

        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }

        let hasExistingChild = false
        let germplasmDbId = ''
        
        let parentageProcessResult = null
        let existingChild = null
        let existingChildDbId = null

        // Get cross number
        let crossNumberProcess =  await seedlotHelper.getChildCrossNumber(crossParents)
        // If cross number process fails, return error
        if (!crossNumberProcess.success) {
            return crossNumberProcess
        }
        let crossNumber = crossNumberProcess.crossNumber

        // validation rules
        let femaleParentGermplasmType = femaleParentGermplasm.germplasmType
        let maleParentGermplasmType = maleParentGermplasm.germplasmType

        // If both female and male parent germplasm_type =F1F, 
        if(femaleParentGermplasmType === 'F1F' && maleParentGermplasmType == 'F1F'){
            // check germplasm relation table
            existingChildDbId = await seedlotHelper.getChildGermplasmId(femaleParentGermplasm.germplasmDbId, maleParentGermplasm.germplasmDbId, false)
            // If there is a child_germplasm_id where parent_germplasm_id order_number=1 AND 
            // order_number=2 are equal to mother and father germplasm_id (regardless of order), 
 
            // then use the child_germplasm_id and create new seed and package records linked to it
            if(existingChildDbId != 0){
                hasExistingChild = true

                existingChild = await seedlotHelper.getGermplasmRecord(existingChildDbId)
                entitiesArray.germplasm = existingChild
                germplasmDbId = existingChildDbId
            }
        }

        if(!hasExistingChild){
            // Get growth habit
            let growthHabitResult = await seedlotHelper.growthHabitProcess(
                femaleParentGermplasm.germplasmDbId,
                maleParentGermplasm.germplasmDbId
            )
            entitiesArray.cross.growthHabit = growthHabitResult.growthHabit
            entitiesArray.cross.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue
            entitiesArray.record.growthHabit = growthHabitResult.growthHabit
            entitiesArray.record.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue

            // Build designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)
            // For Parentage Process
            let parentageProcessResult = await seedlotHelper.wheatParentageProcess(crossParents)
            // Get growth habit
            let growthHabit = entitiesArray.cross.growthHabitAttributeValue

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_name_type'] = "bcid"
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = parentageProcessResult.parentage
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_type'] = 'F1'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)

            if (!result) return false

            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);            

            germplasmDbId = newGermplasmDbId

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = "bcid"
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)

            if(!result) return false

            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)

            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)

            if(!result) return false

            seedGermplasmDbId = newGermplasmDbId

            // Preparation of propagating parent attributes
            let propagateAttributeProcess = await seedlotHelper.parentAttributePropagation(crossParents)

            let grainColor = propagateAttributeProcess.childGrainColor

            // Insert new germplasm attributes
            // GRAIN COLOR
            if (grainColor != null) {
                await seedlotHelper.insertGermplasmAttribute(germplasmDbId, 'GRAIN_COLOR', grainColor, userId)
            }

            // CROSS NUMBER
            if (crossNumber != null) {
                await seedlotHelper.insertGermplasmAttribute(germplasmDbId, 'CROSS_NUMBER', crossNumber, userId)
            }

            // GROWTH HABIT
            if (growthHabit != null) {
                await seedlotHelper.insertGermplasmAttribute(germplasmDbId, 'GROWTH_HABIT', growthHabit, userId)
            }

            // 1.4.a. Create new family
            let newFamilyName = await seedlotHelper.familyNameBuilder(entitiesArray, optional)
            let familyCodePrefix = prefixes.familyCodePrefix
            let newFamilyCode = await seedlotHelper.generateFamilyCode(familyCodePrefix)
            let familyInfo = {}
            familyInfo['family_name'] = newFamilyName
            familyInfo['family_code'] = newFamilyCode
            familyInfo['cross_id'] = entitiesArray.cross.crossDbId
            result = await seedlotHelper.insertRecord('germplasm', 'family', familyInfo, userId)
            if(!result) return false
            let newFamilyDbId = result[0].id
            
            // 1.4.b. Add new germplasm to the family
            result = await seedlotHelper.addFamilyMember(newFamilyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // 2.1 Create seed
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        let seedInfo = {}

        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = germplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)

        if(!result) return false

        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}

        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false
        
        let seedRelationMaleInfo = {}

        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)

        if(!result) return false

        // 3 Create package
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel

        entitiesArray = packageLabelObject.entitiesArray
        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)

        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)

        if(!result) return false
        let newPackageDbId = result[0].id

        return true

    }
    catch(error){
        return false
    }
}

/**
 * 
 * @param {Object} crossRecord cross information
 * @param {Object} harvestData harvest data
 * @param {Integer} userId user identifier
 * @param {Object} harvestSource harvest source information
 * @param {Object} optional prefixes prefix information; default = {}
 * @param {Object} optional additional harvest information; default = {}  
 */
async function harvestHybridFormationBulkWheat(crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}){
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try{
        // Retrieve cross parents
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)

        let femaleParentGermplasmDbId = crossParents.femaleParent.germplasmDbId
        let maleParentGermplasmDbId = crossParents.maleParent.germplasmDbId
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(femaleParentGermplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(maleParentGermplasmDbId)

        let childGermplasmDbId = await seedlotHelper.getChildGermplasmId(femaleParentGermplasmDbId, maleParentGermplasmDbId,false)
        let childGermplasm = await seedlotHelper.getGermplasmInfo(childGermplasmDbId)

        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent

        let isNewCross = childGermplasm == undefined ? true: false
        let hasNotFixedNonF1FParent = (femaleParentGermplasm.germplasmState == 'not_fixed' && femaleParentGermplasm.germplasmType != 'F1F') ||
                                        (maleParentGermplasm.germplasmState == 'not_fixed' && maleParentGermplasm.germplasmType != 'F1F')  

        let seedGermplasmDbId = null

        // Get cross number
        let crossNumberProcess =  await seedlotHelper.getChildCrossNumber(crossParents)
        // If cross number process fails, return error
        if (!crossNumberProcess.success) {
            return crossNumberProcess
        }
        let crossNumber = crossNumberProcess.crossNumber

        // 1.1 Create germplasm
        // if new child germplasm
        if(hasNotFixedNonF1FParent || isNewCross){
            // Get growth habit
            let growthHabitResult = await seedlotHelper.growthHabitProcess(
                femaleParentGermplasm.germplasmDbId,
                maleParentGermplasm.germplasmDbId
            )

            entitiesArray.cross.growthHabit = growthHabitResult.growthHabit
            entitiesArray.cross.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue

            entitiesArray.record.growthHabit = growthHabitResult.growthHabit
            entitiesArray.record.growthHabitAttributeValue = growthHabitResult.growthHabitAttributeValue

            // For Parentage Process
            let parentageProcessResult = await seedlotHelper.wheatParentageProcess(crossParents)

            // Build designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray)

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['germplasm_name_type'] = "bcid"
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['parentage'] = parentageProcessResult.parentage
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo['germplasm_type'] = 'hybrid'
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false

            let newGermplasmDbId = result[0].id
            seedGermplasmDbId = newGermplasmDbId
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId)

            // Manage germplasm-level attributes
            // Get growth habit
            let growthHabit = entitiesArray.cross.growthHabitAttributeValue

            // Insert growth habit of new germplasm, if any
            if (growthHabit != null) {
                await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'GROWTH_HABIT', growthHabit, userId)
            }

            // Last step of Parentage Process: insert new cross number attribute for new germplasm
            await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'CROSS_NUMBER', crossNumber, userId)

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = "bcid"
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false
        }
        else{ // Use existing child germplasm
            seedGermplasmDbId = childGermplasm.germplasmDbId
        }

        // 2.1 Create seed
        let parentNurseryStatus = 'different_nurseries'
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }

        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})

        let seedInfo = {}
        seedInfo['seed_name'] = newSeedName
        seedInfo['seed_code'] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = seedGermplasmDbId
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_plot_id'] = null
        seedInfo['source_entry_id'] = null
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if(!result) return false

        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (female and male)
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
        if(!result) return false

        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2

        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
        if(!result) return false

        // 3.1 Create package
        // 3.2 create new package label
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel

        entitiesArray = packageLabelObject.entitiesArray

        let packageCodePrefix = prefixes.packageCodePrefix
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)

        let packageInfo = {}
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossParents.femaleParent.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false

        let newPackageDbId = result[0].id
    }
    catch(e){
        return false
    }

    return true
}

/**
 * Populate cross and cross parent tables
 * if the plot record does not have an
 * associated cross record yet
 * @param {object} plotRecord plot information
 * @return {object} plotRecord updated plot record with the new crossDbId
 */
async function populateCrossRecord(plotRecord,userId) {
    // if the plot does not have an associated cross record,
    // creat a cross and a cross parent record
    if(plotRecord.crossDbId == null) {
        let crossDbId = await seedlotHelper.createSelfCross(plotRecord, userId)
        plotRecord.crossDbId = crossDbId
        plotRecord.crossParentCount = 1
    }

    return plotRecord
}

/**
 * Populates the relation table of the given schema for a given entity
 * @param {*} schema schema where entity belongs
 * @param {*} entity entity relation
 * @param {*} parentDbId identifier of parent record
 * @param {*} childDbId identifier of child record
 * @param {*} userId user identifier
 */
async function populateRelationTable(schema, entity, parentDbId, childDbId, userId) {
    let supportedSchemas = ['germplasm']
    let supportedEntities = ['germplasm', 'seed']

    // if not supported, return null
    if(!supportedSchemas.includes(schema) || !supportedEntities.includes(entity)) return null

    let relationInfo = {}
    relationInfo[`parent_${entity}_id`] = parentDbId
    relationInfo[`child_${entity}_id`] = childDbId
    relationInfo['order_number'] = 1

    result = await seedlotHelper.insertRecord(schema, `${entity}_relation`, relationInfo, userId)
    if(!result) return null
}

/**
 * Create seed, package, and germplasm records
 * for bulk harvest of fixed materials
 * Currently suppports: Wheat selfing
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestBulkFixedSelfing(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        // 1. Create seed record
        let seedInfo = {}
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
        let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:additionalCondition})
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = plotRecord.germplasmDbId
        seedInfo['program_id'] = plotRecord.programDbId
        seedInfo['source_experiment_id'] = plotRecord.experimentDbId
        seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
        seedInfo['source_location_id'] = plotRecord.locationDbId
        seedInfo['source_plot_id'] = plotRecord.plotDbId
        seedInfo['source_entry_id'] = plotRecord.entryDbId
        seedInfo['cross_id'] = plotRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if (!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2. Create seed_relation record
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 3. Create package record
        packageInfo = {}
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = plotRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for bulk harvest of transgenic/genome-edited materials
 * Currently suppports: RICE SELFING
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestBulkTransgenicGenomeEditedRice(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {

    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        // 1. Create germplasm record
        let germplasmInfo = {}
        let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)
        germplasmInfo["designation"] = newDesignation
        germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(plotRecord.germplasmGeneration)
        germplasmInfo["germplasm_type"] = plotRecord.germplasmType
        germplasmInfo["germplasm_name_type"] = 'derivative_name'
        germplasmInfo["parentage"] = plotRecord.germplasmParentage
        germplasmInfo["germplasm_state"] = plotRecord.germplasmState
        germplasmInfo["crop_id"] = plotRecord.cropDbId
        germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
        if (!result) return false
        let newGermplasmDbId = result[0].id
        entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

        // 2. Create germplasm_name record
        let germplasmNameInfo = {}
        germplasmNameInfo["germplasm_id"] = newGermplasmDbId
        germplasmNameInfo["name_value"] = germplasmInfo["designation"]
        germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
        germplasmNameInfo["germplasm_name_status"] = "standard"

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
        if (!result) return false
        let newGermplasmNameDbId = result[0].id

        // 3. Create germplasm_relation record
        await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

        // Add new germplasm to the family
        let familyDbId = entitiesArray.plot.familyDbId
        if (familyDbId !== undefined && familyDbId !== null) {
            result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // Create germplasm attribute records
        let childGermplasmDbId = newGermplasmDbId
        let parentGermplasmDbId = entitiesArray.record.germplasmDbId
        
        // Populate GERMPLASM_YEAR, MTA_STATUS and ORIGIN based on parent germplasm
        await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'GERMPLASM_YEAR', userId)
        await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'MTA_STATUS', userId)
        await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'ORIGIN', userId)

        // 5. Create seed record
        let seedInfo = {}
        let newSeedCode = await seedlotHelper.generateSeedCode()
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = plotRecord.programDbId
        seedInfo['source_experiment_id'] = plotRecord.experimentDbId
        seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
        seedInfo['source_location_id'] = plotRecord.locationDbId
        seedInfo['source_plot_id'] = plotRecord.plotDbId
        seedInfo['source_entry_id'] = plotRecord.entryDbId
        seedInfo['cross_id'] = plotRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if (!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 6. Create seed_relation record
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 7. Create package record
        packageInfo = {}
        // Create package label
        let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
        let newPackageCode = await seedlotHelper.generatePackageCode()
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = ''
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = plotRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for single plant selection harvest of transgenic/genome-edited materials
 * Currently suppports: RICE SELFING
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
 async function harvestSPSTransgenicGenomeEditedRice(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {

    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {

        let recordsToCreate = parseInt(harvestData.no_of_plants)

        for (let i = 1; i <= recordsToCreate; i++) {
            // 1. Create germplasm record
            let germplasmInfo = {}
            let newDesignation = await buildGermplasmDesignation(entitiesArray)
            germplasmInfo["designation"] = newDesignation
            germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(plotRecord.germplasmGeneration)
            germplasmInfo["germplasm_type"] = plotRecord.germplasmType
            germplasmInfo["germplasm_name_type"] = 'derivative_name'
            germplasmInfo["parentage"] = plotRecord.germplasmParentage
            germplasmInfo["germplasm_state"] = plotRecord.germplasmState
            germplasmInfo["crop_id"] = plotRecord.cropDbId
            germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2. Create germplasm_name record
            let germplasmNameInfo = {}
            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if (!result) return false
            let newGermplasmNameDbId = result[0].id

            // 3. Create germplasm_relation record
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // Create germplasm attribute records
            let childGermplasmDbId = newGermplasmDbId
            let parentGermplasmDbId = entitiesArray.record.germplasmDbId
            
            // Populate GERMPLASM_YEAR, MTA_STATUS and ORIGIN based on parent germplasm
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'GERMPLASM_YEAR', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'MTA_STATUS', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'ORIGIN', userId)

            // 5. Create seed record
            let seedInfo = {}
            let newSeedCode = await seedlotHelper.generateSeedCode()
            let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = plotRecord.programDbId
            seedInfo['source_experiment_id'] = plotRecord.experimentDbId
            seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
            seedInfo['source_location_id'] = plotRecord.locationDbId
            seedInfo['source_plot_id'] = plotRecord.plotDbId
            seedInfo['source_entry_id'] = plotRecord.entryDbId
            seedInfo['cross_id'] = plotRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if (!result) return false
            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 6. Create seed_relation record
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 7. Create package record
            packageInfo = {}
            // Create package label
            let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
            let newPackageCode = await seedlotHelper.generatePackageCode()
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = ''
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = plotRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false
            let newPackageDbId = result[0].id
        }
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for bulk harvest of not_fixed materials
 * Currently suppports: Wheat selfing
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestBulkNotFixedSelfing(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        // 1. Create germplasm record
        let germplasmInfo = {}
        // Check if parent is F1TOP
        if (plotRecord.germplasmGeneration == 'F1TOP') optional.special = [ 'top' ]
        let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray, optional)
        germplasmInfo["designation"] = newDesignation
        germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
        germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(plotRecord.germplasmGeneration)
        germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
        germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_SELECTION_HISTORY')
        germplasmInfo["parentage"] = plotRecord.germplasmParentage
        germplasmInfo["germplasm_state"] = plotRecord.germplasmState
        germplasmInfo["crop_id"] = plotRecord.cropDbId
        germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
        if (!result) return false
        let newGermplasmDbId = result[0].id
        entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

        // 2. Create germplasm_name record
        let germplasmNameInfo = {}
        germplasmNameInfo["germplasm_id"] = newGermplasmDbId
        germplasmNameInfo["name_value"] = germplasmInfo["designation"]
        germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
        germplasmNameInfo["germplasm_name_status"] = "standard"

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
        if (!result) return false
        let newGermplasmNameDbId = result[0].id

        // 3. Create germplasm_relation record
        await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

        // 4. Add germplasm_attribute records for GROWTH_HABIT and CROSS_NUMBER
        await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "GROWTH_HABIT", userId)
        await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "CROSS_NUMBER", userId)

        // 4.b Add germplasm_attribute for GRAIN_COLOR if value is set
        if (entitiesArray.record.grainColor !== null) {
            // Get scale value info for the grain color
            let scaleValueInfo = await seedlotHelper.getScaleValueInfoByAbbrev('GRAIN_COLOR_' + entitiesArray.record.grainColor)
            let displayName = scaleValueInfo.displayName
            // Insert grain color display name as germplasm_attribute
            await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, "GRAIN_COLOR", displayName, userId)
        }

        // Add new germplasm to the family
        let familyDbId = entitiesArray.plot.familyDbId
        if (familyDbId !== undefined && familyDbId !== null) {
            result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // 5. Create seed record
        let seedInfo = {}
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
        let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:additionalCondition})
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = plotRecord.programDbId
        seedInfo['source_experiment_id'] = plotRecord.experimentDbId
        seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
        seedInfo['source_location_id'] = plotRecord.locationDbId
        seedInfo['source_plot_id'] = plotRecord.plotDbId
        seedInfo['source_entry_id'] = plotRecord.entryDbId
        seedInfo['cross_id'] = plotRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if (!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 6. Create seed_relation record
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 7. Create package record
        packageInfo = {}
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = plotRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for bulk harvest of not_fixed materials
 * Currently suppports: Maize selfing
 * @param {object} record - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestBulkNotFixedSelfingMaize(record, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    record = await populateCrossRecord(record,userId)

    // Check if designation needs to be in parentheses
    let startingDesignation = record.germplasmDesignation
    let germplasmParents = await seedlotHelper.getGermplasmParents(record.germplasmDbId)
    let germplasmParentCount = germplasmParents.length
    let isF1 = record.germplasmGeneration.match(/F1$/g)
    if (germplasmParentCount == 2 || isF1) {
        startingDesignation = `(${startingDesignation})`
        record.germplasmDesignation = startingDesignation
    }

    let entitiesArray = {
        record: record,
        plot: record,
        harvestMode: record.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = record

    try{
        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // 1. Create germplasm
        let germplasmInfo = {}
        let newGermplasmDbId

        // Build germplasm name
        let newDesignation = await buildGermplasmDesignationWithTextRepeater(entitiesArray, optional)

        // Check if germplasm of the same name already exists
        let germplasmExisting = await seedlotHelper.getOneRecord(
            'germplasm',
            'germplasm',
            `WHERE designation = '${newDesignation.replace(/'/g, `''`)}' AND is_void = FALSE`
        )

        // If germplasm does not exist, create a new one
        if (germplasmExisting === undefined) {
            germplasmInfo["designation"] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(record.germplasmGeneration)
            germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')
            germplasmInfo["parentage"] = record.germplasmParentage
            germplasmInfo["germplasm_state"] = record.germplasmState
            germplasmInfo["crop_id"] = record.cropDbId
            germplasmInfo["taxonomy_id"] = record.germplasmTaxonomyId
            
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);
            
            // 1.a generate germplasm name
            let germplasmNameInfo = {}

            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            // 1.b create germplasm record
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if (!result) return false
            
            let newGermplasmNameDbId = result[0].id

            // 1.d create germplasm relation record
            await populateRelationTable('germplasm', 'germplasm', record.germplasmDbId, newGermplasmDbId, userId)

            // 1.c create germplasm data record for GROWTH_HABIT, CROSS_NUMBER,  HETEROTIC_GROUP, GRAIN_COLOR
            await seedlotHelper.populateGermplasmAttribute(record.germplasmDbId, newGermplasmDbId, "GROWTH_HABIT", userId)
            await seedlotHelper.populateGermplasmAttribute(record.germplasmDbId, newGermplasmDbId, "CROSS_NUMBER", userId)
            await seedlotHelper.populateGermplasmAttribute(record.germplasmDbId, newGermplasmDbId, "HETEROTIC_GROUP", userId)
            await seedlotHelper.populateGermplasmAttribute(record.germplasmDbId, newGermplasmDbId, "GRAIN_COLOR", userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }
        }
        else {
            newGermplasmDbId = germplasmExisting.id
        }

        // 2. Create seed
        let seedInfo = {}
        let seedCodePrefix = prefixes.seedCodePrefix

        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(record.experimentDbId)
        let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'

        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:additionalCondition})

        // 2.a Create seed record
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = record.programDbId
        seedInfo['source_experiment_id'] = record.experimentDbId
        seedInfo['source_occurrence_id'] = record.occurrenceDbId
        seedInfo['source_location_id'] = record.locationDbId
        seedInfo['source_plot_id'] = record.plotDbId
        seedInfo['source_entry_id'] = record.entryDbId
        seedInfo['cross_id'] = record.crossDbId
        seedInfo['harvest_source'] = harvestSource
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        
        if (!result) return false
        
        let newSeedDbId = result[0].id
        
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)
        
        // 2.b create seed relation record
        await populateRelationTable('germplasm', 'seed', record.seedDbId, newSeedDbId, userId)

        // 3. Create Package
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(record, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        
        // 3.a create package record
        packageInfo = {}
        
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = record.programDbId
        
        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        
        if(!result) return false
        let newPackageDbId = result[0].id
    }
    catch(error){
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for bulk harvest of not_fixed materials
 * Currently suppports: Cowpea selfing
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestBulkNotFixedSelfingCowpea(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    // adjust abbrev values for helpers
    let generation = String(plotRecord.germplasmGeneration).trim()
    let cowpeaVariables = await seedlotHelper.findVariablesByGeneration(generation)
    try {
        // 1. Create germplasm record
        let germplasmInfo = {}

        let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray, optional)
        germplasmInfo["designation"] = newDesignation
        germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
        germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(generation)
        germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev(cowpeaVariables.germplasmType)
        germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev(cowpeaVariables.germplasmNameType)
        germplasmInfo["parentage"] = plotRecord.germplasmParentage
        germplasmInfo["germplasm_state"] = cowpeaVariables.germplasmState
        germplasmInfo["crop_id"] = plotRecord.cropDbId
        germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
        if (!result) return false
        let newGermplasmDbId = result[0].id
        entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

        // 2. Create germplasm_name record
        let germplasmNameInfo = {}
        germplasmNameInfo["germplasm_id"] = newGermplasmDbId
        germplasmNameInfo["name_value"] = germplasmInfo["designation"]
        germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
        germplasmNameInfo["germplasm_name_status"] = "standard"

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
        if (!result) return false
        let newGermplasmNameDbId = result[0].id

        // 3. Create germplasm_relation record
        await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

        // 4. Add new germplasm to the family
        let familyDbId = entitiesArray.plot.familyDbId
        if (familyDbId !== undefined && familyDbId !== null) {
            result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // // 5. Create seed record
        let seedInfo = {}
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
        let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:additionalCondition})
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = plotRecord.programDbId
        seedInfo['source_experiment_id'] = plotRecord.experimentDbId
        seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
        seedInfo['source_location_id'] = plotRecord.locationDbId
        seedInfo['source_plot_id'] = plotRecord.plotDbId
        seedInfo['source_entry_id'] = plotRecord.entryDbId
        seedInfo['cross_id'] = plotRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if (!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 6. Create seed_relation record
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 7. Create package record
        packageInfo = {}
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = plotRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for bulk harvest of not_fixed materials
 * Currently suppports: Dryland crops selfing
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestBulkNotFixedSelfingDCP(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    // If number of plants is supplied, check if value is 0.
    // If 0, do not create new records. Return 'true'.
    if (harvestData.no_of_plants !== null && harvestData.no_of_plants == 0) return true

    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    // adjust abbrev values for helpers
    let generation = String(plotRecord.germplasmGeneration).trim()
    
    try {
        let newGermplasmDbId = ''

        // check if has germplasm record
        let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray, optional)

        // Check if germplasm of the same name already exists
        let germplasmExisting = await seedlotHelper.getOneRecord(
            'germplasm',
            'germplasm',
            `WHERE designation = '${newDesignation.replace(/'/g, `''`)}' AND is_void = FALSE`
        )

        if (germplasmExisting) {
            newGermplasmDbId = germplasmExisting.id
            entitiesArray.germplasm = germplasmExisting
        }
        else {
            // 1. Create germplasm record
            let germplasmInfo = {}

            germplasmInfo["designation"] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(generation)
            germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev("GERMPLASM_TYPE_SEGREGATING")
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev("GERMPLASM_NAME_TYPE_SELECTION_HISTORY")
            germplasmInfo["parentage"] = plotRecord.germplasmParentage
            germplasmInfo["germplasm_state"] = plotRecord.germplasmState
            germplasmInfo["crop_id"] = plotRecord.cropDbId
            germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2. Create germplasm_name record
            let germplasmNameInfo = {}
            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if (!result) return false
            let newGermplasmNameDbId = result[0].id

            // 3. Create germplasm_relation record
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // Create germplasm data record for HETEROTIC_GROUP
            await seedlotHelper.populateGermplasmAttribute(entitiesArray.record.germplasmDbId, newGermplasmDbId, "HETEROTIC_GROUP", userId)

            // 4. Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if (!result) return false
            }
        }

        // // 5. Create seed record
        let seedInfo = {}
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
        let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:additionalCondition})
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = plotRecord.programDbId
        seedInfo['source_experiment_id'] = plotRecord.experimentDbId
        seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
        seedInfo['source_location_id'] = plotRecord.locationDbId
        seedInfo['source_plot_id'] = plotRecord.plotDbId
        seedInfo['source_entry_id'] = plotRecord.entryDbId
        seedInfo['cross_id'] = plotRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if (!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 6. Create seed_relation record
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 7. Create package record
        packageInfo = {}
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = plotRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for single plant harvest of not_fixed materials
 * Currently suppports: Dryland selfing
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestSinglePlantNotFixedSelfingDCP(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    
    plotRecord = await seedlotHelper.populateCrossRecord(plotRecord,userId);

    let recordsToCreate = parseInt(harvestData.no_of_plants)
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        for (let i = 1; i <= recordsToCreate; i++) {
            // 1. Create germplasm record
            let newDesignation = await seedlotHelper.buildGermplasmDesignation(entitiesArray)

            let germplasmInfo = {}
            
            germplasmInfo["designation"] = newDesignation
            germplasmInfo["parentage"] = plotRecord.germplasmParentage
            germplasmInfo["germplasm_state"] = plotRecord.germplasmState
            germplasmInfo["crop_id"] = plotRecord.cropDbId
            germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId
            
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(plotRecord.germplasmGeneration)
            germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_SELECTION_HISTORY')

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false

            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2. Create germplasm_name record
            let germplasmNameInfo = {}
            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if (!result) return false

            let newGermplasmNameDbId = result[0].id

            // 3. Create germplasm_relation record
            await seedlotHelper.populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // 4. Add germplasm_attribute records for HETEROTIC_GROUP
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "HETEROTIC_GROUP", userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // 5. Create seed record
            let seedInfo = {}
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)

            // build seed name
            let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
            let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
            optional['additionalCondition'] = additionalCondition
            let newSeedName = await buildSeedName(entitiesArray, optional)

            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = plotRecord.programDbId
            seedInfo['source_experiment_id'] = plotRecord.experimentDbId
            seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
            seedInfo['source_location_id'] = plotRecord.locationDbId
            seedInfo['source_plot_id'] = plotRecord.plotDbId
            seedInfo['source_entry_id'] = plotRecord.entryDbId
            seedInfo['cross_id'] = plotRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if (!result) return false

            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 6. Create seed_relation record
            await seedlotHelper.populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 7. Create package record

            // create new package label
            let packageLabelObject = await seedlotHelper.buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel

            entitiesArray = packageLabelObject.entitiesArray

            let packageInfo = {}
            let packageCodePrefix = prefixes.packageCodePrefix
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = plotRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false

            let newPackageDbId = result[0].id
        }
    } catch (error) {
        return false
    }

    return true
}

/**
 * Facilitate the creation of harvest materials records for not_fixed Maize materials
 * with Maintain and Bulk harvest method  
 * @param {Object} record 
 * @param {Object} harvestData 
 * @param {Integer} userId 
 * @param {string} harvestSource 
 * @param {Object} prefixes 
 * @param {Object} optional 
 */
async function harvestMaintainAndBulkNotFixedSelfingMaize(record, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    // By selecting this method, a user is indicating that they have taken care to 
    // maintain the allele frequency of a non_fixed material so that they can harvest 
    // a new seed lot and package of the same germplasm; so no new germplasm record 
    // should be created whenever this method is chosen.

    record = await populateCrossRecord(record,userId)

    let entitiesArray = {
        record: record,
        plot: record,
        harvestMode: record.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = record

    try{
        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0) return true

        // 1. Create seed
        let seedInfo = {}
        let seedCodePrefix = prefixes.seedCodePrefix

        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(record.experimentDbId)
        let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'

        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:additionalCondition})

        // 1.a Create seed record
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = record.germplasmDbId
        seedInfo['program_id'] = record.programDbId
        seedInfo['source_experiment_id'] = record.experimentDbId
        seedInfo['source_occurrence_id'] = record.occurrenceDbId
        seedInfo['source_location_id'] = record.locationDbId
        seedInfo['source_plot_id'] = record.plotDbId
        seedInfo['source_entry_id'] = record.entryDbId
        seedInfo['cross_id'] = record.crossDbId
        seedInfo['harvest_source'] = harvestSource
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        
        if (!result) return false
        
        let newSeedDbId = result[0].id
        
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 1.b create seed relation record
        await populateRelationTable('germplasm', 'seed', record.seedDbId, newSeedDbId, userId)

        // 2. Create Package
        // Create package label
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await buildPackageLabel(record, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        entitiesArray = packageLabelObject.entitiesArray

        // 2.a create package record
        packageInfo = {}
        
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = record.programDbId
        
        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        
        if(!result) return false

        let newPackageDbId = result[0].id

        return true
    }
    catch(error){
        return false
    }
}

/**
 * Create seed, package, and germplasm records
 * for individual ear harvest of not_fixed materials in Maize
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestNumberedSelectionSelfingMaize(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    // Check if designation needs to be in parentheses
    let startingDesignation = plotRecord.germplasmDesignation
    let germplasmParents = await seedlotHelper.getGermplasmParents(plotRecord.germplasmDbId)
    let germplasmParentCount = germplasmParents.length
    let isF1 = plotRecord.germplasmGeneration.match(/F1$/g)
    if (germplasmParentCount == 2 || isF1) {
        startingDesignation = `(${startingDesignation})`
        plotRecord.germplasmDesignation = startingDesignation
    }

    let recordsToCreate = parseInt(harvestData.no_of_ears)
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        for (let i = 1; i <= recordsToCreate; i++) {
            // 1. Create germplasm record
            let newDesignation = await buildGermplasmDesignation(entitiesArray)
            let germplasmInfo = {}
            germplasmInfo["designation"] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo["parentage"] = plotRecord.germplasmParentage
            germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(plotRecord.germplasmGeneration)
            germplasmInfo["germplasm_state"] = plotRecord.germplasmState
            germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
            germplasmInfo["crop_id"] = plotRecord.cropDbId
            germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2. Create germplasm_name record
            let germplasmNameInfo = {}
            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if (!result) return false
            let newGermplasmNameDbId = result[0].id

            // 3. Create germplasm_relation record
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // 4. Add germplasm_attribute records for HETEROTIC_GROUP, GRAIN_COLOR, GROWTH_HABIT
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "HETEROTIC_GROUP", userId)
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "GRAIN_COLOR", userId)
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "GROWTH_HABIT", userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }
            
            // 5. Create seed record
            let seedInfo = {}
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            // build seed name
            let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
            let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
            optional['additionalCondition'] = additionalCondition
            let newSeedName = await buildSeedName(entitiesArray, optional)
            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = plotRecord.programDbId
            seedInfo['source_experiment_id'] = plotRecord.experimentDbId
            seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
            seedInfo['source_location_id'] = plotRecord.locationDbId
            seedInfo['source_plot_id'] = plotRecord.plotDbId
            seedInfo['source_entry_id'] = plotRecord.entryDbId
            seedInfo['cross_id'] = plotRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if (!result) return false
            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 6. Create seed_relation record
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 7. Create package record
            // create new package label
            let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel
            entitiesArray = packageLabelObject.entitiesArray
            packageInfo = {}
            let packageCodePrefix = prefixes.packageCodePrefix
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
            // let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray, {counter:1})
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = plotRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false
            let newPackageDbId = result[0].id
        }
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for individual spike/single plant selection harvest of not_fixed materials in Wheat
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestNumberedSelectionSelfingWheat(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let recordsToCreate = parseInt(harvestData.no_of_plants)
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        for (let i = 1; i <= recordsToCreate; i++) {
            // 1. Create germplasm record
            optional.suffix = plotRecord.originSiteCode
            // Check if parent is F1TOP
            if (plotRecord.germplasmGeneration == 'F1TOP') {
                optional.special = [ 'top' ]
                optional.suffix = 'TOP' + plotRecord.originSiteCode
            }
            // generate new designation
            let newDesignation = await buildGermplasmDesignation(entitiesArray, optional)
            let germplasmInfo = {}
            germplasmInfo["designation"] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(plotRecord.germplasmGeneration)
            germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_SELECTION_HISTORY')
            germplasmInfo["parentage"] = plotRecord.germplasmParentage
            germplasmInfo["germplasm_state"] = plotRecord.germplasmState
            germplasmInfo["crop_id"] = plotRecord.cropDbId
            germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2. Create germplasm_name record
            let germplasmNameInfo = {}
            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if (!result) return false
            let newGermplasmNameDbId = result[0].id

            // 3. Create germplasm_relation record
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // 4. Add germplasm_attribute records for GROWTH_HABIT and CROSS_NUMBER
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "GROWTH_HABIT", userId)
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "CROSS_NUMBER", userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // 5. Create seed record
            let seedInfo = {}
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            // build seed name
            let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
            let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
            optional['additionalCondition'] = additionalCondition
            let newSeedName = await buildSeedName(entitiesArray, optional)
            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = plotRecord.programDbId
            seedInfo['source_experiment_id'] = plotRecord.experimentDbId
            seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
            seedInfo['source_location_id'] = plotRecord.locationDbId
            seedInfo['source_plot_id'] = plotRecord.plotDbId
            seedInfo['source_entry_id'] = plotRecord.entryDbId
            seedInfo['cross_id'] = plotRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if (!result) return false
            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 6. Create seed_relation record
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 7. Create package record
            packageInfo = {}
            // Create package label
            let packageCodePrefix = prefixes.packageCodePrefix
            let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
            entitiesArray = packageLabelObject.entitiesArray
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = plotRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false
            let newPackageDbId = result[0].id
        }
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed and package records
 * for individual ear harvest of wheat fixed materials
 * @param {object} record - object containing record information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestNumberedSelectionFixedSelfing(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let recordsToCreate = parseInt(harvestData.no_of_plants)
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        for (let i = 1; i <= recordsToCreate; i++) {
            // 1. Create seed record
            let seedInfo = {}
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            // build seed name
            let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
            let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
            optional['additionalCondition'] = additionalCondition
            let newSeedName = await buildSeedName(entitiesArray, optional)
            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = plotRecord.germplasmDbId
            seedInfo['program_id'] = plotRecord.programDbId
            seedInfo['source_experiment_id'] = plotRecord.experimentDbId
            seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
            seedInfo['source_location_id'] = plotRecord.locationDbId
            seedInfo['source_plot_id'] = plotRecord.plotDbId
            seedInfo['source_entry_id'] = plotRecord.entryDbId
            seedInfo['cross_id'] = plotRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if (!result) return false
            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 2. Create seed_relation record
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 3. Create package record
            packageInfo = {}
            // Create package label
            let packageCodePrefix = prefixes.packageCodePrefix
            let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
            entitiesArray = packageLabelObject.entitiesArray
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = plotRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false
            let newPackageDbId = result[0].id
        }
    } catch (e) {
        return false
    }

    return true
}

/**
 * Creates n germplasm, seeds and packages, 
 * where n = number of specific plant numbers
 * (eg. 1,4,5 creates 3 germplasm, seeds, and packages)
 * @param {object} plotRecord plot information
 * @param {object} harvestData plot harvest data
 * @param {integer} userId id of user
 * @param {string} harvestSource 'plot' or 'cross'
 * @return {boolean} true/false - whether or not the creation of records was successful
 */
async function createSeedlotForNumberedSelectionSpecific(plotRecord, harvestData, userId, harvestSource) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestData: harvestData,
        harvestMode: plotRecord.harvestMode
    }

    try {
        let germplasmGeneration = plotRecord.germplasmGeneration
        let specificPlantNumber = harvestData.specific_plant
        let plantNumbers = specificPlantNumber.split(",")

        for (let i = 0; i < plantNumbers.length; i++) {
            // 1. Create germplasm record
            // // 1.a. Generate the next germplasm designation and generation
            entitiesArray.harvestData.specific_plant = plantNumbers[i]
            let nextGermplasmDesignation = await buildGermplasmDesignationWithRepeater(entitiesArray)

            let newGermplasmGeneration = await seedlotHelper.advanceGermplasmGeneration(String(germplasmGeneration))

            // 1.b. Assemble germplasm data
            let gemplasmNameType = plotRecord.germplasmState == 'not_fixed' ? 'derivative_name' : plotRecord.germplasmNameType
            let germplasmData = await assembleGermplasmData(nextGermplasmDesignation, newGermplasmGeneration, plotRecord, gemplasmNameType)

            // 1.c. Create Germplasm
            let germplasmCreated = await seedlotHelper.createGermplasmRecord(germplasmData, userId)
            let newGermplasmDbId = germplasmCreated[0].id
            germplasmData.germplasmDbId = newGermplasmDbId
            await seedlotHelper.createGermplasmName(germplasmData, plotRecord, userId)
            let germplasmRecord = await seedlotHelper.getGermplasmRecord(germplasmCreated[0].id)
            entitiesArray.germplasm = germplasmRecord

            // Create backcross derivative name
            let germplasmNameInfo = {}
            if (entitiesArray.plot !== undefined
                && entitiesArray.plot.backcrossDerivativeName !== undefined
                && entitiesArray.plot.backcrossDerivativeName !== null
            ) {
                germplasmNameInfo["germplasm_id"] = newGermplasmDbId
                germplasmNameInfo["name_value"] = await buildGermplasmDesignation(entitiesArray, {}, "germplasm_name")
                germplasmNameInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev("GERMPLASM_NAME_TYPE_BACKCROSS_DERIVATIVE")
                germplasmNameInfo["germplasm_name_status"] = "active"

                result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
                if (!result) return false
            }

            // 1.d Populate germplasm relation
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // Create germplasm attribute records
            let childGermplasmDbId = newGermplasmDbId
            let parentGermplasmDbId = entitiesArray.record.germplasmDbId
            
            // Populate GERMPLASM_YEAR, MTA_STATUS and ORIGIN based on parent germplasm
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'GERMPLASM_YEAR', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'MTA_STATUS', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'ORIGIN', userId)

            // 2. Create seed record
            // 2.a. Generate seed name
            let seedName = await seedlotHelper.seedNameBuilder(entitiesArray)

            // 2.b. Assemble seed data
            let seedData = await assembleSeedData(seedName, harvestData, plotRecord, germplasmRecord, harvestSource)

            // 2.c. Create Seed
            let seedCreated = await seedlotHelper.createSeedRecord(seedData, userId)
            let newSeedDbId = seedCreated[0].id
            let seedRecord = await seedlotHelper.getSeedInformation(newSeedDbId)
            entitiesArray.seed = seedRecord

            // 2.d Populate seed relation
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 3. Create package record
            // 3.a. Generate package label
            let packageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)

            // 3.b. Assemble package data
            let packageData = await assemblePackageData(packageLabel, plotRecord, seedRecord)

            // // 3.c. Create package record
            let packageCreated = await seedlotHelper.createPackageRecord(packageData, userId)
            let packageRecord = await seedlotHelper.getPackageInformation(packageCreated[0].id)
        }

    } catch (e) {
        return false
    }

    return true
}

/**
 * Creates n germplasm, seeds and packages, 
 * where n = numeric variable such as NO_OF_PLANT, PANNO_SEL, etc.
 * @param {object} plotRecord plot information
 * @param {object} harvestData plot harvest data
 * @param {integer} userId id of user
 * @param {string} harvestSource 'plot' or 'cross'
 * @return {boolean} true/false - whether or not the creation of records was successful
 */
async function createSeedlotForNumberedSelectionContinuous(plotRecord, harvestData, userId, harvestSource) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode
    }

    try {
        let startingDesignation = plotRecord.germplasmDesignation
        let germplasmGeneration = plotRecord.germplasmGeneration
        let recordsToCreate = parseInt(harvestData.no_of_plants)

        // create seedlots based on number of plants MINUS number of seedlots already created
        for (let i = 0; i < recordsToCreate; i++) {
            // 1. Create germplasm record
            // // 1.a. Generate the next germplasm designation and generation
            let nextGermplasmDesignation = await buildGermplasmDesignation(entitiesArray)

            let newGermplasmGeneration = await seedlotHelper.advanceGermplasmGeneration(String(germplasmGeneration))

            // 1.b. Assemble germplasm data
            let gemplasmNameType = plotRecord.germplasmState == 'not_fixed' ? 'derivative_name' : plotRecord.germplasmNameType;
            let germplasmData = await assembleGermplasmData(nextGermplasmDesignation, newGermplasmGeneration, plotRecord, gemplasmNameType)

            // 1.c Create Germplasm
            let germplasmCreated = await seedlotHelper.createGermplasmRecord(germplasmData, userId)
            let newGermplasmDbId = germplasmCreated[0].id
            germplasmData.germplasmDbId = newGermplasmDbId
            await seedlotHelper.createGermplasmName(germplasmData, plotRecord, userId)
            let germplasmRecord = await seedlotHelper.getGermplasmRecord(germplasmCreated[0].id)
            entitiesArray.germplasm = germplasmRecord

            // Create backcross derivative name
            result = await riceSelfingBCDerivativeNameProcess(entitiesArray, newGermplasmDbId, userId)
            if (!result) return false

            // 1.d Populate germplasm relation
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // Create germplasm attribute records
            let childGermplasmDbId = newGermplasmDbId
            let parentGermplasmDbId = entitiesArray.record.germplasmDbId

            // Populate GERMPLASM_YEAR, MTA_STATUS and ORIGIN based on parent germplasm
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'GERMPLASM_YEAR', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'MTA_STATUS', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'ORIGIN', userId)

            // 2. Create seed record
            // 2.a. Generate seed name
            let seedName = await seedlotHelper.seedNameBuilder(entitiesArray)

            // 2.b. Assemble seed data
            let seedData = await assembleSeedData(seedName, harvestData, plotRecord, germplasmRecord, harvestSource)

            // 2.c. Create Seed
            let seedCreated = await seedlotHelper.createSeedRecord(seedData, userId)
            let newSeedDbId = seedCreated[0].id
            let seedRecord = await seedlotHelper.getSeedInformation(newSeedDbId)
            entitiesArray.seed = seedRecord

            // 2.d Populate seed relation
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 3. Create package record
            // 3.a. Generate package label
            let packageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)

            // 3.b. Assemble package data
            let packageData = await assemblePackageData(packageLabel, plotRecord, seedRecord)

            // // 3.c. Create package record
            let packageCreated = await seedlotHelper.createPackageRecord(packageData, userId)
            let packageRecord = await seedlotHelper.getPackageInformation(packageCreated[0].id)
        }
    } catch (e) {
        return false
    }

    return true
}

/**
 * Creates seed and package
 * for bulk harvest of FIXED material
 * @param {object} plotRecord plot information
 * @param {object} harvestData plot harvest data
 * @param {integer} userId id of user
 * @param {string} harvestSource 'plot' or 'cross'
 * @return {boolean} true/false - whether or not the creation of records was successful
 */
async function createSeedLotFixedBulk(plotRecord, harvestData, userId, harvestSource) {
    plotRecord = await populateCrossRecord(plotRecord,userId)
    
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode
    }

    try{
        let germplasmRecord = await seedlotHelper.getGermplasmRecord(plotRecord.germplasmDbId)
        entitiesArray.germplasm = germplasmRecord

        // 2. Create seed record
        // 2.a. Generate seed name
        let seedName = await seedlotHelper.seedNameBuilder(entitiesArray)

        // 2.b. Assemble seed data
        let seedData = await assembleSeedData(seedName, harvestData, plotRecord, germplasmRecord, harvestSource)

        // 2.c. Create Seed
        let seedCreated = await seedlotHelper.createSeedRecord(seedData, userId)
        let newSeedDbId = seedCreated[0].id
        let seedRecord = await seedlotHelper.getSeedInformation(newSeedDbId)
        entitiesArray.seed = seedRecord

        // 2.d Populate seed relation
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 3. Create package record
        // 3.a. Generate package label
        let packageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)

        // 3.b. Assemble package data
        let packageData = await assemblePackageData(packageLabel, plotRecord, seedRecord)

        // 3.c. Create package record
        let packageCreated = await seedlotHelper.createPackageRecord(packageData, userId)
        let packageRecord = await seedlotHelper.getPackageInformation(packageCreated[0].id)

    } catch (e) {
        return false
    }

    return true
}

/**
 * Creates 1 germplasm, seed, and package
 * for NOT FIXED material
 * @param {object} plotRecord plot information
 * @param {object} harvestData plot harvest data
 * @param {integer} userId id of user
 * @param {string} harvestSource 'plot' or 'cross'
 * @return {boolean} true/false - whether or not the creation of records was successful
 */
async function createSeedLotWithTextPattern(plotRecord, harvestData, userId, harvestSource){
    plotRecord = await populateCrossRecord(plotRecord,userId)
    
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode
    }

    try{
        let germplasmGeneration = plotRecord.germplasmGeneration
        let newGermplasmDbId
        let germplasmRecord

        // 1. Create germplasm record
        // 1.a Set new germplasm equal to the source germplasm designation plus text pattern (-B / -B RGA)
        let nextGermplasmDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)

        // Get normalized name from designation using germplasm helper
        let normalizedName = await germplasmHelper.normalizeText(nextGermplasmDesignation)
        // Check if germplasm of the same name already exists
        let germplasmExisting = await seedlotHelper.getOneRecord(
            'germplasm',
            'germplasm_name',
            `WHERE germplasm_normalized_name = '${normalizedName.replace(/'/g, `''`)}' AND is_void = FALSE`
        )

        // If germplasm does not exist, create a new one
        if (germplasmExisting === undefined) {
            // 1.b Advance germplasm generation e.g. F2 to F3
            let newGermplasmGeneration = await seedlotHelper.advanceGermplasmGeneration(String(germplasmGeneration))

            // Set germplasm state same the source germplasm state
            // 1.c. Assemble germplasm data
            let gemplasmNameType = plotRecord.germplasmState == 'not_fixed' ? 'derivative_name' : plotRecord.germplasmNameType
            let germplasmData = await assembleGermplasmData(nextGermplasmDesignation, newGermplasmGeneration, plotRecord, gemplasmNameType)

            // 1.d Create Germplasm
            let germplasmCreated = await seedlotHelper.createGermplasmRecord(germplasmData, userId)
            newGermplasmDbId = germplasmCreated[0].id
            germplasmData.germplasmDbId = newGermplasmDbId
            await seedlotHelper.createGermplasmName(germplasmData, plotRecord, userId)
            germplasmRecord = await seedlotHelper.getGermplasmRecord(germplasmCreated[0].id)
            entitiesArray.germplasm = germplasmRecord

            // Create backcross derivative name
            let germplasmNameInfo = {}
            if (entitiesArray.plot !== undefined
                && entitiesArray.plot.backcrossDerivativeName !== undefined
                && entitiesArray.plot.backcrossDerivativeName !== null
            ) {
                germplasmNameInfo["germplasm_id"] = newGermplasmDbId
                germplasmNameInfo["name_value"] = await seedlotHelper.backcrossDerivativeNameBuilder(entitiesArray)
                germplasmNameInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev("GERMPLASM_NAME_TYPE_BACKCROSS_DERIVATIVE")
                germplasmNameInfo["germplasm_name_status"] = "active"

                result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
                if (!result) return false
            }

            // 1.d Populate germplasm relation
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // Create germplasm attribute records
            let childGermplasmDbId = newGermplasmDbId
            let parentGermplasmDbId = entitiesArray.record.germplasmDbId

            // Populate GERMPLASM_YEAR, MTA_STATUS and ORIGIN based on parent germplasm
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'GERMPLASM_YEAR', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'MTA_STATUS', userId)
            await seedlotHelper.populateGermplasmAttribute(parentGermplasmDbId, childGermplasmDbId, 'ORIGIN', userId)
        }
        else {
            newGermplasmDbId = germplasmExisting.germplasm_id
            germplasmRecord = await seedlotHelper.getGermplasmRecord(newGermplasmDbId)
        }

        // 2. Create seed record
        // 2.a. Generate seed name
        let seedName = await seedlotHelper.seedNameBuilder(entitiesArray)

        // 2.b. Assemble seed data
        let seedData = await assembleSeedData(seedName, harvestData, plotRecord, germplasmRecord, harvestSource)

        // 2.c. Create Seed
        let seedCreated = await seedlotHelper.createSeedRecord(seedData, userId)
        let newSeedDbId = seedCreated[0].id
        let seedRecord = await seedlotHelper.getSeedInformation(newSeedDbId)
        entitiesArray.seed = seedRecord

        // 2.d Populate seed relation
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 3. Create package record
        // 3.a. Generate package label
        let packageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)

        // 3.b. Assemble package data
        let packageData = await assemblePackageData(packageLabel, plotRecord, seedRecord)

        // 3.c. Create package record
        let packageCreated = await seedlotHelper.createPackageRecord(packageData, userId)
        let packageRecord = await seedlotHelper.getPackageInformation(packageCreated[0].id)

    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for Rice R-line harvest use case.
 * @param {object} plotRecord - object containing plot information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestRiceFixedRLineHarvest(plotRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) {
    plotRecord = await populateCrossRecord(plotRecord,userId)

    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        // Validation: Check plot data for HYB_MATERIAL_TYPE
        let plotData = await seedlotHelper.getPlotData(plotRecord.plotDbId, 'HYB_MATERIAL_TYPE')
        // If plot data value is not 'R', harvest fails
        if (plotData == null) {
            return {
                success: false,
                errors: ['Missing plot data for variable HYB_MATERIAL_TYPE.']
            }
        }
        if (plotData.data_value != 'R') {
            return {
                success: false,
                errors: ['Plot data value for HYB_MATERIAL_TYPE is not R (current value = ' + plotData.data_value + ').']
            }
        }

        // 1. Create germplasm record
        let germplasmInfo = {}
        let newDesignation = await seedlotHelper.germplasmNameBuilder(entitiesArray)
        germplasmInfo["designation"] = newDesignation
        germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode()
        germplasmInfo["generation"] = plotRecord.germplasmGeneration
        germplasmInfo["germplasm_type"] = plotRecord.germplasmType
        germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_LINE_NAME')
        germplasmInfo["parentage"] = plotRecord.germplasmParentage
        germplasmInfo["germplasm_state"] = plotRecord.germplasmState
        germplasmInfo["crop_id"] = plotRecord.cropDbId
        germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
        if (!result) return false
        let newGermplasmDbId = result[0].id
        entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

        // 2. Create germplasm_name record
        let germplasmNameInfo = {}
        germplasmNameInfo["germplasm_id"] = newGermplasmDbId
        germplasmNameInfo["name_value"] = germplasmInfo["designation"]
        germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
        germplasmNameInfo["germplasm_name_status"] = "standard"

        result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
        if (!result) return false
        let newGermplasmNameDbId = result[0].id

        // 3. Create germplasm_relation record
        await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

        // Add new germplasm to the family
        let familyDbId = entitiesArray.plot.familyDbId
        if (familyDbId !== undefined && familyDbId !== null) {
            result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
            if(!result) return false
        }

        // 5. Create seed record
        let seedInfo = {}
        let newSeedCode = await seedlotHelper.generateSeedCode()
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray)
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = newGermplasmDbId
        seedInfo['program_id'] = plotRecord.programDbId
        seedInfo['source_experiment_id'] = plotRecord.experimentDbId
        seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
        seedInfo['source_location_id'] = plotRecord.locationDbId
        seedInfo['source_plot_id'] = plotRecord.plotDbId
        seedInfo['source_entry_id'] = plotRecord.entryDbId
        seedInfo['cross_id'] = plotRecord.crossDbId
        seedInfo['harvest_source'] = harvestSource

        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        if (!result) return false
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 6. Create seed_relation record
        await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

        // 7. Create package record
        packageInfo = {}
        // Create package label
        let newPackageLabel = await seedlotHelper.packageLabelBuilder(entitiesArray)
        let newPackageCode = await seedlotHelper.generatePackageCode()
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = plotRecord.programDbId

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        if(!result) return false
        let newPackageDbId = result[0].id
    } catch (e) {
        return false
    }

    return true
}

/**
 * Create seed, package, and germplasm records
 * for bulk harvest of fixed materials
 * Currently suppports: Maize selfing
 * @param {object} record - object containing record information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestBulkFixedSelfingMaize(record, harvestData, userId, harvestSource, prefixes, optional){
    // populate cross records
    crossRecord = await populateCrossRecord(record,userId)
    let entitiesArray = {
        record: record,
        plot: record,
        harvestMode: record.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = crossRecord

    try{
        // 1. Check germplasm type. If double_haploid, perform DH process
        if(record.germplasmType == 'doubled_haploid') {
            result = await seedlotHelper.doubleHaploidProcess(record.germplasmDbId, entitiesArray, userId)
            if (!result) return false
        }

        // If number of ears is supplied, check if value is 0.
        // If 0, do not create new records. Return 'true'.
        if (
            (harvestData.no_of_ears !== null && harvestData.no_of_ears == 0)
            ||
            (harvestData.no_of_plants !== null && harvestData.no_of_plants == 0)
        ) return true

        // 2. Create Seed
        let seedInfo = {}
        let seedCodePrefix = prefixes.seedCodePrefix

        // 2.a generate seed name
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(record.experimentDbId)
        let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:additionalCondition})
        
        seedInfo["seed_name"] = newSeedName
        seedInfo["seed_code"] = newSeedCode
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['germplasm_id'] = record.germplasmDbId
        seedInfo['program_id'] = record.programDbId
        seedInfo['source_experiment_id'] = record.experimentDbId
        seedInfo['source_occurrence_id'] = record.occurrenceDbId
        seedInfo['source_location_id'] = record.locationDbId
        seedInfo['source_plot_id'] = record.plotDbId
        seedInfo['source_entry_id'] = record.entryDbId
        seedInfo['cross_id'] = record.crossDbId
        seedInfo['harvest_source'] = harvestSource

        // 2.b create seed
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
        
        if (!result) return false
        
        let newSeedDbId = result[0].id
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.c populate seed relation record
        await populateRelationTable('germplasm', 'seed', record.seedDbId, newSeedDbId, userId)

        // 3. Create package
        packageInfo = {}
        let packageCodePrefix = prefixes.packageCodePrefix

        // 3.a generate package label
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        entitiesArray = packageLabelObject.entitiesArray

        // 3.b create package record
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_status'] = 'active'
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = record.programDbId
        
        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
        
        if(!result) return false
        let newPackageDbId = result[0].id
    }
    catch(error){
        return false
    }
 
    return true
}

/**
 * Create seed and package records
 * for individual ear harvest of maize fixed materials
 * @param {object} record - object containing record information
 * @param {object} harvestData - object containing plot's harvest data
 * @param {integer} userId - user dentifier
 * @param {string} harvestSource 'plot' or 'cross'
 * @param {object} prefixes - contains preferred prefixes for seed and package codes
 * @param {object} optional - optional parameters for generic name builder
 */
async function harvestNumberedSelectionFixedSelfingMaize(record, harvestData, userId, harvestSource, prefixes, optional = {}) {
    // populate cross records
    crossRecord = await populateCrossRecord(record,userId)
    let entitiesArray = {
        record: record,
        plot: record,
        harvestMode: record.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = crossRecord

    let recordsToCreate = parseInt(harvestData.no_of_ears)

    try{
        for (let i = 1; i <= recordsToCreate; i++) {
            // 1. Check germplasm type. If double_haploid, perform DH process
            if(record.germplasmType == 'doubled_haploid') {
                result = await seedlotHelper.doubleHaploidProcess(record.germplasmDbId, entitiesArray, userId)
                if (!result) return false
            }

            // 2. Create Seed
            let seedInfo = {}
            let seedCodePrefix = prefixes.seedCodePrefix

            // 2.a generate seed name
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            // build seed name
            let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(record.experimentDbId)
            let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
            optional['additionalCondition'] = additionalCondition
            let newSeedName = await buildSeedName(entitiesArray, optional)
            
            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = record.germplasmDbId
            seedInfo['program_id'] = record.programDbId
            seedInfo['source_experiment_id'] = record.experimentDbId
            seedInfo['source_occurrence_id'] = record.occurrenceDbId
            seedInfo['source_location_id'] = record.locationDbId
            seedInfo['source_plot_id'] = record.plotDbId
            seedInfo['source_entry_id'] = record.entryDbId
            seedInfo['cross_id'] = record.crossDbId
            seedInfo['harvest_source'] = harvestSource

            // 2.b create seed
            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            
            if (!result) return false
            
            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 2.c populate seed relation record
            await populateRelationTable('germplasm', 'seed', record.seedDbId, newSeedDbId, userId)

            // 3. Create package
            packageInfo = {}
            let packageCodePrefix = prefixes.packageCodePrefix

            // 3.a generate package label
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
            let packageLabelObject = await buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel
            entitiesArray = packageLabelObject.entitiesArray

            // 3.b create package record
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = record.programDbId
            
            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            
            if(!result) return false
            let newPackageDbId = result[0].id
        }
    }
    catch(error){
        return false
    }
 
    return true
}

/**
 * Facilitate creation of harvest records for DH1 Individual Ear harvest method
 * @param {Object} record - object containing record information
 * @param {Object} harvestData - object containing plot's harvest data
 * @param {Integer} userId - user dentifier
 * @param {String} harvestSource - 'plot' or 'cross'
 * @param {Object} prefixes - contains preferred prefixes for seed and package codes
 * @param {Object} optional - optional parameters for generic name builder
 */
async function harvestDH1IndividualEarFixedSelfingMaize(record, harvestData, userId, harvestSource, prefixes, optional){
    let plotRecord = await populateCrossRecord(record,userId)

    let lastDelimiterInd = plotRecord.germplasmDesignation.lastIndexOf("@")
    let startingDesignation = plotRecord.germplasmDesignation.substring(0,lastDelimiterInd);
    
    let germplasmParents = await seedlotHelper.getGermplasmParents(plotRecord.germplasmDbId)

    plotRecord.germplasmDesignation = startingDesignation

    let recordsToCreate = parseInt(harvestData.no_of_ears)
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord
    
    try {
        for(let i=0; i < recordsToCreate; i++){
            // 1. Create germplasm record
            let newDesignation = await buildGermplasmDesignation(entitiesArray)
            let germplasmInfo = {}

            germplasmInfo["designation"] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo["parentage"] = plotRecord.germplasmParentage
            germplasmInfo["generation"] = 'DH'
            germplasmInfo["germplasm_state"] = 'fixed'
            germplasmInfo["germplasm_type"] = 'doubled_haploid'
            germplasmInfo["crop_id"] = plotRecord.cropDbId
            germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_PEDIGREE')

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)

            if (!result) return false

            let newGermplasmDbId = result[0].id

            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2. Create germplasm_name record
            let germplasmNameInfo = {}
            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)

            if (!result) return false

            let newGermplasmNameDbId = result[0].id

            // 3. Create germplasm_relation record
            await populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // 4. Add germplasm_attribute records
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "HETEROTIC_GROUP", userId)
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "GRAIN_COLOR", userId)
            await seedlotHelper.populateGermplasmAttribute(plotRecord.germplasmDbId, newGermplasmDbId, "GROWTH_HABIT", userId)

            await seedlotHelper.insertGermplasmAttribute(newGermplasmDbId, 'DH_FIRST_INCREASE_COMPLETED', 'false', userId)

            // Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // 5. Create seed record
            let seedInfo = {}
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)

            let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
            let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
            
            optional = optional === undefined ? [] : optional
            optional['additionalCondition'] = additionalCondition

            let newSeedName = await buildSeedName(entitiesArray, optional)

            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = plotRecord.programDbId
            seedInfo['source_experiment_id'] = plotRecord.experimentDbId
            seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
            seedInfo['source_location_id'] = plotRecord.locationDbId
            seedInfo['source_plot_id'] = plotRecord.plotDbId
            seedInfo['source_entry_id'] = plotRecord.entryDbId
            seedInfo['cross_id'] = plotRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)

            if (!result) return false

            let newSeedDbId = result[0].id

            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 6. Create seed_relation record
            await populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 7. Create package record
            let packageLabelObject = await buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel

            entitiesArray = packageLabelObject.entitiesArray
            packageInfo = {}

            let packageCodePrefix = prefixes.packageCodePrefix
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)

            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = plotRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)

            if(!result) return false

            let newPackageDbId = result[0].id
        }

        return true
    }
    catch(error){
        return false
    }
}

/**
 * Update cross harvest status to COMPLETED or FAILED
 * depending on the result. If result is 'true', harvest status is COMPLETED.
 * Else, harvest status is FAILED.
 * @param {integer} crossDbId cross identifier
 * @param {integer} userId user identifier
 * @param {boolean} result whether or not the harvest was successful
 */
async function updateCrossHarvestStatus(crossDbId, userId, result) {
    // Process result variable
    let success = true
    let errors = []
    if (typeof result === "object") {
        success = result.success
        errors = result.errors
    }
    else success = result


    let status = (success == false ? 'FAILED' : 'COMPLETED')
    // If errors array is not empty, append to status
    if (errors.length > 0) status += ":" + errors.join(";")
    let transaction

    try {
        transaction = await sequelize.transaction({ autocommit: false })
        // Update the cross record
        let updateCrossQuery = `
            UPDATE
                germplasm.cross
            SET
                harvest_status = '${status}',
                modification_timestamp = NOW(),
                modifier_id = ${userId}
            WHERE
                id = ${crossDbId}
        `

        await sequelize.query(updateCrossQuery, {
            type: sequelize.QueryTypes.UPDATE
        })
        await transaction.commit()
    } catch (err) {
        // Rollback transaction (if still open)
        if (transaction.finished !== 'commit') transaction.rollback()
    }
}

/**
 * Update plot harvest status to COMPLETED or FAILED
 * depending on the result. If result is 'true', harvest status is COMPLETED.
 * Else, harvest status is FAILED.
 * @param {integer} plotDbId plot identifier
 * @param {integer} userId user identifier
 * @param {boolean} result whether or not the harvest was successful
 */
async function updatePlotHarvestStatus(plotDbId, userId, result) {
    // Process result variable
    let success = true
    let errors = []
    if (typeof result === "object") {
        success = result.success
        errors = result.errors
    }
    else success = result


    let status = (success == false ? 'FAILED' : 'COMPLETED')
    // If errors array is not empty, append to status
    if (errors.length > 0) status += ":" + errors.join(";")
    let transaction

    try {
        transaction = await sequelize.transaction({ autocommit: false })
        // Update the plot record
        let updatePlotQuery = `
            UPDATE
                experiment.plot
            SET
                harvest_status = '${status}',
                modification_timestamp = NOW(),
                modifier_id = ${userId}
            WHERE
                id = ${plotDbId}
        `

        await sequelize.query(updatePlotQuery, {
            type: sequelize.QueryTypes.UPDATE
        })
        await transaction.commit()
    } catch (err) {
        // Rollback transaction (if still open)
        if (transaction.finished !== 'commit') transaction.rollback()
    }    
}

module.exports = {
    /**
     * Create seedlots
     * POST v3/plots/:id/seedlots
     * @param {*} req Request parameters
     * @param {*} res Response     
     */
    post: async function (req, res) {
        // Checks if there is a request body
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Get userId
        let userId = await tokenHelper.getUserId(req, res)

        // If userId is undefined, terminate
        if (userId === undefined) {
            return
        }
        // If userId is null, return 401: User not found error
        else if (userId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body
        let records = []
        let recordCount = 0

        if (data.harvestMode == undefined) {
            let errMsg = 'Required parameters are missing. Ensure that the harvestMode field is not empty.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (data.occurrenceDbId == undefined) {
            let errMsg = 'Required parameters are missing. Ensure that the occurrenceDbId field is not empty.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!validator.isInt(data.occurrenceDbId)) {
            let errMsg = 'Invalid request, occurrenceDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let occurrenceDbId = data.occurrenceDbId

        let harvestMode = data.harvestMode

        try {

            records = data.records
            recordCount = records.length

            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
        let transaction
        try {
            let insertCount = 0
            let drylandCrops = [
                'chickpea',
                'groundnut',
                'sorghum',
                'finger millet',
                'pearl millet',
                'pigeon pea'
            ]
            let codePrefixes = {
                'chickpea': 'C',
                'groundnut': 'G',
                'sorghum': 'S',
                'finger millet': 'F',
                'pearl millet': 'M',
                'pigeon pea': 'P'
            }

            for (let record of records) {

                if (record.dbId == undefined) {
                    let errMsg = 'Required parameters are missing. Ensure that the dbId field is not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                dbId = record.dbId

                if (!validator.isInt(dbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400038)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                } else {
                    let dbRecord
                    let harvestData
                    let schema
                    let table
                    let harvestSource = harvestMode.toLowerCase()
                    let harvestModeModified = harvestMode
                    if (harvestMode ==  'PLOT') { // retrieve plot information
                        schema = 'experiment'
                        table = 'plot'
                        let plotRecord = await seedlotHelper.getPlotInformation(dbId)

                        if (plotRecord.length == 0) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 404013)
                            res.send(new errors.NotFoundError(errMsg))
                            return
                        }

                        harvestData = await seedlotHelper.getPlotHarvestData(dbId)

                        dbRecord = plotRecord
                    }
                    else if (harvestMode ==  'CROSS') { // retrieve cross information
                        schema = 'germplasm'
                        table = 'cross'
                        let crossRecord = await seedlotHelper.getCrossInformation(dbId, occurrenceDbId)

                        if (crossRecord.length == 0) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 404013)
                            res.send(new errors.NotFoundError(errMsg))
                            return
                        }

                        harvestData = await seedlotHelper.getCrossHarvestData(dbId)

                        // if the cross method is selfing, harvest plot instead
                        if(crossRecord.crossMethod == 'selfing' && crossRecord.plotDbId != null) {
                            let plotRecord = await seedlotHelper.getPlotInformation(crossRecord.plotDbId, dbId)

                            if (plotRecord.length == 0) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 404013)
                                res.send(new errors.NotFoundError(errMsg))
                                return
                            }

                            plotRecord.harvestMethod = crossRecord.harvestMethod

                            harvestModeModified = 'PLOT'

                            dbRecord = plotRecord
                        }
                        else {
                            dbRecord = crossRecord
                        }
                    }

                    transaction = await sequelize.transaction({ autocommit: false })
                    // Update harvest status
                    let updateHarvestStatusQuery = `
                        UPDATE
                            ${schema}.${table}
                        SET
                            harvest_status = 'HARVEST_IN_PROGRESS',
                            modification_timestamp = NOW(),
                            modifier_id = ${userId}
                        WHERE
                            id = ${dbId}
                    `

                    await sequelize.query(updateHarvestStatusQuery, {
                        type: sequelize.QueryTypes.UPDATE
                    })
                    await transaction.commit()

                    let result = false
                    let harvestMethod = String(dbRecord.harvestMethod).trim().toLowerCase()
                    let cropName = String(dbRecord.cropName).trim().toLowerCase()
                    let germplasmState = String(dbRecord.germplasmState).trim().toLowerCase()
                    let germplasmType = String(dbRecord.germplasmType).trim().toLowerCase()
                    let crossMethod = String(dbRecord.crossMethod).trim().toLowerCase()
                    let stage = String(dbRecord.stage).trim().toLowerCase()
                    let generation = String(dbRecord.germplasmGeneration).trim().toLowerCase()
                    dbRecord.harvestMode = harvestModeModified;

                    let optional = {}
                    let prefixes = {}

                    // harvest use cases
                    if (harvestModeModified == 'PLOT') {
                        // RICE
                        if(cropName == 'rice') {
                            // If breeding stage is TCV,
                            // do not create new germplasm/seeds/packages.
                            // Simply transition harvest status to "DONE"
                            if(stage == 'tcv') {
                                if (germplasmState == 'not_fixed' && germplasmType == 'unknown') result = true
                                else if (germplasmState == 'fixed' && germplasmType == 'fixed_line'
                                    && harvestMethod == 'r-line harvest'
                                ) {
                                    result = await harvestRiceFixedRLineHarvest(dbRecord,harvestData,userId, harvestSource)
                                }
                            }
                            // If germplasm is transgenic or genome-edited, perform special case
                            else if(germplasmType == 'transgenic' || germplasmType == 'genome_edited') {
                                if(harvestMethod == 'bulk') {
                                    result = await harvestBulkTransgenicGenomeEditedRice(dbRecord, harvestData, userId, harvestSource)
                                } else if(harvestMethod == 'single plant selection') {
                                    result = await harvestSPSTransgenicGenomeEditedRice(dbRecord, harvestData, userId, harvestSource)
                                }
                            } else if(germplasmState == 'fixed') { // fixed germplasm state
                                if(harvestMethod == 'bulk' || harvestMethod == 'null') {
                                    result = await createSeedLotFixedBulk(dbRecord, harvestData, userId, harvestSource)
                                }
                                if(harvestMethod == 'single plant seed increase'){
                                    result = await harvestFixedSelfingSinglePlantSeedIncrease(dbRecord, harvestData, userId, harvestSource, optional)
                                }
                            } else if(germplasmState == 'not_fixed') { // not_fixed germplasm state
                                if(harvestMethod == 'bulk' || harvestMethod == 'single seed descent') {
                                    result = await createSeedLotWithTextPattern(dbRecord, harvestData, userId, harvestSource)
                                }
                                else if(harvestMethod == 'single plant selection' || harvestMethod == 'panicle selection') {
                                    if(harvestMethod == 'panicle selection') harvestData.no_of_plants = harvestData.panno_sel
                                    result = await createSeedlotForNumberedSelectionContinuous(dbRecord, harvestData, userId, harvestSource)
                                }
                                else if(harvestMethod == 'single plant selection and bulk') {
                                    dbRecord.harvestMethod = 'single plant selection'
                                    result = await createSeedlotForNumberedSelectionContinuous(dbRecord, harvestData, userId, harvestSource)

                                    if (result) {
                                        dbRecord.harvestMethod = 'bulk'
                                        result = await createSeedLotWithTextPattern(dbRecord, harvestData, userId, harvestSource)
                                    }
                                }
                                else if(harvestMethod == 'plant-specific') {
                                    result = await createSeedlotForNumberedSelectionSpecific(dbRecord, harvestData, userId, harvestSource)
                                }
                                else if(harvestMethod == 'plant-specific and bulk') {
                                    dbRecord.harvestMethod = 'plant-specific'
                                    result = await createSeedlotForNumberedSelectionSpecific(dbRecord, harvestData, userId, harvestSource)

                                    if (result) {
                                        dbRecord.harvestMethod = 'bulk'
                                        result = await createSeedLotWithTextPattern(dbRecord, harvestData, userId, harvestSource)
                                    }
                                }
                            }
                        }

                        // WHEAT
                        else if (cropName == 'wheat') {
                            prefixes.familyCodePrefix = 'WFM'
                            prefixes.germplasmCodePrefix = 'WGE'
                            prefixes.seedCodePrefix = 'WSD'
                            prefixes.packageCodePrefix = 'WPK'

                            if (germplasmState == 'fixed') {
                                if (['bulk', 'selected bulk'].includes(harvestMethod)) {
                                    result = await harvestBulkFixedSelfing(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }
                                else if (['individual spike', 'single plant selection'].includes(harvestMethod)) {
                                    result = await harvestNumberedSelectionFixedSelfing(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }
                            } else if (germplasmState == 'not_fixed') {
                                if(harvestMethod == 'bulk') {
                                    result = await harvestBulkNotFixedSelfing(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                } else if (harvestMethod == 'selected bulk') {
                                    optional = {
                                        additionalCondition: (
                                            harvestData.no_of_plants == null ?
                                            'without_no_of_plant' :
                                            'with_no_of_plant'
                                        ),
                                        counter: harvestData.no_of_plants
                                    }
                                    result = await harvestBulkNotFixedSelfing(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                } else if (harvestMethod == 'individual spike' || harvestMethod == 'single plant selection') {
                                    result = await harvestNumberedSelectionSelfingWheat(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                } else if(harvestMethod == 'single plant selection and bulk'){
                                    //  process harvest for single plant selection
                                    dbRecord.harvestMethod = 'single plant selection'
                                    result = await harvestNumberedSelectionSelfingWheat(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                    if (result) {
                                        // process harvest for bulk
                                        dbRecord.harvestMethod = 'bulk'
                                        result = await harvestBulkNotFixedSelfing(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                    }

                                }
                            }
                        }

                        // MAIZE
                        else if (cropName == 'maize') {
                            prefixes.familyCodePrefix = 'MFM'
                            prefixes.germplasmCodePrefix = 'MGE'
                            prefixes.seedCodePrefix = 'MSD'
                            prefixes.packageCodePrefix = 'MPK'

                            if (germplasmState == 'fixed') {
                                // add fixed use case here
                                if(harvestMethod == 'bulk'){
                                    result = await harvestBulkFixedSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                }
                                if(harvestMethod == 'dh1 individual ear'){
                                    result = await harvestDH1IndividualEarFixedSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }
                                if(harvestMethod == 'individual ear') {
                                    result = await harvestNumberedSelectionFixedSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }
                                
                            } else if (germplasmState == 'not_fixed') {
                                if(harvestMethod == 'bulk') {
                                    result = await harvestBulkNotFixedSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }
                                if(harvestMethod == 'individual ear') {
                                    result = await harvestNumberedSelectionSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }
                                if(harvestMethod == 'maintain and bulk') {
                                    result = await harvestMaintainAndBulkNotFixedSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }
                            }
                        }

                        // COWPEA and SOYBEAN
                        else if (cropName == 'cowpea' || cropName == 'soybean'){
                            // For Fixed germplasm_state
                            if (germplasmState == 'fixed') {
                                if(harvestMethod == 'bulk') {
                                    // reuse harvest bulk for fixed materials
                                    result = await harvestBulkFixedSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                }
                            } else if (germplasmState == 'not_fixed') {
                                if(harvestMethod == 'bulk') {
                                    if (generation.match(/f1$|f3$|f4$|f5$|f6$/g)) {
                                        // reuse bulk for not_fixed
                                        result = await harvestBulkNotFixedSelfingCowpea(dbRecord, harvestData, userId, harvestSource, prefixes)
                                    }
                                }
                                else if(harvestMethod == 'single plant selection') {
                                    if (generation.match(/f2$|f3$/g)) {
                                        // single plant selection
                                        result = await cowpeaHarvestHelper.harvestSinglePlantSelectionNotFixedCowPea(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                        }
                                }
                            }
                        }

                        // DRYLAND CROPS
                        else if (drylandCrops.includes(cropName)) {
                            prefixes.familyCodePrefix = codePrefixes[cropName] + 'FM'
                            prefixes.germplasmCodePrefix = codePrefixes[cropName] + 'GE'
                            prefixes.seedCodePrefix = codePrefixes[cropName] + 'SD'
                            prefixes.packageCodePrefix = codePrefixes[cropName] + 'PK'

                            if (germplasmState == 'fixed') {
                                if (harvestMethod == 'bulk') {
                                    // reuse harvest bulk for fixed materials
                                    result = await harvestBulkFixedSelfingMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                }
                            }
                            else if (germplasmState == 'not_fixed') {
                                if (harvestMethod == 'bulk') {
                                    result = await harvestBulkNotFixedSelfingDCP(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (harvestMethod == 'single plant') {
                                    result = await harvestSinglePlantNotFixedSelfingDCP(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                }                        
                            }
                        }
                    }
                    else if (harvestModeModified == 'CROSS') {
                        // RICE
                        if(cropName == 'rice') {
                            prefixes.seedCodePrefix = 'SEED'

                            if(harvestMethod == 'bulk') {
                                if (crossMethod == 'single cross'
                                    || crossMethod == 'double cross'
                                    || crossMethod == 'three-way cross'
                                    || crossMethod == 'complex cross'
                                ) {
                                    result = await harvestSingleCrossBulkRice(dbRecord, harvestData, userId, harvestSource, prefixes, optional)
                                } else if (crossMethod == 'backcross') {
                                    result = await harvestBackcrossBulkRice(dbRecord, harvestData, userId, harvestSource,prefixes)
                                }
                            } else if(harvestMethod == 'single seed numbering') {
                                if (crossMethod == 'single cross') {
                                    result = await harvestSingleCrossSingleSeedNumberingRice(dbRecord, harvestData, userId, harvestSource)
                                } else if (crossMethod == 'backcross') {
                                    result = await harvestBackcrossSingleSeedNumberingRice(dbRecord, harvestData, userId, harvestSource)
                                } else if (crossMethod == 'transgenesis' || crossMethod == 'genome editing') {
                                    result = await harvestTransgenesisGenomeEditiongSSNRice(dbRecord, harvestData, userId, harvestSource)
                                }
                            } else if(harvestMethod == 'h-harvest' && crossMethod == 'hybrid formation') {
                                result = await harvestHybridFormationHHarvestRice(dbRecord, harvestData, userId, harvestSource)
                            } else if(harvestMethod == 'a-line harvest' && crossMethod == 'cms multiplication') {
                                result = await harvestCMSMultALineRice(dbRecord, harvestData, userId, harvestSource)
                            } else if(harvestMethod == 'test-cross harvest' && crossMethod == 'test cross') {
                                result = await harvestTestCrossRice(dbRecord, harvestData, userId, harvestSource)
                            }
                        }

                        // MAIZE
                        else if (cropName == 'maize') {
                            prefixes.familyCodePrefix = 'MFM'
                            prefixes.germplasmCodePrefix = 'MGE'
                            prefixes.seedCodePrefix = 'MSD'
                            prefixes.packageCodePrefix = 'MPK'

                            if (harvestMethod == 'bulk') {
                                if (crossMethod == 'single cross') {
                                    result = await harvestSingleCrossBulkMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'hybrid formation'){
                                    result = await harvestHybridFormationBulkMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'backcross') {
                                    result = await harvestBackcrossBulkMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'three-way cross') {
                                    result = await harvestThreeWayCrossBulkMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'double cross') {
                                    result = await harvestDoubleCrossBulkMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'maternal haploid induction') {
                                    result = await harvestMaternalHaploidInductionBulkMaize(dbRecord, harvestData, userId, harvestSource, prefixes)
                                }
                            } 
                        }

                        // WHEAT
                        else if (cropName == 'wheat') {
                            prefixes.familyCodePrefix = 'WFM'
                            prefixes.germplasmCodePrefix = 'WGE'
                            prefixes.seedCodePrefix = 'WSD'
                            prefixes.packageCodePrefix = 'WPK'
                            if (harvestMethod == 'bulk') {
                                if (crossMethod == 'single cross') {
                                    result = await harvestSingleCrossBulkWheat(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'top cross') {
                                    result = await harvestTopCrossBulkWheat(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'backcross') {
                                    result = await harvestBackcrossBulkWheat(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'double cross') {
                                    result = await harvestDoubleCrossBulkWheat(dbRecord, harvestData, userId, harvestSource, prefixes)
                                } else if (crossMethod == 'hybrid formation'){
                                    result = await harvestHybridFormationBulkWheat(dbRecord, harvestData, userId, harvestSource, prefixes)
                                }
                            }
                        }

                        // COWPEA
                        else if (cropName == 'cowpea' || cropName == 'soybean') {
                            if (harvestMethod == 'bulk') {
                                if (crossMethod == 'bi-parental cross') {
                                    result = await cowpeaHarvestHelper.harvestBulkBiParentalCrossCowPea(dbRecord, harvestData, userId, harvestSource, prefixes);
                                }
                                else if (crossMethod == 'backcross') {
                                    result = await cowpeaHarvestHelper.harvestBulkBackcrossCowpea(dbRecord, harvestData, userId, harvestSource, prefixes)
                                }
                            }
                        }

                        // DRYLAND CROPS
                        else if(drylandCrops.includes(cropName)) {
                            prefixes.familyCodePrefix = codePrefixes[cropName] + 'FM'
                            prefixes.germplasmCodePrefix = codePrefixes[cropName] + 'GE'
                            prefixes.seedCodePrefix = codePrefixes[cropName] + 'SD'
                            prefixes.packageCodePrefix = codePrefixes[cropName] + 'PK'

                            if (harvestMethod == 'bulk') {
                                if (crossMethod == 'single cross') {
                                    result = await harvestSingleCrossBulkDCP(dbRecord, harvestData, userId, harvestSource, prefixes)
                                }
                            }
                        }
                    }

                    // If the result is an object and has a "success" property, evaluate said property.
                    if (typeof result === 'object' && result.success !== undefined) {
                        // If result.success is TRUE, add one to insertCount to signify successful harvest
                        if (result.success) insertCount ++
                    }
                    // Else, evaluate result as boolean.
                    // If TRUE, add one to insertCount to signify successful harvest
                    else if (result) {
                        insertCount++
                    }

                    if (harvestMode == 'PLOT') {
                        await updatePlotHarvestStatus(dbId, userId, result)
                    }
                    else if (harvestMode == 'CROSS') {
                        await updateCrossHarvestStatus(dbId, userId, result)
                    }
                }

            }

            let resultArray = {
                insertCount: insertCount
            }
            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004) 
                + `(Caught error: ${err})` 
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
