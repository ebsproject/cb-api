/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder.js')
let userValidator = require('../../../helpers/person/validator.js')
let validator = require('validator')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../helpers/logger/index.js')
const endpoint = 'breeding-portal-data'

module.exports = {
    // Endpoint for creating new breeding portal data record
    // POST /v3/breeding-portal-data
    post: async function (req, res, next) {
        let resultArray = []
        let breedingPortalDataDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)
        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let bpDataUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/breeding-portal-data'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        records = data.records
        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let bpDataValuesArray = []
            for (const record of records) {
                // Set defaults
                let entityName = null
                let entityValue = null
                let transformedData = null
                let description = null
                let notes = null

                // Check if required columns are in the request body
                if (
                    !record.entityName || !record.entityValue
                ) {
                    let errMsg = `Required parameters are missing. Ensure that entityName and entityValue fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                entityName = record.entityName

                // Check if entityName does not exist yet
                let entityNameQuery = `
                    SELECT count(1)
                    FROM tenant.breeding_portal_data
                    WHERE LOWER(entity_name) = '${entityName.toLowerCase()}'
                `

                let entityNames = await sequelize.query(entityNameQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (entityNames == null) {
                    return
                }

                if (entityNames[0] != null && entityNames[0].count != null && entityNames[0].count > 0) {
                    let errMsg = `The entityName '${entityName}' already exists.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (record.entityValue !== undefined) {
                    entityValue = record.entityValue
                    if (!validator.isJSON(entityValue)) {
                        let errMsg =
                            "Invalid format for entityValue. It must be a valid JSON string."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.transformedData !== undefined) {
                    transformedData = record.transformedData
                    if (!validator.isJSON(transformedData)) {
                        let errMsg =
                            "Invalid format for transformedData. It must be a valid JSON string."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                description = (record.description !== undefined) ? record.description : null
                notes = (record.notes !== undefined) ? record.notes : null

                // get values
                let tempArray = [
                    entityName, entityValue, transformedData, description, notes, personDbId
                ]
                bpDataValuesArray.push(tempArray)
            }

            // Create bpData record
            let bpDataQuery = format(`
                INSERT INTO
                    tenant.breeding_portal_data (
                        entity_name, entity_value, transformed_data, description, notes, creator_id
                    )
                VALUES
                    %L
                RETURNING id`, bpDataValuesArray
            )

            const bpData = await sequelize.transaction(async transaction => {
                return await sequelize.query(bpDataQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (const bpd of bpData[0]) {
                breedingPortalDataDbId = bpd.id
                let bpdRecord = {
                    breedingPortalDataDbId: breedingPortalDataDbId,
                    recordCount: 1,
                    href: bpDataUrlString + '/' + breedingPortalDataDbId
                }
                resultArray.push(bpdRecord)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}