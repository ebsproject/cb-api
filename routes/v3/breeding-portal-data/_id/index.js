/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let logger = require('../../../../helpers/logger')
const endpoint = 'breeding-portal-data/:id'

module.exports = {
    // Implementation of PUT call for /v3/breeding-portal-data/:id
    put: async function (req, res, next) {
        let breedingPortalDataDbId = req.params.id
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!validator.isInt(breedingPortalDataDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400063)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let entityName = null
        let entityValue = null
        let transformedData = null
        let description = null
        let notes = null

        // Build query
        let breedingPortalDataQuery = `
            SELECT
                bp_data.id AS "breedingPortalDataDbId",
                bp_data.entity_name AS "entityName",
                bp_data.entity_value AS "entityValue",
                bp_data.transformed_data AS "transformedData",
                bp_data.description,
                bp_data.notes
            FROM
                tenant.breeding_portal_data bp_data
            WHERE
                is_void = FALSE AND
                id = ${breedingPortalDataDbId}
        `

        // Retrieve breeding portal data record from database
        let breedingPortalData = await sequelize.query(breedingPortalDataQuery,{
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (
            await breedingPortalData == undefined ||
            await breedingPortalData.length < 1
        ) {
            let errMsg = `The breeding portal data record you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordEntityName = breedingPortalData[0].entityName
        let recordEntityValue = breedingPortalData[0].entityValue
        let recordTransformedData = breedingPortalData[0].transformedData
        let recordDescription = breedingPortalData[0].description
        let recordNotes = breedingPortalData[0].notes

        let isSecure = forwarded(req, req.header).secure

        // Set URL for response
        let breedingPortalDataUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/breeding-portal-data/'
            + breedingPortalDataDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let transaction

        try {

            let setQuery = ``

            if (data.entityName !== undefined) {
                entityName = data.entityName

                // Check if user input is same with the current value in the database
                if (entityName != recordEntityName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    entity_name = $$${entityName}$$
                    `
                }
            }

            if (data.entityValue !== undefined) {
                entityValue = data.entityValue

                if (!validator.isJSON(entityValue)) {
                    let errMsg =
                        "Invalid format for entityValue. It must be a valid JSON string."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (entityValue != recordEntityValue) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    entity_value = $$${entityValue}$$
                    `
                }
            }

            if (data.transformedData !== undefined) {
                transformedData = data.transformedData

                if (!validator.isJSON(transformedData)) {
                    let errMsg =
                        "Invalid format for transformedData. It must be a valid JSON string."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (transformedData != recordTransformedData) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    transformed_data = $$${transformedData}$$
                    `
                }
            }

            if (data.description !== undefined) {
                description = data.description

                // Check if user input is same with the current value in the database
                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    description = $$${description}$$
                    `
                }
            }

            if (data.notes !== undefined) {
                notes = data.notes

                // Check if user input is same with the current value in the database
                if (notes != recordNotes) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                    notes = $$${notes}$$
                    `
                }
            }

            if(setQuery.length > 0) setQuery += `,`

            let resultArray
            transaction = await sequelize.transaction(async transaction => {
                let updateBreedingPortalDataQuery = `
                    UPDATE
                        tenant.breeding_portal_data
                    SET
                        ${setQuery}
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE
                        id = ${breedingPortalDataDbId}
                `

                // Return transaction info
                resultArray = {
                    breedingPortalDataDbId: breedingPortalDataDbId,
                    recordCount: 1,
                    href: breedingPortalDataUrlString
                }

                await sequelize.query(updateBreedingPortalDataQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}