/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let test = require('../../../helpers/test')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    // Implementation of POST call for /v3/persons-search
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let params = req.query
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set columns to be excluded in parameters for filtering
            let excludedParametersArray = ['fields']
            // Get filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let personsQuery = null

        let selectClause = null
        let whereClause = null
        let orderByClause = null

        // Check if the client specified values for the fields
        if (parameters['fields'] != null) {
            personsQuery = knex.column(parameters['fields'].split('|'))
            personsQuery += `
                FROM
                    tenant.person person
                LEFT JOIN
                    tenant.person creator ON creator.id = person.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = person.modifier_id
                WHERE
                    person.is_void = FALSE
                ` + addedConditionString + `
                ORDER BY
                    person.id
            `
        } else {
            selectClause = `
                SELECT 
                    person.id AS "personDbId",
                    person.email,
                    person.username,
                    person.first_name AS "firstName",
                    person.last_name AS "lastName",
                    person.person_name AS "personName",
                    person.person_type AS "personType",
                    person.person_status AS "personStatus",
                    person.person_role_id AS "personRoleId",
                    person.is_active AS "isActive",
                    person.person_document AS "personDocument",
                    person.creation_timestamp AS "creationTimestamp",
                    person.creator_id AS "creatorDbId",
                    creator.person_name AS "creator",
                    person.modification_timestamp AS "modificationTimestamp",
                    person.modifier_id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    tenant.person person
                LEFT JOIN 
                    tenant.person creator ON creator.id = person.creator_id
                LEFT JOIN 
                    tenant.person modifier ON modifier.id = person.modifier_id
            `

            // Check whether API caller needs both active and inactive persons
            if (params.forDataBrowser !== undefined || conditionString.includes('isActive')) {
                whereClause = `
                    WHERE 
                        person.is_void = FALSE
                `
            } else {
                whereClause = `
                    WHERE 
                        person.is_void = FALSE AND
                        person.is_active = TRUE
                `
            }

            orderByClause = `
                ORDER BY
                    person.id
            `

            personsQuery = selectClause + whereClause + orderByClause
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Generate the final SQL query
        personsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            personsQuery,
            conditionString,
            orderString
        )
        
        persons = await test.retrieveData(personsFinalSqlQuery, limit, offset)

        if (await persons == undefined || await persons.length < 1) {
            persons = []
            count = 0
        } else {
            // Get the final count of the person records retrieved
            let personsCountFinalSqlQuery = await processQueryHelper
                .getCountFinalSqlQuery(
                    personsQuery,
                    conditionString,
                    orderString
                )
                
            personsCount = await test.retrieveData(personsCountFinalSqlQuery)

            count = personsCount[0].count
        }
        
        res.send(200, {
            rows: persons,
            count: count
        })
        return
    }
}