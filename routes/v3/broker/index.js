/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let errors = require('restify-errors')
let forwarded = require('forwarded-for')
let logger = require('../../../helpers/logger')
let tokenHelper = require('../../../helpers/auth/token')
let errorBuilder = require('../../../helpers/error-builder')
let brokerHelper = require('../../../helpers/broker/index.js')
let entryHelper = require('../../../helpers/entries/index.js')
let plotHelper = require('../../../helpers/plots/index.js')

module.exports = {
  
    // POST /v3/broker
    post: async function (req, res, next) {
        let hostRequest = req.headers.host
        let source = req.query.source ?? 'CB'
        let token = req.headers.authorization
        let httpMethod = null
        let endpoint = null
        let dbIds = null
        let requestData = null
        let allowedHttpMethods = ["DELETE", "PUT"]
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(hostRequest, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        if (!req.body) {
            let errMsg = await errorBuilder.getError(hostRequest, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (
            req.body.httpMethod === undefined ||
            req.body.endpoint === undefined ||
            req.body.dbIds === undefined
        ) {
            let errMsg = 'Required parameters are missing. Ensure that the httpMethod, endpoint, and dbIds fields are not empty.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (req.body.httpMethod != undefined) {
            httpMethod = req.body.httpMethod  
            httpMethod = httpMethod.toUpperCase()

            if (!allowedHttpMethods.includes(httpMethod)) {
                let errMsg = 'You have provided an invalid HTTP method.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        endpoint = (req.body.endpoint != undefined) ? req.body.endpoint : null
        dbIds = (req.body.dbIds != undefined) ? req.body.dbIds : null

        let dbIdsArray = dbIds.split('|').map(item => item.trim())

        let isSecure = forwarded(req, req.headers).secure

        // Set URL for getData
        let urlString = (isSecure ? `https` : `http`)
            + `://`
            + hostRequest
            + `/${endpoint}`
        
        let rejectedData = []
        let recordData = []
        let resultArray = {}

        // For deletion of entities that needs to be reordered after (entries, list members, plots)
        let entryListDbIdArray = []
        let listDbIdArray = []
        let occurrenceDbIdArray = []
    
        // for reordering entries and plots
        let reorderString = null
        let orderNumberArray = []

        if (httpMethod === "PUT") {
            requestData = req.body.requestData

            if (requestData === undefined) {
                let errMsg = 'Invalid request, ensure that the requestData field is not empty.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (typeof(requestData) != 'object') {
                let errMsg = 'Invalid format, ensure that the requestData field is an object.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (endpoint.includes('entries') || endpoint.includes('plots')) {
                if (requestData.reorder != undefined || requestData.reorder != null) {
                    reorderString = requestData.reorder.split(':')
                    orderNumberArray = reorderString[1].split('|').map(item => item.trim())
                }
            }

        }
        // for reordering (bulk update): check if # of IDs == # of orderNumber
        if ((orderNumberArray.length > 0) && (orderNumberArray.length != dbIdsArray.length)) {
            let errMsg = `${reorderString[0]}s are not equal to number of records being reordered.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        for (let index in dbIdsArray) {

            let id = dbIdsArray[index]
            let finalUrl = `${urlString}/${id}`

            if (isNaN(id)) continue

            // for reordering (bulk update)
            if (reorderString != null || reorderString != undefined) {
                requestData[`${reorderString[0]}`] = orderNumberArray[index]
            }

            try { 
                let data = await brokerHelper
                    .getData(httpMethod, finalUrl, token, JSON.stringify(requestData), source.toUpperCase())
                if (data.status != 200 && data.id != null) {
                    rejectedData.push(data)
                } else {
                    recordData.push(data)
                }

                if (httpMethod == "DELETE") {
                    // Calls entry reorder function after the last entry record is deleted
                    if (finalUrl.includes('entries')) {
                        if (!entryListDbIdArray.includes(data.response[0].entryListDbId)) {
                            entryListDbIdArray.push(data.response[0].entryListDbId)
                        }
    
                        if (index == (dbIdsArray.length)-1) {
                            for (var entryListDbId of entryListDbIdArray) {
                                await entryHelper.reorder(entryListDbId, ``, personDbId)
                            }
                        }
                    }

                    // Calls plot reorder function after the last plot record is deleted
                    if (finalUrl.includes('plots')) {
                        if (!occurrenceDbIdArray.includes(data.response[0].occurrenceDbId)) {
                            occurrenceDbIdArray.push(data.response[0].occurrenceDbId)
                        }
    
                        if (index == (dbIdsArray.length)-1) {
                            for (var occurrenceDbId of occurrenceDbIdArray) {

                                // Checks if entry list is not empty
                                let plotsSearchUrl = (isSecure ? 'https' : 'http')
                                    + '://'
                                    + hostRequest
                                    + `/v3/plots-search`

                                let searchRequest = { "occurrenceDbId": `equals ${occurrenceDbId}` }
                                let plotsSearch = await brokerHelper.getData("POST", plotsSearchUrl, token, JSON.stringify(searchRequest), hostRequest)
                                let plotsCount = plotsSearch.response.length

                                // Reorder list members if list is not empty
                                if (plotsCount > 0) {
                                    await plotHelper.reorder(occurrenceDbId, ``, personDbId)
                                }
                            }
                        }
                    }

                    // Execute POST /v3/lists/:id/reorder-list-members after last list member record is deleted
                    if (finalUrl.includes('list-members')) {
                        if (!listDbIdArray.includes(data.response[0].listDbId)) {
                            listDbIdArray.push(data.response[0].listDbId)
                        }
    
                        if (index == (dbIdsArray.length)-1) {
                            for (var listDbId of listDbIdArray) {

                                // Checks if list is not empty
                                let listMembersSearchUrl = (isSecure ? 'https' : 'http')
                                    + '://'
                                    + hostRequest
                                    + `/v3/lists/${listDbId}/members-search`
                                let listMembersSearch = await brokerHelper.getData("POST", listMembersSearchUrl, token, null, hostRequest)
                                let listMembersCount = listMembersSearch.response.length

                                // Reorder list members if list is not empty
                                if (listMembersCount > 0) {
                                    let reorderListUrl = (isSecure ? 'https' : 'http')
                                        + '://'
                                        + hostRequest
                                        + `/v3/lists/${listDbId}/reorder-list-members`

                                    await brokerHelper.getData("POST", reorderListUrl, token, null, hostRequest)
                                }
                            }
                        }
                    }
                }
            } catch (err) {
                logger.logMessage(__filename, err, 'error')
                let errMsg = await errorBuilder.getError(hostRequest, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }
        }

        resultArray = {
            successful: recordData,
            failed: rejectedData
        }
        res.send(200, {
            rows: resultArray,
            count: recordData.length
        })
        return
    }
}