/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    // Endpoint for adding a protocol record
    post: async function (req, res, next) {
        let resultArray = []
        // Retrieve the ID of the client via the access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
        
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let protocolUrlString = (isSecure ? 'https' : 'http')
        + "://"
        + req.headers.host
        + "/v3/protocols"

        // Check for parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body
        let records = []

        try {
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Start transaction
        let transaction
        
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let protocolValuesArray = []
            let visitedArray = []

            for (var record of records) {
                let protocolCode = null
                let protocolName = null
                let protocolType = null
                let description = ''
                let programDbId = null

                let validateQuery = ``
                let validateCount = 0

                // Check if the required columns are in the input
                if (
                    record.protocolCode == undefined||
                    record.protocolName == undefined||
                    record.protocolType == undefined||
                    record.programDbId == undefined
                ) {

                    let errMsg = 'Invalid request. Required parameters protocolCode, protocolName, protocolType, programDbId are missing.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate and set values
                if (record.protocolCode != undefined) {
                    protocolCode = record.protocolCode
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        -- Check if protocol code is existing, return 1
                        SELECT
                            CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE 0
                            END AS count
                        FROM
                            tenant.protocol p
                        WHERE
                            p.is_void = FALSE AND
                            p.protocol_code = '${protocolCode}'
                        )
                    `

                    validateCount += 1
                }

                if (record.programDbId != undefined) {
                    programDbId = record.programDbId
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        -- Check if program ID is existing, return 1
                        SELECT
                            count(1)
                        FROM
                            tenant.program program
                        WHERE
                            program.is_void = FALSE AND
                            program.id = ${programDbId}
                        )
                    `

                    validateCount += 1
                }

                protocolName = record.protocolName
                protocolType = record.protocolType

                description = (record.description != undefined) ? record.description : null
                
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError('checkInputQuery: ' + checkInputQuery))
                    return
                }

                // Check for duplicates if the record count > 1
                if (recordCount > 1) {
                    if (visitedArray.includes(protocolCode)) {
                        let errMsg = 'Invalid request. Duplicate protocolCode values were found.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else {
                        visitedArray.push(protocolCode)
                    }
                }
                // Get values
                let tempArray = [
                    protocolCode, protocolName, protocolType, description, programDbId, userDbId
                ]

                protocolValuesArray.push(tempArray)
            }

            // Create protocol record
            let protocolQuery = format(`
                INSERT INTO tenant.protocol (
                    protocol_code,
                    protocol_name,
                    protocol_type,
                    description,
                    program_id,
                    creator_id
                )
                VALUES 
                    %L
                RETURNING 
                    id`, protocolValuesArray
            )

            let protocols = await sequelize.query(protocolQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            for (protocol of protocols[0]) {
                protocolDbId = protocol.id

                // Return the protocol info
                let protocolObj = {
                    protocolDbId: protocolDbId,
                    recordCount: 1,
                    href: protocolUrlString + '/' + protocolDbId
                }
                
                resultArray.push(protocolObj)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError('err: ' + err))
            return
        }
    }
}