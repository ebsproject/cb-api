/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for updating an existing protocol record
    // PUT /v3/protocols/:id
    put: async function (req, res, next) {

        // Retrieve protocol ID
        let protocolDbId = req.params.id

        if(!validator.isInt(protocolDbId)) {
            let errMsg = `Invalid format, protocol ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let protocolCode = null
        let protocolName = null
        let protocolType = null
        let description = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if protocol is existing
        // Build query
        let protocolQuery = `
            SELECT
                protocol.protocol_code AS "protocolCode",
                protocol.protocol_name AS "protocolName",
                protocol.protocol_type AS "protocolType",
                protocol.description,
                program.id AS "programDbId",
                creator.id AS "creatorDbId"
            FROM
                tenant.protocol protocol
            LEFT JOIN
                tenant.program program ON program.id = protocol.program_id
            LEFT JOIN
                tenant.person creator ON creator.id = protocol.creator_id
            WHERE
                protocol.is_void = FALSE AND
                protocol.id = ${protocolDbId}
        `

        // Retrieve protocol from the database
        let protocol = await sequelize.query(protocolQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await protocol == undefined || await protocol.length < 1) {
            let errMsg = `The protocol you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordProtocolCode = protocol[0].protocolCode
        let recordProtocolName = protocol[0].protocolName
        let recordProtocolType = protocol[0].protocolType
        let recordDescription = protocol[0].description
        let recordProgramDbId = protocol[0].programDbId
        let recordCreatorDbId = protocol[0].creatorDbId

        // Check if user is an admin or an owner of the cross
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                WHERE 
                    program.id = ${recordProgramDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let protocolUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/protocols/"
            + protocolDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body    

        // Start transaction
        let transaction

        try {
            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/

            if (data.protocolCode !== undefined) {
                protocolCode = data.protocolCode

                // Check if user input is same with the current value in the database
                if (protocolCode != recordProtocolCode) {

                    // Validate protocol code
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if protocol code is non-existing return 1
                        SELECT 
                            CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                            END AS count
                        FROM 
                            tenant.protocol protocol
                        WHERE 
                            protocol.is_void = FALSE AND
                            protocol.protocol_code = $$${protocolCode}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        protocol_code = $$${protocolCode}$$
                    `
                }
            }
            
            /** Set values to other parameters **/

            if (data.protocolName !== undefined) {
                protocolName = data.protocolName

                if (protocolName != recordProtocolName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        protocol_name = $$${protocolName}$$
                    `
                }
            }

            if (data.protocolType !== undefined) {
                protocolType = data.protocolType

                if (protocolType != recordProtocolType) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        protocol_type = $$${protocolType}$$
                    `
                }
            }

            if (data.description !== undefined) {
                description = data.description

                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        description = $$${description}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the protocol record
            let updateProtocolQuery = `
                UPDATE
                    tenant.protocol
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${protocolDbId}
            `
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updateProtocolQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let resultArray = {
                protocolDbId: protocolDbId,
                recordCount: 1,
                href: protocolUrlString
            }
            
            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing protocol record
    // DELETE /v3/protocols/:id
    delete: async function (req, res, next) {

        // Retrieve protocol ID
        let protocolDbId = req.params.id

        if(!validator.isInt(protocolDbId)) {
            let errMsg = `Invalid format, protocol ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)
    
        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let protocolQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    program.id AS "programDbId"
                FROM
                    tenant.protocol protocol
                LEFT JOIN
                    tenant.program program ON program.id = protocol.program_id
                LEFT JOIN
                    tenant.person creator ON creator.id = protocol.creator_id
                WHERE
                    protocol.is_void = FALSE AND
                    protocol.id = ${protocolDbId}
            `

            // Retrieve protocol from the database
            let protocol = await sequelize.query(protocolQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await protocol == undefined || await protocol.length < 1) {
                let errMsg = `The protocol you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = protocol[0].creatorDbId
            let recordProgramDbId = protocol[0].programDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    WHERE 
                        program.id = ${recordProgramDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteProtocolQuery = format(`
                UPDATE
                    tenant.protocol
                SET
                    is_void = TRUE,
                    protocol_code = CONCAT('VOIDED-', $$${protocolDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${protocolDbId}
            `)

            await sequelize.query(deleteProtocolQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { protocolDbId: protocolDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }

    }
}