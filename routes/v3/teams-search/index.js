/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
// let Sequelize = require('sequelize')
// let Op = Sequelize.Op
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async (req, res, next) => {
      let limit = req.paginate.limit
      let offset = req.paginate.offset
      let sort = req.query.sort
      let count = 0
      let orderString = ''
      let conditionString = ''
      let addedConditionString = ''
      let parameters = {}

      if (req.body != undefined) {
        if (req.body.fields != null) {
          parameters['fields'] = req.body.fields
        }

        // Set columns to be excluded in parameters for filtering
        let excludedParametersArray = ['fields']

        // Get filter condition
        conditionString = await processQueryHelper.getFilter(req.body, excludedParametersArray)

        if (conditionString.includes('invalid')) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400022)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
      }

      // Build the base retrieval query
      let teamsQuery = null

      // Check if the client specified values for the fields
      if (parameters['fields'] != null) {
        teamsQuery = knex.column(parameters['fields'].split('|'))
        teamsQuery += `
        FROM
          tenant.team team
        LEFT JOIN
          tenant.team_member member ON member.team_id = team.id
          AND member.is_void = FALSE
        LEFT JOIN
          tenant.person person ON person.id = member.person_id
        LEFT JOIN
          tenant.person creator ON creator.id = team.creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = team.modifier_id
        WHERE
          team.is_void = FALSE
            ` + addedConditionString +`
          ORDER BY
            team.id
        `
      } else {
        teamsQuery = `
        SELECT
          team.id AS "teamDbId",
          team.team_code AS "teamCode",
          team.team_name AS "teamName",
          team.description,
          team.creator_id AS "creatorDbId",
          creator.person_name AS "creator",
          team.modification_timestamp AS "modificationTimestamp",
          team.modifier_id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          tenant.team team
        LEFT JOIN
          tenant.person creator ON creator.id = team.creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = team.modifier_id
        WHERE
          team.is_void = FALSE
            ` + addedConditionString +`
          ORDER BY
            team.id
        `
      }

      // Parse the sort parameters 
      if (sort != null) {
        orderString = await processQueryHelper.getOrderString(sort)
        if (orderString.includes('invalid')) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400004)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
      }

      // Generate the final SQL query
      teamsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(teamsQuery, conditionString, orderString)

      // Retrieve the team records
      let teams = await sequelize.query(teamsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      
      if (await teams == undefined || await teams.length < 1) {
        res.send(200, {
          rows: [],
          count: 0
        })
        return
      } else {
        // Get final count of the team records retrieved
        let teamsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(teamsQuery, conditionString, orderString)

        let teamsCount = await sequelize.query(teamsCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
          .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
          })

        count = teamsCount[0].count
      }

      res.send(200, {
        rows: teams,
        count: count
      })
      return
    }
}