/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

  // Endpoint for retrieving pipeline information given a specific pipeline id
  // GET /v3/pipelines/:id

  get: async function (req, res, next) {
    // Retrieve the pipeline id
    let pipelineDbId = req.params.id

    if (!validator.isInt(pipelineDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400182)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let pipelinesQuery = `
      SELECT
        pipeline.id AS "pipelineDbId",
        pipeline.pipeline_code AS "pipelineCode",
        pipeline.pipeline_name AS "pipelineName",
        pipeline.description AS "description",
        pipeline.pipeline_status AS "pipelineStatus",
        pipeline.description,
        pipeline.notes,
        pipeline.creation_timestamp AS "creationTimestamp",
        creator.person_name AS creator,
        pipeline.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS modifier
      FROM
        tenant.pipeline pipeline
      LEFT JOIN
        tenant.person creator ON pipeline.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON pipeline.modifier_id = modifier.id
      WHERE
        pipeline.id = ${pipelineDbId}
        AND pipeline.is_void = FALSE
    `
    // Retrieve pipelines from the database
    let pipelines = await sequelize
      .query(pipelinesQuery, { 
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (await pipelines === undefined || await pipelines.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404001)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: pipelines
    })
    return
  }
}
