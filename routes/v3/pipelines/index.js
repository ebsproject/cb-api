/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../helpers/error-builder')
let userValidator = require('../../../helpers/person/validator.js')
let format = require('pg-format')

module.exports = {
    // Endpoint for retrieving all pipelines
    // GET /v3/pipelines

    get: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let conditionString = ''
        let orderString = ''

        // Build base query for pipelines
        let pipelinesQuery = `
            SELECT
                pipeline.id AS "pipelineDbId",
                pipeline.pipeline_code AS "pipelineCode",
                pipeline.pipeline_name AS "pipelineName",
                pipeline.description AS "description",
                pipeline.pipeline_status AS "pipelineStatus",
                pipeline.description,
                pipeline.notes,
                pipeline.creation_timestamp AS "creationTimestamp",
                creator.person_name AS creator,
                pipeline.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                tenant.pipeline pipeline
            LEFT JOIN
                tenant.person creator ON pipeline.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON pipeline.modifier_id = modifier.id
            WHERE
                pipeline.is_void = FALSE
            ORDER BY
                pipeline.id
        `
        // Get filter condition/s for abbrev
        if (params.pipelineCode !== undefined) {
            let parameters = {
                pipelineCode: params.pipelineCode
            }

            conditionString = await processQueryHelper.getFilterString(
                parameters
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Get filter condition/s for sort
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the final query
        pipelineFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            pipelinesQuery,
            conditionString,
            orderString
        )

        // Retrieve pipeline records from database
        let pipelines = await sequelize
            .query(pipelineFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
          })
        
        if (await pipelines === undefined || await pipelines.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get pipeline count
            pipelineCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                pipelinesQuery,
                conditionString,
                orderString
            )

            pipelineCount = await sequelize
                .query(pipelineCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = pipelineCount[0].count
        }

        res.send(200, {
            rows: pipelines,
            count: count
        })

        return
    },

    /**
     * Create pipeline records
     * POST /v3/pipelines
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res, next) {
        let pipelineCode
        let pipelineName
        let pipelineStatus
        let description
        let notes
        let supportedPipelineStatus = ['active', 'archive', 'draft']
        let resultArray = []

        // Get user ID
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the user is an admin
        let isAdmin = await userValidator.isAdmin(userDbId)

        // If not an admin, return an error
        if (!isAdmin) {
            let errMsg = "Unauthorized request. You do not have the permission to create new pipeline records."
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let pipelinesUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/pipelines'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let data = req.body
        let records = [] 

        try {
            records = data.records
            // Check if the input data is empty
            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } 
        catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            let pipelinesArray = []
            for (var record of records) {
                // Get record values
                pipelineCode = record.pipelineCode
                pipelineName = record.pipelineName
                pipelineStatus = record.pipelineStatus
                description = record.description
                notes = record.notes

                // Check required fields
                if (pipelineCode == null || pipelineName == null 
                    || pipelineStatus == null) {
                    let errMsg = "Required parameters are missing. Ensure that the pipelineCode, pipelineName, and pipelineStatus are specified."
                    res.send(new errors.BadRequestError(errMsg))
                    return

                }

                // Check pipelineCode format
                // Should be alphanumeric. Only allowed symbol is underscore.
                if (!/^[A-Z0-9_]+$/.test(pipelineCode)) {
                    let errMsg = "Invalid pipelineCode format. Must be all uppercase alphanumeric characters and underscores only eg. 'pipeline_ABC_123'."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if pipelineCode does not exist yet
                let pipelineCodeQuery = `
                    SELECT count(1)
                    FROM tenant.pipeline
                    WHERE LOWER(pipeline_code) = '${pipelineCode.toLowerCase()}'
                `

                let pipelineCodes = await sequelize.query(pipelineCodeQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (pipelineCodes == null) {
                    return
                }

                if (pipelineCodes[0] != null && pipelineCodes[0].count != null && pipelineCodes[0].count > 0) {
                    let errMsg = `The pipelineCode '${pipelineCode}' already exists.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if pipelineName does not exist yet
                let pipelineNameQuery = `
                    SELECT count(1)
                    FROM tenant.pipeline
                    WHERE LOWER(pipeline_name) = '${pipelineName.toLowerCase()}'
                `

                let pipelineNames = await sequelize.query(pipelineNameQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

                if (pipelineNames == null) {
                    return
                }

                if (pipelineNames[0] != null && pipelineNames[0].count != null && pipelineNames[0].count > 0) {
                    let errMsg = `The pipelineName '${pipelineName}' already exists.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the pipeline status is valid
                if (supportedPipelineStatus.indexOf(pipelineStatus) === -1) {
                    let errMsg = "Invalid pipelineStatus value. Should be: " + supportedPipelineStatus.join(', ')
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                description = record.description ?? null // Optional; default value set to null
                notes = record.notes ?? null // Optional; default value set to null
              
                // Add data to insertion array
                let tempArray = [
                    pipelineCode,
                    pipelineName,
                    pipelineStatus,
                    description,
                    notes,
                    userDbId
                ]

                pipelinesArray.push(tempArray)
            }


            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            // Insert new record
            let pipelinesInsertQuery = format(`
                INSERT INTO tenant.pipeline 
                    (pipeline_code, pipeline_name, pipeline_status, description, notes, creator_id)
                VALUES 
                    %L 
                RETURNING 
                    id`, pipelinesArray
            )


            let pipelines = await sequelize.query(pipelinesInsertQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            }).catch(async err => {
                if (transaction != null && transaction.finished !== 'commit') transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            })

            if (pipelines == null) {
                return
            }

            // Commit the transaction
            await transaction.commit()

            let pipelinesCreated = pipelines[0] != null ? pipelines[0] : []
            
            // Add id to resultsArray
            for (let pipeline of pipelinesCreated) {
                let currentpipelineDbId = pipeline.id
                resultArray.push({
                    pipelineDbId: currentpipelineDbId,
                    recordCount: 1,
                    href: pipelinesUrlString + '/' + currentpipelineDbId
                });
            }

            res.send(200, {
                rows: resultArray
            })
            return
            
        } catch (e) {
            // Rollback transaction (if still open)
            if (transaction != null && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return   
        }
    }
}
