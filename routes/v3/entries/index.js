/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
const logger = require('../../../helpers/logger/index.js')
const endpoint = 'entries'

module.exports = {

    // Implementation of POST call for /v3/entries
    post: async (req, res, next) => {

        // Set defaults
        let resultArray = []
        let recordCount = 0

        let entryDbId = null
        let entryCode = null
        let entryNumber = 0

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if (personDbId === undefined) {
            return
        }
        // If personDbId is null, return 401: User not found error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let entryUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/entries'


        let data = req.body
        let records = []

        records = data.records
        
        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        recordCount = records.length

        try {
            let entryValuesArray = []
            let visitedArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                let entryListDbId = null
                let germplasmDbId = null
                let seedDbId = null
                let packageDbId = null

                let entryName = null
                let entryType = null
                let entryStatus = null
                let entryRole = null
                let entryClass = null
                let description = null
                let notes = null

                // Check if required columns are in the input
                if (
                    !record.entryName || !record.entryType || !record.entryStatus || 
                    !record.entryListDbId || !record.germplasmDbId
                ) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400233)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */

                if (record.entryType !== undefined) {
                    entryType = record.entryType
                    entryType = entryType.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if entry type is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'ENTRY_TYPE' AND
                            scaleValue.value ILIKE $$${entryType}$$
                        )
                    `
                    validateCount += 1
                }

                if (record.entryStatus !== undefined) {
                    entryStatus = record.entryStatus.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_STATUS' AND
                                scaleValue.value ILIKE $$${entryStatus}$$
                        )
                    `
                    validateCount += 1
                }

                if (record.entryListDbId !== undefined) {
                    entryListDbId = record.entryListDbId

                    if (!validator.isInt(entryListDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate entry list ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry_list entryList
                            WHERE 
                                entryList.is_void = FALSE AND
                                entryList.id = ${entryListDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.germplasmDbId !== undefined) {
                    germplasmDbId = record.germplasmDbId

                    if (!validator.isInt(germplasmDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400238)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate germplasm ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if germplasm is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.germplasm germplasm
                            WHERE 
                                germplasm.is_void = FALSE AND
                                germplasm.id = ${germplasmDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.packageDbId !== undefined) {
                    packageDbId = record.packageDbId
                    
                    if (!validator.isInt(packageDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400234)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the package exists, return 1
                            SELECT
                                count(1)
                            FROM
                                germplasm.package package
                            WHERE
                                package.is_void = FALSE AND
                                package.id = ${packageDbId}
                        )
                    `

                    validateCount += 1
                }

                entryName = record.entryName
                entryName = entryName.trim()

                /** Validation of non-required parameters and set values */

                if (record.entryRole !== undefined) {
                    entryRole = record.entryRole
                    entryRole = entryRole.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if entry role is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'ENTRY_ROLE' AND
                            scaleValue.value ILIKE $$${entryRole}$$
                        )
                    `
                    validateCount += 1
                }

                if (record.seedDbId !== undefined) {
                    seedDbId = record.seedDbId

                    if (!validator.isInt(seedDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400236)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate seed ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if project is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.seed seed
                            WHERE 
                                seed.is_void = FALSE AND
                                seed.id = ${seedDbId}
                        )
                    `
                    validateCount += 1
                }

                entryClass = (record.entryClass !== undefined) ? 
                    record.entryClass.trim() : null

                description = (record.description !== undefined) ? 
                    record.description.trim() : null

                notes = (record.notes !== undefined) ?
                    record.notes.trim() : null    

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Generation of entry number = max(entry_number) */
                let maxEntryNumberQuery = `
                    SELECT
                        CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE MAX(entry.entry_number)+1
                        END AS "maxEntryNumber",
                        count(*) AS "entryCount"
                    FROM
                        experiment.entry entry
                    WHERE
                        entry.is_void = FALSE AND
                        entry.entry_list_id = ${entryListDbId}
                `

                let entryNumberCheck = await sequelize.query(maxEntryNumberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (entryNumberCheck[0].entryCount == 0) {
                    entryNumber+=1
                } else {
                    entryNumber = parseInt(entryNumberCheck[0].maxEntryNumber)

                    if (records.indexOf(record) > 0) {
                        entryNumber = visitedArray[visitedArray.length-1]+1
                    }
                }

                // Check duplicates if recordCount > 1
                if (recordCount > 1) {
                    if (visitedArray.includes(entryNumber)) {
                        entryNumber+=1
                    } else {
                        visitedArray.push(entryNumber)
                    }
                }
                
                // If entryCode does not have user input, entryCode will be the same as entryNumber
                entryCode = (record.entryCode !== undefined) ? 
                    record.entryCode.trim() : entryNumber.toString().trim()

                // Get values
                let tempArray = [
                    entryCode, entryNumber, entryName, entryType, entryStatus, entryListDbId,
                    germplasmDbId, packageDbId, entryRole, entryClass, description, seedDbId, personDbId, notes
                ]
                
                entryValuesArray.push(tempArray)
            }

            // Create entry record
            let entriesQuery = format(`
                INSERT INTO 
                    experiment.entry (
                        entry_code, entry_number, entry_name, entry_type, entry_status,
                        entry_list_id, germplasm_id, package_id, entry_role, entry_class, description,
                        seed_id, creator_id, notes
                    )
                VALUES
                    %L
                RETURNING id`, entryValuesArray
            )

            let entries = await sequelize.transaction(async transaction => {
                return await sequelize.query(entriesQuery, {
                    type: sequelize.QueryTypes.INSERT, 
                    transaction: transaction,
                    raw: true,
                }).catch(err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (entry of entries[0]) {
                entryDbId = entry.id

                let entryRecord = {
                    entryDbId: entryDbId,
                    recordCount: 1,
                    href: entryUrlString + '/' + entryDbId
                }

                resultArray.push(entryRecord)
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}