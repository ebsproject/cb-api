/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let format = require('pg-format')

module.exports = {

    /**
     * POST /v3/entries/:id/delete-entry-data allows the deletion of all entry data 
     * under a specific entry given the entry ID
     * 
     * @param entryDbId integer as a path parameter 
     * @param token string token
     * 
     * @return object response data
     */

    post: async function (req, res, next) {
        
        // Retrieve plot ID
        let entryDbId = req.params.id

        if (!validator.isInt(entryDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400038)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })
            
            let entryQuery = `
                SELECT
                    entryList.id AS "entryListDbId",
                    creator.id AS "creatorDbId"
                FROM
                    experiment.entry entry 
                LEFT JOIN
                    experiment.entry_list entryList ON entryList.id = entry.entry_list_id
                LEFT JOIN
                    tenant.person creator ON creator.id = entry.creator_id
                WHERE
                    entry.is_void = FALSE AND
                    entry.id = ${entryDbId}
            `
            
            // Retrieve cross from the database
            let entry = await sequelize.query(entryQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await entry === undefined || await entry.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404004)
                res.send(new errors.NotFoundError(errMsg))
                return
            }
            
            let recordEntryListDbId = entry[0].entryListDbId
            let recordCreatorDbId = entry[0].creatorDbId

            // Check if user is an admin or an owner of the plot
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    LEFT JOIN 
                        experiment.entry_list entryList ON entryList.experiment_id = entryList.id
                    WHERE 
                        entryList.id = ${recordEntryListDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `
        
                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let entryDataQuery = `
                SELECT 
                    id
                FROM
                    experiment.entry_data
                WHERE
                    is_void = FALSE AND
                    entry_id = ${entryDbId}
                ORDER BY
                    id
            `

            let entryDataRecords = await sequelize.query(entryDataQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            let resultArray = []
            let entryDataObj = {}

            if (entryDataRecords.length != 0) {
                let deleteEntryDataQuery = format(`
                    UPDATE
                        experiment.entry_data
                    SET
                        is_void = TRUE,
                        notes = 'VOIDED',
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE
                        entry_id = ${entryDbId}
                `)

                await sequelize.query(deleteEntryDataQuery, {
                    type: sequelize.QueryTypes.UPDATE
                })

                await transaction.commit()

                for (entryData of entryDataRecords) {
                    entryDataObj = {
                        entryDataDbId: entryData.id
                    }
                    resultArray.push(entryDataObj)
                }
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}