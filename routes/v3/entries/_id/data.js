/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let aclStudy = require('../../../../helpers/acl/study.js')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/entries/:id/data
    get: async function (req, res, next) {

        // Retrieve the entry ID
        let entryDbId = req.params.id

        // Check if the ID is valid
        if (!validator.isInt(entryDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400020)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            // Retrieve the user ID of the client from the access token
            let userId = await tokenHelper.getUserId(req)

            if (userId == null) {
              let errMsg = await errorBuilder.getError(req.headers.host, 401002)
              res.send(new errors.BadRequestError(errMsg))
              return
            }

            let applyAcl = true

            // Check if user is an administrator. If yes, do not apply ACL.
            if (await userValidator.isAdmin(userId)) {
                applyAcl = false
            }

            if (applyAcl == true) {

                // Check if the user has access to the entry
                let canAccess = await aclStudy.isStudyAccessible('entry', '' + entryDbId, 'user', '' + userId)

                if (!canAccess) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401005)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            // Check if the study is committed to set the entry table
            let isCommitted = req.params.committed

            let entryTable = 'entry'
            let entryDbIdName = 'entryDbId'
            if ((isCommitted != null) && (isCommitted.toUpperCase() == 'FALSE')) {
                entryTable = 'temp_entry'
                entryDbIdName = 'tempEntryDbId'
            }

            let entryQuery = `
                SELECT
                    entry.id AS "` + entryDbIdName + `",
                    entry.key AS "entryKey",
                    entry.entry_type AS "entryType",
                    entry.times_rep AS "timesRep",
                    entry.entno,
                    entry.entcode,
                    entry.product_id AS "productDbId",
                    entry.product_gid AS "productGID",
                    entry.product_name AS "productName",
                    RTRIM(product.parentage) AS parentage,
                    entry.seed_storage_log_id AS "seedStorageLogDbId",
                    entry.product_gid_id AS "productGidDbId",
                    entry.metno,
                    entry.sample_id AS "sampleDbId",
                    entry.remarks,
                    entry.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    entry.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier
                FROM 
                    operational.study study,
                    operational.` + entryTable + ` entry
                LEFT JOIN 
                    master.product product ON product.id = entry.product_id
                LEFT JOIN 
                    master.user creator ON entry.creator_id = creator.id
                LEFT JOIN 
                    master.user modifier ON entry.modifier_id = modifier.id
                WHERE 
                    study.is_void = FALSE
                    AND study.id = entry.study_id
                    AND entry.is_void = FALSE
                    AND entry.id = (:entryDbId)
            `

            // Retrieve entry from the database   
            let entries = await sequelize.query(entryQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    entryDbId: entryDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await entries == undefined || await entries.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404005)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Format results from query into an array
            for (entry of entries) {
                let entryDataQuery = `
                    SELECT
                        "entryData".variable_id AS "variableDbId",
                        variable.abbrev AS "variableAbbrev",
                        variable.label AS "variableLabel",
                        "entryData".value AS "variableValue",
                        "entryData".creation_timestamp AS "creationTimestamp",
                        creator.id AS "creatorDbId",
                        creator.display_name AS creator,
                        "entryData".modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.display_name AS modifier
                    FROM operational.` + entryTable + `_data "entryData"
                    LEFT JOIN 
                        master.variable variable ON variable.id = "entryData".variable_id
                    LEFT JOIN 
                        master.user creator ON "entryData".creator_id = creator.id
                    LEFT JOIN 
                        master.user modifier ON "entryData".modifier_id = modifier.id
                    WHERE 
                        "entryData".is_void = FALSE
                        AND "entryData".entry_id = (:entryDbId)
                    ORDER BY variable.abbrev
                `

                let entryData = await sequelize.query(entryDataQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        entryDbId: entry[entryDbIdName]
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                entry["data"] = entryData
            }

            res.send(200, {
                rows: entries
            })
            return
        }
    }
}