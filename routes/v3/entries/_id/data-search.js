/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
  post: async (req, res, next) => {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let addedOrderString = `
      ORDER BY
        entry_data.id
    `
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let parameters = {}
    parameters['distinctOn'] = ''

    let entryDbId = req.params.id

    if (!validator.isInt(entryDbId)) {
      let errMsg =
        'Invalid format for entryDbId. It must be an integer.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let entryQuery = `
      SELECT
        entry.id AS "entryDbId",
        entry.entry_number AS "entryNumber",
        entry.entry_code AS "entryCode",
        entry.entry_name AS "entryName",
        entry.entry_type AS "entryType",
        entry.entry_role AS "entryRole",
        entry.entry_class AS "entryClass",
        entry.entry_status AS "entryStatus",
        entry.description,
        entry.entry_list_id AS "entryListDbId",
        entry.germplasm_id AS "germplasmDbId",
        germplasm.designation,
        germplasm.parentage,
        germplasm.generation,
        entry.seed_id AS "seedDbId",
        seed.seed_code AS "seedCode",
        seed.seed_name AS "seedName",
        seed.source_entry_id AS "sourceEntryDbId",
        seed.source_occurrence_id AS "sourceOccurrenceDbId",
        entry.notes,
        entry.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS "creator",
        entry.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS "modifier"
      FROM
        experiment.entry entry
      LEFT JOIN
        tenant.person creator ON entry.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON entry.modifier_id = modifier.id
      LEFT JOIN
        germplasm.germplasm germplasm ON entry.germplasm_id = germplasm.id
      LEFT JOIN
        germplasm.seed seed ON seed.id = entry.seed_id
      WHERE
        entry.is_void = FALSE AND
        entry.id = ${entryDbId}
    `
      
    let entry = await sequelize
      .query(entryQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (entry == undefined || entry.length < 1) {
      let errMsg =
        'Resource not found, the entry you have requested for does not exist.'
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(
            parameters['distinctOn']
          )
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
      // Set the excluded parameters
      let excludedParametersArray = [
        'fields',
        'distinctOn',
      ]
      // Get the filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray,
      )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the entry data retrieval query
    let entryDataQuery = null
    // Check if the client specified values for fields
    if (parameters['fields'] != null) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters['fields'],
        )
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        entryDataQuery = knex.select(selectString)
      } else {
        entryDataQuery = knex.column(parameters['fields'].split('|'))
      }

      entryDataQuery += `
        FROM
          experiment.entry_data entry_data
        LEFT JOIN
          master.variable variable ON entry_data.variable_id = variable.id
        LEFT JOIN
          tenant.person creator ON entry_data.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON entry_data.modifier_id = modifier.id
        WHERE
          entry_data.is_void = FALSE AND
          entry_data.entry_id = ${entryDbId}
        ` + addedOrderString
    } else {
      entryDataQuery = `
        SELECT
          ${addedDistinctString}
          entry_data.id AS "entryDataDbId",
          entry_data.entry_id AS "entryDbId",
          variable.id AS "variableDbId",
          variable.abbrev AS "variableAbbrev",
          entry_data.data_value AS "dataValue",
          entry_data.data_qc_code AS "dataQcCode",
          entry_data.transaction_id AS "transactionDbId",
          entry_data.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          entry_data.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          experiment.entry_data entry_data
        LEFT JOIN
          master.variable variable ON entry_data.variable_id = variable.id
        LEFT JOIN
          tenant.person creator ON entry_data.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON entry_data.modifier_id = modifier.id
        WHERE
          entry_data.is_void = FALSE AND
          variable.is_void = false AND
          entry_data.entry_id = ${entryDbId}
      ` + addedOrderString
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    let entryDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      entryDataQuery,
      conditionString,
      orderString,
    )

    let entryData = await sequelize
      .query(entryDataFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    entry[0]['data'] = entryData

    res.send(200, {
      rows: entry
    })
    return
  }
}