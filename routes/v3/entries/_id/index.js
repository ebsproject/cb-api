/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator.js')
let errors = require('restify-errors')
let validator = require('validator')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')
const logger = require ('../../../../helpers/logger/index.js')
const endpoint = 'entries/:id'

module.exports = {

    // Implementation for PUT /v3/entries/:id
    put: async function (req, res, next) {
        // Retrieve entry ID
        let entryDbId = req.params.id
        
        // Check if ID is an integer
        if (!validator.isInt(entryDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400020)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Get userDbId
        let personDbId = await tokenHelper.getUserId(req, res)

        // If userDbId is undefined, terminate
        if (personDbId === undefined) {
            return
        }

        // If personDbId is null, return 401: User not found error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Set defaults
        let entryListDbId = null
        let germplasmDbId = null
        let seedDbId = null
        let packageDbId = null

        let entryNumber = null
        let entryCode = null
        let entryName = null
        let entryType = null
        let entryStatus = null
        let entryRole = null
        let entryClass = null
        let description = null

        // Build query
        let entryQuery = `
            SELECT
                entryList.id AS "entryListDbId",
                germplasm.id AS "germplasmDbId",
                seed.id AS "seedDbId",
                package.id AS "packageDbId",
                entry.entry_number AS "entryNumber",
                entry.entry_code AS "entryCode",
                entry.entry_name AS "entryName",
                entry.entry_type AS "entryType",
                entry.entry_status AS "entryStatus",
                entry.entry_role AS "entryRole",
                entry.entry_class AS "entryClass",
                entry.description,
                creator.id AS "creatorDbId",
                experiment.id as "experimentDbId"
            FROM
                experiment.entry entry
            LEFT JOIN
                germplasm.package package ON package.id = entry.package_id
            LEFT JOIN
                experiment.entry_list entryList ON entryList.id = entry.entry_list_id
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = entryList.experiment_id
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = entry.germplasm_id
            LEFT JOIN
                germplasm.seed seed ON seed.id = entry.seed_id
            LEFT JOIN
                tenant.person creator ON creator.id = entry.creator_id
            WHERE
                entry.is_void = FALSE AND
                entry.id = ${entryDbId}
        `

        // Retrieve entry record from database
        let entry = await sequelize.query(entryQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            logger.logFailingQuery(endpoint, 'SELECT', err)
            return undefined
        })

        if (entry == undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            if (!res.headersSent) if (!res.headersSent) res.send(new errors.InternalError(errMsg))

            return
        }

        if (entry.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404005)
            if (!res.headersSent) res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordEntryListDbId = entry[0].entryListDbId
        let recordGermplasmDbId = entry[0].germplasmDbId
        let recordSeedDbId = entry[0].seedDbId
        let recordPackageDbId = entry[0].packageDbId
        let recordCreatorDbId = entry[0].creatorDbId
        
        let recordEntryNumber = entry[0].entryNumber
        let recordEntryCode = entry[0].entryCode
        let recordEntryType = entry[0].entryType
        let recordEntryStatus = entry[0].entryStatus
        let recordEntryRole = entry[0].entryRole
        let recordEntryName = entry[0].entryName
        let recordEntryClass = entry[0].entryClass
        let recordDescription = entry[0].description
        let recordExperimentDbId = entry[0].experimentDbId

        // Check if user is an admin or an owner of the experiment
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${recordExperimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                if (!res.headersSent) res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                if (!res.headersSent) res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.header).secure

        // Set URL for response
        let entryUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/entries/'
            + entryDbId

        let data = req.body

        try {
            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values */

            if (data.entryListDbId !== undefined) {
                entryListDbId = data.entryListDbId

                if (!validator.isInt(entryListDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (entryListDbId != recordEntryListDbId) {

                    // Validate entry list ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry_list entryList
                            WHERE 
                                entryList.is_void = FALSE AND
                                entryList.id = ${entryListDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_list_id = $$${entryListDbId}$$
                    `
                }
            }

            if (data.germplasmDbId !== undefined) {
                germplasmDbId = data.germplasmDbId

                if (!validator.isInt(germplasmDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400238)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (germplasmDbId != recordGermplasmDbId) {

                    // Validate germplasm ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if germplasm is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.germplasm germplasm
                            WHERE 
                                germplasm.is_void = FALSE AND
                                germplasm.id = ${germplasmDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_id = $$${germplasmDbId}$$
                    `
                }
            }

            if (data.packageDbId !== undefined) {
                packageDbId = data.packageDbId

                if (packageDbId !== "null") {
                    // Check if package is a valid integer
                    if (!validator.isInt(packageDbId)) {
                        let errMsg = 'Invalid format, package ID must be an integer.'
                        if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate if the package actually exists
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if package exists, return 1
                            SELECT
                                count(1)
                            FROM
                                germplasm.package package
                            WHERE
                                package.is_void = FALSE AND
                                package.id = ${packageDbId}
                        )
                    `

                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_id = $$${packageDbId}$$
                    `
                } else {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_id = NULL
                    `
                }
            }

            if (data.seedDbId !== undefined) {
                seedDbId = data.seedDbId

                if (!validator.isInt(seedDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400236)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (seedDbId != recordSeedDbId) {

                    // Validate seed ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if seed is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.seed seed
                            WHERE 
                                seed.is_void = FALSE AND
                                seed.id = ${seedDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        seed_id = $$${seedDbId}$$
                    `
                }
            }

            if (data.entryType !== undefined) {
                entryType = data.entryType
                entryType = entryType.trim()

                // Check if user input is same with the current value in the database
                if (entryType != recordEntryType) {

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry type is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_TYPE' AND
                                scaleValue.value ILIKE $$${entryType}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_type = $$${entryType}$$
                    `
                }
            }

            if (data.entryStatus !== undefined) {
                entryStatus = data.entryStatus.trim()

                // Check if user input is same with the current value in the database
                if (entryStatus != recordEntryStatus) {

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_STATUS' AND
                                scaleValue.value ILIKE $$${entryStatus}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_status = $$${entryStatus}$$
                    `
                }
            }

            if (data.entryRole !== undefined) {
                entryRole = data.entryRole.trim()

                // Check if user input is same with the current value in the database
                if (entryRole != recordEntryRole) {
                    if(entryRole !== "null"){
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if entry role is existing return 1
                                SELECT 
                                    count(1)
                                FROM 
                                    master.scale_value scaleValue
                                LEFT JOIN
                                    master.variable variable ON scaleValue.scale_id = variable.scale_id
                                WHERE
                                    variable.abbrev LIKE 'ENTRY_ROLE' AND
                                    scaleValue.value ILIKE $$${entryRole}$$
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            entry_role = $$${entryRole}$$
                        `
                    } else {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            entry_role = NULL
                        `
                    }
                }
            }

            /** Set values of other parameters */
            if (data.entryNumber !== undefined) {
                entryNumber = data.entryNumber

                // Check if user input is same with the current value in the database
                if (entryNumber != recordEntryNumber) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_number = $$${entryNumber}$$
                    `
                }
            }

            if (data.entryCode !== undefined) {
                entryCode = data.entryCode

                // Check if user input is same with the current value in the database
                if (entryCode != recordEntryCode) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_code = $$${entryCode}$$
                    `
                }
            }
            
            if (data.entryName !== undefined) {
                entryName = data.entryName
                entryName = entryName.trim()

                // Check if user input is same with the current value in the database
                if (entryName != recordEntryName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_name = $$${entryName}$$
                    `
                }
            }

            if (data.entryClass !== undefined) {
                entryClass = data.entryClass
                entryClass = entryClass.trim()

                // Check if user input is same with the current value in the database
                if (entryClass != recordEntryClass) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_class = $$${entryClass}$$
                    `
                }
            }

            if (data.description !== undefined) {
                description = data.description
                description = description.trim()

                // Check if user input is same with the current value in the database
                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        description = $$${description}$$
                    `
                }
            }

            let pairEntryListDbId = null
            let pairEntryNumber = null

            if (entryListDbId != null && entryNumber != null) {
                pairEntryListDbId = entryListDbId
                pairEntryNumber = entryNumber
            } else if (entryListDbId != null && entryNumber == null) {
                pairEntryListDbId = entryListDbId
                pairEntryNumber = recordEntryNumber
            } else if (entryListDbId == null && entryNumber != null) {
                pairEntryListDbId = recordEntryListDbId
                pairEntryNumber = entryNumber
            }

            validateQuery += (validateQuery != '') ? ' + ' : ''
            validateQuery += `
                (
                -- Check if entryListDbId-entryNumber pair is unique return 1
                SELECT
                    CASE WHEN
                    (count(1) = 0)
                    THEN 1
                    ELSE 0
                    END AS count
                FROM
                    experiment.entry entry
                WHERE
                    entry.is_void = FALSE AND
                    entry.entry_list_id = ${pairEntryListDbId} AND
                    entry.entry_number = ${pairEntryNumber}
                )
            `
            validateCount += 1


            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    if (!res.headersSent) res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if(setQuery.length > 0) setQuery += `,`

            // Update the entry record
            let updateEntryQuery = `
                UPDATE
                    experiment.entry
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    entry.id = ${entryDbId}
            `

            await sequelize.transaction(async transaction => {
                await sequelize.query(updateEntryQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true,
                }).catch(err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return transaction info
            let resultArray = {
                entryDbId: entryDbId,
                recordCount: 1,
                href: entryUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }, 
    
    // DELETE /v3/entries/:id
    delete: async function (req, res, next) {
        // Retrieve entry ID
        let entryDbId = req.params.id

        if (!validator.isInt(entryDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400020)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let isAdmin = await userValidator.isAdmin(personDbId)

        try {
            let entryQuery = `
                SELECT 
                    entry.entry_code AS "entryCode",
                    entry.entry_list_id AS "entryListDbId",
                    creator.id AS "creatorDbId",
                    experiment.id as "experimentDbId"
                FROM
                    experiment.entry entry 
                LEFT JOIN 
                    tenant.person creator ON creator.id = entry.creator_id
                LEFT JOIN
                    experiment.entry_list entryList ON entryList.id = entry.entry_list_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = entryList.experiment_id
                WHERE
                    entry.is_void = FALSE AND
                    entry.id = ${entryDbId}
            `

            let entry = await sequelize.query(entryQuery, {
                type: sequelize.QueryTypes.SELECT
            }).catch(async err => {
                logger.logFailingQuery(endpoint, 'SELECT', err)
                return undefined
            })
    
            if (entry == undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                if (!res.headersSent) if (!res.headersSent) res.send(new errors.InternalError(errMsg))
    
                return
            }
    
            if (entry.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404005)
                if (!res.headersSent) res.send(new errors.NotFoundError(errMsg))
                return
            }
    
            let recordEntryListDbId = entry[0].entryListDbId
            let recordCreatorDbId = entry[0].creatorDbId
            let recordExperimentDbId = entry[0].experimentDbId

            
            // Check if user is an admin or an owner of the experiment
            if (!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    LEFT JOIN 
                        experiment.entry_list entryList ON entryList.experiment_id = entryList.id
                    WHERE 
                        experiment.id = ${recordExperimentDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                    let person = await sequelize.query(programMemberQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    
                    if (person[0].id === undefined || person[0].role === undefined) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                        res.send(new errors.UnauthorizedError(errMsg))
                        return
                    }
                    
                    // Checks if user is a program team member or has a producer role
                    let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                    let isProducer = (person[0].role == '26') ? true : false
                    
                    if (!isProgramTeamMember && !isProducer) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                        res.send(new errors.UnauthorizedError(errMsg))
                        return
                    }
            }

            // get entry_list_id of the entry
            let getListIdQuery = `
                SELECT
                    entry.entry_list_id as "entryListDbId"
                FROM
                    experiment.entry entry 
                WHERE
                    entry.is_void = FALSE AND
                    entry.id = '${entryDbId}'
            `

            entryList = await sequelize.query(getListIdQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            entryListDbId = entryList[0]['entryListDbId']

            let deleteEntryQuery = format(`
                UPDATE
                    experiment.entry entry 
                SET
                    is_void = TRUE,
                    entry_code = 'VOIDED-${entryDbId}',
                    entry_number = CONCAT('-', $$${entryDbId}$$)::int,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    entry.id = ${entryDbId}
            `)

            await sequelize.transaction(async transaction => {
                await sequelize.query(deleteEntryQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true,
                }).catch(err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            let result = {
                entryListDbId: entryListDbId,
                entryDbId: entryDbId
            }

            res.send(200, {
                rows: result
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}