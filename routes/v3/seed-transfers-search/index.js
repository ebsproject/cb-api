/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const processQueryHelper = require("../../../helpers/processQuery/index");
const errorBuilder = require("../../../helpers/error-builder");
const errors = require("restify-errors");
const { knex } = require("../../../config/knex");
const { sequelize } = require("../../../config/sequelize");

module.exports = {
    post: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let params = req.query
        let count = 0

        let requestBody

        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let addedOrderString = `
            ORDER BY seed_transfer.id
        `

        let excludeParametersArray = ['fields','distinctOn']
        let searchParams = {}
        searchParams['distinctOn'] = ''

        // Set fields from request
        if (req.body) {
            requestBody = req.body

            if (req.body.fields != null) {
                searchParams['fields'] = req.body.fields
            }

            // Set distinctOn from request
            if (req.body.distinctOn) {
                searchParams['distinctOn'] = req.body.distinctOn

                addedDistinctString = await processQueryHelper.getDistinctString(
                    searchParams['distinctOn']
                )

                searchParams['distinctOn'] = `"${searchParams['distinctOn']}"`
                addedOrderString = `
                    ORDER BY ${searchParams['distinctOn']}
                `
            }

            // Get the filter condition/s
            conditionString = await processQueryHelper.getFilter(
                requestBody,
                excludeParametersArray
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build base query
        let seedTransferQuery = null
        let fromQuery = `
            FROM
                germplasm.seed_transfer seed_transfer
            WHERE
                seed_transfer.is_void = FALSE 
            ${addedOrderString}
        `

        if (searchParams['fields'] != null) {

            if (searchParams['distinctOn']) {
                let fieldsString = processQueryHelper.getFieldValuesString(
                    searchParams['fields']
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                seedTransferQuery = knex.select(selectString)
            } else {
                seedTransferQuery = knex.column(searchParams['fields'].split('|'))
            }

            seedTransferQuery += fromQuery
        } else {
            seedTransferQuery = `
                SELECT
                    ${addedDistinctString}
                    seed_transfer.id AS "seedTransferDbId",
                    seed_transfer.sender_program_id AS "senderProgramDbId",
                    (
                        SELECT
                            program.program_code AS "senderProgramCode"
                        FROM
                            tenant.program program
                        WHERE
                            program.is_void = FALSE AND 
                            program.id = seed_transfer.sender_program_id
                    ),
                    seed_transfer.receiver_program_id AS "receiverProgramDbId",
                    (
                        SELECT
                            program.program_code AS "receiverProgramCode"
                        FROM
                            tenant.program program
                        WHERE
                            program.is_void = FALSE AND 
                            program.id = seed_transfer.receiver_program_id
                    ),
                    seed_transfer.source_list_id AS "sourceListDbId",
                    (
                        SELECT
                            list.display_name AS "sourceListDisplayName"
                        FROM
                            platform.list list
                        WHERE
                            list.is_void = FALSE AND 
                            list.id = seed_transfer.source_list_id
                    ),
                    (
                        SELECT
                            lmc.member_count AS "sourceListCount"
                        FROM
                            platform.list list
                        LEFT JOIN (
                            SELECT
                                COUNT (lm.id) AS member_count,
                                lm.list_id
                            FROM
                                platform.list_member lm
                            WHERE
                                lm.is_void = FALSE AND
                                lm.is_active = TRUE
                            GROUP BY
                                lm.list_id
                        ) AS lmc (member_count, list_id)
                            ON lmc.list_id = list.id
                        WHERE
                            list.is_void = FALSE AND
                            list.id = seed_transfer.source_list_id
                    ),
                    seed_transfer.destination_list_id AS "destinationListDbId",
                    (
                        SELECT
                            list.display_name AS "destinationListDisplayName"
                        FROM
                            platform.list list
                        WHERE
                            list.is_void = FALSE AND
                            list.id = seed_transfer.destination_list_id
                    ),
                    seed_transfer.seed_transfer_status AS "seedTransferStatus",
                    seed_transfer.creator_id AS "creatorDbId",
                    (
                        SELECT
                            person.person_name AS "creator"
                        FROM
                            tenant.person person
                        WHERE
                            person.is_void = FALSE AND
                            person.id = seed_transfer.creator_id
                    ),
                    seed_transfer.creation_timestamp AS "creationTimestamp",
                    seed_transfer.sender_id AS "senderDbId",
                    (
                        SELECT
                            person.person_name AS "sender"
                        FROM
                            tenant.person person
                        WHERE
                            person.is_void = FALSE AND
                            person.id = seed_transfer.sender_id
                    ),
                    seed_transfer.send_timestamp AS "sendTimestamp",
                    seed_transfer.receiver_id AS "receiverDbId",
                    (
                        SELECT
                            person.person_name AS "receiver"
                        FROM
                            tenant.person person
                        WHERE
                            person.is_void = FALSE AND
                            person.id = seed_transfer.receiver_id
                    ),
                    seed_transfer.receive_timestamp AS "receiveTimestamp"
            `
            seedTransferQuery += fromQuery
        }

        // Parse sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        try {

            // Generate the final SQL query
            let seedTransferFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
                seedTransferQuery,
                conditionString,
                orderString,
            )

            let seedTransfers = await sequelize.query(seedTransferFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
            })
            // Error occurred during query
            if (seedTransfers == undefined) return

            // Successful query returned 0 results from the database
            if (seedTransfers.length < 1) {
                res.send(200, {
                    rows: [],
                    count: 0
                })
                return
            }

            // Count the number of records returned
            let seedTransfersCountSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                seedTransferQuery,
                conditionString,
                orderString,
            )
            let seedTransfersCount = await sequelize.query(seedTransfersCountSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
            })
            // Error occurred during query
            if (seedTransfersCount == undefined) return

            count = seedTransfersCount[0].count

            res.send(200, {
                rows: seedTransfers,
                count: count,
            })

        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
        }
    }
}