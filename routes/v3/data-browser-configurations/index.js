/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, config } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../helpers/logger')

const endpoint = 'data-browser-configurations'

module.exports = {
  // Implementation of GET call for /v3/data-browser-configurations
  get: async function (req, res, next) {
    
    // Set defaults
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let count = 0
    let conditionString = ''
    let orderString = ''
    let parameters = []

    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Build query
    let configsQuery = `
      SELECT
        configs.id AS "configDbId",
        configs.user_id AS "userDbId",
        configs.data_browser_id AS "dataBrowserDbId",
        configs.name,
        configs.data,
        configs.type AS "type"
      FROM
        platform.user_data_browser_configuration configs
      LEFT JOIN
        tenant.person "user" ON "user".id = configs.user_id AND
        "user".is_void = FALSE
      LEFT JOIN
        tenant.person modifier ON modifier.id = configs.modifier_id
      LEFT JOIN
        tenant.person creator ON creator.id = configs.creator_id
      WHERE
        configs.user_id = ${userDbId}
        and configs.is_void = FALSE
      ORDER BY
        configs.id
    `

    // get filter for dataBrowserDbId and type only
    if (params.dataBrowserDbId !== undefined || params.type !== undefined) {
      if (params.dataBrowserDbId !== undefined) {
        parameters['dataBrowserDbId'] = params.dataBrowserDbId.trim().toLowerCase()
      }
      if (params.type !== undefined) {
        parameters['type'] = params.type
      }

      conditionString = await processQueryHelper.getFilter(parameters)
      
      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)

      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final sql query
    let configsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      configsQuery,
      conditionString,
      orderString
    )
    
    // Retrieve configs from the database
    let configs = await sequelize.query(configsFinalSqlQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        limit: limit,
        offset: offset
      }
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    if ( await configs == undefined || await configs.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get count
      let configCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(configsQuery, conditionString, orderString)

      let configsCount = await sequelize.query(configCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      count = configsCount[0].count
    }

    res.send(200, {
      rows: configs,
      count: count
    })
    return
  },
  // Implementation of POST call for /v3/data-browser-confugurations
  post: async function (req, res, next) {
    let name = null
    let dataHolder = null
    let dataBrowserDbId = null
    let remarks = null
    let type = null
    let creatorId = null
    let resultArray = []

    // Verify if user is an admin
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure
    // Set URL for response
    let configsUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/data-browser-configurations'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = [] 
    let recordCount = 0

    // Records = [{}...]
    records = data.records
    recordCount = records.length
    // Check if the input data is empty
    if (records == undefined || records.length == 0) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400005)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Start transaction
    let transaction
    try {
      let configsValuesArray = []
      let visitedArray = []
      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        // check if the required columns are in the input
        if (
          record.name === undefined 
          || record.data === undefined 
          || record.dataBrowserDbId === undefined 
          || record.type === undefined) {
            let errMsg = 'Required parameters are missing. Ensure that the name, data, dataBrowserDbId, and type fields are not empty.'
            res.send(new errors.BadRequestError(errMsg))
            return
          }

          name = record.name 
          dataHolder = (record.data != undefined) ? record.data : null
          dataBrowserDbId = (record.dataBrowserDbId != undefined) ? record.dataBrowserDbId : null
          remarks = (record.remarks != undefined) ? record.remarks : null
          creatorId = userDbId
          type = record.type

          let tempArray = [
              userDbId,
              name,
              dataHolder,
              dataBrowserDbId,
              remarks,
              creatorId,
              type
          ]

          configsValuesArray.push(tempArray)
        }

        // Create user data browser record
        let configsQuery = format (`
          INSERT INTO platform.user_data_browser_configuration (user_id, name, data, data_browser_id, remarks, creator_id, "type")
          VALUES 
            %L
          RETURNING id`, configsValuesArray
        )
        configsQuery = configsQuery.trim()

        // Create data browser configuration record
        transaction = await sequelize.transaction(async transaction => {
          let configs = await sequelize.query(configsQuery, {
            type: sequelize.QueryTypes.INSERT,
            transaction: transaction
          }).catch(async err => {
            logger.logFailingQuery(endpoint, 'INSERT', err)
            throw new Error(err)
          })

          for (let config of configs[0]) {
            configsDbId = config.id
            // Return the season info to Client
            let array = {
              configsDbId: configsDbId,
              recordCount: 1,
              href: configsUrlString + '/' + configsDbId
            }
            resultArray.push(array)
          }
        })

        res.send(200, {
            rows: resultArray
        })
        return
      } catch (err) {
        logger.logMessage(__filename, err.stack, 'error')
        let errMsg = await errorBuilder.getError(req.headers.host, 500001)
        if (!res.headersSent) res.send(new errors.InternalError(errMsg))
        return
      }
  }
}