/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, configs } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let forwarded = require('forwarded-for')
let logger = require('../../../../helpers/logger/index')

const endpoint = 'data-browser-configurations/:id'

module.exports = {
  // Implementation of GET call for /v3/data-browser-configurations
  get: async function (req, res, next) {
    // Set defaults
    let configDbId = null

    // Retrieve the user ID of the client from the access token
    let userDbId = await tokenHelper.getUserId(req) 

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    configDbId = req.params.id

    // Build query
    if (!validator.isInt(configDbId)) {
      let errMsg = 'You have provided an invalid format for data browser config ID.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    //create the query
    let configsQuery = `
      SELECT
        configs.id AS "configDbId",
        configs.user_id AS "userDbId",
        configs.name,
        configs.data,
        configs.data_browser_id AS "dataBrowserDbId",
        configs.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS "creator",
        configs.modification_timestamp AS "modificatonTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS "modifier",
        configs.type AS "type"
      FROM
        platform.user_data_browser_configuration configs
      LEFT JOIN
        tenant.person "user" ON "user".id = configs.user_id
      LEFT JOIN
        tenant.person modifier ON modifier.id = configs.modifier_id
      LEFT JOIN
        tenant.person creator ON creator.id = configs.creator_id
      WHERE
        configs.is_void = FALSE AND
        configs.id = $$${configDbId}$$ AND
        configs.user_id = $$${userDbId}$$
      ORDER BY
        configs.id`
      
      let configs = await sequelize.query(configsQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })
      if (await configs === undefined || await configs.length < 1){
        let errMsg = 'The data browser config you requested does not exist'
        res.send(new errors.NotFoundError(errMsg))
        return
      }
    
    res.send(200, {
      rows: configs,
    })
    return
  },
  // Endpoint for updating a filter
  // PUT /v3/data-browser-configuration
  put: async function (req, res, next) {
    let name = null
    let dataHolder = null
    let dataBrowserDbId = null
    let remarks = null
    let resultArray = []

    //check if it has id
    let configsDbId = req.params.id
    if (!validator.isInt(configsDbId)) {
        let errMsg = 'Invalid format for configuration Id. Configuration ID must be an integer.'
        res.send(new errors.BadRequestError(errMsg))
        return
    }
    // Retrieve the user ID of the client from the access token
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let configsQuery = `
      SELECT
        configs.name AS name,
        configs.data AS "configsData",
        configs.data_browser_id AS "configsDataBrowserDbId",
        configs.remarks AS "configsRemarks"
      FROM
        platform.user_data_browser_configuration configs
      WHERE 
        configs.is_void = FALSE AND 
        configs.id = $$${configsDbId}$$ AND 
        configs.user_id= $$${userDbId}$$
      `  

    // Retrieve cross list from the database   
    let configs = await sequelize.query(configsQuery, {
      type: sequelize.QueryTypes.SELECT
    })

    // Return error if resource is not found
    if (configs.length < 1 || configs == undefined) {
      let errMsg = 'Invalid request. Ensure that the configuration exist. '
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Get values
    let configsName = configs[0].name
    let configsData = configs[0].configsData
    let configsDataBrowserDbId = configs[0].configsDataBrowserDbId
    let configsRemarks= configs[0].configsRemarks      
    let isSecure = forwarded(req, req.headers).secure

    // Set the URL for response
    let configsUrlString = (isSecure ? 'https' : 'http')
        + "://"
        + req.headers.host
        + "/v3/data-browser-configuratons"

    // Check for parameters
    let data = req.body
    if (data == undefined) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Start transaction
    let transaction
    try {
      let setQuery = ``     
      /**
       * 
       * Validation of the values
       *  
       * */
      if (data.name != undefined) {               
        name = data.name
        if(name != configsName){    
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
              name = $$${name}$$`
        }
      }
      if (data.data != undefined) {
        dataHolder = data.data
        dataHolder = JSON.stringify(dataHolder)
        if(dataHolder != configsData){    
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
              data = $$${dataHolder}$$`
        }
      }
      if (data.dataBrowserDbId != undefined) {
        dataBrowserDbId = data.dataBrowserDbId
        if(dataBrowserDbId != configsDataBrowserDbId){    
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            data_browser_id = $$${dataBrowserDbId}$$`
        }
      }
      if (data.remarks != undefined) {
        remarks = data.remarks
        if(remarks != configsRemarks){    
          setQuery += (setQuery != '') ? ',' : ''
          setQuery += `
            remarks = $$${remarks}$$`
        }
      }
      // Update the data Brows record
      let updateConfigsQuery = `
        UPDATE
          platform.user_data_browser_configuration configs
        SET
          ${setQuery},
          modification_timestamp = NOW(),
          modifier_id = $$${userDbId}$$
        WHERE
          configs.id = $$${configsDbId}$$
        `

      // Get transaction
      transaction = await sequelize.transaction(async transaction => {
        await sequelize.query(updateConfigsQuery, {
            type: sequelize.QueryTypes.UPDATE,
            transaction: transaction
        }).catch(async err => {
          logger.logFailingQuery(endpoint, 'UPDATE', err)
          throw new Error(err)
        })

        // Return the transaction info
        let array = {
          configsDbId: configsDbId,
          recordCount: 1,
          href: configsUrlString + '/' + configsDbId
        }
        resultArray.push(array)
      })

      res.send(200, {
          rows: resultArray
      })
      return

    } 
    catch (err) {
      logger.logMessage(__filename, err.stack, 'error')
      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      if (!res.headersSent) res.send(new errors.InternalError(errMsg))
      return
    }
  },
  delete: async function (req, res, next) {

    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Defaults
    let configsDbId = req.params.id

    if (!validator.isInt(configsDbId)) {
      let errMsg = 'Invalid format for configuration Id. Configuration ID must be an integer'
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    // Start transaction
    let transaction

    try {
      // Validate the data browser config
      // Check if data browser config is existing
      // Build query
      let configsQuery = `
        SELECT
          count(1)
        FROM 
          platform.user_data_browser_configuration configs
        WHERE
          configs.is_void = FALSE AND
          configs.id = ${configsDbId} AND
          configs.user_id = ${userDbId}
      `

      // Retrieve data browser config from the database
      let hasConfigs = await sequelize.query(configsQuery, {
        type: sequelize.QueryTypes.SELECT
        })

        // Check if data browser config is non-existent in database
        if (hasConfigs == undefined || hasConfigs.length === 0 || parseInt(hasConfigs[0].count) === 0) {
          let errMsg = 'Invalid request. Ensure that the configuration exist.'
          res.send(new errors.NotFoundError(errMsg))
          return
        }

        // Build query for voiding
        let deleteConfigsQuery = `
          UPDATE
            platform.user_data_browser_configuration configs
          SET
            is_void = TRUE,
            modification_timestamp = NOW(),
            modifier_id = ${userDbId}
          WHERE
            configs.id = ${configsDbId} AND
            configs.user_id = ${userDbId}
        `

        transaction = await sequelize.transaction(async transaction => {
          await sequelize.query(deleteConfigsQuery, {
            type: sequelize.QueryTypes.UPDATE,
            transaction: transaction
          }).catch(async err => {
            logger.logFailingQuery(endpoint, 'DELETE', err)
            throw new Error(err)
          })
        })

        res.send(200, {
            rows: {
                configsDbId: configsDbId
            }
        })
        return
    } catch (err) {
      logger.logMessage(__filename, err.stack, 'error')
      let errMsg = await errorBuilder.getError(req.headers.host, 500002)
      if (!res.headersSent) res.send(new errors.InternalError(errMsg))
      return
    }
  }
}