/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let errors = require('restify-errors')

module.exports = {
  get: async (req, res, next) => {

    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let count = 0
    let conditionString = ''
    let orderString = ''

    let containerQuery = `
    SELECT
      container.id AS "containerDbId",
      container.type,
      container.label,
      container.name AS "containerDisplayName",
      container.container_qr_code AS "containerQrCode",
      container.status,
      container.remarks,
      container.notes,
      container.creation_timestamp AS "creationTimestamp",
      program.id AS "programDbId",
      program.abbrev AS "programAbbrev",
      program.display_name AS "programDisplayName",
      program.program_status AS "programStatus",
      creator.id AS "creatorDbId",
      creator.display_name AS "creator",
      container.modification_timestamp AS "modificationTimestamp",
      modifier.id AS "modifierDbId",
      modifier.display_name AS "modifier",
      facility.id AS "facilityDbId",
      facility.name AS "facilityDisplayName"
    FROM
      seed_warehouse.container container
    LEFT JOIN
      master.user creator ON container.creator_id = creator.id
    LEFT JOIN
      master.user modifier ON container.modifier_id = modifier.id
    LEFT JOIN
      master.program program ON container.program_id = program.id
    LEFT JOIN
      master.facility facility ON container.facility_id = facility.id
    WHERE
      container.is_void = FALSE
      
    `

    if (params.abbrev !== undefined) {
      let parameters = {
        abbrev: params.abbrev
      };

      conditionString = await processQueryHelper.getFilterString(
        parameters
      );

      if (conditionString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400003)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort);

      if (orderString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let containerFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      containerQuery,
      conditionString,
      orderString
    )

    let containers = await sequelize
      .query(containerFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      }).catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await containers === undefined || await containers.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let containerCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
      containerQuery,
      conditionString,
      orderString
    );

    containerCount = await sequelize
      .query(containerCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    count = containerCount[0].count;

    res.send(200, {
      rows: containers,
      count: count
    })
    return
  }
}