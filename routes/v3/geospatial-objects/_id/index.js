/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

  // Endpoint for retrieving geospatial object information given a specific id
  // GET /v3/geospatial-objects/:id

  get: async function (req, res, next) {
    // Retrieve the geospatial object id
    let geospatialObjectId = req.params.id

    if (!validator.isInt(geospatialObjectId)) {
      let errMsg = `Invalid value for Geospatial Object ID`
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let geospatialObjectQuery = `
      SELECT
        geospatialObject.id AS "geospatialObjectId",
        geospatialObject.geospatial_object_code AS "geospatialObjectCode",
        geospatialObject.geospatial_object_name AS "geospatialObjectName",
        geospatialObject.geospatial_object_type AS "geospatialObjectType",
        geospatialObject.geospatial_object_subtype AS "geospatialObjectSubType",
        geospatialObject.geospatial_coordinates AS "geospatialCoordinates",
        geospatialObject.altitude,
        geospatialObject.parent_geospatial_object_id AS "parentGeoSpatialObjectId",
        geospatialObject.root_geospatial_object_id AS "rootGeoSpatialObjectId",
        geospatialObject.description,
        geospatialObject.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS creator,
        geospatialObject.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS modifier
      FROM
        place.geospatial_object geospatialObject
      LEFT JOIN
        tenant.person creator ON geospatialObject.creator_id = creator.id
      LEFT JOIN
        tenant.person modifier ON geospatialObject.modifier_id = modifier.id
      WHERE
        geospatialObject.id = ${geospatialObjectId}
        AND geospatialObject.is_void = FALSE
    `
    // Retrieve geospatial object from the database
    let geospatialObjects = await sequelize
      .query(geospatialObjectQuery, { 
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (await geospatialObjects === undefined || await geospatialObjects.length < 1) {
      let errMsg = `The geospatial object you have requested does not exist`
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: geospatialObjects
    })
    return
  }
}
