/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

module.exports = {
    // Implementation of POST call for /v3/geospatial-objects
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []

        let geospatialObjectCode = null
        let geospatialObjectName = null
        let geospatialObjectType = null
        let geospatialObjectSubtype = null
        let geospatialCoordinates = null
        let altitude = null
        let description = null
        let parentGeospatialObjectId = null
        let rootGeospatialObjectId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let geospatialObjectUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/geospatial-objects'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction

        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let geospatialObjectValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Check if required columns are in the input
                if(
                    !record.geospatialObjectCode || 
                    !record.geospatialObjectName || 
                    !record.geospatialObjectType ||
                    !record.geospatialObjectSubtype
                ){
                    let errMsg = `Required parameters are missing. Ensure that geospatialObjectCode, geospatialObjectName, geospatialObjectType, and geospatialObjectSubtype fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */
                
                geospatialObjectCode = record.geospatialObjectCode
                geospatialObjectName = record.geospatialObjectName
                geospatialObjectType = record.geospatialObjectType.trim()
                geospatialObjectSubtype = record.geospatialObjectSubtype.trim()

                // check if geospatialObjectCode is set
                if (record.geospatialObjectCode !== undefined) {
                    geospatialObjectCode = record.geospatialObjectCode

                    // Validate geospatialObjectCode if does does not exist
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if geospatial object code is existing return 1
                            SELECT
                            CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                            END AS count
                            FROM
                            place.geospatial_object
                            WHERE
                            is_void = FALSE AND
                            geospatial_object_code = $$${geospatialObjectCode}$$
                        )
                    `
                    validateCount += 1
                }

                // validation for geospatial object type
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'GEOSPATIAL_OBJECT_TYPE' AND
                            scaleValue.value ILIKE $$${geospatialObjectType}$$
                    )
                `
                validateCount += 1

                // validation for geospatial object sub type
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if entry status is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'GEOSPATIAL_OBJECT_SUBTYPE' AND
                            scaleValue.value ILIKE $$${geospatialObjectSubtype}$$
                    )
                `
                validateCount += 1

                description = (record.description !== undefined) ? record.description : null
                altitude = (record.altitude !== undefined) ? record.altitude : null
                geospatialCoordinates = (record.geospatialCoordinates !== undefined) ? record.geospatialCoordinates : null

                // validate parent geospatial object ID
                if (record.parentGeospatialObjectId !== undefined) {
                    parentGeospatialObjectId = record.parentGeospatialObjectId

                    if (!validator.isInt(parentGeospatialObjectId)) {
                        let errMsg = `Invalid format, parent geospatial object ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate parent geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if parent geospatial object is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object
                            WHERE 
                                is_void = FALSE AND
                                id = ${parentGeospatialObjectId}
                        )
                    `
                    validateCount += 1
                }

                // validate root geospatial object ID
                if (record.rootGeospatialObjectId !== undefined) {
                    rootGeospatialObjectId = record.rootGeospatialObjectId

                    if (!validator.isInt(rootGeospatialObjectId)) {
                        let errMsg = `Invalid format, root geospatial object ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate root geospatial object ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if root geospatial object is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                place.geospatial_object
                            WHERE 
                                is_void = FALSE AND
                                id = ${rootGeospatialObjectId}
                        )
                    `
                    validateCount += 1
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [
                    geospatialObjectCode, geospatialObjectName, geospatialObjectType, geospatialObjectSubtype, 
                    geospatialCoordinates, altitude, description, parentGeospatialObjectId, rootGeospatialObjectId, personDbId
                ]

                geospatialObjectValuesArray.push(tempArray)
            }

            // Create geospatial object record
            let geospatialObjectsQuery = format(`
                INSERT INTO
                    place.geospatial_object (
                        geospatial_object_code, geospatial_object_name, geospatial_object_type, geospatial_object_subtype, 
                        geospatial_coordinates, altitude, description, parent_geospatial_object_id,
                        root_geospatial_object_id, creator_id
                    )
                VALUES 
                    %L
                RETURNING id`, geospatialObjectValuesArray
            )

            let geospatialObjects = await sequelize.query(geospatialObjectsQuery,{
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            for (geospatialObject of geospatialObjects[0]) {
                geospatialObjectDbId = geospatialObject.id

                // Return the geospatial object info
                let array = {
                    geospatialObjectDbId: geospatialObjectDbId,
                    recordCount: 1,
                    href: geospatialObjectUrlString + '/' + geospatialObjectDbId
                }
                resultArray.push(array)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}