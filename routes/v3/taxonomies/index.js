/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let userValidator = require('../../../helpers/person/validator.js')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let logger = require('../../../helpers/logger')

module.exports = {
    /**
     * Implementation for GET v3/taxonomies
     * 
     * @param {*} req Request parameters 
     * @param {*} res Response
     */
    get: async function(req, res, next) {
        // initialize default variables
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort

        let count = 0
        let conditionString = ''
        let orderString = ''

        // if has sort order
        if(sort != null){
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // if has taxon_id value
        if(params.taxonId !== undefined){
            let taxonId = params.taxonId

            if(params.taxonId == null || params.taxonId == ''){
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return              
            }

            // check if has taxonomy record with taxonId
            let checkRecordQuery = `
                SELECT
                    COUNT(1)
                FROM
                    germplasm.taxonomy taxonomy
                WHERE
                    taxonomy.is_void = FALSE AND
                    taxonomy.taxon_id = $$${taxonId}$$
            `

            let checkRecordCount = await sequelize.query(checkRecordQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err =>{
                return null
            })

            if(checkRecordCount == undefined || checkRecordCount == null || 
                checkRecordCount.length < 1){
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            if(checkRecordCount[0] != undefined && checkRecordCount[0].count < 1){
                let errMsg = 'The taxonomy record being retrieved does not exist.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            let parameters = {
                taxonId: taxonId
            }

            conditionString = await processQueryHelper.getFilter(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Main retrieval query
        let sourceQuery = `
            SELECT 
                taxonomy.id AS "taxonomyDbId",
                taxonomy.taxon_id AS "taxonId",
                taxonomy.taxonomy_name AS "taxonName",
                taxonomy.crop_id AS "cropDbId",
                crop.crop_code AS "cropCode",
                crop.crop_name AS "cropName",
                taxonomy.creator_id AS "creatorDbId",
                (
                    SELECT
                        person.person_name AS "creator"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = taxonomy.creator_id
                ),
                taxonomy.creation_timestamp AS "creationTimestamp",
                taxonomy.modifier_id AS "modifierDbId",
                (
                    SELECT
                        person.person_name AS "modifier"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = taxonomy.modifier_id
                ),
                taxonomy.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.taxonomy taxonomy
                LEFT JOIN tenant.crop crop ON crop.id = taxonomy.crop_id AND crop.is_void = FALSE 
            WHERE
                taxonomy.is_void = FALSE
        `

        let mainQuery = await processQueryHelper.getFinalSqlQuery(
            sourceQuery,
            conditionString,
            orderString
        )

        let taxonomiesQuery = `
            WITH mainQuery AS (
                ${mainQuery}
            ),
            countQuery AS (
                SELECT
                    count(*) AS "totalCount"
                FROM
                    mainQuery
            )
            SELECT
                mainQuery.*,
                countQuery."totalCount"
            FROM
                countQuery,
                mainQuery
        `

        // Retrieve the records from the database
        let taxonomies = await sequelize.query(taxonomiesQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return            
        })

        if(await taxonomies == undefined || await taxonomies.length == 0){
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }
        else{
            // Build final count query
            taxonomiesCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(sourceQuery, conditionString, orderString)

            taxonomiesCount = await sequelize.query(taxonomiesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = taxonomiesCount[0].count
        }

        res.send(200, {
            rows: taxonomies,
            count: count
        })
        return        

    },

    // Endpoint for creating a new taxonomy record
    post: async function(req, res, next) {
        // Set defaults
        let resultArray = []

        let taxonomyDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if user is Admin
        let isAdmin = await userValidator.isAdmin(personDbId)

        if (!isAdmin) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401028)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let taxonomyUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/taxonomies'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let taxonomyValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Set defaults
                let taxonDbId = null
                let taxonomyName = null
                let description = null
                let cropDbId = null

                // Check if required columns are in the request body
                if (
                    !record.taxonDbId || !record.taxonomyName ||
                    !record.cropDbId
                ) {
                    let errMsg = `Required parameters are missing. Ensure that taxonDbId, taxonomyName, and cropDbId fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return 
                }

                if (record.cropDbId !== undefined) {
                    cropDbId = record.cropDbId

                    if (!validator.isInt(cropDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400051)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate crop ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if crop is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.crop crop
                        WHERE 
                            crop.is_void = FALSE AND
                            crop.id = ${cropDbId}
                        )
                    `
                    validateCount += 1
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                taxonDbId = record.taxonDbId
                taxonomyName = record.taxonomyName
                description = (record.description !== undefined) ? record.description : null
                cropDbId = record.cropDbId

                // get values
                let tempArray = [
                    taxonDbId, taxonomyName, description, cropDbId, personDbId
                ]

                taxonomyValuesArray.push(tempArray)
            }

            // Create taxonomy record
            let taxonomiesQuery = format(`
                INSERT INTO
                    germplasm.taxonomy (
                        taxon_id, taxonomy_name, description, crop_id, creator_id
                    )
                VALUES
                    %L
                RETURNING id`, taxonomyValuesArray
            )

            let taxonomies = await sequelize.query(taxonomiesQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            for (taxonomy of taxonomies[0]) {
                taxonomyDbId = taxonomy.id

                let taxonomyRecord = {
                    taxonomyDbId: taxonomyDbId,
                    recordCount: 1,
                    href: taxonomyUrlString + '/' + taxonomyDbId
                }

                resultArray.push(taxonomyRecord)
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, 'POST taxonomies: ' + JSON.stringify(err), 'error')

            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }    
    }
}