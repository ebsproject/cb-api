/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY "cropProgram".id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn',
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray,
        )

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }

    // Build the base retrieval query
    let cropProgramsQuery = null

    // Check if the client specified values for fields
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])
        
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        cropProgramsQuery = knex.select(selectString)
      } else {
        cropProgramsQuery = knex.column(parameters['fields'].split('|'))
      }

      cropProgramsQuery += `
        FROM
          tenant.crop_program "cropProgram"
        LEFT JOIN
          tenant.organization organization ON organization.id = "cropProgram".organization_id
        LEFT JOIN
          tenant.crop crop ON crop.id = "cropProgram".crop_id
        LEFT JOIN
          tenant.person creator ON creator.id = "cropProgram".creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = "cropProgram".modifier_id
        WHERE
          "cropProgram".is_void = FALSE
        ${addedOrderString}
      `
    } else {
      cropProgramsQuery = `
        SELECT
          "cropProgram".id AS "cropProgramDbId",
          "cropProgram".crop_program_code AS "cropProgramCode",
          "cropProgram".crop_program_name AS "cropProgramName",
          "cropProgram".description,
          organization.id AS "organizationDbId",
          organization.organization_code AS "organizationCode",
          organization.organization_name AS "organizationName",
          crop.id AS "cropDbId",
          crop.crop_code AS "cropCode",
          crop.crop_name AS "cropName",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          "cropProgram".creation_timestamp AS "creationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier",
          "cropProgram".modification_timestamp AS "modificationTimestamp"
        FROM
          tenant.crop_program "cropProgram"
        LEFT JOIN
          tenant.organization organization ON organization.id = "cropProgram".organization_id
        LEFT JOIN
          tenant.crop crop ON crop.id = "cropProgram".crop_id
        LEFT JOIN
          tenant.person creator ON creator.id = "cropProgram".creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = "cropProgram".modifier_id
        WHERE
          "cropProgram".is_void = FALSE
        ${addedOrderString}
      `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let cropProgramsFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        cropProgramsQuery,
        conditionString,
        orderString
      )

    let cropPrograms = await sequelize
      .query(cropProgramsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await cropPrograms == undefined || await cropPrograms.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let cropProgramsCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        cropProgramsQuery,
        conditionString,
        orderString
      )

    let cropProgramsCount = await sequelize
      .query(cropProgramsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })

    count = cropProgramsCount[0].count

    res.send(200, {
      rows: cropPrograms,
      count: count
    })
    return
  }
}