/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for voiding an existing experiment-group record
    // DELETE /v3/experiment-group/:id
    delete: async function (req, res, next) {
        
        // Retrieve experiment group ID
        let experimentGroupDbId = req.params.id

        if (!validator.isInt(experimentGroupDbId)) {
            let errMsg = `Invalid format, experiment group ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let experimentGroupQuery = `
                SELECT
                    creator.id AS "creatorDbId"
                FROM
                    experiment.experiment_group eg
                LEFT JOIN
                    tenant.person creator ON creator.id = eg.creator_id
                WHERE
                    eg.id = ${experimentGroupDbId} AND
                    eg.is_void = FALSE
            `

            let experimentGroup = await sequelize.query(experimentGroupQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
        
            if (
                await experimentGroup == undefined ||
                await experimentGroup.length < 1
            ) {
                let errMsg = `The experiment group you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let deleteExperimentGroupQuery = format(`
                UPDATE
                    experiment.experiment_group
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${experimentGroupDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${experimentGroupDbId} AND
                    is_void = FALSE
            `)

            await sequelize.query(deleteExperimentGroupQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { experimentGroupDbId: experimentGroupDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}