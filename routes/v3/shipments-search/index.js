/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require("../../../config/sequelize");
let { knex } = require("../../../config/knex");
let processQueryHelper = require("../../../helpers/processQuery/index");
let errors = require("restify-errors");
let errorBuilder = require("../../../helpers/error-builder");

module.exports = {
  post: async (req, res, next) => {
    let limit = req.paginate.limit;
    let offset = req.paginate.offset;
    let sort = req.query.sort;
    let params = req.query;
    let count = 0;
    let orderString = "";
    let conditionString = "";
    let addedConditionString = "";
    let addedDistinctString = "";
    let addedOrderString = `
            ORDER BY
                shipment.id
        `;
    let parameters = {};
    parameters["distinctOn"] = "";

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters["fields"] = req.body.fields;
      }

      if (req.body.distinctOn != undefined) {
        parameters["distinctOn"] = req.body.distinctOn;
        addedDistinctString = await processQueryHelper.getDistinctString(
          parameters["distinctOn"]
        );

        parameters["distinctOn"] = `"${parameters["distinctOn"]}"`;
        addedOrderString = `
                    ORDER BY
                    ${parameters["distinctOn"]}
                `;
      }
      // Set columns to be excluded in parameters for filtering
      let excludedParametersArray = ["fields", "distinctOn"];
      // Get the filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      );

      if (conditionString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    // Build the base retrieval query
    let shipmentsQuery = null;

    // Check if the client specified values for the fields
    if (parameters["fields"] != null) {
      if (parameters["distinctOn"]) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters["fields"]
        );
        let selectString = knex.raw(`${addedConditionString} ${fieldsString}`);
        shipmentsQuery = knex.select(selectString);
      } else {
        shipmentsQuery = knex.column(parameters["fields"].split("|"));
      }
      shipmentsQuery += `
        FROM
            inventory.shipment shipment
        LEFT JOIN
            tenant.program program ON shipment.program_id = program.id AND
            program.is_void = false
        
        WHERE
            shipment.is_void = FALSE
            ${addedOrderString}
            `;
    } else {
      shipmentsQuery = `
        SELECT
            ${addedDistinctString}
            shipment.id AS "shipmentDbId",
            shipment.program_id AS "programDbId",
            shipment.shipment_code AS "shipmentCode",
            shipment.shipment_name AS "shipmentName",
            shipment.shu_reference_number AS "shuReferenceNumber",
            shipment.shipment_status AS "shipmentStatus",
            shipment.shipment_tracking_status AS "shipmentTrackingStatus",
            shipment.shipment_transaction_type AS "shipmentTransactionType",
            shipment.shipment_type AS "shipmentType",
            shipment.shipment_purpose AS "shipmentPurpose",
            shipment.entity_id AS "entityId",
            shipment.material_type AS "materialType",
            shipment.material_subtype AS "materialSubtype",
            shipment.total_item_count AS "totalItemCount",
            shipment.total_package_count AS "totalPackageCount",
            shipment.total_package_weight AS "totalPackageWeight",
            shipment.package_unit AS "packageUnit",
            shipment.document_generated_date AS "documentGeneratedDate",
            shipment.document_signed_date AS "documentSignedDate",
            shipment.authorized_signatory AS "authorizedSignatory",
            shipment.shipped_date AS "shippedDate",
            shipment.airway_bill_number AS "airwayBillNumber",
            shipment.received_date AS "receivedDate",
            shipment.processor_name AS "processorName",
            shipment.processor_id AS "processorId",
            shipment.sender_name AS "senderName",
            shipment.sender_email AS "senderEmail",
            shipment.sender_address AS "senderAddress",
            shipment.sender_institution AS "senderInstitution",
            shipment.sender_facility AS "senderFacility",
            shipment.sender_country AS "senderCountry",
            shipment.recipient_type AS "recipientType",
            shipment.recipient_name AS "recipientName",
            shipment.recipient_email AS "recipientEmail",
            shipment.recipient_address AS "recipientAddress",
            shipment.recipient_institution AS "recipientInstitution",
            shipment.recipient_facility AS "recipientFacility",
            shipment.recipient_country AS "recipientCountry",
            shipment.requestor_name AS "requestorName",
            shipment.requestor_email AS "requestorEmail",
            shipment.requestor_address AS "requestorAddress",
            shipment.requestor_institution AS "requestorInstitution",
            shipment.requestor_country AS "requestorCountry",
            shipment_remarks AS "shipmentRemarks",
            shipment.tracking_remarks AS "trackingRemarks",
            shipment.dispatch_remarks AS "dispatchRemarks",
            shipment.acknowledgment_remarks AS "acknowledgmentRemarks",
            shipment.remarks AS "remarks",
            shipment.creator_id AS "creatorDbId",
            shipment.modifier_id AS "modifierDbId",
            shipment.notes  AS "notes",
            program.program_code AS "programCode",
            program.program_name AS "programName",
            (Select count(id) AS "totalEntries" from inventory.shipment_item where shipment_id=shipment.id AND shipment_item_status != 'cancel' AND is_void = 'false'),
            (Select sum(shipment_item_weight) AS "totalItemWeight" from inventory.shipment_item where shipment_id=shipment.id AND shipment_item_status != 'cancel' AND is_void = 'false'),
            (Select sum(package_count) AS "totalPackageCount" from inventory.shipment_item where shipment_id=shipment.id AND shipment_item_status != 'cancel' AND is_void = 'false')
        FROM
            inventory.shipment shipment
        LEFT JOIN
            tenant.program program ON shipment.program_id = program.id AND
            program.is_void = false
        WHERE
            shipment.is_void = FALSE
            ${addedOrderString}
            `;
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort);
      if (orderString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    // Generate the final SQL query
    let shipmentsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      shipmentsQuery,
      conditionString,
      orderString
    );

    let shipments = await sequelize
      .query(shipmentsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        },
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      });

    if ((await shipments) == undefined || (await shipments.length) < 1) {
      res.send(200, {
        rows: [],
        count: 0,
      });
      return;
    }

    let shipmentsCountFinalSqlQuery =
      await processQueryHelper.getCountFinalSqlQuery(
        shipmentsQuery,
        conditionString,
        orderString
      );

    let shipmentsCount = await sequelize
      .query(shipmentsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    count = shipmentsCount[0].count;

    res.send(200, {
      rows: shipments,
      count: count,
    });
    return;
  },
};
