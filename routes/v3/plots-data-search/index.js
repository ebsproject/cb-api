/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let Sequelize = require('sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset  = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the excluded columns
      let excludedParametersArray = ['fields']
      // Get filter conditions
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      )
      
      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let plotsDataQuery = null
    // Check if the client specified values for the field/s
    if (parameters['fields']) {
      plotsDataQuery = knex.column(parameters['fields'].split('|'))
      plotsDataQuery += `
        FROM
          experiment.plot_data "plotData"
        LEFT JOIN
          experiment.plot plot ON "plotData".plot_id = plot.id
        LEFT JOIN
          master.variable variable ON "plotData".variable_id = variable.id
        LEFT JOIN
          tenant.person creator ON "plotData".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "plotData".modifier_id = modifier.id
        ORDER BY
          "plotData".id
      `
    } else {
      plotsDataQuery = `
        SELECT
          "plotData".id AS "plotDataDbId",
          "plotData".plot_id AS "plotDbId",
          "plotData".variable_id AS "variableDbId",
          "plotData".data_value AS "dataValue",
          "plotData".data_qc_code AS "dataQcCode",
          "plotData".transaction_id AS "transactionDbId",
          "plotData".collection_timestamp AS "collectionTimestamp",
          "plotData".creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          "plotData".modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          experiment.plot_data "plotData"
        LEFT JOIN
          experiment.plot plot ON "plotData".plot_id = plot.id
        LEFT JOIN
          master.variable variable ON "plotData".variable_id = variable.id
        LEFT JOIN
          tenant.person creator ON "plotData".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "plotData".modifier_id = modifier.id
        ORDER BY
          "plotData".id
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    plotsDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      plotsDataQuery,
      conditionString,
      orderString
    )
    // Retrieve the plots data records
    let plotsData = await sequelize
      .query(plotsDataFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await plotsData == undefined || await plotsData.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get the final count of the plots data records
      let plotsDataCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          plotsDataQuery,
          conditionString,
          orderString
        )

      let plotsDataCount = await sequelize
        .query(plotsDataCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })
      
      count = plotsDataCount[0].count
    }

    res.send(200, {
      rows: plotsData,
      count: count
    })
    return
  }
}