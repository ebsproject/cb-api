/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let experimentHelper = require('../../../helpers/experiment/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let userValidator = require('../../../helpers/person/validator.js')
const logger = require('../../../helpers/logger')

const endpoint = 'experiments-search'

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let count = 0
    let conditionString = ''
    let addedDistinctString = ''
    let addedConditionString = ''
    let addedColumnString = ''
    let orderString = ''
    let ownedConditionString = ''
    let entryCountSql = ''
    let parameters = {}
    let entryCountFlag = false
    parameters['distinctOn'] = ''
    let permissionCond = ''
    let programCond = ``
    let programIdsString = 0

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }

      let excludedParametersArray = ['fields', 'distinctOn']

      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray,
      )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))

        return
      }

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(
            parameters['distinctOn'],
          )
        parameters['distinctOn'] = `"${parameters['distinctOn']}",`
      }
    }

    let experimentsQuery = null

    // Retrieve the user ID of the client from the access token
    let userId = await tokenHelper.getUserId(req);

    // if show only shared experiments
    if (params.isOwned !== undefined) {

      if (userId == null) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401002)
        res.send(new errors.BadRequestError(errMsg))

        return
      }

      ownedConditionString = ` AND (creator.id = ${userId} OR person.id = ${userId}) `
    }

    // check if user is admin or not
    let isAdmin = await userValidator.isAdmin(userId)

    // if not admin, retrieve only experiments user have access to
    if(!isAdmin){
        // if collaborator program code is set, filter occurrences shared to the collaborator program
        if(params.collaboratorProgramCode != null){
          programCond = ` AND program.program_code = '${params.collaboratorProgramCode}'`
        }

        // get all programs that user belongs to
        let getProgramsQuery =  `
        SELECT 
            program.id 
        FROM 
            tenant.program program,
            tenant.program_team pt,
            tenant.team_member tm
        WHERE
            tm.person_id = ${userId}
            AND pt.team_id = tm.team_id
            AND program.id = pt.program_id
            AND tm.is_void = FALSE
            AND pt.is_void = FALSE
            AND program.is_void = FALSE
            ${programCond}
        `
        let programIds = await sequelize.query(getProgramsQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let accessDataQuery = ''
        // if program does not exist
        if(programIds.length == 0){
          accessDataQuery = `occurrence.access_data @> $$\{"program": {"0":{}}}$$`
        }

        // build program permission query
        for (let programObj of programIds) {
            if (accessDataQuery != ``) {
                accessDataQuery += ` OR `
            }

            programIdsString += ` , ${programObj.id}`

            accessDataQuery += `occurrence.access_data #> $$\{program,${programObj.id}}$$ is not null`

        }

        // include occurrences shared to the user
        let userQuery = `occurrence.access_data #> $$\{person,${userId}}$$ is not null`
        if (accessDataQuery != ``) {
            accessDataQuery += ` OR ` + userQuery
        }

        if(accessDataQuery != ``){
            accessDataQuery = ` AND (${accessDataQuery})`
        }

        // retrieve all occurrences user have access to
        let occurrenceQuery = `
          SELECT
            distinct on (occurrence.experiment_id) occurrence.experiment_id
          FROM
            experiment.occurrence
          WHERE
            occurrence.is_void = FALSE
            ${accessDataQuery}
        `
        let experimentIdsRes = await sequelize.query(occurrenceQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let experimentIdsArr = experimentIdsRes.map(x => x['experiment_id'])
        let experimentIds = experimentIdsArr.join(',')

        if(experimentIds == ''){
          experimentIds = '0'
        }

        permissionCond = ` AND ( experiment.id in (${experimentIds}) OR experiment.program_id in (${programIdsString}) )`
    }
    
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldValues = parameters['fields'].split('|')
        let fieldValuesString = ''
        for (let value of fieldValues) {
          fieldValuesString += `${value}, `
        }
        // Clean the fieldValuesString
        fieldValuesString = fieldValuesString.replace(/,\s*$/, "");
        let x = knex.raw(`${addedDistinctString} ${fieldValuesString}`)

        experimentsQuery = knex.select(x)
      } else {
        if(parameters['fields'].includes('entryCount')){
          entryCountSql = `
          (
            SELECT
              count(e.id)
            FROM
              experiment.entry_list el,
              experiment.entry e
            WHERE
              el.experiment_id = experiment.id AND
              e.entry_list_id = el.id AND
              e.is_void = FALSE
          )::int AS "entryCount"`
          entryCountFlag = true
        }
        experimentsQuery = knex.column(parameters['fields'].split('|'))
      }

      experimentsQuery += `
        FROM
          experiment.experiment experiment
        LEFT JOIN
          master.item item ON item.id = experiment.data_process_id
        LEFT JOIN
          experiment.experiment_plan experiment_plan ON experiment_plan.id = experiment.experiment_plan_id
        LEFT JOIN
          tenant.pipeline pipeline ON pipeline.id = experiment.pipeline_id
        JOIN
          tenant.program program ON program.id = experiment.program_id
        LEFT JOIN
          tenant.project project ON project.id = experiment.project_id
        JOIN
          tenant.season season ON season.id = experiment.season_id
        JOIN
          tenant.stage stage ON stage.id = experiment.stage_id
        LEFT JOIN
          tenant.person person ON person.id = experiment.steward_id
        JOIN
          tenant.person creator ON creator.id = experiment.creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = experiment.modifier_id
        WHERE
          experiment.is_void = false
          ${ownedConditionString}
          ${permissionCond}
        ORDER BY
          ${parameters['distinctOn']}
          experiment.id DESC
      `

    } else {
      experimentsQuery = await experimentHelper.getExperimentQuerySql(
        addedConditionString,
        addedColumnString,
        addedDistinctString,
        parameters['distinctOn'],
        ownedConditionString,
        permissionCond
      )
    }

    if(entryCountFlag){
      experimentsQuery = experimentsQuery.replace('"entryCount"', entryCountSql)
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)

      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400057)
        res.send(new errors.BadRequestError(errMsg))

        return
      }
    }

    // Generate the final SQL query
    experimentsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      experimentsQuery,
      conditionString,
      orderString
    )

    let experiments = await sequelize.query(experimentsFinalSqlQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        limit: limit,
        offset: offset
      }
    }).catch(async err => {
      await logger.logFailingQuery(endpoint, 'SELECT', err)
    })

    if (experiments == undefined) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))

      return
    } else if (experiments.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })

      return
    } else {
      let experimentsCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          experimentsQuery,
          conditionString,
          orderString
        )

      let experimentCount = await sequelize
        .query(experimentsCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          await logger.logFailingQuery(endpoint, 'SELECT', err)
        })

      if (experimentCount === undefined) {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))

        return
      } else if (experimentCount.length < 1) {
        res.send(200, {
          rows: [],
          count: 0
        })

        return
      }

      count = experimentCount[0].count
    }

    res.send(200, {
      rows: await experiments,
      count: count
    })

    return
  }
}