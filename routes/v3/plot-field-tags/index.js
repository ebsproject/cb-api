/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')
const processQueryHelper = require('../../../helpers/processQuery/index')
const validator = require('validator')

module.exports = {
    /**
     * Retrieve plots for field tags
     * GET /v3/plot-field-tags
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let params = req.query
        let sort = req.query.sort
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = {}
        const endpoint = 'plot-field-tags'

        // Check if searchParams need to be updated
        if (params.occurrenceDbId) {
            const occurrenceDbIdArr = params.occurrenceDbId.split(',')
            let hasInvalidValue = false

            // Check if an occurrence ID is NOT an integer
            if (occurrenceDbIdArr.length > 0) {
                await occurrenceDbIdArr.forEach(async (id) => {
                    if(!validator.isInt(id)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                        res.send(new errors.BadRequestError(errMsg))
                        hasInvalidValue = true
                    }
                })
            }

            if (hasInvalidValue) return

            searchParams['occurrenceDbId'] = occurrenceDbIdArr.join('|')
        }

        // Get the filter condition
        conditionString = await processQueryHelper.getFilter(searchParams)

        if (conditionString != undefined && conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))

            return
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString === undefined || orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Build query for geting field tag records
        let fieldTagQuery = `
            SELECT
                pi.plot_id "plotDbId",
                plot.plot_code AS "plotCode",
                plot.plot_number AS "plotNumber",
                plot.field_x AS "fieldX",
                plot.field_y AS "fieldY",
                pi.entry_id AS "entryDbId",
                pi.entry_code AS "entryCode",
                pi.entry_number AS "entryNumber",
                e.id AS "experimentDbId",
                e.experiment_code AS "experimentCode",
                e.experiment_name AS "experimentName",
                e.experiment_year AS "experimentYear",
                season.id AS "experimentSeasonDbId",
                season.season_code AS "experimentSeasonCode",
                season.season_name AS "experimentSeason",
                g.id AS "germplasmDbId",
                g.germplasm_code AS "germplasmCode",
                g.designation AS "germplasmName",
                g.parentage AS "germplasmParentage",
                o.id AS "occurrenceDbId",
                o.occurrence_code AS "occurrenceCode",
                o.occurrence_name AS "occurrenceName",
                o.occurrence_number AS "occurrenceNumber",
                pr.id AS "programDbId",
                pr.program_code AS "programCode",
                pr.program_name AS "programName",
                (
                    SELECT
                        data_value
                    FROM
                        experiment.occurrence_data od
                    WHERE
                        od.occurrence_id = o.id
                        AND variable_id = (
                            SELECT
                                id
                            FROM
                                master.variable
                            WHERE
                                abbrev = 'SEEDING_DATE_CONT'
                        )
                        AND od.is_void = FALSE
                    LIMIT 1
                ) AS "seedingDate",
                site.id AS "siteDbId",
                site.geospatial_object_code AS "siteCode",
                site.geospatial_object_name AS "site"
            FROM 
                experiment.plot plot 
                JOIN
                    experiment.planting_instruction pi ON pi.plot_id = plot.id
                JOIN
                    germplasm.germplasm g ON g.id = pi.germplasm_id
                LEFT JOIN
                    experiment.occurrence o ON o.id = plot.occurrence_id
                JOIN
                    experiment.experiment e ON e.id = o.experiment_id
                LEFT JOIN
                    place.geospatial_object site ON site.id = o.site_id
                JOIN
                    tenant.season season on season.id = e.season_id
                JOIN
                    tenant.program pr ON pr.id = e.program_id
            WHERE
                o.is_void = FALSE
                AND e.is_void = FALSE
                AND pi.is_void = FALSE
                AND g.is_void = FALSE
                AND plot.is_void = FALSE
        `

        // Finalize query build
        let fieldTagsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            fieldTagQuery,
            conditionString,
            orderString
        )

        // Retrieve field tags from the database   
        let fieldTags = await sequelize
            .query(fieldTagsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)

                return undefined
            })

        // If sequelize error was discovered
        if (fieldTags === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        // If no records were retrieved
        if (fieldTags.length < 1) {
            let errMsg = 'Resource not found. The plot field tag records you have requested for do not exist.'
            res.send(new errors.NotFoundError(errMsg))

            return
        }

        // Build query for getting field tag count
        let fieldTagCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                fieldTagQuery,
                conditionString,
                orderString
            )

        // Get the final count of the field tag records
        let fieldTagCount = await sequelize
            .query(fieldTagCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                
                return undefined
            })

        if (fieldTagCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        count = fieldTagCount[0].count

        res.send(200, {
            rows: fieldTags,
            count: count
        })

        return
    }
}
