/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')
let validator = require('validator')
let errors = require('restify-errors')

module.exports = {
    post: async function (req, res, next) {
        const endpoint = 'list-members-search'

        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        
        let parameters = {}
        parameters['distinctOn'] = ''
        
        let members = []
        let count = 0

        let listMembersQuery = ''
        let errMsg

        try{
            if(req.body){
                if (req.body.fields != null) {
                    parameters['fields'] = req.body.fields
                }
    
                // Set the columns to be excluded in parameters for filtering
                let excludedParametersArray = ['fields', 'distinctOn']
                
                let dataDbId
                if(req.body.dataDbId != undefined && req.body.dataDbId != null){
                    dataDbId = req.body.dataDbId

                    // check if dataDbId is empty
                    if(dataDbId == ''){
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // check if dataDbId is not valid format
                    if(dataDbId.includes('equals')){
                        dataDbId = dataDbId.replaceAll('equals',"").trim()
                    }

                    if (!validator.isInt(dataDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return 
                    }
                }
    
                // Get filter condition
                conditionString = await processQueryHelper.getFilter(req.body,excludedParametersArray)
    
                if (conditionString.includes('invalid')) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
    
                if (req.body.distinctOn != undefined) {
                    parameters['distinctOn'] = req.body.distinctOn
                    addedDistinctString = await processQueryHelper.getDistinctString(
                            parameters['distinctOn']
                        )
                    parameters['distinctOn'] = `"${parameters['distinctOn']}",`
                }
            }
    
            // check for specified fields parameters
            if(parameters['fields']){
                if (parameters['distinctOn']) {
                    let fieldsString = await processQueryHelper.getFieldValuesString(
                            parameters['fields']
                        )
    
                    let selectString = knex.raw(`${fieldsString}`)
                    listMembersQuery = knex.select(selectString)
                } 
                else {
                    listMembersQuery = knex.column(parameters['fields'].split('|'))
                }
            }
            else {
                listMembersQuery = `
                    SELECT
                        list_member.id AS "listMemberDbId",
                        list_member.data_id AS "dataDbId",
                        list_member.order_number AS "orderNumber",
                        list_member.creation_timestamp AS "creationTimestamp",
                        list_member.display_value AS "displayValue",
                        list_member.is_active AS "isActive",
                        list_member.remarks
                `
            }
    
            // add FROM statements
            listMembersQuery += `
                FROM    
                    platform.list_member list_member
                WHERE
                    list_member.is_void = FALSE
                    ${addedConditionString}
                ORDER BY
                    ${parameters['distinctOn']}
                    list_member.order_number
            `
    
            // Parse the sort parameters
            if (sort != null) {
                orderString = await processQueryHelper.getOrderString(sort)
                if (orderString.includes('invalid')) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }
    
            // Generate the final SQL query
            let listsMemberQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
                listMembersQuery,
                conditionString,
                orderString
            )
      
            // build final query with count
            let listMemberFinalQuery = await processQueryHelper.getFinalTotalCountQuery(
                listsMemberQuery,
                orderString
            )
    
            // retrieve list member records
            let membersResults = await sequelize.query(listMemberFinalQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                
                return undefined
            })
    
            if (membersResults == undefined || membersResults.length < 1) {
                res.send(200, {
                    rows: [],
                    count: 0
                })
                return
            }

            // Get count
            count = membersResults[0].totalCount
    
            res.send(200, {
                rows: membersResults,
                count: count
            })
            
            return
        }
        catch (e){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}