/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, lists } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let { knex } = require('../../../config/knex')

module.exports = {
  /**
   * Advanced search for suppression rules
   * POST v3/suppression-rules-search
   * @param {*} req 
   * @param {*} res 
   */
  post: async function (req, res) {
    // Set defaults
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let parameters = {}

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']
      // Get filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let suppressionRuleQuery = null

    // Check if the client specified values for the fields
    if (parameters['fields'] != null) {
      suppressionRuleQuery = knex.column(parameters['fields'].split('|'))
      suppressionRuleQuery += `
        FROM
          data_terminal.suppression_rule rule
        LEFT JOIN
          master.variable target_variable ON target_variable.id = rule.target_variable_id
        LEFT JOIN
          master.variable factor_variable ON factor_variable.id = rule.factor_variable_id
        LEFT JOIN
          tenant.person creator ON rule.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON rule.modifier_id = modifier.id
        WHERE
          rule.is_void = FALSE 
      `
    } else {
      suppressionRuleQuery = `
        SELECT
          rule.id::text as "suppressionRuleDbId",
          rule.transaction_id::text as "transactionDbId",
          rule.target_variable_id::text "targetVariableDbId",
          target_variable.abbrev "targetVariableAbbrev",
          target_variable.label "targetVariableLabel",
          rule.factor_variable_id::text "factorVariableDbId",
          factor_variable.abbrev "factorVariableAbbrev",
          factor_variable.label "factorVariableLabel",
          rule.value,
          rule.status,
          rule.conjunction,
          rule.operator,
          rule.entity_id as "entityDbId",
          rule.remarks,
          rule.notes,
          rule.creation_timestamp as "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name as creator,
          rule.modification_timestamp as "modificationTimestamp",
          modifier.id as "modifierDbId",
          modifier.person_name as modifier
        FROM
          data_terminal.suppression_rule rule
        LEFT JOIN
          master.variable target_variable ON target_variable.id = rule.target_variable_id
        LEFT JOIN
          master.variable factor_variable ON factor_variable.id = rule.factor_variable_id
        LEFT JOIN
          tenant.person creator ON rule.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON rule.modifier_id = modifier.id
        WHERE
          rule.is_void = FALSE 
      `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    suppressionRuleFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      suppressionRuleQuery,
      conditionString,
      orderString
    )

    // Retrieve the background process records
    let rules = await sequelize
      .query(suppressionRuleFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await rules == undefined || await rules.length < 1) {
      rules = []
      count = 0
    } else {
      // Get the final count of the background records retrieved
      let suppressionRuleCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          suppressionRuleQuery,
          conditionString,
          orderString
        )
      let rulesCount = await sequelize
        .query(
          suppressionRuleCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = rulesCount[0].count
    }

    res.send(200, {
      rows: rules,
      count: count
    })
    return
  }
}