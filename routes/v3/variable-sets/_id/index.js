/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { variableSet } = require('../../../../config/sequelize')
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

let validator = require('validator')

module.exports = {

    // Implementation of GET call for /v3/variable-sets/:id
    get: async function (req, res, next) {

        let variableSetDbId = req.params.id

        if (!validator.isInt(variableSetDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400079)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let variableSetQuery = `
                SELECT
                    "variableSet".id AS "variableSetDbId",
                    "variableSet".abbrev,
                    "variableSet".name,
                    "variableSet".description,
                    "variableSet".display_name AS "displayName",
                    "variableSet".remarks,
                    "variableSet".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    "variableSet".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier,
                    "variableSet".ontology_id AS "ontologyId",
                    "variableSet".varsetusage,
                    "variableSet".bibref,
                    "variableSet".parvarsetid,
                    "variableSet".usage,
                    "variableSet".access_type AS "accessType",
                    (
                        SELECT count("variableSetMember".id) 
                        FROM master.variable_set_member "variableSetMember" 
                        WHERE "variableSetMember".variable_set_id = "variableSet".id 
                        AND "variableSetMember".is_void = false
                    ) as "memberCount"
                FROM 
                    master.variable_set "variableSet"
                LEFT JOIN 
                    tenant.person creator ON "variableSet".creator_id = creator.id
                LEFT JOIN 
                    tenant.person modifier ON "variableSet".modifier_id = modifier.id
                WHERE 
                    "variableSet".is_void = FALSE
                    AND "variableSet".id = (:variableSetDbId)
            `

            // Retrieve variable sets from the database
            let variableSets = await sequelize.query
                (variableSetQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        variableSetDbId: variableSetDbId
                    }
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await variableSets == undefined || await variableSets.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404028)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: variableSets
            })
            return
        }
    }
}
