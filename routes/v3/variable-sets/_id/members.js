/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { variableSet } = require('../../../../config/sequelize')
let { sequelize } = require('../../../../config/sequelize')
let Sequelize = require('sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

  // Implementation of GET call for /v3/variable-sets/:id/members
  get: async function (req, res, next) {
    // Set defaults
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let orderString = ''
    let conditionString = ''
    let count = 0

    let variableSetDbId = req.params.id

    if (!validator.isInt(variableSetDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400079)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    else {
      let variableSetQuery = `
        SELECT
          "variableSet".id AS "variableSetDbId",
          "variableSet".abbrev,
          "variableSet".name,
          "variableSet".description,
          "variableSet".display_name AS "displayName",
          "variableSet".remarks,
          "variableSet".creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS creator,
          "variableSet".modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS modifier,
          "variableSet".ontology_id AS "ontologyId",
          "variableSet".varsetusage,
          "variableSet".bibref,
          "variableSet".parvarsetid,
          "variableSet".usage,
          "variableSet".access_type AS "accessType",
          (
              SELECT count("variableSetMember".id) 
              FROM master.variable_set_member "variableSetMember" 
              WHERE "variableSetMember".variable_set_id = "variableSet".id 
              AND "variableSetMember".is_void = false
          ) as "memberCount"
        FROM 
          master.variable_set "variableSet"
        LEFT JOIN 
          tenant.person creator ON "variableSet".creator_id = creator.id
        LEFT JOIN 
          tenant.person modifier ON "variableSet".modifier_id = modifier.id
        WHERE 
          "variableSet".is_void = FALSE
          AND "variableSet".id = (:variableSetDbId)
      `

      // Retrieve variable sets from the database
      let variableSets = await sequelize.query
        (variableSetQuery, {
          type: sequelize.QueryTypes.SELECT,
          replacements: {
            variableSetDbId: variableSetDbId
          }
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })
       
      // Format results from query into an array
      for (variableSet of variableSets) {
        let variablesQuery = `
          SELECT
            variable.id AS "variableDbId",
            variable.abbrev,
            variable.label,
            variable.name,
            variable.display_name AS "displayName",
            variable.type,
            variable.data_type AS "dataType",
            variable.data_level AS "dataLevel",
            variable.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            variable.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier,
            variable_set_member.order_number AS "orderNumber"
          FROM 
            master.variable_set_member variable_set_member
          LEFT JOIN 
            master.variable variable on variable.id = variable_set_member.variable_id
          LEFT JOIN 
            tenant.person creator ON variable.creator_id = creator.id
          LEFT JOIN 
            tenant.person modifier ON variable.modifier_id = modifier.id
          WHERE 
            variable_set_member.is_void = FALSE
            AND variable.is_void = false
            AND variable_set_member.variable_set_id = `+variableSetDbId+`
          ORDER by variable_set_member.order_number
        `
        // Get filter condition/s for abbrev
        if (params.abbrev !== undefined){
          let parameters = {
            abbrev: params.abbrev
          }
          conditionString = await processQueryHelper.getFilterString(parameters)

          if (conditionString.includes('invalid')){
            let errMsg = await errorBuilder.getError(req.headers.host, 400003)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }
        // Get filter condition/s for sort
        if (sort != null){
          orderString = await processQueryHelper.getOrderString(sort)

          if (orderString.includes('invalid')){
            let errMsg = await errorBuilder.getError(req.headers.host, 400004)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }
        // Build the final query
        variableFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
          variablesQuery,
          conditionString,
          orderString
        )
       
        let variables = await sequelize.query(variableFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT,
          replacements: {
            limit: limit,
            offset: offset
          }
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })
        
        variableSet["members"] = variables
      }

      if (await variableSets == undefined || await variableSets.length < 1) {
        res.send(200, {
          rows: variableSets
        })
        return
      }
      
      res.send(200, {
        rows: variableSets
      })
      return
    }
  }
}
