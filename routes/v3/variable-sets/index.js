/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize, VariableSet } = require('../../../config/sequelize')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token')
let userValidator = require('../../../helpers/person/validator.js')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/variable-sets
    get: async function (req, res, next) {

        // Set defaults 
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''

        let orderString = ''

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let addedConditionString = ''

        // Check if user is an administrator. If yes, don't check the permission of the variable sets.
        if (!await userValidator.isAdmin(userId)) {
            addedConditionString = `
                AND (
                    (
                        "variableSet".creator_id = ` + userId + ` 
                        OR lower("variableSet".access_type) = 'public'
                    )
                    OR
                    (
                        "variableSetAccess".collaborator_id = ` + userId + ` 
                        AND lower("variableSet".access_type) != 'public'
                    )
                )
            `
        }

        let variableSetsQuery = `
            SELECT
                DISTINCT ON ("variableSet".id)
                "variableSet".id AS "variableSetDbId",
                "variableSet".abbrev,
                "variableSet".name,
                "variableSet".description,
                "variableSet".display_name AS "displayName",
                "variableSet".remarks,
                "variableSet".creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                "variableSet".modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier,
                "variableSet".ontology_id AS "ontologyId",
                "variableSet".varsetusage,
                "variableSet".bibref,
                "variableSet".parvarsetid,
                "variableSet".usage,
                "variableSet".access_type AS "accessType",
				CASE 
					WHEN ("variableSetAccess".collaborator_id = ` + userId + ` AND lower("variableSet".access_type) != 'public') THEN "variableSetAccess".permission
					ELSE 'read-write'
                END AS permission,
                (
                    SELECT count("variableSetMember".id) 
                    FROM master.variable_set_member "variableSetMember" 
                    WHERE "variableSetMember".variable_set_id = "variableSet".id 
                    AND "variableSetMember".is_void = false
                ) as "memberCount"
            FROM 
                master.variable_set "variableSet"
            LEFT JOIN 
                tenant.person creator ON "variableSet".creator_id = creator.id
            LEFT JOIN 
                tenant.person modifier ON "variableSet".modifier_id = modifier.id
            LEFT JOIN
                master.variable_set_access "variableSetAccess" ON "variableSet".id = "variableSetAccess".variable_set_id
            WHERE 
                "variableSet".is_void = FALSE
            ` + addedConditionString + `
            ORDER BY
                "variableSet".id   
        `

        // Get filter condition for abbrev and usage only
        if (params.abbrev !== undefined || params.usage !== undefined) {
            let parameters = []

            if (params.abbrev !== undefined) parameters['abbrev'] = params.abbrev
            if (params.usage !== undefined) parameters['usage'] = params.usage

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400078)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query 
        variableSetFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            variableSetsQuery,
            conditionString,
            orderString
        )

        // Retrieve variableSets from the database   
        let variableSets = await sequelize.query(variableSetFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await variableSets === undefined || await variableSets.length < 1) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            // Get count 
            variableSetCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(variableSetsQuery, conditionString, orderString)

            variableSetCount = await sequelize.query(variableSetCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = variableSetCount[0].count
        }

        res.send(200, {
            rows: variableSets,
            count: count
        })
        return
    }
}