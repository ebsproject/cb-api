/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let {sequelize} = require('../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let germplasmHelper = require('../../../helpers/germplasm/index')
let forwarded = require('forwarded-for')
let validator = require('validator')
let format = require('pg-format')

module.exports = {
    /**
     * Create germplasm file upload records
     * POST /v3/germplasm-file-uploads
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res, next) {
        let programDbId
        let fileName
        let fileData
        let fileDataJson
        let fileUploadOrigin
        let fileUploadAction
        let fileStatus
        let allowedFileStatus = await germplasmHelper.getScaleValueByAbbrev('GERMPLASM_FILE_UPLOAD_STATUS')
        let fileStatusDefault = ['in queue','created']
        let remarks
        let notes
        let entity

        // Get user ID
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let germplasmFileUploadsUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/germplasm-file-uploads'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
    
        let data = req.body
        let records = [] 
        let recordCount = 0

        try {
            records = data.records
            recordCount = records.length
            // Check if the input data is empty
            if (records == undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } 
        catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let germplasmFileUploadsArray = []
            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0
                let entityQuery = ``

                // check if the required columns are in the input
                if (
                    record.programDbId === undefined 
                    || record.fileName === undefined 
                    || record.fileData === undefined
                    || record.fileUploadOrigin === undefined
                    || record.fileUploadAction === undefined
                    || record.entity === undefined
                ) {
                    let errMsg = 'Required parameters are missing. ' +
                        'Ensure that the programDbId, fileName, fileData, ' +
                        'fileUploadOrigin, fileUploadAction and entity fields are ' +
                        'not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if the programDbId provided is an integer
                programDbId = record.programDbId
                if (!validator.isInt(programDbId)) {
                    let errMsg = "Invalid format, program ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        tenant.program program
                    WHERE
                        program.id = ${programDbId}
                        AND program.is_void = FALSE
                )
                `
                validateCount += 1

                // Get fileName value
                fileName = record.fileName
                // Check if fileName is empty
                if (fileName == '') {
                    let errMsg = "Invalid input, fileName must not be empty"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate fileData format
                fileData = record.fileData
                // Check if fileData is an array
                if (!Array.isArray(fileData)) {
                    let errMsg = "Invalid input, fileData must be an array"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                // Check if fileData is empty
                if (fileData.length == 0) {
                    let errMsg = "Invalid input, fileData array must not be empty"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                fileDataJson = JSON.stringify(fileData)

                // Validate fileUploadOrigin value
                fileUploadOrigin = record.fileUploadOrigin
                // Check if fileUploadOrigin is empty
                if (fileUploadOrigin === '') {
                    let errMsg = "Invalid input, fileUploadOrigin must not be empty."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                // Check if user input is a valid database value
                validateQuery += (validateQuery !== '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if platform abbrev is existing return 1
                        SELECT
                            count(1)
                        FROM 
                            platform.application application
                        WHERE
                            application.abbrev ILIKE $$${fileUploadOrigin}$$
                    )
                `
                validateCount += 1

                // Check if fileUpload action is provided
                fileUploadAction = record.fileUploadAction
                if (fileUploadAction === '') {
                    let errMsg = "Invalid input, fileUploadAction must not be empty."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                fileUploadAction = fileUploadAction.trim().toLowerCase()
                // Validate fileUploadOrigin value
                validateQuery += (validateQuery !== '') ? ' + ' : ''
                validateQuery += `
                    (
                    --- Check if FILE_UPLOAD_ACTION is existing return 1
                    SELECT 
                        count(1)
                    FROM 
                        master.scale_value scaleValue
                    LEFT JOIN
                        master.variable variable ON scaleValue.scale_id = variable.scale_id
                    WHERE
                        variable.abbrev LIKE 'FILE_UPLOAD_ACTION' AND
                        scaleValue.value ILIKE $$${fileUploadAction}$$
                    )
                `
                validateCount += 1

                // Check if fileStatus is provided.
                // Do not allow this. The default status should always be 'in queue'
                if (record.fileStatus != undefined) {
                    if (!fileStatusDefault.includes(record.fileStatus)) {
                        let errMsg = `Invalid input, fileStatus should be: ` + fileStatusDefault.join(', ')
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    
                    fileStatus = record.fileStatus
                }
                // Catch if fileStatus is empty
                else {
                    let errMsg = "Invalid input, fileStatus must not be empty"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get remarks value
                if (record.remarks !== undefined) remarks = record.remarks

                // Get notes value
                if (record.notes !== undefined) notes = record.notes

                // Check if entity is provided
                entity = record.entity
                if(entity === ''){
                    let errMsg = "Invalid input, entity must not be empty."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get entity id from entity
                entityQuery = `
                    SELECT id
                    FROM 
                        dictionary.entity entity
                    WHERE 
                        entity.is_void = FALSE AND
                        entity.abbrev = UPPER('${entity}');
                `
                let entityQueryResult = await sequelize.query(entityQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    return
                })

                // Check if insertion query returned results
                if (entityQueryResult == undefined || entityQueryResult.length == 0) {
                    let errMsg = "You have provided an invalid value for entity."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                entityDbId = entityQueryResult[0]['id'] ?? null

                // Validate entity
                validateQuery += (validateQuery !== '') ? ' + ' : ''
                validateQuery += `
                    (
                    SELECT
                        COUNT(1)
                    FROM
                        dictionary.entity entity
                    WHERE
                        entity.is_void = FALSE AND
                        entity.abbrev = UPPER('${entity}')
                    )`

                validateCount += 1
                
                // Perform validation
                if (validateCount > 0) {
                    let checkInputQuery = `
                        SELECT ${validateQuery} AS count
                    `
                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    
                    if (inputCount[0]['count'] != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // Add data to insertion array
                let tempArray = [
                    programDbId,
                    fileName,
                    fileDataJson,
                    fileUploadOrigin,
                    fileUploadAction,
                    fileStatus,
                    userDbId,
                    remarks,
                    notes,
                    entityDbId
                ]

                germplasmFileUploadsArray.push(tempArray)
            }

            // Create germplasm file upload
            let germplasmFileUploadQuery = format (`
                INSERT INTO germplasm.file_upload
                    (program_id, file_name, file_data, file_upload_origin, file_upload_action, file_status, creator_id, remarks, notes, entity_id)
                VALUES 
                    %L
                RETURNING id`, germplasmFileUploadsArray
            )

            let germplasmFileUploads = await sequelize.query(germplasmFileUploadQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })
                .catch(async err => {
                    return
                })

            // Check if insertion query returned results
            if (germplasmFileUploads == undefined || germplasmFileUploads[0].length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            let resultArray = []
            for (let germplasmFileUpload of germplasmFileUploads[0]) {
                germplasmFileUploadDbId = germplasmFileUpload.id
                // Return the germplasm file upload info to Client
                let array = {
                    germplasmFileUploadDbId: germplasmFileUploadDbId,
                    recordCount: 1,
                    href: germplasmFileUploadsUrlString + '/' + germplasmFileUploadDbId
                }
                resultArray.push(array)
            }
            await transaction.commit()
            
            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch (e) {
            // Rollback transaction (if still open)
            if (transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return   
        }
    }
}