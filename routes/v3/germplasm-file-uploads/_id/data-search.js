/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let {sequelize} = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let fuDataHelper = require('../../../../helpers/germplasmFileUpload/index.js')
let logger = require('../../../../helpers/logger/index')

module.exports = {

    /**
     * Endpoint for retrieving data associated with a germplasm file upload record.
     * Path parameter `enitity` is required
     * POST /v3/germplasm-file-uploads/:id/data-search
     */
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let entity = req.query.entity
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let addedOrderString = ``
        let supportedEntities = [
            "germplasm", "germplasm_name", "germplasm_relation", "germplasm_attribute"
        ]
        let defaultFields = ``
        let entityLeftJoin = ``
        let entityCondition = ``

        // Check if entity is provided as a path parameter.
        // If not provided, send back BadRequestError
        if (entity === undefined || entity === null) {
            let errMsg = "Missing required path parameter `entity`. Supported values: " + supportedEntities.join(", ")
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if entity is supported. If not, send back BadRequestError
        if (!supportedEntities.includes(entity.toLowerCase())) {
            let errMsg = "Unsupported `entity` value. Supported values: " + supportedEntities.join(", ")
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if(req.params.id == undefined || req.params.id == ''){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Set defaults
        let germplasmFileUploadDbId = req.params.id

        // check format if id
        if(!validator.isInt(germplasmFileUploadDbId)){
            let errMsg = "Invalid format, germplasm file upload record ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        const endpoint = `germplasm-file-uploads/${germplasmFileUploadDbId}/data-search`

        // set query
        let germplasmFileUploadQuery = `
            SELECT
                gefu.id AS "germplasmFileUploadDbId",
                gefu.program_id AS "programDbDd",
                (
                    SELECT
                        program.program_code AS programCode
                    FROM
                        tenant.program program
                    WHERE
                        program.is_void = FALSE AND
                        program.id = gefu.program_id
                ),
                gefu.file_status AS "fileStatus",
                gefu.file_name AS "fileName",
                gefu.creator_id AS "uploaderDbId",
                (
                    SELECT
                        person.person_name AS "uploader"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.creator_id
                ),
                gefu.creation_timestamp AS "uploaderTimestamp",
                gefu.modifier_id AS "modifierDbId",
                (
                    SELECT
                        person.person_name AS "modifier"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.modifier_id
                ),
                gefu.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.file_upload gefu
            WHERE
                gefu.is_void = FALSE AND
                gefu.id = ${germplasmFileUploadDbId}
        `

        let germplasmFileUpload = await sequelize.query(germplasmFileUploadQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })
        if(await germplasmFileUpload == undefined ) {
            return
        }

        if(await germplasmFileUpload == null || await germplasmFileUpload.length < 1){
            let errMsg = "The germplasm file upload record you requested does not exist."
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinctOn condition is set
            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let germplasmFileUploadDataQuery = null

        let subQueries = await fuDataHelper.fuDataDataSubQueryBuilder(entity)
        defaultFields = subQueries.defaultFields
        entityLeftJoin = subQueries.entityLeftJoin
        entityCondition = subQueries.entityCondition
        addedOrderString = subQueries.addedOrderString

        if (parameters['fields'] != null) {

            germplasmFileUploadDataQuery = knex.column(parameters['fields'].split('|'))

            germplasmFileUploadDataQuery += `
                FROM
                    germplasm.file_upload_germplasm fuData
                ${entityLeftJoin}
                WHERE
                    fuData.file_upload_id = ${germplasmFileUploadDbId}
                    AND fuData.entity = '${entity}'
                    AND fuData.is_void = FALSE
            `
        } else {
            germplasmFileUploadDataQuery = `
                SELECT
                    ${defaultFields}
                FROM
                    germplasm.file_upload_germplasm fuData
                ${entityLeftJoin}
                WHERE
                    fuData.file_upload_id = ${germplasmFileUploadDbId}
                    ${entityCondition}
                    AND fuData.is_void = FALSE
            `
        }

        // Parse the sort parameters
        if (sort != null && addedDistinctString === '') {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let germplasmFileUploadDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            germplasmFileUploadDataQuery,
            conditionString,
            orderString,
            addedDistinctString
        )

        // retrieve the germplasm from the database
        let germplasmFileUploadData = await sequelize
            .query(germplasmFileUploadDataFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
                return
            })

        if (germplasmFileUploadData === undefined) {
            return
        }
        // get count
        let germplasmFileUploadDataCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                germplasmFileUploadDataQuery,
                conditionString,
                orderString,
                addedDistinctString
            )

        let germplasmFileUploadDataCount = await sequelize
            .query(germplasmFileUploadDataCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        if (germplasmFileUploadDataCount === undefined || germplasmFileUploadDataCount === null) {
            return
        }

        // If count is set, get value and set it as the count value for the result
        if (germplasmFileUploadDataCount[0] !== undefined && germplasmFileUploadDataCount[0] !== null
            && germplasmFileUploadDataCount[0]['count'] !== undefined && germplasmFileUploadDataCount[0]['count'] !== null
        ) {
            count = germplasmFileUploadDataCount[0]['count']
        }
        res.send(200, {
            rows: germplasmFileUploadData,
            count: count
        })
        return
    }
}