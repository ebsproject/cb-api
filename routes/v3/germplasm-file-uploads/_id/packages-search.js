/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

    /**
     * Endpoint for retrieving packages of a file upload record
     * POST /v3/germplasm-file-uploads/:id/packages-search
     */
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let addedOrderString = `
            ORDER BY
                germplasm.id
        `

        if (req.params.id == undefined || req.params.id == '') {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Set defaults
        let germplasmFileUploadDbId = req.params.id

        // check format if id
        if (!validator.isInt(germplasmFileUploadDbId)) {
            let errMsg = "Invalid format, germplasm file upload record ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // set query
        let germplasmFileUploadQuery = `
            SELECT
                gefu.id AS "germplasmFileUploadDbId",
                gefu.program_id AS "programDbDd",
                (
                    SELECT
                        program.program_code AS programCode
                    FROM
                        tenant.program program
                    WHERE
                        program.is_void = FALSE AND
                        program.id = gefu.program_id
                ),
                gefu.file_status AS "fileStatus",
                gefu.file_name AS "fileName",
                gefu.creator_id AS "uploaderDbId",
                (
                    SELECT
                        person.person_name AS "uploader"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.creator_id
                ),
                gefu.creation_timestamp AS "uploaderTimestamp",
                gefu.modifier_id AS "modifierDbId",
                (
                    SELECT
                        person.person_name AS "modifier"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.modifier_id
                ),
                gefu.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.file_upload gefu
            WHERE
                gefu.is_void = FALSE AND
                gefu.id = ${germplasmFileUploadDbId}
        `

        let germplasmFileUpload = await sequelize.query(germplasmFileUploadQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await germplasmFileUpload == undefined || await germplasmFileUpload.length < 1) {
            let errMsg = "The germplasm file upload record you requested does not exist."
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinctOn condition is set
            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let germplasmFileUploadPackagesQuery = null

        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields']
                )

                let selectString = knex.raw(`${addedDistinctString} ${fieldString}`)

                germplasmFileUploadPackagesQuery = knex.select(selectString)
            } else {
                germplasmFileUploadPackagesQuery = knex.column(parameters['fields'].split('|'))
            }

            germplasmFileUploadPackagesQuery += `
                FROM
                    germplasm.file_upload_germplasm fug
                LEFT JOIN
                    germplasm.package ON package.id = fug.package_id AND package.is_void = FALSE
                LEFT JOIN
                    germplasm.seed ON seed.id = package.seed_id AND seed.is_void = FALSE
                LEFT JOIN
                    germplasm.germplasm ON germplasm.id = seed.germplasm_id AND germplasm.is_void = FALSE
                LEFT JOIN
                    place.facility ON facility.id = package.facility_id AND facility.is_void = FALSE
                WHERE
                    fug.file_upload_id = ${germplasmFileUploadDbId}
                    AND package.id IS NOT NULL
                    AND fug.is_void = FALSE
                    ${addedOrderString}
            `
        } else {
            germplasmFileUploadPackagesQuery = `
                SELECT
                    ${addedDistinctString}
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation,
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.germplasm_name_type AS "germplasmNameType",
                    germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                    seed.id AS "seedDbId",
                    seed.seed_code AS "seedCode",
                    seed.seed_name AS "seedName",
                    package.id AS "packageDbId",
                    package.package_code AS "packageCode",
                    package.package_label AS "packageLabel",
                    package.package_quantity AS "packageQuantity",
                    package.package_unit AS "packageUnit",
                    package.package_status AS "packageStatus",
                    (
                        SELECT 
                            program.program_code AS "program" 
                        FROM 
                            tenant.program program
                        WHERE
                            program.id = package.program_id AND
                            program.is_void = FALSE
                    ),
                    facility.id AS "facilityDbId",
                    facility.facility_code AS "facilityCode",
                    facility.facility_name AS "facilityName"
                FROM
                    germplasm.file_upload_germplasm fug
                LEFT JOIN
                    germplasm.package ON package.id = fug.package_id AND package.is_void = FALSE
                LEFT JOIN
                    germplasm.seed ON seed.id = package.seed_id AND seed.is_void = FALSE
                LEFT JOIN
                    germplasm.germplasm ON germplasm.id = seed.germplasm_id AND germplasm.is_void = FALSE
                LEFT JOIN
                    place.facility ON facility.id = package.facility_id AND facility.is_void = FALSE
                WHERE
                    fug.file_upload_id = ${germplasmFileUploadDbId}
                    AND package.id IS NOT NULL
                    AND fug.is_void = FALSE
                    ${addedOrderString}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let germplasmFileUploadPackagesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            germplasmFileUploadPackagesQuery,
            conditionString,
            orderString,
        )

        // retrieve the germplasm from the database
        let germplasmFileUploadPackages = await sequelize
            .query(germplasmFileUploadPackagesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
                return
            })

        // get count
        let germplasmFileUploadPackagesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                germplasmFileUploadPackagesQuery,
                conditionString,
                orderString,
            )

        let germplasmFileUploadPackagesCount = await sequelize
            .query(germplasmFileUploadPackagesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = germplasmFileUploadPackagesCount[0].count
        res.send(200, {
            rows: germplasmFileUploadPackages,
            count: count
        })
        return
    }
}