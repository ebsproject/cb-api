/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let {sequelize} = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator')
let germplasmHelper = require('../../../../helpers/germplasm/index.js')
let forwarded = require('forwarded-for')
let validator = require('validator')
let format = require('pg-format')
const logger = require('../../../../helpers/logger/index.js')
const endpoint = 'germplasm-file-uploads/:id'

module.exports = {
    /**
     * Update germplasm file upload records
     * PUT /v3/germplasm-file-uploads/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    put: async function (req, res, next) {
        let germplasmFileUploadDbId
        let fileName
        let fileData
        let fileStatus
        let germplasmCount
        let seedCount
        let packageCount
        let programDbId
        let errorLog
        let remarks
        let entity

        // Get status values in this config
        let allowedFileStatus = await germplasmHelper.getScaleValueByAbbrev('GERMPLASM_FILE_UPLOAD_STATUS')

        // Get user ID
        let userDbId = await tokenHelper.getUserId(req, res)

        // If userDbId is undefined, terminate
        if(userDbId === undefined){
            return
        }

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve germplasm file upload ID
        germplasmFileUploadDbId = req.params.id
        // Check if germplasm file upload ID is an integer
        if (!validator.isInt(germplasmFileUploadDbId)) {
            let errMsg = 'Invalid format. Germplasm file Upload ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if request body exists
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if germplasm file upload ID exists
        let validateGermplasmFileUploadQuery = `
            SELECT
                file_upload.id AS "germplasmFileUploadDbId",
                file_upload.file_name AS "fileName",
                file_upload.file_data AS "fileData",
                file_upload.file_status AS "fileStatus",
                file_upload.germplasm_count AS "germplasmCount",
                file_upload.seed_count AS "seedCount",
                file_upload.package_count AS "packageCount",
                file_upload.program_id AS "programDbId",
                file_upload.error_log AS "errorLog",
                file_upload.remarks AS "remarks",
                file_upload.entity_id AS "entityDbId"
            FROM
                germplasm.file_upload
            WHERE
            file_upload.id = ${germplasmFileUploadDbId}
                AND file_upload.is_void = FALSE
        `

        // Retrieve germplasm file upload from the database   
        let germplasmFileUploads = await sequelize.query(validateGermplasmFileUploadQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        // Return error if resource is not found
        if (await germplasmFileUploads == undefined || await germplasmFileUploads.length == 0) {
            let errMsg = 'Invalid request. Ensure that the germplasm file upload record exists. '
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values
        let germplasmFileUpload = germplasmFileUploads[0]
        let gfuFileName = germplasmFileUpload.fileName
        let gfuFileData = germplasmFileUpload.fileData
        let gfuFileStatus = germplasmFileUpload.fileStatus
        let gfuGermplasmCount = germplasmFileUpload.germplasmCount
        let gfuSeedCount = germplasmFileUpload.seedCount
        let gfuPackageCount = germplasmFileUpload.packageCount
        let gfuProgramDbId = germplasmFileUpload.programDbId
        let gfuErrorLog = germplasmFileUpload.errorLog
        let gfuRemarks = germplasmFileUpload.remarks
        let gfuEntityDbId = germplasmFileUpload.entityDbId

        // Set the URL for response
        let isSecure = forwarded(req, req.headers).secure
        let germplasmFileUploadsUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/germplasm-file-uploads"

        let data = req.body
        let resultArray = []

        try {
            let setQuery = ``
            /**
             * Validation of the values
             * */
            let validateQuery = ``
            let validateCount = 0

            if (data.fileName != undefined) {
                fileName = data.fileName
                if(fileName != gfuFileName){
                    setQuery += `
                        file_name = $$${fileName}$$`
                }
            }

            if (data.fileData != undefined) {
                fileData = data.fileData

                // Check if fileData is an array
                if (!Array.isArray(fileData)) {
                    let errMsg = "Invalid input, fileData must be an array"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                // Check if fileData is empty
                if (fileData.length == 0) {
                    let errMsg = "Invalid input, fileData array must not be empty"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let fileDataJson = JSON.stringify(fileData)
                let gfuFileDataJson = JSON.stringify(gfuFileData)
                if(fileDataJson != gfuFileDataJson){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        file_data = $$${fileDataJson}$$`
                }
            }

            if (data.fileStatus != undefined) {
                fileStatus = data.fileStatus

                // Check if fileStatus provided is included in the allowed values
                if (!allowedFileStatus.includes(fileStatus)) {
                    let errMsg = `Invalid input, fileStatus should be: ` + allowedFileStatus.join(', ')
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(fileStatus != gfuFileStatus){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        file_status = $$${fileStatus}$$`
                }
            }

            if (data.germplasmCount != undefined) {
                germplasmCount = data.germplasmCount

                if (!validator.isInt(germplasmCount)) {
                    let errMsg = "Invalid format, germplasm count must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(germplasmCount != gfuGermplasmCount){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_count = $$${germplasmCount}$$`
                }
            }

            if (data.seedCount != undefined) {
                seedCount = data.seedCount

                if (!validator.isInt(seedCount)) {
                    let errMsg = "Invalid format, seed count must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(seedCount != gfuSeedCount){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        seed_count = $$${seedCount}$$`
                }
            }

            if (data.packageCount != undefined) {
                packageCount = data.packageCount

                if (!validator.isInt(packageCount)) {
                    let errMsg = "Invalid format, package count must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(packageCount != gfuPackageCount){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_count = $$${packageCount}$$`
                }
            }

            if (data.programDbId != undefined) {
                programDbId = data.programDbId

                if (!validator.isInt(programDbId)) {
                    let errMsg = "Invalid format, program ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        tenant.program program
                    WHERE
                        program.id = ${programDbId}
                        AND program.is_void = FALSE
                )
                `
                validateCount += 1

                if(programDbId != gfuProgramDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        program_id = $$${programDbId}$$`
                }
            }

            if (data.errorLog != undefined) {
                errorLog = data.errorLog
                if(errorLog != gfuErrorLog){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        error_log = $$${errorLog}$$`
                }
            }

            if (data.remarks != undefined) {
                remarks = data.remarks
                if(remarks != gfuRemarks){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        remarks = $$${remarks}$$`
                }
            }

            if (data.entity != undefined) {
                entity = data.entity

                // Check if entity is empty
                if (entity.length == 0) {
                    let errMsg = "Invalid input, entity must not be empty"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        dictionary.entity entity
                    WHERE
                        entity.abbrev = UPPER('${entity}')
                        AND entity.is_void = FALSE
                )
                `
                validateCount += 1

                // Get entity id from entity
                let entityQuery = `
                    SELECT id
                    FROM 
                        dictionary.entity entity
                    WHERE 
                        entity.is_void = FALSE AND
                        entity.abbrev = UPPER('${entity}');
                `
                let entityQueryResult = await sequelize.query(entityQuery, {
                    type: sequelize.QueryTypes.SELECT
                }).catch(async err => {
                    return
                })

                // Check if insertion query returned results
                if (entityQueryResult == undefined || entityQueryResult.length == 0) {
                    let errMsg = "You have provided an invalid value for entity."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let entityDbId = entityQueryResult[0]['id'] ?? null


                if(entityDbId != gfuEntityDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entity_id = $$${entityDbId}$$`
                }
            }

            // Perform validation
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // update the germplasm record
            let updateGermplasmFileUploadQuery = `
                UPDATE
                    germplasm.file_upload file_upload
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = $$${userDbId}$$
                WHERE
                    file_upload.id = $$${germplasmFileUploadDbId}$$
            `

            // Update background job record
            await sequelize.transaction(async transaction => {
                return await sequelize.query(updateGermplasmFileUploadQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            let array = {
                germplasmFileUploadDbId: germplasmFileUploadDbId,
                recordCount: 1,
                href: germplasmFileUploadsUrlString + '/' + germplasmFileUploadDbId
            }
            resultArray.push(array)

            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return   
        }
    },

    /**
     * Delete germplasm file upload records
     * DELETE /v3/germplasm-file-uploads/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    delete: async function(req, res, next) {
        let germplasmFileUploadDbId = req.params.id

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if germplasm file upload ID is an integer
        if (!validator.isInt(germplasmFileUploadDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400249)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let germplasmFileUploadQuery = `
            SELECT
                gfu.id AS "germplasmFileUploadDbId"
            FROM 
                germplasm.file_upload gfu
            WHERE 
                gfu.is_void = FALSE AND 
                gfu.id = ${germplasmFileUploadDbId}
        `
    
        let germplasmFileUploadRecord = await sequelize.query(germplasmFileUploadQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If file upload retrieval failed, end execution
        if (germplasmFileUploadRecord === undefined) {
            return
        }

        // If no record was retrieved, return NotFoundError
        if (germplasmFileUploadRecord !== null && germplasmFileUploadRecord.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404055)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
      
            let deleteGermplasmFileUploadQuery = format(`
                UPDATE
                    germplasm.file_upload
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${germplasmFileUploadDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${germplasmFileUploadDbId}
            `)
      
            await sequelize.query(deleteGermplasmFileUploadQuery, {
                type: sequelize.QueryTypes.UPDATE
            })
      
            await transaction.commit()
            res.send(200, {
                rows: { germplasmFileUploadDbId: germplasmFileUploadDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (transaction !== undefined
                && transaction !== null
                && transaction.finished !== 'commit'
            ) {
                transaction.rollback()
            }
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Retrieve a specific germplasm file upload record by id
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    get: async function (req, res, next) {

        if(req.params.id == undefined || req.params.id == ''){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return            
        }

        // Set defaults
        let germplasmFileUploadDbId = req.params.id

        // check format if id
        if(!validator.isInt(germplasmFileUploadDbId)){
            let errMsg = "Invalid format, germplasm file upload record ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // set query
        let germplasmFileUploadQuery = `
            SELECT
                gefu.id AS "germplasmFileUploadDbId",
                gefu.program_id AS "programDbDd",
                (
                    SELECT 
                        program.program_code AS programCode 
                    FROM
                        tenant.program program
                    WHERE 
                        program.is_void = FALSE AND
                        program.id = gefu.program_id
                ),
                gefu.file_name AS "fileName",
                gefu.file_status AS "fileStatus",
                gefu.file_upload_action AS "fileUploadAction",
                gefu.germplasm_count AS "germplasmCount",
                gefu.seed_count AS "seedCount",
                gefu.package_count AS "packageCount",
                gefu.file_data AS "fileData",
                gefu.error_log AS "errorLog",
                gefu.creator_id AS "uploaderDbId",
                (
                    SELECT
                        person.person_name AS "uploader"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.creator_id
                ),
                gefu.creation_timestamp AS "uploaderTimestamp",
                gefu.modifier_id AS "modifierDbId",
                (
                    SELECT
                        person.person_name AS "modifier"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.modifier_id
                ),
                gefu.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.file_upload gefu
            WHERE
                gefu.is_void = FALSE AND
                gefu.id = ${germplasmFileUploadDbId}
        `

        try{
            let germplasmFileUpload = await sequelize.query(germplasmFileUploadQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return                
            })

            if(await germplasmFileUpload == undefined || await germplasmFileUpload.length < 1){
                let errMsg = "The germplasm file upload record you requested does not exist."
                res.send(new errors.BadRequestError(errMsg))
                return                
            }

            res.send(200, {
                rows: germplasmFileUpload,
            })
            return     
        }
        catch(error){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}