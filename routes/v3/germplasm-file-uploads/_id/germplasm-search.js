/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let {sequelize} = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

    /**
     * Endpoint for retrieving germplasm of a file upload record
     * POST /v3/germplasm-file-uploads/:id/germplasm-search
     */
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let addedOrderString = `
            ORDER BY
                germplasm.id
        `

        if(req.params.id == undefined || req.params.id == ''){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Set defaults
        let germplasmFileUploadDbId = req.params.id

        // check format if id
        if(!validator.isInt(germplasmFileUploadDbId)){
            let errMsg = "Invalid format, germplasm file upload record ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // set query
        let germplasmFileUploadQuery = `
            SELECT
                gefu.id AS "germplasmFileUploadDbId",
                gefu.program_id AS "programDbDd",
                (
                    SELECT
                        program.program_code AS programCode
                    FROM
                        tenant.program program
                    WHERE
                        program.is_void = FALSE AND
                        program.id = gefu.program_id
                ),
                gefu.file_status AS "fileStatus",
                gefu.file_name AS "fileName",
                gefu.creator_id AS "uploaderDbId",
                (
                    SELECT
                        person.person_name AS "uploader"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.creator_id
                ),
                gefu.creation_timestamp AS "uploaderTimestamp",
                gefu.modifier_id AS "modifierDbId",
                (
                    SELECT
                        person.person_name AS "modifier"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = gefu.modifier_id
                ),
                gefu.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.file_upload gefu
            WHERE
                gefu.is_void = FALSE AND
                gefu.id = ${germplasmFileUploadDbId}
        `

        let germplasmFileUpload = await sequelize.query(germplasmFileUploadQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if(await germplasmFileUpload == undefined || await germplasmFileUpload.length < 1){
            let errMsg = "The germplasm file upload record you requested does not exist."
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinctOn condition is set
            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let germplasmFileUploadGermplasmQuery = null

        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields']
                )

                let selectString = knex.raw(`${addedDistinctString} ${fieldString}`)

                germplasmFileUploadGermplasmQuery = knex.select(selectString)
            } else {
                germplasmFileUploadGermplasmQuery = knex.column(parameters['fields'].split('|'))
            }

            germplasmFileUploadGermplasmQuery += `
                FROM
                    germplasm.file_upload_germplasm fug
                LEFT JOIN
                    germplasm.germplasm ON germplasm.id = fug.germplasm_id AND germplasm.is_void = FALSE
                LEFT JOIN
                    tenant.crop crop ON germplasm.crop_id = crop.id AND crop.is_void = FALSE
                LEFT JOIN
                    germplasm.taxonomy taxonomy ON germplasm.taxonomy_id = taxonomy.id AND taxonomy.is_void = FALSE
                WHERE
                    fug.file_upload_id = ${germplasmFileUploadDbId}
                    AND fug.is_void = FALSE
                    ${addedOrderString}
            `
        } else {
            germplasmFileUploadGermplasmQuery = `
                SELECT
                    ${addedDistinctString}
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation,
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.germplasm_type AS "germplasmType",
                    germplasm.germplasm_name_type AS "germplasmNameType",
                    germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                    germplasm.crop_id AS "cropDbId",
                    crop.crop_code AS "cropCode",
                    germplasm.taxonomy_id AS "taxonomyDbId",
                    taxonomy.taxon_id AS "taxonId",
                    taxonomy.taxonomy_name AS "taxonomyName"
                FROM
                    germplasm.file_upload_germplasm fug
                LEFT JOIN
                    germplasm.germplasm ON germplasm.id = fug.germplasm_id AND germplasm.is_void = FALSE
                LEFT JOIN
                    tenant.crop crop ON germplasm.crop_id = crop.id AND crop.is_void = FALSE
                LEFT JOIN
                    germplasm.taxonomy taxonomy ON germplasm.taxonomy_id = taxonomy.id AND taxonomy.is_void = FALSE
                WHERE
                    fug.file_upload_id = ${germplasmFileUploadDbId}
                    AND fug.is_void = FALSE
                    ${addedOrderString}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let germplasmFileUploadGermplasmFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            germplasmFileUploadGermplasmQuery,
            conditionString,
            orderString,
        )

        // retrieve the germplasm from the database
        let germplasmFileUploadGermplasm = await sequelize
            .query(germplasmFileUploadGermplasmFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
                return
            })

        if (await germplasmFileUploadGermplasm == undefined || await germplasmFileUploadGermplasm.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // get count
        let germplasmFileUploadGermplasmCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                germplasmFileUploadGermplasmQuery,
                conditionString,
                orderString,
            )

        let germplasmFileUploadGermplasmCount = await sequelize
            .query(germplasmFileUploadGermplasmCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = (germplasmFileUploadGermplasmCount !== undefined && germplasmFileUploadGermplasmCount[0] !== undefined) ? germplasmFileUploadGermplasmCount[0].count : 0

        res.send(200, {
            rows: germplasmFileUploadGermplasm,
            count: count
        })
        return
    }
}