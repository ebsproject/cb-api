/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')

module.exports = { 
	// Endpoint for retrieving all spaces
	// GET /v3/spaces
	get: async function (req, res, next) {
		// Set defaults 
		let limit = req.paginate.limit;
		let offset = req.paginate.offset
		let params = req.query
		let sort = req.query.sort
		let count = 0
		let orderString = ''
		let conditionString = ''
		const endpoint = 'GET spaces'
		const operation = 'SELECT'


		// Build query
		let spacesQuery = `
			SELECT
				space.id AS "spaceDbId",
				space.abbrev,
				space.name,
				space.menu_data AS "menuData",
				space.order_number AS "orderNumber",
				space.team_abbrevs AS "teamAbbrevs",
				space.description,
				space.remarks AS "remarks",
				space.notes,
				space.creation_timestamp AS "creationTimestamp",
				space.creator_id AS "creatorDbId",
				creator.person_name AS creator,
				space.modification_timestamp AS "modificationTimestamp",
				space.modifier_id AS "modifierId",
				modifier.person_name AS modifier
			FROM 
				platform.space space
			JOIN 
				tenant.person creator ON space.creator_id = creator.id
			LEFT JOIN 
				tenant.person modifier ON space.modifier_id = modifier.id
			WHERE
				space.is_void = FALSE
			ORDER BY
				space.id
		`
		// Get filter condition for abbrev only
		if (params.abbrev !== undefined) {
			let parameters = {
				abbrev: params.abbrev
			}

			conditionString = await processQueryHelper.getFilterString(parameters)

			if (conditionString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400003)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}

		if (sort != null) {
			orderString = await processQueryHelper.getOrderString(sort)

			if (orderString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400004)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}

		// Generate the final sql query 
		spacesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
			spacesQuery,
			conditionString,
			orderString
		)

		// Retrieve spaces from the database   
		let spaces = await sequelize.query(spacesFinalSqlQuery, {
			type: sequelize.QueryTypes.SELECT,
			replacements: {
				limit: limit,
				offset: offset
			}
		})
		.catch(async err => {
			await logger.logFailingQuery(endpoint, operation, err)

			let errMsg = await errorBuilder.getError(req.headers.host, 500004)
			res.send(new errors.InternalError(errMsg))
			return
		})

		// Error occurred during query
		if (spaces === undefined) return

		if (await spaces === undefined || await spaces.length < 1) {
			res.send(200, {
				rows: [],
				count: 0
			})
			return
		} else {
			// Get count 
			spaceCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(spacesQuery, conditionString, orderString)

			spaceCount = await sequelize.query(spaceCountFinalSqlQuery, {
				type: sequelize.QueryTypes.SELECT
			})
			.catch(async err => {

				await logger.logFailingQuery(endpoint, operation, err)

				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

			// Error occurred during query
			if (spaceCountFinalSqlQuery === undefined) return

			count = await spaceCount[0].count
		}

		res.send(200, {
			rows: spaces,
			count: count
		})
		return
	}
}
