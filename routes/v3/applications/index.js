/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = { 
	// Endpoint for retrieving all applications
	// GET /v3/applications
	get: async function (req, res, next) {
		// Set defaults 
		let limit = req.paginate.limit;
		let offset = req.paginate.offset
		let params = req.query
		let sort = req.query.sort
		let count = 0
		let orderString = ''
		let conditionString = ''
	
		// Build query
		let applicationsQuery = `
			SELECT
				application.id AS "applicationDbId",
				application.abbrev,
				application.label,
				application.action_label AS "actionLabel",
				application.icon,
				application.description,
				application.is_public AS "isPublic",
				application.remarks AS "remarks",
				application.notes,
				application.creation_timestamp AS "creationTimestamp",
				application.creator_id AS "creatorDbId",
				creator.person_name AS creator,
				application.modification_timestamp AS "modificationTimestamp",
				modifier.id AS "modifierId",
				modifier.person_name AS modifier
			FROM 
				platform.application application
			LEFT JOIN 
				tenant.person creator ON application.creator_id = creator.id
			LEFT JOIN 
				tenant.person modifier ON application.modifier_id = modifier.id
			WHERE
				application.is_void = FALSE
			ORDER BY
				application.id
		`
		// Get filter condition for abbrev only
		if (params.abbrev !== undefined) {
			let parameters = {
				abbrev: params.abbrev
			}
		
			conditionString = await processQueryHelper.getFilterString(parameters)
		
			if (conditionString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400003)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}
	
		if (sort != null) {
			orderString = await processQueryHelper.getOrderString(sort)
		
			if (orderString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400004)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}
	
		// Generate the final sql query 
		applicationsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
			applicationsQuery,
			conditionString,
			orderString
		)
	
		// Retrieve applications from the database   
		let applications = await sequelize.query(applicationsFinalSqlQuery, {
			type: sequelize.QueryTypes.SELECT,
			replacements: {
				limit: limit,
				offset: offset
			}
		})
		.catch(async err => {
			let errMsg = await errorBuilder.getError(req.headers.host, 500004)
			res.send(new errors.InternalError(errMsg))
			return
		})

		if (await applications == undefined || await applications.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
    } else {
      // Get the final count of the entry records
      let applicationCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          applicationsQuery,
          conditionString,
          orderString
        )

      let applicationCount = await sequelize
        .query(applicationCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = applicationCount[0].count

      res.send(200, {
        rows: applications,
        count: count
      })
      return
    }
	}
}
