/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

  // Endpoint for retrieving the list of applications
  // GET /v3/applications/{id}
  get: async function (req, res, next) {

    let applicationDbId = req.params.id
    let count = 0

    if (!validator.isInt(applicationDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400174)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Build query
    let applicationsQuery = `
    SELECT
      application.id AS "applicationDbId",
      application.abbrev,
      application.label,
      application.action_label AS "actionLabel",
      application.icon,
      application.description,
      application.is_public AS "isPublic",
      application.remarks AS "remarks",
      application.notes,
      application.creation_timestamp AS "creationTimestamp",
      application.creator_id AS "creatorDbId",
      creator.person_name AS creator,
      application.modification_timestamp AS "modificationTimestamp",
      modifier.id AS "modifierId",
      modifier.person_name AS modifier
    FROM 
      platform.application application
    LEFT JOIN 
      tenant.person creator ON application.creator_id = creator.id
    LEFT JOIN 
      tenant.person modifier ON application.modifier_id = modifier.id
    WHERE
      application.id = (:applicationDbId) AND
      application.is_void = FALSE 
    `

    // Retrieve application from the database   
    let applications = await sequelize.query(applicationsQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        applicationDbId: applicationDbId
      }
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    // Return error if resource is not found
    if (await applications == undefined || await applications.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404041)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: applications
    })
    return
  }
}
