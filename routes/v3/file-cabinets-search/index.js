/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY "fileCabinet".id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn',
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray,
        )

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }
    // Build the base retrieval query
    let fileCabinetsQuery = null
    // Check if the client specified values for fields
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])
        
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        fileCabinetsQuery = knex.select(selectString)
      } else {
        fileCabinetsQuery = knex.column(parameters['fields'].split('|'))
      }

      fileCabinetsQuery += `
        FROM
          operational.file_cabinet "fileCabinet"
        LEFT JOIN
          experiment.occurrence occurrence ON "fileCabinet".occurrence_id = occurrence.id
        LEFT JOIN
          tenant.person creator ON "fileCabinet".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "fileCabinet".modifier_id = modifier.id
        WHERE
          "fileCabinet".is_void = FALSE
        ${addedOrderString}
      `
    } else {
      fileCabinetsQuery = `
        SELECT
          "fileCabinet".id AS "fileCabinetDbId",
          "fileCabinet".folder_name AS "folderName",
          "fileCabinet".folder_path AS "folderPath",
          "fileCabinet".files,
          "fileCabinet".source,
          occurrence.id AS "occurrenceDbId",
          occurrence.occurrence_code AS "occurrenceCode",
          occurrence.occurrence_name AS "occurrenceName",
          occurrence.occurrence_status AS "occurrenceStatus",
          "fileCabinet".transaction_id AS "transactionDbId",
          "fileCabinet".creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          "fileCabinet".modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          operational.file_cabinet "fileCabinet"
        LEFT JOIN
          experiment.occurrence occurrence ON "fileCabinet".occurrence_id = occurrence.id
        LEFT JOIN
          tenant.person creator ON "fileCabinet".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "fileCabinet".modifier_id = modifier.id
        WHERE
          "fileCabinet".is_void = FALSE
        ${addedOrderString}
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let fileCabinetsFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        fileCabinetsQuery,
        conditionString,
        orderString
      )

    let fileCabinets = await sequelize
      .query(fileCabinetsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await fileCabinets == undefined || await fileCabinets.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let fileCabinetsCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        fileCabinetsQuery,
        conditionString,
        orderString
      )

    let fileCabinetsCount = await sequelize
      .query(fileCabinetsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })

    count = fileCabinetsCount[0].count

    res.send(200, {
      rows: fileCabinets,
      count: count
    })
    return
  }
}