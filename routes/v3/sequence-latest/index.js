/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let {sequelize} = require('../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let germplasmHelper = require('../../../helpers/germplasm/index')

module.exports = {
    /**
     * Gets the last item given a sequence
     * GET /v3/sequence-latest
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    get: async function (req, res, next) {
        let params = req.query
        let schema = params.schema
        let table = params.table
        let field = params.field
        let prefix = params.prefix ?? ''
        let suffix = params.suffix ?? ''
        let pattern = params.pattern

        // TODO: Add validation for schema, table, and field (FUTURE SPRINT)

        // build query
        let sequenceQuery = `
            SELECT 
                ${field}
            FROM 
                ${schema}.${table}
            WHERE  
                ${field} ~ '^${prefix}${pattern}${suffix}$'
                AND ${table}.is_void = FALSE
            ORDER BY ${field} DESC
            LIMIT 1
        `

        // Retrieve record from the database
        let records = await sequelize.query(sequenceQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let record = records[0] ?? {}
        let latest = record[field] ?? null
        let count = 1;
        if (latest == null) count = 0;

        res.send(200, {
			rows: [
                {
                    latest: latest
                }
            ],
			count: count
        })
        return
    }
}