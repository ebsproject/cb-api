/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

//Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

module.exports = {
    //Implementation of POST /v3/parents
    post: async (req, res, next) => {
        //set defaults
        let resultArray = []

        let parentDbId = null
        let parentRole = null
        let parentType = null
        let orderNumber = null
        let experimentDbId = null
        let entryDbId = null
        let occurrenceDbId = null
        let entryListDbId = null

        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let parentsUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/parents'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let recordCount = 0
        let records = []

        try {
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try{
            transaction = await sequelize.transaction({autocommit: false})
            //enter validation and query here
            
            let parentDataValuesArray = []
            let visitedArray  = []
            let description = null

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Check if required columns are in the input
                if ( !record.parentRole || !record.entryListDbId || !record.experimentDbId || !record.entryDbId || !record.occurrenceDbId) {
                    let errMsg = `Required parameters are missing. Ensure that parentRole, entryListDbId, experimentDbId, entryDbId and occurrenceDbId are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                //Validation of required parameters and set values
                //validate parentRole
                if (record.parentRole !== undefined) {
                    parentRole = record.parentRole
                    parentRole = parentRole.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'PARENT_ROLE' AND
                                scaleValue.value ILIKE $$${parentRole}$$
                        )
                    `
                    validateCount += 1
                }

                //validate parentType
                if (record.parentType !== undefined) {
                    parentType = record.parentType
                    parentType = parentType.trim()
                    
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'PARENT_TYPE' AND
                                scaleValue.value ILIKE $$${parentType}$$
                        )
                    `
                    validateCount += 1
                }

                //validate experimentDbId
                if (record.experimentDbId !== undefined) {
                    experimentDbId = record.experimentDbId

                    if (!validator.isInt(experimentDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400025)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry list is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.experiment experiment
                            WHERE 
                                experiment.is_void = FALSE AND
                                experiment.id = ${experimentDbId}
                        )
                    `
                    validateCount += 1
                }

                //validation catch
                if (record.entryDbId !== undefined) {
                     entryDbId = record.entryDbId
                
                    if (!validator.isInt(entryDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400020)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if experiment list is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry ent
                            WHERE 
                                ent.is_void = FALSE AND
                                ent.id = ${entryDbId}
                        )
                    `
                    validateCount += 1
                }
                //validate occurrenceDbId
                if (record.occurrenceDbId !== undefined) {
                    occurrenceDbId = record.occurrenceDbId

                    if (!validator.isInt(occurrenceDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate occurrence list Id
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if list is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.occurrence occ
                            WHERE 
                                occ.is_void = FALSE AND
                                occ.id = ${occurrenceDbId}
                        )
                    `
                    validateCount += 1
                }

                //validate entryListDbId
                if (record.entryListDbId !== undefined) {
                    entryListDbId = record.entryListDbId

                    if (!validator.isInt(entryListDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate occurrence list Id
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if list is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry_list entl
                            WHERE 
                                entl.is_void = FALSE AND
                                entl.id = ${entryListDbId}
                        )
                    `
                    validateCount += 1
                }

                description = (record.description !== undefined) ? 
                record.description.trim() : null

                //orderNumber
                let maxOrderNumberQuery = `
                SELECT
                    CASE WHEN
                        (count(1) = 0)
                        THEN 1
                        ELSE MAX(order_number) 
                    END AS "maxOrderNumber",
                    count(id) AS "parentCount"
                FROM 
                    experiment.parent
                WHERE entry_list_id = ${entryListDbId}
                `

                let maxOrderNumber = await sequelize.query(maxOrderNumberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (maxOrderNumber[0].parentCount == 0) {
                    orderNumber+=1
                } else {
                    orderNumber = parseInt(maxOrderNumber[0].maxOrderNumber)+1
        
                    if (records.indexOf(record) > 0) {
                        orderNumber = visitedArray[visitedArray.length-1]+1
                    }
                }
        
                // Check duplicates if recordCount > 1
                if (recordCount > 1) {
                    if (visitedArray.includes(orderNumber)) {
                        orderNumber+=1
                    } else {
                        visitedArray.push(orderNumber)
                    }
                }

                // Validation of input query in database
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [
                    orderNumber, parentRole, parentType, description, entryListDbId, experimentDbId, 
                    occurrenceDbId, entryDbId, personDbId
                ]
  
                parentDataValuesArray.push(tempArray)
    
            }

            //create new parent record here
            let parentDataQuery = format(`
                INSERT INTO experiment.parent (
                    order_number, parent_role, parent_type, description, entry_list_id, experiment_id, 
                    occurrence_id, entry_id, creator_id
                )
                VALUES
                %L
                RETURNING id`, parentDataValuesArray
            )

            let parentData = await sequelize.query(parentDataQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            for (var parentDatum of parentData[0]) {
                parentDbId = parentDatum.id

                let parentDataRecord = {
                    parentDbId: parentDbId,
                    recordCount: 1,
                    href: parentsUrlString + '/' + parentDbId
                }

                resultArray.push(parentDataRecord)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch(e){
            // Rollback transaction if any errors were encountered
            if (e) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
