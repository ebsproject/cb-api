/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let userValidator = require('../../../../helpers/person/validator.js')
let format = require('pg-format')

module.exports = {

    // Endpoint for updating an existing parent record
    // PUT /v3/parents/:id
    put: async function (req, res, next) {
        // Retrieve parent ID
        let parentDbId = req.params.id

        let parentType = null
        let parentRoles = null
        let description = null

        if (!validator.isInt(parentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400106)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve parent record
        let parentQuery = `
            SELECT
                parent_role AS parentRole,
                parent_type AS parentType,
                description AS description
            FROM
                experiment.parent
            WHERE
                id = ${parentDbId}
        `
        let parent = await sequelize.query(parentQuery,{
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return null
            })

        // Return if database query got an error
        if (await parent === null) { return }

        // Return error if resource is not found
        if (await parent === undefined || await parent.length < 1) {
            let errMsg = `The parent you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let isSecure = forwarded(req, req.headers).secure
        // Set URL for response
        let parentUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/parents"

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let recordParentType = parent[0].parentType || null
        let recordParentRole =  parent[0].parentRole || null
        let recordDescription = parent[0].description || null

        let data = req.body
        let transaction

        try {
            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values */
            if (data.parentType !== undefined) {
                parentType = data.parentType
                parentType = parentType.trim()

                // Check if user input is same with the current value in the database
                if(parentType != recordParentType) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if parent type is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'PARENT_TYPE' AND
                                scaleValue.value ILIKE $$${parentType}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        parent_type = $$${parentType}$$
                    `
                }
            }

            if (data.parentRole !== undefined) {
                parentRole = data.parentRole
                parentRole = parentRole.trim()

                // Check if user input is same with the current value in the database
                if(parentRole != recordParentRole) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if parent role is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'PARENT_ROLE' AND
                                scaleValue.value ILIKE $$${parentRole}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        parent_role = $$${parentRole}$$
                    `
                }
            }

            if (data.description !== undefined) {
                description = data.description.trim()
        
                if (description != recordDescription) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        description = $$${description}$$
                    `
                }
            }

            /** Validation of input in database */
            // Count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the parent record
            let updateParentQuery = `
                UPDATE
                    experiment.parent  
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${parentDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updateParentQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let resultArray = {
                parentDbId: parentDbId,
                recordCount: 1,
                href: parentUrlString + '/' + parentDbId
            }
            
            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return

        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for deleting an existing parent record
    // DELETE /v3/parents/:id
    delete: async function (req, res, next) {
        // Retrieve the parent ID
        let parentDbId = req.params.id

        //validation for parent ID
        if (!validator.isInt(parentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400106)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve the user ID of the client from the access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        
        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let parentQuery = `
                SELECT
                    creator.id AS "creatorDbId"
                FROM
                    experiment.parent "parent"
                LEFT JOIN
                    tenant.person creator ON creator.id = "parent".creator_id
                WHERE
                    "parent".is_void = FALSE AND
                    "parent".id = ${parentDbId}
            `

            let parent = await sequelize.query(parentQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (parent === undefined || parent.length < 1) {
                let errMsg = `The parent you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = parent[0].creatorDbId
            let recordExperimentDbId = parent[0].experimentDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                        LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                        LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                        LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                        LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    WHERE 
                        experiment.id = ${recordExperimentDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteParentQuery = format(`
                UPDATE
                    experiment.parent "parent"
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${parentDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    "parent".id = ${parentDbId}
            `)

            await sequelize.query(deleteParentQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { parentDbId: parentDbId }
            })
            return

        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}