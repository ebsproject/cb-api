/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require("../../../../config/sequelize");
let { knex } = require("../../../../config/knex");
let errors = require("restify-errors");
let validator = require("validator");
let errorBuilder = require("../../../../helpers/error-builder");
let processQueryHelper = require("../../../../helpers/processQuery/index");

module.exports = {
  // Endpoint for retrieving the shipment mls-ancestors
  // POST /v3/shipments/{id}/mls-ancestors
  post: async function (req, res, next) {
    // try {
    let shipmentDbId = req.params.id;
    let limit = req.paginate.limit;
    let offset = req.paginate.offset;
    let sort = req.query.sort;
    // let params = req.query;
    let count = 0;
    let orderString = "";
    let conditionString = "";
    let addedConditionString = "";
    let addedDistinctString = "";
    let addedOrderString = `
            ORDER BY
                shipment_item_number
        `;
    let parameters = {};
    parameters["distinctOn"] = "";

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400056);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters["fields"] = req.body.fields;
      }

      if (req.body.distinctOn != undefined) {
        parameters["distinctOn"] = req.body.distinctOn;
        addedDistinctString = await processQueryHelper.getDistinctString(
          parameters["distinctOn"]
        );

        parameters["distinctOn"] = `"${parameters["distinctOn"]}"`;
        addedOrderString = `
                    ORDER BY
                    ${parameters["distinctOn"]}
                `;
      }
      // Set columns to be excluded in parameters for filtering
      let excludedParametersArray = ["fields", "distinctOn"];
      // Get the filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      );

      if (conditionString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    // Build the base retrieval query
    let shipmentMlsAncestorsQuery = null;

    // Check if the client specified values for the fields
    if (parameters["fields"] != null) {
      if (parameters["distinctOn"]) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters["fields"]
        );
        let selectString = knex.raw(`${addedConditionString} ${fieldsString}`);
        shipmentMlsAncestorsQuery = knex.select(selectString);
      } else {
        shipmentMlsAncestorsQuery = knex.column(
          parameters["fields"].split("|")
        );
      }

      shipmentMlsAncestorsQuery += `
        FROM
          (
            SELECT
              distinct  regexp_split_to_table(mls_ancestors,',') as mlsancestors,
              shipment_item_number,
              shipment_id,
              is_void
            FROM inventory.shipment_item
            WHERE
              shipment_id=(:shipmentDbId) AND
              mta_status in('PUD1','pud1','PUD','pud')
              AND shipment_item_status != 'cancel'
              AND is_void = false
            GROUP BY
              id,regexp_split_to_table(mls_ancestors,',')
            ORDER BY
              shipment_item_number,mlsAncestors
          ) AS AncestorTable
          WHERE
            is_void = FALSE
            ${addedOrderString}
            `;
    } else {
      shipmentMlsAncestorsQuery = `
        SELECT
            ${addedDistinctString}
            mlsancestors,
            shipment_item_number
        FROM
        (
          SELECT
            distinct  regexp_split_to_table(mls_ancestors,',') as mlsancestors,
            shipment_item_number,
            shipment_id,
            is_void
          FROM inventory.shipment_item
          WHERE
            shipment_id=(:shipmentDbId) AND
            mta_status in('PUD1','pud1','PUD','pud')
            AND shipment_item_status != 'cancel'
            AND is_void = false
          GROUP BY
            id,regexp_split_to_table(mls_ancestors,',')
          ORDER BY
            shipment_item_number,mlsAncestors
        ) AS AncestorTable
        WHERE
          is_void = FALSE
          ${addedOrderString}

            `;
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort);
      if (orderString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    // Generate the final SQL query
    let shipmentsAncestorsFinalSqlQuery =
      await processQueryHelper.getFinalSqlQuery(
        shipmentMlsAncestorsQuery,
        conditionString,
        orderString
      );

    // Retrieve mls ancestors from the database
    let mlsAncestors = await sequelize
      .query(shipmentsAncestorsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          shipmentDbId: shipmentDbId,
          limit: limit,
          offset: offset,
        },
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    if ((await mlsAncestors) == undefined || (await mlsAncestors.length) < 1) {
      res.send(200, {
        rows: [],
        count: 0,
      });
      return;
    }

    let shipmentsCountFinalSqlQuery =
      await processQueryHelper.getCountFinalSqlQuery(
        shipmentMlsAncestorsQuery,
        conditionString,
        orderString
      );

    let shipmentsAncestorsCount = await sequelize
      .query(shipmentsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          shipmentDbId: shipmentDbId,
          limit: limit,
          offset: offset,
        },
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      });

    count = shipmentsAncestorsCount[0].count;

    res.send(200, {
      rows: mlsAncestors,
      count: count,
    });
    return;
  },
};
