/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let validator = require("validator");
let tokenHelper = require("../../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let projectHelper = require("../../../../helpers/project/index.js");
let errorBuilder = require("../../../../helpers/error-builder");

module.exports = {
  // Endpoint for retrieving the list of shipmentItems
  // GET /v3/shipments/{id}
  get: async function (req, res, next) {
    let shipmentDbId = req.params.id;
    let count = 0;

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400056);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Build query
    let shipmentItemsQuery = `
          SELECT
            shipment_item.mta_status as "smtaStatus",
            shipment_item.smta_id as "smtaId"
          FROM inventory.shipment_item shipment_item
          WHERE 
            shipment_item.shipment_id=(:shipmentDbId) AND
            shipment_item.is_void = FALSE 
          GROUP BY 
            shipment_item.mta_status,shipment_item.smta_id 
          ORDER BY shipment_item.mta_status
        `;

    // Retrieve project from the database
    let shipmentItems = await sequelize
      .query(shipmentItemsQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          shipmentDbId: shipmentDbId,
        },
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    // Return error if resource is not found
    if (
      (await shipmentItems) === undefined ||
      (await shipmentItems.length) < 1
    ) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404017);
      res.send(new errors.NotFoundError(errMsg));
      return;
    }

    res.send(200, {
      rows: shipmentItems,
    });
    return;
  },
};
