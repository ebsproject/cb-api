/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let validator = require("validator");
let tokenHelper = require("../../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let projectHelper = require("../../../../helpers/project/index.js");
let errorBuilder = require("../../../../helpers/error-builder");

module.exports = {
  // Endpoint for retrieving the smta needed details
  // GET /v3/shipments/{id}/smta-report
  get: async function (req, res, next) {
    let shipmentDbId = req.params.id;
    let smtaId = req.params.smtaId;
    let count = 0;

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400056);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Build query
    let shipmentQuery = `
          SELECT recipient_name AS "recipientName",
            shu_reference_number AS "shuReferenceNumber",
            recipient_address AS "recipientAddress",
            recipient_institution AS "recipientInstitution",
            recipient_country AS "recipientCountry",
            authorized_signatory AS "authorizedSignatory",
 		        (SELECT distinct string_agg(mls_ancestors, ',' ORDER BY mls_ancestors) AS mlsAncestors
                FROM (
			            SELECT distinct  regexp_split_to_table(mls_ancestors,',')  	as mls_ancestors
                  FROM inventory.shipment_item
                  WHERE
              shipment_id=${shipmentDbId} AND  is_void=false and shipment_item_status != 'cancel' AND shipment_item.smta_id='${smtaId}'
                GROUP BY id,regexp_split_to_table(mls_ancestors,',')
                ORDER BY mls_ancestors) as mls_ancestors) as mls_ancestors,

            (SELECT  translate(array_agg(distinct concat(
			          CASE WHEN package.package_label LIKE 'IRGC%'  THEN 
			            package.package_label
			          ELSE
			            seed.seed_code
			          END,							   
		        '(',germplasm.designation,')'))::text,'{}""','') 
	              FROM inventory.shipment_item shipment_item 
	              LEFT JOIN germplasm.germplasm germplasm on germplasm.id= shipment_item.germplasm_id 
	              LEFT JOIN germplasm.package package on package.id= shipment_item.package_id
                LEFT JOIN germplasm.seed seed on seed.id = shipment_item.seed_id
	              WHERE shipment_item.shipment_id=${shipmentDbId} and shipment_item.is_void=false and shipment_item.shipment_item_status != 'cancel' AND shipment_item.smta_id='${smtaId}') as materials,
            
            (SELECT translate(array_agg(distinct concat(seed.seed_code,'(',germplasm.designation,')'))::text,'{}""','') as materials
	              FROM inventory.shipment_item shipment_item 
	              LEFT JOIN germplasm.germplasm germplasm on germplasm.id= shipment_item.germplasm_id 
	              LEFT JOIN germplasm.package package on package.id= shipment_item.package_id
                LEFT JOIN germplasm.seed seed on seed.id = shipment_item.seed_id
	              WHERE shipment_item.shipment_id=${shipmentDbId} and shipment_item.is_void=false and shipment_item.shipment_item_status != 'cancel' AND shipment_item.smta_id='${smtaId}') as material_not_gb,
            
            (SELECT translate(array_agg(distinct concat(package.package_label ,'(',germplasm.designation,')'))::text,'{}""','') as materials_gb
	              FROM inventory.shipment_item shipment_item 
	              LEFT JOIN germplasm.germplasm germplasm on germplasm.id= shipment_item.germplasm_id 
	              LEFT JOIN germplasm.package package on package.id= shipment_item.package_id
                LEFT JOIN germplasm.seed seed on seed.id = shipment_item.seed_id
	              WHERE shipment_item.shipment_id=${shipmentDbId} and shipment_item.is_void=false and shipment_item.shipment_item_status != 'cancel' AND shipment_item.smta_id='${smtaId}') as material_gb

          FROM 
            inventory.shipment 
          WHERE 
            id=${shipmentDbId} AND is_void=false
        `;

    // Retrieve shipment info from the database
    let smtaReport = await sequelize
      .query(shipmentQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    // Return error if resource is not found
    if ((await smtaReport) === undefined || (await smtaReport.length) < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404017);
      res.send(new errors.NotFoundError(errMsg));
      return;
    }

    res.send(200, {
      rows: smtaReport,
    });
    return;
  },
};
