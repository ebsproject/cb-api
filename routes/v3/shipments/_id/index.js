/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let validator = require("validator");
let tokenHelper = require("../../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let projectHelper = require("../../../../helpers/project/index.js");
let errorBuilder = require("../../../../helpers/error-builder");

module.exports = {
  // Endpoint for retrieving the list of shipments
  // GET /v3/shipments/{id}
  get: async function (req, res, next) {
    let shipmentDbId = req.params.id;
    let count = 0;

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400056);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Build query
    let shipmentsQuery = `
      SELECT
        shipment.id AS "shipmentDbId",
        shipment.program_id AS "programId",
        shipment.shipment_code AS "shipmentCode",
        shipment.shipment_name AS "shipmentName",
        shipment.shu_reference_number AS "shuReferenceNumber",
        shipment.shipment_status AS "shipmentStatus",
        shipment.shipment_tracking_status AS "shipmentTrackingStatus",
        shipment.shipment_transaction_type AS "shipmentTransactionType",
        shipment.shipment_type AS "shipmentType",
        shipment.shipment_purpose AS "shipmentPurpose",
        shipment.entity_id AS "entityId",
        shipment.material_type AS "materialType",
        shipment.material_subtype AS "materialSubtype",
        shipment.total_item_count AS "totalItemCount",
        shipment.total_package_count AS "totalPackageCount",
        shipment.total_package_weight AS "totalPackageWeight",
        shipment.package_unit AS "packageUnit",
        shipment.document_generated_date AS "documentGeneratedDate",
        shipment.document_signed_date AS "documentSignedDate",
        shipment.authorized_signatory AS "authorizedSignatory",
        shipment.shipped_date AS "shippedDate",
        shipment.airway_bill_number AS "airwayBillNumber",
        shipment.received_date AS "receivedDate",
        shipment.processor_name AS "processorName",
        shipment.processor_id AS "processorId",
        shipment.sender_name AS "senderName",
        shipment.sender_email AS "senderEmail",
        shipment.sender_address AS "senderAddress",
        shipment.sender_institution AS "senderInstitution",
        shipment.sender_facility AS "senderFacility",
        shipment.sender_country AS "senderCountry",
        shipment.recipient_type AS "recipientType",
        shipment.recipient_name AS "recipientName",
        shipment.recipient_email AS "recipientEmail",
        shipment.recipient_address AS "recipientAddress",
        shipment.recipient_institution AS "recipientInstitution",
        shipment.recipient_facility AS "recipientFacility",
        shipment.recipient_country AS "recipientCountry",
        shipment.requestor_name AS "requestorName",
        shipment.requestor_email AS "requestorEmail",
        shipment.requestor_address AS "requestorAddress",
        shipment.requestor_institution AS "requestorInstitution",
        shipment.requestor_country AS "requestorCountry",
        shipment.shipment_remarks AS "shipmentRemarks",
        shipment.tracking_remarks AS "trackingRemarks",
        shipment.dispatch_remarks AS "dispatchRemarks",
        shipment.acknowledgment_remarks AS "acknowledgmentRemarks",
        shipment.remarks AS "remarks",
        shipment.creator_id AS "creatorDbId",
        shipment.modifier_id AS "modifierDbId",
        shipment.notes  AS "notes"
      FROM
        inventory.shipment shipment
      WHERE
        shipment.id = (:shipmentDbId) AND
        shipment.is_void = FALSE 
      `;

    // Retrieve project from the database
    let shipments = await sequelize
      .query(shipmentsQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          shipmentDbId: shipmentDbId,
        },
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    // Return error if resource is not found
    if ((await shipments) === undefined || (await shipments.length) < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404017);
      res.send(new errors.NotFoundError(errMsg));
      return;
    }

    res.send(200, {
      rows: shipments,
    });
    return;
  },

  // Endpoint for updating an existing shipment record
  // PUT /v3/shipments/:id
  put: async function (req, res, next) {
    // Retrieve the shipment ID
    let shipmentDbId = req.params.id;

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = `Invalid format shipment ID must be an integer.`;
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Set defaults
    let programDbId = null;
    let projectId = null;
    let shipmentCode = null;
    let shipmentName = null;
    let shuReferenceNumber = null;
    let shipmentStatus = null;
    let shipmentTrackingStatus = null;
    let shipmentTransactionType = null;
    let shipmentType = null;
    let shipmentPurpose = null;
    let entityId = null;
    let materialType = null;
    let materialSubtype = null;
    let totalItemCount = null;
    let totalPackageCount = null;
    let totalPackageWeight = null;
    let packageUnit = null;
    let documentGeneratedDate = null;
    let documentSignedDate = null;
    let authorizedSignatory = null;
    let shippedDate = null;
    let airwayBillNumber = null;
    let receivedDate = null;
    let processorName = null;
    let processorId = null;
    let senderName = null;
    let senderEmail = null;
    let senderAddress = null;
    let senderInstitution = null;
    let senderFacility = null;
    let senderCountry = null;
    //let senderId =  null ;
    //let senderAddressId =  null ;
    //let senderInstitutionId =  null ;
    //let senderFacilityId =  null ;
    //let senderCountryId =  null ;
    let recipientType = null;
    let recipientName = null;
    let recipientEmail = null;
    let recipientAddress = null;
    let recipientInstitution = null;
    let recipientFacility = null;
    let recipientCountry = null;
    let requestorName = null;
    let requestorEmail = null;
    let requestorAddress = null;
    let requestorInstitution = null;
    let requestorCountry = null;
    let shipmentRemarks = null;
    let trackingRemarks = null;
    let dispatchRemarks = null;
    let acknowledgmentRemarks = null;
    let remarks = null;
    let creatorId = null;
    let modifierId = null;
    let notes = null;

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req);

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Check if shipment is existing
    // Build query
    let shipmentQuery = `
      SELECT
        shipment.id AS "shipmentDbId",
        shipment.program_id AS "programDbId",
        shipment.shipment_code AS "shipmentCode",
        shipment.shipment_name AS "shipmentName",
        shipment.shu_reference_number AS "shuReferenceNumber",
        shipment.shipment_status AS "shipmentStatus",
        shipment.shipment_tracking_status AS "shipmentTrackingStatus",
        shipment.shipment_transaction_type AS "shipmentTransactionType",
        shipment.shipment_type AS "shipmentType",
        shipment.shipment_purpose AS "shipmentPurpose",
        shipment.entity_id AS "entityId",
        shipment.material_type AS "materialType",
        shipment.material_subtype AS "materialSubtype",
        shipment.total_item_count AS "totalItemCount",
        shipment.total_package_count AS "totalPackageCount",
        shipment.total_package_weight AS "totalPackageWeight",
        shipment.package_unit AS "packageUnit",
        shipment.document_generated_date AS "documentGeneratedDate",
        shipment.document_signed_date AS "documentSignedDate",
        shipment.authorized_signatory AS "authorizedSignatory",
        shipment.shipped_date AS "shippedDate",
        shipment.airway_bill_number AS "airwayBillNumber",
        shipment.received_date AS "receivedDate",
        shipment.processor_name AS "processorName",
        shipment.processor_id AS "processorId",
        shipment.sender_name AS "senderName",
        shipment.sender_email AS "senderEmail",
        shipment.sender_address AS "senderAddress",
        shipment.sender_institution AS "senderInstitution",
        shipment.sender_facility AS "senderFacility",
        shipment.sender_country AS "senderCountry",
        shipment.recipient_type AS "recipientType",
        shipment.recipient_name AS "recipientName",
        shipment.recipient_email AS "recipientEmail",
        shipment.recipient_address AS "recipientAddress",
        shipment.recipient_institution AS "recipientInstitution",
        shipment.recipient_facility AS "recipientFacility",
        shipment.recipient_country AS "recipientCountry",
        shipment.requestor_name AS "requestorName",
        shipment.requestor_email AS "requestorEmail",
        shipment.requestor_address AS "requestorAddress",
        shipment.requestor_institution AS "requestorInstitution",
        shipment.requestor_country AS "requestorCountry",
        shipment_remarks AS "shipmentRemarks",
        shipment.tracking_remarks AS "trackingRemarks",
        shipment.dispatch_remarks AS "dispatchRemarks",
        shipment.acknowledgment_remarks AS "acknowledgmentRemarks",
        shipment.remarks AS "remarks",
        shipment.creator_id AS "creatorDbId",
        shipment.modifier_id AS "modifierDbId",
        shipment.notes  AS "notes"
      FROM
        inventory.shipment shipment
      WHERE
        shipment.is_void = FALSE AND
        id = ${shipmentDbId}
      `;

    // Retrieve shipment from the database
    let shipment = await sequelize
      .query(shipmentQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    if ((await shipment) === undefined || (await shipment.length) < 1) {
      let errMsg = `The shipment you have requested does not exist.`;
      res.send(new errors.NotFoundError(errMsg));
      return;
    }

    // Get values from database, record = the current value in the database
    let recordProgramDbId = shipment[0].programDbId;
    //let recordprojectId = shipment[0].programDbId;
    let recordShipmentCode = shipment[0].shipmentCode;
    let recordShipmentName = shipment[0].shipmentName;
    let recordShuReferenceNumber = shipment[0].shuReferenceNumber;
    let recordShipmentStatus = shipment[0].shipmentStatus;
    let recordShipmentTrackingStatus = shipment[0].shipmentTrackingStatus;
    let recordShipmentTransactionType = shipment[0].shipmentTransactionType;
    let recordShipmentType = shipment[0].shipmentType;
    let recordShipmentPurpose = shipment[0].shipmentPurpose;
    let recordEntityId = shipment[0].entityId;
    let recordMaterialType = shipment[0].materialType;
    let recordMaterialSubtype = shipment[0].materialSubtype;
    let recordTotalItemCount = shipment[0].totalItemCount;
    let recordTotalPackageCount = shipment[0].totalPackageCount;
    let recordTotalPackageWeight = shipment[0].totalPackageWeight;
    let recordPackageUnit = shipment[0].packageUnit;
    let recordDocumentGeneratedDate = shipment[0].documentGeneratedDate;
    let recordDocumentSignedDate = shipment[0].documentSignedDate;
    let recordAuthorizedSignatory = shipment[0].authorizedSignatory;
    let recordShippedDate = shipment[0].shippedDate;
    let recordAirwayBillNumber = shipment[0].airwayBillNumber;
    let recordReceivedDate = shipment[0].receivedDate;
    let recordProcessorName = shipment[0].processorName;
    let recordProcessorId = shipment[0].processorId;
    let recordSenderName = shipment[0].senderName;
    let recordSenderEmail = shipment[0].senderEmail;
    let recordSenderAddress = shipment[0].senderAddress;
    let recordSenderInstitution = shipment[0].senderInstitution;
    let recordSenderFacility = shipment[0].senderFacility;
    let recordSenderCountry = shipment[0].senderCountry;
    let recordRecipientType = shipment[0].recipientType;
    let recordRecipientName = shipment[0].recipientName;
    let recordRecipientEmail = shipment[0].recipientEmail;
    let recordRecipientAddress = shipment[0].recipientAddress;
    let recordRecipientInstitution = shipment[0].recipientInstitution;
    let recordRecipientFacility = shipment[0].recipientFacility;
    let recordRecipientCountry = shipment[0].recipientCountry;
    let recordRequestorName = shipment[0].requestorName;
    let recordRequestorEmail = shipment[0].requestorEmail;
    let recordRequestorAddress = shipment[0].requestorAddress;
    let recordRequestorInstitution = shipment[0].requestorInstitution;
    let recordRequestorCountry = shipment[0].requestorCountry;
    let recordShipmentRemarks = shipment[0].shipmentRemarks;
    let recordTrackingRemarks = shipment[0].trackingRemarks;
    let recordDispatchRemarks = shipment[0].dispatchRemarks;
    let recordAcknowledgmentRemarks = shipment[0].acknowledgmentRemarks;
    let recordRemarks = shipment[0].remarks;
    let recordCreatorId = shipment[0].creatorId;
    let recordModifierId = shipment[0].modifierId;
    let recordNotes = shipment[0].notes;

    // add additional fields

    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipments/" +
      shipmentDbId;

    // Check is req.body has parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Parse the input
    let data = req.body;
    // Start transaction
    let transaction;
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false });

      let setQuery = ``;
      let validateQuery = ``;
      let validateCount = 0;

      /** Validation of parameters and set values **/

      if (data.programDbId !== undefined) {
        programDbId = data.programDbId;

        if (!validator.isInt(programDbId)) {
          let errMsg = "Invalid request, program ID must be an integer.";
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (programDbId != recordProgramDbId) {
          // Validate season ID
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            --- Check if program is existing return 1
            SELECT 
                count(1)
            FROM
                tenant.program program
            WHERE 
                program.is_void = FALSE AND
                program.id = ${programDbId}
            )
            `;
          validateCount += 1;

          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            program_id = $$${programDbId}$$
          `;
        }
      } else {
        programDbId = recordProgramDbId;
      }

      // validate shipmentCode
      if (data.shipmentCode !== undefined) {
        shipmentCode = data.shipmentCode.trim();

        // Check if user input is same with the current value in the database
        if (shipmentCode != recordShipmentCode) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipment_code = $$${shipmentCode}$$
          `;
        }
      }

      // validate shipmentName
      if (data.shipmentName !== undefined) {
        shipmentName = data.shipmentName.trim();

        // Check if user input is same with the current value in the database
        if (shipmentName != recordShipmentName) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipment_name = $$${shipmentName}$$
          `;
        }
      }

      // validate shuReferenceNumber
      if (data.shuReferenceNumber !== undefined) {
        shuReferenceNumber = data.shuReferenceNumber.trim();

        // Check if user input is same with the current value in the database
        if (shuReferenceNumber != recordShuReferenceNumber) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shu_reference_number = $$${shuReferenceNumber}$$
          `;
        }
      }

      // validate shipmentStatus
      if (data.shipmentStatus !== undefined) {
        shipmentStatus = data.shipmentStatus.trim();

        // Check if user input is same with the current value in the database
        if (shipmentStatus != recordShipmentStatus) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipment_status = $$${shipmentStatus}$$
          `;
        }
      }

      // validate recordShipmentStatus
      if (data.shipmentTrackingStatus !== undefined) {
        shipmentTrackingStatus = data.shipmentTrackingStatus.trim();

        // Check if user input is same with the current value in the database
        if (shipmentTrackingStatus != recordShipmentTrackingStatus) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipment_tracking_status = $$${shipmentTrackingStatus}$$
          `;
        }
      }

      // validate shipmentType
      if (data.shipmentType !== undefined) {
        shipmentType = data.shipmentType.trim();

        // Check if user input is same with the current value in the database
        if (shipmentType != recordShipmentType) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipment_type = $$${shipmentType}$$
          `;
        }
      }

      // validate shipmentPurpose
      if (data.shipmentPurpose !== undefined) {
        shipmentPurpose = data.shipmentPurpose.trim();

        // Check if user input is same with the current value in the database
        if (shipmentPurpose != recordShipmentPurpose) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipment_purpose = $$${shipmentPurpose}$$
          `;
        }
      }

      // validate entityId
      if (data.entityId !== undefined) {
        entityId = data.entityId;

        // Check if user input is same with the current value in the database
        if (entityId != recordEntityId) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            entity_id = $$${entityId}$$
          `;
        }
      }

      // validate materialType
      if (data.materialType !== undefined) {
        materialType = data.materialType.trim();

        // Check if user input is same with the current value in the database
        if (materialType != recordMaterialType) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            material_type = $$${materialType}$$
          `;
        }
      }

      // validate material_subtype
      if (data.materialSubtype !== undefined) {
        materialSubtype = data.materialSubtype.trim();

        // Check if user input is same with the current value in the database
        if (materialSubtype != recordMaterialSubtype) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            material_subtype = $$${materialSubtype}$$
          `;
        }
      }

      // validate total_item_count
      if (data.totalItemCount !== undefined) {
        totalItemCount = data.totalItemCount;

        // Check if user input is same with the current value in the database
        if (totalItemCount != recordTotalItemCount) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            total_item_count = $$${totalItemCount}$$
          `;
        }
      }

      // validate totalPackageCount
      if (data.totalPackageCount !== undefined) {
        totalPackageCount = data.totalPackageCount;

        // Check if user input is same with the current value in the database
        if (totalPackageCount != recordTotalPackageCount) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            total_package_count = $$${totalPackageCount}$$
          `;
        }
      }

      // validate totalPackageWeight
      if (data.totalPackageWeight !== undefined) {
        totalPackageWeight = data.totalPackageWeight;

        // Check if user input is same with the current value in the database
        if (totalPackageWeight != recordTotalPackageWeight) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            total_package_weight = $$${totalPackageWeight}$$
          `;
        }
      }

      // validate package_unit
      if (data.packageUnit !== undefined) {
        packageUnit = data.packageUnit.trim();

        // Check if user input is same with the current value in the database
        if (packageUnit != recordPackageUnit) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            package_unit = $$${packageUnit}$$
          `;
        }
      }

      // validate documentGeneratedDate
      if (data.documentGeneratedDate !== undefined) {
        documentGeneratedDate = data.documentGeneratedDate;

        var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
        if (!documentGeneratedDate.match(validDateFormat)) {
          // Invalid format
          let errMsg = `Invalid format, Document Generated Date  must be in format YYYY-MM-DD.`;
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (documentGeneratedDate != recordDocumentGeneratedDate) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            document_generated_date = $$${documentGeneratedDate}$$
          `;
        }
      }

      // validate document_signed_date
      if (data.documentSignedDate !== undefined) {
        documentSignedDate = data.documentSignedDate;

        var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
        if (!documentSignedDate.match(validDateFormat)) {
          // Invalid format
          let errMsg = `Invalid format, Document Generated Date  must be in format YYYY-MM-DD.`;
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (documentSignedDate != recordDocumentSignedDate) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            document_signed_date = $$${documentSignedDate}$$
          `;
        }
      }

      // validate recordAuthorizedSignatory
      if (data.authorizedSignatory !== undefined) {
        authorizedSignatory = data.authorizedSignatory.trim();

        // Check if user input is same with the current value in the database
        if (authorizedSignatory != recordAuthorizedSignatory) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            authorized_signatory = $$${authorizedSignatory}$$
          `;
        }
      }

      // validate shipment date
      if (data.shippedDate !== undefined) {
        shippedDate = data.shippedDate;

        var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
        if (!shippedDate.match(validDateFormat)) {
          // Invalid format
          let errMsg = `Invalid format, shipment date must be in format YYYY-MM-DD.`;
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (shippedDate != recordShippedDate) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipped_date = $$${shippedDate}$$
          `;
        }
      }

      // validate airway_bill_number
      if (data.airwayBillNumber !== undefined) {
        airwayBillNumber = data.airwayBillNumber.trim();

        // Check if user input is same with the current value in the database
        if (airwayBillNumber != recordAirwayBillNumber) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            airway_bill_number = $$${airwayBillNumber}$$
          `;
        }
      }

      // validate received_date date
      if (data.receivedDate !== undefined) {
        receivedDate = data.receivedDate;

        var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
        if (!receivedDate.match(validDateFormat)) {
          // Invalid format
          let errMsg = `Invalid format, shipment date must be in format YYYY-MM-DD.`;
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Check if user input is same with the current value in the database
        if (receivedDate != recordReceivedDate) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            received_date = $$${receivedDate}$$
          `;
        }
      }

      // validate processor_name
      if (data.processorName !== undefined) {
        processorName = data.processorName.trim();

        // Check if user input is same with the current value in the database
        if (processorName != recordProcessorName) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            processor_name = $$${processorName}$$
          `;
        }
      }

      // validate processorId
      if (data.processorId !== undefined) {
        processorId = data.processorId;

        // Check if user input is same with the current value in the database
        if (processorId != recordProcessorId) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            processor_id = $$${processorId}$$
          `;
        }
      }

      // validate sender_name
      if (data.senderName !== undefined) {
        senderName = data.senderName.trim();

        // Check if user input is same with the current value in the database
        if (senderName != recordSenderName) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            sender_name = $$${senderName}$$
          `;
        }
      }

      // validate sender_email
      if (data.senderEmail !== undefined) {
        senderEmail = data.senderEmail.trim();

        // Check if user input is same with the current value in the database
        if (senderEmail != recordSenderEmail) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            sender_email = $$${senderEmail}$$
          `;
        }
      }

      // validate sender_address
      if (data.senderAddress !== undefined) {
        senderAddress = data.senderAddress.trim();

        // Check if user input is same with the current value in the database
        if (senderAddress != recordSenderAddress) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            sender_address = $$${senderAddress}$$
          `;
        }
      }

      // validate sender_institution
      if (data.senderInstitution !== undefined) {
        senderInstitution = data.senderInstitution.trim();

        // Check if user input is same with the current value in the database
        if (senderInstitution != recordSenderInstitution) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            sender_institution = $$${senderInstitution}$$
          `;
        }
      }

      // validate sender_facility
      if (data.senderFacility !== undefined) {
        senderFacility = data.senderFacility.trim();

        // Check if user input is same with the current value in the database
        if (senderFacility != recordSenderFacility) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            sender_facility = $$${senderFacility}$$
          `;
        }
      }

      // validate sender_country
      if (data.senderCountry !== undefined) {
        senderCountry = data.senderCountry.trim();

        // Check if user input is same with the current value in the database
        if (senderCountry != recordSenderCountry) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            sender_country = $$${senderCountry}$$
          `;
        }
      }

      // validate recipient_type
      if (data.recipientType !== undefined) {
        recipientType = data.recipientType.trim();

        // Check if user input is same with the current value in the database
        if (recipientType != recordRecipientType) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            recipient_type = $$${recipientType}$$
          `;
        }
      }

      // validate recipient_name
      if (data.recipientName !== undefined) {
        recipientName = data.recipientName.trim();

        // Check if user input is same with the current value in the database
        if (recipientName != recordRecipientName) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            recipient_name = $$${recipientName}$$
          `;
        }
      }

      // validate recipient_email
      if (data.recipientEmail !== undefined) {
        recipientEmail = data.recipientEmail.trim();

        // Check if user input is same with the current value in the database
        if (recipientEmail != recordRecipientEmail) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            recipient_email = $$${recipientEmail}$$
          `;
        }
      }

      // validate recipient_address
      if (data.recipientAddress !== undefined) {
        recipientAddress = data.recipientAddress.trim();

        // Check if user input is same with the current value in the database
        if (recipientAddress != recordRecipientAddress) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            recipient_address = $$${recipientAddress}$$
          `;
        }
      }

      // validate recipient_institution
      if (data.recipientInstitution !== undefined) {
        recipientInstitution = data.recipientInstitution.trim();

        // Check if user input is same with the current value in the database
        if (recipientInstitution != recordRecipientInstitution) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            recipient_institution = $$${recipientInstitution}$$
          `;
        }
      }

      // validate recipient_facility
      if (data.recipientFacility !== undefined) {
        recipientFacility = data.recipientFacility.trim();

        // Check if user input is same with the current value in the database
        if (recipientFacility != recordRecipientFacility) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            recipient_facility = $$${recipientFacility}$$
          `;
        }
      }

      // validate recipient_country
      if (data.recipientCountry !== undefined) {
        recipientCountry = data.recipientCountry.trim();

        // Check if user input is same with the current value in the database
        if (recipientCountry != recordRecipientCountry) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            recipient_country = $$${recipientCountry}$$
          `;
        }
      }

      // validate requestor_name
      if (data.requestorName !== undefined) {
        requestorName = data.requestorName.trim();

        // Check if user input is same with the current value in the database
        if (requestorName != recordRequestorName) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            requestor_name = $$${requestorName}$$
          `;
        }
      }

      // validate requestor_email
      if (data.requestorEmail !== undefined) {
        requestorEmail = data.requestorEmail.trim();

        // Check if user input is same with the current value in the database
        if (requestorEmail != recordRequestorEmail) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            requestor_email = $$${requestorEmail}$$
          `;
        }
      }

      // validate requestor_address
      if (data.requestorAddress !== undefined) {
        requestorAddress = data.requestorAddress.trim();

        // Check if user input is same with the current value in the database
        if (requestorAddress != recordRequestorAddress) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            requestor_address = $$${requestorAddress}$$
          `;
        }
      }

      // validate requestor_institution
      if (data.requestorInstitution !== undefined) {
        requestorInstitution = data.requestorInstitution.trim();

        // Check if user input is same with the current value in the database
        if (requestorInstitution != recordRequestorInstitution) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            requestor_institution = $$${requestorInstitution}$$
          `;
        }
      }

      // validate requestor_country
      if (data.requestorCountry !== undefined) {
        requestorCountry = data.requestorCountry.trim();

        // Check if user input is same with the current value in the database
        if (requestorCountry != recordRequestorCountry) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            requestor_country = $$${requestorCountry}$$
          `;
        }
      }

      // validate shipment_remarks
      if (data.shipmentRemarks !== undefined) {
        shipmentRemarks = data.shipmentRemarks.trim();

        // Check if user input is same with the current value in the database
        if (shipmentRemarks != recordShipmentRemarks) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            shipment_remarks = $$${shipmentRemarks}$$
          `;
        }
      }

      // validate tracking_remarks
      if (data.trackingRemarks !== undefined) {
        trackingRemarks = data.trackingRemarks.trim();

        // Check if user input is same with the current value in the database
        if (trackingRemarks != recordTrackingRemarks) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            tracking_remarks = $$${trackingRemarks}$$
          `;
        }
      }

      // validate dispatch_remarks
      if (data.dispatchRemarks !== undefined) {
        dispatchRemarks = data.dispatchRemarks.trim();

        // Check if user input is same with the current value in the database
        if (dispatchRemarks != recordDispatchRemarks) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            dispatch_remarks = $$${dispatchRemarks}$$
          `;
        }
      }

      // validate acknowledgment_remarks
      if (data.acknowledgmentRemarks !== undefined) {
        acknowledgmentRemarks = data.acknowledgmentRemarks.trim();

        // Check if user input is same with the current value in the database
        if (acknowledgmentRemarks != recordAcknowledgmentRemarks) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            acknowledgment_remarks = $$${acknowledgmentRemarks}$$
          `;
        }
      }

      // validate remarks
      if (data.remarks !== undefined) {
        remarks = data.remarks.trim();

        // Check if user input is same with the current value in the database
        if (remarks != recordRemarks) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            remarks = $$${remarks}$$
          `;
        }
      }

      // validate creator_id
      if (data.creator_id !== undefined) {
        creatorId = data.creatorId;

        // Check if user input is same with the current value in the database
        if (creatorId != recordCreatorId) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            creator_id = $$${creatorId}$$
          `;
        }
      }

      // validate modifierId
      if (data.modifierId !== undefined) {
        modifierId = data.modifierId;

        // Check if user input is same with the current value in the database
        if (modifierId != recordModifierId) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            modifier_id = $$${modifierId}$$
          `;
        }
      }

      // validate notes
      if (data.notes !== undefined) {
        notes = data.notes.trim();

        // Check if user input is same with the current value in the database
        if (notes != recordNotes) {
          setQuery += setQuery != "" ? "," : "";
          setQuery += `
            notes = $$${notes}$$
          `;
        }
      }

      /** Validation of input in database */
      // count must be equal to validateCount if all conditions are met
      if (validateCount > 0) {
        let checkInputQuery = `
              SELECT
                ${validateQuery}
              AS count
              `;

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT,
        });

        if (inputCount[0]["count"] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015);
          res.send(new errors.BadRequestError(errMsg));
          return;
        }
      }

      if (setQuery.length > 0) setQuery += `,`;

      // Update the shipment record
      let updateshipmentQuery = `
            UPDATE
                inventory.shipment  
            SET
                ${setQuery}
                modification_timestamp = NOW(),
                modifier_id = ${personDbId}
            WHERE
                id = ${shipmentDbId}
            `;
      await sequelize.query(updateshipmentQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      // Return the transaction info
      let resultArray = {
        shipmentDbId: shipmentDbId,
        recordCount: 1,
        href: shipmentUrlString,
      };

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: resultArray,
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500003);
      res.send(new errors.InternalError(errMsg));
      return;
    }
  },

  // Implementation of delete call for /v3/shipments/{id}
  delete: async function (req, res, next) {
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Defaults
    let shipmentDbId = req.params.id;
    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipments";

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = "Invalid format, shipments ID must be an integer";
      res.send(new errors.BadRequestError(errMsg));
      return;
    }
    // Start transaction
    let transaction;

    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false });
      // Build query
      let shipmentQuery = `
        SELECT
          count(1)
        FROM 
          inventory.shipment shipment
        WHERE
          shipment.is_void = FALSE AND
          shipment.id = ${shipmentDbId}
      `;

      // Retrieve shipment from the database
      let hasShipment = await sequelize
        .query(shipmentQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      // Check if shipment is non-existent in database
      if (hasShipment == undefined || hasShipment < 1) {
        let errMsg = `The shipment you requested does not exist.`;
        res.send(new errors.NotFoundError(errMsg));
        return;
      }

      // Build query for voiding
      let deleteShipmentQuery = `
        UPDATE
          inventory.shipment shipment
        SET
          is_void = TRUE,
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          shipment.id = ${shipmentDbId}
        `;

      await sequelize.query(deleteShipmentQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      let deleteShipmentItemQuery = `
        UPDATE
          inventory.shipment_item shipment_item
        SET
          is_void = TRUE,
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          shipment_item.shipment_id = ${shipmentDbId}
        `;

      await sequelize.query(deleteShipmentItemQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: {
          shipmentDbId: shipmentDbId,
          recordCount: 1,
          href: shipmentUrlString + "/" + shipmentDbId,
        },
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500002);
      res.send(new errors.InternalError(errMsg));
      return;
    }
  },
};
