/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let validator = require("validator");
let tokenHelper = require("../../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let projectHelper = require("../../../../helpers/project/index.js");
let errorBuilder = require("../../../../helpers/error-builder");

module.exports = {
  // Endpoint for updating an existing mls status of shipment item record
  // POST /v3/shipment/:id
  post: async function (req, res, next) {
    // Retrieve the shipment item ID
    let shipmentDbId = req.params.id;
    let smtaType = req.params.smtaType;
    let smtaId = req.params.smtaId;

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = `Invalid format, shipment ID must be an integer.`;
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Retrieve person ID of client from access token
    let personDbId = await tokenHelper.getUserId(req);

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Check if shipment is existing
    // Build query
    let shipmentsQuery = `
      SELECT
          shipment.id AS "shipmentDbId"
      FROM
          inventory.shipment shipment
      WHERE
          shipment.is_void = FALSE AND
          id = ${shipmentDbId}
      `;

    // Retrieve shipment from the database
    let shipment = await sequelize
      .query(shipmentsQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    if ((await shipment) === undefined || (await shipment.length) < 1) {
      let errMsg = `The shipment you have requested does not exist.`;
      res.send(new errors.NotFoundError(errMsg));
      return;
    }

    // add additional fields

    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipments/" +
      shipmentDbId;

    // Check is req.body has parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Start transaction
    let transaction;

    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false });
      // Build query
      let shipmentItemsQuery = `
        SELECT
          count(1)
        FROM 
          inventory.shipment_item shipment_item
        WHERE
          shipment_item.is_void = FALSE AND
          shipment_id = ${shipmentDbId}
      `;

      // Retrieve shipment item from the database
      let hasShipmentItems = await sequelize
        .query(shipmentItemsQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      // Check if shipment is non-existent in database
      if (hasShipmentItems == undefined || hasShipmentItems < 1) {
        let errMsg = `The shipment item you requested does not exist.`;
        res.send(new errors.NotFoundError(errMsg));
        return;
      }
      let updateshipmentItemsQuery = "";

      // Build query for voiding
      if (smtaType == "mls") {
        updateshipmentItemsQuery = `
          UPDATE
            inventory.shipment_item shipment_item
          SET
            smta_id = '${smtaId}',
            modification_timestamp = NOW(),
            modifier_id = ${personDbId}
          WHERE
            shipment_id = ${shipmentDbId}  and 
            mta_status IN ('FAO', 'Rel1', 'SMTA', 'MLS','fao','rel1','smta','mls')
          `;
      } else if (smtaType == "pud1") {
        updateshipmentItemsQuery = `
          UPDATE
            inventory.shipment_item shipment_item
          SET
            smta_id = '${smtaId}',
            modification_timestamp = NOW(),
            modifier_id = ${personDbId}
          WHERE
            shipment_id = ${shipmentDbId}  and 
            mta_status IN ('PUD1', 'PUD','pud1','pud')
          `;
      } else if (smtaType == "pud") {
        updateshipmentItemsQuery = `
          UPDATE
            inventory.shipment_item shipment_item
          SET
            smta_id = '${smtaId}',
            modification_timestamp = NOW(),
            modifier_id = ${personDbId}
          WHERE
            shipment_id = ${shipmentDbId}  and 
            mta_status IN ('PUD2', 'pud2')
          `;
      } else if (smtaType == "other") {
        updateshipmentItemsQuery = `
          UPDATE
            inventory.shipment_item shipment_item
          SET
            smta_id = '${smtaId}',
            modification_timestamp = NOW(),
            modifier_id = ${personDbId}
          WHERE
            shipment_id = ${shipmentDbId} 
          `;
      }

      await sequelize.query(updateshipmentItemsQuery, {
        type: sequelize.QueryTypes.UPDATE,
      });

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: {
          shipmentDbId: shipmentDbId,
          recordCount: 1,
          href: shipmentUrlString,
        },
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500001);
      res.send(new errors.InternalError(errMsg));
      return;
    }
  },
};
