/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let errorBuilder = require("../../../../helpers/error-builder");
let tokenHelper = require("../../../../helpers/auth/token");
let validator = require("validator");
let userValidator = require("../../../../helpers/person/validator.js");
let format = require("pg-format");

module.exports = {
  /**
   * POST /v3/shipment/:id/shipment-items-populate allows the population of list-member in shipment items
   *
   *
   * @param listDbId integer as a path parameter
   * @param token string token
   *
   * @return object response data
   */

  post: async function (req, res, next) {
    // Retrieve list-member ID
    let shipmentDbId = req.params.id;
    let listDbId = req.params.listDbId;

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400025);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Retrieve user ID of client from access token
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.UnauthorizedError(errMsg));
      return;
    }

    let isAdmin = await userValidator.isAdmin(userDbId);

    let transaction;
    try {
      transaction = await sequelize.transaction({ autocommit: false });

      let listMemberQuery = `
        SELECT 
          germplasm.id AS germplasm_id,
          seed.id AS seed_id,
          package.id AS package_id,
          list_member.order_number AS order_number,
          germplasm_mta.use AS mta_use,
          germplasm_mta.genetic_stock AS genetic_stock,
          (select mta_status from germplasm.germplasm_mta where is_void=FALSE and seed_id=seed.id limit 1) as mta_status,
          (select mls_ancestors from germplasm.germplasm_mta where is_void=FALSE and seed_id=seed.id limit 1) as mls_ancestors,
          (select mta_status from germplasm.germplasm_mta where is_void=FALSE and germplasm_id=seed.germplasm_id and seed_id is null limit 1) as mta_status2,
          (select mls_ancestors from germplasm.germplasm_mta where is_void=FALSE and germplasm_id=seed.germplasm_id limit 1) as mls_ancestors2,
          (select data_value from germplasm.seed_data where variable_id=(Select id from master.variable where abbrev='RSHT_NO') AND seed_id=seed.id order by id desc limit 1) as testcode
        FROM platform.list  as list
        LEFT JOIN platform.list_member as list_member on list.id=list_member.list_id
        LEFT JOIN germplasm.package as package on list_member.data_id =package.id
        LEFT JOIN germplasm.seed as seed on package.seed_id =seed.id
        LEFT JOIN germplasm.germplasm as germplasm on seed.germplasm_id =germplasm.id
        LEFT JOIN germplasm.germplasm_mta as germplasm_mta on seed.id = germplasm_mta.seed_id
        WHERE list_member.list_id = ${listDbId} AND list_member.is_void = FALSE AND list_member.is_active = TRUE order by list_member.order_number
        `;

      // Retrieve list member from the database
      let listMembers = await sequelize
        .query(listMemberQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      if ((await listMembers) === undefined || (await listMembers.length) < 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404008);
        res.send(new errors.NotFoundError(errMsg));
        return;
      }

      // check the shipmentItem last order number
      let itemCounter = 0;
      let shipmentItemsListQuery = `
        SELECT
          max(shipment_item_number) as shipment_item_number
        FROM 
          inventory.shipment_item shipment_item
        WHERE
          shipment_item.is_void = FALSE AND
          shipment_id = ${shipmentDbId}
      `;

      // Retrieve list member from the database
      let shipmentItems = await sequelize
        .query(shipmentItemsListQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      if (shipmentItems[0]["shipment_item_number"] != null) {
        itemCounter = shipmentItems[0]["shipment_item_number"];
      }

      let resultArray = [];
      let shipmentItemValuesArray = [];

      for (var item of listMembers) {
        itemCounter++;
        let germplasmDbId = item.germplasm_id;
        let seedDbId = item.seed_id;
        let packageDbId = item.package_id;
        let shipmentItemNumber = itemCounter;
        let shipmentItemStatus = "passed";
        let shipmentItemWeight = 0;
        let packageUnit = "g";
        let packageCount = 0;
        let testcode = item.testcode;
        let mtaStatus = "";
        if (item.mta_status != null) {
          mtaStatus = item.mta_status;
        } else {
          mtaStatus = item.mta_status2;
        }
        let mlsAncestors = "";
        if (item.mls_ancestors != null) {
          mlsAncestors = item.mls_ancestors;
        } else {
          mlsAncestors = item.mls_ancestors2;
        }
        let use = item.use;
        let geneticStock = item.genetic_stock;
        let remarks = "";
        let creatorId = userDbId;
        let modifierId = userDbId;
        let notes = "";

        // Get values
        let tempArray = [
          shipmentDbId,
          germplasmDbId,
          seedDbId,
          packageDbId,
          shipmentItemNumber,
          shipmentItemStatus,
          shipmentItemWeight,
          packageUnit,
          packageCount,
          testcode,
          mtaStatus,
          use,
          mlsAncestors,
          geneticStock,
          remarks,
          userDbId,
          modifierId,
          notes,
        ];

        shipmentItemValuesArray.push(tempArray);
      }

      // // Create shipment item record
      let shipmentItemQuery = format(
        `
        INSERT INTO inventory.shipment_item (
          shipment_id,
          germplasm_id,
          seed_id,
          package_id,
          shipment_item_number,
          shipment_item_status,
          shipment_item_weight,
          package_unit,
          package_count,
          test_code,
          mta_status,
          use,
          mls_ancestors,
          genetic_stock,
          remarks,
          creator_id,
          modifier_id,
          notes
        )
        VALUES
          %L
        RETURNING
          id`,
        shipmentItemValuesArray
      );

      await sequelize.query(shipmentItemQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction,
      });

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: resultArray,
        count: resultArray.length,
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500004);
      res.send(new errors.InternalError(err));
      return;
    }
  },
};
