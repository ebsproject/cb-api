/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let errorBuilder = require("../../../../helpers/error-builder");
let tokenHelper = require("../../../../helpers/auth/token");
let validator = require("validator");
let userValidator = require("../../../../helpers/person/validator.js");
let format = require("pg-format");

module.exports = {
  /**
   * POST /v3/shipment/:id/update-list allows the update of test_code,mta_status,mls_ancestors in shipment items
   *
   *
   * @param shipmentDbId integer as a path parameter
   * @param token string token
   *
   * @return object response data
   */

  post: async function (req, res, next) {
    // Retrieve shipment ID
    let shipmentDbId = req.params.id;

    if (!validator.isInt(shipmentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400025);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Retrieve user ID of client from access token
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.UnauthorizedError(errMsg));
      return;
    }

    let isAdmin = await userValidator.isAdmin(userDbId);

    let transaction;
    try {
      let shipmentMemberQuery = `
        
        SELECT 
		      shipment_item.id as shipment_item_id,
          (select genetic_stock from germplasm.germplasm_mta where seed_id = shipment_item.seed_id limit 1) as genetic_stock,
          (select mta_status from germplasm.germplasm_mta where seed_id = shipment_item.seed_id limit 1) as mta_status,
          (select mls_ancestors from germplasm.germplasm_mta where is_void=FALSE and seed_id=shipment_item.seed_id limit 1) as mls_ancestors,
          (select mta_status from germplasm.germplasm_mta where is_void=FALSE and germplasm_id=shipment_item.germplasm_id limit 1) as mta_status2,
          (select mls_ancestors from germplasm.germplasm_mta where is_void=FALSE and germplasm_id=shipment_item.germplasm_id limit 1) as mls_ancestors2,
          (select data_value from germplasm.seed_data where variable_id=(Select id from master.variable where abbrev='RSHT_NO') AND seed_id=shipment_item.seed_id order by id desc limit 1) as testcode
        FROM inventory.shipment_item  as shipment_item
        LEFT JOIN germplasm.germplasm_mta as germplasm_mta on shipment_item.seed_id = germplasm_mta.seed_id
        WHERE shipment_item.shipment_id = ${shipmentDbId} AND shipment_item.is_void = FALSE order by shipment_item.shipment_item_number
        `;

      // Retrieve list member from the database
      let shipmentItems = await sequelize
        .query(shipmentMemberQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      if (
        (await shipmentItems) === undefined ||
        (await shipmentItems.length) < 1
      ) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404008);
        res.send(new errors.NotFoundError(errMsg));
        return;
      }

      for (var item of shipmentItems) {
        transaction = await sequelize.transaction({ autocommit: false });
        let testcode = item.testcode;
        let genetic_stock = item.genetic_stock;
        let mtaStatus = "";
        if (item.mta_status != null) {
          mtaStatus = item.mta_status;
        } else {
          mtaStatus = item.mta_status2;
        }
        let mlsAncestors = "";
        if (item.mls_ancestors != null) {
          mlsAncestors = item.mls_ancestors;
        } else {
          mlsAncestors = item.mls_ancestors2;
        }
        let modifierId = userDbId;

        // // Update shipment item record
        let shipmentItemUpdateQuery = format(
          `
          Update inventory.shipment_item set
          genetic_stock ='${genetic_stock}',
          mta_status ='${mtaStatus}',
          mls_ancestors = '${mlsAncestors}',
          test_code='${testcode}',
          modifier_id = ${modifierId} where id = ${item.shipment_item_id}
        `
        );

        await sequelize.query(shipmentItemUpdateQuery, {
          type: sequelize.QueryTypes.UPDATE,
          transaction: transaction,
        });
        // Commit the transaction
        await transaction.commit();
      }

      res.send(200, {
        shipmentDbId: shipmentDbId,
        shipmentItemCount: shipmentItems.length,
      });
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500004);
      res.send(new errors.InternalError(err));
      return;
    }
  },
};
