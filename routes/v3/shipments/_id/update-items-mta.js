/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../../config/sequelize");
let errors = require("restify-errors");
let errorBuilder = require("../../../../helpers/error-builder");
let tokenHelper = require("../../../../helpers/auth/token");
let validator = require("validator");
let userValidator = require("../../../../helpers/person/validator.js");
let format = require("pg-format");

module.exports = {
  /**
   * POST /v3/shipment/:id/update-items-mta allows the updating of mta status in shipment items
   *
   *
   * @param shipmentDbId integer as a path parameter
   * @param token string token
   *
   * @return object response data
   */

  post: async function (req, res, next) {
    // Retrieve list-member ID
    let shipmentDbId = req.params.id;
    // let listDbId = req.params.listDbId;
    if (!validator.isInt(shipmentDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400025);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Retrieve user ID of client from access token
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.UnauthorizedError(errMsg));
      return;
    }

    let isAdmin = await userValidator.isAdmin(userDbId);
    let transaction;

    try {
      let listMemberQuery = `
          SELECT 
            shipment_item.id as id,shipment_item.seed_id as seed_id,shipment_item.germplasm_id as gid,
		        (select mta_status mtaStatus_Gid from germplasm.germplasm_mta where germplasm_id=shipment_item.germplasm_id and is_void =false order by id desc limit 1) as mtaStatus_Gid ,
		        (select mls_ancestors mlsAncestor_Gid from germplasm.germplasm_mta where germplasm_id=shipment_item.germplasm_id and is_void =false  order by id desc limit 1) as mlsAncestor_Gid,
		        (select mta_status mtaStatus_SeedId from germplasm.germplasm_mta where seed_id=shipment_item.seed_id and is_void =false order by id desc limit 1) as mtaStatus_SeedId,
		        (select mls_ancestors mlsAncestor_SeedId from germplasm.germplasm_mta where seed_id=shipment_item.seed_id and is_void =false order by id desc limit 1) mlsAncestor_SeedId
          FROM inventory.shipment_item shipment_item
          WHERE shipment_item.shipment_id =  ${shipmentDbId} and shipment_item.is_void=FALSE
        `;

      // Retrieve list member from the database
      let shipmentItems = await sequelize
        .query(listMemberQuery, {
          type: sequelize.QueryTypes.SELECT,
        })
        .catch(async (err) => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004);
          res.send(new errors.InternalError(errMsg));
          return;
        });

      if (
        (await shipmentItems) === undefined ||
        (await shipmentItems.length) < 1
      ) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404008);
        res.send(new errors.NotFoundError(errMsg));
        return;
      }

      //   let recordGermplasmDbId = listMember[0].germplasmDbId;

      let resultArray = [];

      transaction = await sequelize.transaction({ autocommit: false });

      for (var item of shipmentItems) {
        let shipmentItemDbId = item.id;
        let mtaStatus = "";
        let mlsAncestors = "";

        if (item.mtastatus_seedid != null) {
          mtaStatus = item.mtastatus_seedid;
          mlsAncestors = item.mlsancestor_seedid;
        } else {
          mtaStatus = item.mtastatus_gid;
          mlsAncestors = item.mlsancestor_gid;
        }

        // // Update shipment item record
        let shipmentItemQuery = `UPDATE
          inventory.shipment_item shipment_item
        SET
          mta_status='${mtaStatus}',
          mls_ancestors='${mlsAncestors}',
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          shipment_item.id = ${shipmentItemDbId}
        `;

        await sequelize.query(shipmentItemQuery, {
          type: sequelize.QueryTypes.UPDATE,
          transaction: transaction,
        });
      }

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: {
          shipmentDbId: shipmentDbId,
          recordCount: shipmentItems.length,
        },
      });
      return;
    } catch (err) {
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500004);
      res.send(new errors.InternalError(err));
      return;
    }
  },
};
