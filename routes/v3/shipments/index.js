/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require("../../../config/sequelize");
let errors = require("restify-errors");
let tokenHelper = require("../../../helpers/auth/token.js");
let forwarded = require("forwarded-for");
let format = require("pg-format");
let errorBuilder = require("../../../helpers/error-builder");

module.exports = {
  // Endpoint for adding a shipment record
  post: async function (req, res, next) {
    let resultArray = [];
    // Retrieve the ID of the client via the access token
    let userDbId = await tokenHelper.getUserId(req);

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    let isSecure = forwarded(req, req.headers).secure;

    // Set the URL for response
    let shipmentUrlString =
      (isSecure ? "https" : "http") +
      "://" +
      req.headers.host +
      "/v3/shipments";

    // Check for parameters
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009);
      res.send(new errors.BadRequestError(errMsg));
      return;
    }

    // Parse the input
    let data = req.body;
    let records = [];

    try {
      records = data.records;
      recordCount = records.length;

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004);
      // res.send(new errors.InternalError(errMsg));
      res.send(new errors.InternalError(err));
      return;
    }

    // Start transaction
    let transaction;

    try {
      transaction = await sequelize.transaction({ autocommit: false });

      let shipmentValuesArray = [];
      let visitedArray = [];

      for (var record of records) {
        let programDbId = null;
        let projectId = null;
        let shipmentCode = null;
        let shipmentName = null;
        let shuReferenceNumber = null;
        let shipmentStatus = null;
        let shipmentTrackingStatus = null;
        let shipmentTransactionType = null;
        let shipmentType = null;
        let shipmentPurpose = null;
        let entityId = null;
        let materialType = null;
        let materialSubtype = null;
        let totalItemCount = null;
        let totalPackageCount = null;
        let totalPackageWeight = null;
        let packageUnit = null;
        let documentGeneratedDate = null;
        let documentSignedDate = null;
        let authorizedSignatory = null;
        let shippedDate = null;
        let airwayBillNumber = null;
        let receivedDate = null;
        let processorName = null;
        let processorId = null;
        let senderName = null;
        let senderEmail = null;
        let senderAddress = null;
        let senderInstitution = null;
        let senderFacility = null;
        let senderCountry = null;
        let recipientType = null;
        let recipientName = null;
        let recipientEmail = null;
        let recipientAddress = null;
        let recipientInstitution = null;
        let recipientFacility = null;
        let recipientCountry = null;
        let requestorName = null;
        let requestorEmail = null;
        let requestorAddress = null;
        let requestorInstitution = null;
        let requestorCountry = null;
        let shipmentRemarks = null;
        let trackingRemarks = null;
        let dispatchRemarks = null;
        let acknowledgmentRemarks = null;
        let remarks = null;
        let creatorId = null;
        let modifierId = null;
        let notes = null;

        let validateQuery = ``;
        let validateCount = 0;

        // Check if the required columns are in the input
        if (
          record.programDbId == undefined ||
          record.shipmentName == undefined ||
          record.shipmentTransactionType
        ) {
          let errMsg =
            "Invalid request. Required parameters are missing.Ensure that programDbId,shipmentName  are not empty";
          res.send(new errors.BadRequestError(errMsg));
          return;
        }

        // Validate and set values

        if (record.programDbId !== undefined) {
          programDbId = record.programDbId;
          validateQuery += validateQuery != "" ? " + " : "";
          validateQuery += `
            (
            -- Check if program ID is existing, return 1
            SELECT
                count(1)
            FROM
                tenant.program program
            WHERE
                program.is_void = FALSE AND
                program.id = ${programDbId}
            )
            `;

          validateCount += 1;
        }

        if (record.documentGeneratedDate !== undefined) {
          documentGeneratedDate = record.documentGeneratedDate;
          // validate
          var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
          if (!documentGeneratedDate.match(validDateFormat)) {
            // Invalid format
            let errMsg =
              "Invalid format, document Generated Date  must be in format YYYY-MM-DD.";
            let errorCode = await responseHelper.getErrorCodebyMessage(
              400,
              errMsg
            );
            errMsg = await errorBuilder.getError(
              req.headers.host,
              errorCode,
              errMsg
            );
            res.send(new errors.BadRequestError(errMsg));
            return;
          }
        }

        if (record.documentSignedDate !== undefined) {
          documentSignedDate = record.documentSignedDate;
          // validate
          var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
          if (!documentSignedDate.match(validDateFormat)) {
            // Invalid format
            let errMsg =
              "Invalid format, document Generated Date  must be in format YYYY-MM-DD.";
            let errorCode = await responseHelper.getErrorCodebyMessage(
              400,
              errMsg
            );
            errMsg = await errorBuilder.getError(
              req.headers.host,
              errorCode,
              errMsg
            );
            res.send(new errors.BadRequestError(errMsg));
            return;
          }
        }

        if (record.shippedDate !== undefined) {
          shippedDate = record.shippedDate;
          // validate
          var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
          if (!shippedDate.match(validDateFormat)) {
            // Invalid format
            let errMsg =
              "Invalid format, document Generated Date  must be in format YYYY-MM-DD.";
            let errorCode = await responseHelper.getErrorCodebyMessage(
              400,
              errMsg
            );
            errMsg = await errorBuilder.getError(
              req.headers.host,
              errorCode,
              errMsg
            );
            res.send(new errors.BadRequestError(errMsg));
            return;
          }
        }

        if (record.receivedDate !== undefined) {
          receivedDate = record.receivedDate;
          // validate
          var validDateFormat = /^\d{4}-\d{2}-\d{2}$/;
          if (!receivedDate.match(validDateFormat)) {
            // Invalid format
            let errMsg =
              "Invalid format, document Generated Date  must be in format YYYY-MM-DD.";
            let errorCode = await responseHelper.getErrorCodebyMessage(
              400,
              errMsg
            );
            errMsg = await errorBuilder.getError(
              req.headers.host,
              errorCode,
              errMsg
            );
            res.send(new errors.BadRequestError(errMsg));
            return;
          }
        }

        shipmentCode =
          record.shipmentCode != undefined ? record.shipmentCode : null;
        shipmentName =
          record.shipmentName != undefined ? record.shipmentName : null;
        shuReferenceNumber =
          record.shuReferenceNumber != undefined
            ? record.shuReferenceNumber
            : null;
        shipmentStatus =
          record.shipmentStatus != undefined ? record.shipmentStatus : null;
        shipmentTrackingStatus =
          record.shipmentTrackingStatus != undefined
            ? record.shipmentTrackingStatus
            : null;
        shipmentTransactionType =
          record.shipmentTransactionType != undefined
            ? record.shipmentTransactionType
            : null;
        shipmentType =
          record.shipmentType != undefined ? record.shipmentType : null;
        shipmentPurpose =
          record.shipmentPurpose != undefined ? record.shipmentPurpose : null;
        entityId = record.entityId != undefined ? record.entityId : null;
        materialType =
          record.materialType != undefined ? record.materialType : null;
        materialSubtype =
          record.materialSubtype != undefined ? record.materialSubtype : null;
        totalItemCount =
          record.totalItemCount != undefined ? record.totalItemCount : null;
        totalPackageCount =
          record.totalPackageCount != undefined
            ? record.totalPackageCount
            : null;
        totalPackageWeight =
          record.totalPackageWeight != undefined
            ? record.totalPackageWeight
            : null;
        documentGeneratedDate =
          record.documentGeneratedDate != undefined
            ? record.documentGeneratedDate
            : null;
        documentSignedDate =
          record.documentSignedDate != undefined
            ? record.documentSignedDate
            : null;
        authorizedSignatory =
          record.authorizedSignatory != undefined
            ? record.authorizedSignatory
            : null;
        shippedDate =
          record.shippedDate != undefined ? record.shippedDate : null;
        airwayBillNumber =
          record.airwayBillNumber != undefined ? record.airwayBillNumber : null;
        receivedDate =
          record.receivedDate != undefined ? record.receivedDate : null;
        processorName =
          record.processorName != undefined ? record.processorName : null;
        processorId =
          record.processorId != undefined ? record.processorId : null;
        senderName = record.senderName != undefined ? record.senderName : null;
        senderEmail =
          record.senderEmail != undefined ? record.senderEmail : null;
        senderAddress =
          record.senderAddress != undefined ? record.senderAddress : null;
        senderInstitution =
          record.senderInstitution != undefined
            ? record.senderInstitution
            : null;
        senderFacility =
          record.senderFacility != undefined ? record.senderFacility : null;
        senderCountry =
          record.senderCountry != undefined ? record.senderCountry : null;
        recipientType =
          record.recipientType != undefined ? record.recipientType : null;
        recipientName =
          record.recipientName != undefined ? record.recipientName : null;
        recipientEmail =
          record.recipientEmail != undefined ? record.recipientEmail : null;
        recipientAddress =
          record.recipientAddress != undefined ? record.recipientAddress : null;
        recipientInstitution =
          record.recipientInstitution != undefined
            ? record.recipientInstitution
            : null;
        recipientFacility =
          record.recipientFacility != undefined
            ? record.recipientFacility
            : null;
        recipientCountry =
          record.recipientCountry != undefined ? record.recipientCountry : null;
        requestorName =
          record.requestorName != undefined ? record.requestorName : null;
        requestorEmail =
          record.requestorEmail != undefined ? record.requestorEmail : null;
        requestorAddress =
          record.requestorAddress != undefined ? record.requestorAddress : null;
        requestorInstitution =
          record.requestorInstitution != undefined
            ? record.requestorInstitution
            : null;
        requestorCountry =
          record.requestorCountry != undefined ? record.requestorCountry : null;
        shipmentRemarks =
          record.shipmentRemarks != undefined ? record.shipmentRemarks : null;
        trackingRemarks =
          record.trackingRemarks != undefined ? record.trackingRemarks : null;
        dispatchRemarks =
          record.dispatchRemarks != undefined ? record.dispatchRemarks : null;
        acknowledgmentRemarks =
          record.acknowledgmentRemarks != undefined
            ? record.acknowledgmentRemarks
            : null;
        remarks = record.remarks != undefined ? record.remarks : null;
        creatorId = record.creatorId != undefined ? record.creatorId : null;
        modifierId = record.modifierId != undefined ? record.modifierId : null;
        notes = record.notes != undefined ? record.notes : null;

        // count must be equal to validateCount if all conditions are met
        let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `;

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT,
        });

        if (inputCount[0]["count"] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015);
          res.send(
            new errors.BadRequestError("checkInputQuery: " + checkInputQuery)
          );
          return;
        }

        // Check for duplicates if the record count > 1
        if (recordCount > 1) {
          if (visitedArray.includes(shipmentName)) {
            let errMsg =
              "Invalid request. Duplicate shipmentName values were found.";
            res.send(new errors.BadRequestError(errMsg));
            return;
          } else {
            visitedArray.push(shipmentName);
          }
        }
        // Get values
        let tempArray = [
          programDbId,
          projectId,
          shipmentName,
          shuReferenceNumber,
          shipmentStatus,
          shipmentTrackingStatus,
          shipmentTransactionType,
          shipmentType,
          shipmentPurpose,
          entityId,
          materialType,
          materialSubtype,
          totalItemCount,
          totalPackageCount,
          totalPackageWeight,
          documentGeneratedDate,
          documentSignedDate,
          authorizedSignatory,
          shippedDate,
          airwayBillNumber,
          receivedDate,
          processorName,
          processorId,
          senderName,
          senderEmail,
          senderAddress,
          senderInstitution,
          senderFacility,
          senderCountry,
          recipientType,
          recipientName,
          recipientEmail,
          recipientAddress,
          recipientInstitution,
          recipientFacility,
          recipientCountry,
          requestorName,
          requestorEmail,
          requestorAddress,
          requestorInstitution,
          requestorCountry,
          shipmentRemarks,
          trackingRemarks,
          dispatchRemarks,
          acknowledgmentRemarks,
          remarks,
          userDbId,
          modifierId,
          notes,
        ];

        shipmentValuesArray.push(tempArray);
      }

      // Create shipment record
      let shipmentQuery = format(
        `
        INSERT INTO inventory.shipment (
          program_id,
          project_id,
          shipment_name,
          shu_reference_number,
          shipment_status,
          shipment_tracking_status,
          shipment_transaction_type,
          shipment_type,
          shipment_purpose,
          entity_id,
          material_type,
          material_subtype,
          total_item_count,
          total_package_count,
          total_package_weight,
          document_generated_date,
          document_signed_date,
          authorized_signatory,
          shipped_date,
          airway_bill_number,
          received_date,
          processor_name,
          processor_id,
          sender_name,
          sender_email,
          sender_address,
          sender_institution,
          sender_facility,
          sender_country,
          recipient_type,
          recipient_name,
          recipient_email,
          recipient_address,
          recipient_institution,
          recipient_facility,
          recipient_country,
          requestor_name,
          requestor_email,
          requestor_address,
          requestor_institution,
          requestor_country,
          shipment_remarks,
          tracking_remarks,
          dispatch_remarks,
          acknowledgment_remarks,
          remarks,
          creator_id,
          modifier_id,
          notes
        )
        VALUES 
          %L
        RETURNING 
          id`,
        shipmentValuesArray
      );

      let shipments = await sequelize.query(shipmentQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction,
      });

      for (shipment of shipments[0]) {
        shipmentDbId = shipment.id;

        // Return the shipment info
        let shipmentObj = {
          shipmentDbId: shipmentDbId,
          recordCount: 1,
          href: shipmentUrlString + "/" + shipmentDbId,
        };

        resultArray.push(shipmentObj);
      }

      // Commit the transaction
      await transaction.commit();

      res.send(200, {
        rows: resultArray,
      });
      return;
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback();
      let errMsg = await errorBuilder.getError(req.headers.host, 500001);
      res.send(new errors.InternalError("err: " + errMsg));
      return;
    }
  },
};
