/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']
      // Get the filter condition
      conditionString = await processQueryHelper
        .getFilter(req.body, excludedParametersArray)
      
      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let itemModulesQuery = null
    // Check if the client specified values for the field/s
    if (parameters['fields']) {
      itemModulesQuery = knex.column(parameters['fields'].split('|'))
      itemModulesQuery += `
        FROM
          platform.item_module item_module
        LEFT JOIN
          tenant.person creator ON item_module.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON item_module.modifier_id = modifier.id
        WHERE
          item_module.is_void = FALSE
      ` + addedConditionString +
      `
        ORDER BY
          item_module.id
      `
    } else {
      itemModulesQuery = `
        SELECT
          item_module.id AS "itemModuleDbId",
          item_module.item_id AS "itemId",
          item_module.module_id AS "moduleDbId",
          item_module.remarks,
          item_module.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          item_module.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          platform.item_module item_module
        LEFT JOIN
          tenant.person creator ON item_module.creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON item_module.modifier_id = modifier.id
        LEFT JOIN
          master.item item ON item_module.item_id = item.id
        LEFT JOIN
          platform.module module ON item_module.module_id = module.id
        WHERE
          item_module.is_void = FALSE 
      ` + addedConditionString +
      `
        ORDER BY
          item_module.id
      `
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    itemModulesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      itemModulesQuery,
      conditionString,
      orderString
    )
    // Retrieve the item module records
    let itemModules = await sequelize
      .query(itemModulesFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await itemModules == undefined || await itemModules.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get the final count of the place records
      let itemModulesCountFinalSqlQuery = await processQueryHelper
        .getCountFinalSqlQuery(
          itemModulesQuery,
          conditionString,
          orderString
        )

      let itemModulesCount = await sequelize
        .query(itemModulesCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = itemModulesCount[0].count
    }

    res.send(200, {
      rows: itemModules,
      count: count
    })
    return
  }
}