/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let validator = require('validator')

module.exports = {
    post: async (req, res, next) => {
        let count = 0
        let addedConditionString = ''
        let parameters = {}
        let germplasmDbIdArr = []
        let pedigree = []
        let validGermplasmDbId = []
        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            if (req.body.germplasmDbId != null) {
                germplasmDbIdArr = req.body.germplasmDbId.split('|')

            }
        }
        for (germplasmDbId of germplasmDbIdArr) {
            germplasmDbId = germplasmDbId.trim()
            if (!validator.isInt(germplasmDbId)) {
                let errMsg = 'Invalid request, germplasmDbId must be an integer.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }
            // Build query
            let germplasmQuery = `
                SELECT
                *
                FROM
                germplasm.germplasm germplasm
                
                WHERE
                germplasm.id = ${germplasmDbId}
                AND germplasm.is_void = FALSE
            
            `
            let germplasm = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
            if (await germplasm === undefined || await germplasm.length < 1) {
                continue;
            }else{
                validGermplasmDbId.push(germplasmDbId)
            }
            // Build the base retrieval query
            let pedigreeQuery = `
                WITH RECURSIVE tree(id, child, root, depth) AS (
                    SELECT
                        c.id,
                        c.child_germplasm_id,
                        c.parent_germplasm_id,
                        1 AS depth
                    FROM
                        germplasm.germplasm_relation c
                    WHERE 
                        c.is_void = FALSE
                        AND c.child_germplasm_id = ${germplasmDbId}
                    
                    UNION ALL
                    
                    SELECT
                        germplasm_relation.id,
                        child_germplasm_id,
                        parent_germplasm_id,
                        tree.depth + 1
                    FROM
                        tree
                        INNER JOIN germplasm.germplasm_relation 
                            ON germplasm_relation.child_germplasm_id = tree.root
                )
            `
            // Check if the client specified values for the field/s
            if (parameters['fields']) {
                pedigreeQuery += knex.column(parameters['fields'].split('|'))
                pedigreeQuery += `
                    FROM
                        tree
                        LEFT JOIN germplasm.germplasm_relation gr
                            ON gr.id = tree.id
                        LEFT JOIN germplasm.germplasm AS parent_germplasm ON parent_germplasm.id = tree.root
                        LEFT JOIN germplasm.germplasm AS child_germplasm ON child_germplasm.id = tree.child
                    ORDER BY tree.depth, gr.order_number
                ` + addedConditionString
            } else {
                pedigreeQuery += `
                    SELECT
                        parent_germplasm.id AS "parentGermplasmDbId",
                        parent_germplasm.designation AS "parentGermplasmDesignation",
                        child_germplasm.id AS "childGermplasmDbId",
                        child_germplasm.designation AS "childGermplasmDesignation",
                        'Line ' || tree.depth as "lineName",
                        CASE
                            WHEN germplasm_relation.order_number = 1
                                THEN 'Female'
                            ELSE 'Male'
                        END AS "parentGermplasmRole",
                        CASE
                            WHEN germplasm_relation.order_number = 1
                                THEN 'F'
                            ELSE 'M'
                        END AS "parentTypeCode"
                    FROM
                        tree
                        LEFT JOIN germplasm.germplasm_relation gr ON gr.id = tree.id
                        LEFT JOIN germplasm.germplasm AS parent_germplasm ON parent_germplasm.id = tree.root
                        LEFT JOIN germplasm.germplasm AS child_germplasm ON child_germplasm.id = tree.child
                        LEFT JOIN germplasm.germplasm_relation AS germplasm_relation ON germplasm_relation.parent_germplasm_id = parent_germplasm.id
                    ORDER BY tree.depth, gr.order_number
                    `
            }

            // Retrieve the place records
            let pedigreeResult = await sequelize
                .query(pedigreeQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
            pedigree.push({
                germplasmDbId: germplasmDbId,
                pedigree: pedigreeResult
            })
        }

        if (await pedigree == undefined || await pedigree.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {

            count = await validGermplasmDbId.length

            res.send(200, {
                rows: pedigree,
                count: count
            })
            return
        }
    }
}