/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY "experimentProtocol".id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set the columns to be excluded in parameters for filtering
      let excludedParametersArray = [
        'fields',
        'distinctOn',
      ]
      // Get filter condition
      conditionString = await processQueryHelper
        .getFilter(
          req.body,
          excludedParametersArray,
        )

      if (req.body.distinctOn != undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }
    // Build the base retrieval query
    let experimentProtocolQuery = null
      // Check i fthe client specified values for fields
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])
        
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        experimentProtocolQuery = knex.select(selectString)
      } else {
        experimentProtocolQuery = knex.column(parameters['fields'].split('|'))
      }

      experimentProtocolQuery += `
        FROM
          experiment.experiment_protocol "experimentProtocol"
        LEFT JOIN
          experiment.experiment experiment ON "experimentProtocol".experiment_id = experiment.id
        LEFT JOIN
          tenant.protocol protocol ON "experimentProtocol".protocol_id = protocol.id
        LEFT JOIN
          tenant.person creator ON "experimentProtocol".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "experimentProtocol".modifier_id = modifier.id
        WHERE
          "experimentProtocol".is_void = FALSE
        ${addedOrderString}
      `
    } else {
      experimentProtocolQuery = `
        SELECT
          "experimentProtocol".id AS "experimentProtocolDbId",
          "experimentProtocol".order_number AS "orderNo",
          experiment.id AS "experimentDbId",
          experiment.data_process_id AS "dataProcessDbId",
          item.display_name AS "experimentTemplate",
          protocol.id AS "protocolDbId",
          protocol.protocol_code AS "protocolCode",
          protocol.protocol_name AS "protocolName",
          protocol.protocol_type AS "protocolType",
          "experimentProtocol".creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          "experimentProtocol".modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          experiment.experiment_protocol "experimentProtocol"
        LEFT JOIN
          experiment.experiment experiment ON "experimentProtocol".experiment_id = experiment.id
        LEFT JOIN
          master.item item ON experiment.data_process_id = item.id
        LEFT JOIN
          tenant.protocol protocol ON "experimentProtocol".protocol_id = protocol.id
        LEFT JOIN
          tenant.person creator ON "experimentProtocol".creator_id = creator.id
        LEFT JOIN
          tenant.person modifier ON "experimentProtocol".modifier_id = modifier.id
        WHERE
          "experimentProtocol".is_void = FALSE
        ${addedOrderString}
      `
    }
    // Parse thesort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let experimentProtocolFinalSqlQuery = await processQueryHelper
      .getFinalSqlQueryWoLimit(
        experimentProtocolQuery,
        conditionString,
        orderString
      )
    

    let experimentProtocolFinalTotalSqlQuery = await processQueryHelper
      .getFinalTotalCountQuery(
        experimentProtocolFinalSqlQuery
    )

    let experimentProtocol = await sequelize
      .query(experimentProtocolFinalTotalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        await logger.logFailingQuery(endpoint, 'SELECT', err)
        
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await experimentProtocol === undefined || await experimentProtocol.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    count = experimentProtocol[0].totalCount ? parseInt(experimentProtocol[0].totalCount) : 0

    res.send(200, {
      rows: experimentProtocol,
      count: count
    })
    return
  }
}