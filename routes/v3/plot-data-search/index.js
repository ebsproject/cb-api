/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    /**
     * Search method for experiment.plot_data
     * POST /v3/plot-data-search
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let plotDataQuery = null
        // Check if the client specified values for the field/s
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                plotDataQuery = knex.select(selectString)
            } else {
                plotDataQuery = knex.column(parameters['fields'].split('|'))
            }

            plotDataQuery += `
                FROM
                    experiment.plot_data plot_data
                LEFT JOIN
                    experiment.plot plot ON plot.id = plot_data.plot_id
                LEFT JOIN
                    experiment.location location ON location.id = plot.location_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                LEFT JOIN
                    master.variable variable ON variable.id = plot_data.variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = plot_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = plot_data.modifier_id
                WHERE
                    plot_data.is_void = FALSE
            `
        } else {
            plotDataQuery = `
                SELECT
                    ${addedDistinctString}
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_code AS "occurrenceCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    location.id AS "locationDbId",
                    location.location_code AS "locationCode",
                    location.location_name AS "locationName",
                    plot.id AS "plotDbId",  
                    plot.plot_code AS "plotCode",
                    plot.plot_number AS "plotNumber",
                    plot.plot_type AS "plotType",
                    plot.harvest_status AS "harvestStatus",
                    plot_data.id::text AS "plotDataDbId",
                    plot_data.variable_id::text AS "variableDbId",
                    variable.abbrev AS "variableAbbrev",
                    variable.label AS "variableLabel",
                    plot_data.data_value AS "dataValue",
                    plot_data.data_qc_code AS "dataQCCode",
                    plot_data.transaction_id::text AS "transactionDbId",
                    plot_data.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    plot_data.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier,
                    plot_data.collection_timestamp AS "collectionTimestamp",
                    plot_data.remarks
                FROM
                    experiment.plot_data plot_data
                LEFT JOIN
                    experiment.plot plot ON plot.id = plot_data.plot_id
                LEFT JOIN
                    experiment.location location ON location.id = plot.location_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                LEFT JOIN
                    master.variable variable ON variable.id = plot_data.variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = plot_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = plot_data.modifier_id
                WHERE
                    plot_data.is_void = FALSE
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        plotDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plotDataQuery,
            conditionString,
            orderString
        )

        // Retrieve the plot data records
        let plotData = await sequelize.query(plotDataFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await plotData == undefined || await plotData.length < 1) {
            res.send(200, {
                rows: plotData,
                count: 0
            })
            return
        }

        // Get the final count of the plot data records
        let plotDataCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            plotDataQuery,
            conditionString,
            orderString
        )

        let plotDataCount = await sequelize.query(plotDataCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = plotDataCount[0].count

        res.send(200, {
            rows: plotData,
            count: count
        })
        return
    }
}