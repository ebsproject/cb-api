/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let addedOrderString = ''
        let parameters = {}

        parameters['distinctOn'] = ''

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set the columns to he excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]
            // Get filter condition
            conditionString = await processQueryHelper
                .getFilter(
                    req.body,
                    excludedParametersArray
                )

            if (conditionString && conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))

                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                        ${parameters['distinctOn']}
                `
            }
        }

        // Build the base retrieval query
        let scaleConversionsQuery = null
        // Check if the client specified values for fields
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(parameters['fields'])
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                scaleConversionsQuery = knex.select(selectString)
            } else {
                scaleConversionsQuery = knex.column(parameters['fields'].split('|'))
            }

            scaleConversionsQuery += `
                FROM
                    master.scale_conversion scale_conversion
                LEFT JOIN
                    master.scale_value source_scale_value
                ON
                    source_scale_value.id = scale_conversion.source_unit_id
                LEFT JOIN
                    master.scale_value target_scale_value
                ON
                    target_scale_value.id = scale_conversion.target_unit_id
                LEFT JOIN
                    tenant.crop crop
                ON
                    crop.id = scale_conversion.crop_id
                LEFT JOIN
                    tenant.person creator
                ON
                    creator.id = scale_conversion.creator_id
                LEFT JOIN
                    tenant.person modifier
                ON
                    modifier.id = scale_conversion.modifier_id
                ${addedOrderString}
            `
        } else {
            scaleConversionsQuery = `
                SELECT
                    scale_conversion.id AS "scaleConversionDbId",
                    scale_conversion.source_unit_id AS "sourceUnitDbId",
                    source_scale_value.value AS "sourceUnit",
                    scale_conversion.target_unit_id AS "targetUnitDbId",
                    target_scale_value.value AS "targetUnit",
                    scale_conversion.conversion_value AS "conversionValue",
                    scale_conversion.crop_id AS "cropDbId",
                    crop.crop_code AS "cropCode",
                    crop.crop_name AS "cropName",
                    scale_conversion.creation_timestamp AS "creationTimestamp",
                    scale_conversion.creator_id AS "creatorDbId",
                    creator.person_name AS "creator",
                    scale_conversion.modification_timestamp AS "modificationTimestamp",
                    scale_conversion.modifier_id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    master.scale_conversion scale_conversion
                LEFT JOIN
                    master.scale_value source_scale_value
                ON
                    source_scale_value.id = scale_conversion.source_unit_id
                LEFT JOIN
                    master.scale_value target_scale_value
                ON
                    target_scale_value.id = scale_conversion.target_unit_id
                LEFT JOIN
                    tenant.crop crop
                ON
                    crop.id = scale_conversion.crop_id
                LEFT JOIN
                    tenant.person creator
                ON
                    creator.id = scale_conversion.creator_id
                LEFT JOIN
                    tenant.person modifier
                ON
                    modifier.id = scale_conversion.modifier_id
                ${addedOrderString}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString && orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Generate final SQL query
        let scaleConversionsFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                scaleConversionsQuery,
                conditionString,
                orderString,
                addedDistinctString
            )

        let scaleConversions = await sequelize
            .query(scaleConversionsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                return undefined
            })

        if (scaleConversions === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        // If no records were retrieved
        if (scaleConversions.length < 1) {
            let errMsg = 'Resource not found. The scale conversion records you have requested for do not exist.'
            res.send(new errors.NotFoundError(errMsg))

            return
        }

        let scaleConversionsCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                scaleConversionsQuery,
                conditionString,
                '',
                addedDistinctString
            )

        let scaleConversionsCount = await sequelize
            .query(scaleConversionsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                return undefined
            })

        if (scaleConversionsCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        count = scaleConversionsCount[0].count

        res.send(200, {
            rows: scaleConversions,
            count: count
        })

        return
    }
}