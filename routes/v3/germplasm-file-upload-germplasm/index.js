/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require("../../../config/sequelize");
let { knex } = require("../../../config/knex");
let forwarded = require('forwarded-for')
let tokenHelper = require("../../../helpers/auth/token")
let processQueryHelper = require("../../../helpers/processQuery/index");
let errors = require("restify-errors");
let errorBuilder = require("../../../helpers/error-builder");
let format = require("pg-format")
let validator = require("validator")
let logger = require('../../../helpers/logger')

const endpoint = 'germplasm-file-upload-germplasm'
module.exports = {
    // create new germplasm.germplasm_file_upload_germplasm record
    post: async function (req, res, next){
        let fileUploadDbId = null
        let germplasmDbId = null
        let seedDbId = null
        let packageDbId = null
        let remarks = ''
        let entity = null
        let entityDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let germplasmFileUploadGermplasmUrlString =
            (isSecure ? "https" : "http") + "://" +
            req.headers.host + "/v3/germplasm-file-upload-germplasm"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = [] 
        let recordCount = 0

        // check if records is empty
        try{
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(
                    req.headers.host,
                    400005
                )
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        catch(err){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let fileUploadGermplasmArr = []

        let transaction
        let resultArray = []

        try{
            let validateQuery = ``
            let validateCount = 0

            let paramsArr = []
            for(var record of records){
                validateQuery = ``
                validateCount = 0
    
                // check if all required fields are provided
                if( record.fileUploadDbId == undefined ||
                    record.fileUploadDbId == '' ||
                    record.fileUploadDbId == null ){
                    let errMsg = await errorBuilder.getError(req.headers.host, 400240)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // validation of parameters

                // validate fileUploadDbId
                if( record.fileUploadDbId != undefined && record.fileUploadDbId != null){
                    fileUploadDbId = record.fileUploadDbId

                    if(!validator.isInt(fileUploadDbId)){
                        let errMsg = await errorBuilder.getError(req.headers.host, 400246)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += validateQuery != "" ? " + " : ""
                    validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.file_upload gfu
                        WHERE
                            gfu.is_void = FALSE AND
                            gfu.id = ${fileUploadDbId}

                        )`
                    validateCount += 1
                }

                // validate germplasmDbId
                if( record.germplasmDbId != undefined && record.germplasmDbId != null ){
                    germplasmDbId = record.germplasmDbId

                    if(!validator.isInt(germplasmDbId)){
                        let errMsg = await errorBuilder.getError(req.headers.host, 400246)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += validateQuery != "" ? " + " : ""
                    validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.germplasm germplasm
                        WHERE
                            germplasm.is_void = FALSE AND
                            germplasm.id = ${germplasmDbId}

                        )`
                    validateCount += 1
                }

                // validate seedDbId
                if(record.seedDbId != undefined && record.seedDbId != null && record.seedDbId != ""){
                    seedDbId = record.seedDbId

                    if(!validator.isInt(seedDbId)){
                        let errMsg = await errorBuilder.getError(req.headers.host, 400113)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += validateQuery != "" ? " + " : ""
                    validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.seed seed
                        WHERE
                            seed.is_void = FALSE AND
                            seed.id = ${seedDbId}

                        )`
                    validateCount += 1
                }

                // validate packageDbId
                if(record.packageDbId != undefined && record.packageDbId != null && record.packageDbId != ""){
                    packageDbId = record.packageDbId

                    if(!validator.isInt(packageDbId)){
                        let errMsg = await errorBuilder.getError(req.headers.host, 400222)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += validateQuery != "" ? " + " : ""
                    validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.package package
                        WHERE
                            package.is_void = FALSE AND
                            package.id = ${packageDbId}

                        )`
                    validateCount += 1
                }

                // validate remarks
                if(record.remarks != undefined && record.remarks != null && record.remarks != ""){
                    remarks = record.remarks
                }

                // validate entity
                if( record.entity != undefined && record.entity != null && record.entity != "" ){
                    entity = record.entity
                    let entityValues = ["germplasm","seed","package","germplasm_relation","germplasm_attribute","germplasm_name"]
                    if(!entityValues.includes(entity)){
                        let errMsg = "Invalid entity value. Must be germplasm, package, seed, germplasm_relation, germplasm_attribute, or germplasm_name."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // validate entity id
                if( record.entityDbId != undefined && record.entityDbId != null && record.entityDbId != "" ){
                    entityDbId = record.entityDbId

                    if(!validator.isInt(entityDbId)){
                        let errMsg = "Invalid request. User has provided an invalid format for entityDbId, it must be integer."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += validateQuery != "" ? " + " : ""
                    
                    //identify type of entity for the validation of the id
                    if(entity == "germplasm"){
                        validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.germplasm germplasm
                        WHERE
                            germplasm.is_void = FALSE AND
                            germplasm.id = ${entityDbId}

                        )`
                        validateCount += 1

                    }
                    else if(entity == "seed"){
                        validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.seed seed
                        WHERE
                            seed.is_void = FALSE AND
                            seed.id = ${entityDbId}

                        )`

                        validateCount += 1
                    }
                    else if(entity == "package"){
                        validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.package package
                        WHERE
                            package.is_void = FALSE AND
                            package.id = ${entityDbId}

                        )`
                        
                        validateCount += 1
                    }
                    else if(entity == "germplasm_relation"){
                        validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.germplasm_relation gr
                        WHERE
                            gr.is_void = FALSE AND
                            gr.id = ${entityDbId}

                        )`
                        
                        validateCount += 1
                    }
                    else if(entity == "germplasm_attribute"){
                        validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.germplasm_attribute ga
                        WHERE
                            ga.is_void = FALSE AND
                            ga.id = ${entityDbId}

                        )`

                        validateCount += 1
                    }
                    else if(entity == "germplasm_name"){
                        validateQuery += `
                        (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.germplasm_name gn
                        WHERE
                            gn.is_void = FALSE AND
                            gn.id = ${entityDbId}

                        )`

                        validateCount += 1
                    }
                }

                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count`

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT,
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host,400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // set insert parameters
                paramsArr = [
                    fileUploadDbId,
                    germplasmDbId,
                    seedDbId,
                    packageDbId,
                    remarks,
                    personDbId,
                    entity,
                    entityDbId
                ]

                fileUploadGermplasmArr.push(paramsArr)
            }

            let insertQuery = format(
            `
                INSERT INTO germplasm.file_upload_germplasm(
                    file_upload_id, germplasm_id, seed_id, package_id, remarks, creator_id, entity, entity_id)
                VALUES 
                    %L
                RETURNING id;
            `,
            fileUploadGermplasmArr
            )

            // Start transaction
            transaction = await sequelize.transaction(async transaction => {
                // Insert query
                let fileUploadGermplasms = await sequelize.query(insertQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true
                }).catch(async err =>{
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
    
                // Return the fileUploadGermplasm info
                for(var fileUploadGermplasm of fileUploadGermplasms[0]){
    
                    let fileUploadGermplasmDbId = fileUploadGermplasm.id
    
                    resultArray.push({
                        fileUploadGermplasmDbId: fileUploadGermplasmDbId,
                        recordCount: 1,
                        href: germplasmFileUploadGermplasmUrlString + "/" + fileUploadGermplasmDbId,
                    })
                }
            })

            res.send(200, {
                rows: resultArray,
            })
            return

        }
        catch(err){
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}