/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')

module.exports = {

    /**
    * Deletes a specific record in germplasm.cross_attribute
    * DELETE /v3/cross-attribute/:id
    * @param {*} req Request parameters
    * @param {*} res Response
    */
    delete: async function (req, res, next) {

        let crossAttributeDbId = req.params.id

        // Check if crossAttributeDbId is integer
        if (!validator.isInt(crossAttributeDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400247)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let crossAttributeQuery = `
                SELECT
                    creator.id AS "creatorDbId", 
                    experiment.id AS "experimentDbId"
                FROM 
                    germplasm.cross_attribute cd
                LEFT JOIN
                    tenant.person creator ON creator.id = cd.creator_id
                LEFT JOIN
                    germplasm.cross "germplasmCross" ON "germplasmCross".id = cd.cross_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = "germplasmCross".experiment_id
                WHERE 
                    cd.is_void = FALSE AND 
                    cd.id = ${crossAttributeDbId}
            `
            let crossAttributeRecord = await sequelize.query(crossAttributeQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
      
            if (await crossAttributeRecord === undefined || await crossAttributeRecord.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404043)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let deleteCrossAttributeQuery = format(`
                UPDATE
                    germplasm.cross_attribute
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${crossAttributeDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${crossAttributeDbId}
            `)
            await sequelize.query(deleteCrossAttributeQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { crossAttributeDbId: crossAttributeDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}