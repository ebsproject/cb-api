/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    /**
     * Search method for germplasm.cross_data
     * POST /v3/cross-data-search
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let crossDataQuery = null
        // Check if the client specified values for the field/s
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                crossDataQuery = knex.select(selectString)
            } else {
                crossDataQuery = knex.column(parameters['fields'].split('|'))
            }

            crossDataQuery += `
                FROM
                    germplasm.cross_data cross_data
                LEFT JOIN
                    germplasm.cross germplasmCross ON germplasmCross.id = cross_data.cross_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = germplasmCross.experiment_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                LEFT JOIN
                    experiment.location_occurrence_group logroup ON logroup.occurrence_id = occurrence.id
                LEFT JOIN
                    experiment.location location ON location.id = logroup.location_id
                LEFT JOIN
                    master.variable variable ON variable.id = cross_data.variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = cross_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = cross_data.modifier_id
                WHERE
                    cross_data.is_void = FALSE
            `
        } else {
            crossDataQuery = `
                SELECT
                    ${addedDistinctString}
                    cross_data.id::text AS "crossDataDbId",
                    cross_data.variable_id::text AS "variableDbId",
                    cross_data.data_value AS "dataValue",
                    cross_data.data_qc_code AS "dataQCCode",
                    cross_data.transaction_id::text AS "transactionDbId",
                    cross_data.creation_timestamp AS "creationTimestamp",
                    germplasmCross.id AS "crossDbId",
                    germplasmCross.cross_name AS "crossName",
                    germplasmCross.cross_method AS "crossMethod",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_code AS "experimentCode",
                    experiment.experiment_name AS "experimentName",
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_code AS "occurrenceCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    location.id AS "locationDbId",
                    location.location_code AS "locationCode",
                    location.location_name AS "locationName",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    cross_data.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier,
                    cross_data.collection_timestamp AS "collectionTimestamp"
                FROM
                    germplasm.cross_data cross_data
                LEFT JOIN
                    germplasm.cross germplasmCross ON germplasmCross.id = cross_data.cross_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = germplasmCross.experiment_id
                LEFT JOIN
                    experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                LEFT JOIN
                    experiment.location_occurrence_group logroup ON logroup.occurrence_id = occurrence.id
                LEFT JOIN
                    experiment.location location ON location.id = logroup.location_id
                LEFT JOIN
                    master.variable variable ON variable.id = cross_data.variable_id
                LEFT JOIN
                    tenant.person creator ON creator.id = cross_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = cross_data.modifier_id
                WHERE
                    cross_data.is_void = FALSE
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        crossDataFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            crossDataQuery,
            conditionString,
            orderString
        )

        // Retrieve the cross data records
        let crossData = await sequelize.query(crossDataFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await crossData == undefined || await crossData.length < 1) {
            res.send(200, {
                rows: crossData,
                count: 0
            })
            return
        }

        // Get the final count of the cross data records
        let crossDataCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            crossDataQuery,
            conditionString,
            orderString
        )

        let crossDataCount = await sequelize.query(crossDataCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = crossDataCount[0].count

        res.send(200, {
            rows: crossData,
            count: count
        })
        return
    }
}