/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')
const processQueryHelper = require('../../../helpers/processQuery/index')
const validator = require('validator')

module.exports = {
    /**
     * Retrieve planting envelopes
     * GET /v3/planting-envelopes
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let params = req.query
        let sort = req.query.sort
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = {}
        let endpoint = 'planting-envelopes'

        // Check if searchParams need to be updated
        if (params.occurrenceDbId) {
            const occurrenceDbIdArr = params.occurrenceDbId.split(',')
            let hasInvalidValue = false

            // Check if an occurrence ID is NOT an integer
            if (occurrenceDbIdArr.length > 0) {
                await occurrenceDbIdArr.forEach(async (id) => {
                    if(!validator.isInt(id)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                        res.send(new errors.BadRequestError(errMsg))
                        hasInvalidValue = true
                    }
                })
            }

            if (hasInvalidValue) return

            searchParams['occurrenceDbId'] = occurrenceDbIdArr.join('|')
        }

        // Get the filter condition
        conditionString = await processQueryHelper.getFilter(searchParams)

        if (conditionString != undefined && conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))

            return
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString === undefined || orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Build query for geting field tag records
        let plantingEnvelopeQuery = `
            SELECT
                program.id AS "programDbId",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                experiment.id AS "experimentDbId",
                experiment.experiment_code AS "experimentCode",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_year AS "experimentYear",
                season.id AS "experimentSeasonDbId",
                season.season_code AS "experimentSeasonCode",
                season.season_name AS "experimentSeason",
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                site.id AS "siteDbId",
                site.geospatial_object_code AS "siteCode",
                site.geospatial_object_name AS "site",
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.designation AS "germplasmName",
                germplasm.parentage AS "germplasmParentage",
                planting_instruction.entry_id AS "entryDbId",
                planting_instruction.entry_code AS "entryCode",
                planting_instruction.entry_number AS "entryNumber",
                planting_instruction.plot_id "plotDbId",
                plot.plot_code AS "plotCode",
                plot.plot_number AS "plotNumber",
                plot.rep AS "plotRep",
                plot.field_x AS "fieldX",
                plot.field_y AS "fieldY",
                seed.id AS "seedDbId",
                seed.seed_code AS "seedCode",
                seed.seed_name AS "seedName",
                package.id AS "packageDbId",
                package.package_code AS "packageCode",
                package.package_label AS "packageLabel",
                planting_job_occurrence.seeds_per_envelope AS "seedsPerEnvelope"
            FROM 
                experiment.planting_instruction planting_instruction 
            JOIN
                experiment.plot plot ON plot.id = planting_instruction.plot_id
            JOIN
                germplasm.germplasm germplasm ON germplasm.id = planting_instruction.germplasm_id
            LEFT JOIN
                germplasm.seed seed ON seed.id = planting_instruction.seed_id
            LEFT JOIN
                germplasm.package package ON package.id = planting_instruction.package_id
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
            LEFT JOIN
                experiment.planting_job_occurrence planting_job_occurrence ON planting_job_occurrence.occurrence_id = occurrence.id
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = occurrence.experiment_id
            LEFT JOIN
                place.geospatial_object site ON site.id = occurrence.site_id
            LEFT JOIN
                tenant.season season on season.id = experiment.season_id
            LEFT JOIN
                tenant.program program ON program.id = experiment.program_id
            WHERE
                planting_instruction.is_void = FALSE AND
                planting_job_occurrence.is_void = FALSE AND
                plot.is_void = FALSE AND
                germplasm.is_void = FALSE AND
                seed.is_void = FALSE AND
                package.is_void = FALSE AND
                occurrence.is_void = FALSE AND
                experiment.is_void = FALSE
        `

        // Finalize query build
        let plantingEnvelopesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            plantingEnvelopeQuery,
            conditionString,
            orderString
        )

        // Retrieve field tags from the database   
        let plantingEnvelopes = await sequelize
            .query(plantingEnvelopesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                return undefined
            })

        // If sequelize error was discovered
        if (plantingEnvelopes === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        // If no records were retrieved
        if (plantingEnvelopes.length < 1) {
            let errMsg = 'Resource not found. The planting envelope records you have requested for do not exist.'
            res.send(new errors.NotFoundError(errMsg))

            return
        }

        // Build query for getting field tag count
        let plantingEnvelopeCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                plantingEnvelopeQuery,
                conditionString,
                orderString
            )

        // Get the final count of the field tag records
        let plantingEnvelopeCount = await sequelize
            .query(plantingEnvelopeCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                return undefined
            })

        if (plantingEnvelopeCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        count = plantingEnvelopeCount[0].count

        res.send(200, {
            rows: plantingEnvelopes,
            count: count
        })

        return
    }
}
