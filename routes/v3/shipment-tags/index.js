/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')
const processQueryHelper = require('../../../helpers/processQuery/index')
const validator = require('validator')

module.exports = {
    /**
     * Retrieve shipment tags
     * GET /v3/shipment-tags
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let params = req.query
        let sort = req.query.sort
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = {}
        let endpoint = 'shipment-tags'

        // Check if searchParams need to be updated
        if (params.occurrenceDbId) {
            const occurrenceDbIdArr = params.occurrenceDbId.split(',')
            let hasInvalidValue = false

            // Check if an occurrence ID is NOT an integer
            if (occurrenceDbIdArr.length > 0) {
                await occurrenceDbIdArr.forEach(async (id) => {
                    if(!validator.isInt(id)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                        res.send(new errors.BadRequestError(errMsg))
                        hasInvalidValue = true
                    }
                })
            }

            if (hasInvalidValue) return

            searchParams['occurrenceDbId'] = occurrenceDbIdArr.join('|')
        }

        // Get the filter condition
        conditionString = await processQueryHelper.getFilter(searchParams)

        if (conditionString != undefined && conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))

            return
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString === undefined || orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Build query for geting field tag records
        let shipmentTagQuery = `
            SELECT
                organization.id AS "organizationDbId",
                organization.organization_code AS "organizationCode",
                organization.organization_name AS "organizationName",
                experiment.id AS "experimentDbId",
                experiment.experiment_code AS "experimentCode",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_year AS "experimentYear",
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                planting_instruction.entry_id AS "entryDbId",
                planting_instruction.entry_code AS "entryCode",
                planting_instruction.entry_number AS "entryNumber"
            FROM 
                experiment.planting_instruction planting_instruction
            JOIN
                experiment.plot plot ON plot.id = planting_instruction.plot_id
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = occurrence.experiment_id
            LEFT JOIN
                tenant.program program ON program.id = experiment.program_id
            LEFT JOIN
                tenant.crop_program crop_program ON crop_program.id = program.crop_program_id
            LEFT JOIN
                tenant.organization organization ON organization.id = crop_program.organization_id
            WHERE
                planting_instruction.is_void = FALSE AND
                plot.is_void = FALSE AND
                occurrence.is_void = FALSE AND
                experiment.is_void = FALSE
        `

        // Finalize query build
        let shipmentTagsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            shipmentTagQuery,
            conditionString,
            orderString
        )

        // Retrieve field tags from the database   
        let shipmentTags = await sequelize
            .query(shipmentTagsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                return undefined
            })

        // If sequelize error was discovered
        if (shipmentTags === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        // If no records were retrieved
        if (shipmentTags.length < 1) {
            let errMsg = 'Resource not found. The shipment tag records you have requested for do not exist.'
            res.send(new errors.NotFoundError(errMsg))

            return
        }

        // Build query for getting field tag count
        let shipmentTagCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                shipmentTagQuery,
                conditionString,
                orderString
            )

        // Get the final count of the field tag records
        let shipmentTagCount = await sequelize
            .query(shipmentTagCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                return undefined
            })

        if (shipmentTagCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        count = shipmentTagCount[0].count

        res.send(200, {
            rows: shipmentTags,
            count: count
        })

        return
    }
}
