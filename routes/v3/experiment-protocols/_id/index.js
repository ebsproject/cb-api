/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for updating an existing experiment protocol record
    // PUT /v3/experiment-protocols/:id
    put: async function (req, res, next) {

        // Retrieve experiment protocol ID
        let experimentProtocolDbId = req.params.id

        if(!validator.isInt(experimentProtocolDbId)) {
            let errMsg = `Invalid format, experiment protocol ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let protocolDbId = null
        let orderNumber = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if experiment protocol is existing
        // Build query
        let experimentProtocolQuery = `
            SELECT
                experiment.id AS "experimentDbId",
                protocol.id AS "protocolDbId",
                experimentProtocol.order_number AS "orderNumber",
                creator.id AS "creatorDbId"
            FROM
                experiment.experiment_protocol experimentProtocol
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = experimentProtocol.experiment_id
            LEFT JOIN
                tenant.protocol protocol ON protocol.id = experimentProtocol.protocol_id
            LEFT JOIN
                tenant.person creator ON creator.id = experimentProtocol.creator_id
            WHERE
                experimentProtocol.is_void = FALSE AND
                experimentProtocol.id = ${experimentProtocolDbId}
        `

        // Retrieve experiment protocol from the database
        let experimentProtocol = await sequelize.query(experimentProtocolQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (experimentProtocol === undefined || experimentProtocol.length < 1) {
            let errMsg = `The experiment protocol you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordExperimentDbId = experimentProtocol[0].experimentDbId
        let recordProtocolDbId = experimentProtocol[0].protocolDbId
        let recordOrderNumber = experimentProtocol[0].orderNumber
        let recordCreatorDbId = experimentProtocol[0].creatorDbId

        // Check if user is an admin or an owner of the experiment
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${recordExperimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let experimentProtocolUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/experiment-protocols/"
            + experimentProtocolDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body    

        // Start transaction
        let transaction

        try {
            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/

            if (data.protocolDbId !== undefined) {
                protocolDbId = data.protocolDbId

                if (!validator.isInt(protocolDbId)) {
                    let errMsg = `Invalid format, protocol ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (protocolDbId != recordProtocolDbId) {

                    // Validate protocol ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if protocol is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.protocol protocol
                        WHERE 
                            protocol.is_void = FALSE AND
                            protocol.id = ${protocolDbId}
                        )
                    `
                    validateCount += 1

                    // Check if the experiment ID and protocol ID pairing is unique
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if the pairing is unique, return 1
                        SELECT
                            CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE 0
                            END AS count
                        FROM
                            experiment.experiment_protocol ep
                        WHERE
                            ep.experiment_id = ${recordExperimentDbId} AND
                            ep.protocol_id = ${protocolDbId}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        protocol_id = $$${protocolDbId}$$
                    `
                }
            }

            if (data.orderNumber !== undefined) {
                orderNumber = data.orderNumber

                if (!validator.isInt(orderNumber)) {
                    let errMsg = `Invalid format, order number must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (orderNumber != recordOrderNumber) { 
                    // Check if the experiment ID and orderNo pairing is unique
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if the pairing is unique, return 1
                        SELECT
                            CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE 0
                            END AS count
                        FROM
                            experiment.experiment_protocol ep
                        WHERE
                            ep.experiment_id = ${recordExperimentDbId} AND
                            ep.order_number = ${orderNumber}
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        order_number = $$${orderNumber}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `
                
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the experiment protocol record
            let updateExperimentProtocolQuery = `
                UPDATE
                    experiment.experiment_protocol experimentProtocol
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${experimentProtocolDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updateExperimentProtocolQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let resultArray = {
                experimentProtocolDbId: experimentProtocolDbId,
                recordCount: 1,
                href: experimentProtocolUrlString
            }
            
            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing experiment protocol record
    // DELETE /v3/experiment-protocols/:id
    delete: async function (req, res, next) {
        
        // Retrieve experiment protocol ID
        let experimentProtocolDbId = req.params.id

        if(!validator.isInt(experimentProtocolDbId)) {
            let errMsg = `Invalid format, experiment protocol ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let experimentProtocolQuery = `
                SELECT
                    experiment.id AS "experimentDbId",
                    creator.id AS "creatorDbId"
                FROM
                    experiment.experiment_protocol experimentProtocol
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = experimentProtocol.experiment_id
                LEFT JOIN
                    tenant.protocol protocol ON protocol.id = experimentProtocol.protocol_id
                LEFT JOIN
                    tenant.person creator ON creator.id = experimentProtocol.creator_id
                WHERE
                    experimentProtocol.is_void = FALSE AND
                    experimentProtocol.id = ${experimentProtocolDbId}
            `

            // Retrieve experiment protocol from the database
            let experimentProtocol = await sequelize.query(experimentProtocolQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (experimentProtocol === undefined || experimentProtocol.length < 1) {
                let errMsg = `The experiment protocol you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Get values from database, record = the current value in the database
            let recordExperimentDbId = experimentProtocol[0].experimentDbId
            let recordCreatorDbId = experimentProtocol[0].creatorDbId

            // Check if user is an admin or an owner of the experiment
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                    LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                    LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                    LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.program_id = program.id
                    WHERE 
                        experiment.id = ${recordExperimentDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteExperimentProtocolQuery = format(`
                UPDATE
                    experiment.experiment_protocol
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${experimentProtocolDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${experimentProtocolDbId}
            `)

            await sequelize.query(deleteExperimentProtocolQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { experimentProtocolDbId: experimentProtocolDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }

    }
} 