/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async function (req, res, next) {
    let resultArray = []
    
    // Retrieve the client's person ID
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).secure

    // Set the URL for response
    let experimentProtocolsUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/experiment-protocols'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      records = data.records
      recordCount = records.length

      if (records === undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (err) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }
    
    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let experimentProtocolValuesArray = []
      let visitedArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0
        
        // Declare variables
        let experimentDbId = null
        let protocolDbId = null
        let orderNo = null
        
        // Check if the required parameters are in the body
        if (
          record.experimentDbId == undefined||
          record.protocolDbId == undefined ||
          record.orderNo == undefined
        ) {
          let errMsg =
            `Required parameters are missing. Ensure that the experimentDbId, protocolDbId, and/or orderNo fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Validate value for experimentDbId
        experimentDbId = record.experimentDbId
        
        if (!validator.isInt(experimentDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400025)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Validate if the experiment ID actually exists
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if experiment exists, return 1
            SELECT
              count(1)
            FROM
              experiment.experiment experiment
            WHERE
              experiment.is_void = FALSE AND
              experiment.id = ${experimentDbId}
          )
        `
        validateCount += 1

        // Validate value for protocolDbId
        protocolDbId = record.protocolDbId
        
        if (!validator.isInt(protocolDbId)) {
          let errMsg = `Invalid format, protocol ID must be an integer.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Validate if the protocol ID actually exists
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if the protocol exists, return 1
            SELECT
              count(1)
            FROM
              tenant.protocol protocol
            WHERE
              protocol.is_void = FALSE AND
              protocol.id = ${protocolDbId}
          )
        `
        validateCount += 1

        // Check if the experiment ID and protocol ID pairing is unique
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if the pairing is unique, return 1
            SELECT
              CASE WHEN
                (count(1) = 0)
                THEN 1
                ELSE 0
              END AS count
            FROM
              experiment.experiment_protocol ep
            WHERE
              ep.experiment_id = ${experimentDbId} AND
              ep.protocol_id = ${protocolDbId}
          )
        `
        validateCount += 1

        // Validate value for orderNo
        orderNo = record.orderNo

        if (!validator.isInt(orderNo)) {
          let errMsg = `Invalid format, order number must be an integer.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if the experiment ID and orderNo pairing is unique
        validateQuery += (validateQuery != '') ? ' + ' : ''
        validateQuery += `
          (
            --- Check if the pairing is unique, return 1
            SELECT
              CASE WHEN
                (count(1) = 0)
                THEN 1
                ELSE 0
              END AS count
            FROM
              experiment.experiment_protocol ep
            WHERE
              ep.experiment_id = ${experimentDbId} AND
              ep.order_number = ${orderNo}
          )
        `
        validateCount += 1
        
        // Validate the input on he db
        let checkInputQuery = `
          SELECT ${validateQuery} AS count
        `

        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0].count != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        if (recordCount > 1) {
          // Check if the records are valid amongst each other
          let experimentProtocolInfo = {
            first: `${experimentDbId}-${protocolDbId}`,
            second: `${experimentDbId}-${orderNo}`
          }

          if (visitedArray.includes(experimentProtocolInfo)) {
            let errMsg = 'Invalid request, duplicate combinations exist within the same request body.'
            res.send(new errors.BadRequestError(errMsg))
            return
          } else {
            visitedArray.push(experimentProtocolInfo)
          }
        }

        // Get values
        let tempArray = [
          experimentDbId,
          protocolDbId,
          orderNo,
          personDbId,
        ]

        experimentProtocolValuesArray.push(tempArray)
      }

      // Create experiment protocol record
      let experimentProtocolQuery = format(`
        INSERT INTO
          experiment.experiment_protocol (
            experiment_id, protocol_id, order_number, creator_id
          )
        VALUES
          %L
        RETURNING
          id
        `, experimentProtocolValuesArray
      )

      let experimentProtocols = await sequelize.query(experimentProtocolQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      for (experimentProtocol of experimentProtocols[0]) {
        experimentProtocolDbId = experimentProtocol.id
        // Return the experimentProtocol info
        let epObj = {
          experimentProtocolDbId: experimentProtocolDbId,
          recordCount : 1,
          href: experimentProtocolsUrlString + '/' + experimentProtocolDbId
        }
        resultArray.push(epObj)
      }

      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}