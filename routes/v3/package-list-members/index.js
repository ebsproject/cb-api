/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let validator = require('validator')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    /**
     * Retrieve packages for package list
     * GET /v3/package-list-members
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let searchParams = []
        let listDbId

        // Retrieve the file upload ID
        listDbId = params.listDbId

        // Check if required parameter listDbId is provided
        if(listDbId === null || listDbId === undefined) {
            let errMsg = 'Missing parameter. The listDbId is required.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if listDbId is an integer
        if(!validator.isInt(listDbId)) {
            let errMsg = 'Invalid parameter. The listDbId must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let listQuery = `
            SELECT
                list.id AS "listDbId",
                list.name AS "listName"
            FROM
                platform.list
            WHERE
                list.is_void = FALSE AND
                list.id = ${listDbId}
        `

        // Find list in the database
        let list = await sequelize
            .query(listQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If the file upload does not exist, return an error
        if (await list == undefined || await list.length < 1) {
            let errMsg = 'Resource not found. The germplasm file upload you have requested for does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Check if list type = package
        listQuery = `
            SELECT
                list.id AS "listDbId",
                list.name AS "listName"
            FROM
                platform.list
            WHERE
                list.is_void = FALSE AND
                list.type = 'package' AND
                list.id = ${listDbId}
        `

        // Find list in the database
        list = await sequelize
            .query(listQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If list is not a package list, return error
        if (await list == undefined || await list.length < 1) {
            let errMsg = 'Invalid request. The list is not a package list.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let packagesQuery = `
            SELECT
                package.id AS "packageDbId",
                package.package_code AS "packageCode",
                package.package_label AS "packageLabel",
                package.package_quantity AS "packageQuantity",
                package.package_unit AS "packageUnit",
                ROW_NUMBER() OVER() AS "packageNumber",
                (
                    SELECT
                        COUNT(id)
                    FROM
                        platform.list_member
                    WHERE
                        list_member.list_id = 1832
                ) AS "totalPackageCount",
                container.id AS "containerDbId",
                container.facility_code AS "containerCode",
                container.facility_name AS "containerName",
                sub_facility.id AS "subFacilityDbId",
                sub_facility.facility_code AS "subFacilityCode",
                sub_facility.facility_name AS "subFacilityName",
                facility.id AS "facilityDbId",
                facility.facility_code AS "facilityCode",
                facility.facility_name AS "facilityName",
                program.id AS "programDbId",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                seed.id AS "seedDbId",
                seed.seed_code AS "seedCode",
                seed.seed_name AS "seedName",
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_name AS "occurrenceName",
                experiment.id AS "experimentDbId",
                experiment.experiment_code AS "experimentCode",
                experiment.experiment_name AS "experimentName",
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.designation AS "designation"
            FROM
                platform.list_member
            LEFT JOIN
                platform.list list ON list.id = list_member.list_id AND list.is_void = FALSE
            LEFT JOIN
                germplasm.package package ON package.id = list_member.data_id
            LEFT JOIN
                place.facility container ON container.id = package.facility_id
            LEFT JOIN
                place.facility sub_facility ON sub_facility.id = container.parent_facility_id
            LEFT JOIN
                place.facility facility ON facility.id = container.root_facility_id
            LEFT JOIN
                tenant.program program ON program.id = package.program_id AND program.is_void = FALSE
            LEFT JOIN
                germplasm.seed seed ON seed.id = package.seed_id AND seed.is_void = FALSE
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id AND occurrence.is_void = FALSE
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = seed.source_experiment_id AND experiment.is_void = FALSE
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = seed.germplasm_id AND seed.is_void = FALSE
            WHERE
                list.id = ${listDbId}
                AND list_member.is_void = FALSE
                AND list.type = 'package'
                AND package.id IS NOT NULL
        `

        // Get the filter condition
        conditionString = await processQueryHelper.getFilter(
            searchParams,
            []
        )

        if (conditionString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let packagesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            packagesQuery,
            conditionString,
            orderString,
        )

        // Retrieve packages from the database   
        let packages = await sequelize
            .query(packagesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await packages == undefined || await packages.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get the final count of the package records
        let packagesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                packagesQuery,
                conditionString,
                orderString
            )

        let packagesCount = await sequelize
            .query(packagesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = packagesCount === undefined ? 0 : packagesCount[0].count

        res.send(200, {
            rows: packages,
            count: count
        })
        return
    }
}
