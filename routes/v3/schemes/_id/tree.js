/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let schemeHelper = require('../../../../helpers/scheme/index.js')
let errorBuilder = require('../../../../helpers/error-builder')

/**
 * Retrieve the type of the scheme
 * 
 * @param schemeDbId integer ID of the scheme
 * 
 * return integer
 */
async function getSchemeType(schemeDbId) {

    let query = `
        SELECT
            scheme.scheme_type AS "schemeType"
        FROM
            master.scheme scheme
        WHERE 
            scheme.is_void = FALSE
            AND scheme.id = (:schemeDbId)
    `

    let schemeType = await sequelize.query(query, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            schemeDbId: schemeDbId
        }
    })

    return schemeType[0]['schemeType']
}

module.exports = {

    // Implementation of GET call for /v3/schemes/:id/tree
    get: async function (req, res, next) {

        // Retrieve the scheme ID
        let schemeDbId = req.params.id

        // Check if the ID is valid
        if (!validator.isInt(schemeDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400061)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {

            // Get the scheme
            let schemeSql = `
                SELECT
                    scheme.id AS "schemeDbId",
                    scheme.code,
                    scheme.name,
                    scheme.description
                FROM
                    master.scheme scheme
                WHERE 
                    scheme.id = (:schemeDbId)
                    AND scheme.is_void = FALSE
            `

            let schemes = await sequelize.query(schemeSql, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    schemeDbId: schemeDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await schemes.length == undefined || await schemes.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404018)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            for (scheme of schemes) {

                // Check the type of scheme
                let schemeType = await getSchemeType(scheme.schemeDbId)
                let key = ''

                // Assign the key
                if (schemeType == 10) key = 'phases'
                else key = 'stages'

                // Retrieve the relation of the given scheme
                let parentSchemeRelationQuery = `
                    SELECT
                        scheme.id AS "schemeDbId",
                        scheme.code,
                        scheme.name,
                        scheme.description
                    FROM
                        master.scheme scheme, master.scheme_relation "schemeRelation"
                    WHERE
                        "schemeRelation".root_scheme_id = (:rootSchemeDbId)  
                        AND "schemeRelation".parent_scheme_id = (:parentSchemeDbId)  
                        AND "schemeRelation".child_scheme_id = scheme.id
                        AND "schemeRelation".is_void = FALSE
                        AND scheme.is_void = FALSE
                    ORDER BY
                        "schemeRelation".order_number
                `

                let parentSchemeRelations = await sequelize.query(parentSchemeRelationQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        rootSchemeDbId: scheme.schemeDbId,
                        parentSchemeDbId: scheme.schemeDbId
                    }
                })

                if (await parentSchemeRelations.length != undefined && await parentSchemeRelations.length > 0) {

                    // Check if there are still relations map to the scheme
                    for (parentSchemeRelation of parentSchemeRelations) {

                        // Check the type of scheme
                        let schemeType = await getSchemeType(parentSchemeRelation.schemeDbId)
                        let nextKey = ''

                        // Assign the key
                        if (schemeType == 10) nextKey = 'phases'
                        else nextKey = 'stages'

                        // Retrieve the relation of the scheme
                        let childSchemeRelationQuery = `
                            SELECT
                                DISTINCT ON ("schemeRelation".order_number, scheme.id)
                                scheme.id AS "schemeDbId",
                                scheme.code,
                                scheme.name,
                                scheme.description
                            FROM
                                master.scheme scheme, master.scheme_relation "schemeRelation"
                            WHERE
                                "schemeRelation".root_scheme_id = (:rootSchemeDbId)  
                                AND "schemeRelation".parent_scheme_id = (:parentSchemeDbId)  
                                AND "schemeRelation".child_scheme_id = scheme.id
                                AND "schemeRelation".is_void = FALSE
                                AND scheme.is_void = FALSE
                            ORDER BY
                                "schemeRelation".order_number, scheme.id
                        `

                        let childSchemeRelations = await sequelize.query(childSchemeRelationQuery, {
                            type: sequelize.QueryTypes.SELECT,
                            replacements: {
                                rootSchemeDbId: scheme.schemeDbId,
                                parentSchemeDbId: parentSchemeRelation.schemeDbId
                            }
                        })

                        if (await childSchemeRelations.length != undefined && await childSchemeRelations.length > 0) {
                            parentSchemeRelation[nextKey] = childSchemeRelations
                        } else {
                            parentSchemeRelation[nextKey] = []
                        }
                    }
                    scheme[key] = parentSchemeRelations
                } else {
                    scheme[key] = []
                }
            }

            res.send(200, {
                rows: schemes
            })
            return

        }
    },

    // Implementation of PUT call for /v3/schemes/:id/tree
    put: async function (req, res, next) {

        // Retrieve the user ID of the client from the access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve the scheme ID
        let schemeDbId = req.params.id

        // Check if the ID is valid
        if (!validator.isInt(schemeDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400061)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else {
            // Get the scheme
            let schemeSql = `
                SELECT
                    scheme.id AS "schemeDbId",
                    scheme.code,
                    scheme.name,
                    scheme.description
                FROM
                    master.scheme scheme
                WHERE 
                    scheme.id = :schemeDbId
                    AND scheme.is_void = false
            `

            let schemes = await sequelize.query(schemeSql, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    schemeDbId: schemeDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await schemes.length == undefined || await schemes.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404018)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Default
            let resultArray = []
            let phaseParentDbId = null
            let phaseChildDbId = null
            let stageParentDbId = null
            let stageChildDbId = null
            let projectRootDbId = null
            let phaseCount = 0
            let stageCount = 0

            // Check if the connection is secure or not (http or https)
            let isSecure = forwarded(req, req.headers).secure

            // Set the URL for response
            let schemeRelationUrlString = (isSecure ? 'https' : 'http')
                + "://"
                + req.headers.host
                + "/v3/schemes"

            // Check parameters
            if (req.body != null) {

                // Parse the input
                let data = req.body
                let records = []

                try {
                    records = data.records

                    if (records == undefined) {

                        return
                    }
                } catch (e) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Start transaction
                let transaction

                try {
                    // get transaction
                    transaction = await sequelize.transaction({ autocommit: false })

                    let projectDbId = null

                    let visitedArray = []

                    // Loop the records array
                    for (var i = 0, recordsLen = records.length; i < recordsLen; i++) {

                        // Get the information for projects
                        code = records[i]['code']
                        name = records[i]['name']
                        description = records[i]['description']

                        // Check if the scheme data info is already visited
                        let schemeDataInfo = code + '-' + name

                        if (visitedArray.includes(schemeDataInfo)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400058)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        } else {
                            visitedArray.push(schemeDataInfo)
                        }

                        // Get the master.project ID
                        let projectQuery = `
                            SELECT
                                project.id AS "projectDbId"
                            FROM
                                master.project project
                            WHERE 
                                project.is_void = FALSE
                                AND (
                                    project.abbrev = :projectAbbrev
                                    OR project.name = :projectName
                                )
                        `

                        let projects = await sequelize.query(projectQuery, {
                            type: sequelize.QueryTypes.SELECT,
                            replacements: {
                                projectAbbrev: code,
                                projectName: name
                            }
                        })

                        if (await projects.length != undefined && await projects.length > 0) {
                            projectDbId = projects[0]['projectDbId']
                        } else {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400059)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        // Retrieve the scheme ID
                        projectRootDbId = await schemeHelper.getSchemeDbId(code, name, description, 10, projectDbId, userDbId)

                        // Check if the scheme ID in call is the same in the new scheme relation
                        if (schemeDbId == projectRootDbId) {
                            // Void the created scheme relation
                            let voidSchemeRelationQuery = `
                                UPDATE
                                    master.scheme_relation
                                SET
                                    is_void = TRUE,
                                    notes = notes || '; Voided by CB-Api',
                                    modifier_id = :userDbId,
                                    modification_timestamp = NOW()
                                WHERE 
                                    root_scheme_id = (:schemeDbId)
                                    AND is_void = false
                            `

                            await sequelize.query(voidSchemeRelationQuery, {
                                type: sequelize.QueryTypes.UPDATE,
                                replacements: {
                                    schemeDbId: schemeDbId,
                                    userDbId: userDbId
                                }
                            })
                                .catch(async err => {
                                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                                    res.send(new errors.InternalError(errMsg))
                                    return
                                })
                        } else {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400062)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        // Assign the project root ID to phase parent ID
                        phaseParentDbId = projectRootDbId

                        // Loop the projects array
                        let schemeProjects = records[i]
                        for (var key in schemeProjects) {
                            if (key != 'code' && key != 'name' && key != 'description') {

                                // Get the phases
                                let phases = schemeProjects[key]
                                for (var j = 0, phasesLen = phases.length; j < phasesLen; j++) {

                                    // Get the information for phases
                                    code = phases[j]['code']
                                    name = phases[j]['name']
                                    description = phases[j]['description']

                                    // Check if the scheme data info is already visited
                                    let schemeDataInfo = code + '-' + name

                                    if (visitedArray.includes(schemeDataInfo)) {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 400058)
                                        res.send(new errors.BadRequestError(errMsg))
                                        return
                                    } else {
                                        visitedArray.push(schemeDataInfo)
                                    }

                                    // Retrieve the scheme ID and assign as the phase child ID
                                    phaseChildDbId = await schemeHelper.getSchemeDbId(code, name, description, 20, projectDbId, userDbId)

                                    // Assign the phase child ID from phase as the stage parent ID
                                    stageParentDbId = phaseChildDbId

                                    // Count phases
                                    phaseCount++

                                    // Create the scheme relation between the project and phase
                                    let phaseSchemeRelationDbId = await schemeHelper.createSchemeRelationRecord(projectRootDbId, phaseParentDbId, phaseChildDbId, phaseCount, userDbId)

                                    if (phaseSchemeRelationDbId == null) {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 500003)
                                        res.send(new errors.InternalError(errMsg))
                                        return
                                    }

                                    // Loop the phase array
                                    let phasesObj = phases[j]
                                    for (var key in phasesObj) {
                                        if (key != 'code' && key != 'name' && key != 'description') {

                                            // Get the stages
                                            let stages = phasesObj[key]
                                            for (var k = 0, stagesLen = stages.length; k < stagesLen; k++) {

                                                // Get the information for stages
                                                code = stages[k]['code']
                                                name = stages[k]['name']
                                                description = stages[k]['description']

                                                // Check if the scheme data info is already visited
                                                let schemeDataInfo = code + '-' + name

                                                if (visitedArray.includes(schemeDataInfo)) {
                                                    let errMsg = await errorBuilder.getError(req.headers.host, 400058)
                                                    res.send(new errors.BadRequestError(errMsg))
                                                    return
                                                } else {
                                                    visitedArray.push(schemeDataInfo)
                                                }

                                                // Retrieve the scheme ID and assign as the stage child ID
                                                stageChildDbId = await schemeHelper.getSchemeDbId(code, name, description, 30, projectDbId, userDbId)

                                                // Count stages
                                                stageCount++

                                                // Create scheme relation between the project, phase and stage
                                                let stageSchemeRelationDbId = schemeHelper.createSchemeRelationRecord(projectRootDbId, stageParentDbId, stageChildDbId, stageCount, userDbId)

                                                if (stageSchemeRelationDbId == null) {
                                                    let errMsg = await errorBuilder.getError(req.headers.host, 500003)
                                                    res.send(new errors.InternalError(errMsg))
                                                    return
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Return the scheme info
                        let array = {
                            schemeDbId: projectRootDbId,
                            recordCount: 1,
                            href: schemeRelationUrlString + '/' + projectRootDbId + '/tree'
                        }
                        resultArray.push(array)
                    }

                    // Commit the transaction
                    await transaction.commit()

                    res.send(200, {
                        rows: resultArray
                    })
                    return
                } catch (err) {
                    // Rollback transaction if any errors were encountered
                    if (err) await transaction.rollback();
                    let errMsg = await errorBuilder.getError(req.headers.host, 500003)
                    res.send(new errors.InternalError(errMsg))
                    return
                }
            } else {
                let errMsg = await errorBuilder.getError(req.headers.host, 400009)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
    }
}