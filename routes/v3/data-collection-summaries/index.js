/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { sequelize } = require('../../../config/sequelize')
const errors = require('restify-errors')
const errorBuilder = require('../../../helpers/error-builder')
const processQueryHelper = require('../../../helpers/processQuery/index')
const validator = require('validator')

module.exports = {
    /**
     * Retrieve data collection summaries
     * GET /v3/data-collection-summaries
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    get: async function (req, res, next) {
        // Set defaults 
        let params = req.query
        let sort = req.query.sort
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let count = 0
        let orderString = ''
        let conditionString = ''
        let variableId = [0]
        let crosstabColumns = null
        let selectColumns = null
        let traitNames = []
        let traitAbbrevs = []
        let occurrenceNames = []
        let dataset = {}

        if (!params.occurrenceDbId || !params.columnHeaderType) {
            let errMsg = 'Incomplete query parameters. The request should have both occurrenceDbId and columnHeaderType values.'

            res.send(new errors.BadRequestError(errMsg))
            return
        }

        const occurrenceDbIdsString = params.occurrenceDbId
        const occurrenceDbIdArr = occurrenceDbIdsString.split(',')
        const columnHeaderType = params.columnHeaderType

        // Check if an occurrence ID is NOT an integer
        for (const id of occurrenceDbIdArr) {
            if(!validator.isInt(id)) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400241)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Build ORDER BY query clause
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString === undefined || orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Get all plot data variables
        let variableQuery = `
            SELECT 
                STRING_AGG( variable_id::text, ',') as "variableId",
                STRING_AGG( 'COALESCE("' || name || '", ''0/0'') AS ' || '"' || name || '"', ',') as "selectColumns",
                STRING_AGG( name, ',') as "traitNames",
                STRING_AGG( abbrev, ',') as "traitAbbrevs",
                STRING_AGG( '"' || name || '"' || ' varchar', ',') as "crosstabColumns"
            FROM
                (SELECT
                    distinct (plot_data.variable_id) as variable_id,
                    v.name,
                    v.abbrev
                FROM
                    experiment.plot_data plot_data
                LEFT JOIN
                    experiment.plot
                ON
                    plot.id = plot_data.plot_id AND
                    plot.is_void = FALSE
                LEFT JOIN
                    master.variable v
                ON
                    v.id = plot_data.variable_id
                WHERE
                    plot.occurrence_id IN (${occurrenceDbIdsString}) AND
                    plot_data.is_void = FALSE
                ORDER BY 
                    v.name) a
        `

        let variableList = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (variableList) {
            // If there is 0 plot data across all included occurrences, then return empty rows
            if (!variableList[0]['variableId']) {
                res.send(200, {
                    rows: [],
                    count: 0
                })

                return
            }

            variableId = variableList[0]['variableId'] == null ? [0] : variableList[0]['variableId']
            crosstabColumns = variableList[0]['crosstabColumns']
            selectColumns = variableList[0]['selectColumns']
            traitNames = variableList[0]['traitNames'].split(',')
            traitAbbrevs = variableList[0]['traitAbbrevs'].split(',')
        }

        if (crosstabColumns != "" && crosstabColumns != null) {
            crosstabColumns = ',' + crosstabColumns
        } else if (crosstabColumns == null) {
            crosstabColumns = ', "null" text';
        }
        if (selectColumns != "" && selectColumns != null) {
            selectColumns = ',' + selectColumns
        } else if (selectColumns == null) {
            selectColumns = ''
        }

        if (columnHeaderType.toLowerCase() == 'trait') {
            // Build query for geting data collection summary records
            dataCollectionSummaryQuery = `
                SELECT
                    DISTINCT occurrence.occurrence_name AS "occurrenceName"
                    ${selectColumns}
                FROM
                    experiment.plot_data plot_data
                LEFT JOIN
                    experiment.plot
                ON
                    plot.id = plot_data.plot_id
                LEFT JOIN
                    experiment.occurrence
                ON
                    occurrence.id = plot.occurrence_id
                LEFT JOIN
                    (
                        SELECT
                            DISTINCT "occurrenceName"
                            ${selectColumns}
                        FROM
                            CROSSTAB(
                                'SELECT
                                    occurrence.occurrence_name,
                                    plot_data.variable_id,
                                    CONCAT(
                                        COUNT(*),
                                        ''/'',
                                        COUNT(*) FILTER (WHERE data_qc_code = ''S'')
                                    )
                                FROM
                                    experiment.plot_data plot_data
                                LEFT JOIN
                                    experiment.plot
                                ON
                                    plot.id = plot_data.plot_id
                                LEFT JOIN
                                    experiment.occurrence
                                ON
                                    occurrence.id = plot.occurrence_id
                                LEFT JOIN
                                    master.variable variable
                                ON
                                    variable.id = plot_data.variable_id
                                WHERE
                                    plot.occurrence_id IN (${occurrenceDbIdsString}) AND
                                    occurrence.is_void = FALSE
                                GROUP BY
                                    occurrence.id, plot_data.variable_id
                                ORDER BY
                                    occurrence.id',
                                'SELECT UNNEST(ARRAY[${variableId}])'
                            ) AS (
                                "occurrenceName" varchar
                                ${crosstabColumns}
                            )
                    ) crosstab
                ON
                    crosstab."occurrenceName" = occurrence.occurrence_name
                LEFT JOIN
                    master.variable variable
                ON
                    variable.id = plot_data.variable_id
                WHERE
                    plot.occurrence_id IN (${occurrenceDbIdsString})
                ORDER BY
                    occurrence.occurrence_name
            `    
        } else if (columnHeaderType.toLowerCase() == 'occurrence') {
            // Get list of occurrence names
            let occurrenceQuery = `
                SELECT
                    occurrence_name AS "occurrenceName"
                FROM
                    experiment.occurrence
                    WHERE
                    id IN (${occurrenceDbIdsString}) AND
                    is_void = FALSE
                ORDER BY
                    occurrence_name
            `

            occurrenceNames = await sequelize.query(occurrenceQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            occurrenceNames = occurrenceNames.map(v => v.occurrenceName)

            dataCollectionSummaryQuery = `
                SELECT
                    variable.abbrev AS "traitAbbrev",
                    variable.name AS "traitName",
                    occurrence.occurrence_name AS "occurrenceName",
                    CONCAT(
                        COUNT(*),
                        '/',
                        COUNT(*) FILTER (WHERE data_qc_code = 'S')
                    ) AS "dataValue"
                FROM
                    experiment.plot_data plot_data
                LEFT JOIN
                    experiment.plot
                ON
                    plot.id = plot_data.plot_id
                LEFT JOIN
                    experiment.occurrence
                ON
                    occurrence.id = plot.occurrence_id
                LEFT JOIN
                    master.variable variable
                ON
                    variable.id = plot_data.variable_id
                WHERE
                    plot.occurrence_id IN (${occurrenceDbIdsString}) AND
                    occurrence.is_void = FALSE
                GROUP BY
                    variable.abbrev, variable.name, occurrence.occurrence_name
                ORDER BY
                    variable.abbrev
            `
        }

        // Finalize query build
        let dataCollectionSummariesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            dataCollectionSummaryQuery,
            conditionString,
            orderString
        )

        // Retrieve data collection summaries from the database   
        let dataCollectionSummaries = await sequelize
            .query(dataCollectionSummariesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {

                return undefined
            })

        // If sequelize error was discovered
        if (dataCollectionSummaries === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        // If no records were retrieved
        if (dataCollectionSummaries.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })

            return
        }

        // Build query for getting data collection summary count
        let dataCollectionSummaryCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                dataCollectionSummaryQuery,
                conditionString,
                orderString
            )

        // Get the final count of the data collection summary records
        let dataCollectionSummaryCount = await sequelize
            .query(dataCollectionSummaryCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {

                return undefined
            })

        if (dataCollectionSummaryCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        count = dataCollectionSummaryCount[0].count

        // Repackage retrieved data if column header type is Occurrence
        if (columnHeaderType == 'occurrence') {
            let dataMap = {}
            let currentTraitAbbrev = ''

            occurrenceNames.forEach( (e) => {
                dataMap[e] = '0/0'
            })

            dataCollectionSummaries.forEach( (element, index) => {
                const traitAbbrev = element['traitAbbrev']
                const traitName = element['traitName']

                if (!dataset[traitAbbrev]) {
                    dataset[traitAbbrev] = {
                        'traitAbbrev': `${traitAbbrev}`,
                        'traitName': `${traitName}`,
                    }
                }

                // Set data values for current trait abbrev when loop changed to the next trait abbrev
                if (currentTraitAbbrev && currentTraitAbbrev != traitAbbrev) {
                    // Load data values into current trait abbrev
                    dataset[currentTraitAbbrev] = {...dataset[currentTraitAbbrev], ...dataMap}

                    // Reset data values
                    occurrenceNames.forEach( (e) => {
                        dataMap[e] = '0/0'
                    })
                }
                // "next" trait abbrev becomes the "current" trait abbrev
                currentTraitAbbrev = traitAbbrev

                dataMap[element['occurrenceName']] = element['dataValue']

                if (index + 1 == count) {
                    dataset[currentTraitAbbrev] = {...dataset[currentTraitAbbrev], ...dataMap}
                }
            })

            dataCollectionSummaries = Object.values(dataset)
            count = dataCollectionSummaries.length
        }

        res.send(200, {
            rows: dataCollectionSummaries,
            count: count
        })

        return
    }
}
