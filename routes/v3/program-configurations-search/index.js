/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require("../../../config/sequelize")
let { knex } = require("../../../config/knex")
let processQueryHelper = require("../../../helpers/processQuery/index")
let errors = require("restify-errors")
let errorBuilder = require("../../../helpers/error-builder")

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ""
        let conditionString = ""
        let addedDistinctString = ""
        let addedConditionString = ""
        let parameters = {}
        parameters["distinctOn"] = ""

        if (req.body) {
            if (req.body.fields != null) {
                parameters["fields"] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = ["fields", "distinctOn"]
            // Get filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray
            )

            if (conditionString.includes("invalid")) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                parameters["distinctOn"] = req.body.distinctOn
                addedDistinctString = await processQueryHelper.getDistinctString(
                    parameters["distinctOn"]
                )

                parameters["distinctOn"] = `"${parameters["distinctOn"]}",`
            }
        }
        // Build the base retrieval query
        let programConfigurationQuery = null
        // Check if the client specified values for the field/s
        if (parameters["fields"]) {
            programConfigurationQuery = knex.column(
                parameters["fields"].split("|")
            )

            programConfigurationQuery += `
                FROM
                    tenant.program_config "programConfig"
                LEFT JOIN 
                    platform.config config on "programConfig".config_id = config.id
                LEFT JOIN 
                    tenant.program program on "programConfig".program_id = program.id
                LEFT JOIN
                    tenant.person creator ON "programConfig".creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON "programConfig".modifier_id = modifier.id
                WHERE
                    "programConfig".is_void = FALSE
                    ${addedConditionString} 
                ORDER BY
                    "programConfig".id
            `
        } else {
            programConfigurationQuery = `
                SELECT
                    ${addedDistinctString}
                    programConfig.id AS "programConfigDbId",
                    programConfig.config_id AS "programConfigConfigId",
                    config.abbrev AS "configAbbrev",
                    config.name AS "configName",
                    config.config_value AS "configValue",
                    config.usage AS "configUsage",
                    program.id as "programDbId",
                    program.program_code AS "programCode",
                    program.program_name AS "programName",
                    programConfig.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    programConfig.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    tenant.program_config  programConfig
                LEFT JOIN 
                    platform.config config on programConfig.config_id = config.id
                LEFT JOIN 
                    tenant.program program on programConfig.program_id = program.id
                LEFT JOIN
                    tenant.person creator ON programConfig.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON programConfig.modifier_id = modifier.id
                WHERE
                    programConfig.is_void = FALSE
                    ${addedConditionString}
                ORDER BY
                    ${parameters["distinctOn"]}
                    programConfig.id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes("invalid")) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Generate the final SQL query
        let programConfigurationFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            programConfigurationQuery,
            conditionString,
            orderString
        )

        // Retrieve the program configuration records
        let programConfigurations = await sequelize
            .query(programConfigurationFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                },
            })
            .catch(async (err) => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await programConfigurations == undefined || await programConfigurations.length < 1) {
            res.send(200, {
                rows: [],
                count: 0,
            })
            return
        }

        let programConfigurationsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            programConfigurationQuery,
            conditionString,
            orderString
        )

        let programConfigurationsCount = await sequelize
            .query(programConfigurationsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async (err) => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = programConfigurationsCount[0].count

        res.send(200, {
            rows: programConfigurations,
            count: count
        })
        return
    }
}
