/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let jwtDecode = require('jwt-decode')
let aclStudy = require('../../../../helpers/acl/study')
let userValidator = require('../../../../helpers/person/validator')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let studyHelper = require('../../../../helpers/study/index')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    get: async function (req, res, next) {
        // Retrieve the experiment location ID
        let experimentLocationDbId = req.params.id

        // Check if ID is valid
        if (!validator.isInt(experimentLocationDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400024)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else {
            // Retrieve the user ID of the client via access token
            let userId = await tokenHelper.getUserId(req)

            if (userId == null) {
              let errMsg = await errorBuilder.getError(req.headers.host, 401002)
              res.send(new errors.BadRequestError(errMsg))
              return
            }

            let applyAcl = true

            // Check if user is an administrator
            if (await userValidator.isAdmin(userId)) {
                applyAcl = false
            }

            if (applyAcl == true) {
                // Check if user has access to experimentLocation
                let canAccess = await aclStudy.isStudyAccessible(
                    'study',
                    '' + experimentLocationDbId,
                    'user',
                    '' + userId
                )

                if (!canAccess) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401005)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let addedConditionString = `AND study.id = (:experimentLocationDbId)`

            // Build query
            let experimentLocationQuery = await studyHelper.getStudyQuerySql(
                addedConditionString,
                '',
                true
            )

            // Retrieve experiment location from database
            let experimentLocations = await sequelize
                .query(experimentLocationQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        experimentLocationDbId: experimentLocationDbId
                    }
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Check if experiment location exists
            if (
                await experimentLocations == undefined ||
                await experimentLocations.length < 1
            ) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404007)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: experimentLocations
            })
            return
        }
    }
}
