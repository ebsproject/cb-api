/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { study } = require('../../../config/sequelize')
let { sequelize } = require('../../../config/sequelize')
let aclStudy = require('../../../helpers/acl/study.js')
let tokenHelper = require('../../../helpers/auth/token')
let userValidator = require('../../../helpers/person/validator')
let processQueryHelper = require('../../../helpers/processQuery/index')
let studyHelper = require('../../../helpers/study/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    get: async (req, res, next) => {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let conditionString = ''
        let orderString = ''
        let studyIds = null

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let applyAcl = true

        if (await userValidator.isAdmin(userId)) {
            applyAcl = false
        }

        if (applyAcl == true) {
            let accessibleStudiesList = null
            accessibleStudiesList = await aclStudy.getAccessibleStudies(
                'user',
                userId,
                null
            )

            if (accessibleStudiesList.length > 0) {
                studyIds = ''
                for (study of accessibleStudiesList) {
                    studyIds += study.study_id + ','
                }
                studyIds = studyIds.replace(/,\s*$/, '')
            } else {
                let errMsg = await errorBuilder.getError(req.headers.host, 401006)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let addedConditionString = ''

        if (studyIds != null) {
            addedConditionString = ` AND study.id in (` + studyIds + `)`
        }

        let isCommitted = req.params.committed

        if (isCommitted != null && isCommitted.toUpperCase() == 'TRUE') {
            addedConditionString += ` AND study_status ILIKE '%committed study%'`
        }

        addedConditionString += ` AND experiment.id IS NOT NULL `

        // Build Query
        let experimentLocationsQuery = await studyHelper.getStudyQuerySql(
            addedConditionString,
            '',
            true
        )

        if (
            params.name !== undefined ||
            params.year !== undefined ||
            params.programDbId !== undefined ||
            params.seasonDbId !== undefined
        ) {
            let parameters = []

            if (params.name !== undefined) parameters['name'] = params.name
            if (params.year !== undefined) parameters['year'] = params.year
            if (params.programDbId !== undefined)
                parameters['programDbId'] = params.programDbId
            if (params.seasonDbId !== undefined)
                parameters['seasonDbId'] = params.seasonDbId

            conditionString = await processQueryHelper.getFilterString(
                parameters
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        experimentLocationsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            experimentLocationsQuery,
            conditionString,
            orderString
        )

        let experimentLocations = await sequelize
            .query(experimentLocationsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Get the final experimentLocations count
        if (
            await experimentLocations === undefined ||
            await experimentLocations.length < 1
        ) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            experimentLocationsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                experimentLocationsQuery,
                conditionString,
                orderString
            )

            experimentLocationsCount = await sequelize
                .query(experimentLocationsCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = experimentLocationsCount[0].count
        }

        res.send(200, {
            rows: experimentLocations,
            count: count
        })
        return
    }
}
