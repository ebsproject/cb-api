/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    /**
     * Search method for master.formula
     * POST /v3/formulas-search
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function (req, res) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinct on condition is set
            if (req.body.distinctOn !== undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let formulasQuery = null

        // Check if the client specified values for the field/s
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields'],
                )
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                formulasQuery = knex.select(selectString)
            } else {
                formulasQuery = knex.column(parameters['fields'].split('|'))
            }

            formulasQuery += `
                FROM
                    master.formula formula
                LEFT JOIN
                    tenant.person creator ON formula.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON formula.modifier_id = modifier.id
                LEFT JOIN
                    master.variable variable ON formula.result_variable_id = variable.id
                WHERE
                    formula.is_void = FALSE
                ORDER BY 
                    formula.id
            `
        } else {
            formulasQuery = `
                SELECT
                    ${addedDistinctString}
                    formula.id as "formulaDbId",
                    formula.formula as "formula",
                    formula.result_variable_id as "resultVariableDbId",
                    variable.abbrev as "resultVariableAbbrev",
                    variable.label as "resultVariableLabel",
                    variable.name as "resultVariableName",
                    formula.method_id as "methodDbId",
                    formula.data_level as "dataLevel",
                    formula.function_name as "functionName",
                    formula.remarks as "remarks",
                    formula.creation_timestamp as "creationTimestamp",
                    formula.creator_id as "creatorId",
                    formula.modification_timestamp as "modificationTimestamp",
                    formula.modifier_id as "modifierId",
                    formula.is_void as "isVoid",
                    formula.formatted_formula as "formattedFormula",
                    formula.database_formula as "databaseFormula",
                    formula.decimal_place as "decimalPlace",
                    (
                        SELECT COUNT(*)
                        FROM master.formula f2
                        WHERE f2.result_variable_id = formula.result_variable_id
                        AND f2.is_void = FALSE
                    ) AS "count"
                FROM
                    master.formula formula
                LEFT JOIN
                    tenant.person creator ON formula.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON formula.modifier_id = modifier.id
                LEFT JOIN
                    master.variable variable ON formula.result_variable_id = variable.id
                WHERE
                    formula.is_void = FALSE
                ORDER BY 
                    formula.id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let formulasFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            formulasQuery,
            conditionString,
            orderString
        )
        
        // Retrieve the formula records
        let formulas = await sequelize.query(formulasFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await formulas == undefined || await formulas.length < 1) {
            res.send(200, {
                rows: formulas,
                count: 0
            })
            return
        }

        // Get the final count of the formula records
        let formulasCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            formulasQuery,
            conditionString,
            orderString
        )

        let formulasCount = await sequelize.query(formulasCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = formulasCount[0].count
        
        res.send(200, {
            rows: formulas,
            count: count
        })
        return
    }
}