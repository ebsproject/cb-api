/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require("../../../config/sequelize");
let { knex } = require("../../../config/knex");
let processQueryHelper = require("../../../helpers/processQuery/index");
let errors = require("restify-errors");
let errorBuilder = require("../../../helpers/error-builder");

module.exports = {
  post: async (req, res, next) => {
    let limit = req.paginate.limit;
    let offset = req.paginate.offset;
    let sort = req.query.sort;
    let params = req.query;
    let count = 0;
    let orderString = "";
    let conditionString = "";
    let addedConditionString = "";
    let addedDistinctString = "";
    let addedOrderString = `
            ORDER BY
                shipment_item.id
        `;
    let parameters = {};
    parameters["distinctOn"] = "";

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters["fields"] = req.body.fields;
      }

      if (req.body.distinctOn != undefined) {
        parameters["distinctOn"] = req.body.distinctOn;
        addedDistinctString = await processQueryHelper.getDistinctString(
          parameters["distinctOn"]
        );

        parameters["distinctOn"] = `"${parameters["distinctOn"]}"`;
        addedOrderString = `
                    ORDER BY
                    ${parameters["distinctOn"]}
                `;
      }
      // Set columns to be excluded in parameters for filtering
      let excludedParametersArray = ["fields", "distinctOn"];
      // Get the filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      );

      if (conditionString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    // Build the base retrieval query
    let shipmentItemsQuery = null;

    // Check if the client specified values for the fields
    if (parameters["fields"] != null) {
      if (parameters["distinctOn"]) {
        let fieldsString = await processQueryHelper.getFieldValuesString(
          parameters["fields"]
        );
        let selectString = knex.raw(`${addedConditionString} ${fieldsString}`);
        shipmentItemsQuery = knex.select(selectString);
      } else {
        shipmentItemsQuery = knex.column(parameters["fields"].split("|"));
      }
      shipmentItemsQuery += `
            FROM
                inventory.shipment_item shipment_item
            WHERE
                shipment_item.is_void = FALSE
                ${addedOrderString}
            `;
    } else {
      shipmentItemsQuery = `
        SELECT
          ${addedDistinctString}
          shipment_item.id AS "shipmentItemDbId",
          shipment_item.shipment_id AS "shipmentDbId" , 
          shipment_item.germplasm_id  AS "germplasmDbId",
          shipment_item.seed_id AS "seedDbId",
          shipment_item.package_id AS "packageDbId",
          shipment_item.shipment_item_number AS "shipmentItemNumber",
          shipment_item.shipment_item_code AS "shipmentItemCode",
          shipment_item.shipment_item_status AS "shipmentItemStatus",
          shipment_item.shipment_item_weight AS "shipmentItemWeight",
          shipment_item.package_unit AS "packageUnit",
          shipment_item.package_count  AS "packageCount",
          shipment_item.test_code AS "testCode",
          shipment_item.mta_status AS "mtaStatus",
          shipment_item.availability AS "availability",
          shipment_item.use AS "use",
          shipment_item.mls_ancestors AS "mlsAncestors",
          shipment_item.genetic_stock AS "geneticStock",
          shipment_item.remarks AS "remarks",
          shipment_item.creator_id AS "creatorDbId",
          shipment_item.modifier_id AS "modifierDbId",
          shipment_item.notes  AS "notes",
          germplasm.designation AS "germplasmDesignation",
          germplasm.parentage AS "germplasmParentage",
          germplasm.germplasm_state AS "germplasmState",
          germplasm.germplasm_state AS "germplasmNormalizedName",
          germplasm.germplasm_state AS "germplasmType",
          germplasm.generation AS "germplasmGeneration",
          seed.seed_code as "seedCode",
          seed.seed_name as "seedName",
          seed.harvest_date as "harvestDate",
          seed.source_experiment_id as "sourceExperimentDbId",
          seed.source_occurrence_id as "seedOccurrenceDbId",
          seed.source_location_id as "seedLocationDbId",
          package.package_code as "packageCode",
          package.package_label as "packageLabel",
          seed.seed_code as "sampleId",
          (SELECT data_value from germplasm.seed_attribute where is_void = FALSE and seed_id=shipment_item.seed_id and variable_id=(Select id from master.variable where abbrev='ORIGIN') limit 1) as "origin",
          (SELECT data_value from germplasm.seed_attribute where is_void = FALSE and seed_id=shipment_item.seed_id and variable_id=(Select id from master.variable where abbrev='SPECIES')limit 1) as "species",
          (coalesce((SELECT occurrence_name from experiment.occurrence where is_void = FALSE and id=source_occurrence_id limit 1),'unknown')) as "sourceStudy",
          (SELECT array_to_string(array_agg(name_value), ', ') from germplasm.germplasm_name where germplasm_id=shipment_item.germplasm_id) as otherNames
        FROM
          inventory.shipment_item shipment_item
        LEFT JOIN germplasm.germplasm as germplasm on germplasm.id=shipment_item.germplasm_id
        LEFT JOIN germplasm.seed as seed on seed.id=shipment_item.seed_id
				LEFT JOIN germplasm.package as package on package.id=shipment_item.package_id
        WHERE
          shipment_item.is_void = FALSE
          ${addedOrderString}
        `;
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort);
      if (orderString.includes("invalid")) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      }
    }

    // Generate the final SQL query
    let shipmentItemsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      shipmentItemsQuery,
      conditionString,
      orderString
    );

    let shipmentItems = await sequelize
      .query(shipmentItemsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset,
        },
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.BadRequestError(errMsg));
        return;
      });

    if (
      (await shipmentItems) == undefined ||
      (await shipmentItems.length) < 1
    ) {
      res.send(200, {
        rows: [],
        count: 0,
      });
      return;
    }

    let shipmentItemsCountFinalSqlQuery =
      await processQueryHelper.getCountFinalSqlQuery(
        shipmentItemsQuery,
        conditionString,
        orderString
      );

    let shipmentItemsCount = await sequelize
      .query(shipmentItemsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async (err) => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004);
        res.send(new errors.InternalError(errMsg));
        return;
      });

    count = shipmentItemsCount[0].count;

    res.send(200, {
      rows: shipmentItems,
      count: count,
    });
    return;
  },
};
