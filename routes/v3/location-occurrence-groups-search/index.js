/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
const logger = require('../../../helpers/logger')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedDistinctString = ''
    let parameters = {}
    let excludedParametersArray = []
    let tableAliases = {}
    let columnAliases = {}
    let responseColString = `log.id as "locationOccurrenceGroupDbId",
      log.location_id AS "locationDbId",
      loc.location_name AS "locationName",
      loc.location_code AS "locationCode",
      log.occurrence_id AS "occurrenceDbId",
      occ.occurrence_name AS "occurrenceName",
      occ.occurrence_code AS "occurrenceCode",
      log.order_number AS "orderNumber",
      log.creation_timestamp AS "creationTimestamp",
      creator.id AS "creatorDbId",
      creator.person_name AS "creator",
      log.modification_timestamp AS "modificationTimestamp",
      modifier.id AS "modifierDbId",
      modifier.person_name AS "modifier"`
    let tableJoins = {
      'loc': ' JOIN experiment.location loc ON loc.id = log.location_id AND loc.is_void = FALSE',
      'occ': ' JOIN experiment.occurrence occ ON occ.id = log.occurrence_id AND occ.is_void = FALSE',
      'creator': ' JOIN tenant.person creator ON creator.id = log.creator_id',
      'modifier': ' LEFT JOIN tenant.person modifier ON modifier.id = log.modifier_id',
    }
    const endpoint = 'location-occurrence-groups-search'
    const operation = 'SELECT'

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }

      // Set the columns to be excluded in parameters for filtering
      excludedParametersArray = ['fields']

      // Get filter condition
      conditionStringArr = await processQueryHelper.getFilterWithInfo(
        req.body, 
        excludedParametersArray,
        responseColString
      )

      conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''
      tableAliases = conditionStringArr.tableAliases
      columnAliases = conditionStringArr.columnAliases

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))

        return
      }
    }

    // Build the base retrieval query
    let locOccGroupsQuery = null

    // Check if the client specified values for the field/s
    if (parameters['fields'] != null) {
      locOccGroupsQuery = knex.column(parameters['fields'].split('|'))

      locOccGroupsQuery += `
        FROM
          experiment.location_occurrence_group log
        JOIN
          experiment.location loc ON loc.id = log.location_id AND loc.is_void = FALSE
        JOIN
          experiment.occurrence occ ON occ.id = log.occurrence_id AND occ.is_void = FALSE
        JOIN 
          tenant.person creator ON creator.id = log.creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = log.modifier_id
        WHERE
          log.is_void = false
        ORDER BY
        log.id
        `
    } else {

      locOccGroupsQuery = `
        SELECT
          ${responseColString}
        FROM
          experiment.location_occurrence_group log
        JOIN
          experiment.location loc ON loc.id = log.location_id AND loc.is_void = FALSE
        JOIN
          experiment.occurrence occ ON occ.id = log.occurrence_id AND occ.is_void = FALSE
        JOIN 
          tenant.person creator ON creator.id = log.creator_id
        LEFT JOIN
          tenant.person modifier ON modifier.id = log.modifier_id
        WHERE
          log.is_void = false
        ORDER BY
          log.id
        `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))

        return
      }
    }

    // Generate the final SQL query
    locOccGroupsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      locOccGroupsQuery,
      conditionString,
      orderString
    )

    // Retrieve the location occurrence group records
    let locOccGroups = await sequelize
      .query(locOccGroupsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        await logger.logFailingQuery(endpoint, operation, err)
      })

    if (locOccGroups == undefined) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))

      return
    } else if (locOccGroups.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })

      return
    } else {
      
      // Get the final count of the location occurrence group records
      let locOccGroupsCountFinalSqlQuery = await processQueryHelper
        .getTotalCountQuery(
          req.body,
          excludedParametersArray,
          conditionString,
          addedDistinctString,
          tableJoins,
          'experiment.location_occurrence_group log',
          tableAliases,
          'log.id',
          columnAliases,
          orderString,
          responseColString
        )
      
      let locOccGroupsCount = await sequelize
        .query(locOccGroupsCountFinalSqlQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
          await logger.logFailingQuery(endpoint, operation, err)
        })

      if (locOccGroupsCount === undefined) {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))

        return
      } else if (locOccGroupsCount.length < 1) {
        res.send(200, {
            rows: [],
            count: 0
        })

        return
      }

      count = locOccGroupsCount[0].count
    }
    
    res.send(200, {
      rows: locOccGroups,
      count: count
    })

    return
  }
}