/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')

module.exports = {
  post: async function (req, res, next) {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let addedDistinctString = ''
    let addedOrderString = `ORDER BY crop.id`
    let parameters = {}
    parameters['distinctOn'] = ''

    if (req.body) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      } 

      // Set columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields', 'distinctOn']

      // Get filter condiiton
      conditionString = await processQueryHelper
        .getFilter(req.body, excludedParametersArray)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req. headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      if(req.body.distinctOn !== undefined) {
        parameters['distinctOn'] = req.body.distinctOn
        addedDistinctString = await processQueryHelper
          .getDistinctString(parameters['distinctOn'])
        
        parameters['distinctOn'] = `"${parameters['distinctOn']}"`
        addedOrderString = `
          ORDER BY
            ${parameters['distinctOn']}
        `
      }
    }

    // Build base retrieval query
    let cropsQuery = null

    // Check if user specified values in fields parameter
    if (parameters['fields']) {
      if (parameters['distinctOn']) {
        let fieldsString = await processQueryHelper
          .getFieldValuesString(parameters['fields'])
        let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
        cropsQuery = knex.select(selectString)
      } else {
        cropsQuery = knex.column(parameters['fields'].split('|'))
      }
      cropsQuery += `
        FROM
          tenant.crop crop
          LEFT JOIN
            tenant.crop_program "cropProgram" ON "cropProgram".crop_id = crop.id
          LEFT JOIN
            tenant.program program ON program.crop_program_id = "cropProgram".id
          LEFT JOIN
            tenant.person creator ON creator.id = crop.creator_id
          LEFT JOIN
            tenant.person modifier ON modifier.id = crop.modifier_id
        WHERE
          crop.is_void = FALSE
        ${addedOrderString}
      `
    } else {
      cropsQuery = `
        SELECT 
          crop.id AS "cropDbId",
          "cropProgram".id AS "cropProgramDbId",
          program.id AS "programDbId",
          crop.crop_code AS "cropCode",
          crop.crop_name AS "cropName",
          crop.description,
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          crop.creation_timestamp AS "creationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier",
          crop.modification_timestamp AS "modificationTimestamp"
        FROM
          tenant.crop crop
          LEFT JOIN
            tenant.crop_program "cropProgram" ON "cropProgram".crop_id = crop.id
          LEFT JOIN
            tenant.program program ON program.crop_program_id = "cropProgram".id
          LEFT JOIN
            tenant.person creator ON creator.id = crop.creator_id
          LEFT JOIN
            tenant.person modifier ON modifier.id = crop.modifier_id
        WHERE
          crop.is_void = FALSE
        ${addedOrderString}
      `
    }

    // Parse the sort parameters
    if (sort != null){
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate final SQL query
    let cropsFinalSqlQuery = await processQueryHelper
      .getFinalSqlQuery(
        cropsQuery,
        conditionString,
        orderString
      )

    let crops = await sequelize
      .query(cropsFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(cropsFinalSqlQuery))
        return
      })

    if (await crops === undefined || await crops.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    let cropsCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        cropsQuery,
        conditionString
      )
    
    let cropsCount = await sequelize
      .query(cropsCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })

    count = cropsCount[0].count

    res.send(200, {
      rows: crops,
      count: count
    })
    return
  }
}