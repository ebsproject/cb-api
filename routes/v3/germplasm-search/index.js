/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let productSearchHelper = require('../../../helpers/queryTool/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let errorBuilder = require('../../../helpers/error-builder')
let germplasmHelper = require('../../../helpers/germplasm/index.js')
let queryToolHelper = require('../../../helpers/queryTool/index.js')
let logger = require('../../../helpers/logger')
const { indexOf } = require('lodash')

module.exports = {

	post: async function (req, res, next) {
		const endpoint = 'germplasm-search'

		//Set defaults
		let limit = req.paginate.limit
		let offset = req.paginate.offset
		let sort = req.query.sort
		let count = 0
		let orderString = ''
		let conditionString = ''
		let addedConditionString = ''
		let addedDistinctString = ''
		let fields = null
		let distinctOn = ''
		let searchQuery = ''
		let addedNamesString = ''
		let excludedParametersArray = {}
		let orderQuery = ``
		let specificOrderQuery = ``

		// specific sort
		let itemArr = []
		let newSortOrder = ``

		// replacement values
		let nameValue = []
		let builtColumnAliases = []
		let builtFilters = []
		let filters

		// updated count query params
		let tableAliases = {}
		let columnAliases = {}
		let baseResponseColStr = `
			germplasm.id AS "germplasmDbId",
			germplasm.designation,
			germplasm.parentage,
			germplasm.generation,
			germplasm.germplasm_state AS "germplasmState",
			germplasm.germplasm_name_type AS "germplasmNameType",
			germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
			germplasm.germplasm_code AS "germplasmCode",
			germplasm.germplasm_type AS "germplasmType",
			germplasm.other_names AS "otherNames",
			germplasm.germplasm_document AS "germplasmDocument"
		`
		let responseColString = `
			germplasm.taxonomy_id AS "taxonomyDbId",
			taxonomy.taxon_id AS "taxonId",
			taxonomy.taxonomy_name AS "taxonomyName",
			crop.id AS "cropDbId",
			crop.crop_code AS "cropCode",
			crop.crop_name AS "cropName",
			crop.description AS "cropDescription",
			creator.id AS "creatorDbId",
			creator.person_name AS creator,
			modifier.id AS "modifierDbId",
			modifier.person_name AS modifier,
			germplasm.creation_timestamp AS "creationTimestamp",
			germplasm.modification_timestamp AS "modificationTimestamp"
		`

		if (req.body != undefined) {
			// Set columns to be excluded in parameters for filtering
			excludedParametersArray = ['fields', 'distinctOn', 'names', 'otherNames', 'designation']
			if (req.body.fields != null) {
				fields = req.body.fields
			}

			let parameters = req.body
			let browserFilters = []
			let condition = ``

			// check if has search parameters for designation, germplasm name
			if (req.body.designation != undefined) {
				let filter = req.body.designation
				let normalizedNameArr = []
				let newNameVal = ''
				var name = ''

				// build germplasmNormalizedName filter
				if(typeof req.body.designation === 'string'){
					for (value of filter.split('|')) {
						if (value.includes('equals')) {
							value = value.replace(/equals/gi, '')
							name = await germplasmHelper.normalizeText(value)
							name  = `equals ${name}`
						}
						else {
							name = await germplasmHelper.normalizeText(value)
							if(!value.includes('%')){
								name  = `equals ${name}`
							}
						}
						newNameVal = name.replaceAll('equals','').trim()

						if(nameValue.indexOf(newNameVal) == -1){
							nameValue.push(newNameVal)
						}
						
						normalizedNameArr.push(name)
					}
					parameters["germplasmNormalizedName"] = normalizedNameArr.join('|')
				}
				else{
					for (operator in filter) {
						filter = filter[operator]

						for (value of filter.split('|')) {
							value = value.replace(/equals/gi, '')
							name = await germplasmHelper.normalizeText(value)

							if(operator == 'list' && !value.toLowerCase().includes('equals') && !value.includes('%')){
								name = `equals ${name}`
							}
							newNameVal = name.replaceAll('equals','').trim()

							if(nameValue.indexOf(newNameVal) == -1){
								nameValue.push(newNameVal)
							}
							normalizedNameArr.push(name)
						}

						parameters["germplasmNormalizedName"] = {
							[operator] : normalizedNameArr.join('|')
						}
						
						builtFilters["germplasmNormalizedName"] = normalizedNameArr.join('|')
						builtColumnAliases.push('germplasm.germplasm_normalized_name AS "germplasmNormalizedName"')

						// sort according to input list items
						itemArr = normalizedNameArr.map(x => { return x.replace(/equals/gi, '') })
						newSortOrder = await queryToolHelper.getSortClauseForSpecificOrder(itemArr,"germplasmNormalizedName")

						specificOrderQuery = newSortOrder
					}
				}
				delete parameters['designation']
			}

			//Build query
			for (column in parameters) {
				if (excludedParametersArray.includes(column)) {
					continue
				}

				if (column == '__browser_filters__') {
					browserFilters = parameters['__browser_filters__']
					continue
				}

				filters = parameters[column]

				if(typeof filters == 'string' && filters != '' && filters != undefined){
					let filterValues = []

					for(value of filters.split('|')){
						if(!value.includes('equals') && !value.includes('%')){
						filterValues.push(`equals ${value}`)
						}
					}

					if(filterValues.length > 0){
						filters = filterValues.join('|')
					}

					builtFilters[column] = filters
				}

				if(typeof filters != 'string' && filters != '' && filters != undefined){
					builtFilters[column] = filters[Object.keys(filters)[0]]

					if(column == 'germplasmCode'){
						// sort according to input list items
						let codeValue = filters[Object.keys(filters)[0]].replace(/equals/gi, '')
						
						itemArr = codeValue.split('|').map(x => { return x.trim() })
						newSortOrder = await queryToolHelper.getSortClauseForSpecificOrder(itemArr,column)

						specificOrderQuery = newSortOrder
					}
				}

				condition = ``

				if(typeof filters != undefined && filters != null && Object.keys(filters).length > 0){
					condition = await productSearchHelper.getFilterCondition(filters, column,false,true,{'germplasmNormalizedName':":name"})

					if (condition !== '') {
						condition = `(`+condition+`)`

						if (searchQuery == '') {
							searchQuery = searchQuery +
							` WHERE `
							+ condition
						} else {
							searchQuery = searchQuery +
							` AND `
							+ condition
						}
					}
				}
			}

			// check if request came from cb browser; add filter from it
			if (browserFilters !== undefined && browserFilters.length != 0) {
				for (column in browserFilters) {

					// normalize values for designation and otherNames
					if(column == 'otherNames' || column == 'designation'){
						let tblColumn = ''
						let nameVal = browserFilters[column]
						let name = ''

						for(operator in browserFilters[column]){
							name = nameVal[operator]

							if(!name.includes('equals') && !name.includes('%')){
								name = `equals ${name}`
							}

							browserFilters[column][operator] = name

							tblColumn = column == 'otherNames' ? 'other_names' : 'designation'

							builtColumnAliases.push('germplasm.'+tblColumn+' AS '+column)
						}
					}

					filters = browserFilters[column]

					builtFilters[column] = typeof filters != 'string' && filters != '' && filters != undefined ?
											filters[Object.keys(filters)[0]] : filters
					
					let condition = productSearchHelper.getFilterCondition(filters, column)
					condition = `(`+condition+`)`

					if (addedConditionString == '') {
						addedConditionString = addedConditionString
						+ condition
					} else {
						addedConditionString = addedConditionString + 
						` AND `
						+ condition
					}
				}
				if (addedConditionString != '') {
					if (searchQuery == '') {
						searchQuery = searchQuery +
						` WHERE `
						+ addedConditionString
					} else {
						searchQuery = searchQuery +
						` AND `
						+ addedConditionString
					}
				}
			}

			if (req.body.distinctOn != undefined) {
				distinctOn = req.body.distinctOn
				addedDistinctString = await processQueryHelper
				.getDistinctString(
				distinctOn
				)
				distinctOn = `"${distinctOn}"`
			}

			// for names, it should search in designation and the germplasm_name
			if (req.body.names != undefined || req.body.otherNames != undefined) {
				let filter = ''
				let namesArray = []
				var name = ''
				let condition = ''
				let column = ''

				if(req.body.names != undefined){
					filter = req.body.names
					column = "names"
				}

				if(req.body.otherNames != undefined){
					filter = req.body.otherNames
					column = "otherNames"
				}

				for (value of filter.split('|')) {
					if (value.includes('equals')) {
						value = value.replace(/equals/gi, '')
						name = await germplasmHelper.normalizeText(value)

						namesArray.push(name)
					} 
					else if(value.includes('%')){
						name = await germplasmHelper.normalizeText(value)
						let tempCondition

						if (condition != ''){ condition += ` OR `}

						tempCondition =  `(
							germplasmName.germplasm_normalized_name like :name
						)`

						nameValue.push(`${name}`)
						condition += tempCondition
					}
					else {
						name = await germplasmHelper.normalizeText(value)

						name  = `${name}`

						nameValue.push(name)
						namesArray.push(name)
					}
				}

				if (namesArray.length > 0) {
					if (condition != ''){ condition += ` OR `}
					nameValue = namesArray
					condition += `(
					germplasmName.germplasm_normalized_name IN ( :name )
					)`
				}

				if (condition != '') {
					addedNamesString = `(${condition})`
				}
				builtFilters[column] = nameValue.join('|')
			}
		}

		responseColString = `
			${baseResponseColStr},
			${responseColString}
		`

		let conditionStringArr = await processQueryHelper.getFilterWithInfo(
			builtFilters,
			excludedParametersArray,
			responseColString
		)

		conditionString = (conditionStringArr.mainQuery != undefined) ?  conditionStringArr.mainQuery : ''
		if(addedNamesString != ''){
			conditionString += addedNamesString
		}

		tableAliases = conditionStringArr.tableAliases
		columnAliases = conditionStringArr.columnAliases

		columnAliases = [...new Set(columnAliases)];

		if(builtColumnAliases != undefined && builtColumnAliases.length > 0){
			for (let value of builtColumnAliases) {

				// parse column with alias
				if(value.includes('AS')){
					value = value.split('AS')[0].trim()
				}

				if(columnAliases.indexOf(value) === -1){
					columnAliases.push(value)
				}
			}
		}

		conditionString = searchQuery
		// Build the base retrieval query
		let germplasmQuery = null

		if (distinctOn) {
			orderQuery = `
			ORDER BY ${distinctOn}
			`
		}

		let germplasmNameQuery = `germplasm.germplasm germplasm`
		if(addedNamesString.length > 0) {
			germplasmNameQuery = `
				(
				SELECT
					DISTINCT
					germplasmName.germplasm_id
				FROM
					germplasm.germplasm_name AS germplasmName
				WHERE
					${addedNamesString}
					AND germplasmName.is_void = FALSE
					AND germplasmName.germplasm_name_status <> 'draft'
				) AS germplasmName
				INNER JOIN
				germplasm.germplasm germplasm
				ON germplasmName.germplasm_id = germplasm.id
			`
		}

		// Check if the client specified values for the fields parameter
		let fromQuery = `
			FROM
				${germplasmNameQuery}
				LEFT JOIN germplasm.taxonomy taxonomy ON taxonomy.id = germplasm.taxonomy_id AND taxonomy.is_void = FALSE
				LEFT JOIN tenant.crop crop ON crop.id = germplasm.crop_id AND crop.is_void = FALSE
				INNER JOIN tenant.person AS creator ON creator.id = germplasm.creator_id
				LEFT JOIN tenant.person AS modifier ON modifier.id = germplasm.modifier_id
			WHERE
				germplasm.is_void = FALSE
			${orderQuery}
		`

		if (fields != null) {
			if(germplasmNameQuery != ''){
				fields += "|germplasm_normalized_name AS germplasmNormalizedName"
			}
			
			if (distinctOn) {
				let fieldValues = fields.split('|')
				let fieldValuesString = ''
				for (let value of fieldValues) {
					fieldValuesString += `${value}, `
				}
				// clean the fieldValuesString
				fieldValuesString = fieldValuesString.replace(/,\s*$/, "");
				let x = knex.raw(`${addedDistinctString} ${fieldValuesString}`)
				germplasmQuery = knex.select(x)
			} 
			else {
				germplasmQuery = knex.column(fields.split('|'))
			}
			germplasmQuery += fromQuery
		} else {
			if (!distinctOn) {
				addedDistinctString = ''
			}
			germplasmQuery = `
				SELECT
					${addedDistinctString}
					${responseColString}
				` + fromQuery
		}

		// Parse the sort parameters
		if (sort != null) {
			orderString = await processQueryHelper.getOrderString(sort)
			if (orderString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400004)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		} else {
			if (orderQuery == ``) {
				orderString = ``
			}
			if (specificOrderQuery) {
				orderString = specificOrderQuery
			}
		}

		// Generate the final sql query
		let finalSqlQuery = await processQueryHelper.getFinalSqlQuery(
			germplasmQuery,
			conditionString,
			orderString
		)

		/**
		 * Temporary fix: Manually replacing values in query, instead of using replacements
		 * Why? germplasm name values with ':' are being treated as object/placeholders like
		 * :name, :limit, and :offset and are not processed properly. Need to check if this
		 * happens with other search tools.
		 */

		finalSqlQuery = finalSqlQuery.replace(':name', '$$$' + nameValue.join(`$$$,$$$`) + '$$$')
		finalSqlQuery = finalSqlQuery.replace(':limit', limit)
		finalSqlQuery = finalSqlQuery.replace(':offset', offset)

		// Retrieve germplasm from the database
		let germplasm = await sequelize.query(finalSqlQuery, {
			type: sequelize.QueryTypes.SELECT
		})
		.catch(async err => {
			await logger.logFailingQuery(endpoint, 'SELECT', err)
		})

		if (await germplasm == undefined) {
			let errMsg = await errorBuilder.getError(req.headers.host, 500004)
			res.send(new errors.InternalError(errMsg))

			return
		}
		
		if(await germplasm.length < 1){
			germplasm = []
			count = 0
		}
		else {
			// Get count
			let tableJoins = {
				'taxonomy': 'LEFT JOIN germplasm.taxonomy taxonomy ON taxonomy.id = germplasm.taxonomy_id AND taxonomy.is_void = FALSE',
				'crop': 'LEFT JOIN tenant.crop crop ON crop.id = germplasm.crop_id AND crop.is_void = FALSE',
				'creator': ' INNER JOIN tenant.person creator ON germplasm.creator_id = creator.id',
				'modifier':' LEFT JOIN tenant.person modifier ON germplasm.modifier_id = modifier.id'
			}

			let mainTable = 'germplasm.germplasm germplasm';
			if(germplasmNameQuery != ''){
				mainTable = germplasmNameQuery
			}

			if(columnAliases == undefined || columnAliases.length == 0){
				columnAliases = [baseResponseColStr]
			}

			let finalGermplasmCountQuery = await processQueryHelper.getTotalCountQuery(
					req.body,
					excludedParametersArray,
					conditionString,
					addedDistinctString,
					tableJoins,
					mainTable,
					tableAliases,
					'germplasm.id',
					columnAliases,
					orderString,
					responseColString
				)

			/**
			 * Temporary fix: Manually replacing values in query, instead of using replacements
			 * Why? germplasm name values with ':' are being treated as object/placeholders like
			 * :name, :limit, and :offset and are not processed properly. Need to check if this
			 * happens with other search tools.
			 */
			finalGermplasmCountQuery = finalGermplasmCountQuery.replace(':name', '$$$' + nameValue.join(`$$$,$$$`) + '$$$')

			let germplasmCount = await sequelize.query(finalGermplasmCountQuery, {
				type: sequelize.QueryTypes.SELECT
			})
			.catch(async err => {
				await logger.logFailingQuery(endpoint, 'SELECT', err)
			})

			if(germplasmCount == undefined){
				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))

				return
			}

			if(	await germplasmCount != undefined && 
				await germplasmCount.length > 0 &&
				await germplasmCount[0] != undefined ){

				count = germplasmCount[0].count

			}
			
		}

		res.send(200, {
			rows: germplasm,
			count: count
		})
		return
	}
}