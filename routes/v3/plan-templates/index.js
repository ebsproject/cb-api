/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


let { sequelize } = require('../../../config/sequelize')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')

module.exports = {
  post: async (req, res, next) => {
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure
    // Set URL for response
    let planTemplatesUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/plan-templates'

    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let records = []
    let recordCount = null
    
    try {
      if (req.body.records == undefined || req.body.records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }

      records = req.body.records
      recordCount = records.length
    } catch (e) {
      console.log(e)
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    let transaction
    
    try {
      transaction = await sequelize.transaction({ autocommit: false })

      let planTemplateValuesArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0
        
         // Check if the required colums are in the input
         if (
          !record.templateName ||
          !record.programDbId ||
          !record.entityType ||
          !record.entityDbId
        ) {
          res.send(new errors.BadRequestError('400: Required parameters are missing. Ensure that the templateName, mandatoryInfo, programDbId, design, entityType, and entityDbId fields are not empty.'))
          return
        }
        // Validate all values that relate to another table (not necessarily foreign keys)
        let programDbId = null
        if (record.programDbId !== undefined) {
          programDbId = record.programDbId
          // Check if programDbId is a valid integer
          if (!validator.isInt(programDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400125)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
          // Validate if program exists given programDbId
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if the program exists, return 1
              SELECT
                count(1)
              FROM
                tenant.program program
              WHERE
                program.is_void = FALSE AND
                program.id = ${programDbId}
            )
          `

          validateCount += 1
        }

        let templateName = null
        if (record.templateName !== undefined) {
          templateName = record.templateName
          
          // Validate if template exists
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if the templateName exists, return 1
              SELECT
                CASE WHEN
                (count(1) = 0)
                THEN 1
                ELSE 0
              END AS count
              FROM
                platform.plan_template planTemplate
              WHERE
                planTemplate.is_void = FALSE AND
                planTemplate.template_name ILIKE '${templateName}'
            )
          `

          validateCount += 1
        }

        let entityDbId = null
        if (record.entityDbId !== undefined) {
          entityDbId = record.entityDbId
          // Check if entityDbId is a valid integer
          if (!validator.isInt(entityDbId)) {
            let errMsg = "Invalid format for entityDbId. ID must be an integer."
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        if (record.programDbId !== undefined) {
          // Validate the filters in the DB; Count must be equal to validateCount
          let checkInputQuery = `SELECT ${validateQuery} AS count`
          let inputCount = await sequelize.query(checkInputQuery, {
            type: sequelize.QueryTypes.SELECT
          })
          
          if (inputCount[0]['count'] != validateCount) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        let tempFinalCabinetDbId
        if (record.tempFinalCabinetDbId !== undefined) {
          tempFinalCabinetDbId = record.tempFinalCabinetDbId
          if (!validator.isInt(tempFinalCabinetDbId)) {
            let errMsg =
              "Invalid format for tempFinalCabinetDbId. ID must be an integer."
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        let entryCount
        if (record.entryCount !== undefined) {
          entryCount = record.entryCount
          if (!validator.isInt(entryCount)) {
            let errMsg =
              "Invalid format for entryCount. Count must be an integer."
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        let plotCount
        if (record.plotCount !== undefined) {
          plotCount = record.plotCount
          if (!validator.isInt(plotCount)) {
            let errMsg =
              "Invalid format for plot count. Count must be an integer."
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        let checkCount
        if (record.checkCount !== undefined) {
          checkCount = record.checkCount
          if (!validator.isInt(checkCount)) {
            let errMsg =
              "Invalid format for checkCount. Count must be an integer."
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        let repCount
        if (record.repCount !== undefined) {
          repCount = record.repCount
          if (!validator.isInt(repCount)) {
            let errMsg =
              "Invalid format for repCount. Count must be an integer."
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        let mandatoryInfo =
          (record.mandatoryInfo !== undefined) ? record.mandatoryInfo : null

        let status = (record.status !== undefined) ? record.status : null
        let remarks = (record.remarks !== undefined) ? record.remarks : null
        let design = (record.design !== undefined) ? record.design : null
        let occurrenceApplied = (record.occurrenceApplied !== undefined) ? record.occurrenceApplied : null
        let notes = (record.notes !== undefined) ? record.notes : null

        let entityType =
          (record.entityType !== undefined) ? record.entityType : null

        // Must be valid JSON
        let randomizationResults
        if (record.randomizationResults !== undefined) {
          randomizationResults = record.randomizationResults
          if (!validator.isJSON(randomizationResults)) {
            let errMsg =
              "Invalid format for randomizationResults. It must be a valid JSON string."
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        } else {
          randomizationResults = null
        }

        let randomizationDataResults
        if (record.randomizationDataResults !== undefined) {
          randomizationDataResults = record.randomizationDataResults
          if (!validator.isJSON(randomizationDataResults)) {
            let errMsg =
              "Invalid format for randomizationDataResults. It must be a valid JSON string."
            res.send(new errors.BadRequestError(errMsg))
            return
          } else {
            randomizationDataResults = null
          }
        }

        let randomizationInputFile
        if (record.randomizationInputFile !== undefined) {
          randomizationInputFile = record.randomizationInputFile
          if (!validator.isJSON(randomizationInputFile)) {
            let errMsg =
              "Invalid format for randomizationInputFile. It must be a valid JSON string."
            res.send(new errors.BadRequestError(errMsg))
            return
          } else {
            randomizationDataResults = null
          }
        }

        let tempArray = [
          templateName,
          mandatoryInfo,
          tempFinalCabinetDbId,
          randomizationResults,
          randomizationInputFile,
          randomizationInputFile,
          status,
          remarks,
          'now()',
          userDbId,
          programDbId,
          occurrenceApplied,
          design,
          entryCount,
          plotCount,
          checkCount,
          repCount,
          entityType,
          entityDbId,
          notes
        ]

        planTemplateValuesArray.push(tempArray)
      }

      // Create plan template record
      let planTemplateQuery = format(`
        INSERT INTO
          platform.plan_template
            (
              template_name,
              mandatory_info,
              temp_file_cabinet_id,
              randomization_results,
              randomization_data_results,
              randomization_input_file,
              status,
              remarks,
              creation_timestamp,
              creator_id,
              program_id,
              occurrence_applied,
              design,
              entry_count,
              plot_count,
              check_count,
              repcount,
              entity_type,
              entity_id,
              notes
            )
        VALUES
          %L
        RETURNING id`, planTemplateValuesArray
      )
      
      planTemplateQuery = planTemplateQuery.trim()

      let planTemplates = await sequelize
        .query(planTemplateQuery, {
          type: sequelize.QueryTypes.INSERT,
          transaction: transaction
        })

      let resultArray = []
      for (let planTemplate of planTemplates[0]) {
        planTemplateDbId = planTemplate.id

        let obj = {
          planTemplateDbId: planTemplateDbId,
          recordCount: 1,
          href: planTemplatesUrlString + '/' + planTemplateDbId
        }

        resultArray.push(obj)
      }

      await transaction.commit()

      res.send(200, {
        rows: resultArray,
        count: resultArray.length
      })
      return

    } catch (e) {
      // Rollback transaction if any errors were encountered
      if (e) await transaction.rollback()
      console.log(e)
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}