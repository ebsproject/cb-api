/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let format = require('pg-format')

module.exports = {

    // Endpoint for retrieving a specific plan template
    // GET /v3/plan-templates/:id
    get: async function (req, res, next) {

        // Retrieve the plan template ID

        let planTemplateId = req.params.id

        if (!validator.isInt(planTemplateId)) {
            let errMsg = "You have provided an invalid format for the plan template ID."
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let planTemplateQuery = `
                SELECT
                    planTemplate.id AS "planTemplateDbId",
                    planTemplate.template_name as "templateName",
                    planTemplate.mandatory_info as "mandatoryInfo",
                    planTemplate.temp_file_cabinet_id as "tempFileCabinetDbId",
                    planTemplate.randomization_results as "randomizationResults",
                    planTemplate.randomization_data_results as "randomizationDataResults",
                    planTemplate.randomization_input_file as "randomizationInputFile",
                    planTemplate.status,
                    planTemplate.remarks,
                    planTemplate.design,
                    planTemplate.notes,
                    planTemplate.program_id as "programDbId",
                    planTemplate.entity_type as "entityType",
                    planTemplate.entity_id as "entityDbId",
                    planTemplate.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    planTemplate.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier
                FROM 
                    platform.plan_template planTemplate
                LEFT JOIN 
                    tenant.person creator ON planTemplate.creator_id = creator.id
                LEFT JOIN 
                    tenant.person modifier ON planTemplate.modifier_id = modifier.id
                WHERE 
                    planTemplate.id = ${planTemplateId}
                    AND planTemplate.is_void = FALSE 
            `

            // Retrieve plan template from the database   
            let planTemplates = await sequelize.query(planTemplateQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await planTemplates == undefined || await planTemplates.length < 1) {
                let errMsg = "The plan template you have requested does not exist"
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: planTemplates
            })
            return
        }
    },

    // Endpoint for updating an existing plan template record
    // PUT /v3/plan-templates/:id
    put: async function (req, res, next) {

        // Retrieve the plan template ID
        let planTemplateDbId = req.params.id

        if(!validator.isInt(planTemplateDbId)) {
            let errMsg = `Invalid format, plan template ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let mandatoryInfo = null
        let tempFileCabinetDbId = null
        let randomizationResults = null
        let randomizationDataResults = null
        let randomizationInputFile = null
        let status = null
        let occurrenceApplied = null
        let entryCount = null
        let plotCount = null
        let checkCount = null
        let repCount = null
        let design = null
        let requestDbId = null
        let notes = null
        let remarks = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if plan template is existing
        // Build query
        let planTemplateQuery = `
            SELECT
                program.id AS "programDbId",
                pt.mandatory_info AS "mandatoryInfo",
                pt.temp_file_cabinet_id AS "tempFileCabinetDbId",
                pt.randomization_results AS "randomizationResults",
                pt.randomization_data_results AS "randomizationDataResults",
                pt.randomization_input_file AS "randomizationInputFile",
                pt.status,
                pt.occurrence_applied AS "occurrenceApplied",
                pt.entry_count AS "entryCount",
                pt.plot_count AS "plotCount",
                pt.check_count AS "checkCount",
                pt.repcount,
                pt.design,
                pt.notes,
                pt.remarks,
                pt.request_id AS "requestDbId",
                creator.id AS "creatorDbId"
            FROM
                platform.plan_template pt
            LEFT JOIN 
                tenant.program program ON program.id = pt.program_id
            LEFT JOIN 
                tenant.person creator ON creator.id = pt.creator_id
            WHERE
                pt.is_void = FALSE AND
                pt.id = ${planTemplateDbId}
        `

        // Retrieve plan template from the database
        let planTemplate = await sequelize.query(planTemplateQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await planTemplate === undefined || await planTemplate.length < 1) {
            let errMsg = `The plan template you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        let recordProgramDbId = planTemplate[0].programDbId
        let recordMandatoryInfo = planTemplate[0].mandatoryInfo
        let recordTempFileCabinetDbId = planTemplate[0].tempFileCabinetDbId
        let recordRandomizationResults = planTemplate[0].randomizationResults
        let recordRandomizationDataResults = planTemplate[0].randomizationDataResults
        let recordRandomizationInputFile = planTemplate[0].randomizationInputFile
        let recordStatus = planTemplate[0].status
        let recordOccurrenceApplied = planTemplate[0].occurrenceApplied
        let recordEntryCount = planTemplate[0].entryCount
        let recordPlotCount = planTemplate[0].plotCount
        let recordCheckCount = planTemplate[0].checkCount
        let recordRepCount = planTemplate[0].repCount
        let recordDesign = planTemplate[0].design
        let recordRequestDbId = planTemplate[0].requestDbId
        let recordCreatorDbId = planTemplate[0].creatorDbId
        let recordNotes = planTemplate[0].notes
        let recordRemarks = planTemplate[0].remarks

        // Check if user is an admin or an owner of the cross
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                WHERE 
                    program.id = ${recordProgramDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let planTemplateUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/plan-templates/"
            + planTemplateDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body    

        // Start transaction
        let transaction
        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/

            if (data.mandatoryInfo !== undefined) {
                mandatoryInfo = data.mandatoryInfo

                // Check if user input is same with the current value in the database
                if (mandatoryInfo != recordMandatoryInfo) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        mandatory_info = $$${mandatoryInfo}$$
                    `
                }
            }

            if (data.tempFileCabinetDbId !== undefined) {
                tempFileCabinetDbId = data.tempFileCabinetDbId

                if (!validator.isInt(tempFileCabinetDbId)) {
                    let errMsg = `Invalid format, temp file cabiner ID must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (tempFileCabinetDbId != recordTempFileCabinetDbId) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        temp_file_cabinet_id = $$${tempFileCabinetDbId}$$
                    `
                }
            }

            if (data.randomizationResults !== undefined) {
                randomizationResults = data.randomizationResults

                if (!validator.isJSON(randomizationResults)) {
                    let errMsg = `Invalid format, randomizationResults field must be a valid JSON string.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (randomizationResults != recordRandomizationResults) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        randomization_results = $$${randomizationResults}$$
                    `
                }
            }

            if (data.randomizationDataResults !== undefined) {
                randomizationDataResults = data.randomizationDataResults

                if (!validator.isJSON(randomizationDataResults)) {
                    let errMsg = `Invalid format, randomizationDataResults field must be a valid JSON string.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (randomizationDataResults != recordRandomizationDataResults) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        randomization_data_results = $$${randomizationDataResults}$$
                    `
                }
            }

            if (data.randomizationInputFile !== undefined) {
                randomizationInputFile = data.randomizationInputFile

                if (!validator.isJSON(randomizationInputFile)) {
                    let errMsg = `Invalid format, randomizationInputFile field must be a valid JSON string.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (randomizationInputFile != recordRandomizationInputFile) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        randomization_input_file = $$${randomizationInputFile}$$
                    `
                }
            }

            if (data.status !== undefined) {
                status = data.status

                // Check if user input is same with the current value in the database
                if (status != recordStatus) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        status = $$${status}$$
                    `
                }
            }

            if (data.design !== undefined) {
                design = data.design

                // Check if user input is same with the current value in the database
                if (design != recordDesign) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        design = $$${design}$$
                    `
                }
            }

            
            if (data.occurrenceApplied !== undefined) {
                occurrenceApplied = data.occurrenceApplied

                // Check if user input is same with the current value in the database
                if (occurrenceApplied != recordOccurrenceApplied) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        occurrence_applied = $$${occurrenceApplied}$$
                    `
                }
            }

            if (data.entryCount !== undefined) {
                entryCount = data.entryCount

                if (!validator.isInt(entryCount)) {
                    let errMsg = `Invalid format, entry count must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (entryCount != recordEntryCount) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_count = $$${entryCount}$$
                    `
                }
            }

            if (data.plotCount !== undefined) {
                plotCount = data.plotCount

                if (!validator.isInt(plotCount)) {
                    let errMsg = `Invalid format, plot count must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (plotCount != recordPlotCount) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        plot_count = $$${plotCount}$$
                    `
                }
            }

            if (data.checkCount !== undefined) {
                checkCount = data.checkCount

                if (!validator.isInt(checkCount)) {
                    let errMsg = `Invalid format, check count must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (checkCount != recordCheckCount) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        check_count = $$${checkCount}$$
                    `
                }
            }

            if (data.repCount !== undefined) {
                repCount = data.repCount

                if (!validator.isInt(repCount)) {
                    let errMsg = `Invalid format, rep count must be an integer.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (repCount != recordRepCount) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        repcount = $$${repCount}$$
                    `
                }
            }

            if (data.requestDbId !== undefined) {
                requestDbId = data.requestDbId

                // Check if user input is same with the current value in the database
                if (requestDbId != recordRequestDbId) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        request_id = $$${requestDbId}$$
                    `
                }
            }

            if (data.notes !== undefined) {
                notes = data.notes

                // Check if user input is same with the current value in the database
                if (notes != recordNotes) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        notes = $$${notes}$$
                    `
                }
            }

            if (data.remarks !== undefined) {
                remarks = data.remarks

                // Check if user input is same with the current value in the database
                if (remarks != recordRemarks) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        remarks = $$${remarks}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the plan template record
            let updatePlanTemplateQuery = `
                UPDATE
                    platform.plan_template
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${planTemplateDbId}
            `

            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            await sequelize.query(updatePlanTemplateQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            // Return the transaction info
            let resultArray = {
                planTemplateDbId: planTemplateDbId,
                recordCount: 1,
                href: planTemplateUrlString
            }
            
            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing plan template record
    // DELETE /v3/plan-templates/:id
    delete: async function (req, res, next) {

        // Retrieve the plan template ID
        let planTemplateDbId = req.params.id

        if(!validator.isInt(planTemplateDbId)) {
            let errMsg = `Invalid format, plan template ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let planTemplateQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    program.id AS "programDbId"
                FROM
                    platform.plan_template pt
                LEFT JOIN
                    tenant.person creator ON creator.id = pt.creator_id
                LEFT JOIN
                    tenant.program program ON program.id = pt.program_id
                WHERE
                    pt.is_void = FALSE AND
                    pt.id = ${planTemplateDbId}
            `

            let planTemplate = await sequelize.query(planTemplateQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await planTemplate == undefined || await planTemplate.length < 1) {
                let errMsg = `The plan template you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = planTemplate[0].creatorDbId
            let recordProgramDbId = planTemplate[0].programDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                    SELECT 
                        person.id,
                        person.person_role_id AS "role"
                    FROM 
                        tenant.person person
                        LEFT JOIN 
                        tenant.team_member teamMember ON teamMember.person_id = person.id
                        LEFT JOIN 
                        tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                        LEFT JOIN 
                        tenant.program program ON program.id = programTeam.program_id 
                    WHERE 
                        experiment.id = ${recordProgramDbId} AND
                        person.id = ${personDbId} AND
                        person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deletePlanTemplateQuery = format(`
                UPDATE
                    platform.plan_template
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${planTemplateDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${planTemplateDbId}
            `)

            await sequelize.query(deletePlanTemplateQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { 
                    planTemplateDbId: planTemplateDbId 
                }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
