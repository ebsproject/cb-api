/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, Phase } = require("../../../config/sequelize");
let errors = require("restify-errors");
let userValidator = require("../../../helpers/person/validator.js");
let tokenHelper = require("../../../helpers/auth/token.js");
let Sequelize = require("sequelize");
let Op = Sequelize.Op;
let processQueryHelper = require("../../../helpers/processQuery/index.js");
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    // Endpoint for retrieving all phases
    // GET /v3/phases
    get: async function (req, res, next) {
        // Set defaults
        let limit = req.paginate.limit;

        let offset = req.paginate.offset;

        let params = req.query;

        let sort = req.query.sort;

        let count = 0;

        let conditionString = "";

        let orderString = "";

        // Build query
        let phasesQuery = `
            SELECT 
                phase.id AS "phaseDbId",
                phase.abbrev,
                phase.name,
                phase.description,
                phase.display_name AS "displayName",
                phase.remarks,
                phase.rank,
                phase.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.display_name AS creator,
                phase.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.display_name AS modifier
            FROM 
                master.phase phase
            LEFT JOIN 
                master.user creator ON phase.creator_id = creator.id
            LEFT JOIN 
                master.user modifier ON phase.modifier_id = modifier.id
            WHERE 
                phase.is_void = FALSE
            ORDER BY 
                phase.id
        `;

        // Get filter condition for abbrev only
        if (params.abbrev !== undefined) {
            let parameters = {
                abbrev: params.abbrev
            };

            conditionString = await processQueryHelper.getFilterString(
                parameters
            );

            if (conditionString.includes("invalid")) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort);

            if (orderString.includes("invalid")) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query
        phaseFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            phasesQuery,
            conditionString,
            orderString
        );

        // Retrieve phases from the database
        let phases = await sequelize
            .query(phaseFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            });

        if (phases === undefined || phases.length < 1) {
            res.send(200, {
              rows: phases,
              count: 0
            })
            return
        } else {
            // Get count
            phaseCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                phasesQuery,
                conditionString,
                orderString
            );

            phaseCount = await sequelize
                .query(phaseCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = phaseCount[0].count;
        }

        res.send(200, {
            rows: phases,
            count: count
        })
        return
    },

    // Endpoint for creating a new phase record
    post: async function (req, res, next) {
        if (req.body == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req);

        if (userId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if the user is an administrator. If not, return HTTP 401.
        if (!(await userValidator.isAdmin(userId))) {
            let errMsg = await errorBuilder.getError(req.headers.host, 403004)
            res.send(new errors.ForbiddenError(errMsg))
            return;
        } else {
            // Check for duplicate records
            Phase.count({
                where: {
                    [Op.or]: [
                        { abbrev: { [Op.iLike]: req.body.abbrev } },
                        { name: { [Op.iLike]: req.body.name } }
                    ]
                }
            }).then(async count => {
                if (count > 0) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400032)
                    res.send(new errors.BadRequestError(errMsg))
                    return;
                } else {
                    // Create new record
                    Phase.create({
                        abbrev: req.body.abbrev,
                        name: req.body.name,
                        description: req.body.description,
                        display_name: req.body.displayName,
                        remarks: req.body.remarks,
                        creator_id: userId,
                        rank: req.body.rank,
                        notes: req.body.notes
                    })
                        .then(phase => {
                            res.send(201, {
                                id: phase.id
                            });
                            return;
                        })
                        .catch(sequelize.Sequelize.ValidationError, async function (
                            err
                        ) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400032)
                            res.send(new errors.BadRequestError(errMsg))
                            return;
                        })
                        .catch(function (err) {
                            res.send(500);
                            return;
                        });
                }
            });
        }
    }
};
