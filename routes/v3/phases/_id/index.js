/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, Phase, Study } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let userValidator = require('../../../../helpers/person/validator.js')
let tokenHelper = require('../../../../helpers/auth/token.js')
let validator = require('validator')
let Sequelize = require('sequelize')
let Op = Sequelize.Op;
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/phases/:id
    get: async function (req, res, next) {

        // Retrieve ID

        let phaseDbId = req.params.id

        if (!validator.isInt(phaseDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400033)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let phasesQuery = `
                SELECT 
                    phase.id AS "phaseDbId",
                    phase.abbrev,
                    phase.name,
                    phase.description,
                    phase.display_name AS "displayName",
                    phase.remarks,
                    phase.rank,
                    phase.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.display_name AS creator,
                    phase.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.display_name AS modifier
                FROM master.phase phase
                LEFT JOIN 
                    master.user creator ON phase.creator_id = creator.id
                LEFT JOIN 
                    master.user modifier ON phase.modifier_id = modifier.id
                WHERE 
                    phase.is_void = FALSE
                    AND phase.id = (:phaseDbId)
            `;

            // Retrieve phases from the database   
            let phases = await sequelize.query(phasesQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    phaseDbId: phaseDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (phases.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404010)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: phases
            })
            return
        }
    },

    // Endpoint for modifying an existing phase record
    put: async function (req, res, next) {

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if the user is an administrator. If not, return HTTP 401.
        if (!await userValidator.isAdmin(userId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 403001)
            res.send(
                new errors.ForbiddenError(
                    errMsg
                )
            )
            return
        }

        else {
            let phaseDbId = req.params.id

            if (!validator.isInt(phaseDbId)) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400033)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            Phase.count({
                where: {
                    id: phaseDbId
                }
            }).then(async count => {
                if (count < 1) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 404010)
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }
                else {
                    // Check if a study is using the phase
                    Study.count({
                        where: {
                            'phase_id': phaseDbId,
                        }
                    }).then(async count => {

                        // If a study is already using the phase, it cannot be modified.
                        if (count > 0) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400034)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                        else {

                            // Check for parameters
                            if (req.body != null) {

                                let phaseUpdateParameters = {}

                                if (req.body.abbrev != null) {
                                    phaseUpdateParameters['abbrev'] = req.body.abbrev

                                    if (phaseUpdateParameters['abbrev'] == "") {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                                        res.send(new errors.BadRequestError(errMsg))
                                        return
                                    }
                                }

                                if (req.body.name != null) {
                                    phaseUpdateParameters['name'] = req.body.name

                                    if (phaseUpdateParameters['name'] == "") {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 400035)
                                        res.send(new errors.BadRequestError(errMsg))
                                        return
                                    }
                                }

                                if (req.body.description != null) {
                                    phaseUpdateParameters['description'] = req.body.description
                                }

                                if (req.body.displayName != null) {
                                    phaseUpdateParameters['display_name'] = req.body.displayName
                                }

                                if (req.body.remarks != null) {
                                    phaseUpdateParameters['remarks'] = req.body.remarks
                                }

                                if (req.body.rank != null) {
                                    if (!validator.isInt(req.body.rank)) {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 400083)
                                        res.send(new errors.BadRequestError(errMsg))
                                        return
                                    }
                                    phaseUpdateParameters['rank'] = req.body.rank
                                }

                                phaseUpdateParameters['modifier_id'] = userId
                                phaseUpdateParameters['modification_timestamp'] = new Date()

                                let parameters = JSON.parse(JSON.stringify(phaseUpdateParameters))
                                let phaseUniquenessConditionArray = []

                                // Build condition for checking whether the update will result to
                                // a duplicate record.

                                if (phaseUpdateParameters['abbrev'] !== undefined) {
                                    phaseUniquenessConditionArray.push(
                                        { abbrev: { [Op.iLike]: req.body.abbrev } }
                                    )
                                }

                                if (phaseUpdateParameters['name'] !== undefined) {
                                    phaseUniquenessConditionArray.push(
                                        { name: { [Op.iLike]: req.body.name } }
                                    )
                                }

                                // Check for duplicate records 
                                Phase.count({
                                    where: {
                                        [Op.or]: phaseUniquenessConditionArray
                                    }
                                }).then(async count => {
                                    if (count > 0) {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 400036)
                                        res.send(new errors.BadRequestError(errMsg))
                                        return
                                    }
                                    else {

                                        Phase.update(parameters, {
                                            returning: true,
                                            where: { id: phaseDbId }
                                        })
                                            .then(function (result) {
                                                res.send(200)
                                                return
                                            })
                                            .catch(async err => {
                                                res.send(new errors.InternalServerError('Something went wrong. Please create a support ticket.'))
                                                return
                                            })
                                    }
                                })
                            }
                            else {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400009)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }
                        }
                    })
                }
            })
        }
    }
}
