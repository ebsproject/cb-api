/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

//Import dependencies
let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')


module.exports = {
    // Endpoint for retrieving all items
    // GET /v3/items
    get: async function (req, res, next) {
        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''

        // Build query
        let itemsQuery = `
            SELECT 
                item.id AS "itemDbId",
                item.abbrev,
                item.name,
                item.type,
                item.display_name AS "displayName",
                item.description,
                item.remarks,
                item.item_url AS "itemUrl",
                item.subject_entity_id AS "subjectEntityDbId",
                item.process_type AS "processType",
                item.item_status AS "itemStatus",
                item.item_class AS "itemClass",
                item.is_active AS "isActive",
                item.item_icon AS "itemIcon",
                item.item_group AS "itemGroup",
                item.item_usage AS "itemUsage",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                item.creation_timestamp AS "creationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier",
                item.modification_timestamp AS "modificationTimestamp"
            FROM
                master.item item
            LEFT JOIN
                tenant.person creator ON item.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON item.modifier_id = modifier.id
            WHERE
                item.is_void = FALSE
            ORDER BY
                item.id
        `
        // Get filter condition/s for abbrev
        if (params.abbrev !== undefined){
            let parameters = {
                abbrev: params.abbrev
            }
            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')){
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Get filter condition/s for sort
        if (sort != null){
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')){
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the final query
        itemFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            itemsQuery,
            conditionString,
            orderString
        )
        
        // Retrieve item records from database
        let items = await sequelize
            .query(itemFinalSqlQuery,{
                type: sequelize.QueryTypes.SELECT, 
                replacements: {
                limit: limit,
                offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await items == undefined || await items.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get item count
            itemCountFinalSqlQuery = await processQueryHelper
                .getCountFinalSqlQuery(
                    itemsQuery,
                    conditionString,
                    orderString
                )

            itemCount = await sequelize
                .query(itemCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = itemCount[0].count
        }
        
        res.send(200, {
            rows: items,
            count: 0
        })
        return
    }
}