/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        let addedOrderString = `ORDER BY program.id`
        parameters['distinctOn'] = ''

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            // Get filter condition
            conditionString = await processQueryHelper
                .getFilter(req.body, excludedParametersArray)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn

                addedDistinctString = await processQueryHelper
                  .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}"`

                addedOrderString = `
                  ORDER BY
                    ${parameters['distinctOn']}
                `
            }
        }

        // Build base retrieval query
        let programsQuery = null

        // Check if client specified values for fields
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                  .getFieldValuesString(parameters['fields'])
                
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                programsQuery = knex.select(selectString)
            } else {
                programsQuery = knex.column(parameters['fields'].split('|'))
            }

            programsQuery += `
                FROM 
                    tenant.program program
                LEFT JOIN
                    tenant.person creator ON program.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON program.modifier_id = modifier.id
                WHERE 
                    program.is_void = FALSE
                ${addedOrderString}
            `
        } else {
            programsQuery = `
                SELECT
                    ${addedDistinctString}
                    program.id AS "programDbId",
                    program.program_code AS "programCode",
                    program.program_name AS "programName",
                    program.description,
                    program.program_type AS "programType",
                    program.program_status AS "programStatus",
                    program.crop_program_id AS "cropProgramDbId",
                    program.creation_timestamp AS "creationTimestamp",
                    creator.person_name AS creator,
                    creator.id AS "creatorDbId",
                    program.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier
                FROM 
                    tenant.program program
                LEFT JOIN
                    tenant.person creator ON program.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON program.modifier_id = modifier.id
                WHERE 
                    program.is_void = FALSE
                ${addedOrderString}
            `
        }

        // Parse sort parameters
        if (sort != undefined) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate final SQL query
        let programsFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                programsQuery,
                conditionString,
                orderString
            )
      
        // Retrieve program records
        let programs = await sequelize.query(programsFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch (async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await programs == undefined || await programs.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let programsCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                programsQuery,
                conditionString,
                orderString
            )

        let programCount = await sequelize.query(programsCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch (async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = programCount[0].count

        res.send(200, {
            rows: programs,
            count: count
        })
        return
    }
}