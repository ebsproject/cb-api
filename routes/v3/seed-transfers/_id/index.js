/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let {sequelize} = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator')
let forwarded = require('forwarded-for')
let validator = require('validator')
let format = require('pg-format')

module.exports = {
    /**
     * Retrieve a specific seed transfer record by id
     * @param {*} req Request parameters
     * @param {*} res Response
     */
     get: async function (req, res, next) {

        if(req.params.id == undefined || req.params.id == ''){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Set defaults
        let seedTransferDbId = req.params.id

        // check format if id
        if(!validator.isInt(seedTransferDbId)){
            let errMsg = "Invalid format, seed transfer record ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // set query
        let seedTransferQuery = `
            SELECT
                st.id AS "seedTransferDbId",
                st.sender_program_id AS "senderProgramDbId",
                st.sender_id AS "senderDbId",
                (
                    SELECT
                        person.person_name AS "sender"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = st.sender_id
                ),
                st.send_timestamp AS "sendTimestamp",
                st.receiver_program_id AS "receiverProgramDbId",
                st.receiver_id AS "receiverDbId",
                (
                    SELECT
                        person.person_name AS "receiver"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = st.receiver_id
                ),
                st.receive_timestamp AS "receiveTimestamp",
                st.source_list_id AS "sourceListDbId",
                st.staging_list_id AS "stagingListDbId",
                st.destination_list_id AS "destinationListDbId",
                st.seed_transfer_status AS "seedTransferStatus",
                st.package_quantity AS "packageQuantity",
                st.package_unit AS "packageUnit",
                st.is_send_whole_package AS "isSendWholePackage",
                st.error_log AS "errorLog",
                st.remarks AS "remarks",
                st.creator_id AS "creatorDbId",
                (
                    SELECT
                        person.person_name AS "creator"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = st.creator_id
                ),
                st.creation_timestamp AS "creatorTimestamp",
                st.modifier_id AS "modifierDbId",
                (
                    SELECT
                        person.person_name AS "modifier"
                    FROM
                        tenant.person person
                    WHERE
                        person.is_void = FALSE AND
                        person.id = st.modifier_id
                ),
                st.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.seed_transfer st
            WHERE
                st.is_void = FALSE AND
                st.id = ${seedTransferDbId}
        `

        try{
            let seedTransfer = await sequelize.query(seedTransferQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            if(await seedTransfer == undefined || await seedTransfer.length < 1){
                let errMsg = "The seed transfer record you requested does not exist."
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            res.send(200, {
                rows: seedTransfer,
            })
            return
        }
        catch(error){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Update seed transfer records
     * PUT /v3/seed-transfers/:id
     *
     * @param {*} req Request parameters
     * @param {*} res Response
     */
     put: async function (req, res, next) {
        let seedTransferDbId
        let senderProgramDbId
        let senderDbId
        let receiverProgramDbId
        let receiverDbId
        let sourceListDbId
        let stagingListDbId
        let destinationListDbId
        let seedTransferStatus
        let packageQuantity
        let packageUnit
        let isSendWholePackage
        let errorLog
        let remarks

        let allowedSeedTransferStatus = [
            'draft', 'creation in progress', 'creation failed', 'created',
            'sending in progress', 'sending failed', 'sent',
            'receipt in progress', 'receipt failed', 'received',
            'declined'
        ]

        // Get user ID
        let userDbId = await tokenHelper.getUserId(req)

        if(userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve seed transfer ID
        seedTransferDbId = req.params.id
        // Check if seed transfer ID is an integer
        if(!validator.isInt(seedTransferDbId)) {
            let errMsg = 'Invalid format. Seed transfer ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if request body exists
        if(!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if seed transfer ID exists
        let validateSeedTransferQuery = `
            SELECT
                st.id AS "seedTransferDbId",
                st.sender_program_id AS "senderProgramDbId",
                st.sender_id AS "senderDbId",
                st.receiver_program_id AS "receiverProgramDbId",
                st.receiver_id AS "receiverDbId",
                st.source_list_id AS "sourceListDbId",
                st.staging_list_id AS "stagingListDbId",
                st.destination_list_id AS "destinationListDbId",
                st.seed_transfer_status AS "seedTransferStatus",
                st.package_quantity AS "packageQuantity",
                st.package_unit AS "packageUnit",
                st.is_send_whole_package AS "isSendWholePackage",
                st.error_log AS "errorLog",
                st.remarks AS "remarks"
            FROM
                germplasm.seed_transfer st
            WHERE
                st.is_void = FALSE AND
                st.id = ${seedTransferDbId}
        `

        // Retrieve seed transfer from the database
        let seedTransfers = await sequelize.query(validateSeedTransferQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        // Return error if resource is not found
        if(await seedTransfers == undefined || await seedTransfers.length == 0) {
            let errMsg = 'Invalid request. Ensure that the seed transfer record exists. '
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values
        let seedTransfer = seedTransfers[0]
        let stSenderProgramDbId = seedTransfer.senderProgramDbId
        let stSenderDbId = seedTransfer.senderDbId
        let stReceiverProgramDbId = seedTransfer.receiverProgramDbId
        let stReceiverDbId = seedTransfer.receiverDbId
        let stSourceListDbId = seedTransfer.sourceListDbId
        let stStagingListDbId = seedTransfer.stagingListDbId
        let stDestinationListDbId = seedTransfer.destinationListDbId
        let stSeedTransferStatus = seedTransfer.seedTransferStatus
        let stPackageQuantity = seedTransfer.packageQuantity
        let stPackageUnit = seedTransfer.packageUnit
        let stIsSendWholePackage = seedTransfer.isSendWholePackage
        let stErrorLog = seedTransfer.errorLog
        let stRemarks = seedTransfer.remarks

        // Set the URL for response
        let isSecure = forwarded(req, req.headers).secure
        let seedTransfersUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/seed-transfers"

        let data = req.body
        let transaction
        let resultArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            let setQuery = ``
            /**
             * Validation of the values
             * */
            let validateQuery = ``
            let validateCount = 0

            if(data.senderProgramDbId != undefined) {
                senderProgramDbId = data.senderProgramDbId

                // Check if senderProgramDbId data type is valid
                if(!validator.isInt(senderProgramDbId)) {
                    let errMsg = "Invalid format, program ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        tenant.program program
                    WHERE
                        program.id = ${senderProgramDbId}
                        AND program.is_void = FALSE
                )
                `
                validateCount += 1

                if(senderProgramDbId != stSenderProgramDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        sender_program_id = $$${senderProgramDbId}$$`
                }
            }

            if(data.senderDbId != undefined) {
                senderDbId = data.senderDbId

                // Check if senderDbId data type is valid
                if(!validator.isInt(senderDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400077)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        tenant.person person
                    WHERE
                        person.id = ${senderDbId}
                        AND person.is_void = FALSE
                )
                `
                validateCount += 1

                if(senderDbId != stSenderDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        sender_id = $$${senderDbId}$$,
                        send_timestamp = NOW()`
                }
            }

            if(data.receiverProgramDbId != undefined) {
                receiverProgramDbId = data.receiverProgramDbId

                // Check if receiverProgramDbId data type is valid
                if(!validator.isInt(receiverProgramDbId)) {
                    let errMsg = "Invalid format, program ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        tenant.program program
                    WHERE
                        program.id = ${receiverProgramDbId}
                        AND program.is_void = FALSE
                )
                `
                validateCount += 1

                if(receiverProgramDbId != stReceiverProgramDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        receiver_program_id = $$${receiverProgramDbId}$$`
                }
            }

            if(data.receiverDbId != undefined) {
                receiverDbId = data.receiverDbId

                // Check if receiverDbId data type is valid
                if(!validator.isInt(receiverDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400077)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        tenant.person person
                    WHERE
                        person.id = ${receiverDbId}
                        AND person.is_void = FALSE
                )
                `
                validateCount += 1

                if(receiverDbId != stReceiverDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        receiver_id = $$${receiverDbId}$$,
                        receive_timestamp = NOW()`
                }
            }

            if(data.sourceListDbId != undefined) {
                sourceListDbId = data.sourceListDbId

                // Check if sourceListDbId data type is valid
                if(!validator.isInt(sourceListDbId)) {
                    let errMsg = "Invalid format, source list ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        platform.list list
                    WHERE
                        list.id = ${sourceListDbId}
                        AND list.is_void = FALSE
                )
                `
                validateCount += 1

                if(sourceListDbId != stSourceListDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        source_list_id = $$${sourceListDbId}$$`
                }
            }

            if(data.stagingListDbId != undefined) {
                stagingListDbId = data.stagingListDbId

                // Check if stagingListDbId data type is valid
                if(!validator.isInt(stagingListDbId)) {
                    let errMsg = "Invalid format, staging list ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        platform.list list
                    WHERE
                        list.id = ${stagingListDbId}
                        AND list.is_void = FALSE
                )
                `
                validateCount += 1

                if(stagingListDbId != stStagingListDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        staging_list_id = $$${stagingListDbId}$$`
                }
            }

            if(data.destinationListDbId != undefined) {
                destinationListDbId = data.destinationListDbId

                // Check if destinationListDbId data type is valid
                if(!validator.isInt(destinationListDbId)) {
                    let errMsg = "Invalid format, destination list ID must be an integer"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                (
                    SELECT
                        count(*)
                    FROM
                        platform.list list
                    WHERE
                        list.id = ${destinationListDbId}
                        AND list.is_void = FALSE
                )
                `
                validateCount += 1

                if(destinationListDbId != stDestinationListDbId){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        destination_list_id = $$${destinationListDbId}$$`
                }
            }

            if(data.seedTransferStatus != undefined) {
                seedTransferStatus = data.seedTransferStatus

                // Check if seedTransferStatus provided is included in the allowed values
                if(!allowedSeedTransferStatus.includes(seedTransferStatus)) {
                    let errMsg = `Invalid input, seedTransferStatus should be: ` + allowedSeedTransferStatus.join(', ')
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(seedTransferStatus != stSeedTransferStatus){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        seed_transfer_status = $$${seedTransferStatus}$$`
                }
            }

            if(data.packageQuantity != undefined) {
                packageQuantity = data.packageQuantity

                // Check if packageQuantity data type is valid
                if(!validator.isFloat(packageQuantity)) {
                    let errMsg = "Invalid format, package quantity must be a float."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(packageQuantity != stPackageQuantity){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_quantity = $$${packageQuantity}$$`
                }
            }

            if(data.packageUnit != undefined) {
                packageUnit = data.packageUnit

                // Check if packageUnit is not empty
                if(packageUnit == '') {
                    let errMsg = "Invalid input, package unit must not be empty"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(packageUnit != stPackageUnit){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_unit = $$${packageUnit}$$`
                }
            }

            if(data.isSendWholePackage != undefined) {
                isSendWholePackage = data.isSendWholePackage

                // Check if isSendWholePackage is not empty
                if(!validator.isBoolean(isSendWholePackage)) {
                    let errMsg = "Invalid input, isSendWholePackage must be boolean"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if(isSendWholePackage != stIsSendWholePackage){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        is_send_whole_package = $$${isSendWholePackage}$$`
                }
            }

            if(data.errorLog != undefined) {
                errorLog = data.errorLog
                if(errorLog != stErrorLog){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        error_log = $$${errorLog}$$`
                }
            }

            if(data.remarks != undefined) {
                remarks = data.remarks
                if(remarks != stRemarks){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        remarks = $$${remarks}$$`
                }
            }

            // Perform validation
            if(validateCount > 0) {
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if(inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if(setQuery.length > 0) setQuery += `,`

            // update the seed transfer record
            let updateSeedTransferQuery = `
                UPDATE
                    germplasm.seed_transfer seed_transfer
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = $$${userDbId}$$
                WHERE
                    seed_transfer.id = $$${seedTransferDbId}$$
            `

            await sequelize.query(updateSeedTransferQuery, {
                type: sequelize.QueryTypes.UPDATE
            })
            // Return the transaction info
            let array = {
                seedTransferDbId: seedTransferDbId,
                recordCount: 1,
                href: seedTransfersUrlString + '/' + seedTransferDbId
            }
            resultArray.push(array)

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        }
        catch (e) {
            // Rollback transaction (if still open)
            if(transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Delete seed transfer records
     * DELETE /v3/seed-transfers/:id
     *
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    delete: async function(req, res, next) {
        let seedTransferDbId = req.params.id

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if(personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if seed transfer ID is an integer
        if(!validator.isInt(seedTransferDbId)) {
            let errMsg = 'Invalid format. Seed transfer ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if seed transfer ID exists
        let seedTransferQuery = `
            SELECT
                st.id AS "seedTransferDbId"
            FROM
                germplasm.seed_transfer st
            WHERE
                st.is_void = FALSE AND
                st.id = ${seedTransferDbId}
        `

        let seedTransferRecord = await sequelize.query(seedTransferQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If seed transfer retrieval failed, end execution
        if(seedTransferRecord === undefined) {
            return
        }

        // If no record was retrieved, return NotFoundError
        if(seedTransferRecord !== null && seedTransferRecord.length < 1) {
            let errMsg = 'The seed transfer record you have requested does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let deleteSeedTransferQuery = format(`
                UPDATE
                    germplasm.seed_transfer
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${seedTransferDbId}
            `)

            await sequelize.query(deleteSeedTransferQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { seedTransferDbId: seedTransferDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if(transaction !== undefined
                && transaction !== null
                && transaction.finished !== 'commit'
            ) {
                transaction.rollback()
            }
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}