/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
const { sequelize } = require("../../../config/sequelize");
const errors = require("restify-errors");
const errorBuilder = require("../../../helpers/error-builder");
const tokenHelper = require("../../../helpers/auth/token");
const forwarded = require("forwarded-for");
const validator = require("validator");
let format = require('pg-format')

module.exports = {

    /**
     * Create seed transfer records
     * POST /v3/seed-transfers
     *
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    post: async function(req, res, next) {
        let senderProgramDbId       // tenant.program
        let receiverProgramDbId     // tenant.program
        let sourceListDbId          // platform.list
        let stagingListDbId         // platform.list
        let destinationListDbId     // platform.list
        let seedTransferStatus      // master.scale_value
        let packageQuantity
        let packageUnit             // master.scale_value
        let isSendWholePackage
        let remarks
        let notes
        
        // Validate user id
        let userDbId = await tokenHelper.getUserId(req)
        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let seedTransfersUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/seed-transfers'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        records = data.records
        // Check if the input data is empty
        if (records === undefined || records.length === 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let seedTransfersArray = []
            for (let record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Check if the required columns are in the input
                if (
                    record.seedTransferStatus === undefined
                ) {
                    let errMsg = 'Required parameter is missing. ' +
                        'Ensure that seedTransferStatus field is not empty.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Validate senderProgramDbId, when provided
                if (record.senderProgramDbId !== undefined) {
                    senderProgramDbId = record.senderProgramDbId

                    if (!validator.isInt(senderProgramDbId)) {
                        let errMsg = "Invalid format, sender program ID must be an integer."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery !== '') ? ' + ' : ''
                    validateQuery += `
                    (
                        --- Check if program is existing return 1
                        SELECT
                            count(1)
                        FROM
                            tenant.program program
                        WHERE
                            program.id = ${senderProgramDbId}
                            AND program.is_void = FALSE
                    )
                    `
                    validateCount += 1
                }

                // Validate receiverProgramDbId, when provided
                if (record.receiverProgramDbId !== undefined) {
                    receiverProgramDbId = record.receiverProgramDbId

                    if (!validator.isInt(receiverProgramDbId)) {
                        let errMsg = "Invalid format, receiver program ID must be an integer."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery !== '') ? ' + ' : ''
                    validateQuery += `
                    (
                        --- Check if program is existing return 1
                        SELECT
                            count(1)
                        FROM
                            tenant.program program
                        WHERE
                            program.id = ${receiverProgramDbId}
                            AND program.is_void = FALSE
                    )
                    `
                    validateCount += 1
                }

                // Validate sourceListDbId, when provided
                if (record.sourceListDbId !== undefined) {
                    sourceListDbId = record.sourceListDbId

                    if (!validator.isInt(sourceListDbId)) {
                        let errMsg = "Invalid format, source list ID must be an integer."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery !== '') ? ' + ' : ''
                    validateQuery += `
                    (
                        --- Check if list is existing return 1
                        SELECT
                            count(1)
                        FROM
                            platform.list list
                        WHERE
                            list.id = ${sourceListDbId}
                            AND list.is_void = FALSE
                    )
                    `
                    validateCount += 1
                }

                // Validate stagingListDbId, when provided
                if (record.stagingListDbId !== undefined) {
                    stagingListDbId = record.stagingListDbId

                    if (!validator.isInt(stagingListDbId)) {
                        let errMsg = "Invalid format, staging list ID must be an integer."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery !== '') ? ' + ' : ''
                    validateQuery += `
                    (
                        --- Check if list is existing return 1
                        SELECT
                            count(1)
                        FROM
                            platform.list list
                        WHERE
                            list.id = ${stagingListDbId}
                            AND list.is_void = FALSE
                    )
                    `
                    validateCount += 1
                }

                // Validate destinationListDbId, when provided
                if (record.destinationListDbId !== undefined) {
                    destinationListDbId = record.destinationListDbId

                    if (!validator.isInt(destinationListDbId)) {
                        let errMsg = "Invalid format, destination list ID must be an integer."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    validateQuery += (validateQuery !== '') ? ' + ' : ''
                    validateQuery += `
                    (
                        --- Check if list is existing return 1
                        SELECT
                            count(1)
                        FROM
                            platform.list list
                        WHERE
                            list.id = ${destinationListDbId}
                            AND list.is_void = FALSE
                    )
                    `
                    validateCount += 1
                }

                // Validate seedTransferStatus
                seedTransferStatus = record.seedTransferStatus.trim()
                validateQuery += (validateQuery !== '') ? ' + ' : ''
                validateQuery += `
                (
                    --- Check if seed transfer status is existing return 1
                    SELECT 
                        count(1)
                    FROM 
                        master.scale_value scaleValue
                    LEFT JOIN
                        master.variable variable ON scaleValue.scale_id = variable.scale_id
                    WHERE
                        variable.abbrev LIKE 'SEED_TRANSFER_STATUS' AND
                        scaleValue.value ILIKE $$${seedTransferStatus}$$
                )
                `
                validateCount += 1

                // Validate packageQuantity, when provided
                if (record.packageQuantity !== undefined) {
                    packageQuantity = record.packageQuantity

                    if (!validator.isFloat(packageQuantity)) {
                        let errMsg = "Invalid format, package quantity must be a float."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (parseFloat(packageQuantity) < 0) {
                        let errMsg = "Invalid number, negative values are not allowed for the package quantity."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // Validate packageUnit, when provided
                if (record.packageUnit !== undefined) {
                    packageUnit = record.packageUnit

                    validateQuery += (validateQuery !== '') ? ' + ' : ''
                    validateQuery += `
                    (
                        --- Check if package unit is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                            LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'UNIT' AND
                            scaleValue.value ILIKE $$${packageUnit}$$
                    )
                    `
                    validateCount += 1
                }

                // Validate isSendWholePackage, when provided
                if (record.isSendWholePackage !== undefined) {
                    isSendWholePackage = record.isSendWholePackage

                    if (!validator.isBoolean(isSendWholePackage)) {
                        let errMsg = "Invalid format, isSendWholePackage must be boolean."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // Get remarks value, when provided
                if (record.remarks !== undefined) remarks = record.remarks

                // Get notes value, when provided
                if (record.notes !== undefined) notes = record.notes

                // Perform validation
                if (validateCount > 0) {
                    let checkInputQuery = `
                        SELECT ${validateQuery} AS count
                    `
                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (inputCount[0]['count'] != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // Add data to insertion array
                let tempArray = [
                    senderProgramDbId,
                    receiverProgramDbId,
                    sourceListDbId,
                    stagingListDbId,
                    destinationListDbId,
                    seedTransferStatus,
                    packageQuantity,
                    packageUnit,
                    isSendWholePackage,
                    userDbId,
                    remarks,
                    notes
                ]

                seedTransfersArray.push(tempArray)
            }

            // Create seed transfer
            let seedTransferQuery = format(`
                INSERT INTO germplasm.seed_transfer
                    (sender_program_id, 
                     receiver_program_id, 
                     source_list_id, 
                     staging_list_id, 
                     destination_list_id, 
                     seed_transfer_status,
                     package_quantity,
                     package_unit,
                     is_send_whole_package,
                     creator_id,
                     remarks,
                     notes)
                VALUES %L
                RETURNING id`, seedTransfersArray)

            let seedTransfers = await sequelize.query(seedTransferQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            }).catch(async err => {
                return
            })

            // Check if insertion query returned results
            if (seedTransfers === undefined || seedTransfers[0].length === 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            let resultArray = []
            for (let seedTransfer of seedTransfers[0]) {
                let seedTransferDbId = seedTransfer.id

                let tempArray = {
                    seedTransferDbId: seedTransferDbId,
                    recordCount: 1,
                    href: seedTransfersUrlString + '/' + seedTransferDbId
                }
                resultArray.push(tempArray)
            }
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (e) {
            // Rollback transaction (if still open)
            if (transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}