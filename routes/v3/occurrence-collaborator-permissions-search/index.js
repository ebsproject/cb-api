/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let tokenHelper = require('../../../helpers/auth/token.js')
let errorBuilder = require('../../../helpers/error-builder')
let userValidator = require('../../../helpers/person/validator.js')

module.exports = {
    post: async function (req, res, next){
        // set default
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let fields = null
        let count = 0
        let orderString = ''
        let conditionString = ''
        let permissionConditionString = ''
        let occurrenceDbIdsConditionString = ``

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req);

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if user is admin or not
        let isAdmin = await userValidator.isAdmin(userId)

        // if not admin, retrieve only occurrences where the user is the experiment steward or creator
        if(!isAdmin){
            permissionConditionString = `
                AND (occurrence.creator_id = ${userId} OR experiment.steward_id = ${userId})
            `
        }

        // Default values
        if (req.body != undefined) {
            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = [
            ]

            if (req.body.fields != null) {
                fields = req.body.fields
            }

            if (req.body.occurrenceDbId != null) {
                occurrenceDbIds = req.body.occurrenceDbId
                occurrenceDbIdsConditionString = ` AND occurrence.id in (` + occurrenceDbIds.replaceAll('|',',') + `)`
            }

            // Get filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body, 
                excludedParametersArray
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

        }

        // Start building the query
        let occurrencesQuery = null

        let fromQuery = `
            FROM
                x,
                experiment.experiment
                LEFT JOIN 
                    tenant.program program ON program.id = experiment.program_id
                LEFT JOIN
                    tenant.person experiment_steward ON experiment_steward.id = experiment.steward_id,
                experiment.occurrence
                LEFT JOIN 
                    place.geospatial_object AS site ON site.id = occurrence.site_id AND site.is_void = FALSE
                LEFT JOIN 
                    place.geospatial_object AS country ON country.id = site.root_geospatial_object_id AND country.is_void = FALSE
                LEFT JOIN
                    tenant.person creator ON creator.id = occurrence.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = occurrence.modifier_id
                LEFT JOIN
                    experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id AND log.is_void = FALSE
                LEFT JOIN 
                    experiment.location ON log.location_id = location.id AND location.is_void = FALSE
                LEFT JOIN
                    tenant.person location_steward ON location_steward.id = location.steward_id
            WHERE
                experiment.id = occurrence.experiment_id
                AND occurrence.id = x."occurrenceDbId"
                AND occurrence.is_void = FALSE
                AND experiment.is_void = FALSE
                ${permissionConditionString}
            GROUP BY
                occurrence.id, 
                experiment.id, 
                experiment_steward.person_name,
                program.id, 
                site.geospatial_object_code,
                site.geospatial_object_name,
                site.root_geospatial_object_id, 
                country.geospatial_object_code, 
                country.geospatial_object_name,
                location.id,
                location_steward.person_name,
                creator.person_name, 
                modifier.id
            ORDER BY
                occurrence.id
        `

        occurrencesQuery = `
            with x AS (
                with y AS (
                    with t AS (
                        SELECT
                            occurrence.id as "occurrenceDbId",
                            occurrence.creator_id AS "creatorDbId",
                            experiment.program_id as "programDbId",
                            (
                                CASE
                                    WHEN (occurrence.access_data = '{}' OR occurrence.access_data is null)
                                    THEN '{"": {"0": {}}}' 
                                    ELSE occurrence.access_data 
                                    END
                            ) AS "accessData"
                        FROM
                            experiment.occurrence,
                            experiment.experiment
                        WHERE
                            occurrence.is_void = FALSE
                            AND experiment.id = occurrence.experiment_id
                            AND experiment.is_void = FALSE
                            ${occurrenceDbIdsConditionString}
                    ) 
                    SELECT 
                        t.*,
                        parent.key AS "entity",
                        children.key::integer AS "entityDbId",
                        s.*
                    FROM
                        t,
                        jsonb_each(t."accessData") parent,
                        jsonb_each(parent.value) children,
                        jsonb_to_record(children.value) AS s("addedOn" timestamp, "permission" text)
                )
                SELECT
                    y.*,
                    (
                        CASE
                            WHEN (y."entity" = 'program' AND y."programDbId" = y."entityDbId") OR (y."entity" = 'person' AND y."creatorDbId" = y."entityDbId")
                            THEN 'null'
                            WHEN y."entity" = 'program'
                            THEN program.program_code
                            WHEN y."entity" = ''
                            THEN 'null'
                            ELSE person.person_name 
                            END
                    ) AS collaborator
                FROM
                    y
                    LEFT JOIN
                        tenant.person person
                    ON
                        person.id = y."entityDbId"
                        AND person.is_void = FALSE
                    LEFT JOIN
                        tenant.program program
                    ON
                        program.id = y."entityDbId"
                        AND program.is_void = FALSE
            )
            SELECT 
                experiment.id AS "experimentDbId",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_code AS "experimentCode",
                experiment.steward_id AS "experimentStewardDbId",
                experiment_steward.person_name AS "experimentSteward",
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_name AS "occurrenceName",
                occurrence.occurrence_code AS "occurrenceCode",
                occurrence.occurrence_status AS "occurrenceStatus",                
                program.id AS "programDbId",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                occurrence.site_id AS "siteDbId",
                site.geospatial_object_code AS "siteCode",
                site.geospatial_object_name AS "siteName",
                site.root_geospatial_object_id AS "countryDbId",
                country.geospatial_object_code AS "countryCode",
                country.geospatial_object_name AS "countryName",
                location.id AS "locationDbId",
                location.location_name AS "locationName",
                location.location_code AS "locationCode",
                location.steward_id AS "locationStewardDbId",
                location_steward.person_name AS "locationSteward",
                occurrence.creator_id AS "creatorDbId",
                creator.person_name AS "creator",
                occurrence.creation_timestamp AS "creationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier",
                occurrence.modification_timestamp AS "modificationTimestamp",
                json_object_agg(x.collaborator, x.permission) AS "collaboratorPermissions"
        ` + fromQuery

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString && orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Generate final SQL query
        let occurrencesFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                occurrencesQuery,
                conditionString,
                orderString,
        )

        let occurrences = await sequelize
            .query(occurrencesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                return undefined
            })

            if (occurrences === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))

            return
        }

        let occurrencesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                occurrencesQuery,
                conditionString,
            )

        let occurrencesCount = await sequelize
            .query(occurrencesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                return undefined
            })

        if (occurrencesCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        count = occurrencesCount[0].count

        res.send(200, {
            rows: occurrences,
            count: count
        })

        return
    }
}