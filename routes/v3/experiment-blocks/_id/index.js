/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let forwarded = require('forwarded-for')
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let errorBuilder = require('../../../../helpers/error-builder')
let userValidator = require('../../../../helpers/person/validator.js')
let format = require('pg-format')

module.exports = {

    // GET /v3/experiment-blocks/{id}
    get: async function (req, res, next) {

        let experimentBlockDbId = req.params.id
        let count = 0

        if (!validator.isInt(experimentBlockDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400090)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let experimentBlocksQuery = `
        SELECT 
            experimentBlock.id AS "experimentBlockDbId",
            experiment.id AS "experimentDbId",
            experimentBlock.parent_id AS "parentDbId",
            experimentBlock.order_number AS "orderNumber",
            experimentBlock.code,
            experimentBlock.name,
            experimentBlock.block_type AS "blockType",
            experimentBlock.no_of_blocks AS "noOfBlocks",
            experimentBlock.no_of_rows_in_block AS "noOfRowsInBlock",
            experimentBlock.no_of_cols_in_block AS "noOfColsInBlock",
            experimentBlock.no_of_reps_in_block AS "noOfRepsInBlock",
            experimentBlock.plot_numbering_order AS "plotNumberingOrder",
            experimentBlock.starting_corner AS "startingCorner",
            experimentBlock.entry_list_ids AS "entryListIds",
            experimentBlock.remarks AS "remarks"
        FROM
            operational.experiment_block experimentBlock
        LEFT JOIN
            operational.experiment experiment ON experimentBlock.experiment_id = experiment.id
        WHERE 
            experimentBlock.id = :experimentBlockDbId AND
            experimentBlock.is_void = FALSE
        `
        // Retrieve experiment blocks from the database
        let experimentBlocks = await sequelize.query(experimentBlocksQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                experimentBlockDbId: experimentBlockDbId
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Return error if resource is not found
        if (experimentBlocks === undefined || experimentBlocks.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404030)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        res.send(200, {
            rows: experimentBlocks
        })
        return
    },

    // Endpoint for updating an existing experiment block record
    // PUT /v3/experiment-blocks/:id
    put: async function (req, res, next) {
        // Retrieve the experiment block ID
        let experimentBlockDbId = req.params.id

        if(!validator.isInt(experimentBlockDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400090)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Set defaults
        let experimentBlockName = null
        let orderNumber = null
        let blockType = null
        let noOfBlocks = null
        let noOfRanges = null
        let noOfCols = null
        let noOfReps = null
        let plotNumberingOrder = null
        let startingCorner = null
        let entryListIdList = null
        let layoutOrderData = null
        let notes = null
        let isActive = null;
        let remarks = null

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if experiment block record is existing
        // Build query
        let experimentBlockQuery = `
            SELECT
                eb.experiment_block_name AS "experimentBlockName",
                eb.order_number AS "orderNumber",
                eb.block_type AS "blockType",
                eb.no_of_blocks AS "noOfBlocks",
                eb.no_of_ranges AS "noOfRanges",
                eb.no_of_cols AS "noOfCols",
                eb.no_of_reps AS "noOfReps",
                eb.plot_numbering_order AS "plotNumberingOrder",
                eb.starting_corner AS "startingCorner",
                eb.entry_list_id_list AS "entryListIdList",
                eb.layout_order_data AS "layoutOrderData",
                eb.notes,
                eb.remarks,   
                experiment.id AS "experimentDbId",
                creator.id AS "creatorDbId"
            FROM
                experiment.experiment_block eb
            LEFT JOIN
                experiment.experiment experiment ON experiment.id = eb.experiment_id
            LEFT JOIN
                tenant.person creator ON creator.id = eb.creator_id
            WHERE
                eb.is_void = FALSE AND
                eb.id = ${experimentBlockDbId}
        `

        // Retrieve experiment block from the database
        let experimentBlock = await sequelize.query(experimentBlockQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
        
        if (experimentBlock === undefined || experimentBlock.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404030)
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        
        // Get values from database, record = the current value in the database
        let recordExperimentBlockName = experimentBlock[0].experimentBlockName
        let recordOrderNumber = experimentBlock[0].orderNumber
        let recordBlockType = experimentBlock[0].blockType
        let recordNoOfBlocks = experimentBlock[0].noOfBlocks
        let recordNoOfRanges = experimentBlock[0].noOfRanges
        let recordNoOfCols = experimentBlock[0].noOfCols
        let recordNoOfReps = experimentBlock[0].noOfReps
        let recordPlotNumberingOrder = experimentBlock[0].plotNumberingOrder
        let recordStartingCorner = experimentBlock[0].startingCorner
        let recordEntryListIdList = experimentBlock[0].entryListIdList
        let recordLayoutOrderData = experimentBlock[0].layoutOrderData
        let recordExperimentDbId = experimentBlock[0].experimentDbId
        let recordCreatorDbId = experimentBlock[0].creatorDbId
        let recordNotes = experimentBlock[0].notes
        let recordIsActive = experimentBlock[0].isActive
        let recordRemarks = experimentBlock[0].remarks

        // Check if user is an admin or an owner of the cross
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${recordExperimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }
        
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let experimentBlockUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/experiment-blocks/"
            + experimentBlockDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body

        // Start transaction
        let transaction
        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/
            if (data.experimentBlockName !== undefined) {
                experimentBlockName = data.experimentBlockName

                if (experimentBlockName != recordExperimentBlockName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        experiment_block_name = $$${experimentBlockName}$$
                    `
                }
            }
            
            if (data.orderNumber !== undefined) {
                orderNumber = data.orderNumber

                if (!validator.isInt(orderNumber)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400093)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (orderNumber != recordOrderNumber) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        order_number = $$${orderNumber}$$
                    `
                }
            }
            
            if (data.blockType !== undefined) {
                blockType = data.blockType

                if (blockType != recordBlockType) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if block type is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                                LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'BLOCK_TYPE' AND
                                scaleValue.value ILIKE $$${blockType}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        block_type = $$${blockType}$$
                    `
                }
            }
            
            if (data.noOfBlocks !== undefined) {
                noOfBlocks = data.noOfBlocks

                if (!validator.isInt(noOfBlocks)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400094)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (noOfBlocks != recordNoOfBlocks) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        no_of_blocks = $$${noOfBlocks}$$
                    `
                }
            }
            
            if (data.noOfRanges !== undefined) {
                noOfRanges = data.noOfRanges

                if (!validator.isInt(noOfRanges)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400230)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (noOfRanges != recordNoOfRanges) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        no_of_ranges = $$${noOfRanges}$$
                    `
                }
            }
            
            if (data.noOfCols !== undefined) {
                noOfCols = data.noOfCols

                if (!validator.isInt(noOfCols)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400096)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (noOfCols != recordNoOfCols) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        no_of_cols = $$${noOfCols}$$
                    `
                }
            }
            
            if (data.noOfReps !== undefined) {
                noOfReps = data.noOfReps

                if (!validator.isInt(noOfReps)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400097)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Check if user input is same with the current value in the database
                if (noOfReps != recordNoOfReps) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        no_of_reps = $$${noOfReps}$$
                    `
                }
            }
            
            if (data.plotNumberingOrder !== undefined) {
                plotNumberingOrder = data.plotNumberingOrder

                if (plotNumberingOrder != recordPlotNumberingOrder) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if plot numbering order is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                                LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'PLOT_NUMBERING_ORDER' AND
                                scaleValue.value ILIKE $$${plotNumberingOrder}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        plot_numbering_order = $$${plotNumberingOrder}$$
                    `
                }
            }
            
            if (data.startingCorner !== undefined) {
                startingCorner = data.startingCorner

                if (startingCorner != recordStartingCorner) {
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if starting corner is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                                LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'STARTING_CORNER' AND
                                scaleValue.value ILIKE $$${startingCorner}$$
                        )
                    `
                    validateCount += 1

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        starting_corner = $$${startingCorner}$$
                    `
                }
            }
            
            if (data.entryListIdList !== undefined) {
                entryListIdList = data.entryListIdList
                
                if (entryListIdList !== "null") {
                    if (!validator.isJSON(entryListIdList)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400231)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (entryListIdList != recordEntryListIdList) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            entry_list_id_list = $$${entryListIdList}$$
                        `
                    }
                } else {
                    entryListIdList = null

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_list_id_list = NULL
                    `
                }
            }

            let appendLayout = false
            if (data.appendLayout) {
                appendLayout = data.appendLayout
            }
            
            if (data.layoutOrderData !== undefined) {
                layoutOrderData = JSON.parse(data.layoutOrderData)
                layoutOrderData = JSON.stringify(layoutOrderData)

                if (layoutOrderData != JSON.stringify(recordLayoutOrderData)) {
                    if (!validator.isJSON(layoutOrderData)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400232)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (appendLayout) {
                        let temp = JSON.parse(layoutOrderData)
                        layoutOrderData = recordLayoutOrderData.concat(temp)
                        layoutOrderData = JSON.stringify(layoutOrderData)
                    }



                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        layout_order_data = $$${layoutOrderData}$$
                    `
                }
            }

            if (data.notes !== undefined) {
                notes = data.notes

                if (notes != recordNotes) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        notes = $$${notes}$$
                    `
                }
            }

            if (data.remarks !== undefined) {
                remarks = data.remarks

                if (remarks !== recordRemarks) {
                    setQuery += (setQuery !== '') ? ',' : ''
                    setQuery += `
                        remarks = $$${remarks}$$
                    `
                }
            }

            if (data.isActive !== undefined) {
                isActive = data.isActive

                // Check if user input is same with the current value in the database
                if (isActive != recordIsActive) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        is_active = $$${isActive}$$
                    `
                }
            }

            /** Validation of input in database */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // Update the experiment block record
            let updateExperimentBlockQuery = `
                UPDATE
                    experiment.experiment_block  
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${experimentBlockDbId}
            `

            let result = await sequelize.transaction(
                async transaction => {
                    return await sequelize.query(updateExperimentBlockQuery, {
                        type: sequelize.QueryTypes.UPDATE,
                        transaction: transaction,
                        raw: true,
                    }).catch(async err => {
                        logger.logFailingQuery(endpoint, 'UPDATE', err)
                        throw new Error(err)
                    })
                }
            )

            // Return the transaction info
            let resultArray = {
                experimentBlockDbId: experimentBlockDbId,
                recordCount: 1,
                href: experimentBlockUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing experiment block record
    // DELETE /v3/experiment-blocks/:id
    delete: async function (req, res, next) {
        // Retrieve the experiment block ID
        let experimentBlockDbId = req.params.id

        if (!validator.isInt(experimentBlockDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400090)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let experimentBlockQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    experiment.id AS "experimentDbId",
                    eb.experiment_block_code AS "experimentBlockCode"
                FROM
                    experiment.experiment_block eb
                LEFT JOIN
                    tenant.person creator ON creator.id = eb.creator_id
                LEFT JOIN
                    experiment.experiment ON experiment.id = eb.experiment_id
                WHERE
                    eb.is_void = FALSE AND
                    eb.id = ${experimentBlockDbId}
            `

            let experimentBlock = await sequelize.query(experimentBlockQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (experimentBlock == undefined || experimentBlock.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404030)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = experimentBlock[0].creatorDbId
            let recordExperimentDbId = experimentBlock[0].experimentDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${recordExperimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deleteExperimentBlockQuery = format(`
                UPDATE
                    experiment.experiment_block eb
                SET
                    is_void = TRUE,
                    experiment_block_code = 'VOIDED-${recordExperimentDbId}-${recordExperimentDbId}-${experimentBlockDbId}',
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    eb.id = ${experimentBlockDbId}
            `)

            await sequelize.query(deleteExperimentBlockQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            await transaction.commit()
            res.send(200, {
                rows: { experimentBlockDbId: experimentBlockDbId }
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}