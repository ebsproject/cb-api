/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')
let validator = require('validator')

module.exports = {

    // GET /v3/experiment-blocks
    get: async function (req, res, next) {

        // Set defaults
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''

        let orderString = ''

        // Build query
        let experimentBlocksQuery = `
            SELECT 
                experimentBlock.id AS "experimentBlockDbId",
                experiment.id AS "experimentDbId",
                experimentBlock.parent_id AS "parentDbId",
                experimentBlock.order_number AS "orderNumber",
                experimentBlock.code,
                experimentBlock.name,
                experimentBlock.block_type AS "blockType",
                experimentBlock.no_of_blocks AS "noOfBlocks",
                experimentBlock.no_of_rows_in_block AS "noOfRowsInBlock",
                experimentBlock.no_of_cols_in_block AS "noOfColsInBlock",
                experimentBlock.no_of_reps_in_block AS "noOfRepsInBlock",
                experimentBlock.plot_numbering_order AS "plotNumberingOrder",
                experimentBlock.starting_corner AS "startingCorner",
                experimentBlock.entry_list_ids AS "entryListDbIds",
                experimentBlock.remarks AS "remarks"
            FROM
                operational.experiment_block experimentBlock
            LEFT JOIN
                operational.experiment experiment ON experimentBlock.experiment_id = experiment.id
            WHERE 
                experimentBlock.is_void=FALSE    
            ORDER BY
                experimentBlock.id
        `

        // Get filter condition for experimentDbId only
        if (params.experimentDbId !== undefined) {
            let parameters = []
            if (params.experimentDbId !== undefined) parameters['experimentDbId'] = params.experimentDbId

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400023)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

        }

        // Generate the final SQL Query
        experimentBlockFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            experimentBlocksQuery,
            conditionString,
            orderString
        )

        // Retreive experiment blocks from the database
        let experimentBlocks = await sequelize.query(experimentBlockFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await experimentBlocks == undefined ||
            await experimentBlocks.length < 1
        ) {
            res.send(200, {
              rows: [], 
              count: 0
            })
            return
        } else {
            // Get count
            experimentBlockFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                experimentBlocksQuery,
                conditionString,
                orderString
            )

            experimentBlockCount = await sequelize.query(experimentBlockFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = experimentBlockCount[0].count
        }

        res.send(200, {
            rows: experimentBlocks,
            count: count
        })
        return
    },
    
    // Endpoint for adding an experiment block record
    post: async function (req, res, next) {
        let resultArray = []

        // Retrieve the ID of the client via access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
        
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let experimentBlockUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/experiment-blocks'
    
        // Check for parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body
        let records = []

        try {
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let experimentBlockValuesArray = []
            let visitedArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Declare variables
                let experimentBlockCode = null // required
                let experimentBlockName = null // required
                let experimentDbId = null // required
                let parentExperimentBlockDbId = null 
                let orderNumber = null // required
                let blockType = null  //required
                let noOfBlocks = null  // required
                let noOfRanges = null  // required
                let noOfCols = null  // required
                let noOfReps = null  // required
                let plotNumberingOrder = null // required
                let startingCorner = null  // required
                let entryListIdList = null
                let layoutOrderData = null
                let isActive = false
                let notes = null
                
                // Check if the required parameters are in the body
                if (
                    record.experimentBlockCode == undefined ||
                    record.experimentBlockName == undefined ||
                    record.experimentDbId == undefined ||
                    record.orderNumber == undefined ||
                    record.blockType == undefined ||
                    record.noOfBlocks == undefined 
                ) {
                    let errMsg =
                        `Required parameters/s are missing. Ensure that the values for experimentBlockCode, experimentBlockName, experimentDbId, orderNumber, blockType, noOfBlocks exist`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
        
                experimentBlockCode = record.experimentBlockCode
                experimentBlockName = record.experimentBlockName
                blockType = record.blockType
                plotNumberingOrder = record.plotNumberingOrder
                startingCorner = record.startingCorner
                parentExperimentBlockDbId = record.parentExperimentBlockDbId
                notes = record.notes
                        
                // Validate the user input
                // Parent experiment block
                if (record.parentExperimentBlockDbId != undefined) {
                    if (!validator.isInt(parentExperimentBlockDbId)) {
                        let errMsg = 'Invalid request, parent experiment block ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Check if it actually exists
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if parent experiment block exists, return 1
                            SELECT
                                count(1)
                            FROM
                                experiment.experiment_block "experimentBlock"
                            WHERE
                                "experimentBlock".is_void = FALSE AND
                                "experimentBlock".id = ${parentExperimentBlockDbId}
                        )
                    `

                    validateCount += 1
                }
                
                if (record.entryListIdList !== undefined) {
                    entryListIdList = record.entryListIdList
                    if (!validator.isJSON(entryListIdList)) {
                        let errMsg =
                            "Invalid format for entryListIdList. It must be a valid JSON string."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.layoutOrderData !== undefined) {
                    layoutOrderData = record.layoutOrderData
                    if (!validator.isJSON(layoutOrderData)) {
                        let errMsg =
                            "Invalid format for layoutOrderData. It must be a valid JSON string."
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // Experiment ID
                experimentDbId = record.experimentDbId

                if (!validator.isInt(experimentDbId)) {
                    let errMsg = 'Invalid request, experiment ID must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if experiment exists, return 1
                        SELECT
                        count(1)
                        FROM
                        experiment.experiment experiment
                        WHERE
                        experiment.is_void = FALSE AND
                        experiment.id = ${experimentDbId}
                    )
                `
                validateCount += 1
                
                // Check if experiment ID and experiment block code pairing is unique
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        --- Check if the pairing is unique, return 1
                        SELECT
                        CASE WHEN
                            (count(1) = 0)
                            THEN 1
                            ELSE 0
                        END AS count
                        FROM
                        experiment.experiment_block eb
                        WHERE
                        eb.experiment_id = ${experimentDbId} AND
                        eb.experiment_block_code =    ` +'$$' + experimentBlockCode + '$$'+`
                    )
                `

                validateCount += 1

                // Order Number
                orderNumber = record.orderNumber

                if (!validator.isInt(orderNumber)) {
                    let errMsg = 'Invalid request, order number must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Number of blocks
                noOfBlocks = record.noOfBlocks
        
                if (!validator.isInt(noOfBlocks)) {
                    let errMsg = 'Invalid request, number of blocks must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                noOfRanges = record.noOfRanges
                // Number of ranges
                if (noOfRanges != undefined) {
                    if (!validator.isInt(noOfRanges)) {
                        let errMsg = 'Invalid request, number of ranges must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                noOfCols = record.noOfCols
                // Number of cols
                if (noOfCols != undefined) {
                    if (!validator.isInt(noOfCols)) {
                        let errMsg = 'Invalid request, number of cols must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                noOfReps = record.noOfReps
                // Number of reps
                if (noOfReps != undefined) {
                    if (!validator.isInt(noOfReps)) {
                        let errMsg = 'Invalid request, number of reps must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    
                    }
                }
                
                // Validate the input
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `
                
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
            
                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (recordCount > 1) {
                // Check if the records are valid
                    let experimentBlockInfo = {
                        pair: `${experimentDbId}-${experimentBlockCode}`
                    }

                    if (visitedArray.includes(experimentBlockInfo)) {
                        let errMsg = 'Invalid request, duplicate experiment id and experiment block code combinations are found in the request body.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else {
                        visitedArray.push(experimentBlockInfo)
                    }
                }

                //Add is_active in the saving 
                if (record.isActive != undefined) {
                    isActive = record.isActive
                } else {
                    isActive = false;
                }


                // Get values
                let tempArray = [
                    experimentBlockCode,
                    experimentBlockName,
                    experimentDbId,
                    parentExperimentBlockDbId,
                    orderNumber,
                    blockType,
                    noOfBlocks,
                    noOfRanges,
                    noOfCols,
                    noOfReps,
                    plotNumberingOrder,
                    startingCorner,
                    entryListIdList, 
                    layoutOrderData,
                    isActive,
                    personDbId,
                    notes
                ]

                experimentBlockValuesArray.push(tempArray)
            }

            // Create experiment block record
            let experimentBlockQuery = format(`
                INSERT INTO experiment.experiment_block (
                    experiment_block_code,
                    experiment_block_name,
                    experiment_id,
                    parent_experiment_block_id,
                    order_number,
                    block_type,
                    no_of_blocks,
                    no_of_ranges,
                    no_of_cols,
                    no_of_reps,
                    plot_numbering_order,
                    starting_corner,
                    entry_list_id_list,
                    layout_order_data,
                    is_active,
                    creator_id,
                    notes
                )
                VALUES
                    %L
                RETURNING
                    id`, experimentBlockValuesArray
            )
            
            let experimentBlocks = await sequelize.query(experimentBlockQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            for (experimentBlock of experimentBlocks[0]) {
                experimentBlockDbId = experimentBlock.id
                
                // Return the experiment block info
                let ebObj = {
                    experimentBlockDbId: experimentBlockDbId,
                    recordCount: 1,
                    href: experimentBlockUrlString + '/' + experimentBlockDbId
                }

                resultArray.push(ebObj)
            }

            // Commit the transaction
            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
} 