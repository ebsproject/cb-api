/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let errorBuilder = require('../../../../helpers/error-builder')
let userValidator = require('../../../../helpers/person/validator.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../../helpers/logger')
const endpoint = 'planting-instructions/:id'

module.exports = {
    // Endpoint for updating an existing planting instruction record
    // PUT /v3/planting-instructions/:id
    put: async function (req, res, next) {

        // Retrieve the planting instruction ID
        let plantingInstructionDbId = req.params.id

        if(!validator.isInt(plantingInstructionDbId)) {
            let errMsg = `Invalid format, planting instruction ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        // Check if planting instruction is existing
        // Build query
        let plantingInstructionQuery = `
            SELECT
                pi.entry_name AS "entryName",
                pi.entry_type AS "entryType",
                pi.entry_role AS "entryRole",
                pi.entry_class AS "entryClass",
                pi.entry_status AS "entryStatus",
                pi.germplasm_id AS "germplasmDbId",
                pi.seed_id AS "seedDbId",
                pi.package_id AS "packageDbId",
                pi.package_log_id AS "packageLogDbId",
                entry.id AS "entryDbId",
                creator.id AS "creatorDbId"
            FROM
                experiment.planting_instruction pi
            LEFT JOIN 
                experiment.entry entry ON entry.id = pi.entry_id
            LEFT JOIN 
                tenant.person creator ON creator.id = pi.creator_id
            WHERE
                pi.is_void = FALSE AND
                pi.id = ${plantingInstructionDbId}
        `

        // Retrieve planting instruction from the database
        let plantingInstruction = await sequelize.query(plantingInstructionQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await plantingInstruction === undefined || await plantingInstruction.length < 1) {
            let errMsg = `The planting instruction you have requested does not exist.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values from database, record = the current value in the database
        
        let recordEntryNumber = plantingInstruction[0].entryNumber
        let recordEntryName = plantingInstruction[0].entryName
        let recordEntryType = plantingInstruction[0].entryType
        let recordEntryRole = plantingInstruction[0].entryRole
        let recordEntryClass = plantingInstruction[0].entryClass
        let recordEntryStatus = plantingInstruction[0].entryStatus
        let recordEntryDbId = plantingInstruction[0].entryDbId
        let recordCreatorDbId = plantingInstruction[0].creatorDbId
        let recordGermplasmDbId = plantingInstruction[0].germplasmDbId
        let recordSeedDbId = plantingInstruction[0].seedDbId
        let recordPackageDbId = plantingInstruction[0].packageDbId
        let recordPackageLogDbId = plantingInstruction[0].packageLogDbId

        // Check if user is an admin or an owner of the cross
        if(!isAdmin && (personDbId != recordCreatorDbId)) {
            let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                LEFT JOIN 
                    experiment.entry_list entryList ON entryList.experiment_id = experiment.id
                LEFT JOIN 
                    experiment.entry entry ON entry.entry_list_id = entryList.id
                WHERE 
                    entry.id = ${recordEntryDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
            `

            let person = await sequelize.query(programMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            
            if (person[0].id === undefined || person[0].role === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
            
            // Checks if user is a program team member or has a producer role
            let isProgramTeamMember = (person[0].id == personDbId) ? true : false
            let isProducer = (person[0].role == '26') ? true : false
            
            if (!isProgramTeamMember && !isProducer) {
                let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                res.send(new errors.UnauthorizedError(errMsg))
                return
            }
        }

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let plantingInstructionUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/planting-instructions/"
            + plantingInstructionDbId

        // Check is req.body has parameters
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the input
        let data = req.body    

        try {

            let setQuery = ``
            let validateQuery = ``
            let validateCount = 0

            /** Validation of parameters and set values **/

            let entryDbId = null
            if (data.entryDbId !== undefined) {
                entryDbId = data.entryDbId

                if (entryDbId != recordEntryDbId) {
                    if (!validator.isInt(entryDbId)) {
                        let errMsg = 'Invalid request, entry ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    let entryQuery = `
                        (
                            --- Check if entry exists, return 1
                            SELECT
                                count(1)
                            FROM
                                experiment.entry
                            WHERE
                                is_void = FALSE AND
                                id = ${entryDbId}
                        )
                    `

                    let entry = await sequelize
                        .query(entryQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                    if (entry[0]['count'] != 1) {
                        let errMsg = 'The entry ID you have provided does not exist.'
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_id = $$${entryDbId}$$
                    `
                }
            }

            let entryNumber = null
            if (data.entryNumber  !== undefined) {
                entryNumber = data.entryNumber

                if (entryNumber != recordEntryNumber) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_number = $$${entryNumber}$$
                    `
                }
            }

            let entryName = null
            if (data.entryName  !== undefined) {
                entryName = data.entryName

                if (entryName != recordEntryName) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_name = $$${entryName}$$
                    `
                }
            }

            let entryType = null
            if (data.entryType  !== undefined) {
                entryType = data.entryType

                // Check if user input is same with the current value in the database
                if (entryType != recordEntryType) {


                    let entryTypeQuery = `
                        (
                            --- Check if entry type is existing return 1
                            SELECT                      
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_TYPE' AND
                                scaleValue.value ILIKE $$${entryType}$$
                        )
                    `

                    let entryTypes = await sequelize
                        .query(entryTypeQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        
                    if (entryTypes[0]['count'] != 1) {
                        let errMsg = 'Invalid request, you have provided an invalid value for entry type. Make sure that it is one of the following: border, check, control, cross, entry, filler, parent, test.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_type = $$${entryType}$$
                    `
                }
            }

            let entryRole = null
            if (data.entryRole  !== undefined) {
                entryRole = data.entryRole.trim()

                // Check if user input is same with the current value in the database
                if (entryRole != recordEntryRole) {

                    entryRoleQuery = `
                        (
                            --- Check if entry role is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_ROLE' AND
                                scaleValue.value ILIKE $$${entryRole}$$
                        )
                    `

                    let entryRoles = await sequelize
                        .query(entryRoleQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        
                    if (entryRoles[0]['count'] != 1) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400235)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_role = $$${entryRole}$$
                    `
                }

            }
            let entryClass = null
            if (data.entryClass  !== undefined) {
                entryClass = data.entryClass

                // Check if user input is same with the current value in the database
                if (entryClass != recordEntryClass) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_class = $$${entryClass}$$
                    `
                }
            }

            let entryStatus = null
            if (data.entryStatus  !== undefined) {
                entryStatus = data.entryStatus

                // Check if user input is same with the current value in the database
                if (entryStatus != recordEntryStatus) {

                    entryStatusQuery = `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_STATUS' AND
                                scaleValue.value ILIKE $$${entryStatus}$$
                        )
                    `

                    let entryStatuses = await sequelize
                        .query(entryStatusQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        
                    if (entryStatuses[0]['count'] != 1) {
                        let errMsg = 'Invalid request, you have provided an invalid value for entry status. Make sure that it is one of the following: active, draft, inactive'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        entry_status = $$${entryStatus}$$
                    `
                }
            }

            let germplasmDbId = null
            if (data.germplasmDbId !== undefined) {
                germplasmDbId = data.germplasmDbId

                if (germplasmDbId != recordGermplasmDbId) {
                    if (!validator.isInt(germplasmDbId)) {
                        let errMsg = 'Invalid request, germplasm ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    let germplasmQuery = `
                        (
                            --- Check if germplasm exists, return 1
                            SELECT
                                count(1)
                            FROM
                                germplasm.germplasm
                            WHERE
                                is_void = FALSE AND
                                id = ${germplasmDbId}
                        )
                    `

                    let germplasm = await sequelize
                        .query(germplasmQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                    if (germplasm[0]['count'] != 1) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 404046)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_id = $$${germplasmDbId}$$
                    `
                }
            }

            let seedDbId = null
            if (data.seedDbId !== undefined) {
                seedDbId = data.seedDbId

                if (seedDbId != recordSeedDbId) {
                    if (!validator.isInt(seedDbId)) {
                        let errMsg = 'Invalid request, seed ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    let seedQuery = `
                        (
                            --- Check if seed exists, return 1
                            SELECT
                                count(1)
                            FROM
                                germplasm.seed
                            WHERE
                                is_void = FALSE AND
                                id = ${seedDbId}
                        )
                    `

                    let seed = await sequelize
                        .query(seedQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                    if (seed[0]['count'] != 1) {
                        let errMsg = 'Invalid request, the seed ID you have provided does not exist.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        seed_id = $$${seedDbId}$$
                    `
                }
            }

            let packageDbId = null
            if (data.packageDbId !== undefined) {
                packageDbId = data.packageDbId

                if (packageDbId != recordPackageDbId) {
                    if (!validator.isInt(packageDbId)) {
                        let errMsg = 'Invalid request, package ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    let packageQuery = `
                        (
                            --- Check if package exists, return 1
                            SELECT
                                count(1)
                            FROM
                                germplasm.package
                            WHERE
                                is_void = FALSE AND
                                id = ${packageDbId}
                        )
                    `

                    let package = await sequelize
                        .query(packageQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                    if (package[0]['count'] != 1) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 404047)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_id = $$${packageDbId}$$
                    `
                }
            }

            let packageLogDbId = null
            if (data.packageLogDbId !== undefined) {
                packageLogDbId = data.packageLogDbId

                if (packageLogDbId != recordSeedDbId) {
                    if (!validator.isInt(packageLogDbId)) {
                        let errMsg = 'Invalid request, package log ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    let packageLogQuery = `
                        (
                            --- Check if package log exists, return 1
                            SELECT
                                count(1)
                            FROM
                                germplasm.package_log
                            WHERE
                                is_void = FALSE AND
                                id = ${packageLogDbId}
                        )
                    `

                    let packageLog = await sequelize
                        .query(packageLogQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })

                    if (packageLog[0]['count'] != 1) {
                        let errMsg = 'Invalid request, the package log ID you have provided does not exist.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        package_log_id = $$${packageLogDbId}$$
                    `
                }
            }

            let updateFlag = 0
            if (setQuery.length > 0) {
                setQuery += ','
                updateFlag = 1
            }

            // Update the planting instruction record
            let updatePlantingInstructionQuery = `
                UPDATE
                    experiment.planting_instruction
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${plantingInstructionDbId}
            `
            
            await sequelize.transaction(async transaction => {
                await sequelize.query(updatePlantingInstructionQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                })
            }).catch(async err => {
                logger.logFailingQuery(endpoint, 'UPDATE', err)
                throw new Error(err)
            })

            // Return the transaction info
            let resultArray = {
                plantingInstructionDbId: plantingInstructionDbId,
                recordCount: 1,
                href: plantingInstructionUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for voiding an existing planting instruction record
    // DELETE /v3/planting-instructions/:id
    delete: async function (req, res, next) {
        // Retrieve the planting instruction ID
        let plantingInstructionDbId = req.params.id

        if (!validator.isInt(plantingInstructionDbId)) {
            let errMsg = `Invalid format, planting instruction ID must be an integer.`
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(personDbId)

        try {
            let plantingInstructionQuery = `
                SELECT
                    creator.id AS "creatorDbId",
                    experiment.id AS "experimentDbId"
                FROM
                    experiment.planting_instruction pi
                LEFT JOIN
                    tenant.person creator ON creator.id = pi.creator_id
                LEFT JOIN
                    experiment.entry e ON e.id = pi.entry_id
                LEFT JOIN
                    experiment.entry_list el ON el.id = e.entry_list_id
                LEFT JOIN
                    experiment.experiment experiment ON experiment.id = el.experiment_id
                WHERE
                    pi.is_void = FALSE AND
                    pi.id = ${plantingInstructionDbId}
            `

            let plantingInstruction = await sequelize
                .query(plantingInstructionQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    logger.logFailure(endpoint, 'DELETE', err)
                    
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await plantingInstruction == undefined || await plantingInstruction.length < 1) {
                let errMsg = `The planting instruction you have requested does not exist.`
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let recordCreatorDbId = plantingInstruction[0].creatorDbId
            let recordExperimentDbId = plantingInstruction[0].experimentDbId

            // Check if user is an admin or an owner of the cross
            if(!isAdmin && (personDbId != recordCreatorDbId)) {
                let programMemberQuery = `
                SELECT 
                    person.id,
                    person.person_role_id AS "role"
                FROM 
                    tenant.person person
                LEFT JOIN 
                    tenant.team_member teamMember ON teamMember.person_id = person.id
                LEFT JOIN 
                    tenant.program_team programTeam ON programTeam.team_id = teamMember.team_id
                LEFT JOIN 
                    tenant.program program ON program.id = programTeam.program_id 
                LEFT JOIN
                    experiment.experiment experiment ON experiment.program_id = program.id
                WHERE 
                    experiment.id = ${recordExperimentDbId} AND
                    person.id = ${personDbId} AND
                    person.is_void = FALSE
                `

                let person = await sequelize.query(programMemberQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (person[0].id === undefined || person[0].role === undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401025)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
                
                // Checks if user is a program team member or has a producer role
                let isProgramTeamMember = (person[0].id == personDbId) ? true : false
                let isProducer = (person[0].role == '26') ? true : false
                
                if (!isProgramTeamMember && !isProducer) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401026)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let deletePlantingInstructionQuery = format(`
                UPDATE
                    experiment.planting_instruction
                SET
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${plantingInstructionDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${plantingInstructionDbId}
            `)

            await sequelize.transaction( async transaction => {
                await sequelize.query(deletePlantingInstructionQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                })
                .catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)

                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
            })

            res.send(200, {
                rows: { plantingInstructionDbId: plantingInstructionDbId }
            })
            return

        } catch (err) {
            logger.logFailure(endpoint, 'DELETE', err)

            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}