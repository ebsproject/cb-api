/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let configHelper = require('../../../helpers/config/index.js')
let logger = require('../../../helpers/logger')
const endpoint = 'plots'

module.exports = {

    // Implementation of POST call for /v3/planting-instructions
    post: async (req, res, next) => {
        // Set defaults
        let resultArray = []

        let recordCount = 0

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        // Retrieve the ID of the client via the access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if (personDbId === undefined) {
            return
        }
        // If personDbId is null, return 401: User not found error
        else if (personDbId === null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let entryUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/planting-instructions'

        let data = req.body
        let records = []

        
        records = data.records
        if (records === undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {

            let entryValuesArray = []
            let visitedArray = []

            for (let record of records) {
                let validateQuery = ``
                let validateCount = 0

                let entryDbId = null
                let plotDbId = null
                let germplasmDbId = null
                let seedDbId = null
                let packageDbId = null

                let entryName = null
                let entryType = null
                let entryStatus = null
                let entryRole = null
                let entryClass = null
                let entryNumber = null
                let entryCode = null

                // Check if required columns are in the input
                if (
                    !record.entryName || !record.entryType || !record.entryStatus ||
                    !record.plotDbId
                ) {
                    let errMsg = `Required parameters are missing. Ensure that entryName, entryType, entryStatus, and plotDbId fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /** Validation of required parameters and set values */

                if (record.entryType !== undefined) {
                    entryType = record.entryType

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry type is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_TYPE' AND
                                scaleValue.value ILIKE $$${entryType}$$
                        )
                    `
                    validateCount += 1
                }

                if (record.entryStatus !== undefined) {
                    entryStatus = record.entryStatus.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry status is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_STATUS' AND
                                scaleValue.value ILIKE $$${entryStatus}$$
                        )
                    `
                    validateCount += 1
                }

                if (record.entryDbId !== undefined) {
                    entryDbId = record.entryDbId

                    if (!validator.isInt(entryDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate entry ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.entry
                            WHERE 
                                is_void = FALSE AND
                                id = ${entryDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.plotDbId !== undefined) {
                    plotDbId = record.plotDbId

                    if (!validator.isInt(plotDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400021)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate entry ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if plot is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                experiment.plot
                            WHERE 
                                is_void = FALSE AND
                                id = ${plotDbId}
                        )
                    `
                    validateCount += 1
                }

                // if germplasmDbId is specified in the request body
                if (record.germplasmDbId !== undefined) {
                    germplasmDbId = record.germplasmDbId

                    if (!validator.isInt(germplasmDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400238)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate germplasm ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if germplasm is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.germplasm
                            WHERE 
                                is_void = FALSE AND
                                id = ${germplasmDbId}
                        )
                    `
                    validateCount += 1
                } else {
                    //get default germplasmDbId
                    let fillerConfig = await configHelper.getConfigValueByAbbrev('DEFAULT_FILLER_GERMPLASM');
                    let borderConfig = await configHelper.getConfigValueByAbbrev('DEFAULT_BORDER_GERMPLASM');
                    let plotQuery = `
                        SELECT 
                            plot.id,
                            plot.plot_type,
                            entry.germplasm_id,
                            program.crop_program_id 
                        FROM 
                            experiment.plot 
                            LEFT JOIN experiment.entry ON entry.id = plot.entry_id 
                            LEFT JOIN experiment.occurrence o 
                                ON o.id = plot.occurrence_id AND o.is_void = FALSE
                            LEFT JOIN experiment.experiment e 
                                ON e.id = o.experiment_id 
                            LEFT JOIN tenant.program ON program.id = e.program_id 
                        WHERE
                            plot.id = ${plotDbId}
                    `
                    let plotResult = await sequelize.query(plotQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    let cropProgramId = plotResult[0]['crop_program_id']
                    
                    if (plotResult[0]['plot_type'] == 'border') {
                        germplasmDbId = borderConfig[cropProgramId]["germplasm_id"]
                    } else if (plotResult[0]['plot_type'] == 'filler') {
                        germplasmDbId = fillerConfig[cropProgramId]["germplasm_id"]
                    } else {
                        // if there is no specified germplasmDbId,
                        // germplasmDbId is retrieved from plot's entry
                        germplasmDbId = plotResult[0]['germplasm_id']
                    }
                }

                entryName = record.entryName
                entryNumber = (record.entryNumber !== undefined) ? record.entryNumber : null
                entryCode = (record.entryCode !== undefined) ? record.entryCode : null

                if (record.entryCode !== undefined && record.entryNumber !== undefined) {
                    // Validate that entry ID, entry number and entry code combination exists in experiment.entry table
                    let checkCombQuery = `
                        SELECT
                            count(1)
                        FROM
                            experiment.entry
                        WHERE
                            id = ${entryDbId} AND
                            entry_number = ${entryNumber} AND
                            entry_code = '${entryCode}' AND
                            is_void = false
                    `

                    let combResult = await sequelize.query(checkCombQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (combResult[0].count < 1) {
                        let errMsg = `The entry ID, entry number and entry code combination does not exist in entry table.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.entryCode !== undefined && record.entryNumber !== undefined) {
                    // Validate that plot ID, entry number and entry code combination doesn't exist in experiment.planting_instruction table
                    let checkCombQuery = `
                        SELECT
                            count(1)
                        FROM
                            experiment.planting_instruction
                        WHERE
                            plot_id = ${plotDbId} AND
                            entry_number = ${entryNumber} AND
                            entry_code = '${entryCode}' AND
                            is_void = false
                    `

                    let combResult = await sequelize.query(checkCombQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (combResult[0].count > 0) {
                        let errMsg = `The plot ID, entry number and entry code combination already exists in planting instruction table.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.seedDbId !== undefined) {
                    seedDbId = record.seedDbId

                    if (!validator.isInt(seedDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400236)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate seed ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if seed is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.seed
                            WHERE 
                                is_void = FALSE AND
                                id = ${seedDbId}
                        )
                    `
                    validateCount += 1
                }

                /** Validation of non-required parameters and set values */

                if (record.entryRole !== undefined) {
                    entryRole = record.entryRole.trim()

                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if entry role is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'ENTRY_ROLE' AND
                                scaleValue.value ILIKE $$${entryRole}$$
                        )
                    `
                    validateCount += 1
                }

                entryClass = (record.entryClass !== undefined) ? record.entryClass : null

                if (record.packageDbId !== undefined) {
                    packageDbId = record.packageDbId

                    if (!validator.isInt(packageDbId)) {
                        let errMsg = `Invalid format, package ID must be an integer.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate package ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if seed is existing return 1
                            SELECT 
                                count(1)
                            FROM 
                                germplasm.package
                            WHERE 
                                is_void = FALSE AND
                                id = ${packageDbId}
                        )
                    `
                    validateCount += 1
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Get values
                let tempArray = [
                    entryCode, entryNumber, entryName, entryType, entryStatus, entryDbId, plotDbId,
                    germplasmDbId, entryRole, entryClass, seedDbId, packageDbId, personDbId
                ]

                entryValuesArray.push(tempArray)
            }

            // Create entry record
            let entriesQuery = format(`
                INSERT INTO 
                    experiment.planting_instruction (
                        entry_code, entry_number, entry_name, entry_type, entry_status,
                        entry_id, plot_id, germplasm_id, entry_role, entry_class,
                        seed_id, package_id, creator_id
                    )
                VALUES
                    %L
                RETURNING id`, entryValuesArray
            )

            let entries = await sequelize.transaction(async transaction => {
                return await sequelize.query(entriesQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true,
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (let entry of entries[0]) {
                plantingInstructionDbId = entry.id

                let entryRecord = {
                    plantingInstructionDbId: plantingInstructionDbId,
                    recordCount: 1,
                    href: entryUrlString + '/' + plantingInstructionDbId
                }

                resultArray.push(entryRecord)
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}