/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let errorBuilder = require('../../../../helpers/error-builder')
let logger = require('../../../../helpers/logger/index')
const endpoint = 'planting-job-entries/:id'

module.exports = {
    /**
     * Update method for planting_job_entries
     * PUT /v3/planting-job-entries/id/
     * @param {*} req Request parameters
     * @param {*} res Response
     */
    put: async function (req, res, next) {
        let plantingJobEntryDbId = req.params.id

        if (!validator.isInt(plantingJobEntryDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400216)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of the client from the access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plantingJobEntryQuery = `
            SELECT
                p.is_sufficient AS "isSufficient",
                p.is_replaced AS "isReplaced",
                p.required_package_quantity AS "requiredPackageQuantity",
                p.required_package_unit AS "requiredPackageUnit",
                p.replacement_type AS "replacementType",
                p.replacement_germplasm_id AS "replacementGermplasmDbId",
                p.replacement_package_id AS "replacementPackageDbId",
                p.replacement_entry_id AS "replacementEntryDbId",
                p.package_log_id AS "packageLogDbId"
            FROM
                experiment.planting_job_entry p
            WHERE
                p.is_void = FALSE AND
                p.id = ${plantingJobEntryDbId}
        `

        let plantingJobEntry = await sequelize
            .query(plantingJobEntryQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJobEntry == undefined ||
            await plantingJobEntry.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404045)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let recordIsSufficient = plantingJobEntry[0].isSufficient
        let recordIsReplaced = plantingJobEntry[0].isReplaced
        let recordRequiredPackageQuantity = plantingJobEntry[0].requiredPackageQuantity
        let recordRequiredPackageUnit = plantingJobEntry[0].requiredPackageUnit
        let recordReplacementType = plantingJobEntry[0].replacementType
        let recordReplacementGermplasmDbId = plantingJobEntry[0].replacementGermplasmDbId
        let recordReplacementPackageDbId = plantingJobEntry[0].replacementPackageDbId
        let recordReplacementEntryDbId = plantingJobEntry[0].replacementEntryDbId
        let recordPackageLogDbId = plantingJobEntry[0].packageLogDbId

        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let plantingJobEntryUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/planting-job-entries/'
            + plantingJobEntryDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let booleanValues = [
            'true',
            'false'
        ]

        // Retrieve the valid units for planting job entries
        let validUnitsQuery = `
            SELECT
                array_agg(value)
            FROM
                master.scale_value
            WHERE
                is_void = FALSE AND
                scale_id = (
                    SELECT
                        scale_id
                    FROM
                        master.variable
                    WHERE
                        abbrev = 'PACKAGE_UNIT'
                )
        `
        
        let units = await sequelize
            .query(validUnitsQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500006)
                res.send(new errors.InternalError(errMsg))
                return
            })

        let validReplacementTypesQuery = `
            SELECT
                array_agg(value)
            FROM
                master.scale_value
            WHERE
                is_void = FALSE AND
                scale_id = (
                    SELECT
                        scale_id
                    FROM
                        master.variable
                    WHERE
                        abbrev = 'REPLACEMENT_TYPE'
                )
        `

        let replacementTypes = await sequelize
            .query(validReplacementTypesQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500005)
                res.send(new errors.InternalError(errMsg))
                return
            })

        let transaction

        try {

            let setQuery = ''

            let isSufficient = null
            if (data.isSufficient !== undefined) {
                isSufficient = data.isSufficient

                if(isSufficient !== String(recordIsSufficient)) {
                    if (booleanValues.includes(isSufficient)) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            is_sufficient = $$${isSufficient}$$
                        `
                    } else {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400217)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }
            }

            let isReplaced = null
            if (data.isReplaced !== undefined) {
                isReplaced = data.isReplaced

                if(isReplaced !== String(recordIsReplaced)) {
                    if (booleanValues.includes(isReplaced)) {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            is_replaced = $$${isReplaced}$$
                        `
                    } else {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400218)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }
            }

            let requiredPackageQuantity = null
            if (data.requiredPackageQuantity !== undefined) {
                requiredPackageQuantity = data.requiredPackageQuantity
                if (requiredPackageQuantity != recordRequiredPackageQuantity) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        required_package_quantity = $$${requiredPackageQuantity}$$
                    `
                }
            }

            let requiredPackageUnit = null
            if (data.requiredPackageUnit !== undefined) {
                requiredPackageUnit = data.requiredPackageUnit
                if (requiredPackageUnit != recordRequiredPackageUnit) {
                    if (!units[0]['array_agg'].includes(requiredPackageUnit)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400219)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }


                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        required_package_unit = $$${requiredPackageUnit}$$
                    `
                }
            }

            let replacementType = null

            if (data.replacementType !== undefined) {
                replacementType = data.replacementType
                
                if (replacementType != recordReplacementType) {
                    if (!replacementTypes[0]['array_agg'].includes(replacementType)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400220)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        replacement_type = $$${replacementType}$$
                    `
                }
            }

            let replacementGermplasmDbId = null
            if (data.replacementGermplasmDbId !== undefined) {
                replacementGermplasmDbId = data.replacementGermplasmDbId

                if (replacementGermplasmDbId == null) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        replacement_germplasm_id = NULL
                    `
                } else {
                    if (!validator.isInt(replacementGermplasmDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400221)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    if (replacementGermplasmDbId != recordReplacementGermplasmDbId) {
                        // Check if germplasm exists
                        let validateGermplasmQuery = `
                            SELECT
                                count(1)
                            FROM
                                germplasm.germplasm
                            WHERE
                                is_void = FALSE AND
                                id = ${replacementGermplasmDbId}
                        `

                        let germplasm = await sequelize
                            .query(validateGermplasmQuery, {
                                type: sequelize.QueryTypes.SELECT
                            })

                        if (germplasm[0]['count'] != 1) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 404046)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        } 

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            replacement_germplasm_id = $$${replacementGermplasmDbId}$$
                        `
                    }
                }
            }

            let replacementPackageDbId = null
            if (data.replacementPackageDbId !== undefined) {
                replacementPackageDbId = data.replacementPackageDbId

                if (!validator.isInt(replacementPackageDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400222)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                
                if (replacementPackageDbId != recordReplacementPackageDbId) {
                    // Check if existing packageDbId
                    let validatePackageQuery = `
                        SELECT
                            count(1)
                        FROM
                            germplasm.package
                        WHERE
                            is_void = FALSE AND
                            id = ${replacementPackageDbId}
                    `

                    let package = await sequelize
                        .query(validatePackageQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        
                    if (package[0]['count'] != 1) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 404047)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } 

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        replacement_package_id = $$${replacementPackageDbId}$$
                    `
                }
            }

            let replacementEntryDbId = null
            if (data.replacementEntryDbId !== undefined) {
                replacementEntryDbId = data.replacementEntryDbId

                if (!validator.isInt(replacementEntryDbId)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400223)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                
                if (replacementEntryDbId != recordReplacementEntryDbId) {
                    // Check if entry exists
                    let validateEntryQuery = `
                        SELECT
                            count(1)
                        FROM
                            experiment.entry
                        WHERE
                            is_void = FALSE AND
                            id = ${replacementEntryDbId}
                    `

                    let entry = await sequelize
                        .query(validateEntryQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                        
                    if (entry[0]['count'] != 1) {
                        let errMsg = 'The entry ID you have provided does not exist.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } 

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        replacement_entry_id = $$${replacementEntryDbId}$$
                    `
                }
            }

            let packageLogDbId = null
            if (data.packageLogDbId !== undefined) {
                packageLogDbId = data.packageLogDbId

                if ((packageLogDbId !== null && packageLogDbId !== '') && !validator.isInt(packageLogDbId)) {
                    let errMsg = "Invalid format for Package Log ID. Package Log ID must be an integer."
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (packageLogDbId != recordPackageLogDbId) {
                    if (packageLogDbId !== null && packageLogDbId !== '') { // Check if existing packageDbId
                        let validatePackageLogQuery = `
                            SELECT
                                count(1)
                            FROM
                                germplasm.package_log
                            WHERE
                                is_void = FALSE AND
                                id = ${packageLogDbId}
                        `

                        let packageLog = await sequelize
                            .query(validatePackageLogQuery, {
                                type: sequelize.QueryTypes.SELECT
                            })
                            
                        if (packageLog[0]['count'] != 1) {
                            let errMsg = "The package log ID you have provided does not exist."
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        } 
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    if (packageLogDbId === null || packageLogDbId === '') {
                        setQuery += `
                            package_log_id = NULL
                        `
                    } else {
                        setQuery += `
                            package_log_id = $$${packageLogDbId}$$
                        `
                    }
                }
            }

            let updateFlag = 0
            if (setQuery.length > 0) {
                setQuery += ','
                updateFlag = 1
            }

            let updatePlantingJobEntryQuery = `
                UPDATE
                    experiment.planting_job_entry
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${plantingJobEntryDbId}
            `
            
            await sequelize.transaction(async transaction => {
                await sequelize.query(updatePlantingJobEntryQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                })

                if (isReplaced === 'true') {
                    // Update is_sufficient column
                    const updateIsSufficientColumnQuery = `UPDATE
                            experiment.planting_job_entry
                        SET
                            is_sufficient = CASE WHEN
                                (
                                    SELECT ( COALESCE(package_quantity, 0) - COALESCE(package_reserved, 0) ) 
                                    FROM germplasm.package WHERE id = ${replacementPackageDbId}
                                )
                                    < required_package_quantity
                                THEN FALSE
                                ELSE TRUE
                            END,
                            modification_timestamp = NOW(),
                            modifier_id = ${personDbId}
                        WHERE
                            id = ${plantingJobEntryDbId};`

                    await sequelize.query(updateIsSufficientColumnQuery, {
                        type: sequelize.QueryTypes.UPDATE,
                        raw: true,
                        transaction: transaction
                    }).then(() => {
                        logger.logCompletion('planting-job-entries',
                            'UPDATE',
                            {
                                plantingJobEntryDbId,
                                'message': 'Successfully updated is_sufficient column!',
                            }
                        )
                    })
                }
            }).catch(async err => {
                logger.logFailingQuery(endpoint, 'UPDATE', err)
                throw new Error(err)
            })

            let resultArray = {
                plantingJobEntryDbId: plantingJobEntryDbId,
                recordCount: updateFlag,
                href: plantingJobEntryUrlString
            }

            res.send(200, {
                rows: resultArray
            })

            logger.logCompletion('planting-job-entries',
                'UPDATE',
                {
                    plantingJobEntryDbId,
                    'message': 'Successfully updated planting job entry record!',
                }
            )

            return
        } catch (err) {
            logger.logMessage(__filename, err, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}