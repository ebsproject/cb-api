/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize, Seasons } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token.js')
let userValidator = require('../../../helpers/person/validator.js')
let format = require('pg-format')
let forwarded = require('forwarded-for')
let validator = require('validator')

module.exports = {

  // Implementation of GET call for /v3/seasons
  get: async function (req, res, next) {

    // Set defaults 
    let limit = req.paginate.limit

    let offset = req.paginate.offset

    let params = req.query

    let sort = req.query.sort

    let count = 0

    let conditionString = ''

    let orderString = ''

    // Build query
    let selectClause = `
      SELECT 
        season.id as "seasonDbId",
        season.season_code as "seasonCode",
        season.season_name as "seasonName",
        season.description,
        season.is_active as "isActive",
        season.creation_timestamp as "creationTimestamp",
        season.creator_id AS "creatorDbId",
        creator.person_name as creator,
        season.modification_timestamp as "modificationTimestamp",
        season.modifier_id as "modifierDbId",
        modifier.person_name as modifier
      FROM tenant.season season
      LEFT JOIN 
        tenant.person creator ON season.creator_id = creator.id
      LEFT JOIN 
        tenant.person modifier ON season.modifier_id = modifier.id
    `

    // Check whether API caller needs both active and inactive seasons
    let whereClause
    if (params.forDataBrowser !== undefined) {
      whereClause = `
        WHERE 
          season.is_void = FALSE
      `
    } else {
      whereClause = `
        WHERE 
          season.is_void = FALSE
          AND season.is_active = TRUE
      `
    }

    let orderByClause = `
      ORDER BY
        season.id
    `

    let seasonsQuery = selectClause + whereClause + orderByClause

    // Get filter condition for season_code only
    if (params.seasonCode !== undefined) {
      let parameters = {
        seasonCode: params.seasonCode
      }

      conditionString = await processQueryHelper.getFilterString(parameters)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400003)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)

      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final sql query 
    seasonFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      seasonsQuery,
      conditionString,
      orderString
    )

    // Retrieve seasons from the database   
    let seasons = await sequelize.query(seasonFinalSqlQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        limit: limit,
        offset: offset
      }
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await seasons == undefined || await seasons.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get count 
      seasonCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(seasonsQuery, conditionString, orderString)

      seasonCount = await sequelize.query(seasonCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      count = seasonCount[0].count
    }

    res.send(200, {
      rows: seasons,
      count: count
    })
    return
  },

  // Implementation of POST call for /v3/seasons
  post: async function (req, res, next) {
    let seasonCode = null
    let seasonName = null
    let description = null
    let isActive = null
    let creatorId = null

    // Verify if user is an admin
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401002)
        res.send(new errors.BadRequestError(errMsg))
        return
    }
    
    if (!await userValidator.isAdmin(userDbId)) {
      res.send(new errors.UnauthorizedError('401: Unauthorized request. You do not have access to create a season.'))
      return
    }


    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure
    // Set URL for response
    let seasonUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/seasons'


    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      // Records = [{}...]
      records = data.records
      recordCount = records.length
      // Check if the input data is empty
      if (records == undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } catch (e) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    // Start transaction
    let transaction

    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let seasonValuesArray = []
      let visitedArray = []

      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0

        // Check if the required colums are in the input
        if (!record.seasonCode || !record.seasonName) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400085)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        /* Validate and set values */

        if (record.seasonCode != undefined) {
          seasonCode = record.seasonCode

          // Validate if the abbreviation does not yet exist in the DB
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              --- Check if the season code does not exist yet; if it doesn't exist, return 0
              SELECT
                CASE WHEN (count(1) > 0)
                  THEN 0
                  ELSE 1
                END
              FROM
                tenant.season season
              WHERE
                season.is_void = FALSE AND
                season.season_code = '${seasonCode}'
            )
          `

          validateCount += 1
        }

        seasonName = (record.seasonName != undefined) ? record.seasonName : null
        description = (record.description != undefined) ? record.description : null
        isActive = (record.isActive != undefined) ? record.isActive : true
        creatorId = userDbId

        // Validate the filters in DB; Count must be equal to validateCount
        let checkInputQuery = `SELECT ${validateQuery} AS count`
        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })
        if (inputCount[0]['count'] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        if (recordCount > 1) {
          // Check if season code is unique among bulk inputs
          if (visitedArray.includes(seasonCode)) {
            res.send(new errors.BadRequestError('400: Your bulk inputs have duplicate season codes.'))
            return
          } else {
            visitedArray.push(seasonCode)
          }
        }

        let tempArray = [
          seasonCode,
          seasonName,
          description,
          isActive,
          creatorId
        ]

        seasonValuesArray.push(tempArray)
      }

      // Create season record
      let seasonQuery = format(`
        INSERT INTO
          tenant.season
            (
              season_code, season_name, description, is_active, creator_id
            )
        VALUES
          %L
        RETURNING id`, seasonValuesArray
      )

      seasonQuery = seasonQuery.trim()

      let seasons = await sequelize.query(seasonQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      })

      let resultArray = []

      for (let season of seasons[0]) {
        seasonDbId = season.id

        // Return the season info to Client
        let array = {
          seasonDbId: seasonDbId,
          recordCount: 1,
          href: seasonUrlString + '/' + seasonDbId
        }

        resultArray.push(array)
      }

      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}
