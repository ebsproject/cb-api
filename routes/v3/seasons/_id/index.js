/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token.js')
let userValidator = require('../../../../helpers/person/validator')
let forwarded = require('forwarded-for')

module.exports = {
  // Implementation of GET call for /v3/seasons/:id
  get: async function (req, res, next) {
    // Retrieve the season ID
    let seasonDbId = req.params.id

    if (!validator.isInt(seasonDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400063)
      res.send(new errors.BadRequestError(errMsg))
      return
    } else {
      // Build query
      let selectClause = `
        SELECT 
          season.id as "seasonDbId",
          season.season_code as "seasonCode",
          season.season_name as "seasonName",
          season.description,
          season.is_active as "isActive",
          season.creation_timestamp as "creationTimestamp",
          season.creator_id AS "creatorDbId",
          creator.person_name as creator,
          season.modification_timestamp as "modificationTimestamp",
          season.modifier_id as "modifierDbId",
          modifier.person_name as modifier
        FROM tenant.season season
        LEFT JOIN 
          tenant.person creator ON season.creator_id = creator.id
        LEFT JOIN 
          tenant.person modifier ON season.modifier_id = modifier.id
        WHERE 
          season.is_void = FALSE AND
          season.id = ${seasonDbId}
      `

      let seasonsQuery = selectClause

      // Retrieve season from the database   
      let seasons = await sequelize.query(seasonsQuery, {
        type: sequelize.QueryTypes.SELECT,
      }).catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      // Return error if resource is not found
      if (await seasons == undefined || await seasons.length < 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404019)
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      res.send(200, {
        rows: seasons
      })
      return
    }
  },

  // Implementation of POST call for /v3/seasons/:id
  put: async function (req, res, next) {
    // Check if user is an administrator. If not, no access.
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401002)
        res.send(new errors.BadRequestError(errMsg))
        return
    }

    let isAdmin = await userValidator.isAdmin(userDbId)
    if (!isAdmin) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401018)
      res.send(new errors.UnauthorizedError(errMsg))
      return
    }

    // Retrieve season ID
    let seasonDbId = req.params.id

    // Check if season ID data type is valid
    if (!validator.isInt(seasonDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400063)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if request body exists
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Set defaults
    let seasonCode = null
    let seasonName = null
    let description = null
    let isActive = null

    let resultArray = []

    // Build query
    let seasonQuery = `
      SELECT
        season.season_code,
        season.season_name,
        season.description,
        season.is_active
      FROM
        tenant.season season
      WHERE
        season.is_void = FALSE AND
        season.id = ${seasonDbId}
    `

    // Retrieve season record from database
    let season = await sequelize.query(seasonQuery, {
      type: sequelize.QueryTypes.SELECT
    }).catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    // Return error if resource is not found
    if (await season == undefined || await season.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404019)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    // Get values from database
    let seasonSeasonCode = season[0].season_code || null
    let seasonSeasonName = season[0].season_name || null
    let seasonDescription = season[0].description || null
    let seasonIsActive = season[0].is_active || null

    let isSecure = forwarded(req, req.headers).secure

    // Set URL for response
    let seasonUrlString = (isSecure ? 'https' : 'http')
      + "://"
      + req.headers.host
      + "/v3/seasons"

    let data = req.body
    let transaction

    try {
      transaction = await sequelize.transaction({ autocommit: false })

      let setQuery = ``
      let validateQuery = ``
      let validateCount = 0

      // Validate and set values
      if (data.seasonCode !== undefined) {
        seasonCode = data.seasonCode

        // Check if the input seasonCode differs from season_code found in database
        if (seasonSeasonCode != seasonCode) {
          // validate seasonCode
          validateQuery += (validateQuery != '') ? ' + ' : ''
          validateQuery += `
            (
              SELECT
                CASE WHEN (count(1) = 0)
                  THEN 1
                  ELSE 0
                END AS count
              FROM tenant.season season
              WHERE
                season.season_code LIKE '${seasonCode}'
            )
          `
          validateCount += 1
        }

        setQuery += (setQuery != '') ? ',' : ''
        setQuery += `
          season_code = $$${(seasonSeasonCode != seasonCode) ? seasonCode : seasonSeasonCode}$$`
      } else {
        setQuery += (setQuery != '') ? ',' : ''
        setQuery += `
          season_code = $$${seasonSeasonCode}$$`
      }

      if (data.seasonName != undefined) {
        seasonName = data.seasonName

        setQuery += (setQuery != '') ? ',' : ''
        setQuery += `
          season_name = $$${(seasonSeasonName != seasonName) ? seasonName : seasonSeasonName}$$`
      } else {
        setQuery += (setQuery != '') ? ',' : ''
        setQuery += `
          season_name = $$${seasonName}$$`
      }

      if (data.description != undefined) {
        description = data.description

        setQuery += (setQuery != '') ? ',' : ''
        setQuery += `
          description = $$${(seasonDescription != description) ? description : seasonDescription}$$`
      }

      if (data.isActive != undefined) {
        isActive = data.isActive

        setQuery += (setQuery != '') ? ',' : ''
        setQuery += `
          is_active = $$${(seasonIsActive != isActive) ? isActive : seasonIsActive}$$`
      } else {
        setQuery += (setQuery != '') ? ',' : ''
        setQuery += `
          is_active = $$${seasonIsActive}$$`
      }

      /* Validate input in DB */
      // count must be equal to validateCount if all conditions are met
      if (validateCount > 0) {
        let checkInputQuery = `
          SELECT ${validateQuery} AS count
        `
        let inputCount = await sequelize.query(checkInputQuery, {
          type: sequelize.QueryTypes.SELECT
        })

        if (inputCount[0]['count'] != validateCount) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400015)
          res.send(new errors.BadRequestError(errMsg))
          return
        }
      }

      // Update the season record
      let updateSeasonQuery = `
        UPDATE 
          tenant.season season
        SET 
          ${setQuery},
          modification_timestamp = NOW(),
          modifier_id = ${userDbId}
        WHERE
          season.id = ${seasonDbId}
      `

      await sequelize.query(updateSeasonQuery, {
        type: sequelize.QueryTypes.UPDATE
      })

      // Return the transaction info
      let array = {
        seasonDbId: seasonDbId,
        recordCount: 1,
        href: seasonUrlString + '/' + seasonDbId
      }
      resultArray.push(array)

      // Commit the transaction
      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return

    } catch (err) {
      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()

      let errMsg = await errorBuilder.getError(req.headers.host, 500003)
      res.send(new errors.InternalError(errMsg))
      return
    }

  },

  // Implementation of DELETE call for /v3/seasons/:id
  delete: async function (req, res, next) {
    // Check if user is an administrator
    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
        let errMsg = await errorBuilder.getError(req.headers.host, 401002)
        res.send(new errors.BadRequestError(errMsg))
        return
    }

    const isAdmin = await userValidator.isAdmin(userDbId)
    if (!isAdmin) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401019)
      res.send(new errors.UnauthorizedError(errMsg))
      return
    }

    // Defaults
    let seasonDbId = req.params.id

    if (!validator.isInt(seasonDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400063)
      res.send(new errors.BadRequestError(errMsg))
      return
    } else {
      // Start transaction
      let transaction

      try {
        // Get transaction
        transaction = await sequelize.transaction({ autocommit: false })

        // Validate the season
        // Check if season is existing
        // Build query
        let seasonQuery = `
          SELECT
            count(1)
          FROM 
            tenant.season season
          WHERE
            season.is_void = FALSE AND
            season.id = ${seasonDbId}
        `

        // Retrieve seasons from the database   
        let hasSeason = await sequelize.query(seasonQuery, {
          type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

        // Check if season is non-existent in database
        if (parseInt(hasSeason[0].count) === 0) {
          let errMsg = await errorBuilder.getError(req.headers.host, 404019)
          res.send(new errors.NotFoundError(errMsg))
          return
        }

        // Build query for voiding
        let deleteSeasonQuery = `
          UPDATE
            tenant.season season
          SET
            is_void = TRUE,
            is_active = FALSE,
            season_code = CONCAT(season.season_code, '-VOIDED-', season.id),
            modification_timestamp = NOW(),
            modifier_id = ${userDbId}
          WHERE
            season.id = ${seasonDbId}
        `

        await sequelize.query(deleteSeasonQuery, {
          type: sequelize.QueryTypes.UPDATE
        })

        // Commit the transaction
        await transaction.commit()

        res.send(200, {
          rows: {
            seasonDbId: seasonDbId
          }
        })
        return
      } catch (err) {
        // Rollback transaction if any errors were encountered
        if (err) await transaction.rollback()
        let errMsg = await errorBuilder.getError(req.headers.host, 500002)
        res.send(new errors.InternalError(errMsg))
        return
      }
    }

  }
}
