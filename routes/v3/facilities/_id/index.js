/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let validator = require('validator')

module.exports = {
  // GET /v3/facilities/{id}
  get: async function (req, res, next) {
    let facilityDbId = req.params.id

    if (!validator.isInt(facilityDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400143)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let facilityQuery = `
      SELECT
        facility.id AS "facilityDbId",
        facility.name,
        facility.other_name AS "otherName",
        facility.facility_type AS "facilityType",
        facility.latitude,
        facility.longitude,
        facility.level,
        facility.facility_qr_code AS "facilityQrCode",
        parentFacility.name AS "parentFacilityName",
        parentFacility.latitude AS "parentFacilityLatitude",
        parentFacility.longitude AS "parentFacilityLongitude",
        institute.id AS "instituteDbId",
        institute.name AS "instituteName",
        institute.institute_type AS "instituteType",
        institute.institute_department AS "instituteDepartment",
        place.id AS "placeDbId",
        place.display_name AS "placeDisplayName",
        facility.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.display_name AS "creator",
        facility.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.display_name AS "modifier"
      FROM
        master.facility facility
      LEFT JOIN
        master.user creator ON facility.creator_id = creator.id
      LEFT JOIN
        master.user modifier ON facility.modifier_id = modifier.id
      LEFT JOIN
        master.institute institute ON facility.institute_id = institute.id
      LEFT JOIN
        master.place place ON facility.place_id = place.id
      LEFT JOIN
        master.facility parentFacility ON facility.root_id = parentFacility.id
      WHERE
        facility.is_void = FALSE AND
        facility.id = ${facilityDbId}
      ORDER BY
        facility.id
    `
    // Retrieve the facility record from the database
    let facility = await sequelize
      .query(facilityQuery, {
        type: sequelize.QueryTypes.SELECT,
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Check if the target facility record exists
    if (await facility == undefined || await facility.length < 1) {
      let errMsg = await errorBuilder.getError(req.headers.host, 404037)
      res.send(new errors.NotFoundError(errMsg))
      return
    }

    res.send(200, {
      rows: facility
    })
    return
  }
}