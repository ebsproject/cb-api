/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')
let userValidator = require('../../../helpers/person/validator.js')

module.exports = {

    /**
     * Implementation for GET v3/facilities
     * 
     * @param {*} req Request parameters 
     * @param {*} res Response
     */
    get: async (req, res, next) => {
        // initialize default variables
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort

        let count = 0
        let conditionString = ''
        let orderString = ''

        // if has sort order
        if(sort != null){
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Get filter condition for facilityCode, facilityType, and facilityClass
        if (params.facilityType !== undefined || params.facilityClass !== undefined || params.facilityCode !== undefined) {
            let parameters = []

            if (params.facilityCode !== undefined) parameters['facilityCode'] = params.facilityCode
            if (params.facilityType !== undefined) parameters['facilityType'] = params.facilityType
            if (params.facilityClass !== undefined) parameters['facilityClass'] = params.facilityClass

            conditionString = await processQueryHelper.getFilter(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // If facilityCode is a parameter, verify if the code exists
        if (params.facilityCode !== undefined) {
            let facilityCode = params.facilityCode

            // check if has taxonomy record with taxonId
            let checkRecordQuery = `
                SELECT
                    COUNT(1)
                FROM
                    place.facility facility
                WHERE
                    facility.is_void = FALSE AND
                    facility.facility_code = $$${facilityCode}$$
            `

            let checkRecordCount = await sequelize.query(checkRecordQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
                .catch(async err =>{
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if(checkRecordCount == undefined){
                return
            }

            if(checkRecordCount === null || checkRecordCount.length < 1){
                let errMsg = 'The facility record being retrieved does not exist.'
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Main retrieval query
        let sourceQuery = `
        SELECT
            facility.id AS "facilityDbId",
            facility.facility_code AS "facilityCode",
            facility.facility_name AS "facilityName",
            facility.facility_class AS "facilityClass",
            facility.facility_type AS "facilityType",
            facility.geospatial_coordinates AS "facilityGeospatialCoordinates",
            go.id AS "geospatialObjectDbId",
            go.geospatial_object_code AS "geospatialObjectCode",
            go.geospatial_object_name AS "geospatialObjectName",
            go.geospatial_object_type AS "geospatialObjectType",
            go.geospatial_object_subtype AS "geospatialObjectSubtype",
            parent_facility.id AS "parentFacilityDbId",
            parent_facility.facility_code AS "parentFacilityCode",
            parent_facility.facility_name AS "parentFacilityName",
            parent_facility.facility_class AS "parentFacilityClass",
            parent_facility.facility_type AS "parentFacilityType",
            root_facility.id AS "rootFacilityDbId",
            root_facility.facility_code AS "rootFacilityCode",
            root_facility.facility_name AS "rootFacilityName",
            root_facility.facility_class AS "rootFacilityClass",
            root_facility.facility_type AS "rootFacilityType",
            creator.id AS "creatorDbId",
            creator.person_name AS "creatorName",
            modifier.id AS "modifierDbId",
            modifier.person_name AS "modifierName"
        FROM
            place.facility facility
        LEFT JOIN
            place.facility parent_facility ON parent_facility.id = facility.parent_facility_id AND parent_facility.is_void = FALSE
        LEFT JOIN
            place.facility root_facility ON root_facility.id = facility.root_facility_id AND root_facility.is_void = FALSE
        LEFT JOIN
            place.geospatial_object go ON go.id = facility.geospatial_object_id AND go.is_void = FALSE
        LEFT JOIN
            tenant.person creator ON creator.id = facility.creator_id
        LEFT JOIN
            tenant.person modifier ON modifier.id = facility.modifier_id
        WHERE
            facility.is_void = FALSE
        `

        // Build final query
        let facilitiesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            sourceQuery,
            conditionString,
            orderString
        )

        // Retrieve the records from the database
        let facilities = await sequelize.query(facilitiesFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return            
        })

        if(await facilities == undefined || await facilities.length == 0){
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }
        else{
            // Build final count query
            facilitiesCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(sourceQuery, conditionString, orderString)

            facilitiesCount = await sequelize.query(facilitiesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if(await facilitiesCount == undefined || await facilitiesCount.length == 0){
                return
            }
            count = facilitiesCount[0].count
        }

        res.send(200, {
            rows: facilities,
            count: count
        })
        return   
    },

    post: async (req, res, next) => {
        let type = 0;
        let placeDbId = null
        let name = null
        let otherName = null
        let facilityType = null
        let longitude = null
        let latitude = null
        let remarks = null
        let parentFacilityDbId = null
        let parentFacilityType = null
        let level = null
        let instituteDbId = null
        let rootDbId = null
        let resultArray = []

        // Verify if user is an admin
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!await userValidator.isAdmin(userDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401016)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let facilityUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/facilities'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            // Records = [{}...]
            records = data.records
            recordCount = records.length
            if (data.records == undefined || data.records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let facilityValuesArray = []
            let visitedArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Check whether it is a facility or a sub-facility
                if (!record.type) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400144)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                // 1 for facility; 2 for sub-facility
                let type = record.type

                if (type == 1) {
                    if (!record.name || !record.placeDbId) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400145)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                } else {
                    if (!record.name || !record.placeDbId || !record.parentFacilityDbId || !record.parentFacilityType) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400146)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.placeDbId != undefined) {
                    placeDbId = record.placeDbId
                    if (isNaN(placeDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400147)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    // Validate if the placeDbId provided exists in the DB
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if the place exists, return 1
                            SELECT
                                count(1)
                            FROM
                                master.place place
                            WHERE
                                place.is_void = FALSE AND
                                place.id = ${placeDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.facilityType != undefined) {
                    facilityType = record.facilityType
                    // Validate if facilityType exists
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                            (
                                --- Check if the facility type exists, return 1
                                SELECT
                                    count(1)
                                FROM
                                    master.facility_type facilityType
                                WHERE
                                    facilityType.is_void = FALSE AND
                                    facilityType.name ILIKE '${facilityType}'
                            )
                        `
                    validateCount += 1

                }

                if (record.latitude != undefined) {
                    latitude = record.latitude
                    // Validate if latitude is a number
                    if (isNaN(latitude)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400148)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.longitude != undefined) {
                    longitude = record.longitude
                    // Validate if longitude is a number
                    if (isNaN(longitude)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400149)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.instituteDbId != undefined) {
                    instituteDbId = record.instituteDbId
                    if (isNaN(instituteDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400150)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    // Validate if institute exists in the database
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            ---Check if institute exists, return 1
                            SELECT
                                count(1)
                            FROM
                                master.institute institute
                            WHERE
                                institute.is_void = FALSE AND
                                institute.id = ${instituteDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.parentFacilityDbId != undefined) {
                    parentFacilityDbId = record.parentFacilityDbId
                    parentFacilityType = record.parentFacilityType
                    if (isNaN(parentFacilityDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400151)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    // Validate if it exists in the database
                    // Compare parentFacilityType
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            ---Check if facility exists, and check if type matches return 1
                            SELECT
                                count(1)
                            FROM
                                master.facility facility
                            WHERE
                                facility.is_void = FALSE AND
                                facility.id = ${parentFacilityDbId} AND
                                facility.facility_type ILIKE '${parentFacilityType}'
                        )
                    `
                    validateCount += 1
                }

                if (record.level != undefined) {
                    level = record.level
                    if (isNaN(level)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400152)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.rootDbId != undefined) {
                    rootDbId = record.rootDbId
                    if (isNaN(rootDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400153)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                    // Validate if rootDbId provided exists in the database
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            ---Check if root facility exists, return 1
                            SELECT
                                count(1)
                            FROM
                                master.facility facility
                            WHERE
                                facility.is_void = FALSE AND
                                facility.id = ${rootDbId}
                        )
                    `
                    validateCount += 1
                }

                name = (record.name != undefined) ? record.name : null
                otherName = (record.otherName != undefined) ? record.otherName : null
                remarks = (record.remarks != undefined) ? record.remarks : null

                // Validate the filters in DB; Count must be equal to validateCount
                let checkInputQuery = `SELECT ${validateQuery} AS count`
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (recordCount > 1) {
                    // Check if name, otherName, QR code is unique
                    let facilityInfo = facilityQrCode
                    if (visitedArray.includes(facilityInfo)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400154)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    } else {
                        visitedArray.push(facilityInfo)
                    }
                }

                let tempArray = [
                    placeDbId, instituteDbId, name, facilityType, latitude, longitude, otherName, remarks, 'Created via CB API', level, parentFacilityDbId
                ]

                facilityValuesArray.push(tempArray)
            }


            // Create facility record
            let facilityQuery = format(`
                INSERT INTO
                    master.facility
                        (
                            place_id, institute_id, name, facility_type, latitude, longitude, other_name, remarks, notes, level, parent_id
                        )
                VALUES
                    %L
                RETURNING id`, facilityValuesArray
            )

            facilityQuery = facilityQuery.trim()

            let facilities = await sequelize.query(facilityQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            for (let facility of facilities[0]) {
                facilityDbId = facility.id
                // Return the experiment block info
                let array = {
                    facilityDbId: facilityDbId,
                    recordCount: 1,
                    href: facilityUrlString + '/' + facilityDbId
                }
                resultArray.push(array)
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}