/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

    // Implementation of GET call for /v3/families/:id/members
    get: async function (req, res, next) {
        // retrieve the family Id
        let familyDbId = req.params.id

        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let conditionString = ``
        let orderString = ``
        let parameters = []

        if (!validator.isInt(familyDbId)) {
            let errMsg = "Invalid format, family ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build family query
        let familyQuery = `
            SELECT
                family.id AS "familyDbId",
                family.family_name AS "familyName",
                family.family_code AS "familyCode",
                family.creator_id AS "creatorDbId",
                "germplasmCross".id AS "crossDbId",
                "germplasmCross".cross_name AS "crossName",
                "germplasmCross".cross_method AS "crossMethod",
                creator.person_name AS "creator",
                family.creation_timestamp AS "creationTimestamp",
                family.modifier_id AS "modifierDbId",
                modifier.person_name AS "modifier",
                family.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.family family
            LEFT JOIN
                germplasm.cross "germplasmCross" on "germplasmCross".id = family.cross_id
            LEFT JOIN
                tenant.person creator ON creator.id = family.creator_id
            LEFT JOIN
                tenant.person modifier ON modifier.id = family.modifier_id
            WHERE
                family.is_void = FALSE
                AND family.id = ${familyDbId}
        `

        // Retrieve family record
        let family = await sequelize.query(familyQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await family == undefined) {
            return
        }
        else if (await family == null || await family.length < 1) {
            let errMsg = "The family you have requested does not exist"
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Build family members query
        let familyMembersQuery = `
            SELECT
                family_member.id AS "familyMemberDbId",
                family_member.order_number AS "familyMemberNumber",
                family_member.germplasm_id AS "germplasmDbId",
                germplasm.designation AS "designation",
                germplasm.germplasm_code AS "germplasmCode"
            FROM
                germplasm.family_member family_member
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = family_member.germplasm_id
                    AND germplasm.is_void = FALSE
            WHERE
                family_member.is_void = FALSE
                AND family_member.family_id = ${familyDbId}
            ORDER BY
                family_member.order_number ASC
        `

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
    
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query
        let familyMembersFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            familyMembersQuery,
            conditionString,
            orderString
        )

        // Retrieve family members from the database
        let familyMembers = await sequelize.query(familyMembersFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
            limit: limit,
            offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })
    
        if (await familyMembers == undefined) {
            return
        }
        else if (await familyMembers == null || await familyMembers.length < 1) {
            family[0].familyMembers = []
        }
        else {
            // Get count
            let familyMembersCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(familyMembersQuery, conditionString, orderString)
    
            let familyMembersCount = await sequelize.query(familyMembersCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            if (await familyMembersCount == undefined) {
                return
            }
            else if (await familyMembersCount == null || await familyMembersCount.length < 1) {
                familyMembersCount = [
                    {count: 0}
                ]
            }
            
            family[0].familyMembers = familyMembers
            count = familyMembersCount[0].count
        }

        res.send(200, {
            rows: family,
            count: count
        })
        return
    }
}