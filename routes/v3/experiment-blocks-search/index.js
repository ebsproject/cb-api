/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let addedOrderString = `ORDER BY "experimentBlock".id`
        let parameters = {}
        parameters['distinctOn'] = ''

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }
            // Set the columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]
            // Get filter condition
            conditionString = await processQueryHelper
                .getFilter(
                    req.body,
                    excludedParametersArray,
                )

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])
                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                        ${parameters['distinctOn']}
                `
            }
        }
        // Build the base retrieval query
        let experimentBlocksQuery = null
        // Check if the client specified values for fields
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(parameters['fields'])
                
                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)
                experimentBlocksQuery = knex.select(selectString)
            } else {
                experimentBlocksQuery = knex.column(parameters['fields'].split('|'))
            }

            experimentBlocksQuery += `
                FROM
                    experiment.experiment_block "experimentBlock"
                LEFT JOIN
                    experiment.experiment experiment ON "experimentBlock".experiment_id = experiment.id
                LEFT JOIN
                    experiment.experiment_block "parentExperimentBlock" ON "experimentBlock".parent_experiment_block_id = "parentExperimentBlock".id
                LEFT JOIN
                    tenant.person creator ON "experimentBlock".creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON "experimentBlock".modifier_id = modifier.id
                WHERE
                    "experimentBlock".is_void = FALSE
                ${addedOrderString}
            `
        } else {
            experimentBlocksQuery = `
                SELECT
                    "experimentBlock".id AS "experimentBlockDbId",
                    "experimentBlock".experiment_block_code AS "experimentBlockCode",
                    "experimentBlock".experiment_block_name AS "experimentBlockName",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_code AS "experimentCode",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_type AS "experimentType",
                    experiment.experiment_year AS "experimentYear",
                    "parentExperimentBlock".id AS "parentExperimentBlockDbId",
                    "parentExperimentBlock".experiment_block_code AS "parentExperimentBlockCode",
                    "parentExperimentBlock".experiment_block_name AS "parentExperimentBlockName",
                    "experimentBlock".order_number AS "orderNumber",
                    "experimentBlock".block_type AS "blockType",
                    "experimentBlock".no_of_blocks AS "noOfBlocks",
                    "experimentBlock".no_of_ranges AS "noOfRanges",
                    "experimentBlock".no_of_cols AS "noOfCols",
                    "experimentBlock".no_of_reps AS "noOfReps",
                    "experimentBlock".plot_numbering_order AS "plotNumberingOrder",
                    "experimentBlock".starting_corner AS "startingCorner",
                    "experimentBlock".entry_list_id_list AS "entryListIdList",
                    "experimentBlock".layout_order_data AS "layoutOrderData",
                    "experimentBlock".notes,
                    "experimentBlock".creation_timestamp AS "creationTimestamp",
                    "experimentBlock".remarks,
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    "experimentBlock".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier",
                    CASE WHEN (
                        "experimentBlock".entry_list_id_list IS NULL OR
                        "experimentBlock".entry_list_id_list = '{}'
                    ) THEN 0
                        ELSE jsonb_array_length("experimentBlock".entry_list_id_list)
                    END AS "entryCount",
                    CASE WHEN (
                        "experimentBlock".entry_list_id_list IS NULL OR
                        "experimentBlock".entry_list_id_list = '{}'
                    ) THEN 0
                        ELSE
                        (
                            SELECT sum(cast(value->>'repno' as integer))
                            FROM experiment.experiment_block
                            CROSS JOIN jsonb_array_elements(entry_list_id_list)
                            WHERE id = "experimentBlock".id and is_void = FALSE
                        )
                    END AS "plotCount",
                    DENSE_RANK () OVER (ORDER BY "experimentBlock".parent_experiment_block_id) AS rowGroup,
                    "experimentBlock".is_active AS "isActive"
                FROM
                    experiment.experiment_block "experimentBlock"
                LEFT JOIN
                    experiment.experiment experiment ON "experimentBlock".experiment_id = experiment.id
                LEFT JOIN
                    experiment.experiment_block "parentExperimentBlock" ON "experimentBlock".parent_experiment_block_id = "parentExperimentBlock".id
                LEFT JOIN
                    tenant.person creator ON "experimentBlock".creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON "experimentBlock".modifier_id = modifier.id
                WHERE
                    "experimentBlock".is_void = FALSE
                ${addedOrderString}
            `
        }
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
     
        let experimentBlocksFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                experimentBlocksQuery,
                conditionString,
                orderString,
            )

    
        let experimentBlocks = await sequelize
            .query(experimentBlocksFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await experimentBlocks == undefined ||
            await experimentBlocks.length < 1
        ) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let experimentBlocksCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                experimentBlocksQuery,
                conditionString,
                orderString,
            )

        let experimentBlocksCount = await sequelize
            .query(experimentBlocksCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
    
        count = experimentBlocksCount[0].count

        res.send(200, {
            rows: experimentBlocks,
            count: count
        })
        return
    }
}