/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let distinctString = ''
        let fields = null
        let distinctOn = ''
        let orderQuery = ''

        // Check if the user provided search filters
        if (req.body != undefined) {
            // Set columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            if (req.body.fields != null) {
                fields = req.body.fields
            }

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                distinctOn = req.body.distinctOn
                distinctString = await processQueryHelper
                    .getDistinctString(distinctOn)
                distinctOn = `"${distinctOn}"`

                orderQuery = `
                    ORDER BY ${distinctOn}
                `
            }
        }
        
        let plantingJobOccurrenceQuery = null

        let fromQuery = `
            FROM
                experiment.planting_job_occurrence "plantingJobOccurrence"
            LEFT JOIN
                experiment.planting_job ON planting_job.id = "plantingJobOccurrence".planting_job_id
            LEFT JOIN
                experiment.occurrence ON occurrence.id = "plantingJobOccurrence".occurrence_id
            LEFT JOIN
                experiment.occurrence_data occurrence_plot_length ON occurrence_plot_length.occurrence_id = occurrence.id
                AND occurrence_plot_length.variable_id in (
                    SELECT
                        id
                    FROM
                        master.variable
                    WHERE 
                        abbrev ilike '%PLOT_LN%'
                )
                AND occurrence_plot_length.is_void = FALSE
            LEFT JOIN
                experiment.occurrence_data occurrence_plot_width ON occurrence_plot_width.occurrence_id = occurrence.id AND
                occurrence_plot_width.variable_id in (
                    SELECT
                        id 
                    FROM
                        master.variable
                    WHERE 
                        abbrev ilike '%PLOT_WIDTH%'
                ) AND occurrence_plot_width.is_void = FALSE
            LEFT JOIN
                experiment.experiment ON experiment.id = occurrence.experiment_id
            LEFT JOIN
                place.geospatial_object site ON site.id = occurrence.site_id
            LEFT JOIN
                tenant.person creator ON "plantingJobOccurrence".creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON "plantingJobOccurrence".modifier_id = modifier.id
            LEFT JOIN
                experiment.experiment_data plot_length ON plot_length.experiment_id = experiment.id 
                AND plot_length.variable_id in (
                    SELECT
                        id
                    FROM
                        master.variable
                    WHERE abbrev ilike '%PLOT_LN%'
                )
                AND plot_length.is_void = FALSE
            LEFT JOIN
                experiment.experiment_data plot_width ON plot_width.experiment_id = experiment.id AND
                plot_width.variable_id in (
                    SELECT
                        id 
                    FROM
                        master.variable
                    WHERE abbrev ilike '%PLOT_WIDTH%'
                ) AND plot_width.is_void = FALSE,
                (
                    SELECT
                        s.unit
                    FROM
                        master.variable v 
                    LEFT JOIN master.scale s ON s.id = v.scale_id WHERE v.abbrev='PLOT_WIDTH') plot_width_unit,
                (
                    SELECT
                        s.unit
                    FROM
                        master.variable v
                    LEFT JOIN master.scale s ON s.id = v.scale_id WHERE v.abbrev='PLOT_LN') plot_length_unit
            WHERE
                "plantingJobOccurrence".is_void = FALSE
                ${orderQuery}
        `

        if (fields != null) {
            if (distinctOn) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(fields)
                let selectString = knex.raw(`${fieldsString}`)
                plantingJobOccurrenceQuery = knex.select(selectString)
            } else {
                plantingJobOccurrenceQuery = knex.column(fields.split('|'))
            }

            plantingJobOccurrenceQuery += fromQuery
        } else {
            plantingJobOccurrenceQuery = `
                SELECT 
                    "plantingJobOccurrence".id AS "plantingJobOccurrenceDbId",
                    planting_job.id AS "plantingJobDbId",
                    planting_job.planting_job_code AS "plantingJobCode",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_code AS "experimentCode",
                    experiment.experiment_name AS "experimentName",
                    site.id AS "siteDbId",
                    site.geospatial_object_code AS "siteCode",
                    site.geospatial_object_name AS "siteName",
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_code AS "occurrenceCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    (
                        SELECT
                            count(e.id)
                        FROM
                            experiment.entry_list el,
                            experiment.entry e
                        WHERE
                            el.experiment_id = occurrence.experiment_id AND
                            e.entry_list_id = el.id AND
                            e.is_void = FALSE
                    )::int AS "occurrenceEntryCount",
                    (
                        SELECT
                            count(p.id)
                        FROM
                            experiment.plot p
                        WHERE
                            p.occurrence_id = occurrence.id AND
                            p.is_void = FALSE
                    )::int AS "occurrencePlotCount",
                    "plantingJobOccurrence".seeds_per_envelope AS "seedsPerEnvelope",
                    CASE WHEN
                        occurrence_plot_length.data_value IS NULL AND
                        occurrence_plot_width.data_value IS NULL AND
                        plot_length.data_value IS NULL AND
                        plot_width.data_value IS NULL
                    THEN
                        NULL
                    WHEN
                        occurrence_plot_length.data_value IS NOT NULL AND
                        occurrence_plot_width.data_value IS NOT NULL
                    THEN
                        CONCAT(
                            COALESCE(occurrence_plot_length.data_value, '0'),
                            COALESCE(plot_length_unit.unit),
                            ' x ',
                            COALESCE(occurrence_plot_width.data_value, '0'),
                            COALESCE(plot_width_unit.unit)
                        )
                    WHEN
                        plot_length.data_value IS NULL AND
                        plot_width.data_value IS NULL
                    THEN
                        NULL
                    ELSE
                        CONCAT(
                            COALESCE(plot_length.data_value, '0'),
                            COALESCE(plot_length_unit.unit),
                            ' x ',
                            COALESCE(plot_width.data_value, '0'),
                            COALESCE(plot_width_unit.unit)
                        )
                    END "plotDimensions",
                    "plantingJobOccurrence".package_unit AS "packageUnit",
                    "plantingJobOccurrence".envelopes_per_plot AS "envelopesPerPlot",
                    "plantingJobOccurrence".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    "plantingJobOccurrence".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier",
                    "plantingJobOccurrence".notes
                ${fromQuery}
            `
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let plantingJobOccurrenceFinalSqlQuery = await processQueryHelper
            .getFinalSqlQuery(
                plantingJobOccurrenceQuery,
                conditionString,
                orderString,
                distinctString
            )

        // Retrieve planting job from the database
        let plantingJobOccurrences = await sequelize
            .query(plantingJobOccurrenceFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJobOccurrences == undefined ||
            await plantingJobOccurrences.length < 1
        ) {
            res.send(200, {
                rows: [],
                count: 0
            })

            return
        }
        // }

        let plantingJobOccurrencesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                plantingJobOccurrenceQuery,
                conditionString,
                orderString,
                distinctString
            )

        let plantingJobOccurrenceCount = await sequelize
            .query(
                plantingJobOccurrencesCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = plantingJobOccurrenceCount[0].count

        res.send(200, {
            rows: plantingJobOccurrences,
            count: count
        })

        return
    }
}