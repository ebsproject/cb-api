/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let validator = require('validator')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {
  post: async (req, res, next) => {
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let resultArray = []
    let allowedColumns = ['studyDbIds']

    if (req.body == null) {
      // let errMsg = await errorBuilder.getError(req.headers.host, 400177)
      let errMsg = 'Invalid request. Provide studyDbIds parameter.'
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let requestBody = req.body
    for (column in requestBody) {
      if (!allowedColumns.includes(column)) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400142)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    let studyDbIds = req.body.studyDbIds
    studyDbIds = studyDbIds.split('|')

    if (studyDbIds.length > 0) {
      for (studyDbId of studyDbIds) {
        studyDbId = studyDbId.trim()
      
        // Check if ID is valid and existing
        if (!validator.isInt(studyDbId)) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400065)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        let studyRecordQuery = `
          SELECT study.id
          FROM operational.study study
          WHERE
            study.is_void = FALSE
            AND study.id = ${studyDbId}
        `
          
        // Validate if study ID exists in the database
        let studyRecord = await sequelize
          .query(studyRecordQuery, {
            type: sequelize.QueryTypes.SELECT
          })
          .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
          })

        if (studyRecord == undefined || studyRecord.length < 1) {
          let errMsg = await errorBuilder.getError(req.headers.host, 404002)
          res.send(new errors.NotFoundError(errMsg))
          return
        }

        // build string query for dynamic columns
        let dynamicColumnsString = ``

        // Retrieve all plot data variables under a study
        let plotDataVariablesQuery = `
          SELECT 
            variable.id,
            variable.abbrev
          FROM
            master.variable variable
          WHERE
            variable.is_void = FALSE
            AND variable.abbrev != 'MAP_X'
            AND variable.abbrev != 'MAP_Y'
            AND variable.id IN (
              SELECT id
              FROM (
                SELECT 
                  DISTINCT plotData.variable_id AS id
                FROM 
                  operational.plot_data plotData
                WHERE
                  plotData.study_id = ${studyDbId}
                  AND plotData.is_void = FALSE
              ) AS variable_ids
            )
        `

        let plotDataVariables = await sequelize
          .query(plotDataVariablesQuery, {
            type: sequelize.QueryTypes.SELECT
          })
          .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
          })

        // return empty if plot data variables do not exist
        if (plotDataVariables != undefined || plotDataVariables.length > 0) {
          for (plotDataVariable of plotDataVariables) {
            let plotDataVariableDbId = plotDataVariable.id
            let plotDataVariableAbbrev = plotDataVariable.abbrev

            let columnString = `
              (
                SELECT
                  plotData.value
                FROM
                  operational.plot_data plotData
                WHERE
                  plotData.is_void = FALSE
                  AND plotData.plot_id = plot.id
                  AND plotData.variable_id = ${plotDataVariableDbId}
                LIMIT 1
              ) AS ${plotDataVariableAbbrev}
            `

            dynamicColumnsString += (dynamicColumnsString != ``) ? `,` : ``
            dynamicColumnsString += columnString
          }
        }

        // Retrieve all plot metadata variables under a study
        let plotMetadataVariablesQuery = `
          SELECT 
            variable.id,
            variable.abbrev
          FROM
            master.variable variable
          WHERE
            variable.is_void = FALSE
            AND variable.abbrev != 'MAP_X'
            AND variable.abbrev != 'MAP_Y'
            AND variable.id IN (
              SELECT id
              FROM (
                SELECT 
                  DISTINCT plotMetadata.variable_id AS id
                FROM 
                  operational.plot_metadata plotMetadata
                WHERE
                  plotMetadata.study_id = ${studyDbId}
                  AND plotMetadata.is_void = FALSE
              ) AS variable_ids
            )
        `
        
        let plotMetadataVariables = await sequelize
          .query(plotMetadataVariablesQuery, {
            type: sequelize.QueryTypes.SELECT
          })
          .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
          })

        if (plotMetadataVariables != undefined || plotMetadataVariables.length > 0) {
          for (plotMetadataVariable of plotMetadataVariables) {
            let plotMetadataVariableDbId = plotMetadataVariable.id
            let plotMetadataVariableAbbrev = plotMetadataVariable.abbrev

            let columnString = `
              (
                SELECT
                  plotMetadata.value
                FROM
                  operational.plot_metadata plotMetadata
                WHERE
                  plotMetadata.is_void = FALSE
                  AND plotMetadata.plot_id = plot.id
                  AND plotMetadata.variable_id = ${plotMetadataVariableDbId}
                LIMIT 1
              ) AS ${plotMetadataVariableAbbrev}
            `

            dynamicColumnsString += (dynamicColumnsString != ``) ? `,` : ``
            dynamicColumnsString += columnString
          }
        }

        dynamicColumnsString = (dynamicColumnsString == ``) ? `` : `, ${dynamicColumnsString}`

        let plotReportsQuery = `
          SELECT 
            "study".id AS "studyDbId",
            "study".name AS "studyName",
            (
              SELECT 
                place.name 
              FROM 
                master.place place 
              WHERE 
                place.is_void = FALSE 
                AND "study".place_id = place.id 
              LIMIT 1
            ) AS "location",
            "study".study,
            "study".design,
            (
              SELECT 
                sm.value 
              FROM 
                operational.study_metadata sm 
              WHERE 
                sm.is_void = FALSE 
                AND sm.study_id = "study".id 
                AND sm.variable_id = 83
              LIMIT 1
            ) AS "dist_bet_rows",
            (
              SELECT 
                sm.value 
              FROM 
                operational.study_metadata sm 
              WHERE 
                sm.is_void = FALSE 
                AND sm.study_id = "study".id 
                AND sm.variable_id = 188 
              LIMIT 1
            ) AS "rows_per_plot_cont",
            (
              SELECT 
                sm.value 
              FROM 
                operational.study_metadata sm 
              WHERE 
                sm.is_void = FALSE 
                AND sm.study_id = "study".id 
                AND sm.variable_id = 380 
              LIMIT 1
            ) AS "hills_per_row_cont",
            (
              SELECT 
                sm.value 
              FROM 
                operational.study_metadata sm 
              WHERE 
                sm.is_void = FALSE 
                AND sm.study_id = "study".id 
                AND sm.variable_id = 381 
              LIMIT 1
            ) AS "dist_bet_hills",
            (
              SELECT 
                sm.value 
              FROM 
                operational.study_metadata sm 
              WHERE 
                sm.is_void = FALSE 
                AND sm.study_id = "study".id 
                AND sm.variable_id = 382 
              LIMIT 1
            ) AS "plot_area_sqm_cont",
            (
              SELECT 
                sm.value 
              FROM 
                operational.study_metadata sm 
              WHERE 
                sm.is_void = FALSE 
                AND sm.study_id = "study".id 
                AND sm.variable_id = 383 
              LIMIT 1
            ) AS "plot_area_ha_cont",
            "entry".product_id,
            "entry".id AS "entry_id",
            "entry".entno AS "entry_no",
            "entry".entcode AS "entry_code",
            (
              SELECT 
                product.designation 
              FROM
                master.product product 
              WHERE 
                product.is_void = FALSE 
                AND product.id = "entry".product_id 
              LIMIT 1
            ) AS "product_preferred_name",
            "entry".product_name AS "designation",
            "entry".product_gid AS "gid", 
            (
              SELECT 
                pn.value 
              FROM 
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='line_name' 
              LIMIT 1
            ) AS "line_name", 
            (
              SELECT
                pn.value 
              FROM
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='elite_lines' 
              LIMIT 1
            ) AS "elite_lines", 
            (
              SELECT 
                pn.value 
              FROM 
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='derivative_name' 
              LIMIT 1
            ) AS "derivative_name",
            (
              SELECT
                pn.value 
              FROM
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='alternative_cultivar_name' 
              LIMIT 1
            ) AS "alternative_cultivar_name", 
            (
              SELECT 
                pn.value 
              FROM 
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='cultivar_name' 
              LIMIT 1
            ) AS "cultivar_name", 
            (
              SELECT 
                pn.value 
              FROM 
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='international_testing_number'
              LIMIT 1
            ) AS "international_testing_number", 
            (
              SELECT 
                pn.value
              FROM 
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='local_common_name' 
              LIMIT 1
            ) AS "local_common_name",
            (
              SELECT 
                pn.value 
              FROM 
                master.product_name pn 
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='release_name' 
              LIMIT 1
            ) AS "release_name", 
            (
              SELECT 
                pn.value 
              FROM 
                master.product_name pn
              WHERE 
                pn.is_void = FALSE 
                AND "entry".product_id = pn.product_id 
                AND pn.name_type='product_preferred_name' 
              LIMIT 1
            ) AS "product_preferred_name",
            "plot".id AS "plot_id",
            "plot".plotno,
            "plot".code AS "plot_code",
            "plot".rep,
            "plot".design_y,
            "plot".design_x,
            "plot".map_x,
            "plot".map_y 
          -- dynamic columns
            ${dynamicColumnsString}
          -- main FROM clause
          FROM
            operational.study "study",
            operational.entry "entry",
            operational.plot "plot"
          WHERE 
            "study".id = ${studyDbId}
            AND "plot".is_void = FALSE
            AND "study".is_void = FALSE
            AND "entry".is_void = FALSE
            AND "entry".study_id = "study".id
            AND "entry".id = "plot".entry_id  
          ORDER BY 
            "study".year DESC, 
            "study".season_id DESC, 
            "study".id ASC, 
            "plot".plotno
          LIMIT 1000
        `
        
        // Parse the sort parameters 
        if (sort != null) {
          orderString = await processQueryHelper.getOrderString(sort)
          if (orderString.includes('invalid')) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400004)
            res.send(new errors.BadRequestError(errMsg))
            return
          }
        }

        // Generate the final SQL query
        plotReportsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
          plotReportsQuery, 
          conditionString, 
          orderString
        )

        // Retrieve the plot reports
        let plotReports = await sequelize
          .query(plotReportsFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
              limit: limit,
              offset: offset
            }
          })
          .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
          })
        
        if (plotReports == undefined || plotReports.length < 1) {
          res.send(200, {
            rows: plotReports,
            count: 0
          })
          return
        } 
        else {
          // Get final count of the plot reports retrieved
          let plotReportsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            plotReportsQuery, 
            conditionString, 
            orderString
          )

          let plotReportsCount = await sequelize
            .query(plotReportsCountFinalSqlQuery, {
              type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
              let errMsg = await errorBuilder.getError(req.headers.host, 500004)
              res.send(new errors.InternalError(errMsg))
              return
            })

          resultArray.push(plotReports)    
          count += parseInt(plotReportsCount[0].count)
        }  
      }
    } else {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    res.send(200, {
      rows: resultArray,
      count: count
    })
    return
  }
}