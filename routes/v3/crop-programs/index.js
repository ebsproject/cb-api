/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let userValidator = require('../../../helpers/person/validator.js')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let logger = require('../../../helpers/logger')

module.exports = {

    // Endpoint for creating a new crop program record
    // POST /v3/crop-programs
    post: async function (req, res, next) {
        // Set defaults
        let resultArray = []

        let cropProgramDbId = null

        // Get person ID from token
        let personDbId = await tokenHelper.getUserId(req)

         if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        //check if user is Admin
        let isAdmin = await userValidator.isAdmin(personDbId)

         if (!isAdmin) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401028)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure

        // Set URL for response
        let cropProgramUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/crop-programs'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            records = data.records

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let cropProgramValuesArray = []

            for (var record of records) {
                let validateQuery = ``
                let validateCount = 0

                // Set defaults
                let cropProgramCode = null
                let cropProgramName = null
                let organizationDbId = null
                let cropDbId = null
                let description = null

                // Check if required columns are in the request body
                if (
                    !record.cropProgramCode || !record.cropProgramName ||
                    !record.organizationDbId || !record.cropDbId 
                ) {
                    let errMsg = `Required parameters are missing. Ensure that cropProgramCode, cropProgramName, organizationDbId, and cropDbId fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                if (record.organizationDbId !== undefined) {
                    organizationDbId = record.organizationDbId

                    if (!validator.isInt(organizationDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400051)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate organization ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if organization is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.organization organization
                        WHERE 
                            organization.is_void = FALSE AND
                            organization.id = ${organizationDbId}
                        )
                    `
                    validateCount += 1
                }

                if (record.cropDbId !== undefined) {
                    cropDbId = record.cropDbId

                    if (!validator.isInt(cropDbId)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400051)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    // Validate crop ID
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        --- Check if crop is existing return 1
                        SELECT 
                            count(1)
                        FROM 
                            tenant.crop crop
                        WHERE 
                            crop.is_void = FALSE AND
                            crop.id = ${cropDbId}
                        )
                    `
                    validateCount += 1
                }

                /** Validation of input in database */
                // count must be equal to validateCount if all conditions are met
                let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }


                cropProgramCode = record.cropProgramCode
                cropProgramName = record.cropProgramName
                organizationDbId = record.organizationDbId
                cropDbId = record.cropDbId
                description = (record.description !== undefined) ? record.description : null

                // get values
                let tempArray = [
                    cropProgramCode, cropProgramName, organizationDbId, cropDbId, description, personDbId
                ]

                cropProgramValuesArray.push(tempArray)
            }

            // Create cropProgram record
            let cropProgramsQuery = format(`
                INSERT INTO
                    tenant.crop_program (
                        crop_program_code, crop_program_name, organization_id, crop_id, description,creator_id
                    )
                VALUES
                    %L
                RETURNING id`, cropProgramValuesArray
            )

            let cropPrograms = await sequelize.query(cropProgramsQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })


            for (cropProgram of cropPrograms[0]) {
                cropProgramDbId = cropProgram.id

                let cropProgramRecord = {
                    cropProgramDbId: cropProgramDbId,
                    recordCount: 1,
                    href: cropProgramUrlString + '/' + cropProgramDbId
                }

                resultArray.push(cropProgramRecord)
            }

            await transaction.commit()

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, 'POST crop_programs: ' + JSON.stringify(err), 'error')

            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}