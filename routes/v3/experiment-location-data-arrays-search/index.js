/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // Implementation of advanced serch functionality for experiment location data array
    post: async function (req, res, next) {

        // Set defaults 

        let experimentLocationIdString = ''

        let traitDbIdString = ''

        let parameters = {}

        parameters['experimentLocationDbId'] = null

        parameters['traitDbId'] = null

        if (req.body != null) {
            // Retrieve parameters

            if (req.body.traitDbId != null) {
                parameters['traitDbId'] = req.body.traitDbId
            }

            if (req.body.experimentLocationDbId != null) {
                parameters['experimentLocationDbId'] = req.body.experimentLocationDbId
            } else {
                let errMsg = await errorBuilder.getError(req.headers.host, 400112)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }  else {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (parameters['experimentLocationDbId'] != null) {

            let experimentLocationIds = parameters['experimentLocationDbId']

            // Parse the value
            for (experimentLocationId of experimentLocationIds.split('|')) {
                if (experimentLocationIdString != '') {
                    experimentLocationIdString += ','
                }
                experimentLocationIdString += experimentLocationId
            }
        }

        if (parameters['traitDbId'] != null) {

            let traitDbIds = parameters['traitDbId']

            // Parse the value
            for (traitDbId of traitDbIds.split('|')) {
                if (traitDbIdString != '') {
                    traitDbIdString += ','
                }
                traitDbIdString += traitDbId
            }
        }

        // Initialize 
        let experimentLocationDataArray = {}

        // Set values for the phenotypic data
        
        let measurementArray = {}

        // Set the measurement array headers
        let measurementArrayHeaders = [
            "locationRepId", "plotId", "traitId", "traitValue", "timestamp"
        ]

        measurementArray["headers"] = measurementArrayHeaders

        // Retrieve the phenotypic data of the given experiment location
        
        // Set query 
        let phenotypicDataConditionString = ''

        if(experimentLocationIdString != '') {
            phenotypicDataConditionString += `
                AND "plotData".study_id IN (
                    ` + experimentLocationIdString + `
                )
            `
        }

        if(traitDbIdString != '') {
            phenotypicDataConditionString += `
                AND "plotData".variable_id IN (
                    ` + traitDbIdString + `
                )
            `
        }

        let phenotypicDataQuery = `
            SELECT
                "plotData".study_id AS "locationRepId", 
                "plotData".plot_id AS "plotId", 
                "plotData".variable_id AS "traitId", 
                "plotData".value AS "traitValue", 
                "plotData".creation_timestamp "timestamp"
            FROM
                operational.plot_data "plotData"
            WHERE 
                "plotData".is_void = FALSE
                ` + phenotypicDataConditionString + `
            ORDER BY
                "plotData".id
        `

        let phenotypicData = await sequelize.query(phenotypicDataQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        let phenotypicDataArray = []

        if (phenotypicData.length > 0) {
            for (data of phenotypicData) {
                // Set the values for phenotypic data
                let phenotypicDataInformation = [
                    data.locationRepId,
                    data.plotId,
                    data.traitId,
                    data.traitValue,
                    data.timestamp
                ]

                phenotypicDataArray.push(phenotypicDataInformation)
            }
        }

        measurementArray["data"] = phenotypicDataArray

        experimentLocationDataArray["measurementArray"] = measurementArray

        // Set values for the plot array

        let plotArray = {}

        // Set the plotArrayHeaders
        let plotArrayHeaders = [
            "experimentId", "locationRepId", "locationId", "subset", "plotId", "entryId", "fieldX", "fieldY", "paX", "paY", "rep", "block"
        ]

        plotArray["headers"] = plotArrayHeaders

        // Retrieve basic information of the experiment location

        // Set query 
        let plotConditionString = ''

        if(experimentLocationIdString != '') {
            plotConditionString += `
                AND plot.study_id IN (
                    ` + experimentLocationIdString + `
                )
            `
        }

        let plotAddedSourceString = ''
        if(traitDbIdString != '') {
            plotAddedSourceString = `
                LEFT JOIN operational.plot_data "plotData"
                    ON plot.id = "plotData".plot_id
                    AND "plotData".is_void = FALSE
            `

            plotConditionString += `
                AND "plotData".variable_id IN (
                    ` + traitDbIdString + `
                )
            `
        }

        let plotQuery = `
            SELECT
                "experimentLocation".experiment_id AS "experimentId", 
                "experimentLocation".id AS "locationRepId", 
                "experimentLocation".place_id AS "locationId",
                1 AS "subset", 
                plot.id AS "plotId", 
                plot.entry_id AS "entryId", 
                plot.field_x AS "fieldX", 
                plot.field_y AS "fieldY", 
                plot.map_x AS "paX", 
                plot.map_y AS "paY", 
                plot.rep, 
                plot.block_no AS "block"
            FROM
                operational.study "experimentLocation"
                LEFT JOIN
                    operational.plot plot
                    ON plot.study_id = "experimentLocation".id
                    AND plot.is_void = FALSE
                ` + plotAddedSourceString + `
            WHERE
                "experimentLocation".is_void = FALSE
                ` + plotConditionString + `
            ORDER BY
                plot.id
        `            

        let plots = await sequelize.query(plotQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        let plotData = []

        if (plots.length > 0) {
            for (plot of plots) {
                // Set the values for plots
                let plotInformation = [
                    plot.experimentId, 
                    plot.locationRepId,
                    plot.locationId,
                    plot.subset,
                    plot.plotId,
                    plot.entryId,
                    plot.fieldX,
                    plot.fieldY,
                    plot.paX,
                    plot.paY,
                    plot.rep,
                    plot.block
                ]

                plotData.push(plotInformation)
            }
        }

        plotArray["data"] = plotData

        experimentLocationDataArray["plotArray"] = plotArray

        // Retrieve the trait list

        // Set query 
        let traitListConditionString = ''

        if(experimentLocationIdString != '') {
            traitListConditionString += `
                AND "plotData".study_id IN (
                    ` + experimentLocationIdString + `
                )
            `
        }

        if(traitDbIdString != '') {
            traitListConditionString += `
                AND "plotData".variable_id IN (
                    ` + traitDbIdString + `
                )
            `
        }

        let traitListQuery = `
            SELECT
                DISTINCT ON (variable.id)
                variable.id AS "traitId",
                variable.abbrev,
                variable.name
            FROM
                master.variable variable
                JOIN operational.plot_data "plotData"
                ON "plotData".variable_id = variable.id
                AND "plotData".is_void = FALSE
            WHERE
                variable.is_void = FALSE
                ` + traitListConditionString + `
            ORDER BY
                variable.id

        `

        let traitLists = await sequelize.query(traitListQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        experimentLocationDataArray["traitList"] = traitLists

        res.send(200, {
            rows: experimentLocationDataArray
        })
        return
    }
}