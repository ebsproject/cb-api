/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')
let processQueryHelper = require('../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let logger = require('../../../helpers/logger')
const endpoint = 'configurations-search'

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let addedConditionString = ''
        let parameters = { 'distinctOn': '' }
        let excludedParametersArray = {}
        let tableAliases = {}
        let columnAliases = {}
        let responseColString = `
            config.id AS "configurationDbId",
            config.abbrev AS "configurationAbbrev",
            config.name AS "configurationName",
            config.config_value AS "configurationValue",
            config.rank AS "configurationRank",
            config.usage AS "configurationUsage",
            config.remarks,
            config.notes,
            config.creator_id AS "creatorDbId",
            creator.person_name AS "creator",
            config.creation_timestamp AS "creationTimestamp",
            config.modifier_id AS "modifierDbId",
            modifier.person_name AS "modifier",
            config.modification_timestamp AS "modificationTimestamp"
        `

        if (req.body !== undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            excludedParametersArray = ['fields', 'distinctOn']

            const conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ?  conditionStringArr.mainQuery : ''
            tableAliases = conditionStringArr.tableAliases
            columnAliases = conditionStringArr.columnAliases

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn

                sort = `"${parameters['distinctOn']}" configurationDbId`

                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }
        }

        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder
                    .getError(req.headers.host, 400057)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } else {
            orderString = `ORDER BY config.id`
        }

        let configsQuery = null
        if (parameters['fields']) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper
                    .getFieldValuesString(
                        parameters['fields'],
                    )

                let selectString
                    = knex.raw(`${addedDistinctString} ${fieldsString}`)
                configsQuery = knex.select(selectString)
            } else {
                configsQuery = knex.column(parameters['fields'].split('|'))
            }

            configsQuery += `
                FROM
                    platform.config config
                LEFT JOIN
                    tenant.person creator ON config.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON config.modifier_id = modifier.id
                WHERE
                    config.is_void = FALSE
                    ${addedConditionString}
                ${orderString}
            `
        } else {
            configsQuery = `
                SELECT
                    ${addedDistinctString}
                    ${responseColString}
                FROM
                    platform.config config
                LEFT JOIN
                    tenant.person creator ON config.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON config.modifier_id = modifier.id
                WHERE
                    config.is_void = FALSE
                ${orderString}
                `
        }

        // Generate the final SQL query
        let configFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            configsQuery,
            conditionString
        )

        // Retrieve experiments from the database
        let configs = await sequelize
            .query(configFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                return err
            })
        
        if (configs instanceof Error) {
            await logger.logFailingQuery(endpoint, 'SELECT', configs)
            
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)

            res.send(new errors.InternalError(errMsg))
            return
        }
    
        if (configs.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let tableJoins = {
            'creator': ' JOIN tenant.person creator ON config.creator_id = creator.id',
            'modifier':' LEFT JOIN tenant.person modifier ON config.modifier_id = modifier.id'
        }

        // retrieval of totalCount values will be processed separately
        let configCountFinalSqlQuery = await processQueryHelper
            .getTotalCountQuery(
                req.body,
                excludedParametersArray,
                conditionString, //filter string
                addedDistinctString, //distinct string used in the main query
                tableJoins, //join statements used in the resource
                'platform.config config',
                tableAliases, //table aliases involved in the join statements
                'config.id', //main column
                columnAliases, //columns used in the filter condition
                orderString,
                responseColString
            )

        let configCount = await sequelize
            .query(
                configCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                return err
            })

        if (configCount instanceof Error) {
            await logger.logFailingQuery(endpoint, 'SELECT', configCount)

            let errMsg = await errorBuilder.getError(req.headers.host, 500004)

            res.send(new errors.InternalError(errMsg))
            return
        }

        count = configCount[0].count

        res.send(200, {
            rows: configs,
            count: count
        })
        return
    }
}
