/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let errorBuilder = require('../../../helpers/error-builder')

module.exports = {

    // GET /v3/experiment-locrep-blocks
    get: async function (req, res, next) {

        // Set defaults
        let limit = req.paginate.limit

        let offset = req.paginate.offset

        let params = req.query

        let sort = req.query.sort

        let count = 0

        let conditionString = ''

        let orderString = ''

        // Build query
        let locRepBlocksQuery = `
            SELECT 
                locRepBlock.id AS "locRepBlockDbId",
                locRepBlock.locrep_id AS "locRepDbId",
                locRepBlock.parent_id AS "parentDbId",
                locRepBlock.order_number AS "orderNumber",
                locRepBlock.code,
                locRepBlock.name,
                locRepBlock.block_type AS "blockType",
                locRepBlock.no_of_blocks AS "noOfBlocks",
                locRepBlock.no_of_rows_in_block AS "noOfRowsInBlock",
                locRepBlock.no_of_cols_in_block AS "noOfColsInBlock",
                locRepBlock.no_of_reps_in_block AS "noOfRepsInBlock",
                locRepBlock.plot_numbering_order AS "plotNumberingOrder",
                locRepBlock.starting_corner AS "startingCorner",
                locRepBlock.entry_ids AS "entryDbIds",
                locRepBlock.remarks
            FROM
                operational.locrep_block locRepBlock
            LEFT JOIN
                operational.study study ON locRepBlock.locrep_id = study.id
            WHERE 
                locRepBlock.is_void=FALSE  
            ORDER BY
                locRepBlock.id
        `
        /* 
         * Filter block
         */

        // Allow filtering using studyDbId
        if (params.studyDbId !== undefined) {
            let parameters = []

            parameters['studyDbId'] = params.studyDbId

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400065)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400023)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL Query
        locRepBlocksFinalQuery = await processQueryHelper.getFinalSqlQuery(
            locRepBlocksQuery,
            conditionString,
            orderString
        )

        // Retrieve locRepBlocks from the database
        let locRepBlocks = await sequelize.query(locRepBlocksFinalQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (locRepBlocks == undefined || locRepBlocks.length < 1) {
            res.send(200, {
              rows: [],
              count: 0
            })
            return
        } else {
            // Get count
            locRepBlocksCountSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
                locRepBlocksQuery,
                conditionString,
                orderString
            )

            let locRepBlocksCount = await sequelize.query(locRepBlocksCountSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            count = locRepBlocksCount[0].count;
        }

        res.send(200, {
            rows: locRepBlocks,
            count: count
        })
        return
    },

    // POST /v3/experiment-blocks
    post: async function (req, res, next) {

        // Set defaults
        let resultArray = []
        let locRepBlockDbId = null
        let recordCount = 0

        let studyDbId = null
        let parentDbId = null
        let orderNumber = 1                 // default value: 1; must be int
        let code = null                     // unique
        let name = null
        let blockType = null
        let noOfBlocks = 0                  // default value: 0; must be int
        let noOfRowsInBlock = 0             // default value: 0; must be int
        let noOfColsInBlock = 0             // default value: 0; must be int
        let noOfRepsInBlock = 1             // default value: 1; must be int
        let plotNumberingOrder = null
        let startingCorner = null
        let entryDbIds = []
        let remarks = null

        let hasParentDbId = false       // checks if it had parentDbID or not

        // Get user ID of client from access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure


        // Set URL for response
        let locRepBlockUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/experiment-locrep-blocks'

        // Check request body
        if (req.body != null) {

            // Parse input
            let data = req.body
            let records = []

            try {
                records = data.records
                recordCount = records.length

                if (records == undefined || recordCount == 0) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            } catch (e) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            // Start transaction
            let transaction

            try {

                // Get transaction
                transaction = await sequelize.transaction({ autocommit: false })

                let locRepBlockValuesArray = []
                let visitedArray = []

                for (var item in records) {
                    let validateQuery = ``
                    let validateCount = 0

                    // Check if the required columns are in the input
                    if (!records[item].studyDbId || !records[item].code || !records[item].name || !records[item].blockType || !records[item].plotNumberingOrder || !records[item].startingCorner) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400105)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    /* 
                        Validate and set values
                    */

                    if (records[item].studyDbId != undefined) {
                        studyDbId = records[item].studyDbId

                        if (isNaN(studyDbId)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400065)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        // Validate experiment
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if study is existing return 1
                                SELECT count(1)
                                FROM operational.study study
                                WHERE 
                                    study.is_void = FALSE AND
                                    study.id = ` + studyDbId + `
                            )
                        `
                        validateCount += 1
                    }


                    if (records[item].parentDbId != undefined) {
                        parentDbId = records[item].parentDbId

                        if (isNaN(parentDbId)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400106)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        // Validate parentID
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        if (parentDbId !== null) {
                            validateQuery += `
                                (
                                    --- Check if parent ID is existing return 1
                                    SELECT count(1)
                                    FROM operational.locrep_block locRepBlock
                                    WHERE 
                                        locRepBlock.is_void = FALSE AND
                                        locRepBlock.id = ` + parentDbId + `
                                )
                                    +
                                (
                                    --- Check if parent ID belongs to the same study as block being created
                                    SELECT count(1)
                                    FROM operational.locrep_block locRepBlock 
                                    LEFT JOIN
                                        operational.study study ON locRepBlock.locrep_id = study.id
                                    WHERE 
                                        locRepBlock.is_void = FALSE AND
                                        locRepBlock.id = ` + parentDbId + ` AND
                                        locRepBlock.locrep_id = ` + studyDbId + `
                                )
                            `
                            validateCount += 2

                            hasParentDbId = true
                        }
                    } else parentDbId = null


                    if (records[item].orderNumber != undefined) {
                        orderNumber = records[item].orderNumber

                        // Validate orderNumber, should be a number
                        if (isNaN(orderNumber)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400093)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                    } else orderNumber = 1


                    if (records[item].code != undefined) {
                        code = records[item].code

                        // Validate code
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if the code is existing return 1
                                SELECT 
                                    CASE WHEN
                                        (count(1) = 0)
                                        THEN 1
                                        ELSE 0
                                    END AS count
                                FROM operational.locrep_block locRepBlock
                                WHERE
                                    locRepBlock.is_void = FALSE AND
                                    locRepBlock.code LIKE '` + code + `'
                            )
                        `
                        validateCount += 1
                    }


                    if (records[item].noOfBlocks != undefined) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400101)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }


                    if (records[item].noOfRowsInBlock != undefined) {
                        noOfRowsInBlock = records[item].noOfRowsInBlock

                        // Validate noOfRowsInBlock, should be a number
                        if (isNaN(noOfRowsInBlock)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400095)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                    } else noOfRowsInBlock = 0


                    if (records[item].noOfColsInBlock != undefined) {
                        noOfColsInBlock = records[item].noOfColsInBlock

                        // Validate noOfColsInBlock, should be a number
                        if (isNaN(noOfColsInBlock)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400096)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                    } else noOfColsInBlock = 0


                    if (records[item].noOfRepsInBlock != undefined) {
                        noOfRepsInBlock = records[item].noOfRepsInBlock

                        // Validate noOfRepsInBlock, should be a number
                        if (isNaN(noOfRepsInBlock)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400097)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                    } else noOfRepsInBlock = 1


                    if (records[item].plotNumberingOrder != undefined) {
                        plotNumberingOrder = records[item].plotNumberingOrder

                        let plotNumberingOrderQuery = `
                            SELECT 
                                scale_value.value AS "value"
                            FROM
                                master.scale_value scale_value
                            LEFT JOIN
                                master.variable variable ON scale_value.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'PLOT_NUMBERING_ORDER'
                        `

                        let plotNumberingOrderValues = await sequelize.query(plotNumberingOrderQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                            .catch(async err => {
                                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                                res.send(new errors.InternalError(errMsg))
                                return
                            })

                        // Validate plotNumberingOrder
                        let validPlotNumberingOrder = []

                        for (i in plotNumberingOrderValues) {
                            validPlotNumberingOrder.push(plotNumberingOrderValues[i]['value'])
                        }

                        if (!validPlotNumberingOrder.includes(plotNumberingOrder)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400098)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }


                    if (records[item].startingCorner != undefined) {
                        startingCorner = records[item].startingCorner

                        let startingCornerQuery = `
                            SELECT 
                                scale_value.value AS "value"
                            FROM
                                master.scale_value scale_value
                            LEFT JOIN
                                master.variable variable ON scale_value.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'STARTING_CORNER'
                        `

                        let startingCornerValues = await sequelize.query(startingCornerQuery, {
                            type: sequelize.QueryTypes.SELECT
                        })
                            .catch(async err => {
                                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                                res.send(new errors.InternalError(errMsg))
                                return
                            })

                        // Validate startingCorner
                        let validStartingCorner = []

                        for (i in startingCornerValues) {
                            validStartingCorner.push(startingCornerValues[i]['value'])
                        }

                        if (!validStartingCorner.includes(startingCorner)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400099)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }


                    let entryDbIdsString = ``

                    if (records[item].entryDbIds != undefined) {
                        entryDbIdsString += `{`
                        entryDbIds = records[item].entryDbIds
                        for (i in entryDbIds) {
                            entryDbIds[i] = entryDbIds[i].toString()
                        }
                        let uniqueEntryDbIds = entryDbIds.filter((uniqueEntryDbId, index, value) => value.indexOf(uniqueEntryDbId) === index)

                        // Validate entryListIds if same experiment as experiment ID of block and parent block
                        for (i in uniqueEntryDbIds) {

                            if (isNaN(uniqueEntryDbIds[i])) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400020)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            validateQuery += (validateQuery != '') ? ' + ' : ''
                            validateQuery += `
                                (
                                    --- Check if the entry exists under the same study ID return 1
                                    SELECT count(1)
                                    FROM operational.entry entry
                                    WHERE 
                                        entry.is_void = FALSE AND
                                        entry.id = ` + uniqueEntryDbIds[i] + ` AND
                                        entry.study_id = ` + studyDbId + `
                                )
                            `
                            validateCount += 1

                            entryDbIdsString += uniqueEntryDbIds[i]
                            if ((uniqueEntryDbIds.length > 1) && (i < (uniqueEntryDbIds.length) - 1)) entryDbIdsString += `,`
                        }
                        entryDbIdsString += `}`
                    } else {
                        entryDbIdsString = `{}`
                    }


                    name = (records[item].name != undefined) ? records[item].name : null

                    blockType = (records[item].blockType != undefined) ? records[item].blockType : null

                    remarks = (records[item].remarks != undefined) ? records[item].remarks : null


                    /* 
                        Validate input in DB
                        count must be equal to validateCount if all conditions are met
                    */
                    let checkInputQuery =
                        `SELECT ` + validateQuery +
                        ` AS count`

                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (inputCount[0]['count'] != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }


                    // Check duplicates if the record count is  > 1
                    if (recordCount > 1) {
                        // Check if code is already used
                        let locRepBlockInfo = code
                        if (visitedArray.includes(locRepBlockInfo)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400111)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        } else {
                            visitedArray.push(locRepBlockInfo)
                        }
                    }

                    // Get values
                    let tempArray = [
                        studyDbId, parentDbId, orderNumber, code, name, blockType,
                        noOfBlocks, noOfRowsInBlock, noOfColsInBlock, noOfRepsInBlock,
                        plotNumberingOrder, startingCorner, entryDbIdsString, userDbId, remarks, 'Created via CB API'
                    ]

                    locRepBlockValuesArray.push(tempArray)

                }

                /*
                    Create locrep block record
                */
                let locRepBlockQuery = format(`
                    INSERT INTO
                        operational.locrep_block
                            (
                                locrep_id, parent_id, order_number, code, name, block_type, 
                                no_of_blocks, no_of_rows_in_block, no_of_cols_in_block, 
                                no_of_reps_in_block, plot_numbering_order, starting_corner, 
                                entry_ids, creator_id, remarks, notes
                            )
                    VALUES 
                        %L
                    RETURNING id`, locRepBlockValuesArray
                )

                locRepBlockQuery = locRepBlockQuery.trim()

                let locRepBlocks = await sequelize.query(locRepBlockQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction
                })
                    .catch(err => {
                        let errMsg = errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })


                for (locRepBlock of locRepBlocks[0]) {
                    locRepBlockDbId = locRepBlock.id

                    // Return the experiment block info
                    let array = {
                        locRepBlockDbId: locRepBlockDbId,
                        recordCount: 1,
                        href: locRepBlockUrlString + '/' + locRepBlockDbId
                    }
                    resultArray.push(array)
                }


                // Update noOfBlocks of parentDbId
                if (hasParentDbId) {
                    let noOfBlocksOfParentQuery = `
                        SELECT 
                            count(1)
                        FROM 
                            operational.locrep_block
                        WHERE 
                            is_void = FALSE AND
                            parent_id = ` + parentDbId + `
                    `
                    let noOfBlocksOfParentCount = await sequelize.query(noOfBlocksOfParentQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                        .catch(async err => {
                            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                            res.send(new errors.InternalError(errMsg))
                            return
                        })

                    let noOfBlocksOfParent = noOfBlocksOfParentCount[0]['count']
                    noOfBlocksOfParent = parseInt(noOfBlocksOfParent)

                    let updateParentBlockQuery = `
                        UPDATE
                            operational.locrep_block parentBlock
                        SET
                            no_of_blocks = ` + (noOfBlocksOfParent + 1) + `
                        WHERE parentBlock.id = ` + parentDbId

                    await sequelize.query(updateParentBlockQuery, {
                        type: sequelize.QueryTypes.UPDATE
                    })
                        .catch(async err => {
                            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                            res.send(new errors.InternalError(errMsg))
                            return
                        })
                }

                // Commit the transaction
                await transaction.commit()

                res.send(200, {
                    rows: resultArray
                })
                return
            } catch (err) {
                // Rollback transaction if any errors were encountered
                if (err) await transaction.rollback()
                let errMsg = await errorBuilder.getError(req.headers.host, 500001)
                res.send(new errors.InternalError(errMsg))
                return
            }

        } else {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
    }
}