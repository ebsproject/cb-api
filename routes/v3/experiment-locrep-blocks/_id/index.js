/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let forwarded = require('forwarded-for')
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let errorBuilder = require('../../../../helpers/error-builder')
let userValidator = require('../../../../helpers/person/validator.js')

module.exports = {

    // GET /v3/experiment-locrep-blocks/{id}
    get: async function (req, res, next) {

        let locRepBlockDbId = req.params.id
        let count = 0

        if (!validator.isInt(locRepBlockDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400100)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build query
        let locRepBlocksQuery = `
        SELECT
            locRepBlock.id AS "locRepBlockDbId",
            locRepBlock.locrep_id AS "locRepDbId",
            locRepBlock.parent_id AS "parentDbId",
            locRepBlock.order_number AS "orderNumber",
            locRepBlock.code,
            locRepBlock.name,
            locRepBlock.block_type AS "blockType",
            locRepBlock.no_of_blocks AS "noOfBlocks",
            locRepBlock.no_of_rows_in_block AS "noOfRowsInBlock",
            locRepBlock.no_of_cols_in_block AS "noOfColsInBlock",
            locRepBlock.no_of_reps_in_block AS "noOfRepsInBlock",
            locRepBlock.plot_numbering_order AS "plotNumberingOrder",
            locRepBlock.starting_corner AS "startingCorner",
            locRepBlock.entry_ids AS "entryDbIds",
            locRepBlock.remarks
        FROM
            operational.locrep_block  locRepBlock
        LEFT JOIN
            operational.study study ON locRepBlock.locrep_id = study.id    
        WHERE
            locRepBlock.is_void=FALSE
        AND
            locRepBlock.id = :locRepBlockDbId
        ORDER BY
            locRepBlock.id
        `
        // Retrieve locrep-blocks from the database
        let locRepBlocks = await sequelize.query(locRepBlocksQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                locRepBlockDbId: locRepBlockDbId
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        res.send(200, {
            rows: locRepBlocks
        })
        return
    },

    // PUT v3/experiment-locrep-blocks/{id}
    put: async function (req, res, next) {

        // retrieve locrep block ID
        let locRepBlockDbId = req.params.id

        // Retrieve user ID of client from the access token
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
          let errMsg = await errorBuilder.getError(req.headers.host, 401002)
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        // Check if user is an administrator. If not, no access
        let isAdmin = await userValidator.isAdmin(userDbId)

        if (!validator.isInt(locRepBlockDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400100)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else {

            // Set defaults
            let resultArray = []

            let orderNumber = 1                 // must be int
            let code = null                     // unique
            let name = null
            let blockType = ''
            let noOfRowsInBlock = 0             // must be int
            let noOfColsInBlock = 0             // must be int
            let noOfRepsInBlock = 1             // must be int
            let plotNumberingOrder = null
            let startingCorner = null
            let entryDbIds = []
            let remarks = null

            // Build query
            let locRepBlockQuery = `
                SELECT
                    locRepBlock.id AS "locRepBlockDbId",
                    locRepBlock.locrep_id AS "studyDbId",
                    locRepBlock.parent_id AS "parentDbId",
                    locRepBlock.order_number AS "orderNumber",
                    locRepBlock.code,
                    locRepBlock.name,
                    locRepBlock.block_type AS "blockType",
                    locRepBlock.no_of_blocks AS "noOfBlocks",
                    locRepBlock.no_of_rows_in_block AS "noOfRowsInBlock",
                    locRepBlock.no_of_cols_in_block AS "noOfColsInBlock",
                    locRepBlock.no_of_reps_in_block AS "noOfRepsInBlock",
                    locRepBlock.plot_numbering_order AS "plotNumberingOrder",
                    locRepBlock.starting_corner AS "startingCorner",
                    locRepBlock.creator_id AS "creatorDbId",
                    locRepBlock.entry_ids AS "entryDbIds",
                    locRepBlock.notes
                FROM
                    operational.locrep_block  locRepBlock
                LEFT JOIN
                    operational.study study ON locRepBlock.locrep_id = study.id    
                WHERE
                    locRepBlock.is_void=FALSE AND
                    locRepBlock.id = ` + locRepBlockDbId

            // Retrieve locrep blocks from database
            let locRepBlocks = await sequelize.query(locRepBlockQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (
                await locRepBlocks == undefined ||
                await locRepBlocks.length < 1
            ) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404034)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Get values from database
            let locRepBlockCreatorDbId = locRepBlocks[0].creatorDbId
            let locRepBlockStudyDbId = locRepBlocks[0].studyDbId
            let locRepBlockCode = locRepBlocks[0].code
            let locRepBlockPlotNumberingOrder = locRepBlocks[0].plotNumberingOrder
            let locRepBlockStartingCorner = locRepBlocks[0].startingCorner
            let locRepBlockEntryDbIds = locRepBlocks[0].entryDbIds
            let locRepBlockNotes = locRepBlocks[0].notes

            // Checks if user is the owner of the locrep block
            if (locRepBlockCreatorDbId != userDbId) {
                if(!isAdmin) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 401013)
                    res.send(new errors.UnauthorizedError(errMsg))
                    return
                }
            }

            let isSecure = forwarded(req, req.headers).secure

            // Set URL for response
            let locRepBlockUrlString = (isSecure ? 'https' : 'http')
                + "://"
                + req.headers.host
                + "/v3/experiment-locrep-blocks"


            // Check for parameters
            if (req.body != null) {

                // Parse the input
                let data = req.body
                let records = null

                try {
                    records = data.records

                    if (records == undefined) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                } catch (err) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                // Start transaction
                let transaction = null

                try {
                    let setQuery = ``

                    for (var item in records) {

                        let validateQuery = ``
                        let validateCount = 0

                        /* Validation of values */

                        // Validate and set values
                        if (records[item].studyDbId != undefined) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400108)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }


                        if (records[item].parentDbId != undefined) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400107)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }


                        if (records[item].orderNumber != undefined) {
                            orderNumber = records[item].orderNumber

                            // Validate orderNumber, should be a number
                            if (isNaN(orderNumber)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400093)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                order_number = $$` + orderNumber + `$$`
                        }


                        if (records[item].code != undefined) {
                            code = records[item].code

                            // Validate value if different from current experiment block info
                            if (code != locRepBlockCode) {

                                // Validate code
                                validateQuery += (validateQuery != '') ? ' + ' : ''
                                validateQuery += `
                                    (
                                        --- Check if the code is existing return 1
                                        SELECT 
                                            CASE WHEN
                                                (count(1) = 0)
                                                THEN 1
                                                ELSE 0
                                            END AS count
                                        FROM operational.locrep_block locRepBlock
                                        WHERE
                                            locRepBlock.is_void = FALSE AND
                                            locRepBlock.code LIKE '` + code + `'
                                    )
                                `
                                validateCount += 1
                            }

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                code = $$` + code + `$$`
                        }


                        if (records[item].name != undefined) {
                            name = records[item].name

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                name = $$` + name + `$$`
                        }


                        if (records[item].blockType != undefined) {
                            blockType = records[item].blockType

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                block_type = $$` + blockType + `$$`
                        }

                        if (records[item].noOfBlocks != undefined) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400101)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }


                        if (records[item].noOfRowsInBlock != undefined) {
                            noOfRowsInBlock = records[item].noOfRowsInBlock

                            // Validate noOfRowsInBlock, should be a number
                            if (isNaN(noOfRowsInBlock)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400102)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                no_of_rows_in_block = $$` + noOfRowsInBlock + `$$`
                        }


                        if (records[item].noOfColsInBlock != undefined) {
                            noOfColsInBlock = records[item].noOfColsInBlock

                            // Validate noOfColsInBlock, should be a number
                            if (isNaN(noOfColsInBlock)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400103)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                no_of_cols_in_block = $$` + noOfColsInBlock + `$$`
                        }


                        if (records[item].noOfRepsInBlock != undefined) {
                            noOfRepsInBlock = records[item].noOfRepsInBlock

                            // Validate noOfRepsInBlock, should be a number
                            if (isNaN(noOfRepsInBlock)) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400104)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                no_of_reps_in_block = $$` + noOfRepsInBlock + `$$`
                        }


                        if (records[item].plotNumberingOrder != undefined) {
                            plotNumberingOrder = records[item].plotNumberingOrder

                            // Validate value if different from the current experiment block info
                            if (plotNumberingOrder != locRepBlockPlotNumberingOrder) {

                                let plotNumberingOrderQuery = `
                                    SELECT 
                                        scale_value.value AS "value"
                                    FROM
                                        master.scale_value scale_value
                                    LEFT JOIN
                                        master.variable variable ON scale_value.scale_id = variable.scale_id
                                    WHERE
                                        variable.abbrev LIKE 'PLOT_NUMBERING_ORDER'
                                `

                                let plotNumberingOrderValues = await sequelize.query(plotNumberingOrderQuery, {
                                    type: sequelize.QueryTypes.SELECT
                                })
                                    .catch(async err => {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                                        res.send(new errors.InternalError(errMsg))
                                        return
                                    })

                                // Validate plotNumberingOrder
                                let validPlotNumberingOrder = []

                                for (i in plotNumberingOrderValues) {
                                    validPlotNumberingOrder.push(plotNumberingOrderValues[i]['value'])
                                }

                                if (!validPlotNumberingOrder.includes(plotNumberingOrder)) {
                                    let errMsg = await errorBuilder.getError(req.headers.host, 400098)
                                    res.send(new errors.BadRequestError(errMsg))
                                    return
                                }

                            }

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                plot_numbering_order = $$` + plotNumberingOrder + `$$`

                        }


                        if (records[item].startingCorner != undefined) {
                            startingCorner = records[item].startingCorner

                            // Validate value if different from current experiment block info
                            if (startingCorner != locRepBlockStartingCorner) {
                                let startingCornerQuery = `
                                    SELECT 
                                        scale_value.value AS "value"
                                    FROM
                                        master.scale_value scale_value
                                    LEFT JOIN
                                        master.variable variable ON scale_value.scale_id = variable.scale_id
                                    WHERE
                                        variable.abbrev LIKE 'STARTING_CORNER'
                                `

                                let startingCornerValues = await sequelize.query(startingCornerQuery, {
                                    type: sequelize.QueryTypes.SELECT
                                })
                                    .catch(async err => {
                                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                                        res.send(new errors.InternalError(errMsg))
                                        return
                                    })

                                // Validate startingCorner
                                let validStartingCorner = []

                                for (i in startingCornerValues) {
                                    validStartingCorner.push(startingCornerValues[i]['value'])
                                }

                                if (!validStartingCorner.includes(startingCorner)) {
                                    let errMsg = await errorBuilder.getError(req.headers.host, 400099)
                                    res.send(new errors.BadRequestError(errMsg))
                                    return
                                }

                            }

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                starting_corner = $$` + startingCorner + `$$`

                        }


                        let entryDbIdsString = ``
                        let uniqueEntryDbIds = []
                        if (records[item].entryDbIds != undefined) {
                            entryDbIds = records[item].entryDbIds
                            for (i in entryDbIds) {
                                entryDbIds[i] = entryDbIds[i].toString()
                            }
                            uniqueEntryDbIds = entryDbIds.filter((uniqueEntryDbId, index, value) => value.indexOf(uniqueEntryDbId) === index)

                            // Validate value if different from current locrep block info
                            if (uniqueEntryDbIds != locRepBlockEntryDbIds) {

                                entryDbIdsString += `{`
                                for (i in uniqueEntryDbIds) {
                                    validateQuery += (validateQuery != '') ? ' + ' : ''
                                    validateQuery += `
                                        (
                                            --- Check if the entry exists under the same study ID return 1
                                            SELECT count(1)
                                            FROM operational.entry entry
                                            WHERE 
                                                entry.is_void = FALSE AND
                                                entry.id = ` + uniqueEntryDbIds[i] + ` AND
                                                entry.study_id = ` + locRepBlockStudyDbId + `
                                        )
                                    `
                                    validateCount += 1
                                    entryDbIdsString += uniqueEntryDbIds[i]
                                    if ((uniqueEntryDbIds.length > 1) && (i < (uniqueEntryDbIds.length - 1))) entryDbIdsString += `,`
                                }
                                entryDbIdsString += `}`

                                setQuery += (setQuery != '') ? ',' : ''
                                setQuery += `
                                    entry_ids = $$` + entryDbIdsString + `$$`
                            }
                        }


                        if (records[item].remarks != undefined) {
                            remarks = records[item].remarks

                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                remarks = $$` + remarks + `$$`
                        }


                        /* Validate input in DB */
                        // count must be equal to validateCount if all conditions are met
                        if (validateCount > 0) {
                            let checkInputQuery =
                                `SELECT ` + validateQuery +
                                ` AS count`

                            let inputCount = await sequelize.query(checkInputQuery, {
                                type: sequelize.QueryTypes.SELECT
                            })

                            if (inputCount[0]['count'] != validateCount) {
                                let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                                res.send(new errors.BadRequestError(errMsg))
                                return
                            }
                        }


                        // Check and update notes
                        if (locRepBlockNotes != null && !locRepBlockNotes.includes('Updated via CB API')) {
                            setQuery += (setQuery != '') ? ',' : ''
                            setQuery += `
                                notes = z_admin.append_text(locRepBlock.notes, 'Updated via CB API')`
                        }


                        // Update the locrep block record
                        let updateLocRepBlockQuery = `
                            UPDATE 
                                operational.locrep_block locRepBlock
                            SET
                                ` + setQuery + `,
                                modification_timestamp = NOW(),
                                modifier_id = ` + userDbId + `
                            WHERE
                                locRepBlock.id = ` + locRepBlockDbId

                        // Get transaction
                        transaction = await sequelize.transaction({ autocommit: false })

                        await sequelize.query(updateLocRepBlockQuery, {
                            type: sequelize.QueryTypes.UPDATE,
                            transaction: transaction
                        })
                            .catch(async err => {
                                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                                res.send(new errors.InternalError(errMsg))
                                return
                            })


                        // Return the transaction info
                        let array = {
                            locRepBlockDbId: locRepBlockDbId,
                            recordCount: 1,
                            href: locRepBlockUrlString + '/' + locRepBlockDbId
                        }
                        resultArray.push(array)

                    }

                    // Commit the transaction
                    await transaction.commit()

                    res.send(200, {
                        rows: resultArray
                    })
                    return
                } catch (err) {
                    // Rollback transaction if any errors were encountered
                    if (err) await transaction.rollback()
                    let errMsg = await errorBuilder.getError(req.headers.host, 500003)
                    res.send(new errors.InternalError(errMsg))
                    return
                }
            } else {
                let errMsg = await errorBuilder.getError(req.headers.host, 400009)
                res.send(new errors.BadRequestError(errMsg))
                return
            }

        }


    }
}