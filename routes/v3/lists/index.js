/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies

let { sequelize, lists } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let listsHelper = require('../../../helpers/lists/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../helpers/logger')

const endpoint = 'lists'

module.exports = {

    // Implementation of GET call for /v3/lists
    get: async function (req, res, next) {
        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let conditionString = ''
        let parameters = {}
        let orderString = ''
        let ownership = ''

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let getProgramsQuery =  `
            SELECT 
                program.id 
            FROM 
                tenant.program program,
                tenant.program_team pt,
                tenant.team_member tm
            WHERE
                tm.person_id = ${userId}
                AND pt.team_id = tm.team_id
                AND program.id = pt.program_id
                AND tm.is_void = FALSE
        `
        let programIds = await sequelize.query(getProgramsQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let teamQuery = ``
        let programFromQuery = ``
        let programPermissionQuery = ``
        let programCount = 1
        for (let programObj of programIds) {
            if (teamQuery != ``) {
                teamQuery += ` OR `
            }

            let programAlias = `programRole${programCount}`
            teamQuery += `list.access_data #> $$\{program,${programObj.id}}$$ is not null`
            programPermissionQuery += ` OR "${programAlias}".person_role_code = 'DATA_OWNER'`

            programFromQuery += `
                LEFT JOIN
                    tenant.person_role "${programAlias}"
                ON
                    "programRole1".id = (list.access_data->'program'->'${programObj.id}'::text->>'dataRoleId')::integer
            `
            programCount++
        }

        let userQuery = `list.access_data #> $$\{user,${userId}}$$ is not null`
        if (teamQuery != ``) {
            userQuery += ` OR ` + teamQuery
        }

        if (params.ownershipType !== undefined) {
            if (params.ownershipType == 'owned') {
                ownership = `list.creator_id = ${userId}`
            } else if (params.ownershipType == 'shared' || params.ownershipType == 'combined') {
                ownership = userQuery
            } else {
                let errMsg = await errorBuilder.getError(req.headers.host, 400164)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } else {
            ownership = userQuery
        }

        // hide working lists by default
        let workingListQuery = ` AND list.list_usage <> 'working list'`
        if (params.showWorkingList !== undefined) {
            if (params.showWorkingList == 'true') {
                workingListQuery = ``
            }
        }

        let listsQuery = null
        let fromQuery = `
            FROM
                platform.list list
            INNER JOIN
                tenant.person creator ON list.creator_id = creator.id
            LEFT JOIN
                tenant.person_role pr
            ON
                pr.id = (list.access_data->'user'->'${userId}'::text->>'dataRoleId')::integer
            ${programFromQuery}
            LEFT JOIN
                tenant.person modifier ON list.modifier_id = modifier.id
            WHERE
                list.is_void = FALSE AND
                (${ownership})
                `+workingListQuery+`
            ORDER BY
                list.id
        `

        // Build query
        listsQuery = `
            SELECT
                list.id as "listDbId",
                list.abbrev,
                list.name,
                list.display_name as "displayName",
                list.type,
                list.list_sub_type AS "subType",
                list.entity_id as "entityId",
                list.description,
                list.remarks,
                list.notes,
                list.is_active as "isActive",
                list.status,
                list.list_usage AS "listUsage",
                list.creation_timestamp as "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name as creator,
                list.modification_timestamp as "modificationTimestamp",
                modifier.id as "modifierDbId",
                modifier.person_name as modifier,
                (
                    SELECT
                    count(lm.id)
                    FROM
                    platform.list_member lm
                    WHERE
                    lm.list_id = list.id AND
                    lm.is_void = FALSE AND
                    lm.is_active = TRUE
                ) as "memberCount",
                (
                    select count(1)
                    from (
                    select
                        jsonb_object_keys(list.access_data->'user')) t
                ) as "shareCount",
                CASE WHEN (pr.person_role_code = 'DATA_OWNER' ${programPermissionQuery}) THEN 'read_write' 
                ELSE 'read' end AS "permission"
            ${fromQuery}
        `

        // Get filter condition for abbrev only
        if (
            params.abbrev !== undefined ||
            params.name !== undefined ||
            params.displayName !== undefined ||
            params.type !== undefined ||
            params.description !== undefined ||
            params.remarks !== undefined ||
            params.creator !== undefined ||
            params.creationTimestamp !== undefined ||
            params.modifier !== undefined ||
            params.modificationTimestamp !== undefined
        ) {
            let parameters = []

            if (params.abbrev !== undefined) parameters['abbrev'] = params.abbrev
            if (params.name !== undefined) parameters['name'] = params.name
            if (params.displayName !== undefined) parameters['displayName'] = params.displayName
            if (params.type !== undefined) parameters['type'] = params.type
            if (params.description !== undefined) parameters['description'] = params.description
            if (params.remarks !== undefined) parameters['remarks'] = params.remarks
            if (params.creator !== undefined) parameters['creator'] = params.creator
            if (params.creationTimestamp !== undefined) parameters['creationTimestamp'] = params.creationTimestamp
            if (params.modifier !== undefined) parameters['modifier'] = params.modifier
            if (params.modificationTimestamp !== undefined) parameters['modificationTimestamp'] = params.modificationTimestamp

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Generate the final sql query
        listFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            listsQuery,
            conditionString,
            orderString
        )

        // Retrieve lists from the database
        let lists = await sequelize.query(listFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (lists === undefined || lists.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get count
        listCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(listsQuery, conditionString, orderString)

        listCount = await sequelize.query(listCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = listCount[0].count

        res.send(200, {
            rows: lists,
            count: count
        })
        return
    },

    // Endpoint for creating a new list record
    // Implementation of POST call for /v3/lists
    post: async function (req, res, next) {
        // Set defaults
        let listValuesArray = []
        let resultArray = []

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let listUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/lists'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let records = data.records
        if (records == undefined || records.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction

        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            for (var record of records) {
                let abbrev = null
                let name = null
                let displayName = null
                let type = null
                let subType = null
                let description = null
                let remarks = null
                let entityAbbrev = null
                let status = `created`
                let isActive = `true`
                let accessData = null
                let listUsage = 'final list'

                // validate if required value exists
                if (
                    record.abbrev === undefined ||
                    record.name === undefined ||
                    record.displayName === undefined ||
                    record.type === undefined
                ) { 
                    let errMsg = await errorBuilder.getError(req.headers.host, 400165)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // validate abbrev and name should be unique
                let validateQuery = ``
                let validateCount = 0

                if (record.abbrev !== undefined) {
                    abbrev = record.abbrev

                    // Check if it exists in DB
                    validateQuery += `
                        (
                        SELECT
                            CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                            END AS count
                        FROM
                            platform.list list
                        WHERE
                            list.is_void = FALSE AND
                            upper(list.abbrev) = '${abbrev}'
                        )
                    `
                    validateCount += 1
                }

                if (record.name !== undefined) {
                    name = record.name

                    // Check if it exists in DB
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                        SELECT
                            CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                            END AS count
                        FROM
                            platform.list list
                        WHERE
                            list.is_void = FALSE AND
                            list.name ILIKE '${name}'
                        )
                    `
                    validateCount += 1
                }

                if (record.type !== undefined) {
                    entityAbbrev = await listsHelper.getEntityGivenType(req,res,record.type)
                }

                // default access data
                let dateQuery =format(`
                    SELECT NOW()`
                )

                let date = await sequelize.query(dateQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    transaction: transaction
                })

                date = date[0].now

                let defaultAccessData = {"addedBy" : userDbId, "addedOn" : date, "dataRoleId" : 25}

                // populate accessdata column
                if (record.accessData !== undefined){
                    accessData = record.accessData

                    // add timestamps
                    if (accessData.program != undefined) {
                        for (var index in accessData.program) {
                            accessData.program[index]['addedOn'] = date
                        }
                    }

                    if (accessData.user != undefined) {
                        for (var index in accessData.user) {
                            accessData.user[index]['addedOn'] = date
                        }
                    }
                    
                    if(accessData.user==undefined || accessData.user[userDbId] == undefined){

                        if (accessData.user == undefined) {
                            accessData.user = {}
                        }
                        accessData.user[userDbId] = defaultAccessData
                    }
                } else {
                    accessData = {}
                    accessData.user = {}
                    accessData.user[userDbId] = defaultAccessData
                }

                if (record.type !== undefined) type = record.type
                if (record.displayName !== undefined) displayName = record.displayName

                // Validate non-required/optional parameters
                if (record.description !== undefined) description = record.description
                if (record.remarks !== undefined) remarks = record.remarks
                if (record.status !== undefined) status = record.status
                if (record.isActive !== undefined) isActive = record.isActive
                if (record.listUsage !== undefined && record.listUsage !== '') {
                    // check if value is valid
                    if (record.listUsage == 'final list' || record.listUsage == 'working list') {
                        listUsage = record.listUsage
                    } else {
                        let errMsg = 'You have provided an invalid list usage value. Please use either final list or working list.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.subType !== undefined) {
                    subType = record.subType

                    if (type == 'germplasm') {
                        if (subType == 'entry list' || subType == 'sample list') {
                            let errMsg = `400: Invalid request, you have provided a list subtype that is not applicable to the list type.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }
    
                    if (type == 'package') {
                        if (subType == 'population list') {
                            let errMsg = `400: Invalid request, you have provided a list subtype that is not applicable to the list type.`
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }
                    }

                    // Check if it exists in DB
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            SELECT 
                                count(1)
                            FROM 
                                master.scale_value scaleValue
                            LEFT JOIN
                                master.variable variable ON scaleValue.scale_id = variable.scale_id
                            WHERE
                                variable.abbrev LIKE 'LIST_SUB_TYPE' AND
                                value ILIKE $$${subType}$$
                        )
                    `
                    validateCount += 1
                }

                // Validate input in DB
                let checkInputQuery = `SELECT ` + validateQuery + ` AS count`

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let getEntityIdQuery = `
                    SELECT
                        entity.id as "entityDbId"
                    FROM
                        dictionary.entity entity
                    WHERE
                        entity.abbrev = '${entityAbbrev}'
                `

                let entity = await sequelize.query(getEntityIdQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                let entityDbId = entity[0]['entityDbId']

                let tempValueArray = [
                    abbrev, name, displayName, type, subType, entityDbId, description, remarks,
                    userDbId, 'NOW()', 'Created by CB API', status, isActive, accessData, listUsage
                ]

                listValuesArray.push(tempValueArray)
            }
            // Create list record
            let listQuery =format(`
                INSERT INTO
                    platform.list (
                        abbrev, name, display_name, type, list_sub_type, entity_id, description, remarks, creator_id, creation_timestamp, notes, status, is_active, access_data, list_usage
                    )
                VALUES
                    %L
                RETURNING
                    id`, listValuesArray
            )
            
            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                let lists = await sequelize.query(listQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'INSERT', err)
                    throw new Error(err)
                })
    
                for (let list of lists[0]) {
                    listDbId = list.id
    
                    let array = {
                        listDbId: listDbId,
                        recordCount: 1,
                        href: listUrlString + '/' + listDbId
                    }
                    resultArray.push(array)
                }
            })

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Show log error message
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}