/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let listMemberHelper = require('../../../../helpers/listsMembers/index.js')
let logger = require('../../../../helpers/logger')
const endpoint = 'lists/:id/members'

module.exports = {

    // Implementation of GET call for /v3/lists/:id/members
    get: async function (req, res, next) {
        // Retrieve the list ID
        let parameters = {}
        let listDbId = req.params.id
        let params = req.params

		// for list members
		let isBasic =  req.params.isBasic ? (req.params.isBasic == 'true' ? true : false) : false
		let hasData = req.params.hasData ? (req.params.hasData == 'true' ? true : false) : false

        // check if duplicatesOnly parameter is defined
        parameters['duplicatesOnly'] = false
        if (params.duplicatesOnly !== undefined) {
            if (params.duplicatesOnly !== 'false') {
                parameters['duplicatesOnly'] = true
            }
        }

		if (!validator.isInt(listDbId)) {
			let errMsg = await errorBuilder.getError(req.headers.host, 400089)
			res.send(new errors.BadRequestError(errMsg))
			return
		} else {
			// Build query
			let listQuery = `
				SELECT
					list.id AS "listDbId",
					list.abbrev,
					list.name,
					list.display_name AS "displayName",
					list.type,
					entity.id AS "entityDbId",
					entity.name AS entity,
					entity.abbrev AS "entityAbbrev",
					list.description,
					list.remarks,
					list.creation_timestamp AS "creationTimestamp",
					creator.id AS "creatorDbId",
					creator.person_name AS creator,
					list.modification_timestamp AS "modificationTimestamp",
					modifier.id AS "modifierDbId",
					modifier.person_name AS modifier
				FROM
					platform.list list
				LEFT JOIN 
					tenant.person creator ON list.creator_id = creator.id
				LEFT JOIN 
					tenant.person modifier ON list.modifier_id = modifier.id
				LEFT JOIN
					dictionary.entity entity ON entity.id = list.entity_id
				WHERE
					list.is_void = FALSE
					AND list.id = ${listDbId}
			`

			// Retrieve list from the database   
			let lists = await sequelize.query(listQuery, {
				type: sequelize.QueryTypes.SELECT
			}).catch(async err => {
				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

			// Return error if resource is not found
			if (await lists.length < 1) {
				let errMsg = await errorBuilder.getError(req.headers.host, 404031)
				res.send(new errors.NotFoundError(errMsg))
				return
			} else {
				// Format results from query into an array
				for await (list of lists) {
					let memberQuery = await listMemberHelper.getMembersQuery(
						listDbId, 
						list.type, 
						null, 
						parameters['duplicatesOnly'],
						isBasic,
						hasData)

					if(memberQuery.includes('WITH') == -1){
						memberQuery = `
							SELECT * 
							FROM (
								${memberQuery}
							) as t
						`
					}

					if (parameters['duplicatesOnly']) {
						memberQuery = `
							SELECT * FROM (${memberQuery}) as t
					    	WHERE
					    	"rowNumber" <> 1
					    	AND "isActive"::TEXT = $$true$$
					  	`
        			} else{
          				// default return items should have isActive = true
						if(memberQuery.includes('WITH') == -1){
							memberQuery += `
								WHERE
									"isActive"::TEXT = $$true$$
							`
						}
          			}

          			let members = await sequelize.query(memberQuery, {
          			  	type: sequelize.QueryTypes.SELECT,
          			  	replacements: {
          			  		listDbId: listDbId
          			  	}
          			}).catch(async err => {
            			let errMsg = await errorBuilder.getError(req.headers.host, 500004)
              			res.send(new errors.InternalError(errMsg))
              			return
            		})
          			
					list["members"] = await members
        		}
			}

			res.send(200, {
				rows: lists
			})
			return
    	}
	},

	// Endpoint for creating a new list member
	// Implementation of POST call for /v3/lists/:id/members
	post: async function (req, res, next) {
		// Set defaults
		let dbInfo = null
		let dataIds = []
		let listMembersValuesArray = []
		let resultArray = []

    	let listDbId = req.params.id

		// Check if the connection is secure or not (http or https)
		let isSecure = forwarded(req, req.headers).secure

		// Set URL for response
		let listMemberUrlString = (isSecure ? 'https' : 'http')
			+ '://'
			+ req.headers.host
			+ '/v3/lists/'
			+ listDbId
			+ '/members'

		// Check if the request body does not exist
		if (!req.body) {
			let errMsg = await errorBuilder.getError(req.headers.host, 400009)
			res.send(new errors.BadRequestError(errMsg))
			return
		}

	    let data = req.body
	    let records = []

		try {
			let validateListQuery = `(
				SELECT
				CASE WHEN
				(count(1) = 1)
				THEN 1
				ELSE 0
				END AS count
				FROM
				platform.list list
				WHERE
				list.is_void = FALSE AND
				list.id = '${listDbId}'
			)`

			validateList = await sequelize.query(validateListQuery, {
				type: sequelize.QueryTypes.SELECT
			})

			if (validateList[0]['count'] != 1) {
				let errMsg = await errorBuilder.getError(req.headers.host, 404031)
				res.send(new errors.NotFoundError(errMsg))
				return
			}

			// Records = [{}...]
			records = data.records
			recordCount = records.length
			if (data.records == undefined || data.records.length == 0) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400005)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		} catch (e) {
			let errMsg = await errorBuilder.getError(req.headers.host, 500004)
			res.send(new errors.InternalError(errMsg))
			return
		}

	    let transaction
	    let userDbId = await tokenHelper.getUserId(req)

		if (userDbId == null) {
			let errMsg = await errorBuilder.getError(req.headers.host, 401002)
			res.send(new errors.BadRequestError(errMsg))
			return
		}

		try {
			if (listDbId !== undefined) {
				let dbInfoQuery = `
				SELECT
					schma.name "schema",
					tbl.name "table"
				FROM
					platform.list list
				LEFT JOIN
					dictionary.entity entity ON entity.id = list.entity_id
				LEFT JOIN
					dictionary.table tbl ON entity.table_id = tbl.id
				LEFT JOIN
					dictionary.schema schma ON tbl.schema_id = schma.id
				WHERE
					list.is_void = FALSE AND
					list.id = ${listDbId}
				`

				dbInfo = await sequelize.query(dbInfoQuery, {
					type: sequelize.QueryTypes.SELECT
				})

				dbInfo = dbInfo[0]
			}

			// get max order number of current list members
			let maxOrderNumberQuery = `
				SELECT 
					max(order_number) as "orderNumber"
				FROM 
					platform.list_member 
				WHERE 
					list_id = ${listDbId} 
					AND is_void = false
			`

			let maxOrderNumberInfo = await sequelize.query(maxOrderNumberQuery, {
				type: sequelize.QueryTypes.SELECT
			})

			maxOrderNumberInfo = maxOrderNumberInfo[0] ?? 0

			let count = maxOrderNumberInfo.orderNumber + 1;

			for (var record of records) {
				// validate if id and display value exists
				if (record.id === undefined) {
					let errMsg = await errorBuilder.getError(req.headers.host, 400168)
					res.send(new errors.BadRequestError(errMsg))
					return
				}

				let dataId = record.id

				if (record.displayValue === undefined) {
					let displayValueDefault
					// for faster inserts using tool, retrieve display value here
					if (dbInfo.table == 'variable') {
						displayValueDefault = 'label'
					} else if (dbInfo.table == 'germplasm') {
						displayValueDefault = 'designation'
					} else if (dbInfo.table == 'seed') {
						displayValueDefault = 'seed_name'
					} else if (dbInfo.table == 'package') {
					displayValueDefault = 'package_label'
					} else if (dbInfo.table == 'plot') {
					displayValueDefault = 'plot_code'
					} else {
						let errMsg = await errorBuilder.getError(req.headers.host, 400168)
						res.send(new errors.BadRequestError(errMsg))
						return
					}

					let displayValueQuery = `
						SELECT
							${displayValueDefault}
						FROM
							${dbInfo.schema}.${dbInfo.table} ${dbInfo.table}
						WHERE
							${dbInfo.table}.is_void = false AND
							${dbInfo.table}.id IN (${dataId})
					`

					let displayValueResult = await sequelize.query(displayValueQuery, {
						type: sequelize.QueryTypes.SELECT
					}).catch(async err => {
						let errMsg = await errorBuilder.getError(req.headers.host, 500004)
						res.send(new errors.InternalError(errMsg))
						return
					})

					if (displayValueResult != undefined && displayValueResult.length > 0) {
						displayValue = displayValueResult[0][displayValueDefault]
					} else {
						displayValue = dataId
					}
				} else {
					displayValue = record.displayValue
				}

				// Validate non-required/optional parameters
				let remarks = (record.remarks !== undefined) ? record.remarks : null
				dataIds.push(dataId)

				let tempListValueArray = [
					listDbId, dataId, count++, displayValue, remarks, userDbId
				]

				listMembersValuesArray.push(tempListValueArray)
			}

			let validateDataIdsQuery = `
				SELECT
				count(1) AS count
				FROM
				${dbInfo.schema}.${dbInfo.table} ${dbInfo.table}
				WHERE
				${dbInfo.table}.is_void = false AND
				${dbInfo.table}.id IN (${dataIds.join()})
			`

			let dataIdsCount = await sequelize.query(validateDataIdsQuery, {
				type: sequelize.QueryTypes.SELECT
			})
			
			// check if dataIdCount matches number of unique ids found in dataIds
			if (dataIdsCount[0]['count'] != [...new Set(dataIds)].length) { 
				let errMsg = await errorBuilder.getError(req.headers.host, 400169)
				res.send(new errors.BadRequestError(errMsg))
				return
			}

			let listMemberQuery = format(`
				INSERT INTO
				platform.list_member (
					list_id, data_id, order_number, display_value, remarks, creator_id
				)
				VALUES
				%L
				RETURNING id`, listMembersValuesArray
			)

			listMemberQuery = listMemberQuery.trim()

			transaction = await sequelize.transaction(async transaction => {
				let listMembers = await sequelize.query(listMemberQuery, {
					type: sequelize.QueryTypes.INSERT,
					transaction: transaction,
					raw: true
				}).catch(async err => {
					logger.logFailingQuery(endpoint, 'INSERT', err)
					throw new Error(err)
				})

				for (let listMember of listMembers[0]) {
					let listMemberDbId = listMember.id
					// Return the list member info
					let array = {
						listMemberDbId: listMemberDbId,
						recordCount: 1,
						href: listMemberUrlString + '/' + listMemberDbId
					}

					resultArray.push(array)
				}
			})

			res.send(200, {
			rows: resultArray,
			count: resultArray.length
		})
		return
		} catch (err) {
			logger.logMessage(__filename, err.stack, 'error')
			let errMsg = await errorBuilder.getError(req.headers.host, 500001)
			if(!res.headersSent) res.send(new errors.InternalError(errMsg))
			return
		}
	}
}