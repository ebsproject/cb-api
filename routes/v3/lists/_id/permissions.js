/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let format = require('pg-format')
let logger = require('../../../../helpers/logger')

const endpoint = 'lists/:id/permissions'

module.exports = {
    // Endpoint for retrieving a specific list
    // GET call /v3/lists/:id/permissions
    get: async function (req, res, next) {
        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let conditionString = ''
        let orderString = ''
        let ownership = ''
        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let listDbId = req.params.id

        if (!validator.isInt(listDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let validateListQuery = `
            (
                SELECT
                CASE WHEN
                    (count(1) = 1)
                    THEN 1
                    ELSE 0
                END AS count
                FROM
                    platform.list list
                WHERE
                    list.is_void = FALSE AND
                    list.id = '${listDbId}'
            )
            `
            validateList = await sequelize.query(validateListQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            if (validateList[0]['count'] != 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404031)
                res.send(new errors.NotFoundError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
        
        // Build query
        let listPermissionsQuery = `
            WITH t AS (
                SELECT 
                    ROW_NUMBER() OVER(ORDER BY t."addedOn" DESC) AS "rowDbId",
                    parent.key AS "entity",
                    children.key::integer AS "entityDbId",
                    t.*
                FROM platform.list, 
                    jsonb_each(list.access_data) parent,
                    jsonb_each(parent.value) children,
                    jsonb_to_record(children.value) AS t("addedBy" integer, "addedOn" timestamp, "dataRoleId" integer)
                WHERE
                    list.id = ${listDbId}
            )
            SELECT
                t.*,
                person.person_name AS "personName",
                person.email AS "personEmail",
                person.username AS "personUserName",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                program.program_type AS "programType",
                (CASE 
                    WHEN "personRole".person_role_code = 'DATA_OWNER' OR "personRole".person_role_code = 'DATA_PRODUCER' 
                    THEN 'read_write' ELSE 'read'
                    END) AS permission
            FROM
                t
            LEFT JOIN
                tenant.person person
            ON
                person.id = t."entityDbId"
                AND person.is_void = FALSE
            LEFT JOIN
                tenant.program program
            ON
                program.id = t."entityDbId"
                AND program.is_void = FALSE
            LEFT JOIN
                tenant.person_role "personRole"
            ON
                "personRole".id = t."dataRoleId"
                AND "personRole".is_void = FALSE
        `
        // Get filter condition for abbrev only
        if (
            params.entity !== undefined ||
            params.personName !== undefined ||
            params.programName !== undefined ||
            params.permission !== undefined
        ) {
            let parameters = []

            if (params.entity !== undefined) parameters['entity'] = params.entity
            if (params.personName !== undefined) parameters['personName'] = params.personName
            if (params.programName !== undefined) parameters['programName'] = params.programName
            if (params.permission !== undefined) parameters['permission'] = params.permission

            conditionString = await processQueryHelper.getFilterString(parameters)

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final sql query
        listPermissionsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            listPermissionsQuery,
            conditionString,
            orderString
        )

        // Retrieve listPermissions from the database
        let listPermissions = await sequelize.query(listPermissionsFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await listPermissions == undefined || await listPermissions.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {
            // Get count
            listCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(listPermissionsQuery, conditionString, orderString)

            listCount = await sequelize.query(listCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            count = listCount[0].count
        }

        res.send(200, {
            rows: listPermissions,
            count: count
        })
        return
    },

    // Endpoint for creating a new list permission
    // Implementation of POST call for /v3/lists/:id/permissions
    post: async function (req, res, next) {
        // Set defaults
        let userIds = []
        let listPermissionsValuesArray = []
        let resultArray = []
        let currentAccess = {}
        let listDbId = req.params.id

        if (!validator.isInt(listDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let listPermissionUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/lists/'
            + listDbId
            + '/permissions'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let records = []

        try {
            let validateListQuery = `
                (
                    SELECT
                        id,
                        access_data AS "accessData"
                    FROM
                        platform.list list
                    WHERE
                        list.is_void = FALSE AND
                        list.id = '${listDbId}'
                )
            `
            validateList = await sequelize.query(validateListQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (validateList.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404031)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            currentAccess = validateList[0]['accessData']

            if (currentAccess == null) {
                currentAccess = {}
            }
            // Records = [{}...]
            records = data.records
            if (data.records == undefined || data.records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // default access data
        let dateQuery =format(`
            SELECT NOW()`
        )

        let date = await sequelize.query(dateQuery, {
            type: sequelize.QueryTypes.SELECT,
            transaction: transaction
        })
        date = date[0].now

        // get role code Ids
        // get DATA_PRODUCER and DATA_CONSUMER
        let readId = 0
        let readWriteId = 0
        let roleQuery = `
            SELECT
                id,
                person_role_code AS "roleCode"
            FROM
                tenant.person_role
            WHERE
                person_role_code IN ('DATA_PRODUCER', 'DATA_CONSUMER')
        `
        let roleList = await sequelize.query(roleQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (roleList.length != 2) {
            let errMsg = 'Missing role codes. DATA_PRODUCER and DATA_CONSUMER roles should be present in the database'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        for (var index in roleList) {
            if (roleList[index]['roleCode'] == 'DATA_PRODUCER') {
                readWriteId = roleList[index]['id']
            } else {
                readId = roleList[index]['id']
            }
        }

        try {
            for (var record of records) {
                // validate if id and permission exists
                if (!record.entity || !record.entityId ||
                    !record.permission
                ) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400170)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let entity = record.entity
                let entityId = record.entityId
                let permission = record.permission

                if (permission !== 'read' && permission !== 'read_write') {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400171)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let permissionRole = 0
                if (permission == 'read') {
                    permissionRole = readId
                } else {
                    permissionRole = readWriteId
                }

                if (entity == 'program') {
                    // validate program
                    let validateProgramQuery = `
                        SELECT
                            id
                        FROM
                            tenant.program
                        WHERE
                            id = ${entityId}
                            AND is_void = FALSE
                    `
                    let validateProgram = await sequelize.query(validateProgramQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (validateProgram.length == 0) {
                        let errMsg = 'Program ID ' + entityId + ' does not exist'
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    if (currentAccess.program == undefined) {
                        currentAccess.program = {}
                    }
                    currentAccess.program[entityId] = {
                        "addedBy" : userDbId,
                        "addedOn" : date,
                        "dataRoleId": permissionRole
                    }
                } else if (entity == 'user') {
                    // validate user
                    let validateUserQuery = `
                        SELECT
                            id
                        FROM
                            tenant.person
                        WHERE
                            id = ${entityId}
                            AND is_void = FALSE
                    `
                    let validateUser = await sequelize.query(validateUserQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (validateUser.length == 0) {
                        let errMsg = 'User ID ' + entityId + ' does not exist'
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    if (currentAccess.user == undefined) {
                        currentAccess.user = {}
                    }
                    currentAccess.user[entityId] = {
                        "addedBy" : userDbId,
                        "addedOn" : date,
                        "dataRoleId": permissionRole
                    }
                } else {
                        let errMsg = 'Invalid entity. Value should be program or user'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                }
            }

            currentAccess = JSON.stringify(currentAccess)
            let updateListQuery = `
                UPDATE
                    platform.list list
                SET
                    access_data = '${currentAccess}',
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    list.id = ${listDbId}
            `

            updateListQuery = updateListQuery.trim()

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateListQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err=> {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })
            
            let resultArray = {
                listDbId: listDbId,
                recordCount: 1,
                href: listPermissionUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Show log error messages
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}