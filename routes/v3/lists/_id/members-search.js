/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let productSearchHelper = require('../../../../helpers/queryTool/index.js')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let listMemberHelper = require('../../../../helpers/listsMembers/index.js')
let logger = require('../../../../helpers/logger/index')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let params = req.params
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedConditionString = ''
        let parameters = {}
        let excludedParametersArray = []
        let filterParameters = []
        let searchQuery = ''
        let endpoint = 'lists/_id/members-search'

        // retrieve the list ID
        let listDbId = req.params.id

        // for list members
		let isBasic = false
		let hasData = false

        // check if duplicatesOnly parameter is defined
        parameters['duplicatesOnly'] = false
        if (params.duplicatesOnly !== undefined) {
            if (params.duplicatesOnly !== 'false') {
                parameters['duplicatesOnly'] = true
            }
        }

        if (!validator.isInt(listDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if(req.body != undefined && req.body.isBasic !== undefined && req.body.isBasic == "true"){
            isBasic = true

            delete(req.body.isBasic)
        }

        if(req.body != undefined && req.body.hasData !== undefined && req.body.hasData == "true"){
            hasData = true

            delete(req.body.hasData)
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields + "|listMember.is_active AS isActive"
            }
            // Set columns to be excluded in parameters for filtering
            excludedParametersArray = ['fields']
        
            if (req.body == '') {
                filterParameters = []
            } else {
                filterParameters = req.body
            }

            //default should be isActive = true
            if (req.body.isActive == undefined) {
                filterParameters['isActive'] = 'true'
            }
        } else {
            filterParameters['isActive'] = 'true'
        }

    // Get filter condition
        let condition = ``
        for (column in filterParameters) {
            if (!excludedParametersArray.includes(column)) { 
                let filters = filterParameters[column]
                condition = ``
                condition = await productSearchHelper
                    .getFilterCondition(filters, column)
                if (condition !== '') {
                    condition = `(`+condition+`)`

                    if (searchQuery == '') {
                        searchQuery = searchQuery +
                        ` WHERE `
                        + condition
                    } else {
                        searchQuery = searchQuery +
                        ` AND `
                        + condition
                    }
                }
            }
        }

        conditionString = searchQuery

        // get list information
        let listQuery = `
            SELECT
                list.id AS "listDbId",
                list.abbrev,
                list.name,
                list.display_name AS "displayName",
                list.type,
                entity.id AS "entityDbId",
                entity.name AS entity,
                entity.abbrev AS "entityAbbrev",
                list.description,
                list.remarks,
                list.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                list.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                platform.list list
                LEFT JOIN 
                tenant.person creator ON list.creator_id = creator.id
                LEFT JOIN 
                tenant.person modifier ON list.modifier_id = modifier.id
                LEFT JOIN
                dictionary.entity entity ON entity.id = list.entity_id
            WHERE
                list.is_void = FALSE
                AND list.id = ${listDbId}
            `

        // Retrieve list from the database   
        let lists = await sequelize.query(listQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
            })

        // Return error if retrieval fails
        if (await lists === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Return error if resource is not found
        if (await lists.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404031)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Start building the query
        let listInfo = lists[0]
        let listType = listInfo.type

        // Check if the fields parameter is not empty
        let memberQuery = await listMemberHelper
            .getMembersQuery(listDbId, listInfo.type, parameters['fields'], parameters['duplicatesOnly'], isBasic, hasData)

        if (parameters['duplicatesOnly']) {
            memberQuery = `
                SELECT * FROM (${memberQuery}) as t
                WHERE
                "rowNumber" <> 1
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let memberQueryFinal = memberQuery
        
        // If memberQuery requires outer SELECT wrap
        if(listType != 'package' || (listType == 'package' && !isBasic)){
            memberQueryFinal = await processQueryHelper.getFinalSqlQuery(
                memberQuery,
                conditionString,
                orderString
            )
        }
        else{
            memberQueryFinal = await processQueryHelper.getFinalQuery(
                memberQuery,
                conditionString,
                orderString
            )
        }

        let members = await sequelize.query(memberQueryFinal, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(memberQueryFinal))
            return
        })
    
        if (await members == undefined || await members.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {

        // Get the final count of the members records retrieved
        let membersCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                memberQuery,
                conditionString,
                orderString
            )
        
        // If memberQuery requires outer SELECT wrap
        if(listType == 'package' && isBasic){
            if(membersCountFinalSqlQuery.includes(`LIMIT ${limit} OFFSET ${offset}`)){
                membersCountFinalSqlQuery = membersCountFinalSqlQuery.replaceAll(`LIMIT ${limit} OFFSET ${offset}`,"")
            }
        }
        let membersCount = await sequelize.query(membersCountFinalSqlQuery,{
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                return undefined
            })

        if(membersCount === undefined){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        count = await membersCount[0].count
      
        res.send(200, {
            rows: members,
            count: count
        })
        
        return
        }
    }
}