/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let tokenHelper = require('../../../../helpers/auth/token')
let listsHelper = require('../../../../helpers/lists/index.js')
let logger = require('../../../../helpers/logger')

const endpoint = 'lists/:id'

module.exports = {

    // Endpoint for retrieving a specific list
    // GET call /v3/lists/:id
    get: async function (req, res, next) {
        // Retrieve the list ID
        let listDbId = req.params.id

        if (!validator.isInt(listDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        } else {
            let listsQuery = `
                SELECT
                    list.id AS "listDbId",
                    list.abbrev,
                    list.name,
                    list.display_name AS "displayName",
                    list.type,
                    list.list_sub_type AS "subType",
                    list.entity_id AS "entityId",
                    list.description,
                    list.remarks,
                    list.notes,
                    list.status,
                    list.is_active AS "isActive",
                    list.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS creator,
                    list.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier,
                    list.access_data AS "accessData",
                    list.list_usage AS "listUsage"
                FROM
                    platform.list list
                LEFT JOIN
                    tenant.person creator ON list.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON list.modifier_id = modifier.id
                WHERE
                    list.is_void = FALSE AND
                    list.id = (:listDbId)
            `
            // Retrieve lists from the database
            let lists = await sequelize.query(listsQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    listDbId: listDbId
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            // Return error if resource is not found
            if (lists.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404031)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: lists
            })
            return
        }
    },

    // Endpoint for updating a list record
    // POST call /v3/lists/:id
    put: async function (req, res, next) {
        // Set defaults
        let entityAbbrev = null

        let listDbId = req.params.id

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let listUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/lists/'
            + listDbId

        if (!validator.isInt(listDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        /** Validate is list is existing */

        let validateListQuery = `
            SELECT
                count(1)
            FROM
                platform.list list
            WHERE
                list.is_void = FALSE AND
                list.id = '${listDbId}'
        `
        let validateList = await sequelize.query(validateListQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (validateList[0]['count'] != 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404031)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        /**  Retrieve list creator id for checking in access_data */
        let creatorQuery = ` 
            SELECT
                creator_id as "creatorDbId"
            FROM
                platform.list list
            WHERE 
                list.is_void = FALSE AND
                list.id = ${listDbId} 
        `
        let validateCreator = await sequelize.query(creatorQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (validateCreator[0]['creatorDbId'] == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404031)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let creatorDbid = validateCreator[0]['creatorDbId']

        /** Retrieve current list type for sub type */
        let listTypeQuery = `
            SELECT
                type
            FROM
                platform.list
            WHERE
                is_void = FALSE AND
                id = ${listDbId}
        `

        let listRecord = await sequelize.query(listTypeQuery, {
            type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        let recordListType = listRecord[0].type

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body
        let transaction

        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let setQuery = ``

            // validate abbrev and name should be unique
            let validateQuery = ``
            let validateCount = 0

            if (data.abbrev !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += ` abbrev = $$${data.abbrev}$$`
                // Check if it exists in DB
                validateQuery += `
                    (
                        SELECT
                            CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                            END AS count
                        FROM
                            platform.list list
                        WHERE
                            list.is_void = FALSE AND
                            upper(list.abbrev) = '${data.abbrev}' AND
                            list.id != ${listDbId}
                    )
                `
                validateCount += 1
            }

            if (data.name !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    name = $$${data.name}$$`
                    // Check if it exists in DB
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                    (
                        SELECT
                            CASE WHEN
                                (count(1) = 0)
                                THEN 1
                                ELSE 0
                            END AS count
                        FROM
                            platform.list list
                        WHERE
                            list.is_void = FALSE AND
                            list.name ILIKE '${data.name}' AND
                            list.id != ${listDbId}
                    )
                `
                validateCount += 1
            }

            if (data.type !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                type = $$${data.type}$$`
                
                entityAbbrev = await listsHelper.getEntityGivenType(req,res,data.type)
                
                let entityIdQuery = `
                    SELECT
                        entity.id AS entity_id
                    FROM
                        dictionary.entity entity
                    WHERE
                        abbrev = '${entityAbbrev}'
                `

                let entityId = await sequelize.query(entityIdQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                entityId = entityId[0]['entity_id']

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    entity_id = $$${entityId}$$`
            }

            if (data.displayName !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    display_name = $$${data.displayName}$$`
            }

            // Non-required/optional parameters
            if (data.description !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    description = $$${data.description}$$`
            }

            if (data.remarks !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    remarks = $$${data.remarks}$$`
            }

            if (data.status !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    status = $$${data.status}$$`
            }

            if (data.isActive !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    is_active = $$${data.isActive}$$`
            }

            if (data.accessData !== undefined) {
                if(data.accessData.user[creatorDbid] == undefined){
                    let errMsg = "Creator of list not present in the user property of the new access data"
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                let convert_ad = JSON.stringify(data.accessData)
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                access_data = $$${convert_ad}$$`
            }

            if (data.listUsage !== undefined) {
                // check if value is valid
                if (data.listUsage == 'final list' || data.listUsage == 'working list') {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        list_usage = $$${data.listUsage}$$`
                } else {
                    let errMsg = 'You have provided an invalid list usage value. Please use either final list or working list.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (data.subType !== undefined) {
                let listType = (data.type !== undefined) ? data.type : recordListType

                if (listType == 'germplasm') {
                    if (data.subType == 'entry list' || data.subType == 'sample list') {
                        let errMsg = `You have provided a list subtype that is not applicable to the list type.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (listType == 'package') {
                    if (data.subType == 'population list') {
                        let errMsg = `You have provided a list subtype that is not applicable to the list type.`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `
                    list_sub_type = $$${data.subType}$$
                `

                // Check if it exists in DB
                validateQuery += (validateQuery != '') ? ' + ' : ''
                validateQuery += `
                    (
                        SELECT 
                            count(1)
                        FROM 
                            master.scale_value scaleValue
                        LEFT JOIN
                            master.variable variable ON scaleValue.scale_id = variable.scale_id
                        WHERE
                            variable.abbrev LIKE 'LIST_SUB_TYPE' AND
                            value ILIKE $$${data.subType}$$
                    )
                `
                validateCount += 1
            }

            if (validateCount > 0) {
                // Validate input in DB
                let checkInputQuery = `SELECT ` + validateQuery + ` AS count`
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            let updateListQuery = `
                UPDATE
                    platform.list list
                SET
                    ${setQuery},
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    list.id = ${listDbId}
            `

            // Get transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateListQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            let resultArray = {
                listDbId: listDbId,
                recordCount: 1,
                href: listUrlString
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Show error log messages
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for deleting a list record
    // DELETE call for /v3/lists/:id
    delete: async function (req, res, next) {
        let listDbId = req.params.id

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let listUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/lists'

        if (!validator.isInt(listDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let validateListQuery = `
                SELECT
                    CASE WHEN
                    (count(1) = 1)
                    THEN 1
                    ELSE 0
                    END AS count
                FROM
                    platform.list list
                WHERE
                    list.is_void = FALSE AND
                    list.id = '${listDbId}'
            `
            validateList = await sequelize.query(validateListQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            if (validateList[0]['count'] != 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404031)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let validateListCreatorQuery = `
                SELECT
                    CASE WHEN
                    (count(1) = 1)
                    THEN 1
                    ELSE 0
                    END AS count
                FROM
                    platform.list list
                WHERE
                    list.is_void = FALSE AND
                    list.id = '${listDbId}' AND
                    list.creator_id = ${userDbId}
            `
            validateListCreator = await sequelize.query(validateListCreatorQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            if (validateListCreator[0]['count'] != 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 403005)
                res.send(new errors.ForbiddenError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction

        try {
            let deleteListQuery = `
                UPDATE
                    platform.list list
                SET
                    is_void = TRUE,
                    abbrev = CONCAT(list.abbrev, '-VOIDED-', list.id),
                    name = CONCAT(list.name, '-VOIDED-', list.id),
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    list.id = ${listDbId}
            `
            deleteListQuery = deleteListQuery.trim()
            await sequelize.query(deleteListQuery, {
                type: sequelize.QueryTypes.UPDATE
            })

            let deleteListMemberQuery = `
                UPDATE
                    platform.list_member lm
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    lm.list_id = ${listDbId}
            `

            deleteListMemberQuery = deleteListMemberQuery.trim()

            // Start transaction for list member
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(deleteListMemberQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            let deleteListPermissionQuery = `
                UPDATE
                    platform.list_access la
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    la.list_id = ${listDbId}
            `

            deleteListPermissionQuery = deleteListPermissionQuery.trim()

            // Start transaction for list permission
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(deleteListPermissionQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            let resultArray = {
                listDbId: listDbId,
                recordCount: 1,
                href: listUrlString + '/' + listDbId
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Log error message
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}