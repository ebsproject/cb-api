/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')

let listMemberHelper = require('../../../../helpers/listsMembers/index.js')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let logger = require('../../../../helpers/logger')
const endpoint = 'lists/:id/reorder-list-members'

module.exports = {

    /**
     * POST /v3/lists/:id/reorder-list-members reorders a specific and existing list
     * 
     * @param listDbId integer as a path parameter 
     * @param reorderCondition string 
     * @param token string token
     * 
     * @return object response data
     */

    post: async function (req, res, next) {
        
        let listDbId = req.params.id
        
        if (!validator.isInt(listDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        let tSource = ``
        let reorderCondition = ``

        if (req.body !== undefined) {
            if (req.body.reorderCondition === undefined) {
                let errMsg = `Invalid request, ensure that reorderCondition field is not empty.`
                res.send(new errors.BadRequestError(errMsg))
                return
            }

            reorderCondition = req.body.reorderCondition

            // if reordering is based on sort configuration
            if (reorderCondition === 'SORT' && req.body.sort !== undefined){
                // retrieve list information
                let listQuery = `
                    SELECT
                        list.id AS "listDbId",
                        list.abbrev,
                        list.name,
                        list.display_name AS "displayName",
                        list.type,
                        entity.id AS "entityDbId",
                        entity.name AS entity,
                        entity.abbrev AS "entityAbbrev",
                        list.description,
                        list.remarks,
                        list.creation_timestamp AS "creationTimestamp",
                        creator.id AS "creatorDbId",
                        creator.person_name AS creator,
                        list.modification_timestamp AS "modificationTimestamp",
                        modifier.id AS "modifierDbId",
                        modifier.person_name AS modifier
                    FROM
                        platform.list list
                        LEFT JOIN 
                            tenant.person creator ON list.creator_id = creator.id
                        LEFT JOIN 
                            tenant.person modifier ON list.modifier_id = modifier.id
                        LEFT JOIN
                            dictionary.entity entity ON entity.id = list.entity_id
                    WHERE
                        list.is_void = FALSE AND
                        list.id = ${listDbId}
                    `

                // Retrieve list from the database   
                let lists = await sequelize.query(listQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    .catch(async err => {
                        let errMsg =   `Error encountered in retrieving list record!`;
                        res.send(new errors.InternalError(errMsg));
                        return
                    })

                if(await lists === undefined){
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                }
                
                // Return error if resource is not found
                if (await lists.length < 1) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 404031)
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                // Start building the query
                let listInfo = lists[0]

                // get member query
                let memberQuery = await listMemberHelper.getMembersQuery(
                    listDbId, 
                    listInfo.type, 
                    null, 
                    false
                )

                // extract sort parameters
                let sort = ''
                if(req.body !== undefined && req.body.sort !== undefined){
                    sort = req.body.sort
                }
                
                let sortOrderString = ''
                if(sort !== ''){
                    sortOrderString = await processQueryHelper.getOrderString(sort)
                    if (sortOrderString.includes('invalid')) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                let conditionString = ''

                // get final search query
                let memberQueryFinal = await processQueryHelper.getFinalSqlQuery(
                    memberQuery,
                    conditionString,
                    ''
                )

                memberQueryFinal = memberQueryFinal.replace(" limit (:limit) offset (:offset)", "")

                tSource = `
                    SELECT 
                        listMembersTable."listMemberDbId" AS id, 
                        listMembersTable."orderNumber",
                        ROW_NUMBER() OVER(
                            ${sortOrderString}
                        ) AS num
                    FROM(
                            ${memberQueryFinal}
                        ) AS listMembersTable
                `
            }
            else{
                tSource = `
                    SELECT
                        lm.id,
                        lm.order_number,
                        lm.modification_timestamp,
                        ROW_NUMBER() OVER(
                            ORDER BY 
                                lm.order_number, 
                                lm.modification_timestamp ${reorderCondition}
                        ) AS num
                    FROM
                        platform.list_member lm
                    WHERE
                        lm.is_void = FALSE AND
                        lm.list_id = ${listDbId} AND
                        lm.is_active = TRUE
                `
            }
        }
        else{
            let errMsg = `Request body is empty!`;
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let transaction
        try {
            if(tSource == '' || personDbId == undefined){
                let errMsg = `Error encountered in reordering list member records!`;
                res.send(new errors.InternalError(errMsg))
                return
            }

            let resultArray
            let reorderListMembersQuery = `
                UPDATE
                    platform.list_member lm
                SET
                    order_number = t.num,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                FROM
                    (
                        ${tSource}
                    ) AS t
                WHERE
                    lm.is_void = FALSE AND
                    lm.is_active = TRUE AND
                    lm.id = t.id
            `

            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(reorderListMembersQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })

                resultArray = {
                    listDbId: listDbId
                }
            })

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}