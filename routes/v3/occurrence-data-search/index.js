/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let processQueryHelper = require('../../../helpers/processQuery/index')
let { knex } = require('../../../config/knex')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
const logger = require('../../../helpers/logger')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let parameters = {}
        let excludedParametersArray = []
        let tableAliases = {}
        let columnAliases = {}
        let responseColString = `occurrence_data.id AS "occurrenceDataDbId",
            occurrence_data.occurrence_id AS "occurrenceDbId",
            occurrence.occurrence_name AS "occurrenceName",
            occurrence.description,
            occurrence_data.variable_id AS "variableDbId",
            variable.abbrev AS "variableAbbrev",
            variable.label AS "variableLabel",
            occurrence_data.protocol_id AS "protocolDbId",
            occurrence_data.data_value AS "dataValue",
            occurrence_data.data_qc_code AS "dataQcCode",
            occurrence_data.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS "creator",
            occurrence_data.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS "modifier"`
        let tableJoins = {
            'occurrence': ' JOIN experiment.occurrence occurrence ON occurrence_data.occurrence_id = occurrence.id AND occurrence.is_void = FALSE',
            'variable': ' JOIN master.variable variable ON variable.id = occurrence_data.variable_id AND variable.is_void = FALSE',
            'creator': ' JOIN tenant.person creator ON creator.id = occurrence_data.creator_id',
            'modifier': ' LEFT JOIN tenant.person modifier ON modifier.id = occurrence_data.modifier_id',
        }
        const endpoint = 'occurrence-data-search'
        const operation = 'SELECT'

        if (req.body) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // Set the columns to be excluded in parameters for filtering
            excludedParametersArray = ['fields']

            // Get filter condition
            conditionStringArr = await processQueryHelper.getFilterWithInfo(
                req.body,
                excludedParametersArray,
                responseColString
            )

            conditionString = (conditionStringArr.mainQuery != undefined) ? conditionStringArr.mainQuery : ''
            tableAliases = conditionStringArr.tableAliases
            columnAliases = conditionStringArr.columnAliases

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let occurrencesDataQuery = null

        // Check if the client specified values for the field/s
        if (parameters['fields'] != null) {
            occurrencesDataQuery = knex.column(parameters['fields'].split('|'))

            occurrencesDataQuery += `
                FROM
                    experiment.occurrence_data occurrence_data
                JOIN
                    experiment.occurrence occurrence ON occurrence_data.occurrence_id = occurrence.id AND occurrence.is_void = FALSE
                JOIN
                    master.variable variable ON variable.id = occurrence_data.variable_id AND variable.is_void = FALSE
                JOIN
                    tenant.person creator ON creator.id = occurrence_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = occurrence_data.modifier_id
                WHERE
                    occurrence_data.is_void = FALSE
                ORDER BY
                    occurrence_data.id
            `
        } else {
            occurrencesDataQuery = `
                SELECT
                    ${responseColString}
                FROM
                    experiment.occurrence_data occurrence_data
                JOIN
                    experiment.occurrence occurrence ON occurrence_data.occurrence_id = occurrence.id AND occurrence.is_void = FALSE
                JOIN
                    master.variable variable ON variable.id = occurrence_data.variable_id AND variable.is_void = FALSE
                JOIN
                    tenant.person creator ON creator.id = occurrence_data.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = occurrence_data.modifier_id
                WHERE
                    occurrence_data.is_void = FALSE
                ORDER BY
                    occurrence_data.id
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        occurrencesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            occurrencesDataQuery,
            conditionString,
            orderString
        )

        // Retrieve the occurrence data records
        let occurrenceData = await sequelize.query(occurrencesFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)

                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await occurrenceData == undefined || await occurrenceData.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        } else {

        // Get the final count of the occurrence data records
        let occurrencesDataCountFinalSqlQuery = await processQueryHelper.getTotalCountQuery(
            req.body,
            excludedParametersArray,
            conditionString,
            '',
            tableJoins,
            'experiment.occurrence_data',
            tableAliases,
            'occurrence_data.id',
            columnAliases,
            orderString,
            responseColString
        )

        let occurrencesDataCount = await sequelize.query(occurrencesDataCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, operation, err)
                
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            count = occurrencesDataCount[0].count
        }

        res.send(200, {
            rows: occurrenceData,
            count: count
        })
        return
    }
}