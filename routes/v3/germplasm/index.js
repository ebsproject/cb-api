/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize} = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let format = require('pg-format')
let germplasmHelper = require('../../../helpers/germplasm/index.js')

module.exports = {
  // Implementation of GET call for /v3/germplasm
  get: async function (req, res, next) {
    
    // Set defaults
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let count = 0
    let conditionString = ''
    let orderString = ''
    let parameters = []

    // Build query
    let germplasmQuery = `
      SELECT
        germplasm.id AS "germplasmDbId",
        germplasm.designation AS "designation",
        germplasm.parentage AS "parentage",
        germplasm.generation AS "generation",
        germplasm.germplasm_state AS "germplasmState",
        germplasm.germplasm_name_type AS "germplasmNameType",
        germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
        germplasm.germplasm_code AS "germplasmCode",
        germplasm.germplasm_type AS "germplasmType",
        germplasm.crop_id AS "cropDbId",
        germplasm.taxonomy_id AS "taxonomyDbId",
        germplasm.product_profile_id AS "productProfileDbId",
        germplasm.creator_id AS "creatorDbId",
        germplasm.creation_timestamp AS "creationTimestamp",
        germplasm.modifier_id AS "modifierDbId",
        germplasm.modification_timestamp AS "modificationTimestamp"
      FROM
        germplasm.germplasm germplasm
      LEFT JOIN
        tenant.crop crop ON crop.id = germplasm.crop_id AND
        crop.is_void = FALSE
      LEFT JOIN
        germplasm.product_profile productProfile ON productProfile.id = germplasm.product_profile_id AND
        productProfile.is_void = FALSE
      WHERE
        germplasm.is_void = FALSE
      ORDER BY
        germplasm.id
    `

    // get filter for germplasm name only
    if (params.germplasm !== undefined) {
      parameters['germplasm'] = params.germplasm.trim().toLowerCase()
    }

    conditionString = await processQueryHelper.getFilterString(parameters)   
       
    if (conditionString.includes('invalid')) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400022)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)

      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final sql query
    let germplasmFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      germplasmQuery,
      conditionString,
      orderString
    )
    
    // Retrieve germplasm from the database
    let germplasm = await sequelize.query(germplasmFinalSqlQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        limit: limit,
        offset: offset
      }
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    if (await germplasm == undefined || await germplasm.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      // Get count
      let germplasmCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(germplasmQuery, conditionString, orderString)

      let germplasmCount = await sequelize.query(germplasmCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      count = (germplasmCount !== undefined && germplasmCount[0] !== undefined) ? germplasmCount[0].count : 0
    }

    res.send(200, {
      rows: germplasm,
      count: count
    })
    return
  },
  post: async function (req, res, next) {
    let designation = null
    let parentage = null
    let generation = null
    let state = null
    let nameType = null
    let normalizedName = null
    let cropId = null
    let taxonomyId = null
    let productProfileId = null
    let germplasmType = null

    let userDbId = await tokenHelper.getUserId(req)

    if (userDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    
    // Check if the connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure
    // Set URL for response
    let germplasmUrlString = (isSecure ? 'https' : 'http')
      + '://'
      + req.headers.host
      + '/v3/germplasm'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = [] 
    let recordCount = 0

    try {
      // Records = [{}...]
      records = data.records
      recordCount = records.length
      // Check if the input data is empty
      if (records == undefined || records.length == 0) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } 
    catch (e) {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    }

    // Start transaction
    let transaction
    try {
      // Get transaction
      transaction = await sequelize.transaction({ autocommit: false })

      let germplasmValuesArray = []
      for (var record of records) {
        let validateQuery = ``
        let validateCount = 0
        // check if the required columns are in the input
        if (
          record.designation === undefined 
          || record.parentage === undefined 
          || record.generation === undefined
          || record.state === undefined 
          || record.nameType === undefined
          || record.cropDbId === undefined
          || record.taxonomyDbId === undefined) {
            let errMsg = 'Required parameters are missing. Ensure that the designation, parentage, generation, state, name type, crop ID, and taxonomy ID fields are not empty.'
            res.send(new errors.BadRequestError(errMsg))
            return
          }
          //validate crop id, product prfile id if they exist, and designation if unique
          if (record.cropDbId != undefined){
            cropId = record.cropDbId
            if (!validator.isInt(cropId)) {
              let errMsg = "Invalid format, crop ID must be an integer"
              res.send(new errors.BadRequestError(errMsg))
              return
            }
            validateQuery += (validateQuery != '') ? ' + ' : ''
            validateQuery += `
              (
                SELECT
                  count(*)
                FROM
                  tenant.crop crop
                WHERE
                  crop.id = ${cropId}
                  AND crop.is_void = FALSE
              )
            `
            validateCount += 1
          }

          if (record.taxonomyDbId != undefined){
            taxonomyId = record.taxonomyDbId
            if (!validator.isInt(taxonomyId)) {
              let errMsg = "Invalid format, taxonomy ID must be an integer"
              res.send(new errors.BadRequestError(errMsg))
              return
            }
            validateQuery += (validateQuery != '') ? ' + ' : ''
            validateQuery += `
              (
                SELECT
                  count(*)
                FROM
                  germplasm.taxonomy taxonomy
                WHERE
                  taxonomy.id = ${taxonomyId}
                  AND taxonomy.is_void = FALSE
              )
            `
            validateCount += 1
          }

          if (record.designation  != undefined){
            designation = record.designation 
            //getting normalized name from designation using germplasm helper
            normalizedName = await germplasmHelper.normalizeText(designation)
            validateQuery += (validateQuery != '') ? ' + ' : ''
            validateQuery += `
                (
                  SELECT
                    CASE WHEN
                      (count(1) = 0)
                      THEN 1
                      ELSE 0
                    END AS count
                  FROM
                    germplasm.germplasm germplasm
                  WHERE
                    germplasm.is_void = FALSE AND
                    germplasm.germplasm_normalized_name = $$${normalizedName}$$
                )
              `
              validateCount += 1
          }
          //getting normalized name from designation useing germplasm helper
          normalizedName = await germplasmHelper.normalizeText(designation)

          //storing values from user

          if (record.productProfileDbId  != undefined){
            productProfileId = record.productProfileDbId
            if (!validator.isInt(productProfileId)) {
              let errMsg = "Invalid format, Product Profile ID must be an integer"
              res.send(new errors.BadRequestError(errMsg))
              return
            }
          }
          parentage = record.parentage
          generation = record.generation
          state = record.state
          nameType = record.nameType

          if (record.germplasmType !== undefined) germplasmType = record.germplasmType

          if (validateCount > 0) {
            let checkInputQuery = `
              SELECT ${validateQuery} AS count
            `
            let inputCount = await sequelize.query(checkInputQuery, {
              type: sequelize.QueryTypes.SELECT
            })
            if (inputCount[0]['count'] != validateCount) {
              let errMsg = await errorBuilder.getError(req.headers.host, 400015)
              res.send(new errors.BadRequestError(errMsg))
              return
            }
          }

          let tempArray = [
            designation,
            parentage,
            generation,
            state,
            nameType,
            normalizedName,
            cropId,
            taxonomyId,
            productProfileId,
            userDbId,
            germplasmType
          ]
          germplasmValuesArray.push(tempArray)
        }

        // Create germplasm
        let germplasmQuery = format (`
          INSERT INTO germplasm.germplasm 
            (designation, parentage, generation, germplasm_state, germplasm_name_type, germplasm_normalized_name, crop_id, taxonomy_id,
            product_profile_id, creator_id, germplasm_type)
          VALUES 
            %L
          RETURNING id`, germplasmValuesArray
        )
        let germplasms = await sequelize.query(germplasmQuery, {
          type: sequelize.QueryTypes.INSERT,
          transaction: transaction
        })
        let resultArray = []
        for (let germplasm of germplasms[0]) {
          germplasmDbId = germplasm.id
          // Return the germplasm info to Client
          let array = {
            germplasmDbId: germplasmDbId,
            recordCount: 1,
            href: germplasmUrlString + '/' + germplasmDbId
          }
          resultArray.push(array)
        }
        await transaction.commit()

        res.send(200, {
            rows: resultArray
        })
        return
      } 
      catch (err) {
        // Rollback transaction if any errors were encountered
        if (err) await transaction.rollback()
        let errMsg = await errorBuilder.getError(req.headers.host, 500001)
        res.send(new errors.InternalError(errMsg))
        return            
      }
  }
}