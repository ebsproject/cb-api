/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')


module.exports = {

  // Implementation of GET call for /v3/germplasm/{id}/names
  get: async function (req, res, next) {

    // Retrieve the germplasm ID

    let germplasmDbId = req.params.id

    if (!validator.isInt(germplasmDbId)) {
      let errMsg = "Invalid format, germplasm ID must be an integer"
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    let validateGermplasmQuery = `
      SELECT
        count(*)
      FROM
        germplasm.germplasm germplasm
      WHERE
        germplasm.is_void = FALSE AND
        germplasm.id = ${germplasmDbId}
    `
    let validateGermplasm = await sequelize.query(validateGermplasmQuery, {
      type: sequelize.QueryTypes.SELECT
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    if (validateGermplasm[0]['count'] != 1) {
      let errMsg = "The germplasm you have requested does not exist"
      res.send(new errors.NotFoundError(errMsg))
      return
    }
    // Build query
    let germplasmNamesQuery = `
    SELECT
      "name".id as "nameDbId",
      "name".name_value as "nameValue",
      "name".germplasm_name_type as "nameType",
      "name".germplasm_name_status as "nameStatus",
      "name".germplasm_normalized_name as "normalizedName",
      "creator".person_name AS "creator",
      "name".creation_timestamp AS "creationTimestamp",
      "modifier".person_name AS "modifier",
      "name".modification_timestamp AS "modificationTimestamp"
    FROM
      germplasm.germplasm_name "name"
    LEFT JOIN 
      germplasm.germplasm germplasm ON germplasm.id = name.germplasm_id
    INNER JOIN tenant.person AS creator
      ON creator.id = name.creator_id
    LEFT JOIN tenant.person AS modifier
      ON modifier.id = name.modifier_id
    WHERE
      germplasm.is_void = FALSE
      AND name.is_void = FALSE
      AND name.germplasm_id = ${germplasmDbId}
    `

    // Retrieve germplasm from the database   
    let germplasmNames = await sequelize.query(germplasmNamesQuery, {
      type: sequelize.QueryTypes.SELECT
    })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    // Return error if resource is not found
    if (await germplasmNames == undefined || await germplasmNames.length < 1) {
      res.send(200, {
        rows: germplasmNames,
        count: 0
      })
      return
    } 
    else {
      // Get count
      let germplasmNamesCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(germplasmNamesQuery)
      let germplasmNamesCount = await sequelize.query(germplasmNamesCountFinalSqlQuery, {
        type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      count = germplasmNamesCount[0].count

      res.send(200, {
        rows: germplasmNames,
        count: count
      })
      return
    }
  }
 
}