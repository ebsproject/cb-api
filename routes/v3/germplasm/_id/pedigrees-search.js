/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let validator = require('validator')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

// import helpers
let germplasmHelper = require('../../../../helpers/germplasm/index')
let logger = require('../../../../helpers/logger/index')

module.exports = {
    // retrieve pedigree information for a given germplasm ID
    post: async function (req, res, next) {
        const endpoint = 'germplasm/:id/pedigrees-search'

        // instatiate variables
        let germplasm
        let pedigree = []
        let count = 0

        let depth

        // retrieve pagination params
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort

        // retrieve params
        let params = req.params
        let reqBody = req.body
        let germplasmDbId = params.id

        let conditionStr = ''
        let errMsg = ''

        // validate germplasm ID value and format
        if(await germplasmHelper.germplasmIdHasNoValue(germplasmDbId)){
            errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.InternalError(errMsg))
            return
        }

        if(await germplasmHelper.germplasmIdHasInvalidFormat(germplasmDbId)){
            errMsg = await errorBuilder.getError(req.headers.host, 400246)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // check if has germplasm record
        let additionalCondition = `AND germplasm.id = ${germplasmDbId}`
        germplasm = await germplasmHelper.getGermplasmRecord(req, res, endpoint, additionalCondition)

        if (germplasm == undefined || germplasm == null){
            errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        if(germplasm.length < 1){
            let errMsg = await errorBuilder.getError(req.headers.host, 404042)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // process request body
        if(reqBody != undefined && reqBody != null){
            if(reqBody.depth != null){
                depth = reqBody.depth

                if(!validator.isInt(depth)) {
                    errMsg = await errorBuilder.getError(req.headers.host, 400022)
                    res.send(new errors.InternalError(errMsg))
                    return
                }

                conditionStr = ` WHERE `

                conditionStr += `
                    tree.depth <= ${depth}
                `
            }
        }

        let ancestryRelationsQuery = await germplasmHelper.getAncestryRelationRecordsQuery(germplasmDbId)
        
        // add conditions to query
        let germplasmPedigreeQuery = `
            ${ancestryRelationsQuery},
            collatedRes AS (
                SELECT
                    *
                FROM
                    tree
                ${conditionStr}
            )
        `

        // extract total count
        germplasmPedigreeQuery += `
            ,
            countCte AS (
                SELECT
                    COUNT(1) AS "totalCount"
                FROM
                    collatedRes
            )
        `
        
        // add limit to filtered results
        germplasmPedigreeQuery += `
            ,
            withLimitRes AS (
                SELECT
                    *
                FROM
                    collatedRes
                LIMIT (:limit) OFFSET (:offset)
            )
        `

        // add further parent information
        germplasmPedigreeQuery += `
            ,
            withChildRes AS ( -- add child germplasm information
                SELECT
                    withLimitRes.*,
                    germplasm.id AS "childGermplasmDbId",
                    germplasm.designation AS "childGermplasmDesignation"
                FROM
                    withLimitRes
                    JOIN germplasm.germplasm germplasm ON germplasm.id = withLimitRes.child
            ),
            withParentRes AS ( -- add parent germplasm information
                SELECT
                    withChildRes.*,
                    germplasm.id AS "parentGermplasmDbId",
                    germplasm.designation AS "parentGermplasmDesignation",
                    CASE
                        WHEN withChildRes.order_number = 1 THEN 'Female'
                        ELSE 'Male'
                    END AS "parentGermplasmRole",
                    CASE
                        WHEN withChildRes.order_number = 1 THEN 'F'
                        ELSE 'M'
                    END AS "parentTypeCode"
                FROM
                    withChildRes
                    JOIN germplasm.germplasm germplasm ON germplasm.id = withChildRes.root
            ),
            allRes AS (
                SELECT
                    withParentRes."childGermplasmDbId",
                    withParentRes."childGermplasmDesignation",
                    withParentRes."parentGermplasmDbId",
                    withParentRes."parentGermplasmDesignation",
                    withParentRes."parentGermplasmRole",
                    withParentRes."parentTypeCode",
                    withParentRes.depth AS "lineName"
                FROM
                    withParentRes
                ORDER BY withParentRes.depth ASC
            )
            SELECT
                countCte."totalCount",
                allRes.*
            FROM
                countCte,
                allRes
        `

        depth = depth ?? await germplasmHelper.getConfigDepthValue()

        let ancestryRelationsData = await germplasmHelper.getAncestryRelationRecordsWithDbFunction(
            req,
            res,
            endpoint,
            germplasmDbId,
            parseInt(depth))

        pedigree = ancestryRelationsData
        
        if (pedigree == undefined || pedigree == null){
            errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        count = pedigree.length ?? 0

        // return extracted values
        res.send(200, {
            rows: pedigree,
            count: count
        })

        return
    }
}