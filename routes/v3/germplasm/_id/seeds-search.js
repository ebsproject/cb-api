/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let parameters = {}
        parameters['distinctOn'] = ''
        let addedOrderString = `
            ORDER BY
                seed.id
        `

        let germplasmDbId = req.params.id

        if (!validator.isInt(germplasmDbId)) {
            let errMsg = 'Invalid request, germplasm ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let germplasmQuery = `
            SELECT
                germplasm.id AS "germplasmDbId"
            FROM
                germplasm.germplasm germplasm
            WHERE
                germplasm.is_void = FALSE AND
                germplasm.id = ${germplasmDbId}
        `

        // Retrieve the germplasm record using the ID
        let germplasm = await sequelize
            .query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Check if germplasm exists
        if (await germplasm == undefined || await germplasm.length < 1) {
            let errMsg = 'Resource not found, germplasm does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            // If distinctOn condition is set
            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(parameters['distinctOn'])

                parameters['distinctOn'] = `"${parameters['distinctOn']}",`
            }

            // Set the parameters that will be excluded for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]

            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let seedsQuery = null 

        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields']
                )

                let selectString = knex.raw(`${addedDistinctString} ${fieldString}`)

                seedsQuery = knex.select(selectString)
            } else {
                seedsQuery = knex.column(parameters['fields'].split('|'))
            }

            seedsQuery += `
                FROM
                    germplasm.seed seed
                LEFT JOIN (
                    SELECT
                        pkg.seed_id,
                        count(1) AS package_count
                    FROM
                        germplasm.package AS pkg
                    WHERE
                        pkg.is_void = FALSE
                    GROUP BY
                        pkg.seed_id
                ) AS pkg (seed_id, package_count)
                    ON seed.id = pkg.seed_id
                LEFT JOIN
                    tenant.program program ON seed.program_id = program.id AND
                    program.is_void = FALSE
                LEFT JOIN
                    experiment.entry entry ON seed.source_entry_id = entry.id
                    AND entry.is_void = FALSE
                LEFT JOIN
                    experiment.experiment experiment ON seed.source_experiment_id = experiment.id AND
                    experiment.is_void = FALSE
                LEFT JOIN (
                    SELECT
                        el.experiment_id,
                        count(1) AS entry_count
                    FROM
                        experiment.entry_list el
                    INNER JOIN
                        experiment.entry e
                        ON el.id = e.entry_list_id
                        AND e.is_void = FALSE
                    WHERE
                        e.entry_list_id = el.id
                    GROUP BY
                        el.experiment_id
                ) AS ent (experiment_id, entry_count)
                    ON experiment.id = ent.experiment_id
                LEFT JOIN
                    experiment.location location ON seed.source_location_id = location.id AND location.is_void = FALSE
                LEFT JOIN
                    experiment.occurrence occurrence ON seed.source_occurrence_id = occurrence.id AND occurrence.is_void = FALSE
                LEFT JOIN
                    experiment.plot plot ON seed.source_plot_id = plot.id AND plot.is_void = FALSE
                LEFT JOIN
                    tenant.season season ON season.id = experiment.season_id
                LEFT JOIN
                    tenant.stage stage ON stage.id = experiment.stage_id
                WHERE
                    seed.germplasm_id = ${germplasmDbId}AND
                    seed.is_void = FALSE
                    ${addedOrderString}
            `
        } else {
            seedsQuery = `
                SELECT
                    ${addedDistinctString}
                    seed.id AS "seedDbId",
                    seed.seed_code AS "seedCode",
                    seed.seed_name AS "seedName",
                    seed.harvest_date AS "harvestDate",
                    seed.harvest_method AS "harvestMethod",
                    seed.germplasm_id AS "germplasmDbId",
                    program.id AS "programDbId",
                    program.program_code AS "programCode",
                    program.program_name AS "programName",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_year AS "experimentYear",
                    experiment.experiment_type AS "experimentType",
                    season.season_code AS "experimentSeason",
                    stage.stage_code AS "experimentStageCode",
                    COALESCE(ent.entry_count, 0) AS "entryCount",
                    entry.entry_name AS "entryName",
                    experiment.planting_season AS "experimentPlantingSeason",
                    COALESCE(pkg.package_count, 0) AS "packageCount",
                    seed.source_entry_id AS "sourceEntryDbId",
                    seed.source_occurrence_id AS "sourceOccurrenceDbId",
                    seed.source_location_id AS "sourceLocationDbId",
                    seed.source_plot_id AS "sourcePlotDbId"
                FROM
                    germplasm.seed seed
                LEFT JOIN (
                    SELECT
                        pkg.seed_id,
                        count(1) AS package_count
                    FROM
                        germplasm.package AS pkg
                    WHERE
                        pkg.is_void = FALSE
                    GROUP BY
                        pkg.seed_id
                ) AS pkg (seed_id, package_count)
                    ON seed.id = pkg.seed_id
                LEFT JOIN
                    tenant.program program ON seed.program_id = program.id AND
                    program.is_void = FALSE
                LEFT JOIN
                    experiment.entry entry ON seed.source_entry_id = entry.id
                    AND entry.is_void = FALSE
                LEFT JOIN
                    experiment.experiment experiment ON seed.source_experiment_id = experiment.id AND
                    experiment.is_void = FALSE
                LEFT JOIN (
                    SELECT
                        el.experiment_id,
                        count(1) AS entry_count
                    FROM
                        experiment.entry_list el
                    INNER JOIN
                        experiment.entry e
                        ON el.id = e.entry_list_id
                        AND e.is_void = FALSE
                    WHERE
                        e.entry_list_id = el.id
                    GROUP BY
                        el.experiment_id
                ) AS ent (experiment_id, entry_count)
                    ON experiment.id = ent.experiment_id
                LEFT JOIN
                    experiment.location location ON seed.source_location_id = location.id AND location.is_void = FALSE
                LEFT JOIN
                    experiment.occurrence occurrence ON seed.source_occurrence_id = occurrence.id AND occurrence.is_void = FALSE
                LEFT JOIN
                    experiment.plot plot ON seed.source_plot_id = plot.id AND plot.is_void = FALSE
                LEFT JOIN
                    tenant.season season ON season.id = experiment.season_id
                LEFT JOIN
                    tenant.stage stage ON stage.id = experiment.stage_id
                WHERE
                    seed.germplasm_id = ${germplasmDbId}AND
                    seed.is_void = FALSE
                    ${addedOrderString}
            `
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let seedsFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            seedsQuery,
            conditionString,
            orderString,
        )

        let seeds = await sequelize
            .query(seedsFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
                return
            })

        if (await seeds == undefined || await seeds.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })

            return
        }

        let seedsCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                seedsQuery,
                conditionString,
                orderString,
            )

        let seedsCount = await sequelize
            .query(seedsCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = seedsCount[0].count

        res.send(200, {
            rows: seeds,
            count: count,
        })

        return
    }
}