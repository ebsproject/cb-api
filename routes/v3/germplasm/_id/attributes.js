/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {

    // Implementation of GET call for /v3/germplasm/:id/attributes
    get: async function (req, res, next) {
        let limit = req.paginate.limit;
        let offset = req.paginate.offset

        let params = req.query
        let conditionString = ""

        // retrieve the germplasm Id
        let germplasmDbId = req.params.id

        if (!validator.isInt(germplasmDbId)) {
            let errMsg = "Invalid format, germplasm ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // get filter condition for abbrev
        if (params.variableAbbrev !== undefined && params.variableAbbrev !== null && params.variableAbbrev !== "") {
            let parameters = {
                variableAbbrev: params.variableAbbrev
            }

            conditionString = await processQueryHelper.getFilterString(parameters)
            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400003)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        let germplasmQuery = `
            SELECT
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.designation AS "designation",
                germplasm.is_void AS "isVoid"
            FROM
                germplasm.germplasm germplasm
            WHERE
                germplasm.is_void = FALSE
                AND germplasm.id = ${germplasmDbId}
        `
        let germplasm = await sequelize.query(germplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await germplasm == undefined || await germplasm.length < 1) {
            let errMsg = "The germplasm you have requested does not exist"
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        //build query
        let germplasmAttributesQuery = `
            SELECT
                attribute.id AS "germplasmAttributeDbId",
                attribute.data_value AS "attributeDataValue",
                attribute.creation_timestamp AS "creationTimestamp",
                attribute.creator_id AS "creatorDbId",
                creator.person_name AS "creator",
                variable.id AS "variableDbId",
                variable.abbrev AS "variableAbbrev",
                variable.display_name AS "variableDisplayName",
                scale_value.remarks AS "scaleValueRemarks"
            FROM
                germplasm.germplasm_attribute attribute
                JOIN
                    master.variable variable ON attribute.variable_id = variable.id
                LEFT JOIN
                    master.scale_value scale_value ON scale_value.scale_id = variable.scale_id 
                    AND (attribute.data_value = scale_value.value OR attribute.data_value = scale_value.display_name)
                    AND scale_value.is_void = FALSE
                JOIN
                    tenant.person creator ON creator.id = attribute.creator_id
            WHERE
                attribute.germplasm_id = ${germplasmDbId}
                AND attribute.is_void = FALSE
        `

        let updatedGermplasmAttributesQuery = germplasmAttributesQuery
        if (conditionString !== "") {
            updatedGermplasmAttributesQuery = await processQueryHelper.getFinalSqlQuery(
                germplasmAttributesQuery,
                conditionString,
                ""
            )
        }

        // retrieve the attributes from the database
        let germplasmAttributes = await sequelize.query(updatedGermplasmAttributesQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch (async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        germplasm[0]['attributes'] = germplasmAttributes
        
        // get count
        let germplasmAttributesCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            germplasmAttributesQuery,
            conditionString, 
            '')
        let germplasmAttributesCount = await sequelize.query(germplasmAttributesCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await germplasmAttributesCount == undefined || await germplasmAttributesCount.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        count = germplasmAttributesCount[0].count
        res.send(200, {
            rows: germplasm,
            count: count
        })
        return
    }
}