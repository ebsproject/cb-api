/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving the number of experiment entries using the germplasm
    // GET /v3/germplasm/:id/entries-count
    get: async function(req, res, next) {
        let count = 0

        // Get germplasm ID from params
        let germplasmDbId = req.params.id

        // Validate format of germplasm ID
        if (!validator.isInt(germplasmDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400238)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Build germplasm query
        let germplasmQuery = `
            SELECT
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.designation AS "germplasmName",
                germplasm.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                germplasm.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                germplasm.germplasm germplasm
            LEFT JOIN
                tenant.person creator ON germplasm.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON germplasm.modifier_id = modifier.id
            WHERE
                germplasm.is_void = FALSE AND
                germplasm.id = ${germplasmDbId}
        `

        // Retrieve the germplasm record using the ID
        let germplasm = await sequelize
            .query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (germplasm == undefined) return 
        if (germplasm == null || germplasm.length < 1) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404046)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Build the entries value retrieval query
        entriesQuery = `
        SELECT
            entry.id AS "entryDbId",
            entry.description,
            entry.notes
        FROM
            experiment.entry entry
        WHERE
            entry.is_void = FALSE
            AND entry.germplasm_id = ${germplasmDbId}
        ORDER BY
            entry.id
        `

        // Build final entries count query
        let entriesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(entriesQuery)
        
        // Retrieve entries count
        let entriesCount = await sequelize
            .query(
                entriesCountFinalSqlQuery, {
                    type: sequelize.QueryTypes.SELECT,
                }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })
                
        
        // If entriesCount is undefined, return
        if (entriesCount === undefined) {
            return
        }

        // Add entries count to the germplasm object
        count = germplasm.count
        germplasm[0]['entriesCount'] = entriesCount[0].count

        res.send(200, {
            rows: germplasm,
            count: count
        })
        return
    }
}