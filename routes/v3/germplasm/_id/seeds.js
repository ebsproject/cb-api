/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')
let logger = require('../../../../helpers/logger/index.js')
let endpoint = 'germplasm/:id/seeds'

module.exports = {

	// Implementation of GET call for /v3/germplasm/:id/seeds
	get: async function (req, res, next) {
		// Set defaults 
		let limit = req.paginate.limit;
		let offset = req.paginate.offset;
		let sort = req.query.sort;
		let orderString = '';
		let conditionString = '';

		// retrieve the germplasm Id
		let germplasmDbId = req.params.id

		if (!validator.isInt(germplasmDbId)) {
			let errMsg = "Invalid format, germplasm ID must be an integer"
			res.send(new errors.BadRequestError(errMsg))
			return
		}

		let validateGermplasmQuery = `
			SELECT count(*)
			FROM
				germplasm.germplasm germplasm
			WHERE
				germplasm.is_void = FALSE
				AND germplasm.id = ${germplasmDbId}
		`
		let validateGermplasm = await sequelize.query(validateGermplasmQuery, {
			type: sequelize.QueryTypes.SELECT
		})
			.catch(async err => {
				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

		if (await validateGermplasm == undefined || await validateGermplasm[0]['count'] != 1) {
			let errMsg = "The germplasm you have requested does not exist"
			res.send(new errors.NotFoundError(errMsg))
			return
		}

		if (sort != null) {
			orderString = await processQueryHelper.getOrderString(sort)

			if (orderString.includes('invalid')) {
				let errMsg = await errorBuilder.getError(req.headers.host, 400004)
				res.send(new errors.BadRequestError(errMsg))
				return
			}
		}

		//build query
		let germplasmSeedsQuery = `
				SELECT
					seed.id AS "seedDbId",
					seed.seed_code AS "seedCode",
					seed.seed_name AS "seedName",
					seed.harvest_date AS "harvestDate",
					seed.harvest_method AS "harvestMethod",
					program.id AS "programDbId",
					program.program_code AS "programCode",
					program.program_name AS "programName",
					experiment.id AS "experimentDbId",
					experiment.experiment_name AS "experimentName",
					experiment.experiment_year AS "experimentYear",
					experiment.experiment_type AS "experimentType",
					occurrence.occurrence_name AS "experimentOccurrence",
					occurrence.id AS "sourceOccurrenceDbId",
					occurrence.occurrence_name AS "sourceOccurrenceName",
					"entry".entry_code AS "sourceEntryCode",
					"entry".entry_number AS "seedSourceEntryNumber",
					plot.plot_code AS "seedSourcePlotCode",
					plot.plot_number AS "seedSourcePlotNumber",
					plot.rep AS "replication",
					(
					SELECT
							s.stage_code
					FROM
							tenant.stage s
					WHERE
							s.is_void = FALSE AND
							s.id = experiment.id
					) AS "experimentStageCode",
					(
					SELECT
							count(e.id)
					FROM
							experiment.entry_list el
							INNER JOIN
									experiment.entry e
							ON el.id = e.entry_list_id
									AND e.is_void = FALSE
					WHERE
							el.experiment_id = experiment.id AND
							e.entry_list_id = el.id
					)::int AS "entryCount",
					entry.entry_name AS "entryName",
					experiment.planting_season AS "experimentPlantingSeason"
				FROM
					germplasm.seed seed
					INNER JOIN
						germplasm.germplasm germplasm ON seed.germplasm_id = germplasm.id AND germplasm.is_void = FALSE
					LEFT JOIN
						tenant.program program ON seed.program_id = program.id AND PROGRAM.is_void = false
					LEFT JOIN
						experiment.experiment experiment ON seed.source_experiment_id = experiment.id AND experiment.is_void = FALSE
					LEFT JOIN
						experiment.location location ON seed.source_location_id = location.id AND LOCATION.is_void = FALSE
					LEFT JOIN
						experiment.occurrence occurrence ON seed.source_occurrence_id = occurrence.id AND occurrence.is_void = FALSE
					LEFT JOIN
						experiment.plot plot ON plot.id = seed.source_plot_id
						AND plot.is_void = FALSE
					LEFT JOIN
						experiment.entry "entry" ON "entry".id = seed.source_entry_id
						AND entry.is_void = FALSE
				WHERE
					germplasm.id = ${germplasmDbId} AND
					seed.is_void = FALSE
		`

		let finalGermplasmSeedQuery = await processQueryHelper.getFinalSqlQuery(
			germplasmSeedsQuery,
			conditionString,
			orderString
		);

		// retrieve the seeds from the database
		let germplasmSeeds = await sequelize.query(finalGermplasmSeedQuery, {
			type: sequelize.QueryTypes.SELECT,
			replacements: {
				limit: limit,
				offset: offset
			}
		})
			.catch(async err => {
				await logger.logFailingQuery(endpoint, 'SELECT', err)

				let errMsg = await errorBuilder.getError(req.headers.host, 500004)
				res.send(new errors.InternalError(errMsg))
				return
			})

		if (await germplasmSeeds == undefined || await germplasmSeeds.length < 1) {
			res.send(200, {
				rows: [],
				count: 0
			})
			return
		} else {
			// get count
			let germplasmSeedsCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(finalGermplasmSeedQuery)
			let germplasmSeedsCount = await sequelize.query(finalGermplasmSeedQuery, {
				type: sequelize.QueryTypes.SELECT,
				replacements: {
					limit: limit,
					offset: offset
				}
			})
				.catch(async err => {
					await logger.logFailingQuery(endpoint, 'SELECT', err)

					let errMsg = await errorBuilder.getError(req.headers.host, 500004)
					res.send(new errors.InternalError(errMsg))
					return
				})

			if (await germplasmSeedsCount == undefined || await germplasmSeedsCount.length < 1) {
				res.send(200, {
					rows: [],
					count: 0
				})
				return
			}

			count = germplasmSeedsCount[0].count
			res.send(200, {
				rows: germplasmSeeds,
				count: count
			})
			return
		}
	}
}