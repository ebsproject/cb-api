/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let format = require('pg-format')

module.exports = {

    /**
     * POST /v3/germplasm/:id/delete-family-members
     * 
     * Deletes family member records given the germplasm ID
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */

    post: async function (req, res, next) {

        // Retrieve germplasm ID
        let germplasmDbId = req.params.id

        if (!validator.isInt(germplasmDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400246)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }
        
        let germplasmQuery = `
            SELECT EXISTS(
                SELECT 1 
                FROM 
                    germplasm.germplasm g 
                WHERE 
                    g.id = ${germplasmDbId}
                    AND g.is_void= FALSE
            )
        `

        // Retrieve germplasm
        let germplasm = await sequelize.query(germplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await germplasm === undefined) {
            return
        }
        if (await germplasm === null || await !germplasm[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404042)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let seedQuery = `
            SELECT EXISTS (
                SELECT 1
                FROM
                    germplasm.germplasm g
                    LEFT JOIN germplasm.seed s ON s.germplasm_id = g.id
                WHERE
                    g.id = ${germplasmDbId}
                    AND s.id IS NOT NULL
                    AND s.is_void = FALSE
            )
        `

        // Retrieve seed
        let seed = await sequelize.query(seedQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await seed === undefined) {
            return
        }
        if (await seed === null || await seed[0]["exists"]) {
            let errMsg = `User is not allowed to delete germplasm and related data with existing seeds.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Retrieve family member record
        let familyMemberQuery = `
            SELECT
                family_member.id AS "familyMemberDbId",
                family_member.family_id AS "familyDbId",
                family_member.germplasm_id AS "germplasmDbId"
            FROM
                germplasm.family_member family_member
            WHERE
                family_member.is_void = FALSE
                AND family_member.germplasm_id = ${germplasmDbId}
        `

        // Retrieve family record
        let familyMember = await sequelize.query(familyMemberQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if (await familyMember == undefined) {
            return
        }
        // If the germplasm does not have a family member record, 
        // return an empty array.
        else if (await familyMember == null || await familyMember.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let familyDbId = familyMember[0]['familyDbId']

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let resultArray = []
            let familyMembersObj = {}

            // Delete family member record
            let deleteQuery = format(`
                UPDATE
                    germplasm.family_member family_member 
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE 
                    family_member.germplasm_id = ${germplasmDbId}
                    AND family_member.is_void = FALSE
                RETURNING family_member.id
        
            `)

            let familyMemberRecords = await sequelize.query(deleteQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            await transaction.commit()

            // Check if family has any members left
            let remainingMembersQuery = `
                SELECT EXISTS (
                    SELECT 1
                    FROM
                        germplasm.family_member fm
                    WHERE
                        fm.family_id = ${familyDbId}
                        AND fm.id IS NOT NULL
                        AND fm.is_void = FALSE
                )
            `

            // Retrieve remaining family members
            let remainingMembers = await sequelize.query(remainingMembersQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            if (await remainingMembers === undefined) {
                return
            }
            // If the family has no more members, delete the family
            else if (await !remainingMembers[0]["exists"]) {
                transaction = await sequelize.transaction({ autocommit: false })

                // Delete family member record
                let deleteQuery = format(`
                    UPDATE
                        germplasm.family family
                    SET
                        is_void = TRUE,
                        modification_timestamp = NOW(),
                        modifier_id = ${personDbId}
                    WHERE 
                        family.id = ${familyDbId}
                        AND family.is_void = FALSE
                    RETURNING family.id
            
                `)

                let familyRecords = await sequelize.query(deleteQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction
                })

                await transaction.commit()

                for (family of familyRecords[0]) {
                    let familyObj = {
                        familyDbId: family["id"]
                    }
                    resultArray.push(familyObj)
                }
            }

            for (familyMember of familyMemberRecords[0]) {
                familyMembersObj = {
                    familyMemberDbId: familyMember["id"]
                }
                resultArray.push(familyMembersObj)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (transaction !== undefined && transaction.finished !== 'commit') transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}