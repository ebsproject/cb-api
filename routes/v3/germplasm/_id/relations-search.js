/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let germplasmHelper = require('../../../../helpers/germplasm/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    // retrieve germplasm relation records for the given germplasm
    post: async function (req, res, next) {
        // instantiate variables
        let ancestry = []
        let count = 0

        let depth
        let conditionStr = ''

        let errMsg = ''

        // retrieve pagination params
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort

        // retrieve params
        let params = req.params
        let reqBody = req.body
        let germplasmDbId = params.id

        // validate data type of germplasm ID provided
        if(germplasmDbId == undefined || germplasmDbId == null || germplasmDbId == ''){
            errMsg = await errorBuilder.getError(req.headers.host, 400005)
            res.send(new errors.InternalError(errMsg))
            return
        }
        
        if(!validator.isInt(germplasmDbId)) {
            errMsg = await errorBuilder.getError(req.headers.host, 400246)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // validate if has existing germplasm record
        let germplasmQuery = `
            SELECT 
                germplasm.id
            FROM
                germplasm.germplasm germplasm
            WHERE
                germplasm.is_void = FALSE
                AND germplasm.id = ${germplasmDbId}
        `

        let germplasm = await sequelize
        .query(
            germplasmQuery, 
            {
                type: sequelize.QueryTypes.SELECT
            }
        ).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        if(germplasm == undefined || germplasm == null){
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        if(germplasm.length < 1){
            let errMsg = await errorBuilder.getError(req.headers.host, 404042)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // retrieve request body
        if(reqBody != undefined){
            conditionStr = `WHERE`

            if(reqBody.depth != null){
                depth = reqBody.depth

                if(!validator.isInt(depth)) {
                    errMsg = await errorBuilder.getError(req.headers.host, 400022)
                    res.send(new errors.InternalError(errMsg))
                    return
                }

                conditionStr += `
                    tree.depth <= ${depth}
                `

            }
        }

        // build base query
        let germplasmRelationQuery =  `
            WITH RECURSIVE tree(id, child, root, order_number, depth) AS (
                SELECT
                    c.id,
                    c.child_germplasm_id,
                    c.parent_germplasm_id,
                    c.order_number,
                    1 AS depth
                FROM
                    germplasm.germplasm_relation c
                WHERE 
                    c.is_void = FALSE
                    AND c.child_germplasm_id = ${germplasmDbId}
                
                UNION ALL
                
                SELECT
                    germplasm_relation.id,
                    child_germplasm_id,
                    parent_germplasm_id,
                    germplasm_relation.order_number,
                    tree.depth + 1
                FROM
                    tree
                    INNER JOIN germplasm.germplasm_relation germplasm_relation
                        ON germplasm_relation.child_germplasm_id = tree.root
            ),
            collatedRes AS (
                SELECT
                    *
                FROM
                    tree
                ${conditionStr}
            )`

        // build fields if existing
        germplasmRelationQuery += `,
            femParent AS (
                SELECT
                    collatedRes.child AS "child",
                    collatedRes.root AS "femaleParentDbId",
                    germplasm.designation AS "femaleParentDesignation",
                    germplasm.germplasm_code AS "femaleParentGermplasmCode",
                    germplasm.generation AS "femaleParentGeneration",
                    germplasm.germplasm_type AS "femaleParentGermplasmType",
                    collatedRes.depth
                FROM
                    collatedRes
                    JOIN germplasm.germplasm germplasm 
                        ON germplasm.id = collatedRes.root
                WHERE
                    collatedRes.order_number = 1 --indicator for female parent
            ),
            maleParent AS (
                SELECT
                    collatedRes.child AS "child",
                    collatedRes.root AS "maleParentDbId",
                    germplasm.designation AS "maleParentDesignation",
                    germplasm.germplasm_code AS "maleParentGermplasmCode",
                    germplasm.generation AS "maleParentGeneration",
                    germplasm.germplasm_type AS "maleParentGermplasmType",
                    collatedRes.depth
                FROM
                    collatedRes
                    JOIN germplasm.germplasm germplasm 
                        ON germplasm.id = collatedRes.root
                WHERE
                    collatedRes.order_number = 2 --indicator for male parent
            ),
            orderedRes AS (
                SELECT
                    collatedRes.child AS "individualDbId",
                    ind.designation AS "individualDesignation",
                    ind.germplasm_code AS "individualGermplasmCode",
                    ind.generation AS "individualGeneration",
                    ind.germplasm_type AS "individualGermplasmType",
                    fp."femaleParentDbId" ,
                    fp."femaleParentDesignation",
                    fp."femaleParentGermplasmCode",
                    fp."femaleParentGeneration",
                    fp."femaleParentGermplasmType",
                    mp."maleParentDbId",
                    mp."maleParentDesignation",
                    mp."maleParentGermplasmCode",
                    mp."maleParentGeneration",
                    mp."maleParentGermplasmType",
                    collatedRes.depth
                FROM
                    collatedRes
                    JOIN germplasm.germplasm ind ON ind.id = collatedRes.child
                    JOIN femParent fp ON fp.child = collatedRes.child
                    JOIN maleParent mp ON mp.child = fp.child
            )
            SELECT
                DISTINCT orderedRes."individualDbId",
                orderedRes.*
            FROM
                orderedRes
            ORDER BY orderedRes.depth ASC
        `

        depth = depth ?? await germplasmHelper.getConfigDepthValue()

        let ancestryRelationsData = await germplasmHelper.getAncestryRelationRecordsWithDbFunction(
            req,
            res,
            'germplasm/{id}/relations-search',
            germplasmDbId,
            parseInt(depth),
            'relations')

        ancestry = ancestryRelationsData

        // retrieve total count of records
        count = ancestry.length ?? 0

        // return extracted values
        res.send(200, {
            rows: ancestry,
            count: count
        })

        return
    }
}