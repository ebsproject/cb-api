/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let format = require('pg-format')

module.exports = {

    /**
     * POST /v3/germplasm/:id/delete-germplasm-relations 
     * 
     * Deletes seed attributes given given the germplasm ID
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */

    post: async function (req, res, next) {

        // Retrieve germplasm ID
        let germplasmDbId = req.params.id

        if (!validator.isInt(germplasmDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400246)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let germplasmQuery = `
            SELECT EXISTS(
                SELECT 1 
                FROM 
                    germplasm.germplasm g 
                WHERE 
                    g.id = ${germplasmDbId}
                    AND g.is_void= FALSE
            )
        `

        // Retrieve germplasm
        let germplasm = await sequelize.query(germplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await germplasm === undefined || await !germplasm[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404042)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let seedQuery = `
            SELECT EXISTS (
                SELECT 1
                FROM
                    germplasm.germplasm g
                    LEFT JOIN germplasm.seed s ON s.germplasm_id = g.id
                WHERE
                    g.id = ${germplasmDbId}
                    AND s.id IS NOT NULL
                    AND s.is_void = FALSE
            )
        `

        // Retrieve seed
        let seed = await sequelize.query(seedQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await seed === undefined || await seed[0]["exists"]) {
            let errMsg = `User is not allowed to delete germplasm and related data with existing seeds.`
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let germplasmRelationQuery = `
            SELECT EXISTS(
                SELECT 1 
                FROM 
                    germplasm.germplasm_relation 
                WHERE 
                    parent_germplasm_id = ${germplasmDbId}
                    AND is_void= FALSE
            )
        `

        // Retrieve germplasm
        let germplasmRelation = await sequelize.query(germplasmRelationQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await germplasmRelation === undefined || await germplasmRelation[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 403008)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let resultArray = []
            let germplasmRelationsObj = {}

            let deleteQuery = format(`
                UPDATE
                    germplasm.germplasm_relation germplasm_relation 
                SET
                    is_void = true,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE 
                    germplasm_relation.child_germplasm_id = ${germplasmDbId}
                    AND germplasm_relation.is_void = FALSE
                RETURNING germplasm_relation.id
        
            `)

            let germplasmNameRecords = await sequelize.query(deleteQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            await transaction.commit()

            for (germplasmRelations of germplasmNameRecords[0]) {
                germplasmRelationsObj = {
                    germplasmRelationDbId: germplasmRelations["id"]
                }
                resultArray.push(germplasmRelationsObj)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}