/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, germplasm } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator')
let germplasmHelper = require('../../../../helpers/germplasm/index.js')
let validator = require('validator')
let forwarded = require('forwarded-for')
const logger = require('../../../../helpers/logger/index.js')
const endpoint = 'germplasm/:id'

module.exports = {
    get: async function (req, res, next) {
        // Set defaults
        let germplasmDbId = req.params.id
        if (!validator.isInt(germplasmDbId)) {
            let errMsg = "Invalid format, germplasm ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        // Build query
        let germplasmQuery = `
            SELECT
                germplasm.id AS "germplasmDbId",
                germplasm.designation AS "designation",
                germplasm.parentage AS "parentage",
                germplasm.generation AS "generation",
                germplasm.germplasm_state AS "germplasmState",
                germplasm.germplasm_name_type AS "germplasmNameType",
                germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.germplasm_type AS "germplasmType",
                germplasm.crop_id AS "cropDbId",
                germplasm.taxonomy_id AS "taxonomyDbId",
                germplasm.product_profile_id AS "productProfileDbId",
                family.id AS "familyDbId",
                family.family_name AS "familyName",
                family.family_code AS "familyCode",
                fm.id AS "familyMemberDbId",
                fm.order_number AS "familyMemberNumber",
                taxonomy.taxon_id AS "taxonId",
                taxonomy.taxonomy_name AS "taxonomyName",
                crop.id AS "cropDbId",
                crop.crop_code AS "cropCode",
                crop.crop_name AS "cropName",
                crop.description AS "cropDescription",
                germplasm.creator_id AS "creatorDbId",
                creator.person_name AS creator,
                germplasm.creation_timestamp AS "creationTimestamp",
                germplasm.modifier_id AS "modifierDbId",
                modifier.person_name AS modifier,
                germplasm.modification_timestamp AS "modificationTimestamp"
            FROM
                germplasm.germplasm germplasm
            LEFT JOIN
                germplasm.family_member fm ON fm.germplasm_id = germplasm.id AND fm.is_void = FALSE
            LEFT JOIN
                germplasm.family family ON family.id = fm.family_id AND family.is_void = FALSE
            LEFT JOIN
                germplasm.product_profile productProfile ON productProfile.id = germplasm.product_profile_id AND
                productProfile.is_void = FALSE
            LEFT JOIN
                germplasm.taxonomy taxonomy ON taxonomy.id = germplasm.taxonomy_id AND taxonomy.is_void = FALSE
            LEFT JOIN
                tenant.crop crop ON crop.id = germplasm.crop_id AND crop.is_void = FALSE
            LEFT JOIN tenant.person AS creator
                ON creator.id = germplasm.creator_id
            LEFT JOIN tenant.person AS modifier
                ON modifier.id = germplasm.modifier_id
            WHERE
                germplasm.id = ${germplasmDbId}
                AND germplasm.is_void = FALSE
            ORDER BY
                germplasm.id
        `
        let germplasm = await sequelize.query(germplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            logger.logMessage(__filename, err, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })
        if (await germplasm === undefined || await germplasm.length < 1){
            let errMsg = 'The germplasm entry you requested does not exist'
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        
        res.send(200, {
            rows: germplasm,
        })
        return
    },    
    // Implementation of delete call for /v3/germplasm/{id}
    delete: async function (req, res, next) {

        let userDbId = await tokenHelper.getUserId(req, res)

        // If userDbId is undefined, terminate
        if(userDbId === undefined){
            return
        }

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Defaults
        let germplasmDbId = req.params.id
        let isSecure = forwarded(req, req.headers).secure

        // Set the URL for response
        let germplasmUrlString = (isSecure ? 'https' : 'http')
                + "://"
                + req.headers.host
                + "/v3/germplasm"

        if (!validator.isInt(germplasmDbId)) {
            let errMsg = "Invalid format, germplasm ID must be an integer"
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            // Build query
            let germplasmQuery = `
                SELECT
                    count(1)
                FROM 
                    germplasm.germplasm germplasm
                WHERE
                    germplasm.is_void = FALSE AND
                    germplasm.id = ${germplasmDbId}
            `

            // Retrieve germpalsm from the database     
            let hasGermplasm = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Check if germplasm is non-existent in database
            if (parseInt(hasGermplasm[0].count) === 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404019)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            // Build query for voiding
            let deleteGermplasmQuery = `
                UPDATE
                    germplasm.germplasm germplasm
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    germplasm.id = ${germplasmDbId}
            `

            // Delete (void) germplasm record
            await sequelize.transaction(async transaction => {
                return await sequelize.query(deleteGermplasmQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            let resultsArray = {
                germplasmDbId: germplasmDbId,
                recordCount: 1,
                href: germplasmUrlString + '/' + germplasmDbId
            }

            res.send(200, {
                rows: resultsArray
            })
            return
        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },
    // Implementation of put call for /v3/germplasm/{id}
    put: async function (req, res, next) {
        let designation = null
        let parentage = null
        let generation = null
        let state = null
        let nameType = null
        let normalizedName = null
        let cropId = null
        let taxonomyId = null
        let productProfileId = null
        let resultArray = []

        // check if user is an administrator. If not, no access
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isAdmin = await userValidator.isAdmin(userDbId)
        if (!isAdmin) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401018)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        // Retrieve germplasm Id
        let germplasmDbId = req.params.id
        if (!validator.isInt(germplasmDbId)) {
            let errMsg = 'Invalid format for germplasm Id. Germplasm ID must be an integer'. 
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if request body exists
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let validateGermplasmQuery = `
            SELECT
                germplasm.id AS "germplasmDbId",
                germplasm.designation AS "designation",
                germplasm.parentage AS "parentage",
                germplasm.generation AS "generation",
                germplasm.germplasm_state AS "germplasmState",
                germplasm.germplasm_name_type AS "germplasmNameType",
                germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                germplasm.germplasm_type AS "germplasmType",
                germplasm.crop_id AS "germplasmCropId",
                germplasm.taxonomy_id AS "germplasmTaxonomyId",
                germplasm.product_profile_id AS "germplasmProfileId"
            FROM
                germplasm.germplasm germplasm
            LEFT JOIN
                tenant.crop crop ON crop.id = germplasm.crop_id AND
                crop.is_void = FALSE
            LEFT JOIN
                germplasm.product_profile productProfile ON productProfile.id = germplasm.product_profile_id AND
                productProfile.is_void = FALSE
            WHERE
                germplasm.id = ${germplasmDbId}
                AND germplasm.is_void = FALSE
        `    

        // Retrieve germplasm from the database     
        let germplasm = await sequelize.query(validateGermplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        })

        // Return error if resource is not found
        if (await germplasm.length < 1 || await germplasm == undefined) {
            let errMsg = 'Invalid request. Ensure that the germplasm exist. '
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // Get values
        let germplasmDesignation = germplasm[0].germplasmDesignation
        let germplasmParentage = germplasm[0].germplasmParentage
        let germplasmGeneration = germplasm[0].germplasmGeneration
        let germplasmState = germplasm[0].germplasmState
        let germplasmNameType = germplasm[0].germplasmNameType
        let germplasmNormalizedName = germplasm[0].germplasmNormalizedName
        let germplasmCropId = germplasm[0].germplasmCropId
        let germplasmTaxonomyId = germplasm[0].germplasmTaxonomyId
        let germplasmProductProfileId = germplasm[0].germplasmProductProfileId
        let germplasmType = germplasm[0].germplasmType

        // Set the URL for response
        let isSecure = forwarded(req, req.headers).secure
        let germplasmUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/germplasm"

        let data = req.body
        let transaction

        try {
            let setQuery = ``         
            /**
             * 
             * Validation of the values
             *    
             * */
            let validateQuery = ``
            let validateCount = 0

            if (data.designation != undefined) {                             
                designation = data.designation
                if(designation != germplasmDesignation){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        designation = $$${designation}$$`
                }

                normalizedName = await germplasmHelper.normalizeText(designation)
                if(normalizedName != germplasmNormalizedName){
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_normalized_name = $$${normalizedName}$$`
                }
            }
            if (data.parentage != undefined) {
                parentage = data.parentage
                if(parentage != germplasmParentage){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        parentage = $$${parentage}$$`
                }
            }
            if (data.generation != undefined) {
                generation = data.generation
                if(generation != germplasmGeneration){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        generation = $$${generation}$$`
                }
            }
            if (data.germplasmState != undefined) {
                state = data.germplasmState
                if(state != germplasmState){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_state = $$${state}$$`
                }
            }
            if (data.germplasmNameType != undefined) {
                nameType = data.germplasmNameType
                if(nameType != germplasmNameType){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        germplasm_name_type = $$${nameType}$$`
                }
            }

            if (data.germplasmType != undefined) {
                type = data.germplasmType
                if (type != germplasmType) {        
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            germplasm_type = $$${type}$$`
                }
            }

            if (data.cropId != undefined) {
                cropId = data.cropId
                if(cropId != germplasmCropId){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        crop_id = $$${cropId}$$`
                    validateQuery += `
                        (
                            SELECT
                                CASE WHEN
                                    (count(1) = 0)
                                    THEN 1
                                    ELSE 0
                                END AS count
                            FROM
                                tenant.crop crop
                            WHERE
                                crop.id = ${cropId}
                                AND crop.is_void = FALSE
                        )
                    `
                    validateCount += 1    
                }
            }
            if (data.taxonomyId != undefined) {
                taxonomyId = data.taxonomyId
                if(taxonomyId != germplasmTaxonomyId){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        taxonomy_id = $$${taxonomyId}$$`
                }
            }
            if (data.productProfileId != undefined) {
                productProfileId = data.productProfileId
                if(productProfileId != germplasmProductProfileId){        
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        product_profile_id = $$${productProfileId}$$`
                }
            }

            /* Validate input in DB */
            // count must be equal to validateCount if all conditions are met
            if (validateCount > 0) {
                let checkInputQuery = `
                    SELECT ${validateQuery} AS count
                `
                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0]['count'] != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += `,`

            // update the germplasm record
            let updateGermplasmQuery = `
                UPDATE
                    germplasm.germplasm germplasm
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = $$${userDbId}$$
                WHERE
                    germplasm.id = $$${germplasmDbId}$$
            `
            
            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateGermplasmQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

                // Return the transaction info
            let array = {
                germplasmDbId: germplasmDbId,
                recordCount: 1,
                href: germplasmUrlString + '/' + germplasmDbId
            }

            resultArray.push(array)

            res.send(200, {
                rows: resultArray
            })
            return
        } 
        catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return     
        }     
    }
}
