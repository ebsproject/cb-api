/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let { knex } = require('../../../../config/knex')
let validator = require('validator')
let processQueryHelper = require('../../../../helpers/processQuery/index')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let params = req.query
        let count = 0
        let orderString = ''
        let conditionString = ''
        let addedDistinctString = ''
        let addedOrderString = `
            ORDER BY
                germplasm_name.id
        `
        let parameters = {}
        parameters['distinctOn'] = ''

        let germplasmDbId = req.params.id

        if (!validator.isInt(germplasmDbId)) {
            let errMsg = 'Invalid request, germplasm ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let germplasmQuery = `
            SELECT
                germplasm.id AS "germplasmDbId"
            FROM
                germplasm.germplasm germplasm
            WHERE
                germplasm.is_void = FALSE AND
                germplasm.id = ${germplasmDbId}
        `

        // Retrieve the germplasm record using the ID
        let germplasm = await sequelize
            .query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Check if germplasm exists
        if (await germplasm == undefined || await germplasm.length < 1) {
            let errMsg = 'Resource not found, germplasm does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (req.body != undefined) {
            if (req.body.fields != null) {
                parameters['fields'] = req.body.fields
            }

            if (req.body.distinctOn != undefined) {
                parameters['distinctOn'] = req.body.distinctOn
                addedDistinctString = await processQueryHelper
                    .getDistinctString(
                        parameters['distinctOn']
                    )

                parameters['distinctOn'] = `"${parameters['distinctOn']}"`
                addedOrderString = `
                    ORDER BY
                    ${parameters['distinctOn']}
                `
            }
            // Set columns to be excluded in parameters for filtering
            let excludedParametersArray = [
                'fields',
                'distinctOn',
            ]
            // Get the filter condition
            conditionString = await processQueryHelper.getFilter(
                req.body,
                excludedParametersArray,
            )

            if (conditionString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400022)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Build the base retrieval query
        let germplasmNamesQuery = null
        
        if (parameters['fields'] != null) {
            if (parameters['distinctOn']) {
                let fieldsString = await processQueryHelper.getFieldValuesString(
                    parameters['fields']
                )

                let selectString = knex.raw(`${addedDistinctString} ${fieldsString}`)

                germplasmNamesQuery = knex.select(selectString)
            } else {
                germplasmNamesQuery = knex.column(parameters['fields'].split('|'))
            }

            germplasmNamesQuery += `
                FROM
                    germplasm.germplasm_name germplasm_name
                LEFT JOIN
                    tenant.person creator ON germplasm_name.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON germplasm_name.modifier_id = modifier.id
                WHERE
                    germplasm_name.is_void = FALSE AND
                    germplasm_name.germplasm_id = ${germplasmDbId}
            ` + addedOrderString
        } else {
            germplasmNamesQuery = `
                SELECT
                    ${addedDistinctString}
                    germplasm_name.id AS "germplasmNameDbId",
                    germplasm_name.germplasm_id AS "germplasmDbId",
                    germplasm_name.name_value AS "nameValue",
                    germplasm_name.germplasm_name_type AS "germplasmNameType",
                    germplasm_name.germplasm_name_status AS "germplasmNameStatus",
                    germplasm_name.germplasm_normalized_name AS "germplasmNormalizedName",
                    germplasm_name.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    germplasm_name.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                FROM
                    germplasm.germplasm_name germplasm_name
                LEFT JOIN
                    tenant.person creator ON germplasm_name.creator_id = creator.id
                LEFT JOIN
                    tenant.person modifier ON germplasm_name.modifier_id = modifier.id
                WHERE
                    germplasm_name.is_void = FALSE
            ` + addedOrderString
        }
        
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        // Generate the final SQL query
        let germplasmNamesFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            germplasmNamesQuery,
            conditionString,
            orderString,
        )

        let germplasmNames = await sequelize
            .query(germplasmNamesFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset,
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.BadRequestError(errMsg))
                return
            })

        if (await germplasmNames == undefined || await germplasmNames.lenggth < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            
            return
        }

        let germplasmNamesCountFinalSqlQuery = await processQueryHelper
            .getCountFinalSqlQuery(
                germplasmNamesQuery,
                conditionString,
                orderString,
            )

        let germplasmNamesCount = await sequelize
            .query(germplasmNamesCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        count = germplasmNamesCount[0].count

        res.send(200, {
            rows: germplasmNames,
            count: count
        })

        return
    }
}