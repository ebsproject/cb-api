/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let validator = require('validator')
let format = require('pg-format')

module.exports = {

    /**
     * POST /v3/germplasm/:id/delete-seed-relations 
     * 
     * Deletes seed relations given given the germplasm ID
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */

    post: async function (req, res, next) {

        // Retrieve germplasm ID
        let germplasmDbId = req.params.id

        if (!validator.isInt(germplasmDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400246)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req, res)

        // If personDbId is undefined, terminate
        if(personDbId === undefined){
            return
        }

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.UnauthorizedError(errMsg))
            return
        }

        let germplasmQuery = `
            SELECT EXISTS(
                SELECT 1 
                FROM 
                    germplasm.germplasm g 
                WHERE 
                    g.id = ${germplasmDbId}
                    AND g.is_void= FALSE
            )
        `

        // Retrieve germplasm
        let germplasm = await sequelize.query(germplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await germplasm === undefined || await !germplasm[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404042)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let seedQuery = `
            SELECT EXISTS(
                SELECT 1 
                FROM 
                    germplasm.seed s
                    LEFT JOIN experiment.entry e ON e.seed_id = s.id
                WHERE 
                    e.id IS NOT NULL
                    AND e.is_void = FALSE
                    AND s.germplasm_id = ${germplasmDbId}
                    AND s.is_void = FALSE
            )
        `

        // Retrieve seeds
        let seed = await sequelize.query(seedQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await seed[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 403009)
            res.send(new errors.ForbiddenError(errMsg))
            return
        }

        let seedRelationQuery = `
            SELECT EXISTS(
                SELECT 1 
                FROM 
                    germplasm.seed s
                    LEFT JOIN germplasm.seed_relation sr ON sr.parent_seed_id = s.id 
             
                WHERE 
                    sr.id IS NOT NULL AND
                    s.germplasm_id = ${germplasmDbId}
                    AND sr.is_void = FALSE
            )
        `
        
        // Retrieve seed relation
        let seedRelation = await sequelize.query(seedRelationQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await seedRelation === undefined || await seedRelation[0]["exists"]) {
            let errMsg = await errorBuilder.getError(req.headers.host, 403007)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let transaction
        try {
            transaction = await sequelize.transaction({ autocommit: false })

            let resultArray = []
            let seedRelationsObj = {}

            let deleteQuery = format(`
                UPDATE
                    germplasm.seed_relation seed_relation 
                SET
                    is_void = true,
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                FROM 
                    germplasm.seed s
                WHERE 
                    s.id = seed_relation.child_seed_id
                    AND s.germplasm_id = ${germplasmDbId}
                    AND seed_relation.is_void = FALSE
                RETURNING seed_relation.id
            `)

            let seedRelationRecords = await sequelize.query(deleteQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

            await transaction.commit()

            for (seedRelations of seedRelationRecords[0]) {
                seedRelationsObj = {
                    seedRelationDbId: seedRelations["id"]
                }
                resultArray.push(seedRelationsObj)
            }

            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })
            return
        } catch (err) {
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}