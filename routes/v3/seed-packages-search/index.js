/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let { knex } = require('../../../config/knex')

let errorBuilder = require('../../../helpers/error-builder')
let processQueryHelper = require('../../../helpers/processQuery/index')
let germplasmHelper = require('../../../helpers/germplasm/index')
let queryToolHelper = require('../../../helpers/queryTool/index')
let seedPackageHelper = require('../../../helpers/seed/packages')
let logger = require('../../../helpers/logger/index')

let validator = require('validator')
let errors = require('restify-errors')

module.exports = {
    post: async function (req, res, next) {
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let dataLevel = req.query.dataLevel
        let count = 0
        let orderString = ''
        let conditionString = ''
        let conditionStringCTE = ''
        let addedConditionString = ''
        let namesCondition = ''
        let addedDistinctString = ''
        let distinctOrderString = ``
        let workingListCondition = ''
        let addedNamesString = ''
        let sortByInputOrderString = ''
        let parameters = {}
        let addGroupLimit = false;
        let groupLimit = 0;
        let retainSelection = false;
        let retainSelectionStr = '';
        let retainSelectionStrSelect = '';
        let packageDistinctColumns = ''
        let orderOuter = '';
        let schemaSource = '';
        let dataSource = '';
        let inputListValuesType = '';
        let inputListValuesSource = '';
        let packageSource = '';
        let additionalSelectFields = '';
        let additionalSourceCondition = '';
        let germplasmSourceCondition = '';
        let germplasmIdSource = '';
        let entryIdsString = '';
        let addPackageCount = false;
        let sortColumn = ''
        let inputValues = ''
        let packageData = ``
        
        parameters['distinctOn'] = ''

        let summariesCte = ''
        let addlDataCte = ''
        let addlDataCteSelect = ''
        let addlDataCteJoin = ''

        let qParams = req.query
        let isBasic = qParams.isBasic !== undefined && qParams.isBasic == 'true'

        // replacement values
        let nameValue = []
        let sortValues = []
        let columnValues = []

        let endpoint = 'seed-packages-search'
        let errMsg = ''
        let sortsColumns = []

        if(req.query.dataLevel === null || req.query.dataLevel === 'undefined'){
            errMsg = await errorBuilder.getError(req.headers.host, 400022)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try{
            schemaSource = 'germplasm';    
            dataSource = dataLevel === 'all' ? ` ${schemaSource}.seed seed ` : ` ${schemaSource}.${dataLevel} ${dataLevel} `
            germplasmIdSource = 'germplasm.id';

            if (req.body) {
                if (req.body.fields != null) {
                    parameters['fields'] = req.body.fields
                }
    
                // Set columns to be excluded in parameters for filtering
                let excludedParametersArray = [
                    'fields', 'distinctOn', 'sortByInput', 
                    'names', 'seedName', 'label' , 'packageCode', 'seedCode', 'germplasmCode',
                    'workingListDbId', 'multipleSelect', 'groupLimit', 'retainSelection',
                    '__browser_filters__','addPackageCount', 'mtaStatus', 'rshtReferenceNo','moistureContent'
                ]
    
                // Get filter condiiton
                conditionString = await processQueryHelper
                    .getFilter(req.body, excludedParametersArray)
    
                if (conditionString.includes('invalid')) {
                    errMsg = await errorBuilder.getError(req.headers.host, 400022)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
    
                if (req.body.__browser_filters__ !== undefined) {
                    let browserFilters = req.body.__browser_filters__
                    let paramsArray = excludedParametersArray
                    let index = []
                    const filterCTE = {}

                    if(browserFilters.seedName) index.push('seedName')
                    if(browserFilters.label) index.push('label')
                    if(browserFilters.seedCode) index.push('seedCode')
                    if(browserFilters.packageCode) index.push('packageCode')
                    if(browserFilters.mtaStatus) index.push('mtaStatus')
                    if(browserFilters.rshtReferenceNo) index.push('rshtReferenceNo')
                    if(browserFilters.moistureContent) index.push('moistureContent')
                    if (browserFilters.mcCleanSeed) index.push('mcCleanSeed')

                    if (index.length > 0){
                        let paramInd = -1
                        for (ind of index) {
                            paramInd = paramsArray.indexOf(ind)
                            if(paramInd !== -1) paramsArray.splice(paramInd,1)
                        }
                    }

                    if(browserFilters.designation){
                        columnValues = browserFilters.designation.replaceAll('|',',').replaceAll('equals','').trim()
                    }

                    // separate CTE variables from browser filters
                    if(browserFilters.mtaStatus){
                        filterCTE.mtaStatus = browserFilters.mtaStatus
                        delete browserFilters.mtaStatus
                    }

                    if(browserFilters.rshtReferenceNo){
                        filterCTE.rshtReferenceNo = browserFilters.rshtReferenceNo
                        delete browserFilters.rshtReferenceNo
                    }

                    if(browserFilters.moistureContent){
                        filterCTE.moistureContent = browserFilters.moistureContent
                        delete browserFilters.moistureContent
                    }

                    if (browserFilters.mcCleanSeed) {
                        filterCTE.mcCleanSeed = browserFilters.mcCleanSeed
                        delete browserFilters.mcCleanSeed
                    }

                    let condition = await processQueryHelper.getFilter(browserFilters, paramsArray, null, true, { 'designation': ":browserColumn" })
                    let conditionCTE = await processQueryHelper.getFilter(filterCTE, paramsArray, null, true, { 'designation': ":browserColumn" })

                    if (conditionString != '') {
                        if(condition != ''){ // This will prevent empty 'AND' condition being added in the query
                            condition = condition.replace("WHERE", '')
                            conditionString += ` AND ` + condition                            
                        }
                    } else {
                        conditionString = condition
                    }
                    // For filtering CTE
                    if(conditionStringCTE != ''){
                        if(conditionCTE != ''){
                            conditionCTE = conditionCTE.replace("WHERE", '')
                            conditionStringCTE += ` AND ` + conditionCTE
                        }
                    } else {
                        conditionStringCTE = conditionCTE
                    }
                }

                // prep entryDdIs condition
                if(req.body.entryDbId !== undefined){
                    let entryIdFilter = req.body.entryDbId
                    let entryIdArr = []

                    let entryIdValue = ''
                    for (value of entryIdFilter.split('|')) {
                        entryIdValue = value.replace('equals ','')
                        entryIdArr.push(entryIdValue)
                    }

                    entryIdsString = entryIdsString + `AND entry.id IN (${entryIdArr.toString()})`
                }

                // prep input list values condition
                if( req.body.packageCode || req.body.label 
                    || req.body.seedName || req.body.names
                    || req.body.seedCode || req.body.germplasmCode){

                    let namesFilter = null
                    let namesArray = []

                    let normalizeNames = false
                    let sourceColumn = ''

                    if(req.body.names != undefined){
                        namesFilter = req.body.names   
                        normalizeNames = true
                        sourceColumn = 'gn.germplasm_normalized_name'
                        inputListValuesType = 'names'
                    }

                    if(req.body.germplasmCode != undefined){
                        namesFilter = req.body.germplasmCode
                        sourceColumn = 'germplasm.germplasm_code'
                        inputListValuesType = 'germplasm codes'
                    }
                    
                    if(req.body.seedCode != undefined){
                        namesFilter = req.body.seedCode
                        sourceColumn = 'seed.seed_code'
                        inputListValuesType = 'seed codes'
                    }
                    
                    if(req.body.seedName != undefined){
                        namesFilter = req.body.seedName
                        sourceColumn = 'seed.seed_name'
                        inputListValuesType = 'seed names'
                    }

                    if(req.body.label != undefined){
                        namesFilter = req.body.label
                        sourceColumn = 'package.package_label'
                        inputListValuesType = 'labels'
                    }

                    if(req.body.packageCode != undefined){
                        namesFilter = req.body.packageCode
                        sourceColumn = 'package.package_code'
                        inputListValuesType = 'package codes'
                    }

                    // process operands
                    var operators = ['not equals','equals']
                    var inCond = namesFilter.includes('not equals') ? 'NOT IN' : 'IN'

                    for(operand of operators){
                        if(namesFilter.includes(operand)){
                            namesFilter = namesFilter.replaceAll(operand+' ','')
                        }
                    }
    
                    let newValue = ''
                    for (value of namesFilter.split('|')) {
                        if(normalizeNames){
                            newValue = await germplasmHelper.normalizeText(value)
                        }
                        else{
                            newValue = value.replace(/\\\\/g,"\\")
                        }

                        namesArray.push(newValue)
                    }

                    nameValue = namesArray                    

                    namesCondition = namesCondition + ` ${sourceColumn} ${inCond} ( :name ) `

                    if (namesCondition != '') addedNamesString = `AND (${namesCondition})`
                }

                if (req.body.workingListDbId !== undefined && req.body.multipleSelect !== undefined) {
    
                    let isMutipleSelect = req.body.multipleSelect
                    let workingListDbId = req.body.workingListDbId

                    // check if needs to retain selected items in the results
                    if (req.body.retainSelection !== undefined) {
                        if (req.body.retainSelection == 'true') {
                            retainSelection = true
                        }
                    }
    
                    // if selection is retained, add check if in working list
                    if (retainSelection) {
                        retainSelectionStr = `
                            LEFT JOIN LATERAL(
                            	SELECT
                            		lm.list_id,
                            		lm.id
                            	FROM
                            		platform.list_member lm
                            	WHERE
                                	lm.data_id = package.id
                                	AND lm.is_void = FALSE
                                	AND lm.list_id = ${workingListDbId}
                                ORDER BY lm.id ASC
                                LIMIT 1
                            ) lm on TRUE`
                        
                        retainSelectionStrSelect = `,
                            CASE WHEN lm.id IS NULL THEN false ELSE true END as "isDisabled"`
    
                    }
                    
                    if(!retainSelection && isMutipleSelect == 'true'){
                        workingListCondition = `
                            "packageDbId" NOT IN (
                                SELECT
                                    lm.data_id
                                FROM 
                                    platform.list_member lm
                                where
                                    lm.list_id = ${workingListDbId}
                                    AND lm.is_void = FALSE
                            )
                        `
                    }

                    if(!retainSelection && !(isMutipleSelect == 'true')){
                        workingListCondition = `
                            "germplasmDbId" NOT IN (
                                SELECT
                                    s.germplasm_id
                                FROM 
                                    platform.list_member lm,
                                    germplasm.package p,
                                    germplasm.seed s
                                where
                                    lm.list_id = ${workingListDbId}
                                    AND lm.is_void = FALSE
                                    AND lm.data_id = p.id
                                    AND s.id = p.seed_id
                                    AND p.is_void = FALSE
                                    AND s.is_void = FALSE
                            )
                        `
                    }
                }

                if (req.body.groupLimit !== undefined) {
                    if (req.body.groupLimit > 0) {
                        addGroupLimit = true
                        groupLimit = req.body.groupLimit
                    }
                }

                // check if there's distinct parameter
                if (req.body.distinctOn !== undefined) {
                    parameters['distinctOn'] = req.body.distinctOn
                    addedDistinctString = await processQueryHelper
                        .getDistinctString(parameters['distinctOn'])
    
                    parameters['distinctOn'] = `${parameters['distinctOn']}`
    
                    packageDistinctColumns = knex.raw(`${addedDistinctString}`)
    
                    let distinctOnValue = parameters['distinctOn'];
    
                    // Split the value of distinctOn via |
                    let columns = distinctOnValue.split('|');
                    let distinctOnSort = ''
                    for (col of columns) {
                        distinctOnSort += `"${col}",`
                    }
    
                    distinctOnSort = distinctOnSort.replace(/,\s*$/, "")
                    distinctOrderString = `DISTINCT ON (${distinctOnSort})`
    
                    orderString = `ORDER BY t."${parameters['distinctOn']}" `
                }
    
                // check if results should be sorted by input list
                if (req.body.sortByInput !== undefined) {
                    parameters['sortByInput'] = req.body.sortByInput
    
                    if (req.body.sortByInput.column === undefined) {
                        res.send(new errors.BadRequestError("Column is required when sorting results by input"))
                        return
                    }
    
                    if (req.body.sortByInput.value === undefined) {
                        res.send(new errors.BadRequestError("Value is required when sorting results by input"))
                        return
                    }
    
                    // Prepare sort order values parameters
                    sortColumn = req.body.sortByInput.column
                    inputValues = req.body.sortByInput.value.split('|')

                    if(sortColumn == 'designation'){ sortColumn = 'germplasmNormalizedName' }

                    // retrieve updated sort order statement
                    sortValues = await queryToolHelper.getSortClauseForSpecificOrder(inputValues,sortColumn)
                    sortByInputOrderString = sortValues
                }
            }

            packageSource = `
                    JOIN germplasm.package package 
                        ON package.seed_id = seed.id
                        AND package.is_void = FALSE
                `

            germplasmSourceCondition =  ''
            
            let dataFilters = []
            let isInitialFilteredSource = false
            let dataFilterIndx = inputListValuesType
                
            if(dataLevel === 'germplasm' || dataLevel === 'all'){
                
                if( namesCondition !== '' && inputListValuesType !== '' 
                    && ['names','germplasm codes'].includes(inputListValuesType)){

                    dataFilterIndx = await seedPackageHelper.toCamelCase(inputListValuesType)
                    dataFilters[dataFilterIndx] = addedNamesString
                    isInitialFilteredSource = true
                }
                
                let germplasmQueryData = await seedPackageHelper.getGermplasmData(isInitialFilteredSource,dataFilters,dataLevel);

                germplasmSourceCondition = germplasmQueryData.germplasmDataJoinQuery
                
                additionalSelectFields += germplasmQueryData.germplasmDataSelectQuery
                additionalSourceCondition = germplasmQueryData.germplasmDataJoinQuery
                
                if(dataLevel == 'germplasm' && !additionalSelectFields.includes("germplasmDbId")){
                    additionalSelectFields += `
                        germplasm.id AS "germplasmDbId",
                    `
                }

                if(additionalSourceCondition !== ''){
                    inputListValuesSource = additionalSourceCondition
                    dataSource = ''
                }
            }

            if(dataLevel === 'seed' || dataLevel === 'all'){
                dataFilters = []
                isInitialFilteredSource = false
                dataFilterIndx = inputListValuesType
                
                // if input list with seed codes and seed names are provided
                if( namesCondition !== '' && inputListValuesType !== '' 
                    && ['seed names','seed codes'].includes(inputListValuesType)){

                    dataFilterIndx = await seedPackageHelper.toCamelCase(inputListValuesType)
                    dataFilters[dataFilterIndx] = addedNamesString
                    isInitialFilteredSource = true

                    germplasmSourceCondition = ''
                }
                
                let seedQueryData = await seedPackageHelper.getSeedData(isInitialFilteredSource,dataFilters,dataLevel)
                if(isInitialFilteredSource && additionalSourceCondition){
                    additionalSourceCondition = ''
                }

                additionalSelectFields += seedQueryData.seedDataSelectQuery
                
                if(!additionalSourceCondition.includes(seedQueryData.seedDataJoinQuery)){
                    additionalSourceCondition += seedQueryData.seedDataJoinQuery
                }

                
                // retrieve package select statements
                let packageQueryData = await seedPackageHelper.getPackageData(false,dataFilters,dataLevel)
                if(!additionalSelectFields.includes('AS "packageDbId"')){
                    packageQueryData.packageDataSelectQuery += ` package.id AS "packageDbId", `
                }

                additionalSelectFields += packageQueryData.packageDataSelectQuery
                additionalSourceCondition += packageQueryData.packageDataJoinQuery

                if(namesCondition !== '' && ['labels','package codes'].includes(inputListValuesType)){

                    dataFilterIndx = await seedPackageHelper.toCamelCase(inputListValuesType)
                    dataFilters[dataFilterIndx] = addedNamesString

                    germplasmSourceCondition = ''

                    packageQueryData = await seedPackageHelper.getPackageData(true,dataFilters,dataLevel)

                    additionalSourceCondition = packageQueryData.packageDataJoinQuery

                    germplasmSourceCondition = ''
                    packageSource = ''
                    dataSource = ''
                }
                
                if(dataLevel === 'all' && !isInitialFilteredSource && additionalSourceCondition != ''){
                    dataSource = ''
                }

                if(dataLevel == 'seed'){
                    germplasmIdSource = 'seed.germplasm_id'
                }

                inputListValuesSource = additionalSourceCondition

                if(additionalSourceCondition.includes(dataSource)){
                    dataSource = ''
                }
            }

            if(dataLevel === 'entry'){
                inputListValuesSource = ''

                additionalSelectFields = `
                    entry.id AS "entryDbId",
                    entry.entry_number AS "entryNumber",
                    entry.entry_code AS "entryCode",
                    entry.package_id AS "entryPackageDbId",
                    germplasm.designation AS "designation",
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                    germplasm.germplasm_state AS "germplasmState",
                    germplasm.germplasm_type AS "germplasmType",
                    germplasm.other_names AS "germplasmOtherNames",
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_year AS "experimentYear",
                    experiment.experiment_type AS "experimentType",
                    stage.id AS "stageDbId",
                    stage.stage_name AS "stageName",
                    stage.stage_code AS "stageCode",
                    season.id AS "seasonDbId",
                    season.season_code AS "seasonCode",
                    seed.seed_name AS "seedName",
                    seed.seed_code AS "seedCode",
                    seed.germplasm_id AS "germplasmDbId",
                `

                dataSource = `
                        (
                            SELECT
                                entry.id,
                                entry.entry_number,
                                entry.entry_code,
                                entry.germplasm_id,
                                entry.package_id,
                                entry.entry_list_id
                            FROM
                                experiment.entry entry
                            WHERE
                                entry.is_void = FALSE
                                ${entryIdsString}
                        ) AS entry `
                
                dataSource += `
                    JOIN 
                        germplasm.germplasm germplasm ON germplasm.id = entry.germplasm_id 
                        AND germplasm.is_void = FALSE
                `

                dataSource += `
                    JOIN 
                        germplasm.seed seed ON seed.germplasm_id = germplasm.id 
                        AND seed.is_void = FALSE 
                    LEFT JOIN 
                        experiment.experiment experiment ON experiment.id = seed.source_experiment_id 
                        AND experiment.is_void = FALSE
                    LEFT JOIN
                        tenant.season season ON season.id = experiment.season_id
                        AND season.is_void = FALSE
                    LEFT JOIN
                        tenant.stage stage ON stage.id = experiment.stage_id
                        AND stage.is_void = FALSE
                    
                `
            }

            if(dataLevel === 'all'){
                additionalSelectFields += `
                    experiment.experiment_name AS "experimentName",
                    experiment.experiment_year AS "experimentYear",
                    experiment.experiment_type AS "experimentType",
                    experiment.experiment_status AS "experimentStatus",
                    stage.id AS "stageDbId",
                    stage.stage_name AS "stageName",
                    stage.stage_code AS "stageCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    program.program_code AS "seedManager",
                    "location".location_code AS "location",
                    "location".location_name AS "locationName",
                    season.id AS "seasonDbId",
                    season.season_code AS "seasonCode",
                    "entry".entry_code AS "entryCode",
                    "entry".entry_number AS "entryNumber",
                    plot.plot_code AS "plotCode",
                    plot.plot_number AS "plotNumber",
                    plot.rep AS "replication",
                `

                dataSource += `
                    LEFT JOIN
                        experiment.location "location" ON "location".id = seed.source_location_id
                        AND location.is_void = FALSE
                    LEFT JOIN
                        experiment.experiment experiment ON experiment.id = seed.source_experiment_id
                        AND experiment.is_void = FALSE
                    LEFT JOIN
                        experiment.plot plot ON plot.id = seed.source_plot_id
                        AND plot.is_void = FALSE
                    LEFT JOIN
                        experiment.entry "entry" ON "entry".id = seed.source_entry_id
                        AND entry.is_void = false
                    LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id
                        AND occurrence.is_void = FALSE
                    LEFT JOIN
                        tenant.season season ON season.id = experiment.season_id
                        AND season.is_void = FALSE
                    LEFT JOIN
                        tenant.stage stage ON stage.id = experiment.stage_id
                        AND stage.is_void = FALSE
                `
            }
            
            if(inputListValuesSource.trim() == dataSource.trim()){
                dataSource = ''
            }

            let firstFilter = `
                ${inputListValuesSource}
                ${dataSource}
            `
            
            let fromQuery = `
                FROM
                    ${firstFilter}
                    ${packageSource}
                    LEFT JOIN
                        tenant.program program ON program.id = package.program_id
                        AND program.is_void = FALSE
                    LEFT JOIN
                        place.facility container ON container.id = package.facility_id
                        AND container.is_void = FALSE
                    LEFT JOIN
                        place.facility "subFacility" ON "subFacility".id = container.parent_facility_id
                        AND "subFacility".is_void = FALSE
                    LEFT JOIN
                        place.facility facility ON facility.id = container.root_facility_id
                        AND facility.is_void = FALSE
                    JOIN
                        tenant.person creator ON creator.id = package.creator_id
                    LEFT JOIN
                        tenant.person modifier ON modifier.id = package.modifier_id
                    ${retainSelectionStr}
                WHERE
                    package.is_void = FALSE
            `

            if (dataLevel != 'germplasm') {

                // add package data CTEs
                let additionalDataQueries = await seedPackageHelper.getAdditionalPackageData('t') // t terms for baseQuery

                packageData = `,`
                packageData += additionalDataQueries.packageDataQuery

                addlDataCteSelect = additionalDataQueries.packageDataSelectQuery
                if (addlDataCteSelect != "") {
                    addlDataCteSelect = `, ${addlDataCteSelect}`
                }

                addlDataCteJoin = additionalDataQueries.packageDataJoinQuery
            }


            addlDataCte += `,
                allRecords AS (
                    SELECT  
                        withLimitOffset.*
                    FROM 
                        withLimitOffset
                )
            `
            
            // Build base retrieval query
            let packagesQuery = null
            
            // Check if user specified values in fields parameter
            addPackageCount = (req.body && req.body.addPackageCount !== undefined) ? req.body.addPackageCount : false
            if (parameters['fields']) {
                let hasGermplasmOtherNamesAttribute = false

                if ( parameters['fields'].includes('germplasmOtherNames') ) {
                    let fieldsArray = parameters['fields'].split('|')

                    fieldsArray.splice(fieldsArray.indexOf('germplasmOtherNames'), 1)

                    parameters['fields'] = fieldsArray.join('|')
                    hasGermplasmOtherNamesAttribute = true
                }

                if (parameters['distinctOn']) {

                    let fieldsString = await processQueryHelper
                        .getFieldValuesString(parameters['fields'])
                    let selectString = knex.raw(`${fieldsString}`)
                    packagesQuery = knex.select(selectString)
                    // if fields is specified, surround columns in double quotes
                    packagesQuery = packagesQuery.toString().replace(/(\s+AS\s*)(\w+)(\s*)/g, "$1\"$2\"$3")

                } else {
                    packagesQuery = knex.column(parameters['fields'].split('|'))
                    packagesQuery += `,EXTRACT (YEAR FROM "seed"."harvest_date") AS "sourceHarvestYear"`
                }

                if (hasGermplasmOtherNamesAttribute)
                    packagesQuery += `,
                        germplasm.other_names AS "germplasmOtherNames"`

                packagesQuery += fromQuery
            } else {
                addPackageCount = true
                    packagesQuery = `
                        SELECT 
                            ${additionalSelectFields}
                            seed.id AS "seedDbId",
                            seed.harvest_date AS "harvestDate",
                            EXTRACT (YEAR FROM seed.harvest_date) AS "sourceHarvestYear",        
                            seed.program_id AS "seedProgramDbId",  
                            facility.id AS "facilityDbId", 
                            container.id AS "containerDbId",
                            "subFacility".id AS "subFacilityDbId",
                            facility.facility_name AS "facility",
                            container.facility_name AS "container",
                            "subFacility".facility_name AS "subFacility",
                            creator.person_name AS "creator",
                            modifier.person_name AS "modifier",
                            program.program_code AS "programCode",
                            program.id AS "programDbId"
                            ${retainSelectionStrSelect}
                        `
                        + fromQuery
                
            }

            let buildSort = ''
            let defaultSort = ``

            // Parse the sort parameters
            if (sort != null) {
                // If package data is used for sort, retrieve package data CTE and query
                let sorting = sort.split('|')

                for(let sorts of sorting){
                    let packageDataColumnsArr = ["mtaStatus", "moistureContent", "rshtReferenceNo", "mcCleanSeed"]
                    sortsColumns = sorts.indexOf(":") < 0 ? [sorts] : sorts.split(":")
                    
                    if(!packageDataColumnsArr.includes(sortsColumns[0])) {
                        defaultSort += sorts+'|'
                    }

                    else if (['moistureContent', 'rshtReferenceNo', 'mcCleanSeed'].includes(sortsColumns[0])) {
                        // If sorting is moistureContent, add float param in sorting
                        floatDataMC = sortsColumns[0] == 'moistureContent' ? '.data_value::float ' : '.data_value '
                        sortsColumns[1] = sortsColumns[1] ? sortsColumns[1] : 'asc'
                        buildSort += sortsColumns[0]+floatDataMC+sortsColumns[1]
                    }

                    else if(sortsColumns[0] = 'mtaStatus') {
                        sortsColumns[1] = sortsColumns[1] ? sortsColumns[1] : 'asc'
                        buildSort += sortsColumns[0]+'.mta_status '+sortsColumns[1]
                    }
                }

                // Process sorting
                orderString = await processQueryHelper.getOrderString(defaultSort)
                if (orderString.includes('invalid')) {
                    errMsg = await errorBuilder.getError(req.headers.host, 400004)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Build orderString
                if(defaultSort){
                    orderString = buildSort ? orderString + `, ` +buildSort : orderString
                }
                else {
                    // This is for sorting input list values as defaultSort is empty
                    orderString = orderString + buildSort
                }
            }

            // Build orderString for input list
            if (sortByInputOrderString != '') {
                let columnOrders = '';
                
                if (orderString != '') {
                    columnOrders = ` ,` + orderString.replace('ORDER BY', '').trim()
                }

                orderString = sortValues + columnOrders
            }

            let packageCountQuery = ``;
            if (addPackageCount) {
                packageCountQuery = `
                    COUNT(*) OVER (PARTITION BY "germplasmDbId") AS "packageCount",
                    array_length(uniq(ARRAY_AGG("seedDbId") over (partition by "germplasmDbId")), 1) AS "seedCount",
                `
            }

            if (workingListCondition != '' && empty(workingListCondition)) {

                if (conditionString != '') {
                    workingListCondition = ' AND ' + workingListCondition
                } else {
                    workingListCondition = ' WHERE ' + workingListCondition
                }

                conditionString += workingListCondition
            }

            let groupLimitQuery = ''
            let groupedLimitCol = ''
            
            if(addGroupLimit){
                groupLimitQuery = `
                    WHERE 
                        "groupOrderNumber" <= ${groupLimit}
                `

                groupedLimitCol = ` ROW_NUMBER() OVER (PARTITION BY "germplasmDbId" ORDER BY "harvestDate" DESC nulls last) AS "groupOrderNumber", `
            }
                
            // Generate final SQL query
            let finalPackagesQuery = ''
            let finalQuery = ''

            finalPackagesQuery = `
                SELECT 
                    ${groupedLimitCol}
                    *
                FROM
                    (
                        ${packagesQuery}
                    ) AS tbl
                ${conditionString}
            `

            if(addGroupLimit){
                finalPackagesQuery =   `
                    SELECT
                        *
                    FROM (
                        ${finalPackagesQuery}
                    ) AS sourceData
                    ${groupLimitQuery}
                `
            }

            let orderStatement = `
                ROW_NUMBER () OVER (
                    ${orderString}
                ) AS "orderNumber"
            ` 
            
            summariesCte = `,
                withSummaries AS (
                    SELECT
                        ${distinctOrderString}
                        allData.*
                    FROM (
                        SELECT
                            ${orderStatement},
                            ${packageCountQuery}
                            t.*
                            ${addlDataCteSelect}
                        FROM
                            baseQuery t
                            ${addlDataCteJoin}
                    ) AS allData
                    ${conditionStringCTE}
                )
            `

            let finalDataSource = 'allRecords'
    
            if(isBasic){
                addlDataCte = ''
                finalDataSource = 'withLimitOffset'
            }

            let limitOffsetCte = `,
                withLimitOffset AS (
                    SELECT
                        withSummaries.*
                    FROM
                        withSummaries
                    LIMIT (:limit) OFFSET (:offset)
                )
            `

            let countCte = `
                ,
                countQuery AS (
                    SELECT
                        COUNT(1) AS "totalCount"
                    FROM
                        withSummaries
                )
            `

            finalQuery =  `
                WITH baseQuery AS (
                    ${finalPackagesQuery}
                )
                ${packageData}
                ${summariesCte}
                ${limitOffsetCte}
                ${addlDataCte}
                ${countCte}
                SELECT
                    countQuery."totalCount",
                    ${finalDataSource}.*
                FROM
                    countQuery,
                    ${finalDataSource}
            `

            /**
             * Temporary fix: Manually replacing values in query, instead of using replacements
             * Why? germplasm name values with ':' are being treated as object/placeholders like
             * :name, :limit, and :offset and are not processed properly. Need to check if this
             * happens with other search tools.
             */
            finalQuery = finalQuery.replace(':browserColumn', columnValues)
            finalQuery = finalQuery.replace(':name', `$$$` + nameValue.join(`$$$,$$$`) + `$$$`)
            finalQuery = finalQuery.replace(':limit', limit)
            finalQuery = finalQuery.replace(':offset', offset)

            let packages = await sequelize
                .query(finalQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                .catch(async err => {
                    await logger.logFailingQuery(endpoint, 'SELECT', err)
                })

            if (await packages === undefined){
                errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }
            
            if(await packages.length < 1) {
                res.send(200, {
                    rows: [],
                    count: 0
                })
                return
            }

            count = packages[0].totalCount ?? 0

            res.send(200, {
                rows: packages,
                count: count
            })
            return
        }
        catch(error){
            errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
