/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
const tokenHelper = require("../../../helpers/auth/token");
const errorBuilder = require("../../../helpers/error-builder");
const errors = require("restify-errors");
const forwarded = require("forwarded-for");
const { sequelize } = require("../../../config/sequelize");

module.exports = {

    /**
     * Get next value in a database sequence of a schema
     *
     * @param {*} req Request parameters
     * @param {*} res Respons
     */
    get: async function(req, res, next) {

        let sequenceSchema = req.query.sequenceSchema
        let sequenceName = req.query.sequenceName

        // Check if sequenceSchema is in the input
        if (sequenceSchema == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if sequenceName is in the input
        if (sequenceName == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Validate user id
        let userDbId = await tokenHelper.getUserId(req)
        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).isSecure
        // Set URL for response
        let seedTransfersUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/db-sequence'

        let validateQuery = ``
        let validateCount = 0

        // Validate sequenceSchema
        validateQuery += (validateQuery !== '') ? ' + ' : ''
        validateQuery += `
        (
            --- Check if the schema exists return 1
            SELECT 
                count(1)
            FROM dictionary.schema
            WHERE
                name = '${sequenceSchema}'
        )
        `
        validateCount += 1

        // Validate sequenceName
        validateQuery += (validateQuery !== '') ? ' + ' : ''
        validateQuery += `
        (
            --- Check if the sequence exists in the schema return 1
            SELECT 
                count(1)
            FROM information_schema.sequences 
            WHERE
                sequence_schema = '${sequenceSchema}' AND
                sequence_name = '${sequenceName}'
        )
        `
        validateCount += 1

        // Perform validation
        let checkInputQuery = `
            SELECT ${validateQuery} AS count
        `
        let inputCount = await sequelize.query(checkInputQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (inputCount[0]['count'] != validateCount) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400015)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Get next database sequence
        let nextSequenceQuery = `
            SELECT
                nextval('${sequenceSchema}.${sequenceName}')
            AS nextValue
        `
        let nextSequence = await sequelize.query(nextSequenceQuery, {
            type: sequelize.QueryTypes.SELECT
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
        })
        // Error occurred during query
        if (nextSequence === undefined) return

        let count = nextSequence[0].count

        res.send(200, {
            rows: nextSequence,
            count: count
        })
    }
}