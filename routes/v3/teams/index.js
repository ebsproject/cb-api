/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let userValidator = require('../../../helpers/person/validator.js')
let validator = require('validator')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../helpers/logger')

module.exports = {

  // Endpoint for retrieving the list of teams
  // GET call /v3/teams
  get: async function (req, res, next) {
    // Set defaults 
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let params = req.query
    let sort = req.query.sort
    let count = 0
    let conditionString = ''
    let orderString = ''

    // Build query
    let teamsQuery = `
      SELECT
        team.id AS "teamDbId",
        team.team_code AS "teamCode",
        team.team_name AS "teamName",
        team.description,
        team.creation_timestamp AS "creationTimestamp",
        creator.id AS "creatorDbId",
        creator.person_name AS "creator",
        team.modification_timestamp AS "modificationTimestamp",
        modifier.id AS "modifierDbId",
        modifier.person_name AS "modifier"
      FROM
        tenant.team team
      LEFT JOIN 
        tenant.person creator ON creator.id = team.creator_id::integer
      LEFT JOIN 
        tenant.person modifier ON modifier.id = team.modifier_id::integer
      WHERE 
        team.is_void = FALSE
      ORDER BY
        team.id
    `

    // Get filter condition for abbrev only
    if (params.abbrev !== undefined) {
      let parameters = {
        abbrev: params.abbrev
      }

      conditionString = await processQueryHelper.getFilterString(parameters)

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400003)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)

      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }

    // Generate the final sql query 
    teamFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
      teamsQuery,
      conditionString,
      orderString
    )

    // Retrieve teams from the database   
    let teams = await sequelize.query(teamFinalSqlQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        limit: limit,
        offset: offset
      }
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    if (await teams == undefined || await teams.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    }

    // Get count
    teamCountFinalSqlQuery = await processQueryHelper
      .getCountFinalSqlQuery(
        teamsQuery,
        conditionString,
        orderString,
      )

    teamCount = await sequelize.query(teamCountFinalSqlQuery, {
      type: sequelize.QueryTypes.SELECT
    })
    .catch(async err => {
      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))
      return
    })

    count = teamCount[0].count

    res.send(200, {
      rows: teams,
      count: count
    })
    return
  },
  // Endpoint for creating a new team record
  // POST /v3/teams
  post: async function (req, res, next) {
    // Set defaults 
    let resultArray = []

    let teamDbId = null

    // Get person ID from token
    let personDbId = await tokenHelper.getUserId(req)

    if (personDbId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    //check if user is Admin
    let isAdmin = await userValidator.isAdmin(personDbId)

    if (!isAdmin) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401028)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Check if connection is secure or not (http or https)
    let isSecure = forwarded(req, req.headers).isSecure

    // Set URL for response
    let teamUrlString = (isSecure ? 'https' : 'http')
    + '://'
    + req.headers.host
    + '/v3/teams'

    // Check if the request body does not exist
    if (!req.body) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400009)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    let data = req.body
    let records = []

    try {
      records = data.records

      if (records === undefined || records.length == 0) {
          let errMsg = await errorBuilder.getError(req.headers.host, 400005)
          res.send(new errors.BadRequestError(errMsg))
          return
      }
    } catch (err) {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
    }

    let transaction
    try {
      transaction = await sequelize.transaction({ autocommit: false })

      let teamValuesArray = []

      for (var record of records) {

        // Set defaults
        let teamCode = null
        let teamName = null
        let description = null

        // Check if required columns are in the request body
        if (
          !record.teamCode || !record.teamName
        ) {
          let errMsg = `Required parameters are missing. Ensure that teamCode and teamName fields are not empty.`
          res.send(new errors.BadRequestError(errMsg))
          return
        }

        teamCode = record.teamCode
        teamName = record.teamName
        description = (record.description !== undefined) ? record.description : null

        // get values
        let tempArray = [
          teamCode, teamName, description, personDbId
        ]

        teamValuesArray.push(tempArray)
      }

      // Create team record
      let teamsQuery = format(`
        INSERT INTO
          tenant.team (
            team_code, team_name, description, creator_id
          )
        VALUES
          %L
        RETURNING id`, teamValuesArray
      )

      let teams = await sequelize.query(teamsQuery, {
        type: sequelize.QueryTypes.INSERT,
        transaction: transaction
      }).catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      for (let team of teams[0]) {
        teamDbId = team.id

        let teamRecord = {
          teamDbId: teamDbId,
          recordCount: 1,
          href: teamUrlString + '/' + teamDbId
        }

        resultArray.push(teamRecord)
      }

      await transaction.commit()

      res.send(200, {
        rows: resultArray
      })
      return
    } catch (err) {
      logger.logMessage(__filename, 'POST teams: ' + JSON.stringify(err), 'error')

      // Rollback transaction if any errors were encountered
      if (err) await transaction.rollback()
      let errMsg = await errorBuilder.getError(req.headers.host, 500001)
      res.send(new errors.InternalError(errMsg))
      return
    }
  }
}
