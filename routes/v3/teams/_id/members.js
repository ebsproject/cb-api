/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { variableSet } = require('../../../../config/sequelize')
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token')
let userValidator = require('../../../../helpers/person/validator.js')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Implementation of GET call for /v3/teams/:id/members
    get: async function (req, res, next) {

        let teamDbId = req.params.id

        // Retrieve the user ID of the client from the access token
        let userId = await tokenHelper.getUserId(req)

        if (userId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!validator.isInt(teamDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400072)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let teamsQuery = `
            SELECT 
                team.id AS "teamDbId",
                team.team_code AS "teamCode",
                team.team_name AS "teamName",
                member.person_id AS "personDbId",
                member.person_role_id AS "personRoleDbId",
                role.person_role_code AS "personRoleCode",
                role.person_role_name AS "personRoleName",
                team.description,
                team.creation_timestamp AS "creationTimestamp",
                team.creator_id AS "creatorDbId",
                creator.person_name AS "creator",
                team.modification_timestamp AS "modificationTimestamp",
                team.modifier_id AS "modifierDbId",
                modifier.person_name AS "modifier"
            FROM 
                tenant.team team
            LEFT JOIN 
                tenant.team_member member on member.team_id = team.id
                AND member.is_void = FALSE
                AND member.person_id = (:userId)
            LEFT JOIN 
                tenant.person_role role on role.id = member.person_role_id
            LEFT JOIN 
                tenant.person creator on creator.id = team.creator_id
            LEFT JOIN 
                tenant.person modifier on modifier.id = team.modifier_id
            WHERE 
                team.id = (:teamDbId)
                AND team.is_void = FALSE
            `

            // Retrieve teams from the database   
            let teams = await sequelize.query(teamsQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    teamDbId: teamDbId,
                    userId: userId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found      
            if (await teams == undefined || await teams.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404024)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let columnString = ''

            // Check if user is an administrator. If yes, include more information 
            if (await userValidator.isAdmin(userId)) {
                columnString = `
                    "member".id AS "memberDbId",
                    "user".id AS "personDbId",
                    "user".person_name AS "personName",
                    "user".email,
                    "user".username,
                    "user".person_status AS "personStatus",
                    "user".last_name AS "lastName",
                    "user".first_name AS "firstName",
                    "role".id AS "roleDbId",
                    "role".person_role_name AS "role",
                    "role".person_role_code AS "roleCode",
                    "member".creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    "member".modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                `
            } else {
                columnString = `
                    "user".id AS "personDbId",
                    "user".email,
                    "user".last_name AS "lastName",
                    "user".first_name AS "firstName",
                    "user".person_name AS "personName",
                    "role".id AS "roleDbId",
                    "role".person_role_name AS "role",
                    "role".person_role_code AS "roleCode"
                `
            }

            // Format results from query into an array
            for (team of teams) {
                let membersQuery = `
                    SELECT
                        ` + columnString + `
                    FROM 
                        tenant.team_member "member"
                    LEFT JOIN  
                        tenant.person "user" ON "user".id = "member".person_id
                    LEFT JOIN 
                        tenant.person_role "role" ON "role".id = "member".person_role_id
                    LEFT JOIN 
                        tenant.person creator ON creator.id = "member".creator_id
                    LEFT JOIN 
                        tenant.person modifier ON modifier.id = "member".modifier_id
                    WHERE 
                        "member".is_void = FALSE
                        AND "member".team_id = (:teamDbId)
                        AND "user".is_active = TRUE
                        AND "user".is_void = FALSE
                        AND "role".is_void = FALSE
                    ORDER BY 
                        "member".order_number
                    `

                // Retrieve members from the database   
                let members = await sequelize.query(membersQuery, {
                    type: sequelize.QueryTypes.SELECT,
                    replacements: {
                        teamDbId: teamDbId
                    }
                })
                    .catch(async err => {
                        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                        res.send(new errors.InternalError(errMsg))
                        return
                    })

                team['members'] = members
            }

            res.send(200, {
                rows: teams
            })
            return
        }
    }
}