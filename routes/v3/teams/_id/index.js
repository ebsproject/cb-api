/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { Product } = require('../../../../config/sequelize')
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

  // Endpoint for retrieving a specific team
  // GET call /v3/teams/:id
  get: async function (req, res, next) {

    // Retrieve the team ID
    let teamDbId = req.params.id

    if (!validator.isInt(teamDbId)) {
      let errMsg = await errorBuilder.getError(req.headers.host, 400072)
      res.send(new errors.BadRequestError(errMsg))
      return
    }
    else {
      let teamsQuery = `
        SELECT
          team.id AS "teamDbId",
          team.team_code AS "teamCode",
          team.team_name AS "teamName",
          team.description,
          team.creation_timestamp AS "creationTimestamp",
          creator.id AS "creatorDbId",
          creator.person_name AS "creator",
          team.modification_timestamp AS "modificationTimestamp",
          modifier.id AS "modifierDbId",
          modifier.person_name AS "modifier"
        FROM
          tenant.team team
        LEFT JOIN 
          tenant.person creator ON creator.id = team.creator_id::integer
        LEFT JOIN 
          tenant.person modifier ON modifier.id = team.modifier_id::integer
        WHERE 
          team.is_void = FALSE
        ORDER BY
          team.id
      `

      // Retrieve teams from the database   
      let teams = await sequelize
        .query(teamsQuery, {
          type: sequelize.QueryTypes.SELECT,
          replacements: {
            teamDbId: teamDbId
          }
        })
        .catch(async err => {
          let errMsg = await errorBuilder.getError(req.headers.host, 500004)
          res.send(new errors.InternalError(errMsg))
          return
        })

      // Return error if resource is not found      
      if (await teams == undefined || await teams.length < 1) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404024)
        res.send(new errors.NotFoundError(errMsg))
        return
      }

      res.send(200, {
        rows: teams
      })
      return
    }
  }
}
