/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../config/sequelize')
let tokenHelper = require('../../../helpers/auth/token')
let errors = require('restify-errors')
let errorBuilder = require('../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let validator = require('validator')
let logger = require('../../../helpers/logger')

const enpoint = 'planting-jobs'

module.exports = {
    post: async function (req, res, next) {
        /**
         * Retrieve the user ID via access token in the request body
         * and verify if the user that the ID points to exists
         */
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let isSecure = forwarded(req, req.headers).isSecure

        let plantingJobUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/planting-jobs'

        /**
         * Check if the request body exists since it is
         * required in POST resources; else, end the function
         * immediately and return an error
         */
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        /**
         * Set default values to the variables
         */

         let resultArray = []
         let recordCount = 0
         let data = req.body
         let records = []

         /**
          * Check if the records array exists in the
          * request body and if it has elements
          */
        try {
            records = data.records
            recordCount = records.length

            if (records === undefined || records.length == 0) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400005)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        // Retrieve the valid set of planting job statuses
        let plantingJobStatusQuery = `
            SELECT
                array_agg(value)
            FROM
                master.scale_value
            WHERE
                is_void = FALSE AND
                scale_id = (
                    SELECT
                        scale_id
                    FROM
                        master.variable
                    WHERE
                        abbrev = 'PLANTING_JOB_STATUS'
                )
        `

        let statuses = await sequelize
            .query(plantingJobStatusQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = 'Error'
                res.send(new errors.InternalError(errMsg))
                return
            })

        // Retrieve the valid set of planting job types
        let plantingJobTypeQuery = `
            SELECT
                array_agg(value)
            FROM
                master.scale_value
            WHERE
                is_void = FALSE AND
                scale_id = (
                    SELECT
                        scale_id
                    FROM
                        master.variable
                    WHERE
                        abbrev = 'PLANTING_JOB_TYPE'
                )
                
        `

        let types = await sequelize
            .query(plantingJobTypeQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = 'Error'
                res.send(new errors.InternalError(errMsg))
                return
            })


        /**
         * Start the creation transaction of 
         * planting jobs
         */
        let transaction
        try {
            let plantingJobValuesArray = []

            for (let record of records) {
                /**
                 * @validateQuery the validation query which returns the count of valid checks
                 * @validateCount a local counter that is counter-checked with the return value of the validateQuery
                 */
                let validateQuery = ''
                let validateCount = 0

                let plantingJobStatus = null // required
                let plantingJobType = null // required
                let plantingJobInstructions = null
                let plantingJobDueDate = null
                let facilityDbId = null
                let programDbId = null
                let notes = null

                /**
                 * Check if the required columns
                 * exist in the request body, else
                 * return an error
                 */
                if (
                    !record.plantingJobStatus ||
                    !record.plantingJobType ||
                    !record.programDbId
                ) {
                    let errMsg = `Required parameters are missing. Ensure that  the the following attributes: plantingJobStatus, plantingJobType and programDbId are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                /**
                 * Perform column validations here
                 */

                plantingJobStatus = record.plantingJobStatus
                if (!statuses[0]["array_agg"].includes(plantingJobStatus)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400226)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                plantingJobType = record.plantingJobType
                if (!types[0]["array_agg"].includes(plantingJobType)) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400227)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                plantingJobDueDate = record.plantingJobDueDate
                if (plantingJobDueDate !== undefined) {
                    let validDateFormat = /^\d{4}-\d{2}-\d{2}$/
                    if (!plantingJobDueDate.match(validDateFormat)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400228)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                if (record.facilityDbId !== undefined) {
                    facilityDbId = record.facilityDbId

                    if (!validator.isInt(facilityDbId)) {
                        let errMsg = 'Invalid input format, facility ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    /**
                     * Generate the validation query segment
                     * for the validation of facility record
                     */
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if facility is existing return 1
                            SELECT
                                count(1)
                            FROM
                                place.facility
                            WHERE
                                is_void = FALSE AND
                                id = ${facilityDbId}
                        )
                    `
                    validateCount += 1

                    let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS
                        count
                `

                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (inputCount[0].count != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                // validate program ID
                if (record.programDbId !== undefined) {
                    programDbId = record.programDbId

                    if (!validator.isInt(programDbId)) {
                        let errMsg = 'Invalid input format, program ID must be an integer.'
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    /**
                     * Generate the validation query segment
                     * for the validation of program record
                     */
                    validateQuery += (validateQuery != '') ? ' + ' : ''
                    validateQuery += `
                        (
                            --- Check if program is existing return 1
                            SELECT
                                count(1)
                            FROM
                                tenant.program
                            WHERE
                                is_void = FALSE AND
                                id = ${programDbId}
                        )
                    `
                    validateCount += 1

                    let checkInputQuery = `
                    SELECT
                        ${validateQuery}
                    AS
                        count
                `

                    let inputCount = await sequelize.query(checkInputQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (inputCount[0].count != validateCount) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }

                plantingJobInstructions = (record.plantingJobInstructions !== undefined) ? record.plantingJobInstructions : null

                notes = (record.notes !== undefined) ? record.notes : ''

                // Get values
                let tempArray = [
                    plantingJobStatus,
                    plantingJobType,
                    plantingJobInstructions,
                    plantingJobDueDate,
                    facilityDbId,
                    programDbId,
                    notes,
                    personDbId
                ]

                plantingJobValuesArray.push(tempArray)
            }

            // Create a planting job insertion query
            let plantingJobQuery = format(`
                INSERT INTO
                    experiment.planting_job (
                        planting_job_status,
                        planting_job_type,
                        planting_job_instructions,
                        planting_job_due_date,
                        facility_id,
                        program_id,
                        notes,
                        creator_id
                    )
                VALUES
                    %L
                RETURNING id
            `, plantingJobValuesArray)

            let plantingJobs

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                plantingJobs = await sequelize.query(plantingJobQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(enpoint, 'INSERT', err)
                    throw new Error(err)
                })
            })

            for (let plantingJob of plantingJobs[0]) {
                let plantingJobDbId = plantingJob.id

                let plantingJobRecord = {
                    plantingJobDbId: plantingJobDbId,
                    recordCount: 1,
                    href: plantingJobUrlString + '/' + plantingJobDbId
                }

                resultArray.push(plantingJobRecord)
            }

            res.send(200, {
                rows: resultArray
            })
        }
        catch (err) {
            // Log message when error encountered
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}