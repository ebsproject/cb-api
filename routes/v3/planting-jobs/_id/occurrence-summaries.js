/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {
    /**
     * Retrieval method for planting_job
     * GET /v3/planting-jobs/{id}/occurrence-summaries
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * @return object Response data
     */
    get: async function (req, res, next) {
        let plantingJobDbId = req.params.id
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let orderString = ''

        /* BEGIN Check if Planting Job exists */
        // Validate if passed value is int
        if (!validator.isInt(plantingJobDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400225)
            res.send(new errors.BadRequestError(errMsg))

            return
        }

        let plantingJobQuery = `
            SELECT
                planting_job.id AS "plantingJobDbId",
                planting_job_status AS "plantingJobStatus",
                planting_job_code AS "plantingJobCode",
                planting_job_type AS "plantingJobType",
                planting_job_due_date AS "plantingJobDueDate",
                planting_job_instructions AS "plantingJobInstructions",
                facility_id AS "facilityDbId"
            FROM
                experiment.planting_job
            WHERE
                is_void = FALSE AND
                id = ${plantingJobDbId}
        `

        let plantingJob = await sequelize.query(plantingJobQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                return
            })

        if (
            await plantingJob === undefined
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        } else if (
            await plantingJob.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404048)
            res.send(new errors.NotFoundError(errMsg))

            return
        }
        /* END Check if Planting Job exists */

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)

            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))

                return
            }
        }

        // Query for retrieving occurrence summaries
        let occurrenceSummaryQuery = `
            SELECT
                e.id AS "experimentDbId",
                e.experiment_name AS "experimentName",
                o.id AS "occurrenceDbId",
                o.occurrence_name AS "occurrenceName",
                COUNT(pje.id) AS "entryCount",
                COUNT(is_replaced) FILTER (WHERE is_replaced = TRUE) AS "replacementCount",
                SUM(envelope_count) AS "envelopeCount"
            FROM
                experiment.planting_job_entry pje
            LEFT JOIN
                experiment.planting_job_occurrence pjo
            ON
                pjo.id = pje.planting_job_occurrence_id
            LEFT JOIN
                experiment.planting_job pj
            ON
                pj.id = pjo.planting_job_id
            LEFT JOIN
                experiment.occurrence o
            ON
                o.id = pjo.occurrence_id
            LEFT JOIN
                experiment.experiment e
            ON
                e.id = o.experiment_id
            WHERE
                pj.id = ${plantingJobDbId}
            GROUP BY
                e.id, o.id, pjo.id
            ORDER BY
                e.id
        `
 
        // Build final query
        let occurrenceSummaryFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            occurrenceSummaryQuery,
            '',
            orderString,
            ''
        )

        // Retrieve occurrence summaries from the database   
        let occurrenceSummaries = await sequelize.query(occurrenceSummaryFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                limit: limit,
                offset: offset
            }
        })
            .catch(async err => {
                return
            })

        // End API call if query encountered an error or returned empty
        if (await occurrenceSummaries === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        } else if (await occurrenceSummaries.length < 1) {
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        // Get count 
        let occurrenceSummaryCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(
            occurrenceSummaryQuery, 
            '', 
            orderString,
            ''
        )

        let occurrenceSummaryCount = await sequelize.query(occurrenceSummaryCountFinalSqlQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                return
            })

        // End API call if query encountered an error
        if (await occurrenceSummaryCount === undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))

            return
        }

        res.send(200, {
            rows: occurrenceSummaries,
            count: occurrenceSummaryCount[0].count
        })

        return
    }
}