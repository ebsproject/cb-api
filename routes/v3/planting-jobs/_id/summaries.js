/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {
    /**
     * Retrieve summary of experiments of a planting job record
     * GET /v3/planting-jobs/{id}/summaries
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * @return object Response data
     */
    get: async function (req, res, next) {
        let plantingJobDbId = req.params.id

        if (!validator.isInt(plantingJobDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400077)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plantingJobQuery = `
            SELECT
                planting_job.id AS "plantingJobDbId",
                planting_job_status AS "plantingJobStatus",
                planting_job_code AS "plantingJobCode",
                planting_job_type AS "plantingJobType",
                planting_job_due_date AS "plantingJobDueDate",
                planting_job_instructions AS "plantingJobInstructions",
                facility_id AS "facilityDbId"
            FROM
                experiment.planting_job
            WHERE
                is_void = FALSE AND
                id = ${plantingJobDbId}
        `

        let plantingJob = await sequelize.query(plantingJobQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJob === undefined ||
            await plantingJob.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404048)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let plantingJobSummaryQuery = `
            SELECT
                (
                    SELECT
                        COUNT(DISTINCT occurrence_id) AS "occurrenceCount"
                    FROM
                        experiment.planting_job_occurrence
                    WHERE
                        is_void = FALSE AND
                        planting_job_id = ${plantingJobDbId}
                ) AS "totalOccurrenceCount",
                (
                    SELECT
                        COUNT(DISTINCT experiment_id)
                    FROM
                        experiment.occurrence
                    WHERE
                        is_void = FALSE AND
                        id IN (
                            SELECT
                                occurrence_id
                            FROM
                                experiment.planting_job_occurrence
                            WHERE
                                is_void = FALSE AND
                                planting_job_id = ${plantingJobDbId}
                        )
                ) AS "totalExperimentCount",
                (
                    SELECT
                        SUM(pje.envelope_count) AS "envelopesCount"
                    FROM    
                        experiment.planting_job_occurrence pj,
                        experiment.planting_job_entry pje
                    WHERE
                        pj.is_void = FALSE AND
                        pj.planting_job_id = ${plantingJobDbId} AND
                        pje.planting_job_occurrence_id = pj.id AND
                        pje.is_void = FALSE               
                ) AS "totalEnvelopesCount",
                (
                    
                    SELECT 
                        SUM("replacementCount")
                    FROM
                        (
                            SELECT
                                COUNT(1) AS "replacementCount"
                            FROM
                                experiment.planting_job_entry e,
                                experiment.planting_job_occurrence o
                            WHERE
                                o.planting_job_id = ${plantingJobDbId} AND
                                o.is_void = FALSE AND
                                e.planting_job_occurrence_id = o.id AND
                                e.is_replaced = TRUE AND
                                e.is_void = FALSE
                            GROUP BY
                                e.planting_job_occurrence_id
                        ) sum
                ) AS "totalReplacementCount"
        `


        let plantingJobSummary = await sequelize
            .query(plantingJobSummaryQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJobSummary == undefined ||
            await plantingJobSummary.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404051)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let experimentQuery = `
            WITH t AS (
                SELECT 
                    e.id AS "experimentDbId",
                    e.experiment_code AS "experimentCode",
                    e.experiment_name AS "experimentName",
                    COUNT(DISTINCT o.id) AS "occurrenceCount",
                    COUNT(DISTINCT pje.entry_id) AS "entryCount",
                    COUNT(DISTINCT pje.entry_id) FILTER (WHERE pje.is_replaced = TRUE) AS "replacementCount",
                    (
                        SELECT
                            SUM(pje.envelope_count)
                        FROM
                            experiment.planting_job_entry AS pje,
                            experiment.planting_job_occurrence AS pjo,
                            experiment.occurrence AS o,
                            experiment.entry_list el,
                            experiment.entry et
                        WHERE
                            o.experiment_id =e.id
                            AND pjo.id = pje.planting_job_occurrence_id
                            AND o.id = pjo.occurrence_id
                            AND e.id = el.experiment_id
                            AND el.is_void = FALSE
                            AND et.entry_list_id = el.id
                            AND et.is_void = FALSE
                            AND pje.entry_id = et.id
                            AND pje.is_void = FALSE
                            AND pjo.is_void = FALSE
                            AND o.is_void = FALSE
                
                    ) AS "envelopeCount"
                FROM 
                    experiment.planting_job_occurrence pjo 
                        LEFT JOIN experiment.occurrence o ON o.id =pjo.occurrence_id
                        LEFT JOIN experiment.experiment e ON o.experiment_id =e.id
                        LEFT JOIN experiment.planting_job_entry pje ON pje.planting_job_occurrence_id =pjo.id
                WHERE 
                    pjo.planting_job_id = ${plantingJobDbId}
                
                GROUP BY e.id, o.id
            )
            SELECT 
                t."experimentDbId",
                t."experimentCode", 
                t."experimentName",
                t."entryCount", 
                t."envelopeCount",
                SUM(t."replacementCount") AS "replacementCount", 
                SUM(t."occurrenceCount") AS "occurrenceCount"
            FROM
                t
            GROUP BY
                t."experimentDbId", t."experimentCode", t."experimentName", t."entryCount", t."envelopeCount"
        `

        let experiments = await sequelize
            .query(experimentQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        plantingJobSummary[0]["experiments"] = experiments
        plantingJob[0]["summaries"] = plantingJobSummary[0]

        res.send(200, {
            rows: plantingJob
        })

        return
    }
}