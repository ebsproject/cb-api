/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let tokenHelper = require('../../../../helpers/auth/token.js')
let errorBuilder = require('../../../../helpers/error-builder')
let forwarded = require('forwarded-for')
let format = require('pg-format')
let logger = require('../../../../helpers/logger')

const endpoint = 'planting-jobs/:id'

module.exports = {
    /**
     * Update planting job record
     * PUT /v3/planting-jobs/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    put: async function (req, res, next) {

        let plantingJobDbId = req.params.id

        if (!validator.isInt(plantingJobDbId)) {
            let errMsg = 'Invalid format, planting job ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plantingJobQuery = `
            SELECT
                p.planting_job_status AS "plantingJobStatus",
                p.planting_job_type AS "plantingJobType",
                p.planting_job_due_date AS "plantingJobDueDate",
                p.planting_job_instructions AS "plantingJobInstructions",
                p.facility_id AS "facilityDbId",
                p.is_submitted as "isSubmitted"
            FROM
                experiment.planting_job p
            WHERE
                p.is_void = FALSE AND
                p.id = ${plantingJobDbId}
        `

        let plantingJobs = await sequelize
            .query(plantingJobQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJobs === undefined ||
            await plantingJobs.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404048)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let recordPlantingJobStatus = plantingJobs[0].plantingJobStatus
        let recordPlantingJobType = plantingJobs[0].plantingJobType
        let recordPlantingJobDueDate = plantingJobs[0].plantingJobDueDate
        let recordPlantingJobInstructions = plantingJobs[0].plantingJobInstructions
        let recordFacilityDbId = plantingJobs[0].facilityDbId
        let recordIsSubmitted = plantingJobs[0].isSubmitted

        let isSecure = forwarded(req, req.headers).secure

        let plantingJobUrlString = (isSecure ? 'https' : 'http')
            + "://"
            + req.headers.host
            + "/v3/planting-jobs/"
            + plantingJobDbId

        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        let transaction
        try {

            let setQuery = ''
            let validateQuery = ''
            let validateCount = ''

            // Validation of parameters
            let plantingJobStatus
            if (data.plantingJobStatus !== undefined) {
                plantingJobStatus = data.plantingJobStatus

                if (plantingJobStatus != recordPlantingJobStatus) {
                    // Retrieve the valid set of planting job statuses
                    let plantingJobStatusQuery = `
                        SELECT
                            array_agg(value)
                        FROM
                            master.scale_value
                        WHERE
                            is_void = FALSE AND
                            scale_id = (
                                SELECT
                                    scale_id
                                FROM
                                    master.variable
                                WHERE
                                    abbrev = 'PLANTING_JOB_STATUS'
                            )
                    `

                    let statuses = await sequelize
                        .query(plantingJobStatusQuery, {
                            type: sequelize.QueryTypes.SELECT,
                        })
                        .catch(async err => {
                            let errMsg = 'Error'
                            res.send(new errors.InternalError(errMsg))
                            return
                        })

                    if (!statuses[0]["array_agg"].includes(plantingJobStatus)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400226)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        planting_job_status = $$${plantingJobStatus}$$
                    `
                }
            }

            let plantingJobType
            if (data.plantingJobType !== undefined) {
                plantingJobType = data.plantingJobType

                if (plantingJobType != recordPlantingJobType) {
                    // Retrieve the valid set of planting job types
                    let plantingJobTypeQuery = `
                        SELECT
                            array_agg(value)
                        FROM
                            master.scale_value
                        WHERE
                            is_void = FALSE AND
                            scale_id = (
                                SELECT
                                    scale_id
                                FROM
                                    master.variable
                                WHERE
                                    abbrev = 'PLANTING_JOB_TYPE'
                            )
                            
                    `

                    let types = await sequelize
                        .query(plantingJobTypeQuery, {
                            type: sequelize.QueryTypes.SELECT,
                        })
                        .catch(async err => {
                            let errMsg = 'Error'
                            res.send(new errors.InternalError(errMsg))
                            return
                        })

                    if (!types[0]["array_agg"].includes(plantingJobType)) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400227)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }

                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        planting_job_type = $$${plantingJobType}$$
                    `
                }
            }

            let plantingJobDueDate
            if (data.plantingJobDueDate !== undefined) {
                plantingJobDueDate = data.plantingJobDueDate

                if (plantingJobDueDate == '') {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        planting_job_due_date = NULL
                    `
                } else {
                    if (plantingJobDueDate != recordPlantingJobDueDate) {
                        let validDateFormat = /^\d{4}-\d{2}-\d{2}$/
                        if (!plantingJobDueDate.match(validDateFormat)) {
                            let errMsg = await errorBuilder.getError(req.headers.host, 400228)
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            planting_job_due_date = $$${plantingJobDueDate}$$
                        `
                    }
                }
            }

            let plantingJobInstructions
            if (data.plantingJobInstructions !== undefined) {
                plantingJobInstructions = data.plantingJobInstructions

                if (plantingJobInstructions != recordPlantingJobInstructions) {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        planting_job_instructions = $$${plantingJobInstructions}$$
                    `
                }
            }

            let facilityDbId
            if (data.facilityDbId !== undefined) {
                facilityDbId = data.facilityDbId

                if (facilityDbId == '') {
                    setQuery += (setQuery != '') ? ',' : ''
                    setQuery += `
                        facility_id = NULL
                    `
                } else {
                    if (facilityDbId != recordFacilityDbId) {
                        if (!validator.isInt(facilityDbId)) {
                            let errMsg = 'Invalid input format, facility ID must be an integer.'
                            res.send(new errors.BadRequestError(errMsg))
                            return
                        }

                        /**
                         * Generate the validation query segment
                         * for the validation of facility record
                         */
                        validateQuery += (validateQuery != '') ? ' + ' : ''
                        validateQuery += `
                            (
                                --- Check if facility is existing return 1
                                SELECT
                                    count(1)
                                FROM
                                    place.facility
                                WHERE
                                    is_void = FALSE AND
                                    id = ${facilityDbId}
                            )
                        `
                        validateCount += 1

                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            facility_id = $$${facilityDbId}$$
                        `
                    }

                }
            }

            let isSubmitted
            if (data.isSubmitted !== undefined) {
                isSubmitted = data.isSubmitted
                
                if (isSubmitted != recordIsSubmitted) {
                    if (isSubmitted == "true" || isSubmitted == "false") {
                        setQuery += (setQuery != '') ? ',' : ''
                        setQuery += `
                            is_submitted = $$${isSubmitted}$$
                        `
                    } else {
                    
                        let errMsg = `Invalid value for isSubmitted. Value should be true or false`
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }
            }

            if (validateCount > 0) {
                let checkInputQuery = `
                SELECT
                    ${validateQuery}
                AS
                    count
                `

                let inputCount = await sequelize.query(checkInputQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if (inputCount[0].count != validateCount) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400015)
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
            }

            if (setQuery.length > 0) setQuery += ','

            // Update the planting instruction record
            let updatePlantingJobQuery = `
                UPDATE
                    experiment.planting_job
                SET
                    ${setQuery}
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${plantingJobDbId}
            `

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updatePlantingJobQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            // Return the transaction info
            let result = {
                plantingJobDbId: plantingJobDbId,
                recordCount: 1,
                href: plantingJobUrlString
            }

            res.send(200, {
                rows: result
            })

            // Update the planting job 
        } catch (err) {
            // Log message when error encountered
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Delete planting job record
     * PUT /v3/planting-jobs/:id
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * 
     * @return object Response data
     */
    delete: async function (req, res, next) {
        let plantingJobDbId = req.params.id

        if (!validator.isInt(plantingJobDbId)) {
            let errMsg = 'Invalid format, planting job ID must be an integer.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve person ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plantingJobQuery = `
            SELECT
                planting_job_status AS "plantingJobStatus"
            FROM
                experiment.planting_job
            WHERE
                id = ${plantingJobDbId} AND
                is_void = FALSE
        `

        let plantingJob = await sequelize
            .query(plantingJobQuery, {
                type: sequelize.QueryTypes.SELECT
            })

        if (
            await plantingJob.length < 1 ||
            await plantingJob == undefined
        ) {
            let errMsg = 'Invalid request, the planting job ID you have provided does not exist.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let plantingJobStatus = plantingJob[0].plantingJobStatus

        if (plantingJobStatus == 'packed') {
            let errMsg = await errorBuilder.getError(req.headers.host, 403011)
            res.send(new errors.ForbiddenError(errMsg))
            return
        }

        let transaction
        try {
            let deletePlantingJobQuery = format(`
                UPDATE
                    experiment.planting_job
                SET
                    planting_job_code = CONCAT('-', $$${plantingJobDbId}$$)::int,
                    is_void = TRUE,
                    notes = CONCAT('VOIDED-', $$${plantingJobDbId}$$),
                    modification_timestamp = NOW(),
                    modifier_id = ${personDbId}
                WHERE
                    id = ${plantingJobDbId}
            `)

            // Start transactions
            transaction = await sequelize.transaction(async transaction =>{
                await sequelize.query(deletePlantingJobQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                })
            }).catch(async err => {
                logger.logFailingQuery(endpoint, 'DELETE', err)
                throw new Error(err)
            })

            res.send(200, {
                rows: {
                    plantingJobDbId: plantingJobDbId
                }
            })

            return
        } catch (err) {
            // Log error message when encountered error
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}