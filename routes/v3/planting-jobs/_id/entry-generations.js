/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let tokenHelper = require('../../../../helpers/auth/token')
let plantingJobHelper = require('../../../../helpers/plantingJob/index.js')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let logger = require('../../../../helpers/logger')

module.exports = {

    // POST /v3/planting-jobs/:id/entry-generations
    post: async function (req, res, next) {
        // Retrieve planting job ID
        let plantingJobDbId = req.params.id
        let isUpdated = false

        // Check if ID is an integer
        if (!validator.isInt(plantingJobDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400025)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Retrieve user ID of client from access token
        let personDbId = await tokenHelper.getUserId(req)

        if (personDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // check if planting job exists
        let query = `
            SELECT
                id
            FROM
                experiment.planting_job
            WHERE
                is_void = FALSE
                and id = ${plantingJobDbId}
        `

        let plantingJobQuery = await sequelize.query(query, {
            type: sequelize.QueryTypes.SELECT
        })

        if (plantingJobQuery.length == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404048)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        // check if there are entry records that need to be recomputed
        query = `
            SELECT 
                planting_job_occurrence.planting_job_id 
            FROM 
                experiment.planting_job_occurrence planting_job_occurrence
            WHERE 
                planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                AND planting_job_occurrence.notes = 'UPDATED'
                AND planting_job_occurrence.is_void = FALSE
            LIMIT
                1

        `

        let plantingJobOccurrenceQuery = await sequelize.query(query, {
            type: sequelize.QueryTypes.SELECT
        })

        // if there are planting job occurrence updated
        if(plantingJobOccurrenceQuery.length > 0){
            isUpdated = true
        }

        if(!isUpdated){
            // check if no previous planting entry records already generated
            query = `
                SELECT 
                    planting_job_occurrence.planting_job_id 
                FROM 
                    experiment.planting_job_occurrence planting_job_occurrence, 
                    experiment.planting_job planting_job, 
                    experiment.planting_job_entry planting_job_entry 
                WHERE 
                    planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                    AND planting_job.id = planting_job_occurrence.planting_job_id 
                    AND planting_job_entry.planting_job_occurrence_id = planting_job_occurrence.id
                    AND planting_job.is_void = FALSE
                    AND planting_job_occurrence.is_void = FALSE
                    AND planting_job_entry.is_void = FALSE
                LIMIT 1
            `
            let plantingJobEntryQuery = await sequelize.query(query, {
                type: sequelize.QueryTypes.SELECT
            })

            if (plantingJobEntryQuery.length > 0) {
                let errMsg = `Planting job entry records already exist.`
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        try {

            let entryCount
            if(!isUpdated){
                entryCount = await plantingJobHelper.generatePlantingJobEntries(plantingJobDbId, personDbId, res)
            }else{
                entryCount = await plantingJobHelper.computeDerivedValues(plantingJobDbId, personDbId, res)
            }

            if (typeof entryCount === "boolean") {
                return
            }

            resultArray = {
                plantingJobDbId: plantingJobDbId,
                recordCount: entryCount
            }

            res.send(200, {
                rows: resultArray
            })
            return

        } catch (err) {
            logger.logMessage(__filename, err.stack, 'error')

            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if (!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}