/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')
let processQueryHelper = require('../../../../helpers/processQuery/index.js')

module.exports = {
    /**
     * Retrieval method for planting_job
     * GET /v3/planting-jobs/{id}/entries-summaries
     * 
     * @param {*} req Request parameters
     * @param {*} res Response
     * @return object Response data
     */
    get: async function (req, res, next) {
        let plantingJobDbId = req.params.id
        let experimentDbId = req.query.experimentDbId
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let sort = req.query.sort
        let orderString = ''
        let paginate = ` LIMIT (:limit) OFFSET (:offset) `

        let plantingJobQuery = `
            SELECT
                planting_job.id AS "plantingJobDbId",
                planting_job_status AS "plantingJobStatus",
                planting_job_code AS "plantingJobCode",
                planting_job_type AS "plantingJobType",
                planting_job_due_date AS "plantingJobDueDate",
                planting_job_instructions AS "plantingJobInstructions",
                facility_id AS "facilityDbId"
            FROM
                experiment.planting_job
            WHERE
                is_void = FALSE AND
                id = ${plantingJobDbId}
        `

        let plantingJob = await sequelize.query(plantingJobQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJob === undefined ||
            await plantingJob.length < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404048)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (experimentDbId == undefined) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400224)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        if (!validator.isInt(experimentDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400133)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let experimentQuery = `
            SELECT
                count(1)
            FROM
                experiment.experiment
            WHERE
                is_void = FALSE AND
                id = ${experimentDbId}
        `

        let experiment = await sequelize
            .query(experimentQuery, {
                type: sequelize.QueryTypes.SELECT
            })

        if (experiment[0]['count'] == 0) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404049)
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        if (!validator.isInt(plantingJobDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400225)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }

        plantingJobQuery = `
            SELECT
                planting_job.id AS "plantingJobDbId",
                planting_job_status AS "plantingJobStatus",
                planting_job_code AS "plantingJobCode",
                planting_job_type AS "plantingJobType",
                planting_job_due_date AS "plantingJobDueDate",
                planting_job_instructions AS "plantingJobInstructions",
                facility_id AS "facilityDbId"
            FROM
                experiment.planting_job
            WHERE
                is_void = FALSE AND
                id = ${plantingJobDbId}
        `

        plantingJob = await sequelize.query(plantingJobQuery, {
            type: sequelize.QueryTypes.SELECT
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (
            await plantingJob === undefined ||
            await plantingJob.length < 1
        ) {
            let errMsg = 'The planting job you have provided does not exist.'
            res.send(new errors.NotFoundError(errMsg))
            return
        }

        let selectQuery = `
            SELECT
                planting_job_entry.*,
                e.id AS "entryDbId",
                e.entry_name AS "entryName",
                e.entry_code AS "entryCode",
                e.entry_number AS "entryNumber",
                ex.id AS "experimentDbId",
                ex.experiment_name AS "experimentName",
                ex.experiment_code AS "experimentCode",
                germplasm.id AS "germplasmDbId",
                germplasm.designation AS "germplasmName",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.parentage AS "germplasmParentage"
            FROM
            experiment.entry_list el 
                LEFT JOIN experiment.experiment ex ON ex.id = el.experiment_id 
                LEFT JOIN experiment.entry e ON e.entry_list_id = el.id
                LEFT JOIN experiment.planting_job_entry planting_job_entry ON planting_job_entry.entry_id =e.id
                LEFT JOIN experiment.planting_job_occurrence ON planting_job_occurrence.id=planting_job_entry.planting_job_occurrence_id
                LEFT JOIN experiment.occurrence o ON o.id = planting_job_occurrence.occurrence_id
                LEFT JOIN germplasm.germplasm germplasm ON germplasm.id = e.germplasm_id 
            WHERE 
                ex.id = ${experimentDbId} 
                AND el.is_void= FALSE
                AND planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                AND o.is_void = FALSE
                AND planting_job_entry.is_void = FALSE
                AND planting_job_occurrence.is_void = FALSE
        `

        let entrySummaryQuery = `
            SELECT 
                "entryDbId",
                "entryName",
                "entryCode",
                "entryNumber",
                "experimentDbId",
                "experimentName",
                "experimentCode",
                "germplasmDbId",
                "germplasmName",
                "germplasmCode",
                "germplasmParentage",
                SUM(required_package_quantity) AS "totalRequiredPackageQuantity",
                STRING_AGG( DISTINCT required_package_unit, ', ') AS "requiredPackageUnit",
                SUM(envelope_count)::int AS "totalEnvelopeCount",
                COUNT (DISTINCT planting_job_occurrence_id) FILTER (WHERE is_replaced = TRUE)::int AS "replacedOccurrenceCount",
                (CASE WHEN (COUNT( is_replaced) FILTER (WHERE is_replaced = TRUE))>0 then TRUE else FALSE end) AS "isReplaced"
                
            FROM 
                entries
            GROUP BY "entryDbId", "entryName", "entryCode", 
            "entryNumber", entries."experimentDbId", entries."experimentName", 
            entries."experimentCode", entries."germplasmDbId", entries."germplasmName", 
            entries."germplasmCode", entries."germplasmParentage"
            
        `

        let entrySummary = await sequelize
            .query(`WITH entries AS (${selectQuery}) ${entrySummaryQuery} ${orderString} ${paginate}`, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    limit: limit,
                    offset: offset
                }
            })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        let countSql =
            `
            SELECT COUNT(*) 
            FROM (                
                SELECT
                    1
                FROM
                    experiment.planting_job_entry planting_job_entry 
                    LEFT JOIN experiment.entry e ON planting_job_entry.entry_id =e.id
                    LEFT JOIN experiment.entry_list el ON e.entry_list_id = el.id
                    LEFT JOIN experiment.experiment ex ON ex.id = el.experiment_id 
                    LEFT JOIN experiment.planting_job_occurrence ON planting_job_occurrence.id=planting_job_entry.planting_job_occurrence_id
                    LEFT JOIN experiment.occurrence o ON o.id = planting_job_occurrence.occurrence_id
                WHERE 
                    el.experiment_id = ${experimentDbId} 
                    AND el.is_void= FALSE
                    AND planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                    AND o.is_void = FALSE
                    AND planting_job_entry.is_void = FALSE
                    AND planting_job_occurrence.is_void = FALSE
                    GROUP BY e.id
            ) t
        `
        let entryCount = await sequelize
            .query(countSql, {
                type: sequelize.QueryTypes.SELECT
            })
        if (
            await entrySummary == undefined ||
            await entryCount[0]["count"] < 1
        ) {
            let errMsg = await errorBuilder.getError(req.headers.host, 404050)
            res.send(new errors.NotFoundError(errMsg))
            return
        }
        plantingJob[0]["entry-summaries"] = entrySummary

        res.send(200, {
            rows: plantingJob,
            count: entryCount[0]["count"]
        })

        return
    }
}