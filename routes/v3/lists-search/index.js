/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize, lists } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')
let tokenHelper = require('../../../helpers/auth/token')
let logger = require('../../../helpers/logger')
let { knex } = require('../../../config/knex')

module.exports = {
  // Endpoint for searching list record with advanced filters
  // Implementation of POST call for /v3/lists-search
  post: async function (req, res, next) {
    const endpoint = 'lists-search'

    // Set defaults
    let limit = req.paginate.limit
    let offset = req.paginate.offset
    let sort = req.query.sort
    let count = 0
    let orderString = ''
    let conditionString = ''
    let addedConditionString = ''
    let parameters = {}
    let params = req.params
    let ownership = ''    
    let teamQuery = ``
    let programPermissionQuery = ``
    let personRoleCond = ``

    // Retrieve the user ID of the client from the access token
    let userId = await tokenHelper.getUserId(req)

    if (userId == null) {
      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
      res.send(new errors.BadRequestError(errMsg))
      return
    }

    // Get all programs that the user belongs to
    let getProgramsQuery =  `
      SELECT 
        program.id 
      FROM 
        tenant.program program,
        tenant.program_team pt,
        tenant.team_member tm
      WHERE
        tm.person_id = ${userId}
        AND pt.team_id = tm.team_id
        AND program.id = pt.program_id
        AND tm.is_void = FALSE
        AND pt.is_void = FALSE
        AND program.is_void = FALSE
    `
    let programIds = await sequelize.query(getProgramsQuery, {
      type: sequelize.QueryTypes.SELECT
    })

    // Get person role ID of Data owner and Data producer
    let personRoleQuery = `
      SELECT 
        id 
      FROM 
        tenant.person_role
      WHERE
        person_role_code in ('DATA_OWNER', 'DATA_PRODUCER')
    `   

    let personRoleIds = await sequelize.query(personRoleQuery, {
      type: sequelize.QueryTypes.SELECT
    })

    // build person role condition
    for (let personRoleObj of personRoleIds) {
      personRoleCond += (personRoleCond == ``) ? `'${personRoleObj.id}'` : `,'${personRoleObj.id}'`
    }

    personRoleCond = (personRoleCond == ``) ? `'0'` : personRoleCond

    for (let programObj of programIds) {
      if (teamQuery != ``) {
        teamQuery += ` OR `
      }

      teamQuery += `list.access_data #> $$\{program,${programObj.id}}$$ is not null`
      
      programPermissionQuery += ` WHEN list.access_data->'program'->'${programObj.id}'->>'dataRoleId' IN (${personRoleCond}) THEN 'read_write'`
      
    }

    let userQuery = `list.access_data #> $$\{user,${userId}}$$ is not null`
    if (teamQuery != ``) {
      userQuery += ` OR ` + teamQuery
    }

    if (params.ownershipType !== undefined) {
      if (params.ownershipType == 'owned') {
        ownership = `list.creator_id = ${userId}`
      } else if (params.ownershipType == 'combined') {
        ownership = userQuery
      } else if (params.ownershipType == 'shared') {
        ownership = `(` + userQuery + `) AND list.creator_id != ${userId}`
      } else {
        let errMsg = await errorBuilder.getError(req.headers.host, 400164)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    } else {
      ownership = userQuery
    }

    // hide working lists by default
    let workingListQuery = ` AND list.list_usage <> 'working list'`
    if (params.showWorkingList !== undefined) {
        if (params.showWorkingList == 'true') {
            workingListQuery = ``
        }
    }

    if (req.body != undefined) {
      if (req.body.fields != null) {
        parameters['fields'] = req.body.fields
      }
      // Set columns to be excluded in parameters for filtering
      let excludedParametersArray = ['fields']
      // Get filter condition
      conditionString = await processQueryHelper.getFilter(
        req.body,
        excludedParametersArray
      )

      if (conditionString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400022)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Build the base retrieval query
    let listsQuery = null
    let fromQuery = `
      FROM
        platform.list list
        JOIN tenant.person creator ON list.creator_id = creator.id        
        LEFT JOIN tenant.person modifier ON list.modifier_id = modifier.id
      WHERE
        list.is_void = FALSE AND
        (${ownership})
        `+addedConditionString+`
        `+workingListQuery+`
      ORDER BY
        list.id
    `
    // Check if the client specified values for the fields
    if (parameters['fields'] != null) {
      listsQuery = knex.column(parameters['fields'].split('|'))
      listsQuery += fromQuery
    } else {
      listsQuery = `
        SELECT DISTINCT ON (list.id)
            list.id AS "listDbId",
            list.abbrev,
            list.name,
            list.display_name AS "displayName",
            list.type,
            list.list_sub_type AS "subType",
            list.entity_id AS "entityId",
            list.description,
            list.remarks,
            list.notes,
            list.is_active AS "isActive",
            list.status,
            list.list_usage AS "listUsage",
            list.creation_timestamp AS "creationTimestamp",
            creator.id AS "creatorDbId",
            creator.person_name AS creator,
            list.modification_timestamp AS "modificationTimestamp",
            modifier.id AS "modifierDbId",
            modifier.person_name AS modifier,
            (
              SELECT
                  COUNT(lm.id) AS member_count
              FROM
                  platform.list_member lm
              WHERE
                  lm.list_id = list.id
                  AND lm.is_void = FALSE
                  AND lm.is_active = TRUE
            ) AS "memberCount",
            (
              COALESCE(array_length(array(SELECT jsonb_object_keys(list.access_data->'user')), 1) - 1, 0) + 
              COALESCE(array_length(array(SELECT jsonb_object_keys(list.access_data->'program')), 1), 0)
            ) AS "shareCount",
            (
              CASE 
                WHEN list.access_data->'user'->'${userId}'->>'dataRoleId' IN (${personRoleCond}) THEN 'read_write'
                ${programPermissionQuery}
                ELSE 'read' 
              END
            ) AS "permission"
        ${fromQuery}
      `
    }

    // Parse the sort parameters
    if (sort != null) {
      orderString = await processQueryHelper.getOrderString(sort)
      if (orderString.includes('invalid')) {
        let errMsg = await errorBuilder.getError(req.headers.host, 400004)
        res.send(new errors.BadRequestError(errMsg))
        return
      }
    }
    // Generate the final SQL query
    listsFinalSqlQuery = await processQueryHelper.getFinalSqlQueryWoLimit(
      listsQuery,
      conditionString,
      orderString
    )

    let finalDataCountQuery = await processQueryHelper
      .getFinalTotalCountQuery(listsFinalSqlQuery, orderString)

    // Retrieve the list records
    let lists = await sequelize
      .query(finalDataCountQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
          limit: limit,
          offset: offset
        }
      })
      .catch(async err => {
        await logger.logFailingQuery(endpoint, 'SELECT', err)

        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

    if (await lists == undefined || await lists.length < 1) {
      res.send(200, {
        rows: [],
        count: 0
      })
      return
    } else {
      
      count = lists[0].totalCount

      res.send(200, {
        rows: lists,
        count: count
      })
      return
    }
  }
}