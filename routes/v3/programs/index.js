/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../config/sequelize')
let errors = require('restify-errors')
let processQueryHelper = require('../../../helpers/processQuery/index.js')
let errorBuilder = require('../../../helpers/error-builder')

const validator = require('validator')
const tokenHelper = require('../../../helpers/auth/token.js')
let forwarded = require('forwarded-for')
const logger = require('../../../helpers/logger')
const format = require('pg-format')
let userValidator = require('../../../helpers/person/validator.js')

module.exports = {
    // Endpoint for retrieving the list of programs
    // GET /v3/programs
    get: async function (req, res, next) {
        // Set defaults
        let limit = req.paginate.limit
        let offset = req.paginate.offset
        let params = req.query
        let sort = req.query.sort
        let count = 0
        let conditionString = ''
        let orderString = ''
        // Build query
        let programsQuery = `
            SELECT
                program.id AS "programDbId",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                program.description,
                program.program_type AS "programType",
                program.program_status AS "programStatus",
                program.crop_program_id AS "cropProgramDbId",
                program.creation_timestamp AS "creationTimestamp",
                creator.person_name AS creator,
                creator.id AS "creatorDbId",
                program.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM 
                tenant.program program
            LEFT JOIN
                tenant.person creator ON program.creator_id = creator.id
            LEFT JOIN
                tenant.person modifier ON program.modifier_id = modifier.id
            WHERE 
                program.is_void = FALSE
            ORDER BY
                program.id
        `
        // Get filter condition for program code only
        if (params.programCode !== undefined) {
            let parameters = {
                programCode: params.programCode
            }
            conditionString = await processQueryHelper.getFilterString(parameters)
            if (conditionString.includes('invalid')) {
                let errMsg = "You have provided an invalid filter value for the program code."
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Parse the sort parameters
        if (sort != null) {
            orderString = await processQueryHelper.getOrderString(sort)
            if (orderString.includes('invalid')) {
                let errMsg = await errorBuilder.getError(req.headers.host, 400004)
                res.send(new errors.BadRequestError(errMsg))
                return
            }
        }
        // Generate the final sql query
        programFinalSqlQuery = await processQueryHelper.getFinalSqlQuery(
            programsQuery,
            conditionString,
            orderString
        )
        // Retrieve programs from the database
        let programs = await sequelize
          .query(programFinalSqlQuery, {
              type: sequelize.QueryTypes.SELECT,
              replacements: {
                  limit: limit,
                  offset: offset
              }
          }).catch(async err => {
              let errMsg = await errorBuilder.getError(req.headers.host, 500004)
              res.send(new errors.InternalError(errMsg))
              return
          })

        if (await programs == undefined || await programs.length < 1) {
          res.send(200, {
              rows: [],
              count: 0
          })
          return

        } else {
            // Get count 
            programCountFinalSqlQuery = await processQueryHelper.getCountFinalSqlQuery(programsQuery, conditionString, orderString)
            programCount = await sequelize.query(programCountFinalSqlQuery, {
                type: sequelize.QueryTypes.SELECT
            }).catch(async err => {
                  let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                  res.send(new errors.InternalError(errMsg))
                  return
            })
            count = programCount[0].count
        }

        res.send(200, {
            rows: programs,
            count: count
        })

        return
    },

    // Endpoint for creating new programs
    // POST /v3/programs
    post: async function(req, res, next) {
        let resultArray = []
  		  // Get person ID from token
  		  let personDbId = await tokenHelper.getUserId(req)
  		  // If person ID is null, display error
  		  if(personDbId == null) {
  		      let errMsg = await errorBuilder.getError(req.headers.host, 401002)
  		      res.send(new errors.BadRequestError(errMsg))
  		      return
  		  }

  		  //check if user is admin
  		  let isAdmin = await userValidator.isAdmin(personDbId)
  		  if(!isAdmin) {
  		      let errMsg = await errorBuilder.getError(req.headers.host, 401028)
  		      res.send(new errors.BadRequestError(errMsg))
  		      return
  		  }

  		  // Check if connection is secure or not (http or https)
  		  let isSecure = forwarded(req, req.headers).secure

  		  // Set the URL for response
  		  let programUrlString = (isSecure ? 'https' : 'http')
  		    	+ "://"
  		  	  + req.headers.host
  		  	  + "/v3/programs"

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // If request body is empty, return error message
        let data = req.body
        let records = data.records ?? []
        if(records === undefined ||  records.length == 0) {
        	let errMsg = await errorBuilder.getError(req.headers.host, 400005)
        	res.send(new errors.BadRequestError(errMsg))
        	return
        }

        // Validation request body
        let transaction
        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let programValuesArray = []
            for(var record of records) {
                let programCode = null
                let programName = null
                let programType = null
                let programStatus = null
                let description = null
                let cropProgramDbId = null

                // Validation for the required parameters
                if(record.programCode == undefined || record.programName == undefined || record.programType == undefined || record.cropProgramDbId == undefined ) {
                    let errMsg = `Required parameters are missing. Ensure that programCode, programName, programType and cropProgramDbId fields are not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                // Assign request body params to variables
                programCode = record.programCode
                programName = record.programName
                programType = record.programType
                programStatus = record.programStatus ?? "active" // Optional; default value is set to active
                description = record.description ?? null // Optional; default value set to null
                cropProgramDbId = record.cropProgramDbId ?? "1"

                // Validation for cropProgramDbId
                if(!validator.isInt(cropProgramDbId)) {
                    let errMsg = `Invalid format for cropProgramDbId. cropProgramDbId must be integer`
                    res.send(new errors.BadRequestError(errMsg))
                }
                
                // Build query for  cropProgramDbId validatiion
                let validateCropProgramQuery = `
                    --- Check if cropProgramDbId is existing return 1
                    SELECT 
                        count(1)
                    FROM
                        tenant.crop_program cp
                    WHERE
                        cp.is_void = FALSE AND
                        cp.id = ${cropProgramDbId}
                `
                // Run corp program ID validation query
                let validateCropProgramCount = await sequelize.query(validateCropProgramQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                // If results are 0, display error message
                if(validateCropProgramCount == 0) {
                    let errMsg = `You have provided a non-valid cropProgramDbId. Please verify your input parameters.`
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }
                
                let tempArray = [
                  programCode, programName, programType, programStatus, description, cropProgramDbId, personDbId
                ]

                programValuesArray.push(tempArray)
            }

            // Insert query
            let programsQuery = format(`
                INSERT INTO
                    tenant.program (
                      program_code, program_name, program_type, program_status, description, crop_program_id, creator_id
                    )
                VALUES
                    %L
                RETURNING id`, programValuesArray
            )

            // Run query
            let programs = await sequelize.query(programsQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            }).catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
            })

            // Building result
            for(let program of programs[0]) {
                programDbId = program.id
                let array = {
                    programDbId: program.id,
                    recordCount: 1,
                    href: programUrlString + '/' + programDbId
                }
                resultArray.push(array)
            }

            await transaction.commit()
            res.send(200, {
                rows: resultArray,
                count: resultArray.length
            })

            return
        }
        //Log error
        catch (err) {
            logger.logMessage(__filename,'POST programs: '+ JSON.stringify(err), 'error')
            // Rollback transaction if any errors were encountered
            if (err) await transaction.rollback()
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}
