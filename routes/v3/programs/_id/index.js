/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let validator = require('validator')
let errorBuilder = require('../../../../helpers/error-builder')

module.exports = {

    // Endpoint for retrieving a specific program
    // GET /v3/programs/:id
    get: async function (req, res, next) {

        // Retrieve the program ID

        let programDbId = req.params.id

        if (!validator.isInt(programDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400051)
            res.send(new errors.BadRequestError(errMsg))
            return
        }
        else {
            let programsQuery = `
                SELECT
                    program.id AS "programDbId",
                    program.program_code AS "programCode",
                    program.program_name AS "programName",
                    program.description,
                    program.program_type AS "programType",
                    program.program_status AS "programStatus",
                    program.crop_program_id AS "cropProgramDbId",
                    program.creation_timestamp AS "creationTimestamp",
                    creator.person_name AS creator,
                    creator.id AS "creatorDbId",
                    program.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS modifier
                FROM 
                    tenant.program program
                LEFT JOIN 
                    tenant.person creator ON program.creator_id = creator.id
                LEFT JOIN 
                    tenant.person modifier ON program.modifier_id = modifier.id
                WHERE 
                    program.id = (:programDbId)
                    AND program.is_void = FALSE 
            `

            // Retrieve programs from the database   
            let programs = await sequelize.query(programsQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    programDbId: programDbId
                }
            })
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            // Return error if resource is not found
            if (await programs == undefined || await programs.length < 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404016)
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            res.send(200, {
                rows: programs
            })
            return
        }
    },
}
