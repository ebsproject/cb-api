/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Import dependencies
let { sequelize } = require('../../../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../../../helpers/error-builder')
let tokenHelper = require('../../../../helpers/auth/token')
let forwarded = require('forwarded-for')
let validator = require('validator')
let userValidator = require('../../../../helpers/person/validator')
let brokerHelper = require('../../../../helpers/broker/index.js')
let logger = require('../../../../helpers/logger')

const endpoint = 'list-members/:id'

module.exports = {
    // Endpoint for updating a list member
    // Implementation of PUT call for /v3/list-members/:id
    put: async function (req, res, next) {
        // Set defaults
        let orderNumber = null
        let origOrderNumber = null
        let listDbId = null

        let listMemberDbId = req.params.id
        if (!validator.isInt(listMemberDbId)) {
            let errMsg = 'You have provided an invalid format for the list member ID.'
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        // Check if the connection is secure or not (http or https)
        let isSecure = forwarded(req, req.headers).secure

        // Set URL for response
        let listMemberUrlString = (isSecure ? 'https' : 'http')
            + '://'
            + req.headers.host
            + '/v3/list-members'

        // Check if the request body does not exist
        if (!req.body) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400009)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let data = req.body

        try {
            let validateListMemberQuery = `
                (
                    SELECT
                        CASE WHEN
                            (count(1) = 1)
                            THEN 1
                            ELSE 0
                        END AS count
                    FROM
                        platform.list_member lm
                    WHERE
                        lm.is_void = FALSE AND
                        lm.id = '${listMemberDbId}'
                )
            `
            validateListMember = await sequelize.query(validateListMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (validateListMember[0]['count'] != 1) {
                let errMsg = 'The list member you have requested does not exist.'
                res.send(new errors.NotFoundError(errMsg))
                return
            }
        } catch (e) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            res.send(new errors.InternalError(errMsg))
            return
        }

        let transaction
        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        try {
            let setQuery = ``

            // Get necessary values
            let getListIdAndOrigOrderNumberQuery = `
                SELECT
                    lm.list_id AS "listDbId",
                    lm.order_number AS "orderNumber"
                FROM
                    platform.list_member lm
                WHERE
                    lm.is_void = FALSE AND
                    lm.id = '${listMemberDbId}'
            `
            let resultQuery = await sequelize.query(getListIdAndOrigOrderNumberQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            origOrderNumber = resultQuery[0]['orderNumber']
            listDbId = resultQuery[0]['listDbId']

            if (await origOrderNumber === undefined || await listDbId === undefined) {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            }

            if (data.orderNumber !== undefined) {
                orderNumber = data.orderNumber

                if (!validator.isInt(orderNumber)) {
                    let errMsg = 'You have provided an invalid format for order number.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `order_number = $$${orderNumber}$$`
            } else {
                orderNumber = origOrderNumber
            }

            // Check if there is an existing row in given order number and retrieve the timestamp
            let timestampExistsQuery = `
                SELECT
                    lm.modification_timestamp AS "modificationTimestamp"
                FROM
                    platform.list_member lm
                WHERE
                    lm.list_id = '${listDbId}' AND
                    lm.order_number = '${orderNumber}'
            `
            let timestamp = await sequelize.query(timestampExistsQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (timestamp.length > 0) {
                timestamp = timestamp[0]['modificationTimestamp']
            } else {
                timestamp = null
            }

            // Add optional fields
            if (data.displayValue !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `display_value = $$${data.displayValue}$$`
            }

            if (data.remarks !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `remarks = $$${data.remarks}$$`
            }

            if (data.searchInput !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `search_input = $$${data.searchInput}$$`
            }

            if (data.isActive !== undefined) {
                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `is_active = $$${data.isActive}$$`
            }

            if (data.listDbId !== undefined) {

                let newListDbId = data.listDbId
                let newListType = null
                // check if new list id has the same type as the current
                // get type of new list db id
                try {
                    let validateListDbIdQuery = `
                        (
                            SELECT
                                CASE WHEN
                                    (l.id IS NOT NULL)
                                    THEN 1
                                    ELSE 0
                                END AS count,
								l.type
                            FROM
                                platform.list l
                            WHERE
                                l.is_void = FALSE AND
								l.id = '${newListDbId}'
                        )
                    `
                    validateNewList = await sequelize.query(validateListDbIdQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
        
                    if (validateNewList[0]['count'] != 1) {
                        let errMsg = 'The list you have requested does not exist.'
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    newListType = validateNewList[0]['type']
                } catch (e) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                }

                let currentType = null
                // get type of current list db id
                try {
                    let validateListDbIdQuery = `
                        (
                            SELECT
                                CASE WHEN
                                    (l.id IS NOT NULL)
                                    THEN 1
                                    ELSE 0
                                END AS count,
								l.type
                            FROM
                                platform.list l
                            WHERE
                                l.is_void = FALSE AND
								l.id = '${listDbId}'
                        )
                    `
                    validateNewList = await sequelize.query(validateListDbIdQuery, {
                        type: sequelize.QueryTypes.SELECT
                    })
        
                    if (validateNewList[0]['count'] != 1) {
                        let errMsg = 'The list you have requested does not exist.'
                        res.send(new errors.NotFoundError(errMsg))
                        return
                    }

                    currentType = validateNewList[0]['type']
                } catch (e) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                }

                if (currentType != newListType) {
                    let errMsg = 'The new list does not match the type of the current list.'
                    res.send(new errors.NotFoundError(errMsg))
                    return
                }

                setQuery += (setQuery != '') ? ',' : ''
                setQuery += `list_id = ${newListDbId}`
            }

            let addNullsCond = ``
            // If all order_numbers need to be updated
            if (orderNumber != origOrderNumber) {
                if (timestamp === undefined || timestamp === null) {
                    if (orderNumber < origOrderNumber) {  // upward
                        addNullsCond = `DESC NULLS LAST`
                    } else {  // downward
                        addNullsCond = `ASC`
                    }
                } else {  // timestamp of previous list member in orderNumber exists
                    if (orderNumber < origOrderNumber) {  // upward
                        addNullsCond = `DESC`
                    } else {  // downward
                        addNullsCond = `ASC`
                    }
                }
            }

            // Update single list member
            let updateListMemberQuery = `
                UPDATE
                    platform.list_member
                SET
                    ${setQuery},
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    id = ${listMemberDbId}
            `

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateListMemberQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })

            let resultArray = {
                listMemberDbId: listMemberDbId,
                recordCount: 1,
                href: listMemberUrlString + '/' + listMemberDbId
            }
            
            // If orderNumber is updated, then execute POST /v3/lists/:id/reorder-list-members
            if (data.orderNumber !== undefined) {
                let url = (isSecure ? 'https' : 'http')
                    + '://'
                    + req.headers.host
                    + `/v3/lists/${listDbId}/reorder-list-members`
                let token = req.headers.authorization
                let requestBody = {"reorderCondition": addNullsCond}

                await brokerHelper.getData("POST", url, token, JSON.stringify(requestBody))
            }
            
            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Log error message when error encountered
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500003)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    },

    // Endpoint for deleting a list member
    // Implementation of DELETE call for /v3/list-members/:id
    delete: async function (req, res, next) {
        // Set defaults
        let listMemberDbId = req.params.id
        let listDbId = null

        if (!validator.isInt(listMemberDbId)) {
            let errMsg = await errorBuilder.getError(req.headers.host, 400089)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let userDbId = await tokenHelper.getUserId(req)

        if (userDbId == null) {
            let errMsg = await errorBuilder.getError(req.headers.host, 401002)
            res.send(new errors.BadRequestError(errMsg))
            return
        }

        let transaction

        try {
            let validateListMemberQuery = `
                SELECT
                    CASE WHEN
                    (count(1) = 1)
                    THEN 1
                    ELSE 0
                    END AS count
                FROM
                    platform.list_member lm
                WHERE
                    lm.is_void = false AND
                    lm.id = '${listMemberDbId}'
            `
            validateListMemberQuery = await sequelize.query(validateListMemberQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (validateListMemberQuery[0]['count'] != 1) {
                let errMsg = 'The list member you have requested does not exist.'
                res.send(new errors.NotFoundError(errMsg))
                return
            }

            let getListIdQuery = `
                SELECT
                    lm.list_id as "listDbId"
                FROM
                    platform.list_member lm
                WHERE
                    lm.is_void = FALSE AND
                    lm.id = '${listMemberDbId}'
            `
            list = await sequelize.query(getListIdQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            listDbId = list[0]['listDbId']

            let validateListQuery = `
                SELECT
                    CASE WHEN
                    (count(1) = 1)
                    THEN 1
                    ELSE 0
                    END AS count,
                    list.creator_id AS "creatorDbId"
                FROM
                    platform.list list
                WHERE
                    list.is_void = FALSE AND
                    list.id = '${listDbId}'
                GROUP BY list.creator_id
            `

            validateList = await sequelize.query(validateListQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            if (validateList[0]['count'] != 1) {
                let errMsg = await errorBuilder.getError(req.headers.host, 404031)
                res.send(new errors.NotFoundError(errMsg))
                return
            }
            // check if user is creator
            let hasAccess = false
            if (userValidator.isAdmin(userDbId) || validateList[0]['creatorDbId'] == userDbId) {
                hasAccess = true
            } else {
                // Get all programs that the user belongs to
                let getProgramsQuery =  `
                    SELECT 
                        program.id 
                    FROM 
                        tenant.program program,
                        tenant.program_team pt,
                        tenant.team_member tm
                    WHERE
                        tm.person_id = ${userDbId}
                        AND pt.team_id = tm.team_id
                        AND program.id = pt.program_id
                        AND tm.is_void = FALSE
                `
                let programIds = await sequelize.query(getProgramsQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                let teamQuery = ``
                let programFromQuery = ``
                let programCount = 1

                for (let programObj of programIds) {
                    if (teamQuery != ``) {
                        teamQuery += ` OR `
                    }

                    let programAlias = `programRole${programCount}`
                    teamQuery += `(list.access_data #> $$\{program,${programObj.id}}$$ IS NOT NULL AND "${programAlias}".person_role_code = 'DATA_PRODUCER')`

                    programFromQuery += `
                        LEFT JOIN
                            tenant.person_role "${programAlias}"
                        ON
                            "programRole1".id = (list.access_data->'program'->'${programObj.id}'::text->>'dataRoleId')::integer
                    `
                    programCount++
                }

                let userQuery = `(list.access_data #> $$\{user,${userDbId}}$$ IS NOT NULL AND pr.person_role_code='DATA_PRODUCER')`
                if (teamQuery != ``) {
                    userQuery += ` OR ` + teamQuery
                }

                let validateListCreatorQuery = `
                    SELECT
                        CASE WHEN
                        (count(1) > 0)
                        THEN 1
                        ELSE 0
                        END AS count
                    FROM
                        platform.list list
                    LEFT JOIN
                        tenant.person_role pr
                    ON
                        pr.id = (list.access_data->'user'->'${userDbId}'::text->>'dataRoleId')::integer
                    ${programFromQuery}
                    WHERE
                        list.is_void = FALSE AND
                        list.id = '${listDbId}' AND
                        (
                            list.creator_id = ${userDbId} OR 
                            (${userQuery})
                        )     
                `

                validateListCreator = await sequelize.query(validateListCreatorQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
                
                if (validateListCreator[0]['count'] == 1) {
                    hasAccess = true;
                }
            }

            if (!hasAccess) {
                let errMsg = 'The client is not the creator or does not have read and write access to the list the client is trying to retrieve.'
                res.send(new errors.ForbiddenError(errMsg))
                return
            }

            let deleteListMemberQuery = `
                UPDATE
                    platform.list_member lm
                SET
                    is_void = TRUE,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                WHERE
                    lm.id = '${listMemberDbId}'
            `

            deleteListMemberQuery = deleteListMemberQuery.trim()

            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(deleteListMemberQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'DELETE', err)
                    throw new Error(err)
                })
            })

            let resultArray = {
                listDbId: listDbId,
                listMemberDbId: listMemberDbId
            }

            res.send(200, {
                rows: resultArray
            })
            return
        } catch (err) {
            // Log error message when encountered error
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500002)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}