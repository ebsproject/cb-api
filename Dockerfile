# Copyright (C) 2024 Enterprise Breeding System
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# SPDX-License-Identifier: GPL-3.0-or-later

# Build image
FROM node:16.19.1-buster-slim AS builder

LABEL maintainer="Jack Elendil B. Lagare <j.lagare@irri.org> & Eugenia A. Tenorio <e.tenorio@irri.org>"

COPY . /cb-api/

WORKDIR /cb-api

COPY package*.json /cb-api/

RUN npm ci --only=production

# Production image
FROM node:16.19.1-buster-slim

ENV CBAPI_PORT=3000
ENV CBAPI_DB_HOST=cb-db
ENV CBAPI_DB_NAME=cb
ENV CBAPI_DB_PORT=5432
ENV CBAPI_DB_USERNAME=postgres
ENV CBAPI_DB_PASSWORD=br33d1ng4R35ult5
ENV CBAPI_AUTH_URL=https://sg.ebsproject.org:9443/oauth2/authorize
ENV CBAPI_CLIENT_ID=xxxx
ENV CBAPI_CLIENT_SECRET=xxxx
ENV CBAPI_REDIRECT_URI=https://cbapi.local/v3/auth/callback
ENV CBAPI_TOKEN_URL=https://sg.ebsproject.org:9443/oauth2/token
ENV CBAPI_JWKS_URI=https://sg.ebsproject.org:9443/oauth2/jwks
ENV CBAPI_JWT_ALGORITHM=RS256
ENV CB_RABBITMQ_HOST=rabbitmq
ENV CB_RABBITMQ_PORT=5672
ENV CB_RABBITMQ_USER=cbadmin
ENV CB_RABBITMQ_PASSWORD=cB@dm!n

WORKDIR /cb-api

COPY . .

COPY --from=builder /cb-api/node_modules node_modules/

EXPOSE 3000

CMD ["npm","run","start"]
