# CB API

The Core Breeding API provides a RESTful application programming interface allowing external applications to utilize the CB data model in conducting
trial management activities such as study creation, data collection, product management, data analysis, and so much more.

The third version of the API is implemented using Node.js.

## Getting Started

...

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Node.js](https://nodejs.org/en/) - The primary server-side language used in the implementation
* [Restify](http://restify.com/) - A Node.js web service framework
* [NPM](https://www.npmjs.com/) - Package management

## Contributing

Please read [CONTRIBUTING.md]https://link.com) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* List of developers to be populated in the future.
* **CB IRRI Development Team* 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
