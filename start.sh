#!/bin/bash

if [ "$NODE_ENV" = "development" ]; then
  node-dev index.js;
else
  node index.js;
fi
