/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')

module.exports = {

    /**
     * Retrieve the schemeDbId.
     * 
     * Check first if the data is existing already.
     * If not existing, a record will be created.
     * 
     * @param code string unique identifier of the scheme
     * @param name string name of the scheme
     * @param description string description of the scheme
     * @param schemeType integer type of the scheme (10 - project, 20 - phase, 30 - stage)
     * @param projectDbId integer ID of the project
     * @param userDbId integer ID of the user
     * 
     * @return schemeDbId integer
     */
    getSchemeDbId: async (code, name, description, schemeType, projectDbId, userDbId) => {

        // Default
        let schemeDbId = null

        // Get the scheme
        let schemeQuery = `
            SELECT
                scheme.id AS "schemeDbId",
                scheme.code,
                scheme.name,
                scheme.description
            FROM
                master.scheme scheme
            WHERE
                scheme.is_void = FALSE
                AND code ilike :code
        `

        // Retrieve scheme from the database   
        let scheme = await sequelize.query(schemeQuery,{
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                code: code
            }
        })
        .catch(err => {
            console.log(err)
            return null
        })

        if(scheme.length < 1){
            // Create new record
            let createSchemeQuery = `
                INSERT INTO
                    master.scheme
                    (code, name, description, scheme_type, project_id, creation_timestamp, creator_id, notes)
                VALUES
                    (:code, :name, :description, :schemeType, :projectDbId, NOW(), :userDbId, 'Created via CB API')
                RETURNING 
                        id
            `

            await sequelize.query(createSchemeQuery,{
                type: sequelize.QueryTypes.INSERT,
                replacements: {
                    code: code,
                    name: name,
                    displayName: name,
                    description: description,
                    schemeType: schemeType,
                    projectDbId: projectDbId,
                    userDbId: userDbId
                }
            })
            .then(function (schemeArray) {
                // Get the created ID
                schemeDbId =  schemeArray[0][0]['id']
            })
            .catch(err => {
                console.log(err)
                return null
            })

        } else {

            // Retrieve the record
            schemeDbId = scheme[0]['schemeDbId']

            // Check if the name and description are different
            schemeName = scheme[0]['name']
            schemeDescription = scheme[0]['description']

            if(schemeName != name || schemeDescription != description) {

                // Update description
                let updateSchemeDescriptionQuery = `
                    UPDATE
                        master.scheme scheme
                    SET 
                        name = :name,
                        description = :description,
                        modifier_id = :userDbId,
                        modification_timestamp = NOW()
                    WHERE
                        scheme.id = :schemeDbId
                `

                await sequelize.query(updateSchemeDescriptionQuery,{
                    type: sequelize.QueryTypes.UPDATE,
                    replacements: {
                        name: name,
                        description: description,
                        userDbId: userDbId,
                        schemeDbId: schemeDbId
                    }
                })
                .catch(err => {
                    console.log(err)
                    return null
                })
            }
        }

        return schemeDbId

    },

    /**
     * Create the relation between schemes.
     * 
     * @param rootSchemeDbId integer ID of the scheme root
     * @param parentSchemeDbId integer ID of the scheme parent
     * @param childSchemeDbId integer ID of the scheme child
     * @param orderNumber integer order of the relation
     * @param userDbId integer ID of the user
     * 
     * @return schemeRelationDbId integer
     */
    createSchemeRelationRecord: async (rootSchemeDbId, parentSchemeDbId, childSchemeDbId, orderNumber, userDbId) => {

        // Default
        let schemeRelationDbId = null

        // Create new record
        let createSchemeQuery = `
            INSERT INTO
                master.scheme_relation
                (root_scheme_id, parent_scheme_id, child_scheme_id, order_number, creation_timestamp, creator_id, notes)
            VALUES
                (:rootSchemeDbId, :parentSchemeDbId, :childSchemeDbId, :orderNumber, NOW(), :userDbId, 'Created via CB API')
            RETURNING 
                id
        `

        await sequelize.query(createSchemeQuery,{
            type: sequelize.QueryTypes.INSERT,
            replacements: {
                rootSchemeDbId: rootSchemeDbId,
                parentSchemeDbId: parentSchemeDbId,
                childSchemeDbId: childSchemeDbId,
                orderNumber: orderNumber,
                userDbId: userDbId
            }
        })
        .then(function (schemeRelation) {
            // Get the ID created
            schemeRelationDbId =  schemeRelation[0][0]["id"]
        })

        return schemeRelationDbId
    },

    /**
     * Retrieve the type of the scheme
     * 
     * @param schemeDbId integer ID of the scheme
     * 
     * return integer
     */
    getSchemeType: async (schemeDbId) => {
        let query = `
            SELECT
                scheme.scheme_type AS "schemeType"
            FROM
                master.scheme scheme
            WHERE 
                scheme.is_void = FALSE
                AND scheme.id = (:schemeDbId)
        `

        let schemeType = await sequelize.query(query,{
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                schemeDbId: schemeDbId
            }
        })

        return schemeType[0]['schemeType']
    }
}