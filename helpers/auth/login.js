/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')

module.exports = {
    /**
     *
     *  Checks if the client information is existing
     *  @param clientId ID of the client
     *  @param clientSecret client secret of the application
     *  @param redirectUri location to redirect the user after authorization
     *
     */
    isExisting: async (clientId, clientSecret, redirectUri) => {
        let countQuery = `
            SELECT
                count(1)
            FROM
                api.client
            WHERE
                client_id = (:clientId)
            AND
                client_secret = (:clientSecret)
            AND
                redirect_uri = (:redirectUri)
        `

        let count = await sequelize.query(countQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                clientId: clientId,
                clientSecret: clientSecret,
                redirectUri: redirectUri
            }
        })

        if (count[0].count < 1) {
            return false
        } else {
            return true
        }
    }
}
