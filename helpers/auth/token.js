/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let jwtDecode = require('jwt-decode')
let validator = require('validator')
let errors = require('restify-errors')

module.exports = {

    getUserId: async (req, res = null) => {
        try {
            let person
            let userId

            // Get userId from the request (if any)
            if (req.body !== undefined
                && req.body !== null
                && req.body.userId !== undefined
                && req.body.userId !== null
            ) {
                userId = req.body.userId
            }

            // If the userId is provided in the request body,
            // find the userId in the tenant.person table.
            if (userId !== undefined && res !== null) {

                // If the userId is not an integer, send back an error message
                if (!validator.isInt(userId)) {
                    let errMsg = 'Invalid format, userId must be an integer.'
                    res.send(new errors.BadRequestError(errMsg))
                    return
                }

                let personQuery = `
                    SELECT
                        person.id
                    FROM 
                        tenant.person person
                    WHERE
                        id = ${userId} AND
                        person.is_void = FALSE
                `

                person = await sequelize.query(personQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
            }
            // Else, parse the token to get the email,
            // and find the email in the tenant.person table
            else {
                let token = req.headers.authorization.split(' ')[1]

                let email = jwtDecode(token)['http://wso2.org/claims/emailaddress'] != undefined ?
                    jwtDecode(token)['http://wso2.org/claims/emailaddress'] : jwtDecode(token)['email']
                let personQuery = `
                    SELECT
                        person.id
                    FROM 
                        tenant.person person
                    WHERE
                        lower(person.email) = lower($$${email}$$) AND
                        person.is_void = FALSE
                `

                person = await sequelize.query(personQuery, {
                    type: sequelize.QueryTypes.SELECT
                })
            }

            // If the user is found in the tenant.person table,
            // Return the personDbId of the user.
            if (person != null && person != undefined && person.length > 0) {
                return Number(person[0].id)
            }
            else {
                return null
            }
        }
        catch (e) {
            return null
        }
    }
}