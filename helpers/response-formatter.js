/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const responseMessage = require('./response-messages');
const { PaginationFormat, ResponseFormat } = require('./response-format');
const errors = require('restify-errors');


module.exports = (req, res, body) => {

    let responseObj = new ResponseFormat();
    let paginationObj = new PaginationFormat();
    let totalCount = 0
    let totalPages = 1

    if (body instanceof Error) {
        responseObj.setStatus({
            message: body.body.message,
            messageType: body.body.code
        });
    } else {
        if (body.count != null) {
            // SELECT or <entities>-search detected
            totalCount = body.count
        } else if (Object.keys(body).includes('token') || Object.keys(body).includes('refreshToken')) {
            // Endpoint used is token generation-related (auth/login, auth/callback)
            totalCount = 1
        } else if (req.method.toLowerCase() === 'post' || req.method.toLowerCase() === 'get') {
            totalCount = body.rows.length
        } else if (req.method.toLowerCase() === 'put') {
            totalCount = body.rows.recordCount
        } else if (req.method.toLowerCase() === 'delete') {
            totalCount = (body.rows) ? 1 : 0
        }

        //handler for findAndCountAll() sequelize function
        if(body.rows){
            totalPages = (totalCount > 1)? Math.ceil(totalCount/req.paginate.per_page) : 1;
            body = body.rows;
        }

        responseObj.setData(body);
        paginationObj = {
            pageSize : req.paginate.per_page,
            totalCount : Number(totalCount),
            currentPage : req.paginate.page,
            totalPages : totalPages ?? 1,
        };

        responseObj.setPagination(paginationObj);

        responseObj.setStatus({
            message: responseMessage(res.statusCode),
            messageType: 'INFO'
        });
    
    }

    return JSON.stringify(responseObj);
}