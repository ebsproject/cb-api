/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')

/**
 * Retrieve the variable given the abbrev
 * 
 * @param {string} abbrev variable abbreviation
 * 
 * @returns {object} containing the variable information
 */
async function getVariableByAbbrev(abbrev) {
    // Generate the query
    let variableQuery = `
    SELECT
        variable.id AS "variableDbId",
        variable.abbrev AS "abbrev",
        variable.data_type AS "dataType"
    FROM
        master.variable variable
    WHERE
        variable.abbrev = '${abbrev}'
        AND variable.is_void = false
    `

    // Retrieve the variable
    let variable = await sequelize.query(variableQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    return variable[0]
}
/**
 * Get information about schema of tables
 * 
 * @param schemas list of schema
 * @param columnName column name of the entity
 * @return array records of information schema
 * 
 */
async function getInformationSchema(schemas, columnName) {
    let schemaStr = "$$"+schemas.join("$$,$$")+"$$"
    let columnList = []
    let columnInfoSql = `
            SELECT 
                table_name, 
                column_name, 
                data_type, 
                column_default, 
                is_nullable
            FROM 
                information_schema.columns
            WHERE 
                table_schema in (${schemaStr})
                AND column_name = '${columnName}'`

    columnList = await sequelize.query(columnInfoSql, {
        type: sequelize.QueryTypes.SELECT
    })
    return columnList
}


/**
 * Get total count limit from config
 * 
 * @return Integer Total count limit
 * 
 */
async function getTotalCountLimit() {
    let configQuery = `
        SELECT
            config_value
        FROM
            platform.config
        WHERE
            abbrev = 'CB_API_TOTAL_COUNT_LIMIT'

        `

    let config = await sequelize.query(configQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    // if config is not found, return default 500000
    if (await config == undefined || config.length < 1) {
        return ` limit ` + 5000000
    }

    return ` limit ` + config[0]['config_value']
}

module.exports = {

    /**
     * Generate the final SQL query
     * 
     * @param sqlQuery string initial query
     * @param conditionString string condition 
     * @param orderString string order
     * @param distinctString string distinct on
     * 
     * @return finalSqlQuery string generated query
     */
    getFinalSqlQuery: async (sqlQuery, conditionString = '', orderString = '', distinctString = '') => {
        // Generate the query
        let finalSqlQuery = `
        select 
        `
            + distinctString +
            `
            *
        from
            (
        `
            + sqlQuery +
            `
            ) as tbl
        `
            + conditionString +
            ` `
            + orderString +
            ` limit (:limit) offset (:offset)`

        return finalSqlQuery
    },

    getOccurrenceFinalSqlQuery: async (sqlQuery, conditionString = '', orderString = '', distinctString = '') => {
        // Generate the query
        let finalSqlQuery = `
        SELECT
            *
        FROM
            (
            SELECT
                ${distinctString} *
            FROM
                (
                ${sqlQuery}
                )
            AS tbl
            ${conditionString}
            )
        AS finalTbl
        ${orderString}
        limit (:limit) offset (:offset)
        `

        return finalSqlQuery
    },
    getOccurrenceFinalSqlQueryWoLimit: async (sqlQuery, conditionString = '', orderString = '', distinctString = '') => {
        // Generate the query
        let finalSqlQuery = `
        SELECT
            *
        FROM
            (
            SELECT
                ${distinctString} *
            FROM
                (
                ${sqlQuery}
                )
            AS tbl
            ${conditionString}
            )
        AS finalTbl
        ${orderString}
        `

        return finalSqlQuery
    },

    /**
     * Generate the final SQL query
     * 
     * @param sqlQuery string initial query
     * @param conditionString string condition 
     * @param orderString string order
     * @param distinctString string distinct on
     * 
     * @return finalSqlQuery string generated query
     */
    getFinalQuery: async (sqlQuery, conditionString = '', orderString = '', distinctString = '') => {
        // Generate the query
        let finalSqlQuery = `
        select
            *
        from
            (
            select 
            `
            + distinctString +
            `
                *
            from
                (
            `
            + sqlQuery +
            `
                ) as tbl
            `
            + conditionString +
            `) as tbl ` +
            ` `
            + orderString +
            ` limit (:limit) offset (:offset)`

        return finalSqlQuery
    },

    /**
     * Generate the count for the final SQL query
     * 
     * @param sqlQuery string initial query
     * @param conditionString string condition 
     * @param orderString string order
     * @param distinctString string distinct on
     * 
     * @return countFinalSqlQuery string generated query
     */
    getCountFinalSqlQuery: async (sqlQuery, conditionString = '', orderString = '', distinctString = '') => {
        // Generate the query
        let countFinalSqlQuery = `
        select 
            count(1)
        from
            (
            select
        `
            + distinctString +
            `
                *
            from
                (
        `
            + sqlQuery +
            `
                ) as tbl
        `
            + conditionString +
            `
            ) as tbl2
        `

        return countFinalSqlQuery
    },

    /**
     * Generate the cross tab SQL query
     * 
     * @param source string initial query
     * @param categories string variable IDs
     * @param columnString string name of the columns and their data type
     * 
     * @return string generated query
     */
    getCrossTabSqlQuery: async (source, categories, columnString) => {
        // Generate the cross tab query
        let sql = `
        select 
            *
        from crosstab(
            '
            ` + source + `
            ',
            '
            ` + categories + `
            '
        ) as ` + columnString

        return sql
    },

    /**
     * Generate the order string
     * 
     * @param sort string query given by the user
     * 
     * @return orderQuery string generated query for sorting
     * 
     */
    getOrderString: async (sort) => {

        let orderQuery = 'ORDER BY '
        sort = sort.toString();

        let sortArray = sort.split('|').map(item => item.trim())

        let query = ''

        for (sortString of sortArray) {
            let [column, order] = sortString.split(':')

            // Check if column is empty
            if(column!=''){
                // Check if order is given
                // If not, set the default
                if (order === undefined) {
                    order = 'ASC'
                }

                // Check if the order given is valid
                if (order.toUpperCase() !== 'ASC' && order.toUpperCase() !== 'DESC') {
                    return 'You have provided invalid parameters for sort order.'
                }

                if (query != '') {
                    query = query + ', '
                }

                // Generate the sub query
                query = query + `"` + column + `" ` + order
            }
        }

        // Concatenate the order query with the sub query
        orderQuery = orderQuery + query

        return orderQuery
    },

    /**
     * Generate the order string when traits are included in the sort
     * 
     * @param {string} sort query given by the user
     * @param {array} traits traits included in the retrieval
     * @param {string} dataValue 'dataValue' for committed data, 'value' for uncommitted data
     * 
     * @returns {string} generated query
     */
       getOrderStringTrait: async (sort, traits, dataValue = 'dataValue') => {

        let traitQuery = 'ORDER BY '
        sort = sort.toString();

        let sortArray = sort.split('|').map(item => item.trim())

        let query = ''
        
        let numericTypes = [ 'integer', 'float' ]

        for (sortString of sortArray) {
            let [column, order] = sortString.split(':')

            // Check if order is given
            // If not, set the default
            if (order === undefined) {
                order = 'ASC'
            }

            // Check if the order given is valid
            if (order.toUpperCase() !== 'ASC' && order.toUpperCase() !== 'DESC') {
                return 'You have provided invalid parameters for sort order.'
            }

            // Add comma
            if (query != '') {
                query = query + ', '
            }
            
            // If the column is a trait, check data type of trait
            if (traits.includes(column)) {
                // Check data type of trait
                let variable = await getVariableByAbbrev(column)
                // If numeric, type cast column to float8
                if (numericTypes.includes(variable.dataType)) {
                    query = query + ` float8("${column}"->>'` + dataValue + `') ${order}`
                }
                else query = query + ` "${column}"->>'` + dataValue + `' ${order}`
            }
            else {
                // Generate the sub query
                query = query + `"` + column + `" ` + order 
            }

            
        }

        // Concatenate the order query with the sub query
        traitQuery = traitQuery + query

        return traitQuery
    },

    /**
     * Generate the condition string for filtering
     * 
     * @param parameters array parameters given by the user
     * @param excludedParametersArray array exclude parameters in the array in filtering
     * 
     * @return searchQuery string generated query for filtering
     */
    getFilterString: (parameters, excludedParametersArray = []) => {
        let searchQuery = ''
        // Build query
        for (column in parameters) {
            let filter = parameters[column]
            let valueList = ''
            let columnString = ''
            let count = 0

            if (filter == '' || filter == null) {
                return 'You have provided invalid values for filter.'
            } else {
                // Check if the column is not excluded in the filter
                if (!excludedParametersArray.includes(column)) {
                    if (column.includes('Timestamp')) {
                        // Set filter for columns with timestamp
                        // Given must be in 'yyyy-mm-dd' format
                        columnString = `to_char("` + column + `",'yyyy-mm-dd')`
                    } else if (column.toUpperCase() == column) {
                        // Filter the variables
                        // All the variable abbrev are in Uppercase
                        columnString = `lower("` + column + `"->>'variableValue')`
                    } else if (column == 'fieldLocation') {
                        // Set filter for fieldLocation
                        columnString = `lower("` + column + `"->>'display_name')`
                    } else if (column == 'parentage') {
                        // Set filter for parentage
                        columnString = `RTRIM(lower("` + column + `"))`
                    } else {
                        // default
                        columnString = `lower("` + column + `"::text)`
                    }

                    // Parse the value
                    for (value of filter.split('|')) {
                        valueList += "$$" + (value.trim()).toLowerCase() + "$$,"
                        count++
                    }

                    // Set the condition
                    let condition = ''
                    if (count > 1) {
                        // For multiple values
                        valueList = valueList.replace(/(^,)|(,$)/g, "")

                        condition = columnString
                            + ` IN (`
                            + valueList
                            + `)`
                    } else {
                        // For single values
                        if (columnString.includes('Id')) {
                            // Search for exact values if the column name includes 'Id'
                            condition = columnString
                                + ` = '`
                                + value
                                + `'`
                        }else if (columnString.includes('abbrev')) {
                            // Search for exact values if the column name includes 'abbrev'
                            condition = columnString
                                + ` = '`
                                + value.toLowerCase()
                                + `'`
                        } else {
                            // Search for ilike values if the column name does not include 'Id'
                            condition = columnString
                                + ` ILIKE '`
                                + value
                                + `%' `
                        }
                    }

                    // Set the condition based on the parameters
                    if (searchQuery == '') {
                        searchQuery = searchQuery +
                            ` WHERE `
                            + condition
                    } else {
                        searchQuery = searchQuery +
                            ` AND `
                            + condition
                    }
                }
            }
        }
        return searchQuery
    },

    getFilter: (parameters, excludedParametersArray = [], delimiter, replacements = false, replacementValues = null) => {
        let paramsArray = []
        let mainQuery = ''

        if (parameters == null || typeof parameters !== 'object') {
            return mainQuery
        }

        // check if multiple groups of conditions are used
        if ('conditions' in parameters) {
            paramsArray = parameters['conditions']
        }
        else {
            paramsArray.push(parameters)
        }

        // assign default delimiter
        delimiter = (delimiter == undefined) ? '|' : delimiter

        // check if has replacement values
        let hasReplacementValues = replacements && replacementValues != null

        // loop through condition groups
        for (parameters of paramsArray) {
            let subQuery = ''
            let andObj = { glue: 'AND' }
            let andParams = {}
            let orObj = { glue: 'OR' }
            let orParams = {}
            let orGroups = {}
            let conditionArrays = []

            // separate conditions into AND/OR groups
            for (column in parameters) {
                let filter = parameters[column]
                if (typeof filter === 'string') {
                    let regexMatch = filter.match(/^\s+\[/) || filter.match(/^(\[OR|\s*OR\]|\[\s*OR\s+\]|\[\s+OR\s*\])\s/)
                    if (regexMatch) {
                        return 'You have provided invalid values for filter.'
                    }
                    else {
                        if (filter.includes('[OR] ')) {
                            filter = filter.replace('[OR] ', '')

                            // check if grouping term is invalid (if any)
                            regexMatch = filter.match(/^\s+\[/) || filter.match(/\]\s\s+/) || filter.match(/^\[(G\s+|\s+G|G[a-zA-Z]+|G)\]/) || filter.match(/^(\[G\d*\s|\s*G\d*\]|\[\s*G\d*\s+\]|\[\s+G\d*\s*\])/)
                            if (regexMatch) {
                                return 'You have provided invalid values for filter.'
                            }

                            regexMatch = filter.match(/^\[G\d*\]/)
                            if (regexMatch == null) { // no grouping term
                                orParams[column] = filter
                            }
                            else {    // with valid grouping term
                                let group = regexMatch[0]
                                filter = filter.replace(group + ' ', '')

                                if (group in orGroups) {
                                    orGroups[group]['params'][column] = filter

                                } else {
                                    tempOrParams = {}
                                    tempOrParams[column] = filter
                                    tempOrObj = { glue: 'OR', params: tempOrParams }
                                    orGroups[group] = tempOrObj
                                }
                            }
                        }
                        else {
                            andParams[column] = filter
                        }
                    }
                }
            }

            andObj['params'] = andParams
            orObj['params'] = orParams
            conditionArrays.push(andObj)
            conditionArrays.push(orObj)

            for (column in orGroups) {
                conditionArrays.push(orGroups[column])
            }

            for (index in conditionArrays) {
                let query = ''
                let condArray = conditionArrays[index]
                let glue = condArray['glue']
                let parameters = condArray['params']

                // loop through parameters
                for (column in parameters) {
                    // Empty string to build query
                    let filter = parameters[column] // "abbrev": "RICE"
                    let neqArray = []
                    let equalsArray = []
                    let notNullArray = []
                    let nullArray = []
                    let nilArray = []
                    let rangeArray = []
                    let lessThanArray = []
                    let lessThanEqualArray = []
                    let greaterThanArray = []
                    let greaterThanEqualArray = []
                    let emptyArray = []
                    let beforeArray = []
                    let afterArray = []
                    let inCondition = null
                    let condition = '('
                    // Check if filter value is empty
                    if (filter == '' || filter == null) {
                        return 'You have provided invalid values for filter.'
                    } else {
                        if (!excludedParametersArray.includes(column)) {
                            // Split filter values
                            try {
                                let values = filter.split(delimiter)

                                for (value of values) {

                                    // Separate not equal and equal values
                                    if (value.includes('not equals')) {
                                        value = value.replace('not equals ', '')
                                        neqArray.push(value)
                                    } else if (value == 'empty') {
                                        emptyArray.push('')
                                    } else if (value.includes('equals')) {
                                        value = value.replace('equals ', '')
                                        equalsArray.push(value)
                                    } else if (value.includes('not null')) {
                                        notNullArray.push(value)
                                    } else if (value.includes('null')) {
                                        nullArray.push(value)
                                    } else if (value.includes('not ilike')) {
                                        value = value.replace('not ilike ', '')
                                        nilArray.push(value)
                                    } else if (value.includes('in:')) {
                                        value = value.replace('in: ', '')
                                        inCondition = value
                                    } else if (value.includes('range:')) {
                                        value = value.replace('range: ', '')
                                        rangeArray.push(value)
                                    } else if (value.includes('less than') && !value.includes('less than or equal to')) {
                                        value = value.replace('less than ', '')
                                        lessThanArray.push(value)
                                    } else if (value.includes('less than or equal to')) {
                                        value = value.replace('less than or equal to ', '')
                                        lessThanEqualArray.push(value)
                                    } else if (value.includes('greater than') && !value.includes('greater than or equal to')) {
                                        value = value.replace('greater than ', '')
                                        greaterThanArray.push(value)
                                    } else if (value.includes('greater than or equal to')) {
                                        value = value.replace('greater than or equal to ', '')
                                        greaterThanEqualArray.push(value)
                                    } else if (value.includes('before')) {
                                        value = value.replace('before ', '')
                                        beforeArray.push(value)
                                    } else if (value.includes('after')) {
                                        value = value.replace('after ', '')
                                        afterArray.push(value)
                                    } else {
                                        // Set the value of condition
                                        if (isNaN(value) && !Number.isInteger(value)) {
                                            value = value.toString()
                                        }
                                        let tempCondition
                                        if (column.includes('Timestamp')) {
                                            let tempColumnString = `to_char("` + column + `", 'yyyy-mm-dd')`
                                            column = tempColumnString
                                            tempCondition = `(${tempColumnString}) ILIKE '${value}'`
                                        }
                                        else if (column.includes('Document')) {
                                            // query using document column
                                            tempCondition = `"` + column + `" @@ to_tsquery($$ ` + value + ` $$)`
                                        }
                                        else {
                                            tempCondition = `("${column}"::text ILIKE '${value}') `
                                        }
                                        if (condition != '(') tempCondition = `OR ${tempCondition}`
                                        // Update base condition
                                        condition += tempCondition
                                    }
                                }
                                // Traverse the not equals
                                if (neqArray.length > 0) {
                                    if (condition != '(') condition += ') AND ('
                                    let count = 0
                                    let filterString = ''
                                    for (value of neqArray) {
                                        filterString += "$$"+value.trim()+"$$,"
                                        count++
                                    }
                                    columnName = ` ("${column}"::text `
                                    if (column.includes('Id')) {
                                        columnName = ` ("${column}" `
                                    }
                                    if (column.includes('Timestamp')) {
                                        columnTemp = `to_char("` + column + `",'yyyy-mm-dd')`
                                        columnName = ` (${columnTemp}) `
                                    }
                                    if (count > 1) {
                                        
                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                                        condition = condition +columnName
                                              + `NOT IN` +` (`
                                              + filterString
                                              + `))`
                                    } else {
                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                                        condition = condition + columnName
                                            + `<>`
                                            + "$$"+value.trim()+"$$"+` )`
                                    }
                                }

                                if (equalsArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    let count = 0
                                    let filterString = ''
                                    for (value of equalsArray) {
                                        filterString += "$$"+value.trim()+"$$,"
                                        count++
                                    }
                                    columnName = ` ("${column}"::text `
                                    if (column.includes('Id')) {
                                        columnName = ` ("${column}" `
                                    }
                                    if (column.includes('Timestamp')) {
                                        columnTemp = `to_char("` + column + `",'yyyy-mm-dd')`
                                        columnName = ` (${columnTemp}) `
                                    }
                                    if (count > 1) {
                                       
                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                  
                                        condition = condition + columnName
                                              + `IN` +` (`
                                              + filterString 
                                              + `))`
                                    } else {
                                        let tempColumnString
                                        let valueString = "$$"+value.trim()+"$$"

                                        // check if has replacement values
                                        if(hasReplacementValues && replacementValues[column] != undefined){
                                            valueString = ` ${replacementValues[column]}`
                                        }

                                        condition = condition + columnName
                                            + `=`
                                            + valueString +` )`
                                    }
                                    
                                }

                                if (notNullArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    for (value of notNullArray) {
                                        let tempCondition = `("${column}"::text IS NOT NULL)`
                                        if (notNullArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                        condition += tempCondition
                                    }
                                }

                                if (emptyArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    for (value of emptyArray) {
                                        let tempCondition = `("${column}"::text = '')`
                                        if (emptyArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                        condition += tempCondition
                                    }
                                }

                                if (nullArray.length > 0) {
                                    if (values.length > 1) condition += ' OR '
                                    else if (condition != '(') condition += ') OR ('
                                    for (value of nullArray) {
                                        let tempCondition =
                                            `("${column}"::text IS NULL OR "${column}"::text = '')`
                                        if (nullArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                        condition += tempCondition
                                    }
                                }
                                if (nilArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    if (column.includes('Id')) {
                                        let filterString = ''

                                        for (value of nilArray) {
                                            filterString += '$$' + (value.trim()).toLowerCase() + '$$,'
                                        }

                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                                        let tempCondition = `("${column}" IN (${filterString}))`
                                        condition += tempCondition
                                    } else {
                                        for (value of nilArray) {
                                            let tempCondition
                                            if (column.includes('Timestamp')) {
                                                let tempColumnString = `to_char("` + column + `", 'yyyy-mm-dd')`
                                                column = tempColumnString
                                                tempCondition = `(${tempColumnString}) NOT ILIKE '${value}'`
                                            } else {
                                                tempCondition = `("${column}"::text NOT ILIKE '${value}')`
                                            }

                                            if (nilArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                            condition += tempCondition
                                        }
                                    }
                                }
                                // Traverse range object
                                if (rangeArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    for (value of rangeArray) {
                                        let tempCondition
                                        // Split the range into min and max values
                                        let range = value.split('-')
                                        let min = range[0]
                                        let max = range[1]

                                        if (column.includes('Timestamp')) {
                                            tempColumnString = `to_char("${column}", 'yyyy-mm-dd')`
                                            column = tempColumnString
                                            tempCondition = `(${tempColumnString}) >= $$${min}$$ AND <= $$${max}$$`
                                        } else {
                                            tempCondition = `("${column}"::integer BETWEEN ${min} AND ${max})`
                                        }

                                        if (rangeArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`

                                        condition += tempCondition
                                    }
                                }

                                if (lessThanArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer < '${value}')`

                                    if (lessThanArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (lessThanEqualArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer <= '${value}')`

                                    if (lessThanEqualArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (greaterThanArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer > '${value}')`

                                    if (greaterThanArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (greaterThanEqualArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer >= '${value}')`

                                    if (greaterThanEqualArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (beforeArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    if (column.includes('Timestamp')) {
                                        tempColumnString = `to_char("${column}", 'YYYY-MM-DD HH24'||CHR(58)||'MI'||CHR(58)||'SS')`
                                        column = tempColumnString
                                        tempCondition = `(${tempColumnString}) < $$${value}$$`
                                    } else {
                                        tempCondition = `("${column}"::integer < '${value}')`
                                    }

                                    if (beforeArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (afterArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    if (column.includes('Timestamp')) {
                                        tempColumnString = `to_char("${column}", 'YYYY-MM-DD HH24'||CHR(58)||'MI'||CHR(58)||'SS')`
                                        column = tempColumnString
                                        tempCondition = `(${tempColumnString}) > $$${value}$$`
                                    } else {
                                        tempCondition = `("${column}"::integer > '${value}')`
                                    }

                                    if (afterArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (inCondition !== null) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}") IN ${inCondition}`
                                    condition += tempCondition
                                }

                                condition += ')'

                                if (query == '') {
                                    query += condition
                                } else {
                                    query += ` ` + glue + ` ` + condition
                                }
                            } catch (err) {
                                return 'You have provided invalid values for filter.'
                            }
                        }
                    }
                }

                // build sub query
                if (query == '') continue

                if (subQuery == '') {
                    subQuery += ` (` + query + `)`
                }
                else {
                    subQuery += ` AND (` + query + `)`
                }
            }

            // build main query
            if (subQuery == '') continue

            if (mainQuery == '') {
                mainQuery += ` WHERE (` + subQuery + `)`
            }
            else {
                mainQuery += ` OR (` + subQuery + `)`
            }
        }

        return mainQuery
    },

    getFilterWithInfo: (parameters, excludedParametersArray = [], responseColumns = null,delimiter, replacements = false, replacementValues = null) => {
        let paramsArray = []
        let mainQuery = ''
        let mainQueryInfo = ''
        let tableAliases = []
        let columnAliases = []

        if (parameters == null || typeof parameters !== 'object') {
            return {
                mainQuery: mainQuery,
                mainQueryInfo: mainQueryInfo
            }
        }

        // check if multiple groups of conditions are used
        if ('conditions' in parameters) {
            paramsArray = parameters['conditions']
        }
        else {
            paramsArray.push(parameters)
        }

        // assign default delimiter
        delimiter = (delimiter == undefined) ? '|' : delimiter

        // check if has replacement values
        let hasReplacementValues = replacements && replacementValues != null

        //parse responseColumns
        let responseColArr = responseColumns.split(",").map(e => e.trim())

        // loop through condition groups
        for (parameters of paramsArray) {
            let subQuery = ''
            let subQueryInfo = ''
            let andObj = { glue: 'AND' }
            let andParams = {}
            let orObj = { glue: 'OR' }
            let orParams = {}
            let orGroups = {}
            let conditionArrays = []

            // separate conditions into AND/OR groups
            for (column in parameters) {
                let filter = parameters[column]
                if (typeof filter === 'string') {
                    let regexMatch = filter.match(/^\s+\[/) || filter.match(/^(\[OR|\s*OR\]|\[\s*OR\s+\]|\[\s+OR\s*\])\s/)
                    if (regexMatch) {
                        return 'You have provided invalid values for filter.'
                    }
                    else {
                        if (filter.includes('[OR] ')) {
                            filter = filter.replace('[OR] ', '')

                            // check if grouping term is invalid (if any)
                            regexMatch = filter.match(/^\s+\[/) || filter.match(/\]\s\s+/) || filter.match(/^\[(G\s+|\s+G|G[a-zA-Z]+|G)\]/) || filter.match(/^(\[G\d*\s|\s*G\d*\]|\[\s*G\d*\s+\]|\[\s+G\d*\s*\])/)
                            if (regexMatch) {
                                return 'You have provided invalid values for filter.'
                            }

                            regexMatch = filter.match(/^\[G\d*\]/)
                            if (regexMatch == null) { // no grouping term
                                orParams[column] = filter
                            }
                            else {    // with valid grouping term
                                let group = regexMatch[0]
                                filter = filter.replace(group + ' ', '')

                                if (group in orGroups) {
                                    orGroups[group]['params'][column] = filter

                                } else {
                                    tempOrParams = {}
                                    tempOrParams[column] = filter
                                    tempOrObj = { glue: 'OR', params: tempOrParams }
                                    orGroups[group] = tempOrObj
                                }
                            }
                        }
                        else {
                            andParams[column] = filter
                        }
                    }
                }
            }

            andObj['params'] = andParams
            orObj['params'] = orParams
            conditionArrays.push(andObj)
            conditionArrays.push(orObj)

            for (column in orGroups) {
                conditionArrays.push(orGroups[column])
            }

            for (index in conditionArrays) {
                let query = ''
                let queryInfo = ''
                let condArray = conditionArrays[index]
                let glue = condArray['glue']
                let parameters = condArray['params']

                // loop through parameters
                for (column in parameters) {
                    // Empty string to build query
                    let filter = parameters[column] // "abbrev": "RICE"
                    let neqArray = []
                    let equalsArray = []
                    let notNullArray = []
                    let nullArray = []
                    let nilArray = []
                    let rangeArray = []
                    let lessThanArray = []
                    let lessThanEqualArray = []
                    let greaterThanArray = []
                    let greaterThanEqualArray = []
                    let beforeArray = []
                    let afterArray = []
                    let emptyArray = []
                    let inCondition = null
                    let condition = '('

                    let tempColumn = column
                    if(column.includes('DbId')){
                        tempColumn = tempColumn.replace("DbId", "Id")
                    }
                    let transformedColumn =  tempColumn.replace(/[A-Z]/g, (letter, index) => { return index == 0 ? letter.toLowerCase() : '_'+ letter.toLowerCase();});
                    let alias = ''
                    
                    for(let columnMap of responseColArr){
                        if(columnMap.includes(column)){
                            let aliasArr = columnMap.split(".")
                            transformedColumn = aliasArr[0] +"."+transformedColumn
                            tableAliases.push(aliasArr[0])

                            let tempAlias = columnMap.split(",")
                            columnAliases.push(tempAlias[0])
                            break;
                        }
                    }

                    //parse responseColumn string to get table alias

                    let conditionInfo = '('

                    // Check if filter value is empty
                    if (filter == '' || filter == null) {
                        return 'You have provided invalid values for filter.'
                    } else {
                        if (!excludedParametersArray.includes(column)) {
                            // Split filter values
                            try {
                                let values = filter.split(delimiter)

                                for (value of values) {

                                    // Separate not equal and equal values
                                    if (value.includes('not equals')) {
                                        value = value.replace('not equals ', '')
                                        neqArray.push(value)
                                    } else if (value == 'empty') {
                                        emptyArray.push('')
                                    } else if (value.includes('equals')) {
                                        value = value.replace('equals ', '')
                                        equalsArray.push(value)
                                    } else if (value.includes('not null')) {
                                        notNullArray.push(value)
                                    } else if (value.includes('null')) {
                                        nullArray.push(value)
                                    } else if (value.includes('not ilike')) {
                                        value = value.replace('not ilike ', '')
                                        nilArray.push(value)
                                    } else if (value.includes('in:')) {
                                        value = value.replace('in: ', '')
                                        inCondition = value
                                    } else if (value.includes('range:')) {
                                        value = value.replace('range: ', '')
                                        rangeArray.push(value)
                                    } else if (value.includes('less than') && !value.includes('less than or equal to')) {
                                        value = value.replace('less than ', '')
                                        lessThanArray.push(value)
                                    } else if (value.includes('less than or equal to')) {
                                        value = value.replace('less than or equal to ', '')
                                        lessThanEqualArray.push(value)
                                    } else if (value.includes('greater than') && !value.includes('greater than or equal to')) {
                                        value = value.replace('greater than ', '')
                                        greaterThanArray.push(value)
                                    } else if (value.includes('greater than or equal to')) {
                                        value = value.replace('greater than or equal to ', '')
                                        greaterThanEqualArray.push(value)
                                    } else if (value.includes('before')) {
                                        value = value.replace('before ', '')
                                        beforeArray.push(value)
                                    } else if (value.includes('after')) {
                                        value = value.replace('after ', '')
                                        afterArray.push(value)
                                    } else {
                                        // Set the value of condition
                                        if (isNaN(value) && !Number.isInteger(value)) {
                                            value = value.toString()
                                        }
                                        let tempCondition, tempConditionInfo
                                        if (column.includes('Timestamp')) {
                                            let tempColumnString = `to_char("` + column + `", 'yyyy-mm-dd')`
                                            column = tempColumnString
                                            tempCondition = `(${tempColumnString}) ILIKE '${value}'`

                                            let tempColumnStr = `to_char("` + transformedColumn + `", 'yyyy-mm-dd')`
                                            transformedColumn = tempColumnStr
                                            tempConditionInfo = `(${tempColumnStr}) ILIKE '${value}'`
                                        }
                                        else if (column.includes('Document')) {
                                            // query using document column
                                            tempCondition = `"` + column + `" @@ to_tsquery($$ ` + value + ` $$)`

                                            tempConditionInfo = `"` + transformedColumn + `" @@ to_tsquery($$ ` + value + ` $$)`
                                        }
                                        else {
                                            tempCondition = `("${column}"::text ILIKE '${value}') `
                                            tempConditionInfo = `(cast(${transformedColumn} as text) ILIKE '${value}') `
                                        }
                                        if (condition != '(') tempCondition = `OR ${tempCondition}`
                                        // Update base condition
                                        condition += tempCondition

                                        if (conditionInfo != '(') tempConditionInfo = `OR ${tempConditionInfo}`
                                        // Update base conditionInfo
                                        conditionInfo += tempConditionInfo
                                    }
                                }
                                // Traverse the not equals
                                if (neqArray.length > 0) {
                                    if (condition != '(') condition += ') AND ('
                                    let count = 0
                                    let filterString = ''
                                    for (value of neqArray) {
                                        filterString += "$$"+value.trim()+"$$,"
                                        count++
                                    }
                                    columnName = ` ("${column}"::text `
                                    columnNameInfo = ` (cast(${transformedColumn} as text) `
                                    if (column.includes('Id')) {
                                        columnName = ` ("${column}" `
                                        columnNameInfo = ` (cast(${transformedColumn} as text) `
                                    }
                                    if (column.includes('Timestamp')) {
                                        columnTemp = `to_char("` + column + `",'yyyy-mm-dd')`
                                        columnName = ` (${columnTemp}) `

                                        columnTempInfo = `to_char("` + transformedColumn + `",'yyyy-mm-dd')`
                                        columnNameInfo = ` (${columnTempInfo}) `
                                    }
                                    if (count > 1) {
                                        
                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                                        condition = condition +columnName
                                              + `NOT IN` +` (`
                                              + filterString
                                              + `))`

                                        conditionInfo = conditionInfo +columnNameInfo
                                              + `NOT IN` +` (`
                                              + filterString
                                              + `))`
                                    } else {
                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                                        condition = condition + columnName
                                            + `<>`
                                            + "$$"+value.trim()+"$$"+` )`

                                        conditionInfo = conditionInfo +columnNameInfo
                                              + `NOT IN` +` (`
                                              + filterString
                                              + `))`
                                    }
                                }

                                if (equalsArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    let count = 0
                                    let filterString = ''
                                    for (value of equalsArray) {
                                        filterString += "$$"+value.trim()+"$$,"
                                        count++
                                    }
                                    columnName = ` ("${column}"::text `
                                    columnNameInfo = ` (cast(${transformedColumn} as text) `
                                    if (column.includes('Id')) {
                                        columnName = ` ("${column}" `
                                        columnNameInfo = ` (cast(${transformedColumn} as text) `
                                    }
                                    if (column.includes('Timestamp')) {
                                        columnTemp = `to_char("` + column + `",'yyyy-mm-dd')`
                                        columnName = ` (${columnTemp}) `

                                        columnTempInfo = `to_char("` + transformedColumn + `",'yyyy-mm-dd')`
                                        columnNameInfo = ` (${columnTempInfo}) `
                                    }
                                    if (count > 1) {
                                       
                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                  
                                        condition = condition + columnName
                                              + `IN` +` (`
                                              + filterString 
                                              + `))`

                                        conditionInfo = conditionInfo + columnNameInfo
                                              + `IN` +` (`
                                              + filterString 
                                              + `))`
                                    } else {
                                        let tempColumnString
                                        let valueString = "$$"+value.trim()+"$$"

                                        // check if has replacement values
                                        if(hasReplacementValues && replacementValues[column] != undefined){
                                            valueString = ` ${replacementValues[column]}`
                                        }

                                        condition = condition + columnName
                                            + `=`
                                            + valueString +` )`

                                        conditionInfo = conditionInfo + columnNameInfo
                                            + `=`
                                            + valueString +` )`
                                    }
                                }

                                if (notNullArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    for (value of notNullArray) {
                                        let tempCondition = `("${column}"::text IS NOT NULL)`
                                        if (notNullArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                        condition += tempCondition

                                        let tempConditionInfo = `(cast(${transformedColumn} as text) IS NOT NULL)`
                                        if (notNullArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                        conditionInfo += tempConditionInfo
                                    }
                                }

                                if (emptyArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    for (value of emptyArray) {
                                        let tempCondition = `("${column}"::text = '')`
                                        if (emptyArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                        condition += tempCondition

                                        let tempConditionInfo = `(cast(${transformedColumn} as text) = '')`
                                        if (emptyArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                        conditionInfo += tempConditionInfo
                                    }
                                }

                                if (nullArray.length > 0) {
                                    if (values.length > 1) condition += ' OR '
                                    else if (condition != '(') condition += ') OR ('
                                    for (value of nullArray) {
                                        let tempCondition =
                                            `("${column}"::text IS NULL OR "${column}"::text = '')`
                                        if (nullArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                        condition += tempCondition

                                        let tempConditionInfo =
                                            `(cast(${transformedColumn} as text) IS NULL OR cast("${transformedColumn}" as text) = '')`
                                        if (nullArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                        conditionInfo += tempConditionInfo
                                    }
                                }
                                if (nilArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('
                                    if (column.includes('Id')) {
                                        let filterString = ''

                                        for (value of nilArray) {
                                            filterString += '$$' + (value.trim()).toLowerCase() + '$$,'
                                        }

                                        filterString = filterString.replace(/(^,)|(,$)/g, "")
                                        let tempCondition = `("${column}" IN (${filterString}))`
                                        condition += tempCondition

                                        let tempConditionInfo = `("${transformedColumn}" IN (${filterString}))`
                                        conditionInfo += tempConditionInfo
                                    } else {
                                        for (value of nilArray) {
                                            let tempCondition
                                            if (column.includes('Timestamp')) {
                                                let tempColumnString = `to_char("` + column + `", 'yyyy-mm-dd')`
                                                column = tempColumnString
                                                tempCondition = `(${tempColumnString}) NOT ILIKE '${value}'`

                                                let tempColumnStringInfo = `to_char("` + transformedColumn + `", 'yyyy-mm-dd')`
                                                columnInfo = tempColumnStringInfo
                                                tempConditionInfo = `(${tempColumnStringInfo}) NOT ILIKE '${value}'`
                                            } else {
                                                tempCondition = `("${column}"::text NOT ILIKE '${value}')`
                                                tempConditionInfo = `(cast(${transformedColumn} as text) NOT ILIKE '${value}')`
                                            }

                                            if (nilArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                            condition += tempCondition

                                            if (nilArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                            conditionInfo += tempConditionInfo
                                        }
                                    }
                                }
                                // Traverse range object
                                if (rangeArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    for (value of rangeArray) {
                                        let tempCondition
                                        // Split the range into min and max values
                                        let range = value.split('-')
                                        let min = range[0]
                                        let max = range[1]

                                        if (column.includes('Timestamp')) {
                                            tempColumnString = `to_char("${column}", 'yyyy-mm-dd')`
                                            column = tempColumnString
                                            tempCondition = `(${tempColumnString}) >= $$${min}$$ AND <= $$${max}$$`

                                            tempColumnStringInfo = `to_char("${transformedColumn}", 'yyyy-mm-dd')`
                                            columnInfo = tempColumnStringInfo
                                            tempConditionInfo = `(${tempColumnStringInfo}) >= $$${min}$$ AND <= $$${max}$$`
                                        } else {
                                            tempCondition = `("${column}"::integer BETWEEN ${min} AND ${max})`
                                            tempConditionInfo = `(cast(${transformedColumn} as integer) BETWEEN ${min} AND ${max})`
                                        }

                                        if (rangeArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                        condition += tempCondition

                                        if (rangeArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                        conditionInfo += tempConditionInfo
                                    }
                                }

                                if (lessThanArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer < '${value}')`

                                    let tempConditionInfo = `(cast(${transformedColumn} as integer) < '${value}')`

                                    if (lessThanArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition

                                    if (lessThanArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                    conditionInfo += tempConditionInfo
                                }

                                if (lessThanEqualArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer <= '${value}')`
                                    let tempConditionInfo = `(cast(${transformedColumn} as integer) <= '${value}')`

                                    if (lessThanEqualArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition

                                    if (lessThanEqualArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                    conditionInfo += tempConditionInfo
                                }

                                if (greaterThanArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer > '${value}')`
                                    let tempConditionInfo = `(cast(${transformedColumn} as integer) > '${value}')`

                                    if (greaterThanArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition

                                    if (greaterThanArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                    conditionInfo += tempConditionInfo
                                }

                                if (greaterThanEqualArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}"::integer >= '${value}')`
                                    let tempConditionInfo = `(cast(${transformedColumn} as integer) >= '${value}')`

                                    if (greaterThanEqualArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition

                                    if (greaterThanEqualArray.indexOf(value) != 0) tempConditionInfo = ` OR ${tempConditionInfo}`
                                    conditionInfo += tempConditionInfo
                                }

                                if (beforeArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    if (column.includes('Timestamp')) {
                                        tempColumnString = `to_char("${column}", 'YYYY-MM-DD HH24'||CHR(58)||'MI'||CHR(58)||'SS')`
                                        column = tempColumnString
                                        tempCondition = `(${tempColumnString}) < $$${value}$$`
                                    } else {
                                        tempCondition = `("${column}"::integer < '${value}')`
                                    }

                                    if (beforeArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (afterArray.length > 0) {
                                    if (condition != '(') condition += ') OR ('

                                    if (column.includes('Timestamp')) {
                                        tempColumnString = `to_char("${column}", 'YYYY-MM-DD HH24'||CHR(58)||'MI'||CHR(58)||'SS')`
                                        column = tempColumnString
                                        tempCondition = `(${tempColumnString}) > $$${value}$$`
                                    } else {
                                        tempCondition = `("${column}"::integer > '${value}')`
                                    }

                                    if (afterArray.indexOf(value) != 0) tempCondition = ` OR ${tempCondition}`
                                    condition += tempCondition
                                }

                                if (inCondition !== null) {
                                    if (condition != '(') condition += ') OR ('

                                    let tempCondition = `("${column}") IN ${inCondition}`
                                    condition += tempCondition

                                    let tempConditionInfo = `("${transformedColumn}") IN ${inCondition}`
                                    conditionInfo += tempConditionInfo
                                }

                                condition += ')'
                                conditionInfo += ')'

                                if (query == '') {
                                    query += condition
                                    queryInfo += conditionInfo
                                } else {
                                    query += ` ` + glue + ` ` + condition
                                    queryInfo += ` ` + glue + ` ` + conditionInfo
                                }
                            } catch (err) {
                                return 'You have provided invalid values for filter.'
                            }
                        }
                    }
                }

                // build sub query
                if (query == '') continue

                if (subQuery == '') {
                    subQuery += ` (` + query + `)`
                    subQueryInfo += ` (` + queryInfo + `)`
                }
                else {
                    subQuery += ` AND (` + query + `)`
                    subQueryInfo += ` AND (` + queryInfo + `)`
                }
            }

            // build main query
            if (subQuery == '') continue

            if (mainQuery == '') {
                mainQuery += ` WHERE (` + subQuery + `)`
                mainQueryInfo += ` (` + subQueryInfo + `)`
            }
            else {
                mainQuery += ` OR (` + subQuery + `)`
                mainQueryInfo += ` OR (` + subQueryInfo + `)`
            }
        }

        return {
            "mainQuery":mainQuery,
            "mainQueryInfo": mainQueryInfo, //different format for filter, currently not in use
            "tableAliases": tableAliases.filter((value, index, array) => array.indexOf(value) === index),
            "columnAliases": columnAliases
        }
    },
    /**
     * Generate the added distinct string for the query
     * 
     * @param columnName the column to set as distinct value
     * 
     * @return the DISTINCT ON (columnName) string
     */

    getDistinctString: (columnName) => {
        let distinctOnValue = columnName;

        // Split the value of distinctOn via |
        let columns = distinctOnValue.split('|');
        let arrLen = columns.length

        let distinct = 'DISTINCT ON ('
        for (col of columns) {
            distinct += `"${col}",`
        }

        distinct = distinct.replace(/,\s*$/, "")
        distinct += ")"

        return distinct
    },

    /**
     * Get added order string for disticntON
     * based on provided columnNames string
     * @param {string} columnNames 
     * @returns {string} added order string
     */
    getAddedOrderString: (columnNames) => {        
        // Split the value of distinctOn via |
        let columns = columnNames.split('|');

        let addedOrder = ''
        for (col of columns) {
            addedOrder += `"${col}",`
        }

        addedOrder = addedOrder.replace(/,\s*$/, "")

        return addedOrder
    },

    /**
     * Builds order string based on distinct columns parameter
     * @param {String} columnName column(s) to be added to DISTINCT ON clause
     * @returns {String} distinct order string
     */
    getDistinctOrderString: (columnName) => {
        let distinctOnValue = columnName;

        // Split the value of distinctOn via |
        let columns = distinctOnValue.split('|');

        let distinctOrderArray = []
        for (col of columns) {
            distinctOrderArray.push(`"${col}"`)
        }

        return distinctOrderArray.join(', ')
    },

    /**
     * Generate the string for the field values
     * 
     * @param fields the string contained in the fields attribute
     * 
     * @return the process fields string for use when distinct on 
     * and fields are both enabled
     * 
     */
    getFieldValuesString: (fields) => {
        let fieldValues = fields.split('|')
        let fieldValuesString = ''
        for (let value of fieldValues) {
            fieldValuesString += `${value}, `
        }

        fieldValuesString = fieldValuesString.replace(/,\s*$/, "")
        return fieldValuesString
    },
    
    /**
     * Generate the string for the field values with double quoted alias
     * 
     * @param fields the string contained in the fields attribute
     * 
     * @return the process fields string for use when distinct on 
     * and fields are both enabled
     * 
     */
    getFieldValuesStringWithQuotes: (fields) => {
        let fieldValues = fields.split('|')
        let fieldValuesString = ''
        for (let value of fieldValues) {
            let parts = value.split(" AS ")
            parts[1] = `"${parts[1]}"`
            value = parts.join(" AS ")
            fieldValuesString += `${value}, `
        }

        fieldValuesString = fieldValuesString.replace(/,\s*$/, "")
        return fieldValuesString
    },

    getVariableList: async (entity, tableName, addedConditionString = '') => {
        // Generate the query
        let sql = `
        SELECT
            distinct on(variable.abbrev) variable.abbrev
        FROM
            master.variable variable,
            operational.` + tableName + ` "tableNameData"
        WHERE
            "tableNameData".is_void = FALSE AND
            "tableNameData".variable_id = variable.id
        ` + addedConditionString

        // Retrieve the variables
        let variableList = await sequelize.query(sql, {
            type: sequelize.QueryTypes.SELECT
        })

        if (variableList == undefined || variableList.length < 1) {
            return null
        }

        return variableList
    },
    /**
     * Generate the order string for sorting by value
     * 
     * @param sort string query given by the user
     * @return orderQuery string generated query for sorting
     * 
     */
    getOrderStringBySortValue: async (sort) => {

        let orderQuery = 'ORDER BY '

        sortValueString = `(CASE `

        let sortValue = sort['sortValue'].split('|');

        for (let [index, val] of sortValue.entries()) {
            if (index == sortValue.length - 1) {
                sortValueString += `WHEN "${sort['attribute']}" LIKE '${val}' THEN `
                    + (index + 1) + ` ELSE ` + (sortValue.length + 1) + ` END)`
            } else {
                sortValueString += `WHEN "${sort['attribute']}" LIKE '${val}' THEN `
                    + (index + 1) + ` `
            }
        }

        // Concatenate the order query with the sub query
        orderQuery = orderQuery + sortValueString
        return orderQuery
    },

    /**
     * Generate the query of counting total records
     * 
     * @param parameters given by the user
     * @param excludedParametersArray not involved in the query
     * @param conditionString filter string condition
     * @param addedDistinctString string for disctinct parameter
     * @param tableJoins tables to be joined in main table
     * @param mainTable name of the main table
     * @param tableAliases alias used in reference to the filters
     * @param mainCol main column of the table
     * @param columnAliases columns to be used in reference to the filters
     * @param orderString string for order parameter (when there's distinct parameter)
     * @param responseColumns string containing list of all response columns
     * @return countQuery string generated query for total count
     * 
     */
    getTotalCountQuery: async (parameters, excludedParametersArray, conditionString, addedDistinctString, tableJoins, mainTable, tableAliases, mainCol, columnAliases, orderString, responseColumns) => {

        //get joins that will be used in query

        
        let joinStr = ''
        let columnStr = mainCol
        let usedJoin = []

        //parse responseColumns
        let responseColArr = responseColumns.split(",").map(e => e.trim())
        let tableKeys = Object.keys(tableJoins)
        for (column in parameters) {
            if (!excludedParametersArray.includes(column)) {
                let tempColumn = column
                if(column.includes('DbId')){
                    tempColumn = column.replace("DbId", "Id")
                }
                let transformedCol =  tempColumn.replace(/[A-Z]/g, (letter, index) => { return index == 0 ? letter.toLowerCase() : '_'+ letter.toLowerCase();});
               
                if(tableAliases.length > 0){
                    
                    for (let colInfo of tableAliases){
                        let colIndex = tableKeys.indexOf(colInfo)
                        if(colIndex >= 0 && !usedJoin.includes(tableKeys[colIndex])){
                            joinStr += tableJoins[tableKeys[colIndex]]
                            usedJoin.push(tableKeys[colIndex])
                        }
                    }
                }

                if(columnAliases.length > 0){
                    if(!columnAliases.join(",").includes(mainCol)){
                        columnStr = mainCol+", "+columnAliases.join(",")
                    } else {
                        columnStr = columnAliases.join(",")
                    }
                }
            }
        }
        let mainTblAliasArr = mainCol.split(".")
        let mainTblAlias = mainTblAliasArr[0]

        if(addedDistinctString == ''){ //use orderString only if there's distinct parameter
            orderString = ''
        } else {
            //parse distinct string
            let tempStr = addedDistinctString.replace("DISTINCT ON (", "")
            tempStr = tempStr.replace(")","")
            let tempArr = tempStr.split(",")

            for(let temp of tempArr){
                let indexCol = (responseColArr.map(e => e.includes(temp))).indexOf(true)
               
                if(!columnStr.includes(responseColArr[indexCol])){
                    columnStr += ', '+responseColArr[indexCol]
                    let aliasArr = responseColArr[indexCol].split(".")
                    
                    let colIndex = tableKeys.indexOf(aliasArr[0].trim())
                    if(colIndex >= 0 && !usedJoin.includes(tableKeys[colIndex])){
                        joinStr += tableJoins[tableKeys[colIndex]]
                        usedJoin.push(tableKeys[colIndex])
                    }
                }
            }
        }

        // retrieve limit condition from config
        let limit = await getTotalCountLimit()

        let countQuery = `
            SELECT 
                count(1)
            FROM
                (
                SELECT
            `
                + addedDistinctString +
                `
                    *
                FROM
                    (
                    SELECT
                        ${columnStr}
                    FROM
                        ${mainTable}
                    ${joinStr}
                    WHERE
                        ${mainTblAlias}.is_void = false
                    ${orderString}
                    ) as tbl
            `
                + conditionString +
                limit +
                `
                ) as tbl2
            `

        return countQuery
    },

    /**
     * Generate combination of final query and total count
     * 
     * @param finalQuery main query formed
     * @param orderString order to be applied in query
     * @return finalTotalQuery string generated query for total count
     * 
     */
    getFinalTotalCountQuery: async (finalQuery, orderString = '') => {
        let finalTotalQuery = `
                WITH mainQuery AS (
                    ${finalQuery}
                ),
                orderedRes AS (
                    SELECT
                         ROW_NUMBER () OVER (${orderString}) AS "orderNumber",
                        *
                    FROM
                        mainQuery
                    LIMIT (:limit) OFFSET (:offset)
                ),
                countQuery AS (
                    SELECT
                        COUNT(1) AS "totalCount"
                    FROM
                        mainQuery
                )
                SELECT
                    countQuery."totalCount",
                    orderedRes.*
                FROM
                    countQuery,
                    orderedRes
            `
        return finalTotalQuery
    },

    /**
     * Generate the final SQL query without LIMIT
     * 
     * @param sqlQuery string initial query
     * @param conditionString string condition 
     * @param orderString string order
     * @param distinctString string distinct on
     * 
     * @return finalSqlQuery string generated query
     */
    getFinalSqlQueryWoLimit: async (sqlQuery, conditionString = '', orderString = '', distinctString = '') => {
        // Generate the query
        let finalSqlQuery = `
        select 
        `
            + distinctString +
            `
            *
        from
            (
        `
            + sqlQuery +
            `
            ) as tbl
        `
            + conditionString +
            ` `
            + orderString 

        return finalSqlQuery
    }
}
