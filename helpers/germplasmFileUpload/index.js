/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let errorBuilder = require('../error-builder')
let errors = require('restify-errors')

module.exports = {

    /**
     * Builds the default fields, left join, entity condition, and order string
     * based on the given entity.
     * @param {string} entity - the entity being retrieved
     */
    fuDataDataSubQueryBuilder: async (entity) => {
        let defaultFields = ``
        let entityLeftJoin = `
            LEFT JOIN
                germplasm.${entity} ON ${entity}.id = fuData.entity_id AND ${entity}.is_void = FALSE
        `
        let entityCondition = `
                AND fuData.entity = '${entity}'
        `
        let addedOrderString = `
            ORDER BY
                ${entity}.id
        `

        if (entity == 'germplasm') {
            defaultFields = `
                ${entity}.id AS "germplasmDbId",
                ${entity}.germplasm_code AS "germplasmCode",
                ${entity}.designation,
                ${entity}.parentage,
                ${entity}.generation,
                ${entity}.germplasm_state AS "germplasmState",
                ${entity}.germplasm_type AS "germplasmType"
            `
        }
        else if (entity == 'germplasm_name') {
            defaultFields = `
                ${entity}.id AS "germplasmNameDbId",
                ${entity}.germplasm_id AS "germplasmDbId",
                ${entity}.name_value AS "nameValue",
                ${entity}.germplasm_name_type AS "germplasmNameType",
                ${entity}.germplasm_name_status AS "germplasmNameStatus",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.designation,
                germplasm.parentage,
                germplasm.generation,
                germplasm.germplasm_state AS "germplasmState",
                germplasm.germplasm_type AS "germplasmType"
            `

            entityLeftJoin += `
                JOIN
                    germplasm.germplasm
                        ON germplasm.id = ${entity}.germplasm_id
                        AND germplasm.is_void = FALSE
            `
        }
        else if(entity === 'germplasm_relation') {
            defaultFields = `
                ${entity}.id AS "germplasmRelationDbId",
                ${entity}.order_number AS "orderNumber",
                parent_germplasm.id AS "parentGermplasmDbId",
                parent_germplasm.designation AS "parentGermplasmName",
                parent_germplasm.germplasm_code AS "parentGermplasmCode",
                child_germplasm.id AS "childGermplasmDbId",
                child_germplasm.designation AS "childGermplasmName",
                child_germplasm.germplasm_code AS "childGermplasmCode"
            `
            entityLeftJoin += `
                JOIN
                    germplasm.germplasm parent_germplasm
                        ON parent_germplasm.id = ${entity}.parent_germplasm_id
                        AND parent_germplasm.is_void = FALSE
                JOIN
                    germplasm.germplasm child_germplasm
                        ON child_germplasm.id = ${entity}.child_germplasm_id
                        AND child_germplasm.is_void = FALSE
            `
        }
        else if(entity === 'germplasm_attribute') {
            defaultFields = `
            germplasm_attribute.id AS "germplasmAttributeDbId",
            germplasm_attribute.germplasm_id AS "germplasmDbId",
            germplasm.germplasm_code AS "germplasmCode",
            germplasm.designation AS "designation",
            growthHabit.data_value AS "growthHabit",
            dhFirstIncrease.data_value AS "dhFirstIncreaseCompleted",
            heteroticGroup.data_value AS "heteroticGroup",
            mtaStatus.data_value AS "mtaStatus",
            crossNumber.data_value AS "crossNumber",
            grainColor.data_value AS "grainColor"
            `
            entityLeftJoin += `
                LEFT JOIN 
                    germplasm.germplasm germplasm 
                        ON germplasm.id = ${entity}.germplasm_id
            	LEFT JOIN LATERAL (
                    SELECT
                        germplasm_attribute.data_value
                    FROM
                        master.variable variable
                    WHERE
                        variable.is_void = FALSE
                        AND variable.abbrev = 'GROWTH_HABIT'
                        AND variable.id = germplasm_attribute.variable_id
                    LIMIT 1
                ) growthHabit ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        germplasm_attribute.data_value
                    FROM
                        master.variable variable
                    WHERE
                        variable.is_void = FALSE
                        AND variable.abbrev = 'DH_FIRST_INCREASE_COMPLETED'
                        AND variable.id = germplasm_attribute.variable_id
                    LIMIT 1
                ) dhFirstIncrease ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        germplasm_attribute.data_value
                    FROM
                        master.variable variable
                    WHERE
                        variable.is_void = FALSE
                        AND variable.abbrev = 'HETEROTIC_GROUP'
                        AND variable.id = germplasm_attribute.variable_id
                    LIMIT 1
                ) heteroticGroup ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        germplasm_attribute.data_value
                    FROM
                        master.variable variable
                    WHERE
                        variable.is_void = FALSE
                        AND variable.abbrev = 'MTA_STATUS'
                        AND variable.id = germplasm_attribute.variable_id
                    LIMIT 1
                ) mtaStatus ON TRUE                
                LEFT JOIN LATERAL (
                    SELECT
                        germplasm_attribute.data_value
                    FROM
                        master.variable variable
                    WHERE
                        variable.is_void = FALSE
                        AND variable.abbrev = 'GRAIN_COLOR'
                        AND variable.id = germplasm_attribute.variable_id
                    LIMIT 1
                ) grainColor ON TRUE                
                LEFT JOIN LATERAL (
                    SELECT
                        germplasm_attribute.data_value
                    FROM
                        master.variable variable
                    WHERE
                        variable.is_void = FALSE
                        AND variable.abbrev = 'CROSS_NUMBER'
                        AND variable.id = germplasm_attribute.variable_id
                    LIMIT 1
                ) crossNumber ON TRUE
            `
        }

        return {
            defaultFields: defaultFields,
            entityLeftJoin: entityLeftJoin,
            entityCondition: entityCondition,
            addedOrderString: addedOrderString
        }
    }
}