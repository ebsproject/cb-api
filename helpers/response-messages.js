/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


//add default status messages here

module.exports = (statusCode) => {
    switch (statusCode) {

        case 200:

            message = 'Request has been successfully completed.';

            break;

        case 201:
            message = 'Record has been successfully created.'
            break

        case 401:

            message = "Request Unauthorized";

            break;

        case 403:

            message = "Request not allowed."

            break;

        case 400:

            message = "Your request has invalid/missing information."

            break

        case 404:

            message = "Resource not found."

            break

        case 409:
            message = 'Your request was unsuccessful due to a data conflict.'
            break

        case 500:
            message = "The server failed to execute your request."
            break
    }

    return message;


}