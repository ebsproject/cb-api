/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize,Study} = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op

module.exports = {

    /**
     * Generate the allowed study IDS and variable IDs per program
     * 
     * @param type integer specify if the studies are owned
     * @param programDbId integer ID of the program
     * @param isOperational boolean if the studies going to be retrieved is for operational
     * @param isTempOp boolean if the studies going to be retrieved is for temporary
     * @param isService boolean if the studies going to be retrieved is for service 
     * @param isDraft boolean if the studies going to be retrieved is for draft 
     * 
     * @return array consisting the study IDs and variable IDs
     */
    constructIds: async (type, programDbId, isOperational = false, isTempOp = false, isService = false, isDraft = false) => {

        let sql = ''
        let variableIdString = ''
        let studyIdString = ''
        let conditionString = ''

        // Set the variables to be excluded
        let excludeVariableString = `
            'PLOT_FILE',
            'ENTRY_FILE',
            'STUDY_FILE',
            'TERMINAL_STUDY_ID',
            'SEASON',
            'STUDY_TITLE',
            'YEAR',
            'PLACE',
            'PHASE',
            'PROGRAM',
            'REP_COUNT',
            'PROCESS_PATH_ID',
            'AUTHOR',
            'STUDY',
            'STUDY_TYPE',
            'DESIGN',
            'ECOSYS',
            'ECOSYSTEM',
            'ESTABLISHMENT',
            'FLD_LOC',
            'ENTRY_COUNT_CONT',
            'PLOT_COUNT',
            'AUTHOR_ID',
            'PROCESS_PATH'
        `

        // Set condition
        if(isService){
            conditionString = conditionString + `
                and (
                    study.study_status is null
                    or (
                        (
                            study.study_status ilike '%created%'
                        )
                        and (
                            study.study_status not ilike '%archived%'
                        )
                    )
                )
            `
        } else {
            if(isOperational){
                if(isTempOp){
                    conditionString = conditionString + `
                        and (
                            study.study_status is null
                            or ((
                                    study.study_status ilike '%committed study%'
                                    or study.study_status ilike '%operational%'
                                )
                                and (
                                    study.study_status not ilike '%archived%'
                                )
                            )
                        )
                    `
                } else {
                    conditionString = conditionString + `
                        and (
                            study.study_status is null
                            or ((
                                    study.study_status ilike '%committed study%'
                                )
                                and (
                                    study.study_status not ilike '%archived%'
                                )
                            )
                        )
                    `
                }
            } else {
                conditionString = conditionString + `
                    and (
                        study.study_status is null
                        or (
                            study.study_status like '%created%'
                            and study.study_status not ilike '%committed%'
                        )
                    )
                `
            }
        }

        let isDraftCondition = ''

        if(isDraft){
            conditionString = conditionString + `
                and study.is_draft = true
            `
        }

        // Default
        let op1 = ''
        let op2 = ''

        if(type != 'owned'){
            if(type == 'received'){
                //get studies from other teams that uses a process path that is owned by the current program
                op1 = '<>'
                op2 = '='
            } else {
                //get studies from current team that uses a process path that is owned by other programs
                op1 = '='
                op2 = '<>'
            }

            // Generate the query
            sql = `
                select
                    array_to_string(array_agg(distinct variable.id), ',') as variable_id,
                    array_to_string(array_agg(distinct study.id), ',') as study_id
                from
                    master.program_team "programTeam",
                    master.item_service_team "itemServiceTeam",
                    master.item item,
                    master.service service,
                    operational.study study
                left join
                    operational.study_metadata "studyMetadata"
                on
                    "studyMetadata".study_id = study.id
                    and "studyMetadata".is_void = false
                left join
                    master.variable variable
                on
                    variable.id = "studyMetadata".variable_id
                    and variable.is_void = false
                    and variable.type <> 'system'
                    and variable.abbrev not in (` + excludeVariableString + `)
                where
                    study.program_id ` + op1 + ` ` + programDbId + `
                    and study.is_void = false
                    ` + conditionString + `
                    ` + isDraftCondition + `
                    and "programTeam".program_id ` + op2 + ` ` + programDbId + `
                    and "programTeam".is_void = false
                    and "itemServiceTeam".proc_path_id = study.process_path_id::integer
                    and "itemServiceTeam".team_id = "programTeam".team_id
                    and "itemServiceTeam".is_void = false
                    and "itemServiceTeam".service_id = service.id
                    and service.is_void = false
                    and item.id = "itemServiceTeam".proc_path_id
                    and item.is_void = false
                    and item.is_active = true
            `
        } else {
            // Generate the query
            sql = `
                select
                    array_to_string(array_agg(distinct variable.id), ',') as variable_id,
                    array_to_string(array_agg(distinct study.id), ',') as study_id
                from
                (
                select
                    distinct study.id
                from
                    operational.study study,
                    master.program_team "programTeam",
                    master.item_service_team "itemServiceTeam",
                    master.item item
                where
                    study.program_id = ` + programDbId + `
                    and study.is_void = false
                    ` + conditionString + `
                    ` + isDraftCondition + `
                    and "programTeam".program_id = study.program_id
                    and "programTeam".is_void = false
                    and study.process_path_id::integer = "itemServiceTeam".item_id
                    and "itemServiceTeam".team_id = "programTeam".team_id
                    and "itemServiceTeam".is_void = false
                    and item.id = "itemServiceTeam".item_id
                    and item.is_void = false
                    and item.is_active = true
                union
                select 
                    distinct study.id
                from
                    operational.study study,
                    master.item item
                where
                    item.type = 40
                    and item.process_type = 'operational'
                    and item.is_void = false
                    and item.is_active = true
                    and item.id not in(
                        select distinct "itemServiceTeam".item_id
                        from master.item_service_team "itemServiceTeam", master.item i2
                        where "itemServiceTeam".item_id = i2.id and "itemServiceTeam".is_void = false and i2.type = 40 and i2.process_type = 'operational' and i2.is_void = false
                    )
                    and study.process_path_id::integer = item.id
                    and study.is_void = false
                    ` + conditionString + `
                    and study.program_id = ` + programDbId + `
                ) study
                left join
                    operational.study_metadata "studyMetadata"
                on
                    "studyMetadata".study_id = study.id
                    and "studyMetadata".is_void = false
                left join
                    master.variable variable
                on
                    variable.id = "studyMetadata".variable_id
                    and variable.is_void = false
                    and variable.type <> 'system'
                    and variable.abbrev not in (` + excludeVariableString + `)
            `
        }

        let result = await sequelize.query(sql,{
            type: sequelize.QueryTypes.SELECT
        })

        if(result[0] != null){
            variableIdString = result[0]['variable_id']
            studyIdString = result[0]['study_id']
        }
        
        return {
            'variableIdString': variableIdString,
            'studyIdString': studyIdString
        }
    }
}