/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const axios = require('axios')
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args))
const logger = require('../logger')

module.exports = {

    /**
     *    Retrieve response data by calling existing API endpoints
     *
     *    @param httpMethod string http method
     *    @param url string url + endpoint + id (if applicable)
     *    @param token string token
     *    @param requestData array request data
     *    @param source string request source e.g. PDM
     *
     *    @return object response data
     */
    getData: async (httpMethod, url, token, requestData, source) => {
        let properties = {
            url: url,
            method: httpMethod,
            headers: {
                "Authorization": token,
                "Content-type": "application/json",
                "Accept": "application/json"
            },
            body: null
        }

        let allowedSearchCalls = ["entries-search", "members-search", "plots-search", "reorder-list-members"]

        if (httpMethod === "POST") {
                let searchCallString = url.split('/')

                searchCallString = searchCallString[searchCallString.length-1]

                if (allowedSearchCalls.includes(searchCallString)) {
                        properties.body = requestData
                }
        }

        if(source !== 'PDM'){
            if (httpMethod === "PUT") {
                properties.body = requestData
            }

            let res = await fetch(url, properties)
                .catch(async err => {
                    logger.logMessage(__filename, 'Broker request: ' + httpMethod + ' ' + url
                        + ' ' + JSON.stringify(err), 'error')
                return
            })

            let jsonData = await res.json()
            let data = {
                id: null,
                status: res.status,
                response: (res.ok) ? jsonData.result.data : jsonData.metadata.status
            }
            
            let urlArray = url.split('/').map(item => item.trim())
            let id = urlArray[urlArray.length-1]
            data.id = id

            return data
        }else{
            if (httpMethod === "PUT") {
                properties.data = requestData
            }
            let res = await axios(properties)
                .catch(async err => {
                    logger.logMessage(__filename, 'Broker request: ' + httpMethod + ' ' + url +
                    + ' request data' + JSON.stringify(requestData), 'custom')
                    logger.logMessage(__filename, 'Error in axios: ' + err.response.status + ' '
                    + err.response.statusText +  ' ' + JSON.stringify(err.response.data), 'error')
                return
            })

            let urlArray = url.split('/').map(item => item.trim())
            let id = urlArray[urlArray.length-1]

            let jsonData = res.data
            return {
                id: id,
                status: res.status,
                response: (res.statusText == 'OK') ? jsonData.result.data : jsonData.metadata.status,
            }
        }
    }
}