/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')
let germplasmHelper = require('../../helpers/germplasm/index')

module.exports = {
  getFilterCondition: (filters, column, columnCast=false,replacements=false,replacementValues=null) => {
    let condition = '';

    let hasReplacementValues = replacements && replacementValues != null && replacementValues[column] != undefined

    if (typeof filters === 'string') {
      let filterOperator = 'like'
      // check for operator in string
      if (filters.includes('not equals')) {
        filterOperator = 'not equals'
        filters = filters.replace(/not equals/gi, '').trim()
      } else if (filters.includes('equals')) {
        filterOperator = 'equals'
        filters = filters.replace(/equals/gi, '').trim()
      } else if (filters.includes('not null')) {
        filterOperator = 'is not null'
        filters = filters.replace('not null', '').trim()
      } else if (filters.includes('null')) {
        filterOperator = 'is null'
        filters = filters.replace('null', '').trim()
      } else if (filters.includes('not ilike')) {
        filterOperator = 'not like'
        filters = filters.replace(/not ilike/gi, '').trim()
      } else {
        filterOperator = 'like'
      }
      filters = {[filterOperator]:filters}
    }

    // loop through the filter
    for (operator in filters) {
      let columnString = `"` + column + `"`
      if (columnCast) {
        columnString = column 
      }
      let filter = filters[operator]
      operator = operator.toLowerCase();

      if (operator == 'not equals') {
          if (condition != '') {
              condition += ` AND `
          }
      } else {
          if (condition != '') {
              condition += ` OR `
          }
      }

      if (operator != 'is null' && operator != 'is not null') {
        if (filter == '' || filter == null) {
          return 'You have provided invalid values for filter.'
        }
        
        if (['equals', 'not equals'].includes(operator) && !column.includes('Document')) {
          // Parse the value
          let valueList = ''
          let count = 0
          var operatorGlue = 'OR';
          let finalOperatorMultiple = `IN`
          let finalOperatorSingle = `=`

          if (operator == 'not equals') {
            finalOperatorMultiple = `NOT IN`
            finalOperatorSingle = `<>`
            operatorGlue = `AND`
          }
          for (value of filter.split('|')) {
            valueList += "$$"+value.trim()+"$$,"
            count++
          }

          if (count > 1) {
            if (column.includes('Timestamp')) {
              columnString = `to_char("` + column + `",'yyyy-mm-dd')`
            }
            valueList = valueList.replace(/(^,)|(,$)/g, "")

            // check if has replacement values for given column
            if(hasReplacementValues){
              valueList = replacementValues[column]
            }

            condition = condition + columnString
              + ` `+ finalOperatorMultiple +` (`
              + valueList
              + `)`
          } else {
            condition = condition + columnString 
              + ` `+ finalOperatorSingle
              + "$$"+value+"$$"
          }
        } else if (operator == 'range') {
          // Parse the value; for range, values should just be min and max
          let rangeValue = filter.split('|')

          if (column.includes('Timestamp')) {
            columnString = `to_char("` + column + `",'yyyy-mm-dd')`
            let minRange = "$$" + rangeValue[0] + "$$"
            let maxRange = "$$" + rangeValue[1] + "$$"

            condition = condition + columnString
              + ` >= ` + minRange
              + ` AND ` + columnString
              + ` <= ` + maxRange
          } else {
            condition = condition + columnString
              + ` BETWEEN ` + rangeValue[0] + ` AND `
              + rangeValue[1]
          }
        } else if (['less than', 'less than or equal to', 'greater than', 'greater than or equal to'].includes(operator)) {
          var finalOperator = '';
          if (operator == 'less than') { finalOperator = `<` }
          else if (operator == 'less than or equal to') { finalOperator = `<=` }
          else if (operator == 'greater than') { finalOperator = `>` }
          else { finalOperator  = `>=`}

          // These operators accepts single values only
          // check if value is valid number
          var value = filter;
          if (Number.isNaN(Number(value)) == true) {
            let errMsg = 'Invalid input for numeric type.'
            res.send(new errors.ForbiddenError(errMsg))
            return
          }

          condition = condition + columnString
            + ` ` + finalOperator + ` `
            + value
        } else { // For like, not like, and contains
          
          // Parse the value
          var operatorGlue = 'OR';
          let finalOperator = `ILIKE`

          if (operator == 'not like') {
            finalOperator = `NOT ILIKE`
            operatorGlue = `AND`
          }
          
          if (column == 'otherNames') {
            var tempCondition = '';
            columnString = `"` + column + `"::text`
            // Parse the value
            for (value of filter.split('|')) {
              value = "$$%"+value.trim()+"%$$"
              if (tempCondition != ''){ tempCondition += ` `+operatorGlue+` `}
              tempCondition = tempCondition + columnString
                  + ` ` +finalOperator+` `
                  + value
            }
            condition = condition + `(` + tempCondition + `)`
          } else {
            if (column.includes('Timestamp')) {
              columnString = `to_char("` + column + `",'yyyy-mm-dd')`
            } else if (column.includes('Document')){
              columnString = `"` + column + `" @@ to_tsquery`
            } else {
              columnString = columnString + `::text`
            }

            let filterCount = filter.split('|').length
            // check if has replacement values for given column
            if(hasReplacementValues){
              filter = filter.replaceAll('|',',')
            }

            var tempCondition = '';
            var separator = filter.includes('|') ? '|' : ',';

            for (value of filter.split(separator)) {
              // handles case if value has "equals" for lists
              if(value.startsWith('equals ')) {
                finalOperator = filterCount > 1 ? `IN` : `=`
                value = value.substring(7)  // removes 'equals '
              }

              originalValue = "$$'"+value.trim()+"'$$"
              value = (operator == 'contains') ? "$$%"+value.trim()+"%$$" : "$$"+value.trim()+"$$"

              // check if has replacement values for given column
              if(hasReplacementValues){
                originalValue = replacementValues[column]
              }

              if (tempCondition != ''){ tempCondition += ` `+operatorGlue+` `}
              if (column.includes('Document')) {
                tempCondition = tempCondition + columnString
                    + `(` + originalValue + `)`
              } else {
                tempCondition = tempCondition + columnString
                    + ` ` +finalOperator+` (`
                    + value + `)`
              }
            }

            condition = condition + `(` + tempCondition + `)`
          }
        }
        
      } else {
        condition = condition + columnString
          + ` `+operator
      }
    }
    return condition;
  },

  /**
   * Build search param items order clause
   * 
   * @param {Array} itemArr array of search parameter items
   * @param {String} column name of column to be sorted
   * 
   * @returns String orderQuery
   */
  getSortClauseForSpecificOrder: async (itemArr,column) => {

    let caseConditions = ''
    let condCount = 0
    
    for(item of itemArr){
      if(column === 'germplasmNormalizedName'){
        item = await germplasmHelper.normalizeText(item)
      }

      caseConditions += `
            WHEN "${column}"::TEXT = $$${item}$$ THEN ${(condCount + 1)}`
      condCount += 1
    }
    
    orderQuery = `
      ORDER BY
          CASE
            ${caseConditions}
          END
      ASC
    `
    
    return orderQuery
  }
}