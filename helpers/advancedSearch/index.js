/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let amqp = require('amqplib')
let logger = require('../logger/index.js')

/**
 * Sends data to worker to update the Advanced Search records
 * @param {string} endpoint - cb api endpoint used to update the record in cb-db
 * @param {string} method - method of endpoint
 * @param {object} searchQuery - query to be used to retrieved advanced search records
 * @param {object} updatedValues - values that were updated
 */
async function triggerAdvancedSearchUpdateWorker(endpoint, method, searchQuery, updatedValues) {
    // Format updated values
    switch (endpoint) {
        case 'packages/_id':
            if('packageQuantity' in updatedValues) {
                updatedValues['quantity'] = parseFloat(updatedValues['packageQuantity'])
                delete updatedValues['packageQuantity']
            }
            if('packageUnit' in updatedValues) {
                updatedValues['unit'] = updatedValues['packageUnit']
                delete updatedValues['packageUnit']
            }
            if('geospatialObjectDbId' in updatedValues) {
                updatedValues['geospatialObjectDbId'] = parseInt(updatedValues['geospatialObjectDbId'])
            }
            if('programDbId' in updatedValues) {
                updatedValues['programDbId'] = parseInt(updatedValues['programDbId'])
            }
            if('facilityDbId' in updatedValues) {
                updatedValues['facilityDbId'] = parseInt(updatedValues['facilityDbId'])
            }
            break;
        default:
            break;
    }

    let collectionName = 'cb-seed-packages-search'
    // Set data to pass to worker
    let data = {
        'collectionName': collectionName,
        'searchQuery': searchQuery,
        'updatedValues': updatedValues
    }

    let workerName = 'AdvancedSearch'

    // Retrieve config values
    let host = (process.env.CB_RABBITMQ_HOST)
    let port = (process.env.CB_RABBITMQ_PORT)
    let user = (process.env.CB_RABBITMQ_USER)
    let password = (process.env.CB_RABBITMQ_PASSWORD)
    password = encodeURIComponent(password)

    // Create connection to RabbitMQ worker
    let connection = await amqp.connect('amqp://' + user + ':' + password + '@' + host + ':' + port)

    // Create channel
    let channel = await connection.createChannel()

    // Assert the queue for the worker
    await channel.assertQueue(workerName, { durable: true })

    // Send data to RabbitMQ worker
    await channel.sendToQueue(workerName, Buffer.from(JSON.stringify(data)), {
        contentType: 'application/json',
        persistent: true
    })

    await logger.logCustomStatus('advancedSearch', 'Data successfully passed to advancedSearch worker.')
}

module.exports = {
    /**
     * Sends data to worker to update the Advanced Search records
     * @param {string} endpoint - cb api endpoint used to update the record in cb-db
     * @param {string} method - method of endpoint
     * @param {object} searchQuery - query to be used to retrieved advanced search records
     * @param {object} updatedValues - values that were updated
     */
    triggerAdvancedSearchUpdate: async (endpoint, method, searchQuery, updatedValues) => {
        await triggerAdvancedSearchUpdateWorker(endpoint, method, searchQuery, updatedValues)
    }
}