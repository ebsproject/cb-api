/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let errorBuilder = require('../error-builder.js')
let errors = require('restify-errors')

module.exports = {

    getEntityGivenType: async (req,res,type) => {

        let entityAbbrev = null

        switch (type) {
            case 'designation':
            case 'germplasm':
                entityAbbrev = `GERMPLASM`
                break
            case 'location':
                entityAbbrev = `LOCATION`
                break
            case 'plot':
                entityAbbrev = `PLOT`
            break
            case 'seed':
                entityAbbrev = `SEED`
                break
            case 'trait':
            case 'trait protocol':
                entityAbbrev = 'TRAIT'
                break
            case 'variable':
                entityAbbrev = 'VARIABLE'
                break
            case 'package':
                entityAbbrev= 'PACKAGE'
                break
            case 'experiment':
                entityAbbrev = 'EXPERIMENT'
                break
            case 'sites':
                entityAbbrev = 'SITE'
                break
            default:
                let errMsg = await errorBuilder.getError(req.headers.host, 400166)
                res.send(new errors.BadRequestError(errMsg))
                return
        }

        return entityAbbrev
    }
}