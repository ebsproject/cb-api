/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let Sequelize = require('sequelize')
let errors = require('restify-errors')
const errorBuilder = require('../error-builder')

module.exports = {
    /**
     * Generates a new plot code, updates the plot code set in the plot table
     * and returns the record count
     * 
     * @param array payload request body
     * @param integer experimentDbId the id of the target experiment
     * @param array pattern the pattern response of the pattern query to the database
     * @param object res response
     * 
     * @return integer count of the updated result
     */
    generateExperimentPlotCode: async (payload, experimentDbId, pattern, res) => {
        // Retrieve the pattern from the parameters
        pattern = pattern[0]['data_value']
        let fromArr = []
        let selectArr = []
        let offset = 0

        for (let guide of pattern['pattern']) {
            if (guide['is_variable']) {
                if (
                    guide['entity_table'] == 'experiment.experiment' ||
                    guide['entity_table'] == 'experiment.plot' ||
                    guide['entity_table'] == 'experiment.occurrence'
                ) {
                    selectArr.push(guide['entity_column'])
                } else {
                    selectArr.push(guide['entity_column'])

                    const fromStr = `
                        LEFT JOIN
                            ${guide['entity_table']}
                        ON
                            ${guide['entity_table']}.${guide['entity_reference_column']} = ${guide['entity_reference_table']}
                    `

                    if (fromArr.includes(fromStr)) continue

                    fromArr.push(fromStr)
                }
            } else if (guide['value'] == 'COUNTER') {
                let padding = ''
                let counterOnly = ''

                if (payload.offset != undefined) {
                    offset = payload.offset
                } else {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400243)
                    res.send(new errors.BadRequestError(errMsg))
                    return false
                }

                // This generates numbers wihout zero padding and number length restrictions (e.g. 501, 502, ..., 600, ..., 1000, ...)
                if (!guide['zero_padding'] || !guide['digits']) {
                    counterOnly = `( ${offset - 1} + ROW_NUMBER() OVER (PARTITION BY experiment.id ORDER BY plot.occurrence_id, plot.plot_number))::text`
                    selectArr.push(counterOnly)

                    continue // Move on to the next iteration
                }

                if (guide['zero_padding'] == 'leading') {
                    padding = 'lpad'
                } else {
                    padding = 'rpad'
                }

                padding = padding + `(
                    ( ${offset - 1} + ROW_NUMBER() OVER (PARTITION BY experiment.id ORDER BY plot.occurrence_id, plot.plot_number))::text, ` + parseInt(guide['digits']) + `, '0')`

                selectArr.push(padding)
            } else {
                selectArr.push(`'${guide['value']}'`)
            }
        }

        // Build the query
        let selectStr = selectArr.join(',')
        let fromStr = ''

        // Append the left join
        if (fromArr.length > 0) {
            fromStr = fromArr.join('')
        }

        let query = `
            WITH rows AS (
                UPDATE
                    experiment.plot
                SET
                    plot_code = pattern.code
                FROM (
                    SELECT
                        CONCAT(${selectStr}) AS code,
                        plot.id AS plot_id
                    FROM
                        experiment.plot
                        LEFT JOIN
                            experiment.occurrence
                        ON
                            occurrence.id = plot.occurrence_id
                        LEFT JOIN
                            experiment.experiment
                        ON
                            experiment.id = occurrence.experiment_id
                        ${fromStr}
                    WHERE
                        plot.is_void = FALSE AND
                        experiment.id = ${experimentDbId}
                ) pattern
                WHERE
                    plot.id = pattern.plot_id
                RETURNING
                    1
            )
            SELECT
                COUNT(1)
            FROM
                rows
        `

        let updateResult = await sequelize.query(query, {
            type: sequelize.QueryTypes.UPDATE
        })

        let result = parseInt(updateResult[0][0]['count'])

        return result
    },
    /**
     * Generates a new plot code, updates the plot code set in the plot table
     * and returns the record count
     * 
     * @param array payload request body
     * @param integer experimentDbId the id of the target experiment
     * @param object res response
     * 
     * @return integer count of the updated result
     */
    generateOccurrencePlotCode: async (payload, experimentDbId, res) => {
        let updateCount = 0

        let query = `
            SELECT
                id
            FROM
                experiment.occurrence
            WHERE
                is_void = FALSE AND
                experiment_id = ${experimentDbId}
        `

        let occurrences = await sequelize.query(query, {
            type: sequelize.QueryTypes.SELECT
        })

        for (let key of Object.keys(payload)) {
            let occurrenceMatch = false

            for (let occurrence of occurrences) {
                if (occurrence['id'] == key) {
                    occurrenceDbId = occurrence['id']
                    occurrenceMatch = true

                    query = `
                        SELECT
                            data_value::json
                        FROM
                            experiment.occurrence_data
                        WHERE
                            variable_id = (
                                SELECT
                                    id
                                FROM
                                    master.variable
                                WHERE
                                    abbrev = 'PLOT_CODE_PATTERN'
                            ) AND
                            is_void = FALSE AND
                            occurrence_id = ${occurrenceDbId}
                    `

                    let occ = await sequelize.query(query, {
                        type: sequelize.QueryTypes.SELECT
                    })

                    if (occ.length == 0) {
                        let errMsg = await errorBuilder.getError(req.headers.host, 400244)
                        res.send(new errors.BadRequestError(errMsg))
                        return
                    }
                }
            }

            if (!occurrenceMatch) {
                let errMsg = 'Invalid request. Ensure that the occurrenceDbId is an occurrence of an experiment.'
                res.send(new errors.BadRequestError(errMsg))
                return false
            }
        }

        let offset

        for (let occurrence of occurrences) {
            occurrenceDbId = occurrence['id']
            if (payload[occurrenceDbId] != undefined) {
                if (payload[occurrenceDbId]["offset"] == undefined) {
                    let errMsg = await errorBuilder.getError(req.headers.host, 400245)
                    res.send(new errors.BadRequestError(errMsg))
                    return false
                }
                offset = payload[occurrenceDbId]['offset']
            } else {
                continue
            }

            query = `
                SELECT
                    data_value::json
                FROM
                    experiment.occurrence_data
                WHERE
                    variable_id IN (
                        SELECT
                            id
                        FROM
                            master.variable
                        WHERE
                            abbrev = 'PLOT_CODE_PATTERN'
                    ) AND
                    is_void = FALSE AND
                    occurrence_id = ${occurrenceDbId}
                LIMIT
                    1
            `

            let occ = await sequelize.query(query, {
                type: sequelize.QueryTypes.SELECT
            })

            if (occ.length > 0) {
                pattern = occ[0]['data_value']

                let fromArr = []
                let selectArr = []

                for (let guide of pattern['pattern']) {
                    if (guide['is_variable']) {
                        if (
                            guide['entity_table'] == 'experiment.plot' ||
                            guide['entity_table'] == 'experiment.occurrence' ||
                            guide['entity_table'] == 'experiment.experiment'
                        ) {
                            selectArr.push(guide['entity_column'])
                        } else {
                            selectArr.push(guide['entity_column'])

                            const fromStr = `
                                LEFT JOIN
                                    ${guide['entity_table']}
                                ON
                                    ${guide['entity_table']}.${guide['entity_reference_column']} = ${guide['entity_reference_table']}
                            `

                            if (fromArr.includes(fromStr)) continue

                            fromArr.push(fromStr)
                        }
                    } else if (guide['value'] == 'COUNTER') {
                        let padding = ''
                        let counterOnly = ''

                        // This generates numbers wihout zero padding and number length restrictions (e.g. 501, 502, ..., 600, ..., 1000, ...)
                        if (!guide['zero_padding'] || !guide['digits']) {
                            counterOnly = `( ${offset - 1} + ROW_NUMBER() OVER (PARTITION BY occurrence.id ORDER BY plot.occurrence_id, plot.plot_number))::text`
                            selectArr.push(counterOnly)

                            continue // Move on to the next iteration
                        }

                        if (guide['zero_padding'] == 'leading') {
                            padding = 'lpad'
                        } else {
                            padding = 'rpad'
                        }

                        padding = padding + `(
                            ( ${offset - 1} + ROW_NUMBER() OVER (PARTITION BY occurrence.id ORDER BY plot.occurrence_id, plot.plot_number))::text, ` + parseInt(guide['digits']) + `, '0')`

                        selectArr.push(padding)
                    } else {
                        selectArr.push(`'${guide['value']}'`)
                    }
                }

                let selectStr = selectArr.join(',')
                let fromStr = ''

                if (fromArr.length > 0) {
                    fromStr = fromArr.join('')
                }

                let query = `
                    WITH rows AS (
                        UPDATE
                            experiment.plot
                        SET
                            plot_code = pattern.code
                        FROM (
                            SELECT
                                CONCAT(${selectStr}) AS code,
                                plot.id AS plot_id
                            FROM
                                experiment.plot
                                LEFT JOIN
                                    experiment.occurrence
                                ON
                                    occurrence.id = plot.occurrence_id
                                LEFT JOIN
                                    experiment.experiment
                                ON
                                    experiment.id = occurrence.experiment_id
                                ${fromStr}
                            WHERE
                                plot.is_void = FALSE AND
                                plot.occurrence_id = ${occurrenceDbId}
                        ) pattern
                        WHERE
                            plot.id = pattern.plot_id
                        RETURNING
                            1
                    )
                    SELECT
                        COUNT(1)
                    FROM
                        rows
                `

                let updateResult = await sequelize.query(query, {
                    type: sequelize.QueryTypes.UPDATE
                })

                updateCount += parseInt(updateResult[0][0]['count'])
            }
        }

        return updateCount
    }
}   