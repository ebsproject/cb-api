/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../config/sequelize')

module.exports = {
  getResponseCodes: async (code) => {
    let responseQuery = `
      SELECT
        *
      FROM
        api.messages
      WHERE
        code = (:code)
    `
    let responses = await sequelize.query(responseQuery, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        code: code
      }
    }).catch(err => {
      return
    })

    return responses
  },
  // Retrieve the error code given the error message
  getErrorCodebyMessage: async (httpCode, message) => {
    
    // Check if error message is in database
    let errorCodeQuery = `
      SELECT 
        code 
      FROM 
        api.messages 
      WHERE 
        message = '${message}'
    `
    let errorCodeArray = await sequelize.query(errorCodeQuery, {
      type: sequelize.QueryTypes.SELECT
    })

    if (errorCodeArray.length == 0) {
      errorCode = httpCode // default error code is the HTTP code
    } else {
      errorCode = errorCodeArray[0]['code']
    }
    return errorCode
  }
}