/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')
let { knex } = require('../../config/knex')
let validator = require('validator')

let logger = require('../logger/index')
let config = require('../config/index')
let errorBuilder = require('../error-builder')

let errors = require('restify-errors')
const { concat } = require('lodash')

// check if germplasm ID value is empty or undefined
async function germplasmIdHasNoValue(germplasmDbId) {
  return (germplasmDbId == undefined || germplasmDbId == null || germplasmDbId == '')
}

// check if germplasm ID value is an integer
async function germplasmIdHasInvalidFormat(germplasmDbId) {
  return !validator.isInt(germplasmDbId)
}

/**
 * Retrieve config-defined depth value
 * 
 * @returns depthValue
 */
async function getConfigDepthValue(){
  let depthValue = 6
  let configAbbrev = 'GERMPLASM_MANAGER_BG_PROCESSING_THRESHOLD'
  
  let configValues = await config.getConfigValueByAbbrev(configAbbrev)

  if( configValues !=undefined && configValues != [] &&
      configValues[0] != undefined && configValues[0]['ancestryExportDepth'] != undefined){
    depthValue = configValues[0]['ancestryExportDepth']
  }

  return depthValue
}

/**
 * Retrieve relation records with stored function
 * 
 * @param {*} germplasmDbId germplasm identifier
 * @param {*} depth relationship depth
 * 
 * @returns mixed
 */
async function getRelationRecordsWithDbFunction (germplasmDbId,depth) {
  let relations = []
  try {
    let germplasmRelationQuery =`
      SELECT 
        *
      FROM
        germplasm.get_parent_germplasm(${germplasmDbId})
    `
    let germplasmRelations = await sequelize.query(germplasmRelationQuery, {
      type: sequelize.QueryTypes.SELECT,
    })

    let relations = germplasmRelations ?? []

    relations = relations.map((x) => {
      x['depth'] = x['depth'] + depth
      return x
    })

    return relations
  } 
  catch (err) {
    return relations
  }
}

/**
 * Extract parent role and code from given order_number
 * @param {*} orderNumber germplasm relation order_number
 * 
 * @returns mixed
 */
async function getParentRoleAndCode(orderNumber){
  switch(parseInt(orderNumber)){
    case 1:
      parentGermplasmRole = 'Female'
      parentTypeCode = 'F'
      break;
    case 2:
      parentGermplasmRole = 'Male'
      parentTypeCode = 'M'
      break;
    default:
      parentGermplasmRole = ''
      parentTypeCode = ''
      break;
  } 

  return {
    parentGermplasmRole: parentGermplasmRole,
    parentTypeCode: parentTypeCode
  }
}

/**
 * 
 * @param {*} req request
 * @param {*} res result
 * @param {*} endpoint API call endpoint
 * @param {*} additionalCondition additional search filters
 * 
 * @returns mixed
 */
async function getGermplasmRecord (req, res, endpoint, additionalCondition) {
  let germplasmQuery = `
      SELECT 
          germplasm.id,
          germplasm.designation,
          germplasm.germplasm_code AS "germplasmCode",
          germplasm.generation AS "generation",
          germplasm.germplasm_type AS "germplasmType"
      FROM
          germplasm.germplasm germplasm
      WHERE
          germplasm.is_void = FALSE
          ${additionalCondition}
  `

  let germplasm = await sequelize.query(germplasmQuery, 
      {
          type: sequelize.QueryTypes.SELECT
      }
  ).catch(async err => {
      await logger.logFailingQuery(endpoint, 'SELECT', err)

      let errMsg = await errorBuilder.getError(req.headers.host, 500004)
      res.send(new errors.InternalError(errMsg))

      return null
  })

  return germplasm
}

/**
 * Check if has existing germplasm record
 * @param {Integer} germplasmDbId 
 * @returns {Boolean} exists
 */
async function hasGermplasmRecord(req, res, germplasmDbId) {
  // Check if germplasm exists
  let germplasmQuery = `
    SELECT EXISTS(
      SELECT 1 
      FROM 
        germplasm.germplasm g
      WHERE 
        g.id = ${germplasmDbId}
        AND g.is_void= FALSE
    )`

  // Retrieve germplasm record
  let germplasm = await sequelize.query(germplasmQuery, {
    type: sequelize.QueryTypes.SELECT
  })

  // If germplasm does not exist, return error
  if (await germplasm === undefined || await !germplasm[0]["exists"]) {
    return 0;
  }

  return germplasm[0]["exists"];
}

/**
 * Returns the value of the scale value given its abbrev
 * Based on scale id on variable table
 * @param {string} abbrev string abbreviation of the scale value
 * @return {string} value the string value of the scale value
 */
async function getScaleValueByAbbrev(abbrev) {
  try {
      let scaleValueQuery = `
          SELECT
              ARRAY_AGG(sv.value) AS "scaleValue"
          FROM
              master.variable v
          JOIN 
              master.scale_value sv ON sv.scale_id = v.scale_id AND sv.is_void = FALSE
          WHERE
              v.is_void = FALSE 
              AND v.abbrev = '${abbrev}'
      `
      // Retrieve scale values from the database
      let scaleValue = await sequelize.query(scaleValueQuery, {
          type: sequelize.QueryTypes.SELECT
      })
      .catch(async err => {
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      })

      // If scale value is empty
      if(scaleValue == []){
        let errMsg = await errorBuilder.getError(req.headers.host, 500004)
        res.send(new errors.InternalError(errMsg))
        return
      }

      return scaleValue[0]['scaleValue'] ?? []
      
  } catch (err) {
      return null
  }
}

module.exports = {

  normalizeText: async (designation) => {

    if(designation === undefined){
      return ''
    }
    
    // a) remove all spaces
    designation = designation.replace(/\s/g,'')

    // b) capitalize all letters
    tempStr = designation.toUpperCase()

    normalizedName = tempStr
    return normalizedName
  },

  normalizeTextComplex: async (designation) => {
    
    let normalizedName = ''
    let resultArray = []

    // a) capitalize all letters
    designation = designation.toUpperCase()

    // b) L( becomes L^( and )L becomes )^L IR64(BPH) becomes IR64 (BPH)
    // c) N( becomes N^( and )N becomes )^N IR64(5A) becomes IR64 (5A)
    let tempStr = designation.replace(
      /[(\(]/g, ' ('
    )
    tempStr = tempStr.replace(
      /([a-zA-Z])(\()/g,
      function (match, p1, p2, p3) {
        return p1 + " ("
      }
    )
    tempStr = tempStr.replace(
      /(\))([a-zA-Z])/g,
      function (match, p1, p2, p3) {
        return ") " + p2      }
    )

    //d) L. becomes L^ IR 64 SEL. becomes IR 64 SEL
    tempStr = tempStr.replace(
      /[\.]/g, ''
    )
    
    //e) LN becomes L^N EXCEPT SLN MALI105 becomes MALI 105 but MALI-F4 IS unchanged
    tempStr = tempStr.replace(
      /([^\-\''\[\]\+\.]|)([a-zA-Z])([0-9])/g,
      function (match, p1, p2, p3) {
        return p1 + p2 + " " + p3
      }
    )
    
    //f) NL becomes N^L EXCEPT SNL B 533A-1 becomes B 533 A-1 but B 533 A-4B is unchanged
    tempStr = tempStr.replace(
      /([^\-\''\[\]\+\.]|)([0-9])([a-zA-Z])/g,
      function (match, p1, p2, p3) {
        return p1 + p2 + " " + p3
      }
    )

    //g) LL-LL becomes LL^LL KHAO-DAWK-MALI 105 becomes KHAO DAWK MALI 105
    tempStr = tempStr.replace(
      /([a-zA-Z]{2,})?(-)([a-zA-Z]{2,})/g,
      function (match, p1, p2, p3) {
        return ((p1 !== undefined) ? p1 : '') + " " + p3
      }
    )
    
    //h) ^0N becomes ^N IRTP 00123 becomes IRTP 123
    let match = tempStr.match(/IR(\s*)[0-9]{2}(\s*)[a-zA-Z]{1}(\s*)[0-9]+/g)
    if (match == null) {
      tempStr = tempStr.replace(
        /(\s+)(0+)([0-9])/g,
        function (match, p1, p2, p3) {
          return p1+p3
        }
      )
    }

    //i) ^^ becomes ^
    tempStr = tempStr.replace(
      /(\s+)/g,' '
    )

    //j) REMOVE LEADING OR TRAILING ^
    tempStr = tempStr.trim()

    //k) ^) becomes ) and (^ becomes (
    tempStr = tempStr.replace(
      /(\s+)(\))/g,
      function (match, p1, p2, p3) {
        return p2
      }
    )

    tempStr = tempStr.replace(
      /(\()(\s+)/g,
      function (match, p1, p2, p3) {
        return p1
      }
    )

    //l) L­N becomes L^N when there is only one '–' in the name and L is not preceded by a space
    match = tempStr.match(/^[a-zA-Z0-9]+\-[0-9]+$/g)
    if (match !== null) {
      tempStr = tempStr.replace(
        /-/g,' '
      )
    }

    //m) ^/ becomes / and /^ becomes /
    tempStr = tempStr.replace(
      /(\s+)(\/)/g,
      function (match, p1, p2, p3) {
        return p2
      }
    )

    tempStr = tempStr.replace(
      /(\/)(\s+)/g,
      function (match, p1, p2, p3) {
        return p1
      }
    )
    
    normalizedName = tempStr
    return normalizedName
  },

  germplasmIdHasNoValue: async (germplasmDbId) => {
    return await germplasmIdHasNoValue(germplasmDbId);
  },

  germplasmIdHasInvalidFormat: async (germplasmDbId) => {
    return await germplasmIdHasInvalidFormat(germplasmDbId);
  },

  getGermplasmRecord: async (req, res, endpoint, additionalCondition) => {
    return await getGermplasmRecord (req, res, endpoint, additionalCondition)
  },

  getAncestryRelationRecordsQuery: async (germplasmDbId) => {

    if(await germplasmIdHasNoValue(germplasmDbId)){
      return ''
    }
    else if(await germplasmIdHasInvalidFormat(germplasmDbId)){
      return ''
    }
    else{
      return `
        WITH RECURSIVE tree(id, child, root, order_number, depth) AS (
          SELECT
              c.id,
              c.child_germplasm_id,
              c.parent_germplasm_id,
              c.order_number,
              1 AS depth
          FROM
              germplasm.germplasm_relation c
          WHERE 
              c.is_void = FALSE
              AND c.child_germplasm_id = ${germplasmDbId}
          
          UNION ALL
          
          SELECT
              germplasm_relation.id,
              child_germplasm_id,
              parent_germplasm_id,
              germplasm_relation.order_number,
              tree.depth + 1
          FROM
              tree
              INNER JOIN germplasm.germplasm_relation germplasm_relation
                  ON germplasm_relation.child_germplasm_id = tree.root
        )
      `
    }
  },

  /**
   * Facilitate retrival of ancestry records for pedigree or relations
   * 
   * @param {*} req request
   * @param {*} res results
   * @param {*} endpoint API call endpoint
   * @param {*} germplasmDbId germplasm identifier
   * @param {*} depth ancestry depth
   * @param {*} format needed format (pedigree / relations)
   * 
   * @returns mixed
   */
  getAncestryRelationRecordsWithDbFunction: async (req, res, endpoint, germplasmDbId, depth, format = 'pedigree') => {
    let childGermplasmDbId = germplasmDbId
    let childGermplasmDesignation = ''
    
    let ancTreeQueue = []
    let nodeData = {}
    
    let rootIndArr = []
    let germplasmRel = {}
    let rootArrIndx = 0

    let parentRoleAndCode = null
    let germplasmInfo
    let addParent = false

    rootIndArr[rootArrIndx] = [parseInt(childGermplasmDbId)]

    while(rootArrIndx < depth){
      rootIndArr[rootArrIndx+1] = [] // initialize arr for 1st set of parents
      rootIndArr[rootArrIndx+2] = [] // initialize arr for 2nd set of parents

      // for each item in depth
      for(let currentDbId of rootIndArr[rootArrIndx]){
        if(currentDbId == undefined || currentDbId == null){
          break
        }

        // get parents
        parentInfo = await getRelationRecordsWithDbFunction(currentDbId,rootArrIndx)

        // for each parent
        for(let parent of parentInfo){
          currentDbId = parent.child
          addParent = false

          if(germplasmRel[parent.child] == undefined){
            germplasmRel[parent.child] = []
          }

          if(germplasmRel[parent.child] !=undefined){
            germplasmRel[parent.child] = germplasmRel[parent.child].concat(parent)
          }          

          // add 1st set of parent
          if((parent.depth-rootArrIndx) == 1 && parent.depth <= depth){
            rootIndArr[rootArrIndx+1].push(parent.root) 
            addParent = true
          }

          // add 2nd set of parent
          if((parent.depth-rootArrIndx) == 2 && parent.depth <= depth ){
            rootIndArr[rootArrIndx+2].push(parent.root)
            addParent = true
          }

          if(addParent){
            // retrieve parent role and parent code
            parentRoleAndCode = await getParentRoleAndCode(parent.order_number)

            // retrieve parent designation info
            germplasmInfo = await getGermplasmRecord(req,res,endpoint,` AND germplasm.id = ${currentDbId} `)
            
            childGermplasmDesignation = ''
            if(germplasmInfo != null && germplasmInfo != undefined && germplasmInfo.length > 0){
              childGermplasmDesignation = germplasmInfo[0]['designation'] ?? ''
            }

            if(format == 'pedigree'){
              nodeData = { 
                childGermplasmDbId: parent.child,
                childGermplasmDesignation: childGermplasmDesignation,
                parentGermplasmDbId: parent.root,
                parentGermplasmRole: parentRoleAndCode.parentGermplasmRole ?? '',
                parentTypeCode: parentRoleAndCode.parentTypeCode ?? '',
                parentGermplasmDesignation: parent.designation,  
                lineName: parent.depth
              }
  
              // add to current tree
              ancTreeQueue.push(nodeData)
            }
          }
        }
      }
      rootArrIndx += 2 // move to next layer
    }

    let femParentInfo = {}
    let maleParentInfo = {}
    let params = ''

    if(format == 'relations'){
      for(let [key,values] of Object.entries(germplasmRel)){
        // retrieve parent designation info
        params = ` AND germplasm.id = ${key} `
        germplasmInfo = await getGermplasmRecord(req,res,endpoint,params)
        
        femParent = values.filter(x => x.order_number == 1);
        maleParent = values.filter(x => x.order_number == 2);

        if(femParent.length > 0){
          params = ` AND germplasm.id = ${femParent[0]['root']} `
          femParentInfo = await getGermplasmRecord(req,res,endpoint,params)
        }
        
        if(maleParent.length > 0){
          params = ` AND germplasm.id = ${maleParent[0]['root']} `
          maleParentInfo = await getGermplasmRecord(req,res,endpoint,params)
        }          

        nodeData = { 
          individualDbId: parseInt(key),
          individualDesignation: germplasmInfo[0]['designation'] ?? '',
          individualGermplasmCode: germplasmInfo[0]['germplasmCode'] ?? '',
          individualGeneration: germplasmInfo[0]['generation'] ?? '',
          individualGermplasmType: germplasmInfo[0]['germplasmType'] ?? '',
          femaleParentDbId: femParent[0] != undefined ? femParent[0]['root'] : '',
          femaleParentDesignation: femParent[0] != undefined ? femParent[0]['designation'] : '',
          femaleParentGermplasmCode: femParent[0] != undefined ? femParentInfo[0]['germplasmCode'] : '',
          femaleParentGeneration: femParent[0] != undefined ? femParentInfo[0]['generation'] : '',
          femaleParentGermplasmType: femParent[0] != undefined ? femParentInfo[0]['germplasmType'] : '',
          maleParentDbId: maleParent[0] != undefined ? maleParent[0]['root'] : '',
          maleParentDesignation: maleParent[0] != undefined ? maleParent[0]['designation'] : '',
          maleParentGermplasmCode: maleParent[0] != undefined ? maleParentInfo[0]['germplasmCode'] : '',
          maleParentGeneration: maleParent[0] != undefined ?  maleParentInfo[0]['generation'] : '',
          maleParentGermplasmType: maleParent[0] != undefined ? maleParentInfo[0]['germplasmType'] : '',
          depth: femParent[0]['depth'] ?? (maleParent[0]['depth'] ?? '')
        }

        ancTreeQueue.push(nodeData)
        
      }
    }

    // sort ancestry tree
    ancTreeQueue = ancTreeQueue.sort((a,b) => { return a.depth > b.depth ? 1 : -1 })
    
    return ancTreeQueue    
  },

  /**
   * Retrieve config-defined depth value
   * 
   * @returns depthValue
   */
  getConfigDepthValue: async () => {
    return await getConfigDepthValue();
  },

  /**
     * Returns the value of the scale value
     * given its abbrev
     * @param {string} abbrev string abbreviation of the scale value
     * @return {string} value the string value of the scale value
  */
  getScaleValueByAbbrev: async (abbrev) => {
    return await getScaleValueByAbbrev(abbrev)   
  },

  hasGermplasmRecord: async (req, res, germplasmDbId) => {
    return await hasGermplasmRecord(req, res, germplasmDbId)
  }
}