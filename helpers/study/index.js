/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize, Study } = require("../../config/sequelize");
let Sequelize = require("sequelize");
let Op = Sequelize.Op;

module.exports = {
    /**
     * Check if the study is committed
     *
     * @param studyDbId integer ID of the study
     * @return text source model
     */
    isCommitted: async studyDbId => {
        // Get count of the study given the id and status with committed

        let countQuery = `
            SELECT 
                count(1)
            FROM 
                operational.study
            WHERE 
                id = ?
            AND study_status ILIKE '%committed study%'
        `;

        let count = await sequelize.query(countQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: [studyDbId]
        });

        if (count[0].count < 1) {
            // Study is not committed
            return false;
        } else {
            // Study is committed
            return true;
        }
    },

    /**
     * Retrieve the sql query for getting the study record
     *
     * @param addedConditionString string added condition string for study
     * @param addedColumnString string added columns for study
     *
     * @return sql query string
     */
    getStudyQuerySql: async (
        addedConditionString = "",
        addedColumnString = "",
        addedDistinctString = "",
        distinctColumn = "",
        isExperiment = false
    ) => {
        let studyQuery = `SELECT`;
        if (isExperiment) {
            studyQuery += `
                ${addedDistinctString}
                study.id AS "experimentLocationDbId",
                experiment.id AS "experimentDbId",
                experiment.experiment_name AS "experiment",
                study.key,
                study.study,
                program.id AS "programDbId",
                program.display_name AS "program",
                program.abbrev AS "programAbbrev",
                place.id AS "siteDbId",
                place.display_name AS "site",
                country.name AS "country",
                place.abbrev AS "siteAbbrev",
                phase.id AS "phaseDbId",
                phase.display_name AS "phase",
                phase.abbrev AS "phaseAbbrev",
                study.year,
                season.id AS "seasonDbId",
                season.name AS "season",
                season.abbrev AS "seasonAbbrev",
                study.name,
                study.title,
                study.study_status AS "studyStatus",
                study.study_group_id AS "studyGroupId",
                study.study_type AS "studyType",
                study.design,
                author.id AS "stewardDbId",
                author.display_name AS "steward",
                study.ecosystem,
                study.establishment,
                study.fld_loc AS "fieldLocation",
                study.entry_count AS "entryCount",
                study.plot_count AS "plotCount",
                study.rep_count AS "repCount",
                "processPath".id AS "processPathDbId",
                "processPath".display_name AS "processPath",
                study.remarks,
                study.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.display_name AS "creator",
                study.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.display_name AS "modifier"
            `;
        } else {
            studyQuery += `
                ${addedDistinctString}
                study.id AS "studyDbId",
                study.key,
                study.study,
                study.access_data AS "accessData",
                program.id AS "programDbId",
                program.display_name AS "program",
                program.abbrev AS "programAbbrev",
                place.id AS "placeDbId",
                place.display_name AS "place",
                place.abbrev AS "placeAbbrev",
                country.name AS "country",
                phase.id AS "phaseDbId",
                phase.display_name AS "phase",
                phase.abbrev AS "phaseAbbrev",
                study.year,
                season.id AS "seasonDbId",
                season.name AS "season",
                season.abbrev AS "seasonAbbrev",
                study.name,
                study.title,
                study.study_status AS "studyStatus",
                study.study_group_id AS "studyGroupId",
                study.study_type AS "studyType",
                study.design,
                experiment.id AS "experimentDbId",
                experiment.experiment_name AS "experiment",
                author.id AS "authorDbId",
                author.display_name AS "author",
                study.ecosystem,
                study.establishment,
                study.fld_loc AS "fieldLocation",
                study.entry_count AS "entryCount",
                study.plot_count AS "plotCount",
                study.rep_count AS "repCount",
                "processPath".id AS "processPathDbId",
                "processPath".display_name AS "processPath",
                study.remarks,
                study.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.display_name AS "creator",
                study.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.display_name AS "modifier"
            `;
        }

        studyQuery +=
            addedColumnString +
            `
            FROM operational.study study
            LEFT JOIN 
                master.program program ON program.id = study.program_id
            LEFT JOIN 
                master.place place ON place.id = study.place_id
            LEFT JOIN 
                master.phase phase ON phase.id = study.phase_id
            LEFT JOIN 
                master.season season ON season.id = study.season_id
            LEFT JOIN 
                master.user author ON author.id = study.author_id::integer
            LEFT JOIN 
                master.user creator ON creator.id = study.creator_id::integer
            LEFT JOIN 
                master.user modifier ON modifier.id = study.modifier_id::integer
            LEFT JOIN 
                master.item "processPath" ON "processPath".id = study.process_path_id
            LEFT JOIN
                operational.experiment experiment ON experiment.id = study.experiment_id
            LEFT JOIN 
                master.geolocation geolocation ON geolocation.id = place.geolocation_id
            LEFT JOIN 
                master.country country ON country.id = geolocation.country_id
            WHERE 
                study.is_void = FALSE
        ` +
            addedConditionString +
            `   ORDER BY
                    ${distinctColumn}
                    study.id
                `;
            
        return studyQuery;
    }
};
