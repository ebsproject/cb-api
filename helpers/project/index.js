/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op

module.exports = {

    /**
     * Retrieve the sql query for getting the project record
     * 
     * @param addedConditionString string added condition string for project
     * @param addedColumnString string added columns for project
     * 
     * @return sql query string
     */
    getProjectQuerySql: async (addedConditionString = '', addedColumnString = '') => {

        let projectQuery = `
            SELECT
                project.id AS "projectDbId",
                project.name AS "project",
                project.abbrev,
                project.description,
                program.id AS "programDbId",
                program.name AS "program",
                program.abbrev AS "programAbbrev",
                project.remarks,
                project.status,
                leader.display_name AS leader,
                leader.id AS "leaderDbId",
                project.creation_timestamp AS "creationTimestamp",
                creator.display_name AS creator,
                creator.id AS "creatorDbId",
                project.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.display_name AS modifier 
                ` + addedColumnString + `
            FROM 
                master.project project
            LEFT JOIN 
                master.user creator ON project.creator_id = creator.id
            LEFT JOIN 
                master.user modifier ON project.modifier_id = modifier.id
            LEFT JOIN 
                master.user leader ON project.leader_id = leader.id
            LEFT JOIN
                master.program program ON project.program_id = program.id
            WHERE 
                project.is_void = FALSE 
                `   + addedConditionString + `
            ORDER BY
                project.id
        `

        return projectQuery
    }, 

    /**
     * Retrieve the projectDbId.
     * 
     * Check first if the data is existing already.
     * If not existing, a record will be created.
     * 
     * @param abbrev string unique identifier of the scheme
     * @param name string name of the scheme
     * @param description string description of the scheme
     * @param status string can be either active or inactive, default is active
     * @param leaderDbId integer ID of the project leader
     * @param userDbId integer ID of the user
     * 
     * @return projectDbId integer
     */
    getProjectDbId: async (programDbId, abbrev, name, description, status, leaderDbId, userDbId) => {

        // Default
        let projectDbId = null

        // Get the project
        let projectQuery = `
            SELECT
                id AS "projectDbId"
            FROM
                master.project project
            WHERE
                project.is_void = FALSE
                AND abbrev = :abbrev
                AND name = :name
        `

        // Retrieve project from the database   
        let project = await sequelize.query(projectQuery,{
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                abbrev: abbrev,
                name: name
            }
        })
        .catch(err => {
            return null
        })

        if(project.length < 1){
            // Create new record
            let createProjectQuery = `
                INSERT INTO
                    master.project
                    (program_id, abbrev, name, description, status, leader_id, creation_timestamp, creator_id, notes)
                VALUES
                    (:programDbId, :abbrev, :name, :description, :status, :leaderDbId, NOW(), :userDbId, 'Created via CB API')
                RETURNING 
                        id
            `

            await sequelize.query(createProjectQuery,{
                type: sequelize.QueryTypes.INSERT,
                replacements: {
                    programDbId: programDbId,
                    abbrev: abbrev,
                    name: name,
                    description: description,
                    status: status,
                    leaderDbId: leaderDbId,
                    userDbId: userDbId
                }
            })
            .then(function (projectArray) {
                // Get the created ID
                projectDbId =  projectArray[0][0]['id']
            })
            .catch(err => {
                return null
            })

        } else {
            // Retrieve the record
            projectDbId = project[0]['projectDbId']
        }

        return projectDbId
    }
}