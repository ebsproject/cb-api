/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')

module.exports = {

    isAdmin: async (id) => {
        let personQuery = `
            SELECT
                person.person_type
            FROM 
                tenant.person person
            WHERE
                person.id = ${id}
        `

        let person = await sequelize.query(personQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        if (await person !== undefined && await person[0].person_type == 'admin') {
            return true
        }
        else {
            return false
        }
    }
}