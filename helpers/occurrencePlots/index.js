/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op

module.exports = {
	/**
	 * Retrieve the sql query for getting the entries of an occurrence
	 * @param occurenceDbId integer occurrence identifier
	 * @param addedDistinctString strig distinct string
	 * @param distinctColumn string distinct column
	 * @param responseColString string column values
	 * 
	 * @return sql query string
	 */
	getOccurrencePlotsQuerySql: async (occurrenceDbId = 0, addedDistinctString = '', distinctColumn = '', responseColString) => {

		let occurrencePlotsQuery = `
			SELECT
				${addedDistinctString}
				${responseColString}
			FROM
				experiment.plot plot
			LEFT JOIN
				experiment.entry entry
			ON
				plot.entry_id = entry.id
			LEFT JOIN
				germplasm.seed seed
			ON
				seed.id = entry.seed_id
			LEFT JOIN
				experiment.planting_instruction pi
			ON 
				plot.id = pi.plot_id
				and pi.is_void = false
			LEFT JOIN LATERAL (
				SELECT
					exptdes.*
				FROM
					experiment.experiment_design AS exptdes
				WHERE
					exptdes.plot_id = plot.id
					AND exptdes.block_name = 'RowBlock'
					AND exptdes.is_void = FALSE
			) AS descol1
				ON TRUE
			LEFT JOIN LATERAL (
				SELECT
					exptdes.*
				FROM
					experiment.experiment_design AS exptdes
				WHERE
					exptdes.plot_id = plot.id
					AND exptdes.block_name = 'ColBlock'
					AND exptdes.is_void = FALSE
			) AS descol2
				ON TRUE
			LEFT JOIN
				germplasm.package package
			ON
				package.id = entry.package_id
			JOIN 
				tenant.person creator 
			ON 
				creator.id = plot.creator_id::integer
			JOIN
				germplasm.germplasm g
			ON
				entry.germplasm_id = g.id
			LEFT JOIN 
				tenant.person modifier
			ON
				modifier.id = plot.modifier_id::integer
			WHERE
				plot.occurrence_id = ` + occurrenceDbId + `
				AND plot.is_void = false
			ORDER BY
				${distinctColumn}
				plot.plot_number
		`

		return occurrencePlotsQuery
	}
}