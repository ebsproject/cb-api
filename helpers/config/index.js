/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let Sequelize = require('sequelize')

module.exports = {

    /**
     * Retrieve the config value based on abbrev
     * @param String configAbbrev 
     */
    getConfigValueByAbbrev: async (configAbbrev) => {
        let configQuery = `
            SELECT 
                config_value
            FROM
                platform.config
            WHERE
                abbrev ='${configAbbrev}'
                AND is_void = false
        `
        let configValue = await sequelize.query(configQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return configValue[0]['config_value']
    }
}
