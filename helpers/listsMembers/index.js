/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')
let { knex } = require('../../config/knex')
let errorBuilder = require('../error-builder.js')
let errors = require('restify-errors')
let logger = require('../logger')

const endpoint = 'helpers/listsMembers'

/**
 * Previous query for retrieval of package list member records
 * @param {*} memberQuery Additional query parameters
 * @param {*} listDbId list ID
 * @param {*} getDuplicateQuery 
 * @returns 
 */
async function getOldPackageListQuery(memberQuery,listDbId,getDuplicateQuery) {
    if (memberQuery == null) {
        memberQuery = `
            SELECT
                "listMember".id AS "listMemberDbId",
                "listMember".order_number AS "orderNumber",
                "listMember".creation_timestamp AS "creationTimestamp",
                "listMember".display_value AS "displayValue",
                "listMember".is_active AS "isActive",
                "listMember".remarks,
                experiment.experiment_name AS "experiment",
                experiment.experiment_name AS "experimentName",
                experiment.id AS "experimentDbId",
                experiment.experiment_year AS "experimentYear",
                experiment.experiment_type AS "experimentType",
                stage.id AS "experimentStageDbId",
                stage.stage_name AS "experimentStage",
                stage.stage_code AS "experimentStageCode",
                occurrence.occurrence_name AS "experimentOccurrence",
                occurrence.id AS "sourceOccurrenceDbId",
                occurrence.occurrence_name AS "sourceOccurrenceName",
                program.program_code AS "seedManager",
                program.program_code AS "program",
                program.id AS "programDbId",
                seed.id AS "seedDbId",    
                seed.seed_name AS "seedName",
                seed.seed_code AS "GID",
                seed.seed_code AS "seedCode",
                seed.program_id AS "seedProgramDbId",
                (
                    SELECT
                        program.program_code 
                    FROM 
                        tenant.program program
                    WHERE 
                        program.is_void = FALSE AND
                        program.id = seed.program_id
                    LIMIT 1
                ) AS "seedProgram",
                germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                germplasm.other_names AS "otherNames",
                germplasm.designation,
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.parentage,
                germplasm.id AS "germplasmDbId",
                germplasm.designation AS "germplasmName",
                "location".id AS "locationDbId",
                "location".location_code AS "location",
                "location".location_name AS "sourceStudyName",
                season.id AS "sourceStudySeasonDbId",
                season.season_code AS "sourceStudySeason",
                "entry".entry_code AS "sourceEntryCode",
                "entry".entry_number AS "seedSourceEntryNumber",
                plot.plot_code AS "seedSourcePlotCode",
                plot.plot_number AS "seedSourcePlotNumber",
                plot.rep AS "replication",
                seed.harvest_date AS "harvestDate",
                (
                    SELECT 
                        EXTRACT (YEAR FROM seed.harvest_date)
                ) AS "sourceHarvestYear",
                package.id AS "packageDbId",
                package.package_code AS "packageCode",
                package.package_label AS "label",
                package.package_quantity AS "quantity",
                package.package_unit AS "unit",
                (
                    SELECT
                        data_value 
                    FROM 
                        germplasm.package_data
                    WHERE 
                        is_void=FALSE AND
                        package_id = package.id AND
                        variable_id = (SELECT id FROM master.variable WHERE abbrev = 'MC_CONT' AND is_void = false)
                    LIMIT 1
                ) AS "moistureContent",
                (
                    SELECT
                        data_value 
                    FROM 
                        germplasm.package_data
                    WHERE 
                        is_void=FALSE AND
                        package_id = package.id AND
                        variable_id = (SELECT id FROM master.variable WHERE abbrev = 'RSHT_NO' AND is_void = false)
                    LIMIT 1
                ) AS "rsht",
                (
                    SELECT
                        data_value 
                    FROM 
                        germplasm.package_data
                    WHERE 
                        is_void=FALSE AND
                        package_id = package.id AND
                        variable_id = (SELECT id FROM master.variable WHERE abbrev = 'GENETIC_STOCK' AND is_void = false)
                    LIMIT 1
                ) AS "geneticStock",
                (
                    SELECT
                        data_value 
                    FROM 
                        germplasm.package_data
                    WHERE 
                        is_void=FALSE AND
                        package_id = package.id AND
                        variable_id = (SELECT id FROM master.variable WHERE abbrev = 'ORIGIN' AND is_void = false)
                    LIMIT 1
                ) AS "origin",
                package.package_quantity AS "packageQuantity",
                facility.facility_name AS "facility",
                container.facility_name AS "container",
                subFacility.facility_name AS "subFacility",
                taxonomy.id AS "taxonomyDbId",
                taxonomy.taxonomy_name AS "taxonomyName"
                ${getDuplicateQuery}
        `
    }
    memberQuery += `
        FROM        
            platform.list_member "listMember"
            INNER JOIN
                germplasm.package package
            ON package.id = "listMember".data_id
                AND package.is_void = FALSE
            LEFT JOIN 
                tenant.program program ON program.id = package.program_id
            LEFT JOIN 
                place.facility container ON container.id = package.facility_id
            LEFT JOIN 
                place.facility subFacility ON subFacility.id = container.parent_facility_id
            LEFT JOIN 
                place.facility facility ON facility.id = container.root_facility_id
            JOIN 
                germplasm.seed seed ON seed.id = package.seed_id
            JOIN 
                germplasm.germplasm germplasm ON germplasm.id = seed.germplasm_id                    
            LEFT JOIN
                germplasm.taxonomy taxonomy ON taxonomy.id = germplasm.taxonomy_id
            LEFT JOIN 
                experiment.location "location" ON "location".id = seed.source_location_id
            LEFT JOIN 
                experiment.experiment experiment ON experiment.id = seed.source_experiment_id
            LEFT JOIN 
                experiment.plot plot ON plot.id = seed.source_plot_id
            LEFT JOIN 
                experiment.entry "entry" ON "entry".id = seed.source_entry_id
            LEFT JOIN 
                experiment.occurrence occurrence ON occurrence.id = seed.source_occurrence_id
            LEFT JOIN
                tenant.season season ON season.id = experiment.season_id
            LEFT JOIN
                tenant.stage stage ON stage.id = experiment.stage_id
            LEFT JOIN 
                tenant.person creator ON creator.id = package.creator_id
            LEFT JOIN 
                tenant.person modifier ON modifier.id = package.modifier_id
        WHERE
            "listMember".list_id = ${listDbId}
            AND "listMember".is_void = FALSE
            AND germplasm.is_void = FALSE
            AND package.is_void = FALSE
        ORDER BY
            "listMember".order_number
    `

    return memberQuery
}

/**
 * Updated query for retrieval of package list member records
 * @param {Integer} listDbId list ID
 * @param {String} memberQuery Additional query parameters
 * @param {Boolean} isBasic use trimmed down query
 * @param {Boolean} hasData add data info to trimmed down query
 * @returns 
 */
async function getPackageListQuery(listDbId, memberQuery = '', isBasic = false, hasData = false) {
    let additionalExperimentSelect = `
        ,
        experiment.*,
        occurrence.*,
        season.*,
        stage.*,
        location.*,
        entry.*,
        plot.*
    `

    let experimentCTEs = `
        experiment AS (
            SELECT
                experiment.experiment_name AS "experiment",
                experiment.experiment_name AS "experimentName",
                experiment.id AS "experimentDbId",
                experiment.experiment_year AS "experimentYear",
                experiment.experiment_type AS "experimentType",
                experiment.season_id AS "experimentSeasonDbId",
                experiment.stage_id AS "experimentStageDbId"
            FROM
                experiment.experiment experiment
            WHERE
                experiment.is_void = FALSE
        ),
        occurrence AS (
            SELECT
                occurrence.occurrence_name AS "experimentOccurrence",
                occurrence.id AS "sourceOccurrenceDbId",
                occurrence.occurrence_name AS "sourceOccurrenceName"
            FROM
                experiment.occurrence occurrence
            WHERE
                occurrence.is_void = FALSE
        ),
    `

    let additionalExperimentJoins = `
        LEFT JOIN experiment experiment ON experiment."experimentDbId" = sd."seedExperimentDbId"
        LEFT JOIN occurrence occurrence ON occurrence."sourceOccurrenceDbId" = sd."seedOccurrenceDbId"
        LEFT JOIN LATERAL (
            SELECT
                ss.id AS "sourceStudySeasonDbId",
                ss.season_code AS "sourceStudySeason"
            FROM
                tenant.season ss
            WHERE
                ss.is_void = FALSE
                AND ss.id = experiment."experimentSeasonDbId"
        ) season ON TRUE
        LEFT JOIN LATERAL (
            SELECT
                sg.id AS "experimentStageDbId",
                sg.stage_name AS "experimentStage",
                sg.stage_code AS "experimentStageCode"
            FROM
                tenant.stage sg
            WHERE
                sg.is_void = FALSE
                AND sg.id = experiment."experimentStageDbId"
        ) stage ON TRUE        
        LEFT JOIN LATERAL (
            SELECT
                "location".id AS "locationDbId",
                "location".location_code AS "location",
                "location".location_name AS "sourceStudyName"
            FROM
                experiment.location "location"
            WHERE
                "location".is_void = FALSE
                AND "location".id = sd."seedLocationDbId"
            LIMIT 1
        ) location ON TRUE
        LEFT JOIN LATERAL (
            SELECT
                "entry".entry_code AS "sourceEntryCode",
                "entry".entry_number AS "seedSourceEntryNumber"
            FROM
                experiment.entry "entry"
            WHERE
                "entry".is_void = FALSE
                AND "entry".id = sd."seedSourceEntryDbId"
            LIMIT 1
        ) entry ON TRUE
        LEFT JOIN LATERAL (
            SELECT
                plot.plot_code AS "seedSourcePlotCode",
                plot.plot_number AS "seedSourcePlotNumber",
                plot.rep AS "replication"
            FROM
                experiment.plot plot
            WHERE
                plot.is_void = FALSE
                AND plot.id = sd."seedSourcePlotDbId"
            LIMIT 1
        ) plot ON TRUE
    `

    let additionalAllData = `
        ,
        rshtReference."dataValue" AS "rsht",
        gemrplasmMta.*,
        mc."dataValue" AS "moistureContent",
        og."dataValue" AS "origin"
    `

    let additionalJoins = `
        LEFT JOIN rshtReference rshtReference ON rshtReference."sourceSeedDbId" = sd."seedDbId"
        LEFT JOIN LATERAL (
            SELECT
                STRING_AGG( DISTINCT germplasm_mta.mta_status::text, ',') AS "mtaStatus",
                germplasm_mta.mls_ancestors AS "mlsAncestors",
                germplasm_mta.genetic_stock AS "geneticStock"
            FROM
                germplasm.germplasm_mta germplasm_mta
            WHERE
                germplasm_mta.is_void = FALSE
                AND sd."seedDbId" = germplasm_mta.seed_id 
                AND sd."seedGermplasmDbId" = germplasm_mta.germplasm_id
            GROUP BY germplasm_mta.seed_id, germplasm_mta.germplasm_id, germplasm_mta.mls_ancestors, germplasm_mta.genetic_stock
            LIMIT 1
        )gemrplasmMta ON TRUE
        LEFT JOIN  moistureContent mc ON mc."sourcePackageDbId" = pk."packageDbId"
        LEFT JOIN origin og ON og."sourceSeedDbId" = pk."packageDbId"
        LEFT JOIN shuRefNumber srn ON srn."sourceSeedDbId" = sd."seedDbId"
    `
    let fullQuery = `
        WITH listMembers AS (
            SELECT
                "listMember".id AS "listMemberDbId",
                "listMember".order_number AS "orderNumber",
                "listMember".display_value AS "displayValue",
                "listMember".data_id AS "dataDbId",
                "listMember".is_active AS "isActive",
                "listMember".is_void AS "isVoid",
                "listMember".remarks,
                "listMember".creation_timestamp AS "creationTimestamp"
            FROM
                platform.list_member "listMember"
            WHERE
                "listMember".list_id = ${listDbId}
                AND "listMember".is_active = TRUE
                AND "listMember".is_void = FALSE
        ),
        packages AS (
            SELECT
                package.id AS "packageDbId",
                package.package_code AS "packageCode",
                package.package_label AS "label",
                package.package_quantity AS "quantity",
                package.package_unit AS "unit",
                package.is_void AS "isVoided",
                package.seed_id AS "packageSeedDbId",
                package.program_id AS "packageProgramDbd",
                container.*,
                subFacility.*,
                facility.*
                
            FROM
                germplasm.package "package"
                LEFT JOIN LATERAL (
                    SELECT
                        pf.facility_name AS "container",
                        pf.parent_facility_id AS "parentFacilityDbId",
                        pf.root_facility_id AS "rootFacilityDbId"
                    FROM
                        place.facility pf
                    WHERE   
                        pf.id = "package".facility_id
                        AND pf.is_void = FALSE
                ) container ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        fc.facility_name AS "subFacility"
                    FROM
                        place.facility fc
                    WHERE   
                        fc.id = container."parentFacilityDbId"
                        AND fc.is_void = FALSE
                ) subFacility ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        ff.facility_name AS "facility"
                    FROM
                        place.facility ff
                    WHERE   
                        ff.id = container."rootFacilityDbId"
                        AND ff.is_void = FALSE
                ) facility ON TRUE
            WHERE
                "package".is_void = FALSE
        ),
        seed AS (
            SELECT
                seed.id AS "seedDbId",    
                seed.seed_name AS "seedName",
                seed.seed_code AS "GID",
                seed.seed_code AS "seedCode",
                EXTRACT (YEAR FROM seed.harvest_date) AS "sourceHarvestYear",
                seed.program_id AS "seedProgramDbId",
                seed.germplasm_id AS "seedGermplasmDbId",
                seed.source_experiment_id AS "seedExperimentDbId",
                seed.source_occurrence_id AS "seedOccurrenceDbId",
                seed.source_location_id AS "seedLocationDbId",
                seed.source_entry_id AS "seedSourceEntryDbId",
                seed.source_plot_id AS "seedSourcePlotDbId",
                seed.is_void AS "isVoid"
            FROM
                germplasm.seed seed
            WHERE
                seed.is_void = FALSE
        ),
        rshtReference AS (
            SELECT
                "seedData".seed_id AS "sourceSeedDbId",
                "data".data_value AS "dataValue",
                "seedData".is_void AS "isVoid"
            FROM 
                germplasm.seed_data "seedData" 
                LEFT JOIN LATERAL (
                    SELECT 
                        "seedData".data_value
                    FROM
                        master.variable "variable" 
                    WHERE            
                        "variable".abbrev = 'RSHT_NO'
                        AND "seedData".variable_id = "variable".id
                        AND "variable".is_void = FALSE
                    LIMIT 1
                ) "data" ON TRUE
            LIMIT 1
        ),
        shuRefNumber AS (
            SELECT
                "seedeData".id AS "sourceSeedDbId",
                "data".data_value AS "dataValue",
                "seedeData".is_void AS "isVoid"
            FROM 
                germplasm.seed_data "seedeData" 
                LEFT JOIN LATERAL (
                    SELECT 
                        "seedeData".data_value
                    FROM
                        master.variable "variable" 
                    WHERE            
                        "variable".abbrev = 'SHU_REFERENCE_NUMBER'
                        AND "seedeData".variable_id = "variable".id
                        AND "variable".is_void = FALSE
                    LIMIT 1
                ) "data" ON TRUE
            LIMIT 1
        ),
        origin AS (
            SELECT
                "seedData".id AS "sourceSeedDbId",
                "data".data_value AS "dataValue",
                "seedData".is_void AS "isVoid"
            FROM 
                germplasm.seed_data "seedData" 
                LEFT JOIN LATERAL (
                    SELECT 
                        "seedData".data_value
                    FROM
                        master.variable "variable" 
                    WHERE            
                        "variable".abbrev = 'ORIGIN'
                        AND "seedData".variable_id = "variable".id
                        AND "variable".is_void = FALSE
                    LIMIT 1
                ) "data" ON TRUE
            LIMIT 1
        ),
        moistureContent AS (
            SELECT
                "packageData".package_id AS "sourcePackageDbId",
                "data".data_value AS "dataValue",
                "packageData".is_void AS "isVoid"
            FROM 
                germplasm.package_data "packageData" 
                LEFT JOIN LATERAL (
                    SELECT 
                        "packageData".data_value
                    FROM
                        master.variable "variable" 
                    WHERE            
                        "variable".abbrev = 'MC_CONT'
                        AND "packageData".variable_id = "variable".id
                        AND "variable".is_void = FALSE
                    LIMIT 1
                ) "data" ON TRUE
            LIMIT 1
        ),
        ${ isBasic && hasData ? experimentCTEs : '' }
        allData AS (
            SELECT 
                listMembers.*,
                pk.*,
                program.*,
                sd.*,
                germplasm.*,
                taxonomy.id AS "taxonomyDbId",
                taxonomy.taxonomy_name AS "taxonomyName"
                ${ isBasic == true && hasData == true ? additionalAllData: '' }
                ${ isBasic && hasData ? additionalExperimentSelect : '' }
            FROM
                listMembers listMembers
                JOIN packages pk ON pk."packageDbId" = listMembers."dataDbId" AND pk."isVoided" = FALSE
                LEFT JOIN LATERAL (
                    SELECT
                        pg.program_code AS "seedManager",
                        pg.id AS "programDbId"
                    FROM
                        tenant.program pg
                    WHERE
                        pg.id = pk."packageProgramDbd" 
                        AND pg.is_void = FALSE
                ) program ON TRUE
                LEFT JOIN seed sd ON sd."seedDbId" = pk."packageSeedDbId" AND sd."isVoid" = FALSE
                LEFT JOIN LATERAL (
                    SELECT
                        gm.germplasm_normalized_name AS "germplasmNormalizedName",
                        gm.other_names AS "otherNames",
                        gm.designation,
                        gm.germplasm_code AS "germplasmCode",
                        gm.parentage,
                        gm.id AS "germplasmDbId",
                        gm.designation AS "germplasmName",
                        gm.taxonomy_id AS "taxonomyDbId"
                    FROM
                        germplasm.germplasm gm
                    WHERE
                        gm.id = sd."seedGermplasmDbId" 
                        AND gm.is_void = FALSE
                    LIMIT 1
                ) germplasm ON TRUE
                LEFT JOIN germplasm.taxonomy taxonomy ON taxonomy.id = germplasm."taxonomyDbId"
                ${ isBasic == true && hasData == true ? additionalJoins : '' }
                ${ isBasic && hasData ? additionalExperimentJoins : '' }
            ORDER BY
                listMembers."orderNumber"
        )
        SELECT
            *
        FROM
            allData
    `

    return fullQuery
}

module.exports = {
    getMembersQuery: (listDbId, type, fields, getDuplicates = false, isBasic = false, hasData = false) => {
        let memberQuery = null
        let getDuplicateQuery = ``

        if (getDuplicates) {
            getDuplicateQuery = `
                , row_number() over(partition by (
                    "listMember".list_id, "listMember".data_id, "listMember".is_active, "listMember".is_void) order by "listMember".creation_timestamp
                ) AS "rowNumber"
            `
        }

        if (fields != null) {
            memberQuery = knex.column(fields.split('|'))
            if (getDuplicates) {
                memberQuery += getDuplicateQuery
            }
        }

        // check the type of list
        if (type.includes('trait') || type.includes('management') || type == 'variable') {
            if (memberQuery == null) {
                memberQuery = `
                    SELECT
                        "listMember".id AS "listMemberDbId",
                        "listMember".order_number AS "orderNumber",
                        "listMember".creation_timestamp AS "creationTimestamp",
                        "listMember".display_value AS "displayValue",
                        "listMember".is_active AS "isActive",
                        "listMember".remarks,
                        variable.id AS "variableDbId",
                        variable.abbrev,
                        variable.label,
                        variable.name,
                        variable.display_name AS "displayName",
                        variable.type,
                        variable.data_type AS "dataType",
                        variable.data_level AS "dataLevel",
                        (SELECT '(' || STRING_AGG (value, ', ' ORDER BY value) || ')'
                            FROM master.scale_value 
                            WHERE scale_id = variable.scale_id AND is_void = false
                            GROUP BY scale_id
                            ORDER BY scale_id
                        ) AS "scaleValue"
                        ${getDuplicateQuery}
                `
            }
            memberQuery += `
                FROM
                    platform.list_member "listMember"
                    LEFT JOIN 
                        master.variable variable ON variable.id = "listMember".data_id
                        AND variable.is_void = FALSE
                WHERE
                    "listMember".is_void = FALSE
                    AND "listMember".list_id = ${listDbId}
                ORDER BY
                    "listMember".order_number
            `
        } else if (type == 'designation' || type == 'germplasm') {
            if (memberQuery == null) {
                memberQuery = `
                    SELECT
                        "listMember".id AS "listMemberDbId",
                        "listMember".order_number AS "orderNumber",
                        "listMember".creation_timestamp AS "creationTimestamp",
                        "listMember".display_value AS "displayValue",
                        "listMember".is_active AS "isActive",
                        "listMember".remarks,
                        germplasm.id AS "germplasmDbId",
                        germplasm.designation,
                        germplasm.parentage,
                        germplasm.generation,
                        germplasm.germplasm_code AS "germplasmCode",
                        germplasm.germplasm_state AS "germplasmState",
                        germplasm.germplasm_name_type AS "germplasmNameType",
                        germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                        germplasm.other_names AS "otherNames",
                        germplasm.designation AS "germplasmName"
                        ${getDuplicateQuery}
                `
            }
            memberQuery += `
                FROM
                    platform.list_member "listMember",
                    germplasm.germplasm germplasm
                WHERE
                    "listMember".is_void = FALSE
                    AND germplasm.is_void = FALSE
                    AND "listMember".data_id = germplasm.id
                    AND "listMember".list_id = ${listDbId}
                GROUP BY
                    "listMember".id, germplasm.id
                ORDER BY "listMember".order_number
            `
        } else if (type == 'seed') {
            if (memberQuery == null) {
                memberQuery = `
                    SELECT
                        "listMember".id AS "listMemberDbId",
                        "listMember".order_number AS "orderNumber",
                        "listMember".creation_timestamp AS "creationTimestamp",
                        "listMember".display_value AS "displayValue",
                        "listMember".is_active AS "isActive",
                        "listMember".remarks,
                        seed.id AS "seedDbId",
                        seed.seed_code AS "seedCode",
                        seed.seed_name AS "seedName",
                        seed.harvest_date AS "harvestDate",
                        seed.harvest_method AS "harvestMethod",
                        germplasm.id AS "germplasmDbId",
                        germplasm.designation as "designation",
                        germplasm.germplasm_code as "germplasmCode",
                        germplasm.parentage as "parentage",
                        germplasm.designation AS "germplasmName",
                        program.id AS "programDbId",
                        program.program_code AS "programCode",
                        program.program_name AS "programName",
                        experiment.id AS "experimentDbId",
                        experiment.experiment_year AS "experimentYear",
                        experiment.experiment_type AS "experimentType",
                        (
                            SELECT
                                s.stage_code
                            FROM
                                tenant.stage s
                            WHERE
                                s.is_void = FALSE AND
                                s.id = experiment.id
                        ) AS "experimentStageCode",
                        entry.entry_name AS "entryName",
                        experiment.planting_season AS "experimentPlantingSeason"
                        ${getDuplicateQuery}
                `
            }
            memberQuery += `
                FROM
                    platform.list_member "listMember"
                INNER JOIN
                    germplasm.seed seed ON seed.id = "listMember".data_id AND seed.is_void = FALSE
                INNER JOIN
                    germplasm.germplasm germplasm ON seed.germplasm_id = germplasm.id AND germplasm.is_void = FALSE
                LEFT JOIN
                    tenant.program program ON seed.program_id = program.id AND PROGRAM.is_void = false
                LEFT JOIN
                    experiment.entry entry ON seed.source_entry_id = entry.id AND entry.is_void = FALSE
                LEFT JOIN
                    experiment.experiment experiment ON seed.source_experiment_id = experiment.id AND experiment.is_void = FALSE
                LEFT JOIN
                    experiment.location location ON seed.source_location_id = location.id AND LOCATION.is_void = FALSE
                LEFT JOIN
                    experiment.occurrence occurrence ON seed.source_occurrence_id = occurrence.id AND occurrence.is_void = FALSE
                LEFT JOIN
                    experiment.plot plot ON seed.source_plot_id = plot.id AND plot.is_void = FALSE
                WHERE
                    "listMember".is_void = FALSE
                    AND "listMember".list_id = ${listDbId}
                ORDER BY "listMember".order_number
            `
        } else if (type == 'plot') {
            if (memberQuery == null) {
                memberQuery = `
                    SELECT
                        "listMember".id AS "listMemberDbId",
                        "listMember".order_number AS "orderNumber",
                        "listMember".creation_timestamp AS "creationTimestamp",
                        "listMember".display_value AS "displayValue",
                        "listMember".is_active AS "isActive",
                        "listMember".remarks,
                        plot.id AS "plotDbId",
                        plot.location_id AS "locationDbId",
                        plot.occurrence_id AS "occurrenceDbId",
                        occurrence.occurrence_name AS "occurrenceName",
                        entry.id AS "entryDbId",
                        pi.entry_code AS "entryCode",
                        pi.entry_number AS "entryNumber",
                        plot.plot_code AS "plotCode",
                        plot.plot_number AS "plotNumber",
                        plot.plot_type AS "plotType",
                        plot.rep AS "rep",
                        plot.design_x AS "designX",
                        plot.design_y AS "designY",
                        plot.pa_x AS "paX",
                        plot.pa_y AS "paY",
                        plot.block_number AS "blockNumber",
                        plot.harvest_status AS "harvestStatus",
                        occurrence.experiment_id AS "experimentDbId",
                        entry.entry_name AS "entryName",
                        entry.germplasm_id AS "germplasmDbId",
                        germplasm.designation AS "designation",
                        germplasm.germplasm_code AS "germplasmCode",
                        germplasm.parentage AS "parentage",
                        germplasm.germplasm_state AS "state",
                        germplasm.designation AS "germplasmName",
                        crop.id AS "cropDbId",
                        crop.crop_code AS "cropCode"
                        ${getDuplicateQuery}
                `
            }
            memberQuery += `
                FROM
                    platform.list_member "listMember"
                INNER JOIN
                    experiment.plot plot
                ON
                    plot.id = "listMember".data_id
                    AND plot.is_void = FALSE
                LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                LEFT JOIN
                        experiment.entry entry ON entry.id = plot.entry_id
                LEFT JOIN
                        experiment.planting_instruction pi ON pi.plot_id = plot.id AND pi.entry_id = plot.entry_id AND pi.is_void = false
                LEFT JOIN
                        germplasm.germplasm germplasm ON germplasm.id = pi.germplasm_id
                LEFT JOIN
                        tenant.crop crop ON crop.id = germplasm.crop_id
                WHERE
                    "listMember".is_void = FALSE
                    AND "listMember".list_id = ${listDbId}
                ORDER BY
                    "listMember".order_number
            `
        } else if (type == 'location') {
            if (memberQuery == null) {
                memberQuery = `
                    SELECT
                        "listMember".id AS "listMemberDbId",
                        "listMember".order_number AS "orderNumber",
                        "listMember".creation_timestamp AS "creationTimestamp",
                        "listMember".display_value AS "displayValue",
                        "listMember".is_active AS "isActive",
                        "listMember".remarks,
                        location.id AS "locationDbId",
                        location.location_code AS "locationCode",
                        location.location_name AS "locationName",
                        location.location_status AS "locationStatus",
                        location.location_type AS "locationType",
                        location.description,
                        (
                            SELECT
                                count(p.id)
                            FROM
                                experiment.plot p
                            WHERE
                                p.location_id = location.id AND
                                p.is_void = FALSE
                        )::int AS "plotCount",
                        (
                            SELECT
                                count(log.id)
                            FROM
                                experiment.location_occurrence_group log
                            WHERE
                                log.location_id = location.id AND
                             log.is_void = FALSE
                        )::int AS "occurrenceCount",
                        location.location_document AS "locationDocument",
                        location.location_planting_date AS "plantingDate",
                        location.location_harvest_date AS "harvestDate",
                        site.id AS "siteDbId",
                        site.geospatial_object_code AS "siteCode",
                        site.geospatial_object_name AS "site",
                        field.id AS "fieldDbId",
                        field.geospatial_object_code AS "fieldCode",
                        field.geospatial_object_name AS "field",
                        geospatial_object.id AS "geospatialObjectDbId",
                        geospatial_object.geospatial_object_code AS "geospatialObjectCode",
                        geospatial_object.geospatial_object_name AS "geospatialObjectName",
                        geospatial_object.geospatial_object_type AS "geospatialObjectType",
                        location.location_year AS "year",
                        season.id AS "seasonDbId",
                        season.season_code AS "seasonCode",
                        season.season_name AS "season",
                        steward.id AS "stewardDbId",
                        steward.person_name AS "steward"
                        ${getDuplicateQuery}
                `
            }
            memberQuery += `
                FROM        
                    platform.list_member "listMember"
                INNER JOIN
                    experiment.location location
                ON
                    location.id = "listMember".data_id
                    AND location.is_void = FALSE
                LEFT JOIN
                    tenant.person steward ON location.steward_id = steward.id
                LEFT JOIN
                    place.geospatial_object geospatial_object ON location.geospatial_object_id = geospatial_object.id
                LEFT JOIN
                    place.geospatial_object site ON site.id = location.site_id
                LEFT JOIN
                    place.geospatial_object field ON field.id = location.field_id
                LEFT JOIN
                    tenant.season season on season.id = location.season_id
                WHERE
                    "listMember".is_void = FALSE
                    AND "listMember".list_id = ${listDbId}
                ORDER BY
                    "listMember".order_number
            `
        } else if (type == 'package') {
            if(!isBasic){
                memberQuery = getOldPackageListQuery(memberQuery,listDbId,getDuplicateQuery)
            }
            else{
                hasData = true
                memberQuery = getPackageListQuery(listDbId, '', isBasic, hasData)
            }
        } else if (type == 'sites') {
                if (memberQuery == null) {
                    memberQuery = `
                        SELECT
                            "listMember".id AS "listMemberDbId",
                            "listMember".order_number AS "orderNumber",
                            "listMember".creation_timestamp AS "creationTimestamp",
                            "listMember".display_value AS "displayValue",
                            "listMember".is_active AS "isActive",
                            "listMember".remarks,
                            "geospatialObject".id AS "geospatialObjectDbId",
                            "geospatialObject".geospatial_object_code AS "geospatialObjectCode",
                            "geospatialObject".geospatial_object_name AS "geospatialObjectName",
                            "geospatialObject".geospatial_object_type AS "geospatialObjectType",
                            "geospatialObject".geospatial_object_subtype AS "geospatialObjectSubtype",
                            "geospatialObject".geospatial_coordinates AS "geospatialObjectCoordinates",
                            "geospatialObject".altitude,
                            "geospatialObject".description,
                            "geospatialObject".geospatial_object_name||' ('||root.geospatial_object_code||')' AS "scaleName",
                            parent.id AS "parentGeospatialObjectDbId",
                            parent.geospatial_object_code AS "parentGeospatialObjectCode",
                            parent.geospatial_object_name AS "parentGeospatialObjectName",
                            parent.geospatial_object_type AS "parentGeospatialObjectType",
                            root.id aS "rootGeospatialObjectDbId",
                            root.geospatial_object_code AS "rootGeospatialObjectCode",
                            root.geospatial_object_name AS "rootGeospatialObjectName",
                            root.geospatial_object_type AS "rootGeospatialObjectType"
                            ${getDuplicateQuery}
                    `
                }
                memberQuery += `
                    FROM        
                        platform.list_member "listMember"
                        INNER JOIN
                            place.geospatial_object "geospatialObject"
                        ON "geospatialObject".id = "listMember".data_id
                            AND "geospatialObject".is_void = FALSE
                        LEFT JOIN
                            place.geospatial_object parent ON "geospatialObject".parent_geospatial_object_id = parent.id
                        LEFT JOIN
                            place.geospatial_object root ON "geospatialObject".root_geospatial_object_id = root.id
                    WHERE
                        "listMember".list_id = ${listDbId}
                        AND "listMember".is_void = FALSE
                    ORDER BY
                        "listMember".order_number
                `
        } else if (type == 'experiment') {
            if (memberQuery == null) {
                    memberQuery = `
                        SELECT
                            "listMember".id AS "listMemberDbId",
                            "listMember".order_number AS "orderNumber",
                            "listMember".creation_timestamp AS "creationTimestamp",
                            "listMember".display_value AS "displayValue",
                            "listMember".is_active AS "isActive",
                            "listMember".remarks,
                            experiment.id AS "experimentDbId",
                            experiment.experiment_year AS "experimentYear",
                            program.id AS "programDbId",
                            program.program_code AS "programCode",
                            program.program_name AS "programName",
                            item.display_name AS "experimentTemplate",
                            pipeline.id AS "pipelineDbId",
                            pipeline.pipeline_code AS "pipelineCode",
                            pipeline.pipeline_name AS "pipelineName",
                            stage.id AS "stageDbId",
                            stage.stage_code AS "stageCode",
                            stage.stage_name AS "stageName",
                            project.id AS "projectDbId",
                            project.project_code AS "projectCode",
                            project.project_name AS "projectName",
                            season.id AS "seasonDbId",
                            season.season_code AS "seasonCode",
                            season.season_name AS "seasonName",
                            crop.id AS "cropDbId",
                            crop.crop_code AS "cropCode",
                            crop.crop_name AS "cropName",
                            experiment.planting_season AS "plantingSeason",
                            experiment.experiment_code AS "experimentCode",
                            experiment.experiment_name AS "experimentName",
                            experiment.experiment_objective AS "experimentObjective",
                            experiment.experiment_type AS "experimentType",
                            experiment.experiment_sub_type AS "experimentSubType",
                            experiment.experiment_sub_sub_type AS "experimentSubSubType",
                            experiment.experiment_design_type AS "experimentDesignType",
                            experiment.experiment_status AS "experimentStatus",
                            experiment.description,
                            experiment.data_process_id AS "dataProcessDbId",
                            item.abbrev AS "dataProcessAbbrev",
                            experiment.steward_id AS "stewardDbId",
                            person.person_name AS "steward",
                            experiment.experiment_plan_id AS "experimentPlanDbId",
                            (
                                SELECT
                                    count(e.id)
                                FROM
                                    experiment.entry_list el,
                                    experiment.entry e
                                WHERE
                                    el.experiment_id = experiment.id AND
                                    e.entry_list_id = el.id AND
                                    e.is_void = FALSE
                            )::int AS "entryCount",
                            (
                                SELECT
                                    count(o.id)
                                FROM
                                    experiment.occurrence o
                                WHERE
                                    o.experiment_id = experiment.id AND
                                    o.is_void = FALSE
                            )::int AS "occurrenceCount",
                            experiment.creation_timestamp AS "experimentCreationTimestamp",
                            creator.id AS "experimentCreatorDbId",
                            creator.person_name AS "experimentCreator"
                            ${getDuplicateQuery}
                    `
            }
                
            memberQuery += `
                FROM        
                    platform.list_member "listMember"
                INNER JOIN
                    experiment.experiment experiment
                ON experiment.id = "listMember".data_id
                    AND experiment.is_void = FALSE
                LEFT JOIN
                    master.item item ON item.id = experiment.data_process_id
                LEFT JOIN
                    experiment.experiment_plan experiment_plan ON experiment_plan.id = experiment.experiment_plan_id
                LEFT JOIN
                    tenant.pipeline pipeline ON pipeline.id = experiment.pipeline_id
                LEFT JOIN
                    tenant.program program ON program.id = experiment.program_id
                LEFT JOIN
                    tenant.project project ON project.id = experiment.project_id
                LEFT JOIN
                    tenant.season season ON season.id = experiment.season_id
                LEFT JOIN
                    tenant.stage stage ON stage.id = experiment.stage_id
                LEFT JOIN
                    tenant.crop crop ON crop.id = experiment.crop_id
                LEFT JOIN
                    tenant.person person ON person.id = experiment.steward_id
                LEFT JOIN
                    tenant.person creator ON creator.id = experiment.creator_id
                WHERE
                    "listMember".list_id = ${listDbId}
                    AND "listMember".is_void = FALSE
                ORDER BY
                    "listMember".order_number
            `
        } else {
            if (memberQuery == null) {
                memberQuery = `
                    SELECT
                        "listMember".id AS "listMemberDbId",
                        "listMember".data_id AS "dataDbId",
                        "listMember".order_number AS "orderNumber",
                        "listMember".creation_timestamp AS "creationTimestamp",
                        "listMember".display_value AS "displayValue",
                        "listMember".is_active AS "isActive",
                        "listMember".remarks
                        ${getDuplicateQuery}
                `
            }
            memberQuery += `
                FROM        
                    platform.list_member "listMember"
                WHERE
                    "listMember".is_void = FALSE
                    AND "listMember".list_id = ${listDbId}
                ORDER BY
                    "listMember".order_number
            `
        }
        return memberQuery
    },

    reorder: async (req,res,listDbId, addNullsCond=``, userDbId) => {
        let transaction
        try {
            // Update order_number of every list member in the list
            let updateListMemberOrderNumberQuery = `
                UPDATE
                    platform.list_member lm
                SET
                    order_number = t.num,
                    modification_timestamp = NOW(),
                    modifier_id = ${userDbId}
                FROM
                    (SELECT
                        lm.id,
                        lm.order_number,
                        lm.modification_timestamp,
                        ROW_NUMBER() OVER(ORDER BY lm.order_number, lm.modification_timestamp ${addNullsCond}) AS num
                    FROM
                        platform.list_member lm
                    WHERE
                        lm.is_void = FALSE AND
                        lm.list_id = ${listDbId} AND
                        lm.is_active = TRUE
                    ) AS t
                WHERE
                    lm.is_void = FALSE AND
                    lm.is_active = TRUE AND
                    lm.id = t.id
            `
            
            // Start transactions
            transaction = await sequelize.transaction(async transaction => {
                await sequelize.query(updateListMemberOrderNumberQuery, {
                    type: sequelize.QueryTypes.UPDATE,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint, 'UPDATE', err)
                    throw new Error(err)
                })
            })
        } catch (err) {
            // Log error message when error encountered
            logger.logMessage(__filename, err.stack, 'error')
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            if(!res.headersSent) res.send(new errors.InternalError(errMsg))
            return
        }
    }
}