/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let errorBuilder = require('../error-builder')
let errors = require('restify-errors')

module.exports = {

    /**
     * Retrieve variable scale values
     * 
     * @param {String} variableAbbrev 
     * 
     * @returns {Array} scaleValues
     */
    getScaleValues: async (variableAbbrev) => {
        try{
            
            let finalQuery = `
                SELECT
                    ARRAY_AGG(sv.value) AS "scaleValue"
                FROM
                    master.variable v
                    JOIN master.scale_value sv 
                        ON sv.scale_id = v.scale_id AND sv.is_void = FALSE
                WHERE
                    v.is_void = FALSE 
                    AND v.abbrev = '${variableAbbrev}'
            `

            let values = await sequelize.query(
                finalQuery, 
                { type: sequelize.QueryTypes.UPDATE }
            )
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

            return values[0][0]['scaleValue'] ?? []

        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Check if value is in the scale values for variable
     * @param {String} variableAbbrev abbrev of variable 
     * @param {String} value value to be searched
     * @returns
     */
    isInScaleValues: async (variableAbbrev, value) => {
        try {

            let finalQuery = `
                SELECT
                    COUNT(1)
                FROM
                    master.scale_value scaleValue
                LEFT JOIN master.variable variable
                    ON scaleValue.scale_id = variable.scale_id
                WHERE
                    variable.abbrev = $$${variableAbbrev}$$ 
                    AND scaleValue.value = $$${value}$$
            `

            let count = await sequelize.query(
                finalQuery,
                { type: sequelize.QueryTypes.UPDATE }
            )
                .catch(async err => {
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })

            return count[0] ?? 0;

        } catch (err) {
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    }
}