/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op

module.exports = {

    /**
     * Retrieve the pattern of configuration based on abbrev
     * @param String configAbbrev 
     */
    getPattern: async (configAbbrev) => {
        let configQuery = `
            SELECT 
                config_value
            FROM
                platform.config
            WHERE
                abbrev ='${configAbbrev}'
                AND is_void = false
        `
        let configValue = await sequelize.query(configQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if(!configValue || !configValue[0]){
            return []
        }

        return configValue[0]['config_value']['pattern']
    },
    /**
     * Pad a number
     * @param Integer counter Counter value
     * @param String paddingPosition value is either trailing or leading 
     * @param String width Number of expected digits
     */
    getPaddedNumber: async (counter, paddingPosition, width) => {

        counter = counter + '';
        if (paddingPosition == 'leading') {
            return counter.length >= width ? counter : new Array(width - counter.length + 1).join('0') + counter;
        } else {
            return counter + counter.length >= width ? counter : new Array(width - counter.length + 1).join('0');
        }

    },
    /**
     * Retrieve the value of a record
     * @param String schema Database schema name
     * @param String entity Database table
     * @param String entityColumn Database column of table 
     * @param Integer entityId Database table identifier
     */
    getEntityValue: async (schema, entity, entityColumn, entityId) => {
        let query = `
        SELECT 
            ${entityColumn}
        FROM
            ${schema}.${entity}
        WHERE
            id = '${entityId}'
            AND is_void = FALSE
    `
        let value = await sequelize.query(query, {
            type: sequelize.QueryTypes.SELECT
        })

        return value[0]
    },

    /**
     * Get the next count of experiment based on conditions
     * @param String schema Database schema name
     * @param String entity Database table
     * @param String condition Custom condition to retrieve counter
     * @param String columns Columns used to group the experiment
     * @param Integer startingCounter starting number
     */
    getCounter: async (schema, entity, condition, columns, startingCounter) => {

        let counter = startingCounter
        let counterQuery = `
            SELECT 
                MAX("orderNumber") + ${counter} as max
            FROM (
                SELECT 
                    ROW_NUMBER() OVER (PARTITION BY ${columns} ORDER BY ${columns} ) AS "orderNumber"
                FROM 
                    ${schema}.${entity}
                WHERE
                    ${condition}
            )e
        `
        let maxCounter = await sequelize.query(counterQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (maxCounter[0]['max'] == undefined) {
            return counter
        }
        
        return maxCounter[0]['max']
    }
}
