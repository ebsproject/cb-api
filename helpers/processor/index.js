/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const axios = require('axios')

let { sequelize } = require('../../config/sequelize')
let errors = require('restify-errors')
let errorBuilder = require('../../helpers/error-builder')

module.exports = {

    /**
     *  Retrieve response data by calling existing API endpoints using axios
     *  
     *  @param httpMethod string http method
     *  @param url string url + endpoint + id (if applicable)
     *  @param token string token
     * 
     *  @return object response data
     */
    getBackgroundJobData: async (httpMethod, url, token, requestData) => {
        let properties = {
            method: httpMethod,
            headers: {
                "Authorization": token,
                "Content-type": "application/json",
                "Accept": "application/json"
            },
            data: requestData
        }

        properties.url = url
        let res = await axios(properties)
        let jsonData = res.data

        let data = {
            response: (res.statusText == 'OK') ? jsonData.result.data : jsonData.metadata.status,
            id: (res.status == 200) ? jsonData.result.data[0]['backgroundJobDbId'] : null,
            status: res.status,
        }
        return data
    },

    /**
     * Checks if experiment is drafted by checking its experiment_status
     * 
     * @param experimentDbId integer
     * 
     * @return true or false
     */
    isDraftedExperiment: async (experimentDbId) => {
        let experimentQuery = `
            SELECT
                experiment_status AS "experimentStatus"
            FROM
                experiment.experiment
            WHERE
                is_void = FALSE AND
                id = ${experimentDbId}
        `

        let experiment = await sequelize.query(experimentQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        if (await experiment === undefined || await experiment.length < 1) {
            return null
        }

        let experimentStatus = experiment[0].experimentStatus
        return (experimentStatus.includes('draft'))
    }
}