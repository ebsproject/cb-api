/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')
let patternHelper = require('../../helpers/patternGenerator/index')

module.exports = {
    /**
     * Retrieve the experiment crop code and breeding info given the experiment id
     * @param {Integer} experimentDbId experiment identifier
     * 
     * @return {Array} array containing the cropCode, isBreeding, isHybrid
     */
    checkBreedingExperiment: async(experimentDbId) => {

        let experimentQuery = `
            SELECT
                crop.crop_code AS "cropCode",
                (CASE 
                    WHEN crop.crop_code = 'RICE' THEN ((stage_name ilike 'Breeding Crosses' AND 
                        (experiment_sub_type is null OR experiment_sub_type not ilike 'Hybrid Crosses')) OR 
                        (experiment_sub_type ilike 'Breeding Crosses' AND stage_name not ilike 'Hybrid seed production'))
                    ELSE ((stage_name ilike 'Breeding Crosses' AND (experiment_sub_type is null OR experiment_sub_type not ilike 'Hybrid Crosses')) OR 
                        (experiment_sub_type ilike 'Breeding Crosses' AND stage_name not ilike 'Hybrid Crosses'))
                END) AS "isBreeding",
                (CASE 
                    WHEN crop.crop_code = 'RICE' THEN (stage_name ilike 'Hybrid seed production' AND 
                    (experiment_sub_type is null OR experiment_sub_type not ilike 'Breeding Crosses'))
                    ELSE ((stage_name ilike 'Hybrid Crosses' AND (experiment_sub_type is null OR experiment_sub_type not ilike 'Breeding Crosses')) OR 
                        (experiment_sub_type ilike 'Hybrid Crosses' AND stage_name not ilike 'Breeding Crosses'))
                END) AS "isHybrid"
                FROM
                    experiment.experiment AS experiment
                LEFT JOIN tenant.stage on experiment.stage_id = stage.id
                LEFT JOIN tenant.crop on experiment.crop_id = crop.id
                WHERE 
                    experiment.id = ${experimentDbId}
                    AND experiment.is_void = FALSE
                    AND stage.is_void = FALSE
                    AND crop.is_void = FALSE
       `

       let result = await sequelize.query(experimentQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return (result === undefined)? null : result[0]

    },

    /**
     * Retrieve germplasm info given the germplasm name
     * @param {String} germplasmName 
     * @returns 
     */
    getGermplasmInfo: async(germplasmName) => {

        let germplasmIdQuery = `
            SELECT
                id AS "germplasmDbId",
                parentage,
                generation,
                germplasm_state AS "germplasmState"
            FROM
                germplasm.germplasm
            WHERE
                germplasm_normalized_name = (SELECT platform.normalize_text('${germplasmName}'))
                AND is_void = FALSE
        `
        // Retrieve germplasm from the database

        let result = await sequelize.query(germplasmIdQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if(result === undefined){
            return null
        }
    
        return (result === undefined)? null : result[0]
    },

    /**
     * Facilitate the generation of system-defined entry list name
     * 
     * @param {Object} pattern pattern configuration
     * @param {Integer} experimentYear source experiment year
     * @param {Integer} seasonDbId season indetifier
     * @param {Integer} siteDbId site identifier
     * @param {Integer} startingCount entry list counter
     * 
     * @returns {String} generated code
     */
    generateCode: async(pattern, experimentYear, seasonDbId, siteDbId, startingCount) => {
        let code = []

        for (i = 0; i < pattern.length; i++) {
            let value = ''
            if (pattern[i]["is_variable"]) {
                switch(pattern[i]["value"]){
                    case "YEAR":
                        code.push(experimentYear)
                        break;
                    case "SEASON_CODE":
                        value = await patternHelper.getEntityValue(
                                'tenant', 
                                'season', 
                                'season_code', 
                                seasonDbId
                                )
                        code.push(value['season_code'])
                        break;
                    case "SITE_CODE":
                        value = await patternHelper.getEntityValue(
                            'place', 
                            'geospatial_object', 
                            'geospatial_object_code', 
                            siteDbId
                            )
                        code.push(value['geospatial_object_code'])
                        break;
                }
            } 
            else {
                if (pattern[i]["value"] == "-") {
                    code.push('-')
                }
                if (pattern[i]["value"] == "COUNTER") {
                    let counter = 1

                    let columns = pattern[i]["max_value"].split('|');
                    let condition = []
                    let selectColumns = []

                    for (column of columns) {
                        switch(column){
                            case "SEASON":
                                condition.push('season_id =' + seasonDbId)
                                selectColumns.push('season_id')
                                break;
                            case "YEAR":
                                selectColumns.push('experiment_year')
                                condition.push('experiment_year =' + experimentYear)
                                break;
                            case "SITE":
                                selectColumns.push('site_id')
                                condition.push('site_id =' + siteDbId)
                        }

                    }
                    
                    selectColumns = selectColumns.join(", ")
                    let conditionStr = condition.join(" AND ");
                    
                    counter = await patternHelper.getCounter(
                        'experiment', 
                        'entry_list', 
                        conditionStr, 
                        selectColumns,
                        startingCount
                        )
                    
                    counter = await patternHelper.getPaddedNumber(
                        counter, 
                        pattern[i]["zero_padding"], 
                        parseInt(pattern[i]["digits"])
                        )
                    
                    code.push(counter)
                }
            }
        }

        return code.join("")
    }

}