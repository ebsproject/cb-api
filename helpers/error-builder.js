/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let responseMessageHelper = require('./response-messages')
let responseHelper = require('./responses')

// Returns the error message string
module.exports = {
  getError: async (host, code, message = null) => {
    if (code) {
      let errorHost = host
      let errorCode = code;
      // Extracts the first 3 from the errorCode
      let httpCode = errorCode.toString().substring(0, 3)
      // Get the appropriate response entry from the database
      let temp;
      try {
          let temp = await responseHelper.getResponseCodes(code)
          // Build the error object
          let error = {
              "code": parseInt(httpCode),
              "msg": temp[0].message,
              "url": `${errorHost}${temp[0].url}`
          }
          return `${error.code}: ${error.msg} | ${error.url}`
      } catch (err) {
        if (message != null) {
          return message
        }
        return "Something went wrong in handling the error. Make sure the error code exists in the database."
      }
    }
  },

  getErrorResponse: async (host, routine, message = null) => {
      const routineMap = {
        'errorMissingColumn': 400057
      }
      const code = routineMap[routine] ?? 400015

      if (code) {
        const errorHost = host
        const errorCode = code;
        // Extracts the first 3 from the errorCode
        const httpCode = errorCode.toString().substring(0, 3)
        // Get the appropriate response entry from the database
        try {
            const temp = await responseHelper.getResponseCodes(code)
            // Build the error object
            const error = {
                "code": parseInt(httpCode),
                "msg": temp[0].message,
                "url": `${errorHost}${temp[0].url}`
            }
            return `${error.code}: ${error.msg} | ${error.url}`
        } catch (err) {
          if (message != null) {
            return message
          }
          return "Something went wrong in handling the error. Make sure the error code exists in the database."
        }
      }
  }
}