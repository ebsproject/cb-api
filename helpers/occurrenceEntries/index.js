/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op

module.exports = {
    /**
     * Retrieve the sql query for getting the entries of an occurrence
     * @param occurenceDbId integer occurrence identifier
     * @param addedDistinctString strig distinct string
     * @param distinctColumn string distinct column
     * 
     * @return sql query string
     */
    getOccurrenceEntriesQuerySql: async (occurrenceDbId = 0, addedDistinctString = '', distinctColumn = '', responseColString = '') => {

        return `
            SELECT
                ${addedDistinctString}
                ${responseColString}
            FROM
                experiment.plot plot
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
            JOIN
                experiment.planting_instruction pi ON pi.plot_id = plot.id
            JOIN
                experiment.entry e ON pi.entry_id = e.id
            JOIN
                germplasm.germplasm g ON g.id = pi.germplasm_id AND g.id = pi.germplasm_id
            LEFT JOIN
                germplasm.seed seed ON seed.id = pi.seed_id
            LEFT JOIN
                germplasm.package package ON package.id = pi.package_id
            JOIN 
                tenant.person creator ON creator.id = pi.creator_id::integer
            LEFT JOIN 
                tenant.person modifier ON modifier.id = pi.modifier_id::integer
            WHERE
                plot.occurrence_id = ` + occurrenceDbId + `
                AND plot.is_void = false
                AND e.is_void = false
                AND pi.is_void = false
                AND plot.occurrence_id = occurrence.id
            `
    }
}