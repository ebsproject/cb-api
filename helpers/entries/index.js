/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')
let { knex } = require('../../config/knex')

module.exports = {
 
  reorder: async (entrylistDbId, addNullsCond=``, userDbId) => {
    // Update order_number of every entry in the entrylist
    let updateEntryNumberQuery = `
      UPDATE
        experiment.entry ee
      SET
        entry_number = t.num,
        entry_code = t.num,
        modification_timestamp = NOW(),
        modifier_id = ${userDbId}
      FROM
        (SELECT
          ee.id,
          ee.entry_number,
          ee.modification_timestamp,
          ROW_NUMBER() OVER(ORDER BY ee.entry_number, ee.modification_timestamp ${addNullsCond}) AS num
        FROM
          experiment.entry ee
        WHERE
          ee.is_void = FALSE AND
          ee.entry_list_id = ${entrylistDbId}
        ) AS t
      WHERE
        ee.is_void = FALSE AND
        ee.id = t.id
    `

    await sequelize.query(updateEntryNumberQuery, {
      type: sequelize.QueryTypes.INSERT
    })
  }
}