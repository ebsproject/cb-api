/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')

module.exports = {

  reorder: async (occurrenceDbId, addNullsCond=``, personDbId) => {
    // Update plot_number of every plot in the location
    let updatePlotNumberQuery = `
        UPDATE
            experiment.plot p
        SET
            plot_number = t.num,
            plot_code = t.num,
            modification_timestamp = NOW(),
            modifier_id = ${personDbId}
        FROM
            (
                SELECT
                    p.id,
                    p.plot_number,
                    p.modification_timestamp,
                    ROW_NUMBER() OVER(ORDER BY p.plot_number, p.modification_timestamp ${addNullsCond}) AS num
                FROM
                    experiment.plot p
                WHERE
                    p.is_void = FALSE AND
                    p.occurrence_id = ${occurrenceDbId}
            ) AS t
        WHERE
            p.is_void = FALSE AND
            p.id = t.id
    `
    
    await sequelize.query(updatePlotNumberQuery, {
        type: sequelize.QueryTypes.UPDATE
    })
  }
}