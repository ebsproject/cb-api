/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize, Study } = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op

module.exports = {

    /**
     * Generate the parentage of a product given the cross ID
     * 
     * @param crossDbId integer unique identifier of a cross record
     * 
     * @return parentage string generated parentage
     */
    generateParentage: async (crossDbId) => {

        // Default
        let parentage = ''

        // Get cross information
        let crossSql = `
            SELECT
                "cross".id AS "crossDbId",
                "cross".cross_name AS designation,
                "crossMethod".id AS "methodDbId",
                "crossMethod".display_name AS method,
                female.id AS "femaleEntryDbId",
                female.study_id AS "femaleStudyDbId",
                female.product_id AS "femaleProductDbId",
                female.study AS "femaleSourceStudy",
                female.entcode AS "femaleSource",
                female.designation AS "femaleParent",
                RTRIM(female.parentage) AS "femaleParentage",
                female.generation AS "femaleGeneration",
                male.id AS "maleEntryDbId",
                male.study_id AS "maleStudyDbId",
                male.product_id AS "maleProductDbId",
                male.study AS "maleSourceStudy",
                male.entcode AS "maleSource",
                male.designation AS "maleParent",
                RTRIM(male.parentage) AS "maleParentage",
                male.generation AS "maleGeneration"
            FROM
                operational.cross "cross"
                LEFT JOIN
                    master.cross_method "crossMethod"
                ON
                    "crossMethod".id = "cross".cross_method_id
                LEFT JOIN
                (
                    SELECT 
                        product.id as product_id, product.parentage, entry.id, 
                        product.designation, study.study, entry.study_id, entry.entcode,
                        product.generation
                    FROM 
                        master.product product, 
                        operational.entry entry, 
                        operational.study study
                    WHERE entry.product_id = product.id
                    AND entry.study_id = study.id
                ) female
                ON 
                    female.id = "cross".female_entry_id
                LEFT JOIN
                (
                    SELECT 
                        product.id as product_id, product.parentage, entry.id, 
                        product.designation, study.study, entry.study_id, entry.entcode,
                        product.generation
                    FROM 
                        master.product product, 
                        operational.entry entry, 
                        operational.study study
                    WHERE entry.product_id = product.id
                    AND entry.study_id = study.id
                ) male
                ON 
                    male.id = "cross".male_entry_id

            WHERE
                "cross".id = :crossDbId
        `

        let crosses = await sequelize.query(crossSql, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                crossDbId: crossDbId
            }
        })

        // Return error if resource is not found
        if (crosses.length < 1) {
            return '?/?'
        } else {
            for (cross of crosses) {

                // Set values
                let femaleProductName = cross.femaleParent
                let maleProductName = cross.maleParent

                let femaleGeneration = cross.femaleGeneration
                let maleGeneration = cross.maleGeneration

                let femaleParentage = (cross.femaleParentage != null) ? cross.femaleParentage : cross.femaleParent
                let maleParentage = (cross.maleParentage != null) ? cross.maleParentage : cross.maleParent

                let femaleParents = femaleParentage.split('/')
                let femaleLength = femaleParents.length
                let maleParents = maleParentage.split('/')
                let maleLength = maleParents.length

                let count = 0
                let counter = 0
                let femaleCounter = 0
                let maleCounter = 0

                // Get the highest number of slash (/)

                for (let i = 0; i < femaleLength - 1; i++) {
                    if (femaleParents[i] != '') {
                        if (femaleCounter < count) {
                            femaleCounter = count
                        }
                    } else {
                        count += 1
                    }
                }

                for (let i = 0; i < maleLength - 1; i++) {
                    if (maleParents[i] != '') {
                        if (maleCounter < count) {
                            maleCounter = count
                        }
                    } else {
                        count += 1
                    }
                }

                if (maleCounter > femaleCounter) {
                    counter = maleCounter
                } else {
                    counter = femaleCounter
                }

                // Generate the parentage according to cross method              

                if (cross.methodDbId == 1 || cross.method == 'Single cross') {
                    // Concatenate female product name and male product name
                    parentage = femaleProductName + '/' + maleProductName
                } else if (cross.methodDbId == 2 || cross.method == 'Backcross') {

                    // Parse female parent
                    let number = 0
                    let femaleParentsTemp1 = femaleParents[0].split('*')[0]

                    let femaleParentsTemp2 = ''
                    let femaleParentsTemp3 = ''

                    if (femaleParents[femaleLength - 1] != undefined) {
                        femaleParentsTemp2 = femaleParents[femaleLength - 1].split('*')[0]
                        femaleParentsTemp3 = femaleParents[femaleLength - 1].split('*')[1]
                    }

                    // Check if femaleParentsTemp1 is equal to male product name
                    if (femaleParentsTemp1 == maleProductName) {
                        let femaleSplit = femaleParents[0].split('*')[1]

                        // Set the number of recurring parent
                        if (femaleSplit == null || femaleSplit == '') {
                            number = 1
                        } else {
                            number = femaleSplit
                        }

                        // Set the expansion of female parent
                        let finalNumber = parseInt(number) + 1
                        parentage = femaleParentsTemp1 + '*' + finalNumber

                        for (let i = 1; i < femaleLength; i++) {
                            parentage += '/' + femaleParents[i]
                        }
                    } else if (femaleParentsTemp2 == maleProductName || femaleParentsTemp3 == maleProductName) {  // Check if femaleParentsTemp2 or femaleParentsTemp3 is equal to male product name 

                        // Set the number of recurring parent
                        number = 1
                        temp = femaleParentsTemp2

                        if (femaleParentsTemp3 == maleProductName) {
                            number = femaleParents[femaleLength - 1].split('*')
                            temp = femaleParentsTemp3
                        }

                        // Set the expansion of female parent

                        parentage = femaleParents[0]

                        let finalNumber = parseInt(number) + 1
                        for (let i = 1; i < femaleLength; i++) {
                            parentage += '/' + finalNumber + '*' + temp
                        }
                    } else {

                        // Parse male parent
                        let maleParentsTemp1 = maleParents[0].split('*')[0]

                        let maleParentsTemp2 = ''
                        let maleParentsTemp3 = ''
                        if (maleParents[maleLength - 1] != undefined) {
                            maleParentsTemp2 = maleParents[maleLength - 1].split('*')[0]
                            maleParentsTemp3 = maleParents[maleLength - 1].split('*')[1]
                        }

                        // Check if maleParentsTemp1 is equal to female product name 
                        if (maleParentsTemp1 == femaleProductName) {

                            // Set the number of recurring parent
                            maleSplit = maleParents[0].split('*')[1]

                            if (maleSplit == null || maleSplit == '') {
                                number = 1
                            } else {
                                number = maleSplit
                            }

                            // Set the expansion of male parent
                            let finalNumber = parseInt(number) + 1

                            parentage = maleParentsTemp1 + '*' + finalNumber

                            for (let i = 1; i < maleLength; i++) {
                                parentage += '/' + maleParents[i]
                            }

                        } else if (maleParentsTemp2 == femaleProductName || maleParentsTemp3 == femaleProductName) { // Check if maleParentsTemp2 or maleParentsTemp3 is equal to male product name 

                            // Set the number of recurring parent
                            number = 1
                            temp = maleParentsTemp2

                            if (maleParentsTemp3 == femaleProductName) {
                                number = maleParents[maleLength - 1].split('*')
                                temp = maleParentsTemp3
                            }

                            // Set the expansion of male parent
                            parentage = maleParents[0]

                            for (let i = 1; i < femaleLength; i++) {
                                parentage += '/' + maleParents[i]
                            }

                            let finalNumber = parseInt(number) + 1
                            parentage += '/' + finalNumber + '*' + temp
                        }
                    }
                } else if (cross.methodDbId == 3 || cross.method == 'Complex cross') {

                    // Get the expansion of female parents
                    parentage = femaleParents[0]

                    for (let i = 1; i < femaleLength; i++) {
                        parentage += '/' + femaleParents[i]
                    }

                    // Get the number of slashes
                    if (counter < 3) {
                        for (let i = 0; i < counter + 1; i++) {
                            parentage += '/'
                        }
                    } else {
                        parentage += '/' + (counter + 1) + '/'
                    }

                    // Get the expansion of male parents
                    parentage += '/' + maleParents[0]

                    for (let i = 1; i < maleLength; i++) {
                        parentage += '/' + maleParents[i]
                    }

                } else if (cross.methodDbId == 4 || cross.method == 'Three-way cross') {

                    // Check the generation
                    if (femaleGeneration != 'F1') {
                        // Get the female product name
                        parentage = femaleProductName

                        // Get the number of slashes
                        if (maleCounter < 3) {
                            for (let i = 0; i < maleCounter + 1; i++) {
                                parentage += '/'
                            }
                        } else {
                            parentage += '/' + (maleCounter + 1) + '/'
                        }

                        // Get the expansion of male parents
                        for (let i = 0; i < maleLength; i++) {
                            parentage += '/' + maleParents[i]
                        }
                    } else {

                        // Get the expansion of female parents
                        parentage = femaleParents[0]

                        for (let i = 1; i < femaleLength; i++) {
                            parentage += '/' + femaleParents[i]
                        }

                        // Get the number of slashes
                        if (femaleCounter < 3) {
                            for (let i = 0; i < femaleCounter + 1; i++) {
                                parentage += '/'
                            }
                        } else {
                            parentage += '/' + (femaleCounter + 1) + '/'
                        }

                        // Get the male product name
                        parentage += '/' + maleProductName
                    }
                } else if (cross.methodDbId == 5 || cross.method == 'Double cross') {

                    // Get the expansion of the female parent
                    parentage = femaleParents[0]

                    for (let i = 1; i < femaleLength; i++) {
                        parentage += '/' + femaleParents[i]
                    }

                    // Get the number of slashes
                    if (counter < 3) {
                        for (let i = 0; i < counter + 1; i++) {
                            parentage += '/'
                        }
                    } else {
                        parentage += '/' + (counter + 1) + '/'
                    }

                    // Get the expansion of the male parent
                    parentage += '/' + maleParents[0]

                    for (let i = 1; i < maleLength; i++) {
                        parentage += '/' + maleParents[i]
                    }
                } else if (cross.methodDbId == 7 || cross.method == 'Hybrid formation') {
                    // Concatenate female product name and male product name
                    parentage = femaleProductName + '/' + maleProductName
                } else {
                    // Default

                    // Get the expansion of the female parent
                    parentage = femaleParents[0]

                    for (let i = 1; i < femaleLength; i++) {
                        parentage += '/' + femaleParents[i]
                    }

                    // Get the number of slashes
                    if (counter < 3) {
                        for (let i = 0; i < counter + 1; i++) {
                            parentage += '/'
                        }
                    } else {
                        parentage += '/' + (counter + 1) + '/'
                    }

                    // Get the expansion of the male parent
                    parentage += '/' + maleParents[0]

                    for (let i = 1; i < maleLength; i++) {
                        parentage += '/' + maleParents[i]
                    }
                }
            }
        }

        return parentage
    },

    /**
     * 
     * Retrieve product GID using product ID
     * 
     * @param productDbId integer unique identifier for product record
     * 
     * @return productGidId
     * 
     */
    getProductGidId: async (productDbId) => {
        let productGidQuery = `
            SELECT
                product.product_gid_id AS productGidId
            FROM
                master.product product
            WHERE
                product.is_void = FALSE AND
                product.id = ${productDbId}
        `

        let productGidId = await sequelize.query(productGidQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        if (!productGidId.length) return null

        return productGidId[0].productGidId
    }
}