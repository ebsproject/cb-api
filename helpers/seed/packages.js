/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let logger = require('../../helpers/logger/index')

/**
 * Retrieve platform config record using abbrev value
 * @param {String} configAbbrev configuration abbrev
 * 
 * @returns mixed
 */
async function getConfigByAbbrev(configAbbrev) {
    try {
        let finalQuery = `
                SELECT
                    config.config_value
                FROM
                    platform.config config
                WHERE
                    config.is_void = FALSE
                    AND config.abbrev = $$${configAbbrev}$$
            `

        let values = await sequelize.query(finalQuery, {
            type: sequelize.QueryTypes.UPDATE
        }).catch(async err => {
            let errMsg = await errorBuilder.getError(req.headers.host, 500004)
            logger.logMessage('seed-packages-search', errMsg, 'error')

            res.send(new errors.InternalError(errMsg))
            return
        })

        return values ?? []

    } catch (err) {
        let errMsg = await errorBuilder.getError(req.headers.host, 500001)
        logger.logMessage('seed-packages-search', errMsg, 'error')

        res.send(new errors.InternalError(errMsg))
        return
    }
}

/**
 * Build data queries from config variables
 * @param {String} baseQueryAlias main query alias
 * @param {String} sourceAlias source alias values
 * @param {Array} variableArray variable array values
 * 
 * @returns mixed
 */
async function buildDataQuery(baseQueryAlias, sourceAlias, variableArray) {

    var dataQuery = ``;
    var dataSelect = ``;
    var dataJoin = ``;

    if (variableArray == undefined || variableArray.length < 1) {
        return "";
    }

    var last = variableArray[variableArray.length - 1]['variable_abbrev'] ?? "";
    var dataSources = ['package_data', 'seed_data'];

    try {
        for (key in variableArray) {
            let record = variableArray[key];

            if (record['target_table'] == undefined || record['target_table'] == null
                || record['target_table'] == "") {
                continue;
            }

            let targetTable = record['target_table'].split('.');
            let schema = targetTable[0] ?? "";
            let entity = targetTable[1].split('_')[0] ?? "";
            let source = targetTable[1] ?? "";
            let alias = record['api_body_param'] ?? "";
            let variableAbbrev = record['variable_abbrev'] ?? "";

            if (targetTable == undefined || entity == undefined || source == undefined
                || !dataSources.includes(source) || alias == undefined || variableAbbrev == '') {
                continue;
            }

            dataQuery += `
                ${alias} AS (
                    SELECT
                        trait.${entity}_id,
                        trait.data_value AS data_value
                    FROM
                        ${schema}.${source} trait
                        JOIN master.variable variable
                            ON variable.id = trait.variable_id AND variable.is_void = FALSE
                        JOIN ${baseQueryAlias}
                            ON ${baseQueryAlias}."${entity}DbId" = trait.${entity}_id
                    WHERE
                        trait.is_void = FALSE
                        AND variable.abbrev = $$${variableAbbrev}$$
                )
            `;

            dataSelect += `${alias}.data_value AS "${alias}"`;
            dataJoin += `
                LEFT JOIN ${alias} ${alias}
                    ON ${alias}.${entity}_id = ${sourceAlias}."${entity}DbId"
            `;

            if (record['variable_abbrev'] != last) {
                dataQuery += ',';
                dataSelect += ',';
            }

        }

        return {
            'query': dataQuery,
            'selectStatement': dataSelect,
            'joinStatement': dataJoin
        };
    }
    catch (error) {
        let errMsg = 'Error encountered in building package data queries!';
        logger.logMessage('seed-packages-search', errMsg, 'error-strong')
    }
}

module.exports = {

    /**
     * Retrieve germplasm level query statement data
     * @param {*} isSource if data level source
     * @param {*} dataFilter filters
     * @param {*} dataLevel data level
     * 
     * @returns mixed
     */
    getGermplasmData: async (isSource = false, dataFilter = [],dataLevel) => {

        let germplasmDataSelectQuery = `
            germplasm.designation AS "designation",
            germplasm.parentage,
            germplasm.generation,
            germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
            germplasm.germplasm_state AS "germplasmState",
            germplasm.germplasm_type AS "germplasmType",
            germplasm.germplasm_code AS "germplasmCode",
            germplasm.other_names AS "germplasmOtherNames",
            germplasm.id AS "germplasmDbId",
        `
        
        // default JOIN; if germplasm is the initial source filter, and dataLevel = all
        let germplasmDataJoinQuery = ` germplasm.germplasm germplasm `

        // if germplasm data is not the initial source data
        if(!isSource & dataLevel == 'seed'){
            germplasmDataJoinQuery = `
                JOIN germplasm.germplasm germplasm 
                    ON germplasm.id = seed.germplasm_id 
                    AND germplasm.is_void = FALSE
            `
        }
        
        // if germplasm is the initial data source AND has germplasm name filter
        if( isSource && Object.keys(dataFilter).length > 0 && 
            dataFilter['names'] != undefined && dataFilter['names'] != ''){
            germplasmDataJoinQuery = `
                (
                    SELECT
                        gn.germplasm_id
                    FROM
                        germplasm.germplasm_name gn
                    WHERE
                        gn.is_void = FALSE
                        ${dataFilter['names']}
                    GROUP BY gn.germplasm_id
                ) AS gn (germplasm_id)
            `

            germplasmDataJoinQuery += `
                INNER JOIN germplasm.germplasm germplasm 
                    ON germplasm.id = gn.germplasm_id 
                    AND germplasm.is_void = FALSE
            `
        }

        // if germplasm is the initial data source AND has germplasm code filter
        if( isSource && Object.keys(dataFilter).length > 0 && 
            dataFilter['germplasmCodes'] != undefined && dataFilter['germplasmCodes'] != ''){
            germplasmDataJoinQuery = `
                (
                    SELECT
                        germplasm.designation,
                        germplasm.parentage,
                        germplasm.generation,
                        germplasm.germplasm_normalized_name,
                        germplasm.germplasm_state,
                        germplasm.germplasm_type,
                        germplasm.germplasm_code,
                        germplasm.other_names,
                        germplasm.id
                    FROM
                        germplasm.germplasm germplasm
                    WHERE
                        germplasm.is_void = FALSE
                        ${dataFilter['germplasmCodes']}
                    GROUP BY germplasm.id
                ) AS germplasm
            `
        }

        if(['germplasm','all'].includes(dataLevel)){
            germplasmDataSelectQuery += `package.id AS "packageDbId", `

            germplasmDataJoinQuery += `
            JOIN germplasm.seed seed 
                ON seed.germplasm_id = germplasm.id 
                AND seed.is_void = FALSE
            `
        }

        return {
            'germplasmDataSelectQuery': germplasmDataSelectQuery,
            'germplasmDataJoinQuery': germplasmDataJoinQuery
        }
    },

    /**
     * Retrieve seed level query statement data
     * @param {*} isSource if data level source
     * @param {*} dataFilter filters
     * @param {*} dataLevel data level
     * 
     * @returns mixed
     */
    getSeedData: async (isSource = false, dataFilter = [], dataLevel) => {
        let seedDataSelectQuery = `
            seed.seed_name AS "seedName",
            seed.seed_code AS "seedCode",
            seed.source_experiment_id AS "experimentDbId",
            seed.source_occurrence_id AS "occurrenceDbId",
            seed.source_location_id AS "locationDbId",
            seed.source_entry_id AS "entryDbId",
            seed.cross_id AS "crossDbId",
            seed.harvest_source AS "harvestSource",`

        if(['seed'].includes(dataLevel)){
            seedDataSelectQuery += `
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
                germplasm.designation AS "designation", `
        }

        let seedDataJoinQuery = ` germplasm.seed seed `

        // data level = germplasm AND not isInitialFilteredSource
        if(!isSource & !['all','seed'].includes(dataLevel)){
            seedDataJoinQuery = `
                JOIN germplasm.seed seed 
                    ON seed.germplasm_id = germplasm.id 
                    AND seed.is_void = FALSE 
            `
        }

        // data level = seed AND not isInitialFilteredSource 
        if(!isSource & dataLevel == 'seed'){
            seedDataJoinQuery += `
                JOIN germplasm.germplasm germplasm 
                    ON germplasm.id = seed.germplasm_id 
                    AND germplasm.is_void = FALSE 
            `
        }
        
        // if seed is the initial data source AND has seed code or seed names filter
        if( isSource && Object.keys(dataFilter).length > 0 && 
            ((dataFilter['seedNames'] != undefined && dataFilter['seedNames'] != '') ||
             (dataFilter['seedCodes'] != undefined && dataFilter['seedCodes'] != ''))){

                seedDataJoinQuery = `
                    (
                        SELECT
                            seed.id,
                            seed.seed_name,
                            seed.seed_code,
                            seed.source_experiment_id,
                            seed.source_occurrence_id,
                            seed.source_location_id,
                            seed.source_entry_id,
                            seed.source_plot_id,
                            seed.harvest_date,
                            seed.cross_id,
                            seed.harvest_source,
                            seed.germplasm_id,
                            seed.program_id
                        FROM
                            germplasm.seed seed
                        WHERE
                            seed.is_void = FALSE
                            ${dataFilter['seedNames'] ?? dataFilter['seedCodes']}
                        GROUP BY
                            seed.id
                    ) AS seed
                `

                seedDataJoinQuery += `
                JOIN germplasm.germplasm germplasm 
                    ON seed.germplasm_id = germplasm.id 
                    AND germplasm.is_void = FALSE
            `
        }

        return {
            'seedDataSelectQuery': seedDataSelectQuery,
            'seedDataJoinQuery': seedDataJoinQuery
        }
    },

    /**
     * Retrieve seed level query statement data
     * @param {*} isSource if data level source
     * @param {*} dataFilter filters
     * @param {*} dataLevel data level
     * 
     * @returns mixed
     */
    getPackageData: async (isSource = false, dataFilter = [], dataLevel) => {
        let packageDataSelectQuery = ``

        packageDataSelectQuery = `
            seed.program_id AS "seedProgramDbId",  
            package.package_code AS "packageCode",
            package.package_label AS "label",
            package.package_quantity AS "quantity",
            package.package_unit AS "unit",
            package.package_document AS "packageDocument",
            package.package_status AS "packageStatus",
            package.package_reserved AS "packageReserved",
        `

        // by default, packages are JOINED to the seed and germplasm data
        let packageDataJoinQuery = ``

        if(isSource && Object.keys(dataFilter).length > 0 && 
        ((dataFilter['labels'] != undefined && dataFilter['labels'] != '') ||
         (dataFilter['packageCodes'] != undefined && dataFilter['packageCodes'] != ''))){
            packageDataSelectQuery += ` package."package_id" AS "packageDbId", `

            packageDataJoinQuery = `
                (
                    SELECT
                        package.id as "package_id",
                        package.package_code,
                        package.package_label,
                        package.package_quantity,
                        package.package_unit,
                        package.package_document,
                        package.package_status,
                        package.package_reserved,
                        package.seed_id,
                        package.program_id,
                        package.facility_id,
                        package.creator_id,
                        package.modifier_id,
                        package.is_void,
                        package.id
                    FROM
                        germplasm.package package
                    WHERE
                        package.is_void = FALSE
                        ${dataFilter['labels'] ?? dataFilter['packageCodes']}
                    GROUP BY
                        package.id
                ) AS package
            `

            packageDataJoinQuery += `
                JOIN germplasm.seed seed 
                    ON seed.id = package.seed_id 
                    AND seed.is_void = FALSE
                JOIN germplasm.germplasm germplasm 
                    ON seed.germplasm_id = germplasm.id 
                    AND seed.is_void = FALSE
            `
        }

        return {
            'packageDataSelectQuery': packageDataSelectQuery,
            'packageDataJoinQuery': packageDataJoinQuery
        }
    },
    
    /**
     * Retrieve query statements for retrieval of package-level data
     * 
     * @param {String} sourceAlias 
     * 
     * @returns mixed
     */
    getAdditionalPackageData: async (sourceAlias) => {

        let variablesConfig = await getConfigByAbbrev('FIND_SEEDS_QUERY_RESULTS');
        let variablesArr = variablesConfig[0][0]['config_value']['values'] ?? [];

        let dataQueryInfo = await buildDataQuery('baseQuery', sourceAlias, variablesArr);

        let dataQuery = dataQueryInfo['query'] !== "" ? `${dataQueryInfo['query']},` : "";
        let dataQuerySelect = dataQueryInfo['selectStatement'] !== "" ? `${dataQueryInfo['selectStatement']},` : "";
        let dataQueryJoin = dataQueryInfo['joinStatement'] !== "" ? `${dataQueryInfo['joinStatement']}` : "";

        let packageDataQuery = `
            ${dataQuery}
            germplasmMta AS (
                SELECT
                    germplasm_mta.seed_id,
                    germplasm_mta.germplasm_id,
                    STRING_AGG( germplasm_mta.mta_status::text, ',') AS mta_status,
                    STRING_AGG( germplasm_mta.mls_ancestors::text, ',') AS mls_ancestors,
                    STRING_AGG( germplasm_mta.genetic_stock::text, ',') AS genetic_stock
                FROM
                    germplasm.germplasm_mta germplasm_mta
                    JOIN baseQuery
                        ON baseQuery."seedDbId" = germplasm_mta.seed_id
                        AND baseQuery."germplasmDbId" = germplasm_mta.germplasm_id
                WHERE
                    germplasm_mta.is_void = FALSE
                GROUP BY germplasm_mta.seed_id, germplasm_mta.germplasm_id
            )
        `

        let packageDataSelectQuery = `
            ${dataQuerySelect}
            germplasmMta.mta_status AS "mtaStatus",
            germplasmMta.mls_ancestors AS "mlsAncestors",
            germplasmMta.genetic_stock AS "geneticStock"
        `

        let packageDataJoinQuery = `
            ${dataQueryJoin}
            LEFT JOIN germplasmMta germplasmMta
                ON germplasmMta.germplasm_id = t."germplasmDbId" 
                AND germplasmMta.seed_id = t."seedDbId"
        `

        return {
            'packageDataQuery': packageDataQuery,
            'packageDataJoinQuery': packageDataJoinQuery,
            'packageDataSelectQuery': packageDataSelectQuery
        }
    },

    /**
     * Transform text to camelCase
     * @param {*} text 
     * @returns 
     */
    toCamelCase: (text) => {
        return text.toLowerCase().replace(
                    /(?:^\w|[A-Z]|\b\w)/g, 
                    (ltr, idx) => idx === 0 ? ltr.toLowerCase() : ltr.toUpperCase()
                ).replace(/\s+/g, '')
    }
}