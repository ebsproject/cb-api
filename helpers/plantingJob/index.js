/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let errors = require('restify-errors')
const endpoint = 'planting-jobs/:id/entry-generations'

module.exports = {
    
    /**
     * Generate planting job entries given planting job ID
     *
     * @param Integer plantingJobDbId planting job identifier
     * @param Integer personDbId person identifier
     * @param Object res response
     */
    generatePlantingJobEntries: async (plantingJobDbId, personDbId, res) => {
        let insertCount = 0

        // generate planting job entry records
        query = `
            WITH rows AS (
                INSERT INTO 
                    experiment.planting_job_entry
                    (planting_job_occurrence_id, entry_id, envelope_count, required_package_quantity, required_package_unit, is_sufficient, replacement_type, creator_id)
                    (
                        WITH z AS (
                            WITH x AS (
                                WITH y AS (
                                    WITH s AS (
                                        WITH t AS
                                        (
                                            SELECT
                                                entry.id AS entry_id,
                                                entry.entry_code,
                                                COUNT(entry.entry_code) AS occurrence_count
                                             FROM 
                                                experiment.planting_job_occurrence planting_job_occurrence,
                                                experiment.occurrence occurrence,
                                                experiment.experiment experiment,
                                                experiment.entry_list entry_list,
                                                experiment.entry entry
                                            WHERE
                                                planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                                                AND planting_job_occurrence.is_void = FALSE
                                                AND planting_job_occurrence.occurrence_id = occurrence.id
                                                AND occurrence.experiment_id = experiment.id
                                                AND experiment.id = entry_list.experiment_id
                                                AND entry_list.is_void = FALSE
                                                AND entry.entry_list_id = entry_list.id
                                                AND entry.is_void = FALSE
                                            GROUP BY
                                                entry.entry_code, entry.id
                                            ORDER BY entry.entry_number
                                        )
                                        SELECT
                                            planting_job_occurrence.id AS planting_job_occurrence_id,
                                            entry.id AS entry_id,
                                            package.package_quantity package_quantity,
                                            planting_job_occurrence.seeds_per_envelope,
                                            planting_job_occurrence.envelopes_per_plot,
                                            planting_job_occurrence.package_unit AS required_package_unit,
                                            count(plot.id) AS entry_plot_count,
                                            t.occurrence_count
                                        FROM
                                            t,
                                            experiment.planting_job_occurrence planting_job_occurrence,
                                            experiment.occurrence occurrence,
                                            experiment.experiment experiment,
                                            experiment.entry_list entry_list,
                                            experiment.plot plot,
                                            experiment.entry entry
                                            LEFT JOIN
                                                germplasm.package package
                                            ON
                                                entry.package_id = package.id
                                                AND package.is_void = FALSE
                                        WHERE
                                            planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                                            AND planting_job_occurrence.is_void = FALSE
                                            AND planting_job_occurrence.occurrence_id = occurrence.id
                                            AND occurrence.experiment_id = experiment.id
                                            AND experiment.id = entry_list.experiment_id
                                            AND entry_list.is_void = FALSE
                                            AND entry.entry_list_id = entry_list.id
                                            AND entry.is_void = FALSE
                                            AND plot.entry_id = entry.id
                                            AND plot.is_void = FALSE
                                            AND plot.occurrence_id = occurrence.id
                                            AND entry.entry_code = t.entry_code
                                            AND entry.id = t.entry_id
                                        GROUP BY
                                            entry.id, planting_job_occurrence.id, package.package_quantity, package.package_unit, t.occurrence_count
                                        ORDER BY
                                            planting_job_occurrence.id, entry.entry_number
                                    )
                                    SELECT
                                        *,
                                        (s.envelopes_per_plot * s.entry_plot_count) as envelope_count
                                    FROM
                                    s
                                )
                                SELECT
                                    y.*,
                                    (y.seeds_per_envelope * y.envelope_count) AS required_package_quantity
                                FROM
                                    y
                            )
                            SELECT
                                x.*,
                                (x.required_package_quantity * occurrence_count) AS total_required_package_quantity
                            FROM
                                x
                        )
                        SELECT
                            z.planting_job_occurrence_id,
                            z.entry_id,
                            z.envelope_count,
                            z.required_package_quantity,
                            z.required_package_unit,
                            CASE
                            WHEN z.package_quantity < z.total_required_package_quantity OR z.package_quantity IS NULL
                                THEN FALSE
                            ELSE
                                TRUE
                            END AS is_sufficient,
                            'replace',
                            ${personDbId}
                        FROM
                            z
                    )
                RETURNING 1
            )
            SELECT count(*) FROM rows;
        `

        let insertResult = await sequelize.transaction(async transaction => { 
            return await sequelize.query(query, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction,
                raw: true
            })
        }).catch(async err => {
            logger.logFailingQuery(endpoint, 'INSERT', err)
            throw new Error(err)
        })

        insertCount = parseInt(insertResult[0][0]['count'])

        return insertCount
    },

    /**
     * Compute derived planting job entry values given planting job ID
     *
     * @param Integer plantingJobDbId planting job identifier
     * @param Integer personDbId person identifier
     * @param Object res response
     */
    computeDerivedValues: async (plantingJobDbId, personDbId, res) => {
        query = `
            WITH rows AS (
                WITH subquery AS (
                    WITH z AS (
                        WITH x AS (
                            WITH y AS (
                                WITH s AS (
                                    WITH t AS
                                    (
                                        SELECT
                                            entry.id AS entry_id,
                                            entry.entry_code,
                                            COUNT(entry.entry_code) AS occurrence_count
                                         FROM 
                                            experiment.planting_job_occurrence planting_job_occurrence,
                                            experiment.occurrence occurrence,
                                            experiment.experiment experiment,
                                            experiment.entry_list entry_list,
                                            experiment.entry entry
                                        WHERE
                                            planting_job_occurrence.is_void = FALSE
                                            AND planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                                            AND planting_job_occurrence.notes = 'UPDATED'
                                            AND planting_job_occurrence.occurrence_id = occurrence.id
                                            AND occurrence.experiment_id = experiment.id
                                            AND experiment.id = entry_list.experiment_id
                                            AND entry_list.is_void = FALSE
                                            AND entry.entry_list_id = entry_list.id
                                            AND entry.is_void = FALSE
                                        GROUP BY
                                            entry.entry_code, entry.id
                                        ORDER BY entry.entry_number
                                    )
                                    SELECT
                                        planting_job_occurrence.id AS planting_job_occurrence_id,
                                        entry.id AS entry_id,
                                        package.package_quantity package_quantity,
                                        planting_job_occurrence.seeds_per_envelope,
                                        planting_job_occurrence.envelopes_per_plot,
                                        planting_job_occurrence.package_unit AS required_package_unit,
                                        count(plot.id) AS entry_plot_count,
                                        t.occurrence_count
                                    FROM
                                        t,
                                        experiment.planting_job_occurrence planting_job_occurrence,
                                        experiment.occurrence occurrence,
                                        experiment.experiment experiment,
                                        experiment.entry_list entry_list,
                                        experiment.plot plot,
                                        experiment.entry entry
                                        LEFT JOIN
                                            germplasm.package package
                                        ON
                                            entry.package_id = package.id
                                            AND package.is_void = FALSE
                                    WHERE
                                        planting_job_occurrence.is_void = FALSE
                                        AND planting_job_occurrence.planting_job_id = ${plantingJobDbId}
                                        AND planting_job_occurrence.notes = 'UPDATED'
                                        AND planting_job_occurrence.occurrence_id = occurrence.id
                                        AND occurrence.experiment_id = experiment.id
                                        AND experiment.id = entry_list.experiment_id
                                        AND entry_list.is_void = FALSE
                                        AND entry.entry_list_id = entry_list.id
                                        AND entry.is_void = FALSE
                                        AND plot.entry_id = entry.id
                                        AND plot.is_void = FALSE
                                        AND plot.occurrence_id = occurrence.id
                                        AND entry.entry_code = t.entry_code
                                        AND entry.id = t.entry_id
                                    GROUP BY
                                        entry.id, planting_job_occurrence.id, package.package_quantity, package.package_unit, t.occurrence_count
                                    ORDER BY
                                        planting_job_occurrence.id, entry.entry_number
                                )
                                SELECT
                                    *,
                                    (s.envelopes_per_plot * s.entry_plot_count) as envelope_count
                                FROM
                                s
                            )
                            SELECT
                                y.*,
                                (y.seeds_per_envelope * y.envelope_count) AS required_package_quantity
                            FROM
                                y
                        )
                        SELECT
                            x.*,
                            (x.required_package_quantity * occurrence_count) AS total_required_package_quantity
                        FROM
                            x
                    )
                    SELECT
                        z.planting_job_occurrence_id,
                        z.entry_id,                            
                        CASE
                        WHEN z.package_quantity < z.total_required_package_quantity OR z.package_quantity IS NULL
                            THEN FALSE
                        ELSE
                            TRUE
                        END AS is_sufficient,
                        z.required_package_quantity,
                        z.required_package_unit,
                        z.envelope_count,
                        ${personDbId} as modifier_id,
                        NOW() as modification_timestamp
                    FROM
                        z
                )
                UPDATE 
                    experiment.planting_job_entry pje
                SET
                    required_package_quantity = subquery.required_package_quantity,
                    required_package_unit = subquery.required_package_unit,
                    envelope_count = subquery.envelope_count,
                    modifier_id = subquery.modifier_id,
                    modification_timestamp = subquery.modification_timestamp,
                    is_sufficient = subquery.is_sufficient
                FROM
                    subquery
                WHERE
                    pje.entry_id = subquery.entry_id
                    AND pje.planting_job_occurrence_id = subquery.planting_job_occurrence_id
                RETURNING 1
            )
            SELECT count(*) FROM rows
        `

        let result = await sequelize.transaction(async transaction => {
            return await sequelize.query(query, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction,
                raw: true
            })
        }).catch(async err => {
            logger.logFailingQuery(endpoint, 'UPDATE', err)
            throw new Error(err)
        })

        count = parseInt(result[0][0]['count'])

        return count

    }
}