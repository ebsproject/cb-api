/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let Sequelize = require('sequelize')
let Op = Sequelize.Op
let errors = require('restify-errors')
const errorBuilder = require('../error-builder')

module.exports = {

    /**
     * Retrieve the sql query for getting the experiment record
     * 
     * @param addedConditionString string added condition string for experiment
     * @param addedColumnString string added columns for experiment
     * @param addedDistinctString
     * @param distinctColumn
     * 
     * @return sql query string
     */
    getExperimentQuerySql: async (addedConditionString = '', addedColumnString = '', addedDistinctString = '', distinctColumn = '', ownedConditionString = '', permissionCond = '') => {

        let experimentQuery = `
            SELECT
                ${addedDistinctString}
                experiment.id AS "experimentDbId",
                experiment.experiment_year AS "experimentYear",
                program.id AS "programDbId",
                program.program_code AS "programCode",
                program.program_name AS "programName",
                item.display_name AS "experimentTemplate",
                pipeline.id AS "pipelineDbId",
                pipeline.pipeline_code AS "pipelineCode",
                pipeline.pipeline_name AS "pipelineName",
                stage.id AS "stageDbId",
                stage.stage_code AS "stageCode",
                stage.stage_name AS "stageName",
                project.id AS "projectDbId",
                project.project_code AS "projectCode",
                project.project_name AS "projectName",
                season.id AS "seasonDbId",
                season.season_code AS "seasonCode",
                season.season_name AS "seasonName",
                crop.id AS "cropDbId",
                crop.crop_code AS "cropCode",
                crop.crop_name AS "cropName",
                experiment.planting_season AS "plantingSeason",
                experiment.experiment_code AS "experimentCode",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_objective AS "experimentObjective",
                experiment.experiment_type AS "experimentType",
                experiment.experiment_sub_type AS "experimentSubType",
                experiment.experiment_sub_sub_type AS "experimentSubSubType",
                experiment.experiment_design_type AS "experimentDesignType",
                experiment.experiment_status AS "experimentStatus",
                experiment.description,
                experiment.remarks,
                experiment.data_process_id AS "dataProcessDbId",
                item.abbrev AS "dataProcessAbbrev",
                experiment.steward_id AS "stewardDbId",
                person.person_name AS "steward",
                experiment.experiment_plan_id AS "experimentPlanDbId",
                (
                    SELECT
                        count(e.id)
                    FROM
                        experiment.entry_list el,
                        experiment.entry e
                    WHERE
                        el.experiment_id = experiment.id AND
                        e.entry_list_id = el.id AND
                        e.is_void = FALSE
                )::int AS "entryCount",
                (
                    SELECT
                        count(o.id)
                    FROM
                        experiment.occurrence o
                    WHERE
                        o.experiment_id = experiment.id AND
                        o.is_void = FALSE
                )::int AS "occurrenceCount",
                experiment.experiment_document AS "experimentDocument",
                experiment.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS "creator",
                experiment.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS "modifier",
                experiment.notes
                ` + addedColumnString + `
            FROM
                experiment.experiment experiment
            LEFT JOIN
                master.item item ON item.id = experiment.data_process_id
            LEFT JOIN
                experiment.experiment_plan experiment_plan ON experiment_plan.id = experiment.experiment_plan_id
            LEFT JOIN
                tenant.pipeline pipeline ON pipeline.id = experiment.pipeline_id
            JOIN
                tenant.program program ON program.id = experiment.program_id
            LEFT JOIN
                tenant.project project ON project.id = experiment.project_id
            JOIN
                tenant.season season ON season.id = experiment.season_id
            JOIN
                tenant.stage stage ON stage.id = experiment.stage_id
            LEFT JOIN
                tenant.crop crop ON crop.id = experiment.crop_id
            LEFT JOIN
                tenant.person person ON person.id = experiment.steward_id
            JOIN
                tenant.person creator ON creator.id = experiment.creator_id
            LEFT JOIN
                tenant.person modifier ON modifier.id = experiment.modifier_id
            WHERE
                experiment.is_void = FALSE
                ${ownedConditionString}
                ${permissionCond}
                `   + addedConditionString + `
            ORDER BY
                ${distinctColumn}
                experiment.id DESC
        `
        return experimentQuery
    },

    generateCode: async (pattern, programDbId, stageDbId, experimentYear, seasonDbId) => {

        let code = []

        for (i = 0; i < pattern.length; i++) {
            let value = ''
            if (pattern[i]["is_variable"]) {
                if (pattern[i]["value"] == "YEAR") {
                    code.push(experimentYear)
                }
                if (pattern[i]["value"] == "PROGRAM_CODE") {
                    value = await getEntityValue('tenant', 'program', 'program_code', programDbId)
                    code.push(value['program_code'])
                }
                if (pattern[i]["value"] == "STAGE_CODE") {
                    value = await getEntityValue('tenant', 'stage', 'stage_code', stageDbId)
                    code.push(value['stage_code'])
                }
                if (pattern[i]["value"] == "SEASON_CODE") {
                    value = await getEntityValue('tenant', 'season', 'season_code', seasonDbId)
                    code.push(value['season_code'])
                }
            } else {
                if (pattern[i]["value"] == "-") {
                    code.push('-')
                }
                if (pattern[i]["value"] == "COUNTER") {
                    let counter = 1

                    let columns = pattern[i]["max_value"].split('|');
                    let condition = []
                    let selectColumns = []
                    for (column of columns) {
                        if (column == "PROGRAM") {
                            condition.push('program_id =' + programDbId)
                            selectColumns.push('program_id')
                        }

                        if (column == "SEASON") {
                            condition.push('season_id =' + seasonDbId)
                            selectColumns.push('season_id')
                        }

                        if (column == "YEAR") {
                            selectColumns.push('experiment_year')
                            condition.push('experiment_year =' + experimentYear)
                        }

                        if (column == "STAGE") {
                            selectColumns.push('stage_id')
                            condition.push('stage_id =' + stageDbId)
                        }

                    }
                    selectColumns = selectColumns.join(", ")
                    let conditionStr = condition.join(" AND ");
                    counter = await getExperimentCodeCounter('experiment', 'experiment', conditionStr, selectColumns)
                    counter = await getPaddedNumber(counter, pattern[i]["zero_padding"], parseInt(pattern[i]["digits"]))
                    code.push(counter)
                }
            }
        }

        return code.join("")
    },

    /**
     * Retrieve the pattern of configuration based on abbrev
     * @param String configAbbrev 
     */
    getPattern: async (configAbbrev) => {
        let configQuery = `
            SELECT 
                config_value
            FROM
                platform.config
            WHERE
                abbrev ='${configAbbrev}'
                AND is_void = false
        `
        let configValue = await sequelize.query(configQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        return configValue[0]['config_value']['pattern']
    },
    /**
     * Generate the entry code based on configuration of an occurrence
     * @param object params Request body
     * @param integer experimentDbId experiment identifier 
     * @param object res Response
     * @return integer count of updated entries
     */
    generateOccurrenceEntryCode: async (params, experimentDbId, res) => {
        let updateCount = 0
        let query = `
            SELECT 
                id
            FROM experiment.occurrence
            WHERE
                is_void = FALSE
                AND experiment_id = ${experimentDbId}
        `
        let occurrenceListQuery = await sequelize.query(query, {
            type: sequelize.QueryTypes.SELECT
        })

        let occurrenceDbId
        for (var key of Object.keys(params)) {
            let occurrenceMatch = false
            for (let occurrence of occurrenceListQuery) {
                if (occurrence['id'] == key) {
                    occurrenceDbId = occurrence['id']
                    occurrenceMatch = true
                    query = `
                        SELECT 
                            data_value::json
                        FROM experiment.occurrence_data
                        WHERE
                            variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'ENTRY_CODE_PATTERN')
                            AND is_void = FALSE
                            AND occurrence_id = ${occurrenceDbId}
                        LIMIT 1
                    `
                    let occurrenceQuery = await sequelize.query(query, {
                        type: sequelize.QueryTypes.SELECT
                    })
                    if (occurrenceQuery.length == 0) {
                        let errMsg = `Ensure that there is a pattern to generate entry code for occurrenceDbId ` + occurrenceDbId + `.`
                        res.send(new errors.BadRequestError(errMsg))
                        return false
                    }
                }
            }
            if (!occurrenceMatch) {
                let errMsg = `Invalid Request. Ensure that the occurrenceDbId is an occurrence of the experiment.`
                res.send(new errors.BadRequestError(errMsg))
                return false
            }
        }

        for (let occurrence of occurrenceListQuery) {
            let offset
            occurrenceDbId = occurrence['id']

            if (params[occurrenceDbId] != undefined) {
                if (params[occurrenceDbId]["offset"] == undefined) {

                    let errMsg = await errorBuilder.getError(req.headers.host, 400245)
                    res.send(new errors.BadRequestError(errMsg))
                    return false
                }
                offset = params[occurrenceDbId]["offset"]
            } else {
                continue
            }

            query = `
                SELECT 
                    data_value::json
                FROM experiment.occurrence_data
                WHERE
                    variable_id IN (SELECT id FROM master.variable WHERE abbrev = 'ENTRY_CODE_PATTERN')
                    AND is_void = FALSE
                    AND occurrence_id = ${occurrenceDbId}
                LIMIT 1
            `
            let occurrenceQuery = await sequelize.query(query, {
                type: sequelize.QueryTypes.SELECT
            })
            if (occurrenceQuery.length > 0) {

                pattern = occurrenceQuery[0]['data_value'];
                let queryFrom = []
                let querySelect = []
                let counter = 1
                let series = []
                for (let patternFormat of pattern["pattern"]) {

                    if (patternFormat["is_variable"]) {

                        // we accept experiment, entry, occurrence columns
                        if (patternFormat['entity_table'] == 'experiment.experiment'
                            || patternFormat['entity_table'] == 'experiment.entry'
                            || patternFormat['entity_table'] == 'experiment.occurrence'
                            || patternFormat['entity_table'] == 'experiment.planting_instruction'
                        ) {
                            querySelect.push(patternFormat['entity_column'])
                        } else {

                            // add column in select
                            querySelect.push(patternFormat['entity_column'])
                            // add reference table, assumption: reference tables that can only 
                            // be reference using experiment, occurrence and entry

                            const fromStr = `
                                LEFT JOIN
                                    ${patternFormat['entity_table']}
                                ON
                                    ${patternFormat['entity_table']}.${patternFormat['entity_reference_column']} = ${patternFormat['entity_reference_table']}
                            ` 

                            if (queryFrom.includes(fromStr)) continue

                            queryFrom.push(fromStr)
                        }
                    } else if (patternFormat['value'] == 'COUNTER') {
                        let padding = ''
                        let counterOnly = ''

                        // This generates numbers wihout zero padding and number length restrictions (e.g. 501, 502, ..., 600, ..., 1000, ...)
                        if (!patternFormat['zero_padding'] || !patternFormat['digits']) {
                            counterOnly = `( ${offset - 1}+ ROW_NUMBER() OVER (ORDER BY e.entry_number ))::text AS pattern` + counter
                            series.push(counterOnly)
                            querySelect.push(`pattern${counter}`)

                            continue // Move on to the next iteration
                        }

                        // check if trailing or leading
                        if (patternFormat["zero_padding"] == "leading") {
                            padding = 'lpad'
                        } else {
                            padding = 'rpad'
                        }
                        // add counter in select column
                        padding = '(' + padding + `(( ${offset - 1}+ ROW_NUMBER() OVER (ORDER BY e.entry_number ))::text, ` + parseInt(patternFormat["digits"]) + `, '0')) AS pattern` + counter
                        series.push(padding)
                        querySelect.push(`pattern${counter}`)
                    } else {
                        // add character(s) in select column
                        let patternStr = patternFormat['value']
                        querySelect.push(`'${patternStr}'`)
                    }
                    counter++
                }

                let selectString = querySelect.join(',')
                let fromString = ``
                let seriesString = ``

                if (queryFrom.length > 0) {
                    fromString = queryFrom.join('')
                }
                if (series.length > 0) {
                    seriesString = `,` + series.join(',')
                }

                query = `
                WITH rows AS (
                    UPDATE
                        experiment.planting_instruction 
                    SET entry_code = pattern.code
                    FROM(
                        SELECT 
                          
                            CONCAT(${selectString}) AS code,
                            planting_instruction.id as planting_instruction_id
                            
                        FROM 
                            (
                                SELECT 
                                    entry.id
                                    ${seriesString}
                                from 
                                    (
                                    SELECT 
                                        DISTINCT (planting_instruction.entry_id) AS id
                                        
                                    FROM 
                                        experiment.planting_instruction 
                                        LEFT JOIN experiment.plot ON plot.id = planting_instruction.plot_id 
                                        LEFT JOIN experiment.entry ON entry.id= plot.entry_id
                                        LEFT JOIN experiment.occurrence ON occurrence.id = plot.occurrence_id
        
                                    where
                                        planting_instruction.is_void = FALSE AND
                                        occurrence.is_void = FALSE AND 
                                        entry.is_void = FALSE
                                        AND occurrence.id = ${occurrenceDbId}
                                )entry
                                LEFT JOIN experiment.entry e ON entry.id = e.id
                            )occurrence_entry
                            LEFT JOIN experiment.planting_instruction ON planting_instruction.entry_id = occurrence_entry.id
                            LEFT JOIN experiment.plot ON plot.id = planting_instruction.plot_id 
                            LEFT JOIN experiment.entry ON entry.id= plot.entry_id	
                            LEFT JOIN experiment.occurrence ON occurrence.id = plot.occurrence_id
                            LEFT JOIN experiment.experiment ON experiment.id = occurrence.experiment_id
                            ${fromString}
                        WHERE
                            planting_instruction.is_void = FALSE AND
                            occurrence.is_void = FALSE AND 
                            entry.is_void = FALSE
                            AND occurrence.id = ${occurrenceDbId}
                        
                    )pattern
                    WHERE
                        planting_instruction.id = pattern.planting_instruction_id
                    RETURNING 1
                )
                SELECT COUNT(1) FROM rows
                `
                let updateResult = await sequelize.query(query, {
                    type: sequelize.QueryTypes.UPDATE
                })

                updateCount += parseInt(updateResult[0][0]['count'])

            }

        }
        return updateCount

    },
    /**
     * Generate the entry code based on configuration of an experiment
     * @param object params Request body
     * @param integer experimentDbId Experiment identifier 
     * @param array pattern Pattern for entry code
     * @param object res Response
     * @return integer Count of updated entries
     */
    generateExperimentEntryCode: async (params, experimentDbId, pattern, res) => {
        let offset = 0
        pattern = pattern[0]['data_value'];
        let queryFrom = []
        let querySelect = []
        let counter = 1
        let series = []

        for (let patternFormat of pattern["pattern"]) {

            if (patternFormat["is_variable"]) {

                // we accept experiment, entry columns
                if (patternFormat['entity_table'] == 'experiment.experiment'
                    || patternFormat['entity_table'] == 'experiment.entry'
                    || patternFormat['entity_table'] == 'experiment.occurrence'
                    || patternFormat['entity_table'] == 'experiment.planting_instruction') {

                    // add column as pattern
                    querySelect.push(patternFormat['entity_column'])

                } else {

                    // add column as pattern
                    querySelect.push(patternFormat['entity_column'])

                    const fromStr = `
                        LEFT JOIN
                            ${patternFormat['entity_table']}
                        ON
                            ${patternFormat['entity_table']}.${patternFormat['entity_reference_column']} = ${patternFormat['entity_reference_table']}
                    `

                    if (queryFrom.includes(fromStr)) continue

                    // add reference table, assumption: reference tables that can only 
                    // be reference using experiment and entry
                    queryFrom.push(fromStr)
                }
            } else if (patternFormat['value'] == 'COUNTER') {

                let padding = ''
                let counterOnly = ''

                if (params.offset != undefined) {
                    offset = params.offset
                } else {
                    let errMsg = `Required parameters are missing. Ensure that offset field is not empty.`
                    res.send(new errors.BadRequestError(errMsg))
                    return false
                }

                // This generates numbers wihout zero padding and number length restrictions (e.g. 501, 502, ..., 600, ..., 1000, ...)
                if (!patternFormat['zero_padding'] || !patternFormat['digits']) {
                    counterOnly = `( ${offset - 1}+ ROW_NUMBER() OVER (PARTITION BY occurrence.experiment_id ORDER BY occurrence.occurrence_number,entry.entry_number ))::text AS pattern` + counter
                    series.push(counterOnly)
                    querySelect.push(`pattern${counter}`)

                    continue // Move on to the next iteration
                }

                // check if trailing or leading
                if (patternFormat["zero_padding"] == "leading") {
                    padding = 'lpad'
                } else {
                    padding = 'rpad'
                }

                // add counter in select column
                padding = '(' + padding + `(( ${offset - 1}+ ROW_NUMBER() OVER (PARTITION BY occurrence.experiment_id ORDER BY occurrence.occurrence_number,entry.entry_number ))::text, ` + parseInt(patternFormat["digits"]) + `, '0')) AS pattern` + counter
                series.push(padding)
                querySelect.push(`pattern${counter}`)
            } else {
                // add character(s) in the select column
                let patternStr = patternFormat['value']
                querySelect.push(`'${patternStr}'`)
            }
        }
        // build select columns
        let selectString = querySelect.join(',')
        let fromString = ``

        // build LEFT JOIN 
        if (queryFrom.length > 0) {
            fromString = queryFrom.join('')
        }

        if (series.length > 0) {
            seriesString = `,` + series.join(',')
        }

        query = `

            WITH rows AS (
                UPDATE
                    experiment.planting_instruction 
                SET entry_code = pattern.code
                FROM(
                    SELECT 
                        CONCAT(${selectString}) AS code,
                        planting_instruction.id as planting_instruction_id

                    FROM 
                        experiment.planting_instruction 
                        LEFT JOIN experiment.plot ON plot.id = planting_instruction.plot_id
                        LEFT JOIN(                                 
                            SELECT 
                                occurrence.id as occurrence_id,
                                entry.id
                                ${seriesString}
                                    
                                FROM 
                                    experiment.planting_instruction 
                                    LEFT JOIN experiment.plot ON plot.id = planting_instruction.plot_id 
                                    LEFT JOIN experiment.entry ON entry.id= plot.entry_id
                                    LEFT JOIN experiment.occurrence ON occurrence.id = plot.occurrence_id

                                WHERE
                                    planting_instruction.is_void = FALSE AND
                                    occurrence.is_void = FALSE AND 
                                    entry.is_void = FALSE
                                    AND occurrence.experiment_id = ${experimentDbId}
                                    GROUP BY occurrence.id, entry.id

                        )occurrence_entry 
                            ON planting_instruction.entry_id = occurrence_entry.id 
                         	AND occurrence_entry.occurrence_id= plot.occurrence_id
                        LEFT JOIN experiment.entry ON entry.id= plot.entry_id
                        LEFT JOIN experiment.occurrence ON occurrence.id = plot.occurrence_id
                        LEFT JOIN experiment.experiment ON experiment.id = occurrence.experiment_id
                        
                        ${fromString}   
                    WHERE
                        planting_instruction.is_void = FALSE AND
                        occurrence.is_void = FALSE AND 
                        entry.is_void = FALSE
                        AND experiment.id = ${experimentDbId}
                    
                )pattern
                WHERE
                    planting_instruction.id = pattern.planting_instruction_id
                RETURNING 1
            ) 
            SELECT COUNT(1) FROM rows
            `

        let updateResult = await sequelize.query(query, {
            type: sequelize.QueryTypes.UPDATE
        })
        return parseInt(updateResult[0][0]['count'])

    }

}
/**
 * Pad a number
 * @param Integer counter Counter value
 * @param String paddingPosition value is either trailing or leading 
 * @param String width Number of expected digits
 */
async function getPaddedNumber(counter, paddingPosition, width) {

    counter = counter + '';
    if (paddingPosition == 'leading') {
        return counter.length >= width ? counter : new Array(width - counter.length + 1).join('0') + counter;
    } else {
        return counter + counter.length >= width ? counter : new Array(width - counter.length + 1).join('0');
    }

}
/**
 * Retrieve the value of a record
 * @param String schema Database schema name
 * @param String entity Database table
 * @param String entityColumn Database column of table 
 * @param Integer entityId Database table identifier
 */
async function getEntityValue(schema, entity, entityColumn, entityId) {
    let query = `
        SELECT 
            ${entityColumn}
        FROM
            ${schema}.${entity}
        WHERE
            id = '${entityId}'
            AND is_void = FALSE
    `
    let value = await sequelize.query(query, {
        type: sequelize.QueryTypes.SELECT
    })

    return value[0]
}

/**
 * Get the next count of experiment based on conditions
 * @param String schema Database schema name
 * @param String entity Database table
 * @param String condition Custom condition to retrieve counter
 * @param String columns Columns used to group the experiment
 */
async function getExperimentCodeCounter(schema, entity, condition, columns) {

    let counter = 1
    let counterQuery = `
        SELECT 
            MAX("orderNumber") + 1 as max
        FROM (
            SELECT 
                ROW_NUMBER() OVER (PARTITION BY ${columns} ORDER BY ${columns} ) AS "orderNumber"
            FROM 
                ${schema}.${entity}
            WHERE
                ${condition}
        )e
    `
    let maxCounter = await sequelize.query(counterQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    if (maxCounter[0]['max'] == undefined) {
        return counter
    }

    return maxCounter[0]['max']
}