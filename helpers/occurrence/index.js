/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')
let patternHelper = require('../patternGenerator/index.js')
let logger = require('../logger/index.js')
let errors = require('restify-errors')

/**
 * 
 * @param {String} value inputted distinct column value
 * @param {Array} referenceTable column alias reference array
 * @returns distinctStr distinct string statement
 */
async function getDistictParameter(value, referenceTable){
    let distinctStr = ''

    if(value != undefined && value != null && value != ''){
        for (col of referenceTable) {
            if(col == value){
                distinctStr = `DISTINCT ON ("${value.trim()}")`
            }
        }
    }

    return distinctStr;
}

/**
 * 
 * @param {Array} parameters search parameters
 * @param {Array} referenceTable column alias reference array
 * @param {String} sourceTable source table name; default empty
 * @returns newSelectStr column table alias statement
 */
async function getColumnsParameter(parameters, referenceTable, sourceTable = ''){
    let newSelectStr = ''

    if(parameters['fields']){
        let fieldsParam = parameters['fields'].split('|')
        let splitVal = []
        let newVal = ''
        let newColName = ''

        if(fieldsParam.length > 0){
            let ind = 0
            for(var param of fieldsParam){
                splitVal = param.split(/ as /i)

                for(var colParam of Object.entries(referenceTable)){
                    newVal = colParam[1].trim()

                    // if has AS in fields values
                    if(splitVal.length > 1 && colParam[0].trim() == splitVal[0].trim()){
                        newVal = splitVal[1].trim()
                        newColName = colParam[0].trim().split('.')

                        newSelectStr += ( sourceTable == '' ? 
                                        ` "${newColName[0]}".${newColName[1]}` :
                                        ` ${sourceTable}."${colParam[1].trim()}"` )
                        newSelectStr += ` AS "${newVal}" `           

                        break
                    }

                    // if no AS, only column aliases
                    if(splitVal.length == 1 && newVal == splitVal[0].trim()){
                        newSelectStr += ( sourceTable == '' ? 
                                        ` ${colParam[0].trim()}."${colParam[1].trim()}"` :
                                        ` ${sourceTable}."${colParam[1].trim()}"` ) 
                        newSelectStr += ` AS "${newVal}" `

                        break
                    }
                }

                if((ind+1) < fieldsParam.length && newSelectStr != ''){
                    newSelectStr += `, `
                }

                ind += 1
            }
        }
    }
    
    return newSelectStr;
}

module.exports = {
    /**
     * Retrieve the sql query for getting the occurrences record
     * @param addedConditionString string added condition string for experiment
     * @param addedColumnString string added columns for experiment
     * @param addedDistinctString
     * @param distinctColumn
     * 
     * @return sql query string
     */
    getOccurrenceQuerySql: async(addedConditionString = '', addedColumnString = '', addedDistinctString = '', distinctColumn = '', ownedConditionString = '', isBasic = false, accessDataQuery = '', responseColString = '') => {

        let occurrenceQuery = null
        if(isBasic){
            occurrenceQuery = `
                SELECT
                    ${responseColString}
                    ${addedColumnString}
                FROM
                    experiment.occurrence occurrence
                JOIN
                    experiment.experiment experiment ON occurrence.experiment_id = experiment.id
                JOIN 
                    tenant.person creator ON creator.id = occurrence.creator_id
                LEFT JOIN
                    experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                LEFT JOIN
                    experiment.location location ON location.id = log.location_id
                LEFT JOIN
                    place.geospatial_object site ON site.id = occurrence.site_id
                LEFT JOIN
                    place.geospatial_object field ON field.id = occurrence.field_id
                JOIN
                    tenant.stage stage on stage.id = experiment.stage_id AND stage.is_void = FALSE
                JOIN
                    tenant.season season on season.id = experiment.season_id AND season.is_void = FALSE
                JOIN 
                    tenant.program program ON program.id = experiment.program_id AND program.is_void = FALSE
                LEFT JOIN 
                    tenant.person steward ON steward.id = experiment.steward_id
                WHERE
                    occurrence.is_void = FALSE
                    ${accessDataQuery}
                    ${ownedConditionString}
                    ` + addedConditionString + `
                ORDER BY
                    occurrence.id
                `
        } else {

            occurrenceQuery = `
                SELECT
                    ${responseColString}
                    ${addedColumnString},(
                        SELECT
                            data_value
                        FROM
                            experiment.occurrence_data occurrenceData
                        WHERE
                            occurrenceData.occurrence_id = occurrence.id
                            AND variable_id = (
                                SELECT
                                    id
                                FROM master.variable
                                WHERE abbrev = 'CONTCT_PERSON_CONT'
                            )
                            AND occurrenceData.is_void = false
                        LIMIT 1
                    ) AS "contactPerson"
                FROM
                    experiment.occurrence occurrence
                JOIN
                    experiment.experiment experiment ON occurrence.experiment_id = experiment.id
                LEFT JOIN
                    experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                LEFT JOIN
                    experiment.location location ON location.id = log.location_id
                LEFT JOIN 
                    tenant.person location_steward ON location_steward.id = location.steward_id
                LEFT JOIN 
                    place.geospatial_object goi ON goi.id = occurrence.geospatial_object_id
                LEFT JOIN
                    place.geospatial_object site ON site.id = occurrence.site_id
                LEFT JOIN
                    place.geospatial_object field ON field.id = occurrence.field_id
                JOIN 
                    tenant.person creator ON creator.id = occurrence.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = occurrence.modifier_id
                LEFT JOIN
                    tenant.project project on project.id = experiment.project_id
                JOIN
                    tenant.stage stage on stage.id = experiment.stage_id AND stage.is_void = FALSE
                JOIN
                    tenant.season season on season.id = experiment.season_id AND season.is_void = FALSE
                JOIN 
                    tenant.program program ON program.id = experiment.program_id AND program.is_void = FALSE
                LEFT JOIN 
                    tenant.person steward ON steward.id = experiment.steward_id
                LEFT JOIN
                    master.item item ON item.id = experiment.data_process_id
                WHERE
                    occurrence.is_void = FALSE
                    ${accessDataQuery}
                    ${ownedConditionString}
                    ` + addedConditionString + `
                ORDER BY
                    occurrence.id
                `
        }

        return occurrenceQuery
    },

    /**
     * Retrieve the sql query for getting the occurrences record including the 
     * @param {String} addedConditionString string added condition string for experiment
     * @param {String} addedColumnString string added columns for experiment
     * @param {String} addedDistinctString string added distinct condition
     * @param {String} distinctColumn distinct column
     * @param {String} ownedConditionString owned condition string
     * @param {Boolean} isBasic is basic
     * @param {String} accessDataQuery access data query
     * 
     * @return sql query string
     */
     getOccurrenceTraitQuerySql: async(addedConditionString = '', addedColumnString = '', addedDistinctString = '', distinctColumn = '', ownedConditionString = '', isBasic = false, accessDataQuery = '') => {

        let occurrenceQuery = null
        if(isBasic){
            occurrenceQuery = `
                SELECT
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_name AS "occurrenceName",
                    occurrence.occurrence_design_type AS "occurrenceDesignType",
                    location.id AS "locationDbId",
                    location.location_name AS "location",
                    location.location_code AS "locationCode",
                    program.id AS "programDbId",
                    site.id AS "siteDbId",
                    site.geospatial_object_code AS "siteCode",
                    field.id AS "fieldDbId",
                    field.geospatial_object_code AS "fieldCode",
                    field.geospatial_object_name AS "field",
                    occurrence.experiment_id AS "experimentDbId",
                    experiment.experiment_code AS "experimentCode",
                    experiment.experiment_name AS "experiment",
                    experiment.experiment_type AS "experimentType",
                    experiment.experiment_status AS "experimentStatus",
                    stage.id AS "experimentStageDbId",
                    stage.stage_code AS "experimentStageCode",
                    experiment.experiment_year AS "experimentYear",
                    season.id AS "experimentSeasonDbId",
                    season.season_code AS "experimentSeasonCode",
                    experiment.experiment_design_type AS "experimentDesignType",
                    occurrence.entry_count AS "entryCount",
                    occurrence.plot_count AS "plotCount",
                    occurrence.occurrence_status AS "occurrenceStatus"
                        ` + addedColumnString + `
                FROM
                    experiment.occurrence occurrence
                LEFT JOIN 
                    tenant.person creator ON creator.id = occurrence.creator_id
                LEFT JOIN
                    experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                LEFT JOIN
                    experiment.location location ON location.id = log.location_id
                LEFT JOIN
                    place.geospatial_object site ON site.id = occurrence.site_id
                LEFT JOIN
                    place.geospatial_object field ON field.id = occurrence.field_id,
                experiment.experiment experiment
                LEFT JOIN
                    tenant.stage stage on stage.id = experiment.stage_id
                LEFT JOIN
                    tenant.season season on season.id = experiment.season_id
                LEFT JOIN 
                    tenant.program program ON program.id = experiment.program_id
                LEFT JOIN 
                    tenant.person steward ON steward.id = experiment.steward_id
                WHERE
                    occurrence.experiment_id = experiment.id
                    AND occurrence.is_void = FALSE
                    ${accessDataQuery}
                    ${ownedConditionString}
                    ` + addedConditionString + `
                ORDER BY
                    occurrence.id
                `
        } else {

            occurrenceQuery = `
                SELECT
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_code AS "occurrenceCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    occurrence.occurrence_number AS "occurrenceNumber",
                    occurrence.occurrence_design_type AS "occurrenceDesignType",
                    location.id AS "locationDbId",
                    location.location_name AS "location",
                    location.location_code AS "locationCode",
                    location_steward.person_name AS "locationSteward",
                    program.id AS "programDbId",
                    program.program_code AS "programCode",
                    program.program_name AS "program",
                    site.id AS "siteDbId",
                    site.geospatial_object_code AS "siteCode",
                    site.geospatial_object_name AS "site",
                    field.id AS "fieldDbId",
                    field.geospatial_object_code AS "fieldCode",
                    field.geospatial_object_name AS "field",
                    occurrence.experiment_id AS "experimentDbId",
                    experiment.experiment_code AS "experimentCode",
                    experiment.experiment_name AS "experiment",
                    experiment.experiment_type AS "experimentType",
                    experiment.experiment_status AS "experimentStatus",
                    stage.id AS "experimentStageDbId",
                    stage.stage_code AS "experimentStageCode",
                    stage.stage_name AS "experimentStage",
                    experiment.experiment_year AS "experimentYear",
                    season.id AS "experimentSeasonDbId",
                    season.season_code AS "experimentSeasonCode",
                    season.season_name AS "experimentSeason",
                    experiment.experiment_design_type AS "experimentDesignType",
                    experiment.steward_id AS "experimentStewardDbId",
                    steward.person_name AS "experimentSteward",
                    occurrence.entry_count AS "entryCount",
                    occurrence.plot_count AS "plotCount",
                    occurrence.occurrence_status AS "occurrenceStatus",
                    t.collected_traits AS "collectedTraits",
                    EXISTS(
                        SELECT
                            id
                        FROM
                            experiment.plot
                        WHERE
                            plot.is_void = FALSE
                            AND plot.pa_x IS NOT NULL
                            AND plot.pa_y IS NOT NULL
                            AND plot.occurrence_id = occurrence.id
                        LIMIT
                            1
                      ) AS "hasPaCoordinates",
                    occurrence.description,
                    occurrence.remarks,
                    occurrence.creation_timestamp AS "creationTimestamp",
                    creator.id AS "creatorDbId",
                    creator.person_name AS "creator",
                    occurrence.modification_timestamp AS "modificationTimestamp",
                    modifier.id AS "modifierDbId",
                    modifier.person_name AS "modifier"
                    ` + addedColumnString + `
                FROM
                    experiment.occurrence occurrence
                LEFT JOIN
                    experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
                LEFT JOIN
                    experiment.location location ON location.id = log.location_id
                LEFT JOIN 
                    tenant.person location_steward ON location_steward.id = location.steward_id
                JOIN
                    place.geospatial_object site ON site.id = occurrence.site_id
                LEFT JOIN
                    place.geospatial_object field ON field.id = occurrence.field_id
                JOIN 
                    tenant.person creator ON creator.id = occurrence.creator_id
                LEFT JOIN
                    tenant.person modifier ON modifier.id = occurrence.modifier_id
                JOIN
                    experiment.experiment experiment ON occurrence.experiment_id = experiment.id
                JOIN
                    tenant.stage stage on stage.id = experiment.stage_id
                JOIN
                    tenant.season season on season.id = experiment.season_id
                JOIN 
                    tenant.program program ON program.id = experiment.program_id
                LEFT JOIN 
                    tenant.person steward ON steward.id = experiment.steward_id
                LEFT JOIN LATERAL (
                        SELECT
                            array_agg(
                                json_build_object(
                                    'variableDbId',var.id,
                                    'variableAbbrev',var.abbrev,
                                    'variableDataType',var.data_type,
                                    'isQualityChecked', ('G' = ANY(t.distinct_data_qc_codes) AND NOT 'Q' = ANY(t.distinct_data_qc_codes)) -- at least one good data (G) and no questionable data (Q)
                                )
                            )
                        FROM (
                                SELECT
                                    pd.variable_id,
                                    ARRAY_AGG(DISTINCT pd.data_qc_code) AS distinct_data_qc_codes
                                FROM
                                    experiment.plot AS p
                                    INNER JOIN experiment.plot_data pd
                                        ON pd.plot_id = p.id
                                WHERE
                                    p.occurrence_id = occurrence.id
                                    AND p.is_void = FALSE
                                    AND pd.is_void = FALSE
                                GROUP BY
                                    pd.variable_id
                            ) AS t (
                                variable_id,
                                distinct_data_qc_codes
                            )
                            INNER JOIN master.variable AS var
                                ON var.id = t.variable_id
                    ) AS t (
                        collected_traits
                    ) ON TRUE
                WHERE
                    occurrence.experiment_id = experiment.id
                    AND occurrence.is_void = FALSE
                    ${accessDataQuery}
                    ${ownedConditionString}
                    ` + addedConditionString
        }

        return occurrenceQuery
    },

    /**
     * Retrieve collected traits of plots 
     * belonging to the specified occurrence
     * @param {Integer} occurrenceDbId occurrence identifier
     */
    getCollectedTraits: async(occurrenceDbId) => {

        let collectedTraitsQuery = `
            SELECT
                DISTINCT variable.id AS "variableDbId",
                variable.abbrev AS "variableAbbrev",
                variable.data_type AS "variableDataType"
            FROM
                experiment.occurrence
            LEFT JOIN
                experiment.plot ON plot.occurrence_id = occurrence.id AND plot.is_void = FALSE
            LEFT JOIN
                experiment.plot_data ON plot_data.plot_id = plot.id AND plot_data.is_void = FALSE
            LEFT JOIN
                master.variable ON variable.id = plot_data.variable_id AND variable.is_void = FALSE
            WHERE
                occurrence.id = ${occurrenceDbId}
                AND occurrence.is_void = FALSE
                AND plot_data.id IS NOT NULL

        `

        // Retrieve plot from the database
        let collectedTraits = await sequelize.query(collectedTraitsQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return collectedTraits

    },

    /**
     * Generate occurrence code
     * @param String pattern pattern for occurrence code
     * @param Integer experimentDbId experiment identifier
     * @param Integer startingCounter starting number
     */
    generateCode: async (pattern, experimentDbId, startingCounter) => {

        let code = []

        for (i = 0; i < pattern.length; i++) {
            let value = ''
            if (pattern[i]["is_variable"]) {
                
                if (pattern[i]["value"] == "EXPERIMENT_NAME") {
                    value = await patternHelper.getEntityValue('experiment', 'experiment', 'experiment_name', experimentDbId)
                    code.push(value['experiment_name'])
                }

            } else {
                if (pattern[i]["value"] == "-") {
                    code.push('-')
                }
                if (pattern[i]["value"] == "COUNTER") {
                    
                    let columns = pattern[i]["max_value"].split('|');
                    let condition = []
                    let selectColumns = []
                    for (column of columns) {
                        if (column == "EXPERIMENT") {
                            condition.push('experiment_id =' + experimentDbId)
                            selectColumns.push('experiment_id')
                        }
                    }
                    selectColumns = selectColumns.join(", ")
                    let conditionStr = condition.join(" AND ");
                    counter = await patternHelper.getCounter('experiment', 'occurrence', conditionStr + ' AND is_void=FALSE', selectColumns, startingCounter)
                    counter = await patternHelper.getPaddedNumber(counter, pattern[i]["zero_padding"], parseInt(pattern[i]["digits"]))
                    code.push(counter)
                }
            }
        }

        return code.join("")
    },

    /**
     * Update occurrence status when at least one PLOT is committed
     * @param {String} plotIdParameter
     */
    updateOccurrenceStatus: async (plotIdParameter) => {

        let updateQuery = 
            `UPDATE experiment.occurrence
                SET occurrence_status =  occurrence_status || ';trait data collected'
                WHERE 
                    (occurrence_status like 'planted%' OR occurrence_status like 'mapped%')
                    AND occurrence_status not like '%trait data collected%'
                    AND id IN (SELECT DISTINCT occurrence_id FROM experiment.plot where id IN (${plotIdParameter})) 
                    AND is_void = FALSE`

        await sequelize.query(updateQuery, {
            type: sequelize.QueryTypes.UPDATE
        }).catch(async err => {
            logger.logMessage(__filename, err, 'error')
        })
    },

    /**
     * 
     * @param {String} baseData base data source name
     * @param {String} baseDataAlias base data source alias
     * @param {Integer} creatorDbId record creator identifier
     * @param {Array} variableIdArr observation data variable identifier
     * @param {String} conditionStr query condition string
     * @returns sqlQuery harvest and terminal data query
     */
    getHarvestAndTerminalDataQuery: async (baseData,baseDataAlias,creatorDbId,variableIdArr,conditionStr,parameters) => {
        let selectStr = `
            "harvestDate".data_value AS "harvestDate",
            "harvestMethod".data_value AS "harvestMethod",
            "crossingDate".data_value AS "crossingDate",
            "noOfSeed".data_value AS "noOfSeed",
            "noOfBag".data_value AS "noOfBag",
            "noOfPlant".data_value AS "noOfPlant",
            "specificPlantNo".data_value AS "specificPlantNo",
            "noOfPanicle".data_value AS "noOfPanicle",
            "noOfEar".data_value AS "noOfEar",
            "grainColor".data_value AS "grainColor",
            "terminalHarvestDate".value AS "terminalHarvestDate",
            "terminalHarvestMethod".value AS "terminalHarvestMethod",
            "terminalCrossingDate".value AS "terminalCrossingDate",
            "terminalNoOfSeed".value AS "terminalNoOfSeed",
            "terminalNoOfBag".value AS "terminalNoOfBag",
            "terminalNoOfPlant".value AS "terminalNoOfPlant",
            "terminalSpecificPlantNo".value AS "terminalSpecificPlantNo",
            "terminalNoOfPanicle".value AS "terminalNoOfPanicle",
            "terminalNoOfEar".value AS "terminalNoOfEar",
            "terminalGrainColor".value AS "terminalGrainColor"
        `

        let sqlQuery = `
                SELECT
                ${baseDataAlias != '' ? (baseDataAlias + '.*,') : baseDataAlias}
                ${selectStr}
            FROM
                ${baseData}
                LEFT JOIN germplasm.cross_data "harvestDate" 
                    ON  "harvestDate".variable_id = ${variableIdArr['harvestDateVarId']}
                    AND "harvestDate".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "harvestDate".is_void = FALSE
                LEFT JOIN germplasm.cross_data "harvestMethod" 
                    ON "harvestMethod".variable_id  = ${variableIdArr['harvestMethodVarId']} 
                    AND "harvestMethod".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "harvestMethod".is_void = FALSE
                LEFT JOIN germplasm.cross_data "crossingDate" 
                    ON "crossingDate".variable_id = ${variableIdArr['crossingDateVarId']}
                    AND "crossingDate".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "crossingDate".is_void = FALSE
                LEFT JOIN germplasm.cross_data "noOfSeed" 
                    ON "noOfSeed".variable_id  = ${variableIdArr['noOfSeedVarId']} 
                    AND "noOfSeed".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "noOfSeed".is_void = FALSE
                LEFT JOIN germplasm.cross_data "noOfBag" 
                    ON "noOfBag".variable_id  = ${variableIdArr['noOfBagVarId']} 
                    AND "noOfBag".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "noOfBag".is_void = FALSE
                LEFT JOIN germplasm.cross_data "noOfPlant" 
                    ON "noOfPlant".variable_id  = ${variableIdArr['noOfPlantVarId']} 
                    AND "noOfPlant".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "noOfPlant".is_void = FALSE
                LEFT JOIN germplasm.cross_data "specificPlantNo" 
                    ON "specificPlantNo".variable_id  = ${variableIdArr['specificPlantNoVarId']} 
                    AND "specificPlantNo".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "specificPlantNo".is_void = FALSE
                LEFT JOIN germplasm.cross_data "noOfPanicle" 
                    ON "noOfPanicle".variable_id  = ${variableIdArr['noOfPanicleVarId']}
                    AND "noOfPanicle".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "noOfPanicle".is_void = FALSE
                LEFT JOIN germplasm.cross_data "noOfEar" 
                    ON "noOfEar".variable_id  = ${variableIdArr['noOfEarVarId']}
                    AND "noOfEar".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "noOfEar".is_void = FALSE
                LEFT JOIN germplasm.cross_data "grainColor" 
                    ON "grainColor".variable_id  = ${variableIdArr['grainColorVarId']} 
                    AND "grainColor".cross_id = ${baseDataAlias}."crossDbId" 
                    AND "grainColor".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalHarvestDate"
                    ON "terminalHarvestDate".variable_id = ${variableIdArr['harvestDateVarId']}
                    AND "terminalHarvestDate".entity_id = ${baseDataAlias}."crossDbId" 
                    AND "terminalHarvestDate".creator_id = ${creatorDbId} 
                    AND "terminalHarvestDate".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalHarvestMethod"
                    ON "terminalHarvestMethod".variable_id = ${variableIdArr['harvestMethodVarId']} 
                    AND "terminalHarvestMethod".entity_id = ${baseDataAlias}."crossDbId" 
                    AND "terminalHarvestMethod".creator_id = ${creatorDbId} 
                    AND "terminalHarvestMethod".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalCrossingDate"
                    ON  "terminalCrossingDate".variable_id = ${variableIdArr['crossingDateVarId']}
                    AND "terminalCrossingDate".entity_id = ${baseDataAlias}."crossDbId"
                    AND "terminalCrossingDate".creator_id = ${creatorDbId}
                    AND "terminalCrossingDate".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalNoOfSeed"
                    ON "terminalNoOfSeed".variable_id = ${variableIdArr['noOfSeedVarId']} 
                    AND "terminalNoOfSeed".entity_id = ${baseDataAlias}."crossDbId"
                    AND "terminalNoOfSeed".creator_id = ${creatorDbId}
                    AND "terminalNoOfSeed".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalNoOfBag"
                    ON "terminalNoOfBag".variable_id = ${variableIdArr['noOfBagVarId']}
                    AND "terminalNoOfBag".entity_id = ${baseDataAlias}."crossDbId"
                    AND "terminalNoOfBag".creator_id = ${creatorDbId}
                    AND "terminalNoOfBag".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalNoOfPlant"
                    ON "terminalNoOfPlant".variable_id = ${variableIdArr['noOfPlantVarId']}
                    AND "terminalNoOfPlant".entity_id = ${baseDataAlias}."crossDbId"
                    AND "terminalNoOfPlant".creator_id = ${creatorDbId}
                    AND "terminalNoOfPlant".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalSpecificPlantNo"
                    ON "terminalSpecificPlantNo".variable_id = ${variableIdArr['specificPlantNoVarId']}
                    AND "terminalSpecificPlantNo".entity_id = ${baseDataAlias}."crossDbId"
                    AND "terminalSpecificPlantNo".creator_id = ${creatorDbId}
                    AND "terminalSpecificPlantNo".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalNoOfPanicle"
                    ON "terminalNoOfPanicle".variable_id = ${variableIdArr['noOfPanicleVarId']}
                    AND "terminalNoOfPanicle".entity_id = ${baseDataAlias}."crossDbId"
                    AND "terminalNoOfPanicle".creator_id = ${creatorDbId}
                    AND "terminalNoOfPanicle".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalNoOfEar"
                    ON  "terminalNoOfEar".variable_id = ${variableIdArr['noOfEarVarId']}
                    AND "terminalNoOfEar".entity_id = ${baseDataAlias}."crossDbId"
                    AND "terminalNoOfEar".creator_id = ${creatorDbId}
                    AND "terminalNoOfEar".is_void = FALSE
                LEFT JOIN data_terminal.transaction_dataset "terminalGrainColor"
                    ON "terminalGrainColor".variable_id = ${variableIdArr['grainColorVarId']}
                    AND "terminalGrainColor".entity_id = ${baseDataAlias}."crossDbId" 
                    AND "terminalGrainColor".creator_id = ${creatorDbId}
                    AND "terminalGrainColor".is_void = FALSE
            ${conditionStr}
        `

        return {
            'sqlQuery': sqlQuery,
            'selectStr': selectStr
        };
    },

    /**
     * 
     * @param {String} baseData base data source name
     * @param {String} baseDataAlias base data source alias
     * @param {String} crossDbIdAlias cross identifier alias
     * @param {String} crossParentType cross parent type (male/female)
     * @param {Array} parentRoleArr parent role array
     * @param {Object} parameters search parameters
     * @returns parentInfoQuery cross parent data query
     */
    getCrossParentInfoQuery: async (baseData,baseDataAlias,crossDbIdAlias,crossParentType,parentRoleArr,parameters) => {
        let aliasKey = crossParentType.toLowerCase();
        let selectStr = `
            "cross${crossParentType}ParentGermplasm".designation AS "cross${crossParentType}Parent",
            "cross${crossParentType}ParentGermplasm".id AS "${aliasKey}GermplasmDbId",
            "cross${crossParentType}ParentGermplasm".parentage AS "${aliasKey}Parentage",
            "cross${crossParentType}ParentGermplasm".germplasm_state AS "${aliasKey}GermplasmState",
            seed.seed_code AS "${aliasKey}ParentSeedSource",
            seed.seed_name AS "${aliasKey}SourceSeedName",
            "parentEntryTbl".id AS "${aliasKey}SourceSeedEntryDbId",
            "parentEntryTbl".entry_code AS "${aliasKey}SourceSeedEntry",
            "cross${crossParentType}ParentPI".entry_id AS "${aliasKey}EntryDbId",
            "cross${crossParentType}ParentPI".entry_code AS "${aliasKey}EntryCode",
            "cross${crossParentType}ParentPI".entry_number AS "${aliasKey}EntryNumber",
            "parentPlotTbl".id AS "${aliasKey}SourcePlotDbId",
            "parentPlotTbl".plot_code AS "${aliasKey}SourcePlot"
        `

        let parentInfoQuery = `
            SELECT
                DISTINCT ON ("cross${crossParentType}Parent".id)
                ${baseDataAlias != '' ? (baseDataAlias + '.*,') : baseDataAlias}
                ${selectStr}
            FROM
                ${baseData}
                LEFT JOIN germplasm.cross_parent "cross${crossParentType}Parent"
                    ON "cross${crossParentType}Parent".cross_id = ${baseDataAlias}."${crossDbIdAlias}" 
                    AND "cross${crossParentType}Parent".parent_role IN ('${parentRoleArr.join("','")}')
                    AND "cross${crossParentType}Parent".is_void = FALSE
                LEFT JOIN germplasm.seed seed 
                    ON "cross${crossParentType}Parent".seed_id = seed.id AND seed.is_void = FALSE
                LEFT JOIN experiment.entry "parentEntryTbl"
                    ON "parentEntryTbl".id = seed.source_entry_id 
                    AND "parentEntryTbl".is_void = FALSE
                LEFT JOIN experiment.plot "parentPlotTbl" 
                    ON "parentPlotTbl".id = seed.source_plot_id 
                    AND "parentPlotTbl".is_void = FALSE
                LEFT JOIN experiment.planting_instruction "cross${crossParentType}ParentPI"
                    ON "cross${crossParentType}ParentPI".entry_id = "cross${crossParentType}Parent".entry_id
                LEFT JOIN experiment.plot "cross${crossParentType}ParentPlot"
                    ON "cross${crossParentType}ParentPlot".id = "cross${crossParentType}ParentPI".plot_id
                LEFT JOIN germplasm.germplasm "cross${crossParentType}ParentGermplasm" 
                    ON "cross${crossParentType}ParentPI".germplasm_id = "cross${crossParentType}ParentGermplasm".id 
                    AND "cross${crossParentType}ParentGermplasm".is_void = FALSE
                WHERE
                    "cross${crossParentType}ParentPlot".occurrence_id = "cross${crossParentType}Parent".occurrence_id
        `

        return {
            'sqlQuery': parentInfoQuery,
            'selectStr': selectStr
        };
    },

    /**
     * 
     * @param {String} value inputted distinct column value
     * @param {Array} referenceTable column alias reference array
     * @returns distinctStr distinct string statement
     */
    getDistictParameter: async (value,referenceTable) => {
        return await getDistictParameter(value,referenceTable)
    },

    /**
     * 
     * @param {Array} parameters search parameters
     * @param {Array} referenceTable column alias reference array
     * @param {String} sourceTable source table name; default empty
     * @returns newSelectStr column table alias statement
     */
    getColumnsParameter: async (parameters, referenceTable,sourceTable = '') => {
        return getColumnsParameter(parameters,referenceTable,sourceTable)
    },

    /**
     * Retrieves supported HM variables
     * and builds field names for retrieval
     * @param {String} dataLevel data level (eg. plot, cross)
     * @returns {Object} containing variable ids and field names
     */
    getHMSupportedVariables: async (dataLevel = "plot") => {
        // Declare field names for known supported variables
        let fieldNames = {
            "'HVDATE_CONT'": '"harvestDate"',
            "'HV_METH_DISC'": '"harvestMethod"',
            "'NO_OF_PLANTS'": '"noOfPlant"',
            "'PANNO_SEL'": '"noOfPanicle"',
            "'SPECIFIC_PLANT'": '"specificPlantNo"',
            "'NO_OF_EARS'": '"noOfEar"',
            "'GRAIN_COLOR'": '"grainColor"',
            "'DATE_CROSSED'": '"crossingDate"',
            "'NO_OF_SEED'": '"noOfSeed"',
            "'NO_OF_BAGS'": '"noOfBag"',
            "'HYB_MATERIAL_TYPE'": '"hybridMaterialType"'
        }
        // Declare supported abbrevs based on data level
        let abbrevs = []
        if (dataLevel === "plot") {
            abbrevs = [
                "'HVDATE_CONT'", "'HV_METH_DISC'", "'NO_OF_PLANTS'", "'PANNO_SEL'",
                "'SPECIFIC_PLANT'", "'NO_OF_EARS'", "'GRAIN_COLOR'", "'HYB_MATERIAL_TYPE'", "'NO_OF_BAGS'"
            ]
        }
        else if (dataLevel === "cross") {
            abbrevs = [
                "'HVDATE_CONT'", "'HV_METH_DISC'", "'NO_OF_PLANTS'", "'PANNO_SEL'",
                "'SPECIFIC_PLANT'", "'NO_OF_EARS'", "'GRAIN_COLOR'", "'DATE_CROSSED'",
                "'NO_OF_SEED'", "'NO_OF_BAGS'"
            ]
        }

        // Retrieve variables
        let variableQuery = `
            SELECT
                STRING_AGG( id::text, ',') as "variableId",
                STRING_AGG( abbrev::text, ',') as "variableAbbrev",
                STRING_AGG( ('''' || abbrev::text || ''''), ',') as "variableAbbrevQ"
            FROM(
                SELECT
                    variable.id,
                    variable.abbrev
                FROM
                    master.variable
                WHERE
                    variable.abbrev IN (${abbrevs.join(",")})
                    AND variable.is_void = FALSE
                ORDER BY
                    variable.id ASC
            ) tbl
        `

        let variables = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let variableIds
        let variableAbbrevs
        let variableAbbrevsQ
        let abbrevsOrdederd
        
        // Get variable ids and abbrevs
        if (variables[0] !== undefined) {
            variableIds = variables[0].variableId
            variableAbbrevs = variables[0].variableAbbrev
            variableAbbrevsQ = variables[0].variableAbbrevQ
            abbrevsOrdederd = variableAbbrevsQ.split(",")

            // Add missing variables (if any)
            // to prevent errors with the crosstab query.
            let abbrevsLen = abbrevs.length
            let placeholderId = 0
            for (let i = 0; i < abbrevsLen; i++) {
                let currentAbbrev = abbrevs[i].replaceAll("'","")
                // If the current abbrev is not part of the results,
                // append it to the abbrevs string,
                // then append placeholder id to the variable ids string.
                if (!variableAbbrevs.includes(currentAbbrev)) {
                    variableAbbrevs += "," + currentAbbrev
                    variableIds += "," + placeholderId
                    // Update placeholder id value
                    placeholderId = placeholderId - 1
                    
                }

                // If the current abbrev is not included in the ordered abbrevs array,
                // add it to the array.
                if (!abbrevsOrdederd.includes(abbrevs[i])) {
                    abbrevsOrdederd.push("'" + currentAbbrev + "'")
                }
            }
        }

        // Build query strings for committed data
        let committedData = abbrevsOrdederd.map((abb) => {
            return fieldNames[abb]
        })
        let committedDataAs = committedData.map((data) => {
            return data + " text"
        })
        let committedDataSelect = committedData.map((data) => {
            return "committedData." + data
        })
        // Build query strings for terminal data
        let terminalData = committedData.map((data) => {
            return '"terminal' + data.charAt(1).toUpperCase() + data.slice(2)
        })
        let terminalDataAs = terminalData.map((data) => {
            return data + " text"
        })
        let terminalDataSelect = terminalData.map((data) => {
            return "terminalData." + data
        })

        return {
            variableIds: variableIds,
            variableAbbrevs: variableAbbrevs,
            committedData: committedData,
            committedDataAs: committedDataAs,
            committedDataSelect: committedDataSelect,
            terminalData: terminalData,
            terminalDataAs: terminalDataAs,
            terminalDataSelect: terminalDataSelect,
        }
    },

    /**
     * Retrieve cross tab columns for an entity
     * 
     * @param {String} entity data entity
     * @param {String} conditionStr query condition
     * @param {String} variableCondition variable query condition
     * @param {Response} res response object
     * 
     * @returns mixed
     */
    getCrossTabColumns: async (entity, conditionStr, res, schema = 'experiment', ext = 'data') => {

        let baseQuery = `    
            SELECT
                STRING_AGG( tbl.variable_id::text, ',') AS "variableId",
                STRING_AGG( '"' || tbl.abbrev || '"', ',') AS "selectColumns",
                STRING_AGG( '"' || tbl.abbrev || '"' || ' varchar', ',') AS "crossTabColumns"
            FROM
                (
                    SELECT
                        DISTINCT ${entity}_data.variable_id,
                        variable.abbrev
                    FROM
                        ${schema}.${entity} ${entity}
                        JOIN ${schema}.${entity}_${ext} ${entity}_data 
                            ON ${entity}_data.${entity}_id = ${entity}.id 
                            AND ${entity}_data.is_void = FALSE
                        LEFT JOIN master.variable variable 
                            ON variable.id = ${entity}_data.variable_id 
                            AND variable.is_void = FALSE
                    ${conditionStr}
                ) tbl
            `

        // Retrieve data
        let results = await sequelize.query(baseQuery, {
            type: sequelize.QueryTypes.SELECT
        })
        .catch(async err => {
            let errMsg = 'Encountered error in retrieving data variables'
            res.send(new errors.BadRequestError(errMsg))
            return
        })

        if(results == undefined){
            return null
        }

        if(results[0].length < 1){
            res.send(200, {
                rows: [],
                count: 0
            })
            return
        }

        let selectColumns = results[0].selectColumns
        let crossTabColumns = results[0].crossTabColumns
        let variableIdArr = results[0].variableId

        return {
            selectColumns: selectColumns,
            columns: crossTabColumns,
            ids: variableIdArr
        }
    },

    /**
     * Retrieve experiment information query
     * 
     * @param {String} alias data source alias
     * @param {String} conditionStr query condition string
     * 
     * @returns 
     */
    getExperimentInfoQuery: async (alias,conditionStr) => {

        return `
            SELECT
                experiment.*,
                season.*,
                stage.*,
                project.*,    
                pipeline.*,
                steward.*
            FROM
                ${alias}
                JOIN LATERAL (
                    SELECT 
                        experiment.id AS "experimentDbId",
                        experiment.experiment_code as "experimentCode",
                        experiment.experiment_name as "experimentName",
                        experiment.experiment_year as "experimentYear",
                        experiment.experiment_type as "experimentType",
                        experiment.experiment_sub_type as "experimentSubtype",
                        experiment.experiment_design_type as "experimentDesignType",
                        experiment.experiment_status as "experimentStatus",
                        experiment.description AS "experimentDescription",
                        experiment.remarks as "experimentRemarks",
                        experiment.season_id as "experimentSeasonDbId",
                        experiment.stage_id as "experimentStageDbId",
                        experiment.project_id as "experimentProjectDbId",
                        experiment.planting_season AS "plantingSeason",
                        experiment.experiment_objective AS "experimentObjective",
                        experiment.pipeline_id as "experimentPipelineDbId",
                        experiment.steward_id as "experimentStewardDbId"
                    FROM
                        experiment.experiment experiment
                    WHERE
                        experiment.is_void = FALSE
                        ${conditionStr}
                    LIMIT 1
                ) experiment on true
                LEFT JOIN LATERAL (
                    SELECT
                        season.season_code as "experimentSeason"
                    FROM
                        tenant.season season
                    WHERE
                        season.is_void = FALSE
                        AND season.id = experiment."experimentSeasonDbId"
                    LIMIT 1
                ) season ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        stage.stage_code as "experimentStageCode"
                    FROM
                        tenant.stage stage
                    WHERE
                        stage.is_void = FALSE
                        AND stage.id = experiment."experimentStageDbId"
                    LIMIT 1
                ) stage ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        project.project_code as "experimentProject"
                    FROM
                        tenant.project project
                    WHERE
                        project.is_void = FALSE
                        AND project.id = experiment."experimentProjectDbId"
                    LIMIT 1
                ) project ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        pipeline.pipeline_code as "experimentPipeline"
                    FROM
                        tenant.pipeline pipeline
                    WHERE
                        pipeline.is_void = FALSE
                        AND pipeline.id = experiment."experimentPipelineDbId"
                    LIMIT 1
                ) pipeline ON TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        steward.person_name as "experimentSteward"
                    FROM
                        tenant.person steward
                    WHERE
                        steward.is_void = FALSE
                        AND steward.id = experiment."experimentStewardDbId"
                    LIMIT 1
                ) steward ON TRUE
        `
    },

    /**
     * Retrieve entry information query
     * 
     * @param {String} alias data source alias
     * @param {String} occurrenceDbId occurrence ID
     * 
     * @returns 
     */
    getEntryInfoQuery: async (alias, occurrenceDbId) => {

        return `
            SELECT
                entry.*,
                ${alias}."occurrenceDbId",
                ${alias}."locationDbId"
            FROM
                ${alias}
                JOIN LATERAL (
                    SELECT DISTINCT ON (pi.entry_id)
                        pi.entry_id AS "entryDbId",
                        pi.germplasm_id AS "germplasmDbId",
                        pi.entry_number as "entryNumber",
                        pi.entry_code as "entryCode",
                        pi.entry_type as "entryType",
                        pi.entry_class as "entryClass",
                        pi.entry_role as "entryRole"
                    FROM
                        experiment.plot plot
                    LEFT JOIN
                        experiment.occurrence occurrence ON occurrence.id = plot.occurrence_id
                    JOIN
                        experiment.planting_instruction pi ON pi.plot_id = plot.id
                    JOIN
                        experiment.entry e ON pi.entry_id = e.id
                    JOIN
                        germplasm.germplasm g ON g.id = pi.germplasm_id AND g.id = pi.germplasm_id
                    WHERE
                        plot.occurrence_id = ${occurrenceDbId}
                        AND plot.is_void = false
                        AND e.is_void = false
                        AND pi.is_void = false
                        AND occurrence.is_void = false
                        AND plot.occurrence_id = occurrence.id
                ) entry ON TRUE
        `
    },

    /**
     * Retrieve plot information query
     * 
     * @param {String} alias data source alias
     * @param {String} conditionStr query condition string
     * 
     * @returns 
     */
    getPlotInfoQuery: async (alias,conditionStr) => {
        
        return `
            SELECT
                DISTINCT ON ("plotDbId")
                plot.id AS "plotDbId",
                plot.occurrence_id AS "occurrenceDbId",
                plot.entry_id AS "entryDbId",
                plot.plot_number AS "plotNumber",
                plot.plot_code AS "plotCode",
                plot.plot_type AS "plotType",
                plot.rep AS "rep",
                plot.pa_x AS "paX",
                plot.pa_y AS "paY",
                plot.block_number AS "blockNumber"
            FROM
                entry
                JOIN experiment.plot plot 
                    ON plot.is_void = FALSE
                    ${conditionStr}
        `
    },

    /**
     * Retrieve germplasm information query
     * 
     * @param {String} alias data source alias
     * 
     * @returns 
     */
    getGermplasmInfoQuery: async (alias) => {

        return `
            SELECT
                DISTINCT ON ("germplasmDbId")
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_code AS "germplasmCode",
                germplasm.designation AS "germplasmName",
                germplasm.germplasm_state AS "germplasmState",
                germplasm.taxonomy_id AS "taxomonyDbId",
                germplasm.parentage,
                germplasm.germplasm_type AS "germplasmType",
                germplasm.germplasm_name_type AS "germplasmNameType",
                femParent.parent_germplasm_id AS "femParentDbId",
                (SELECT germplasm_code FROM germplasm.germplasm WHERE id = femParent.parent_germplasm_id) AS "femaleParentGermplasmCode",
                maleParent.parent_germplasm_id AS "maleParentDbId",
                (SELECT germplasm_code FROM germplasm.germplasm WHERE id = maleParent.parent_germplasm_id) AS "maleParentGermplasmCode"
            FROM
                ${alias}
                JOIN germplasm.germplasm germplasm 
                    ON germplasm.id = ${alias}."germplasmDbId" 
                    AND germplasm.is_void= FALSE
                LEFT JOIN LATERAL (
                    SELECT
                        *
                    FROM
                        germplasm.germplasm_relation femParent 
                    WHERE
                        femParent.child_germplasm_id = germplasm.id
                        AND femParent.order_number = 1 
                        AND femParent.is_void = FALSE
                    LIMIT 1
                ) femParent on TRUE
                LEFT JOIN LATERAL (
                    SELECT
                        *
                    FROM
                        germplasm.germplasm_relation maleParent 
                    WHERE
                        femParent.child_germplasm_id = germplasm.id
                        AND femParent.order_number = 2 
                        AND femParent.is_void = FALSE
                    LIMIT 1
                ) maleParent ON TRUE
            LEFT JOIN LATERAL (
                SELECT
                    taxonomy.taxonomy_name AS "taxonomyName",
                    taxonomy.taxon_id AS "taxonId" 
                FROM
                    germplasm.taxonomy taxonomy
                WHERE
                    taxonomy.is_void = FALSE
                    AND taxonomy.id = germplasm.taxonomy_id
                LIMIT 1
            ) taxonomy ON TRUE
        `;
    },

    /**
     * Retrieve count CTE
     * 
     * @param {String} alias data source query
     * 
     * @returns mixed
     */
    getResultTotalCount: async (alias) => {
        return `
            ,total_count AS (
                SELECT COUNT(*) AS "totalCount" FROM ${alias}
            )
        `
    },

    /**
     * Retrieve parameters for adding DATA_QC_CODE values for plot trait data
     * 
     * @param {String} variableDbIdStr variable ids
     * @param {String} variableAbbrevStr variable abbrevs
     * 
     * @returns mixed
     */
    getVarQcCodeColumns: async (variableDbIdStr, variableAbbrevStr, sourceTable = 'plot_data') => {
        let ids = variableDbIdStr ? variableDbIdStr.split(',') : []
        let abbrevs = variableAbbrevStr ? variableAbbrevStr.split(',') : []

        if(ids.length < 1){
            return ''
        }

        let columnStr = ``
        let selectStr = ``
        let condStr = ``

        let index
        let abbrev
        for(var id of ids){
            index = ids.indexOf(id)
            abbrev = abbrevs[index].split('"')

            columnStr += `
                (
                    CASE WHEN variable_id = ${id} THEN ${sourceTable}.data_qc_code END
                ) "${abbrev[1]}_QC"
            `

            columnStr += ','

            selectStr += `"${abbrev[1]}_QC"`
            condStr += `"${abbrev[1]}_QC" varchar`

            if(index < ids.length-1 ){
                selectStr += ','
                condStr += ','
            }
        }

        return {
            query: columnStr,
            select: selectStr,
            condition: condStr
        }
    },

    /**
     * Generate germplasm data query
     * 
     * @param {String} qcCodeSelectColumns 
     * @param {String} selectColumns 
     * @param {String} varDbIds 
     * @param {String} varCrossColumns 
     * @param {Integer} experimentDbId 
     * 
     * @returns 
     */
    getGermplasmDataInfo: async (qcCodeSelectColumns,selectColumns,varDbIds,varCrossColumns,dataQcSelect,experimentDbId) => {
        let query = "";
        
        if(varCrossColumns != undefined && varCrossColumns != null && varCrossColumns != ""){
            varCrossColumns = `, ${varCrossColumns}`;
        }

        if(selectColumns != undefined && selectColumns != null && selectColumns != ""){
            selectColumns = `, ${selectColumns}`;
        }
        
        if(experimentDbId != undefined && experimentDbId != null && experimentDbId != ""){
            query = `
                , germplasm_trait_data_values AS (
                    SELECT
                        DISTINCT ON ("germplasmDbId")
                        *
                    FROM
                        experiment.entry_list entry_list
                        JOIN experiment.entry entry ON entry.is_void = FALSE AND entry.entry_list_id = entry_list.id
                        LEFT JOIN (
                            SELECT 
                                *
                            FROM
                                CROSSTAB(
                                'SELECT 
                                    germplasm.id AS "germplasmDbId",
                                    germplasm_data.variable_id,
                                    germplasm_data.data_value
                                FROM
                                    experiment.entry_list entry_list
                                    JOIN experiment.entry entry 
                                        ON entry.entry_list_id = entry_list.id 
                                        AND entry.is_void = FALSE
                                    JOIN germplasm.germplasm 
                                        ON germplasm.id = entry.germplasm_id 
                                        AND germplasm.is_void = FALSE
                                    LEFT JOIN germplasm.germplasm_attribute germplasm_data 
                                        ON germplasm.id = germplasm_data.germplasm_id 
                                        AND germplasm_data.is_void = FALSE
                                WHERE
                                    entry_list.experiment_id = ${experimentDbId} 
                                    AND entry_list.is_void = FALSE
                                ORDER BY
                                    germplasm.id
                                ',
                                ' SELECT UNNEST(ARRAY[${varDbIds}]) '
                                ) AS (
                                    "germplasmDbId" integer
                                    ${varCrossColumns}
                                )
                        ) crosstab ON crosstab."germplasmDbId" = entry.germplasm_id
                    WHERE
                        entry_list.experiment_id = ${experimentDbId}
                        AND entry_list.is_void = FALSE
                ),
                germplasm_trait_data_qc AS (
                    SELECT
                        DISTINCT ON ("germplasmDbId")
                        *
                    FROM
                        experiment.entry_list entry_list
                        JOIN experiment.entry entry ON entry.is_void = FALSE AND entry.entry_list_id = entry_list.id
                        LEFT JOIN (
                            SELECT 
                                *
                            FROM
                                CROSSTAB(
                                '
                                    SELECT 
                                        germplasm.id AS "germplasmDbId",
                                        germplasm_data.variable_id,
                                        germplasm_data.data_qc_code
                                    FROM
                                        experiment.entry_list entry_list
                                        JOIN experiment.entry entry 
                                            ON entry.entry_list_id = entry_list.id 
                                            AND entry.is_void = FALSE
                                        JOIN germplasm.germplasm 
                                            ON germplasm.id = entry.germplasm_id 
                                            AND germplasm.is_void = FALSE
                                        LEFT JOIN germplasm.germplasm_attribute germplasm_data 
                                            ON germplasm.id = germplasm_data.germplasm_id 
                                            AND germplasm_data.is_void = FALSE
                                    WHERE
                                        entry_list.experiment_id = ${experimentDbId} 
                                        AND entry_list.is_void = FALSE
                                    ORDER BY
                                        germplasm.id
                                ',
                                ' SELECT UNNEST(ARRAY[${varDbIds}]) '
                                ) AS (
                                    "germplasmDbId" integer, 
                                    ${dataQcSelect}
                                )
                        ) crosstab ON crosstab."germplasmDbId" = entry.germplasm_id
                    WHERE
                        entry_list.experiment_id = ${experimentDbId}
                        AND entry_list.is_void = FALSE
                ),
                germplasm_trait_data AS (
                    SELECT
                        gdv."germplasmDbId"
                        ${selectColumns}
                    FROM
                        germplasm_trait_data_values gdv
                        JOIN germplasm_trait_data_qc gdq ON gdq."germplasmDbId" = gdv."germplasmDbId"
                )
            `;
        }       

        return query;
    },

    getPlotTraitDataAndQc: async (occurrenceDbId,plotSelectColumns,ConditionsQc,variableDbIds,ConditionsData,traitDataValueQuery) => {
        let query = "";
        
        if(occurrenceDbId != "" && plotSelectColumns != "" && ConditionsData!= ""){
            query = `
                plot_trait_data_values AS (
                    SELECT 
                        *
                    FROM
                        CROSSTAB(
                        $$
                            SELECT 
                                plot.id AS "traitPlotDbId",
                                plot_data.variable_id ,
                                ${traitDataValueQuery}
                            FROM
                                experiment.plot plot
                                    LEFT JOIN experiment.plot_data
                                    ON plot.id = plot_data.plot_id
                            WHERE
                                plot.occurrence_id = ${occurrenceDbId}
                                AND plot_data.is_void = FALSE
                                AND plot.is_void = FALSE
                            ORDER BY
                                plot.id
                        $$,
                        'SELECT UNNEST(ARRAY[${variableDbIds}])'
                        ) AS (
                            "traitPlotDbId" integer,
                            ${ConditionsData}
                        )
                ),
                plot_trait_data_qc AS (
                    SELECT 
                        *
                    FROM
                        CROSSTAB(
                        $$
                            SELECT 
                                plot.id AS "traitPlotDbId",
                                plot_data.variable_id ,
                                plot_data.data_qc_code
                            FROM
                                experiment.plot plot
                                    LEFT JOIN experiment.plot_data
                                    ON plot.id = plot_data.plot_id
                            WHERE
                                plot.occurrence_id = ${occurrenceDbId}
                                AND plot_data.is_void = FALSE
                                AND plot.is_void = FALSE
                            ORDER BY
                                plot.id
                        $$,
                        'SELECT UNNEST(ARRAY[${variableDbIds}])'
                        ) AS (
                            "traitPlotDbId" integer,
                            ${ConditionsQc}
                        )
                )
                , plot_trait_data AS (
                    SELECT
                        pdv."traitPlotDbId",
                        ${plotSelectColumns}
                    FROM
                        plot_trait_data_values pdv
                        JOIN plot_trait_data_qc pdq ON pdq."traitPlotDbId" =  pdv."traitPlotDbId"
                )
            `;
        }
        
        return query;
    },

    getGermplasmDataAndQc: async () => {

    }
}