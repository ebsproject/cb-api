/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let seedlotHelper = require('./index.js')

module.exports = {

    create: async (cropName, plotDbId, harvestMethod, crossParents, plotRecord, userId) => {
        let experimentDbId = crossParents.experimentDbId
        let crossDbId = crossParents.crossDbId
        let crossMethod = crossParents.crossMethod
        let parent1Role = crossParents.parent1Role
        let parent1EntryDbId = crossParents.parent1EntryDbId
        let parent1GermplasmState = crossParents.parent1GermplasmState
        let parent1GermplasmDbId = crossParents.parent1GermplasmDbId
        let parent1Generation = crossParents.parent1Generation
        let parent1Designation = crossParents.parent1Designation
        let parent2GermplasmDbId = crossParents.parent2GermplasmDbId
        let parent2Role = crossParents.parent2Role
        let parent2EntryDbId = crossParents.parent2EntryDbId
        let parent2Designation = crossParents.parent2Designation
        let parent2GermplasmState = crossParents.parent2GermplasmState
        let parent2Generation = crossParents.parent2Generation

        if (harvestMethod == 'bulk' && cropName == 'maize') {
            /**
             * Condition 1 Cross A by B exists in the database with method = simple cross
               Condition 2 Cross B by A exists in the database with method = simple cross
               Condition 3 Cross A by B exists in the database with a different method
               Condition 4 Cross B by A exists in the database with a different method

                                                    Conditions
                parent role | germplasm state | 1  |  2  |  3  |  4 | create germplasm?
            1   Female      | fixed             N  |  N  |  N  |  N |  Y
                male        | fixed
                
            2   Female      | fixed             Y  |  N  |  N  |  N |  N
                male        | fixed

            3   Female      | fixed             N  |  Y  |  N  |  N |  N
                male        | fixed

            4   Female      | fixed             N  |  N  |  Y  |  Y |  Y
                male        | fixed

            5   Female      | not_fixed         N  |  N  |  N  |  N |  Y
                male        | fixed

            6   Female      | not_fixed         N  |  N  |  Y  |  N |  Y
                male        | fixed

            7   Female      | fixed             N  |  N  |  N  |  Y |  Y
                male        | unknown

            8   Female      | not_fixed         N  |  N  |  N  |  N |  Y
                male        | not_fixed
             */
            if (parent1Role == 'female') {
                femaleGermplasmState = parent1GermplasmState
                maleGermplasmState = parent2GermplasmState

                femaleGermplasmDbId = parent1GermplasmDbId
                maleGermplasmDbId = parent2GermplasmDbId

                femaleDesignation = parent1Designation
                maleDesignation = parent2Designation
            } else {
                femaleGermplasmState = parent2GermplasmState
                maleGermplasmState = parent1GermplasmState

                femaleGermplasmDbId = parent2GermplasmDbId
                maleGermplasmDbId = parent1GermplasmDbId

                femaleDesignation = parent2Designation
                maleDesignation = parent1Designation
            }

            let condition1 = `female = ${femaleGermplasmDbId} AND male = ${maleGermplasmDbId}  
                AND cross_method = '${crossMethod}'`

            let condition2 = `female = ${maleGermplasmDbId} AND male = ${femaleGermplasmDbId}  
                AND cross_method = '${crossMethod}'`

            let condition3 = `female = ${femaleGermplasmDbId} AND male = ${maleGermplasmDbId}  
                AND cross_method <> '${crossMethod}'`

            let condition4 = `female = ${maleGermplasmDbId} AND male = ${femaleGermplasmDbId}  
                AND cross_method <> '${crossMethod}'`

            let plotQuery = `
            WITH cross_parents AS (
                SELECT *
                FROM 
                    CROSSTAB(
                        '
                            SELECT
                                "cross".id cross_id,
                                "cross".cross_method,
                                "cross".germplasm_id cross_germplasm_id,
                                cross_parent.parent_role,
                                cross_parent.germplasm_id
                            FROM 
                                germplasm.cross_parent
                                    LEFT JOIN germplasm."cross" ON "cross".id = cross_parent.cross_id
                            WHERE 
                                cross_parent.entry_id IN (${parent1EntryDbId}, ${parent2EntryDbId})
                                AND "cross".id <> ${crossDbId}
                                AND cross_parent.is_void = FALSE
                                AND "cross".germplasm_id IS NOT NULL

                            ORDER BY "cross".id, parent_role
                        ',
                        ' SELECT UNNEST(ARRAY[''female'', ''male''])'
                    ) AS (cross_id integer, cross_method text,cross_germplasm_id integer,  female integer, male integer)
            ),
            rules AS(
                SELECT  
                    (CASE WHEN ${condition1} THEN TRUE ELSE FALSE END) condition1,
                    (CASE WHEN ${condition2} THEN TRUE ELSE FALSE END) condition2,
                    (CASE WHEN ${condition3} THEN TRUE ELSE FALSE END) condition3,
                    (CASE WHEN ${condition4} THEN TRUE ELSE FALSE END) condition4,
                    cross_germplasm_id
                FROM 
                    cross_parents
                WHERE 
                    female IS NOT NULL AND 
                    male IS NOT NULL
            )
            SELECT
                (CASE 
                    WHEN condition1 = FALSE AND condition2 = FALSE AND condition3 = FALSE
                        AND condition4 = FALSE AND 'fixed' = '${parent1GermplasmState}' AND 'fixed' = '${parent2GermplasmState}'
                            THEN 'new_germplasm'

                    WHEN condition1 = TRUE AND condition2 = FALSE AND condition3 = FALSE
                        AND condition4 = FALSE AND 'fixed' = '${parent1GermplasmState}' AND 'fixed' = '${parent2GermplasmState}'
                            THEN cross_germplasm_id::text

                    WHEN condition1 = FALSE AND condition2 = TRUE AND condition3 = FALSE
                        AND condition4 = FALSE AND 'fixed' = '${parent1GermplasmState}' AND 'fixed' = '${parent2GermplasmState}'
                            THEN cross_germplasm_id::text
                    
                    WHEN condition1 = FALSE AND condition2 = FALSE AND condition3 = TRUE
                        AND condition4 = TRUE AND 'fixed' = '${parent1GermplasmState}' AND 'fixed' = '${parent2GermplasmState}'
                            THEN 'new_germplasm'

                    WHEN condition1 = FALSE AND condition2 = TRUE AND condition3 = TRUE
                        AND condition4 = TRUE AND 'not_fixed' = '${parent1GermplasmState}' AND 'fixed' = '${parent2GermplasmState}'
                            THEN 'new_germplasm'
                    
                    WHEN condition1 = FALSE AND condition2 = FALSE AND condition3 = TRUE
                        AND condition4 = FALSE AND 'not_fixed' = '${parent1GermplasmState}' AND 'fixed' = '${parent2GermplasmState}'
                            THEN 'new_germplasm'
                    
                    WHEN condition1 = FALSE AND condition2 = FALSE AND condition3 = FALSE
                        AND condition4 = TRUE AND 'fixed' = '${parent1GermplasmState}' AND 'unknown' = '${parent2GermplasmState}'
                            THEN 'new_germplasm'

                    WHEN condition1 = FALSE AND condition2 = FALSE AND condition3 = FALSE
                        AND condition4 = TRUE AND 'not_fixed' = '${parent1GermplasmState}' AND 'not_fixed' = '${parent2GermplasmState}'
                            THEN 'new_germplasm'
                END) AS germplasm_status
            FROM 
                rules              
            `
            let status = await sequelize.query(plotQuery, {
                type: sequelize.QueryTypes.SELECT
            })
            let newGermplasmId
            let createGermplasmStatus = true
            if (status.length == 0) {
                createGermplasmStatus = true
            } else {

                for (let record of status) {
                    if (record.germplasm_status !== 'new_germplasm') {
                        createGermplasmStatus = false
                        newGermplasmDbId = record.germplasm_status
                        break
                    }
                }
            }

            let germplasmData
            if (createGermplasmStatus) {
                // set germplasmData
                germplasmData = {
                    designation: femaleDesignation + '/' + maleDesignation,
                    generation: 'F1',
                    parentage: femaleDesignation + '/' + maleDesignation,
                    germplasmState: 'not_fixed',
                    germplasmNameType: plotRecord.germplasmNameType,
                    germplasmNormalizedName: femaleDesignation + '/' + maleDesignation,
                    cropId: plotRecord.cropDbId,
                    taxonomyId: plotRecord.germplasmTaxonomyId,
                    germplasmDbId: null
                }
                newGermplasmId = await seedlotHelper.createGermplasm(germplasmData, userId)
                germplasmData.germplasmDbId = newGermplasmId[0]['id']
                newGermplasmDbId = newGermplasmId[0]['id']
                await seedlotHelper.createGermplasmName(germplasmData, plotRecord, userId)
            }

            let seedCounter = 1
            let harvestData = await seedlotHelper.getPlotHarvestData(plotDbId) // retrieve harvest data
            let seedId = await seedlotHelper.createSeed(newGermplasmDbId, plotRecord, harvestData, userId, seedCounter)
            let seedDbId = seedId[0]["id"]

            let seedRecord = await seedlotHelper.getSeedformation(seedDbId)

            await seedlotHelper.createPackage(seedRecord, plotRecord, userId)
            return true

        }
    }
}