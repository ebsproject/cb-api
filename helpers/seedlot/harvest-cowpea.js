/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


let { sequelize } = require('../../config/sequelize')

let seedlotHelper = require('./index.js')
let germplasmHelper = require('../../helpers/germplasm/index.js')

let format = require('pg-format')
let escapeRegExp = require('lodash/fp/escapeRegExp');

/**
 * Facilitate creation of harvest records for not_fixed germplasm, F2/F3 generation, w/ Single Plant Selection
 * 
 * @param {Array} plotRecord - plot record
 * @param {Array} harvestData - harvest information
 * @param {Integer} userId - unser identification
 * @param {Array} harvestSource - harvest source
 * @param {String} prefixes - prefix to be used
 * @param {Array} optional - optional parameters
 * @returns 
 */
async function harvestSinglePlantSelectionNotFixedCowPea(plotRecord, harvestData, userId, harvestSource, prefixes, optional){

    plotRecord = await seedlotHelper.populateCrossRecord(plotRecord,userId);

    // Check if designation needs to be in parentheses
    let startingDesignation = plotRecord.germplasmDesignation
    let germplasmParents = await seedlotHelper.getGermplasmParents(plotRecord.germplasmDbId)
    let germplasmParentCount = germplasmParents.length
    let isF1 = plotRecord.germplasmGeneration.match(/F1$/g)
    
    if (germplasmParentCount == 2 || isF1) {
        startingDesignation = `(${startingDesignation})`
        plotRecord.germplasmDesignation = startingDesignation
    }

    let recordsToCreate = parseInt(harvestData.no_of_plants)
    let entitiesArray = {
        record: plotRecord,
        plot: plotRecord,
        harvestMode: plotRecord.harvestMode,
        harvestData:harvestData
    }

    if (harvestSource == 'cross') entitiesArray[harvestSource] = plotRecord

    try {
        for (let i = 1; i <= recordsToCreate; i++) {
            // 1. Create germplasm record
            let newDesignation = await seedlotHelper.buildGermplasmDesignation(entitiesArray)

            let germplasmInfo = {}
            
            germplasmInfo["designation"] = newDesignation
            germplasmInfo["parentage"] = plotRecord.germplasmParentage
            germplasmInfo["germplasm_state"] = plotRecord.germplasmState
            germplasmInfo["crop_id"] = plotRecord.cropDbId
            germplasmInfo["taxonomy_id"] = plotRecord.germplasmTaxonomyId
            
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo["generation"] = await seedlotHelper.advanceGermplasmGeneration(plotRecord.germplasmGeneration)
            germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_CROSS_CODE')

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false

            let newGermplasmDbId = result[0].id
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 2. Create germplasm_name record
            let germplasmNameInfo = {}
            germplasmNameInfo["germplasm_id"] = newGermplasmDbId
            germplasmNameInfo["name_value"] = germplasmInfo["designation"]
            germplasmNameInfo["germplasm_name_type"] = germplasmInfo["germplasm_name_type"]
            germplasmNameInfo["germplasm_name_status"] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if (!result) return false

            let newGermplasmNameDbId = result[0].id

            // 3. Create germplasm_relation record
            await seedlotHelper.populateRelationTable('germplasm', 'germplasm', plotRecord.germplasmDbId, newGermplasmDbId, userId)

            // 4. Add new germplasm to the family
            let familyDbId = entitiesArray.plot.familyDbId
            if (familyDbId !== undefined && familyDbId !== null) {
                result = await seedlotHelper.addFamilyMember(familyDbId, newGermplasmDbId, userId)
                if(!result) return false
            }

            // 5. Create seed record
            let seedInfo = {}
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)

            // build seed name
            let occurrenceCount = await seedlotHelper.getOccurrenceCountOfExperiment(plotRecord.experimentDbId)
            let additionalCondition = occurrenceCount == 1 ? 'single_occurrence' : 'multiple_occurrence'
            
            optional['additionalCondition'] = additionalCondition

            let newSeedName = await seedlotHelper.buildSeedName(entitiesArray, optional)

            seedInfo["seed_name"] = newSeedName
            seedInfo["seed_code"] = newSeedCode
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['germplasm_id'] = newGermplasmDbId
            seedInfo['program_id'] = plotRecord.programDbId
            seedInfo['source_experiment_id'] = plotRecord.experimentDbId
            seedInfo['source_occurrence_id'] = plotRecord.occurrenceDbId
            seedInfo['source_location_id'] = plotRecord.locationDbId
            seedInfo['source_plot_id'] = plotRecord.plotDbId
            seedInfo['source_entry_id'] = plotRecord.entryDbId
            seedInfo['cross_id'] = plotRecord.crossDbId
            seedInfo['harvest_source'] = harvestSource

            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if (!result) return false

            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 6. Create seed_relation record
            await seedlotHelper.populateRelationTable('germplasm', 'seed', plotRecord.seedDbId, newSeedDbId, userId)

            // 7. Create package record

            // create new package label
            let packageLabelObject = await seedlotHelper.buildPackageLabel(plotRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel

            entitiesArray = packageLabelObject.entitiesArray

            let packageInfo = {}
            let packageCodePrefix = prefixes.packageCodePrefix
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_status'] = 'active'
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = plotRecord.programDbId

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false

            let newPackageDbId = result[0].id
        }
    } catch (error) {
        return false
    }

    return true
}

async function harvestBulkBiParentalCrossCowPea(crossRecord, harvestData, userId, harvestSource, prefixes, optional){
    let result
    let entitiesArray = {
        record: crossRecord,
        cross: crossRecord,
        harvestMode: crossRecord.harvestMode
    }

    try {
        let crossParents = await seedlotHelper.getCrossParents(crossRecord.crossDbId)
        entitiesArray.femaleCrossParent = crossParents.femaleParent
        entitiesArray.maleCrossParent = crossParents.maleParent
        let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        let isBothParentsFixed = femaleParentGermplasm.germplasmState == "fixed" && maleParentGermplasm.germplasmState == "fixed"
        let F1FChild = await seedlotHelper.getF1FGermplasmChild(crossParents.femaleParent.germplasmDbId, crossParents.maleParent.germplasmDbId)
        let isNewCross = F1FChild == undefined? true: false
        let seedGermplasmDbId = null

        // create new child
        if(isNewCross){
            // Build germplasm
            let newDesignation = await seedlotHelper.buildGermplasmDesignation(entitiesArray)
            
            // Parentage process
            let parentageProcessResult = await seedlotHelper.wheatParentageProcess(crossParents)

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode(prefixes.germplasmCodePrefix)
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['generation'] = 'F1'
            germplasmInfo['designation'] = newDesignation
            germplasmInfo['germplasm_normalized_name'] = await germplasmHelper.normalizeText(newDesignation)
            germplasmInfo['parentage'] = await seedlotHelper.riceParentageProcess(femaleParentGermplasm, maleParentGermplasm, crossRecord.crossMethod)
            germplasmInfo['taxonomy_id'] = await seedlotHelper.taxonomyProcess(femaleParentGermplasm, maleParentGermplasm)
            germplasmInfo["germplasm_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
            germplasmInfo["germplasm_name_type"] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_CROSS_CODE')
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false

            let newGermplasmDbId = result[0].id
            seedGermplasmDbId = newGermplasmDbId
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_CROSS_CODE')
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (male, female)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

        }
        // use existing child
        else{
            seedGermplasmDbId = F1FChild.childGermplasmDbId
        }

        // 2.1 Create seed
        let parentNurseryStatus = 'different_nurseries'
        
        let seedCodePrefix = prefixes.seedCodePrefix
        let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
        let newSeedName = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        
        if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
            parentNurseryStatus = 'same_nursery'
        }
        
        let seedInfo = {}
        seedInfo['germplasm_id'] = seedGermplasmDbId
        seedInfo['cross_id'] = crossRecord.crossDbId
        seedInfo['harvest_method'] = harvestData.harvest_method
        seedInfo['seed_name'] = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
        seedInfo['source_experiment_id'] = crossRecord.experimentDbId
        seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
        seedInfo['source_location_id'] = crossRecord.locationDbId
        seedInfo['source_entry_id'] = null  // from female cross parent and selected occ/loc
        seedInfo['harvest_date'] = harvestData.harvest_date
        seedInfo['seed_code'] = newSeedCode
        seedInfo['program_id'] = crossRecord.programDbId
        seedInfo['source_plot_id'] = null
        seedInfo['harvest_source'] = harvestSource
        
        result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)

        if(!result) return false
        let newSeedDbId = result[0].id
        
        entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

        // 2.2 Create seed relation records (male, female)

        // female
        let seedRelationFemaleInfo = {}
        seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
        seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
        seedRelationFemaleInfo['order_number'] = 1
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)

        if(!result) return false

        // male
        let seedRelationMaleInfo = {}
        seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
        seedRelationMaleInfo['child_seed_id'] = newSeedDbId
        seedRelationMaleInfo['order_number'] = 2
        result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)

        if(!result) return false

        // 3.1 Create package label
 
        let packageCodePrefix = prefixes.packageCodePrefix
        let packageLabelObject = await seedlotHelper.buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
        let newPackageLabel = packageLabelObject.newPackageLabel
        let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
        
        // 3.2 Create package 
        entitiesArray = packageLabelObject.entitiesArray

        let packageInfo = {}
        packageInfo['seed_id'] = newSeedDbId
        packageInfo['program_id'] = crossRecord.programDbId // = seed.source_experiment -> experiment.program_id
        packageInfo['package_label'] = newPackageLabel
        packageInfo['package_quantity'] = 0
        packageInfo['package_unit'] = 'g'
        packageInfo['package_code'] = newPackageCode
        packageInfo['package_status'] = 'active'

        result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)

        if(!result) return false
        let newPackageDbId = result[0].id
    }
    catch(error) {
        return false
    }

    return true
}

module.exports = {
    harvestSinglePlantSelectionNotFixedCowPea: async (dbRecord, harvestData, userId, harvestSource, prefixes, optional) => {
        return await harvestSinglePlantSelectionNotFixedCowPea(dbRecord, harvestData, userId, harvestSource, prefixes, optional);
    },

    harvestBulkBiParentalCrossCowPea: async (dbRecord, harvestData, userId, harvestSource, prefixes) => {
        return harvestBulkBiParentalCrossCowPea(dbRecord, harvestData, userId, harvestSource, prefixes);
    },
    
    harvestBulkBackcrossCowpea: async (crossRecord, harvestData, userId, harvestSource, prefixes = {}, optional = {}) => {
        let result
        let entitiesArray = {
            record: crossRecord,
            cross: crossRecord,
            harvestMode: crossRecord.harvestMode
        }

        try {
            // Retrieve cross parents
            let crossDbId = crossRecord.crossDbId
            let crossParents = await seedlotHelper.getCrossParents(crossDbId)
            let femaleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
            let maleParentGermplasm = await seedlotHelper.getGermplasmInfo(crossParents.maleParent.germplasmDbId)
            entitiesArray.femaleCrossParent = crossParents.femaleParent
            entitiesArray.maleCrossParent = crossParents.maleParent
            entitiesArray.femaleParentGermplasm = femaleParentGermplasm
            entitiesArray.maleParentGermplasm = maleParentGermplasm

            // Find the recurrent parent
            let recurrentParentCheck = await seedlotHelper.findBackcrossRecurrentParent(
                femaleParentGermplasm.germplasmDbId,
                maleParentGermplasm.germplasmDbId
            )

            
            // if no recurrent parent, the pairing is not a true backcross
            // so the harvest fails
            if (recurrentParentCheck == null) {
                return {
                    success: false,
                    errors: [ "No recurrent parent found." ]
                }
            }
            // COWPEA RULE: If the non-recurrent parent IS NOT the female,
            // harvest fails.
            if (recurrentParentCheck.nonRecurrentParentGermplasm.germplasmDbId != femaleParentGermplasm.germplasmDbId) {
                return {
                    success: false,
                    errors: [ "The non-recurrent parent is not female." ]
                }
            }

            // add recurrent and non recurrent parents to entities array
            let recurrentParentGermplasm = recurrentParentCheck.recurrentParentGermplasm
            let nonRecurrentParentGermplasm = recurrentParentCheck.nonRecurrentParentGermplasm
            entitiesArray.recurrentParentGermplasm = recurrentParentGermplasm
            entitiesArray.nonRecurrentParentGermplasm = nonRecurrentParentGermplasm

            // Pre-processing: remove -BN before bulding new designation
            let regexExp = /\-B[1-9][0-9]*$/g
            let matches = entitiesArray.nonRecurrentParentGermplasm.designation.match(regexExp)
            if (matches) {
                entitiesArray.nonRecurrentParentGermplasm.designation = entitiesArray.nonRecurrentParentGermplasm.designation.replace(regexExp,'')
            }
            // Build designation
            let newDesignation = await seedlotHelper.buildGermplasmDesignation(entitiesArray)

            // Build parentage based on recurrent parent
            let parentageProcess = await seedlotHelper.backcrossParentageProcess(entitiesArray, 'designation', false)
            let newParentage = parentageProcess.bcParentage

            // Determine taxonomy
            let taxonomyInfo = await seedlotHelper.determineTaxonomy(
                femaleParentGermplasm,
                maleParentGermplasm,
                {
                    cropCode: entitiesArray.record.cropCode, 
                    programCode: entitiesArray.record.programCode
                }
            )
            if (taxonomyInfo.taxonomyDbId == null) {
                return false
            }

            // 1.1 Create germplasm
            let germplasmInfo = {}
            germplasmInfo['designation'] = newDesignation
            germplasmInfo["germplasm_code"] = await seedlotHelper.generateGermplasmCode()
            germplasmInfo['parentage'] = newParentage
            germplasmInfo['generation'] = await seedlotHelper.advanceGermplasmBCGeneration(nonRecurrentParentGermplasm.generation)
            germplasmInfo['germplasm_state'] = "not_fixed"
            germplasmInfo['germplasm_name_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_NAME_TYPE_CROSS_CODE')
            germplasmInfo['taxonomy_id'] = taxonomyInfo.taxonomyDbId
            germplasmInfo['germplasm_type'] = await seedlotHelper.getScaleValueByAbbrev('GERMPLASM_TYPE_SEGREGATING')
            germplasmInfo['crop_id'] = crossRecord.cropDbId

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm', germplasmInfo, userId)
            if (!result) return false
            let newGermplasmDbId = result[0].id
            bcGermplasmDbId = newGermplasmDbId
            entitiesArray.germplasm = await seedlotHelper.getGermplasmRecord(newGermplasmDbId);

            // 1.2 Create germplasm name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = newGermplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = "bcid"
            germplasmNameInfo['germplasm_name_status'] = "standard"

            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false
            let newGermplasmNameDbId = result[0].id

            // 1.3 Create germplasm relation records (female and male)
            let germplasmRelationFemaleInfo = {}
            germplasmRelationFemaleInfo['parent_germplasm_id'] = crossParents.femaleParent.germplasmDbId
            germplasmRelationFemaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationFemaleInfo, userId)
            if(!result) return false

            let germplasmRelationMaleInfo = {}
            germplasmRelationMaleInfo['parent_germplasm_id'] = crossParents.maleParent.germplasmDbId
            germplasmRelationMaleInfo['child_germplasm_id'] = newGermplasmDbId
            germplasmRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'germplasm_relation', germplasmRelationMaleInfo, userId)
            if(!result) return false

            // 2.1 Create seed
            let seedCodePrefix = prefixes.seedCodePrefix
            let newSeedCode = await seedlotHelper.generateSeedCode(seedCodePrefix)
            let parentNurseryStatus = 'different_nurseries'
            if (crossParents.femaleParent.sourceExperimentDbId == crossParents.maleParent.sourceExperimentDbId) {
                parentNurseryStatus = 'same_nursery'
            }
            let seedInfo = {}
            seedInfo['germplasm_id'] = bcGermplasmDbId
            seedInfo['cross_id'] = crossRecord.crossDbId
            seedInfo['harvest_method'] = harvestData.harvest_method
            seedInfo['seed_name'] = await seedlotHelper.seedNameBuilder(entitiesArray, {additionalCondition:parentNurseryStatus})
            seedInfo['source_experiment_id'] = crossRecord.experimentDbId
            seedInfo['source_occurrence_id'] = crossRecord.occurrenceDbId
            seedInfo['source_location_id'] = crossRecord.locationDbId
            seedInfo['harvest_date'] = harvestData.harvest_date
            seedInfo['seed_code'] = newSeedCode
            seedInfo['program_id'] = crossRecord.programDbId
            seedInfo['harvest_source'] = harvestSource
            
            result = await seedlotHelper.insertRecord('germplasm', 'seed', seedInfo, userId)
            if(!result) return false

            let newSeedDbId = result[0].id
            entitiesArray.seed = await seedlotHelper.getSeedInformation(newSeedDbId)

            // 2.2 Create seed relation records (female and male)
            let seedRelationFemaleInfo = {}
            seedRelationFemaleInfo['parent_seed_id'] = crossParents.femaleParent.seedDbId
            seedRelationFemaleInfo['child_seed_id'] = newSeedDbId
            seedRelationFemaleInfo['order_number'] = 1
            result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationFemaleInfo, userId)
            if(!result) return false

            let seedRelationMaleInfo = {}
            seedRelationMaleInfo['parent_seed_id'] = crossParents.maleParent.seedDbId
            seedRelationMaleInfo['child_seed_id'] = newSeedDbId
            seedRelationMaleInfo['order_number'] = 2
            result = await seedlotHelper.insertRecord('germplasm', 'seed_relation', seedRelationMaleInfo, userId)
            if(!result) return false

            // 3 Create package
            let packageInfo = {}
            // Create package label
            let packageCodePrefix = prefixes.packageCodePrefix
            let packageLabelObject = await seedlotHelper.buildPackageLabel(crossRecord, entitiesArray, harvestSource, userId)
            let newPackageLabel = packageLabelObject.newPackageLabel
            let newPackageCode = await seedlotHelper.generatePackageCode(packageCodePrefix)
            entitiesArray = packageLabelObject.entitiesArray
            packageInfo['seed_id'] = newSeedDbId
            packageInfo['program_id'] = crossRecord.programDbId
            packageInfo['package_label'] = newPackageLabel
            packageInfo['package_quantity'] = 0
            packageInfo['package_unit'] = 'g'
            packageInfo['package_code'] = newPackageCode
            packageInfo['package_status'] = 'active'

            result = await seedlotHelper.insertRecord('germplasm', 'package', packageInfo, userId)
            if(!result) return false

            let newPackageDbId = result[0].id
        } catch (error) {
            return false
        }

        return true
    }
}