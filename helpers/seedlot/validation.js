/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let errorBuilder = require('../error-builder')
let errors = require('restify-errors')

module.exports = {
    /**
     * Updates the harvest_status if harvest data is supplied or not for plots
     * in advancements
     * @param {*} req Request parameters
     * @param {*} res Response
     * @param {string} plotIdList string list of plot ids
     * @param {*} transaction Database Transaction
     */
    updatePlotAdvancingHarvestStatus: async (req, res, plotIdList, transaction) => {
        try{
            let updateQuery = `
                WITH plotHarvestData AS (
                    SELECT
                        *
                    FROM
                        CROSSTAB(
                        '
                            SELECT
                                DISTINCT plot_id, 
                                variable_id, 
                                data_value 
                            FROM experiment.plot_data 
                            WHERE plot_id IN (${plotIdList}) 
                                AND is_void = false
                            ORDER by plot_id
                        ',
                        '
                            SELECT 
                                id 
                            FROM master.variable 
                            WHERE 
                                abbrev IN (
                                    ''HV_METH_DISC'', ''HVDATE_CONT'', ''NO_OF_PLANTS'', 
                                    ''SPECIFIC_PLANT'', ''PANNO_SEL'', ''NO_OF_EARS'', ''NO_OF_BAGS''
                                )
                            ORDER BY variable.abbrev
                        '       
                        ) 
                        AS (
                            "plot_id" integer,
                            harvest_date text, 
                            harvest_method text, 
                            no_of_bags text,
                            no_of_ears text,
                            no_of_plants text , 
                            panno_sel text,
                            specific_plant text
                        )
                ),
                plots AS (
                    SELECT
                        plotHarvestData.*,
                        stage.stage_name,
                        stage.stage_code,
                        (
                            SELECT
                                id
                            FROM
                                germplasm.germplasm
                            WHERE
                                germplasm.designation IN (
                                    SELECT
                                        g.designation || '-' || seq.sp_array[seq.s] AS "sp_designation"
                                    FROM
                                        (
                                            SELECT 
                                                generate_subscripts(string_to_array(plotHarvestData.specific_plant,','), 1) AS s,
                                                string_to_array(plotHarvestData.specific_plant,',') AS sp_array,
                                                plot_id
                                            FROM
                                                plotHarvestData
                                        ) seq
                                    WHERE
                                        plotHarvestData.plot_id = seq.plot_id
                                )
                                AND germplasm.is_void = FALSE
                            LIMIT 1
                        ) AS child_germplasm,
						(
							SELECT EXISTS(
								SELECT
									pd.id
								FROM
									experiment.plot_data pd
                                JOIN master.variable v
                                    ON v.id = pd.variable_id
								WHERE
									pd.plot_id = plotHarvestData.plot_id
									AND pd.is_void = FALSE
									AND pd.data_qc_code = 'B'
                                    AND v.abbrev IN (
                                        'HV_METH_DISC', 'HVDATE_CONT', 'NO_OF_PLANTS', 
                                        'SPECIFIC_PLANT', 'PANNO_SEL', 'NO_OF_EARS', 'NO_OF_BAGS')
                                LIMIT 1
							)
						) AS has_b_data_qc_code
                    FROM
                        plotHarvestData
                    LEFT JOIN
                        experiment.planting_instruction pi ON pi.plot_id = plotHarvestData.plot_id AND pi.is_void = FALSE
                    LEFT JOIN
                        experiment.plot plot ON plot.id = plotHarvestData.plot_id AND plot.is_void = FALSE
                    LEFT JOIN
                        experiment.occurrence occ ON occ.id = plot.occurrence_id AND occ.is_void = FALSE
                    LEFT JOIN
                        experiment.experiment exp ON exp.id = occ.experiment_id AND exp.is_void = FALSE
                    LEFT JOIN
                        tenant.stage stage ON stage.id = exp.stage_id AND stage.is_void = FALSE
                    LEFT JOIN
                    germplasm.germplasm g ON g.id = pi.germplasm_id AND g.is_void = FALSE
                )

                UPDATE experiment.plot 
                SET harvest_status= 
                    CASE 
                        WHEN harvest_method IS NULL
                            AND harvest_date IS NULL
                            AND no_of_ears IS NULL
                            AND no_of_plants IS NULL
                            AND panno_sel IS NULL
                            AND specific_plant IS NULL
                            THEN 'NO_HARVEST'
                        WHEN germplasm_state IN ('unknown') THEN 'INVALID_STATE'
                        WHEN harvest_date IS NULL
                            AND crop_code NOT IN ('PIGEONPEA', 'PEARLMILLET', 'FINGERMILLET', 'SORGHUM', 'GROUNDNUT', 'CHICKPEA') 
                            THEN 'INCOMPLETE: HVDATE'
                        WHEN stage_code IN ('TCV')
                            AND germplasm_state = 'not_fixed'
                            AND harvest_date IS NOT NULL THEN 'READY'
                        WHEN crop_code = 'MAIZE'
                            AND germplasm_type = 'haploid'
                            AND harvest_method IS NOT NULL
                            AND LOWER(harvest_method) <> 'dh1 individual ear'
                            THEN 'CONFLICT: Haploid cannot be harvested'
                        WHEN crop_code = 'MAIZE'
                            AND germplasm_type NOT IN ('population', 'synthetic', 'composite', 'landrace')
                            AND harvest_method IS NOT NULL
                            AND LOWER(harvest_method) = 'maintain and bulk'
                            THEN 'CONFLICT: Harvest method not compatible with germplasm type'
                        WHEN crop_code <> 'RICE' AND germplasm_state = 'fixed' AND harvest_method IS NULL THEN 'INCOMPLETE: HV_METH'
                        WHEN germplasm_state <> 'fixed' AND harvest_method IS NULL THEN 'INCOMPLETE: HV_METH'
                        WHEN crop_code = 'RICE'
                            AND germplasm_state = 'fixed' 
                            AND germplasm_type = 'fixed_line' 
                            AND LOWER(harvest_method) IN ('single plant seed increase') 
                            AND no_of_plants IS NULL
                            THEN 'INCOMPLETE: NO OF PLANTS'
                        WHEN LOWER(harvest_method) IN ('individual spike', 'individual plant', 'single plant', 'single plant selection', 'single plant selection and bulk') AND no_of_plants IS NULL THEN 'INCOMPLETE: NO OF PLANTS SELECTED'
                        WHEN LOWER(harvest_method) IN ('individual ear', 'dh1 individual ear') AND no_of_ears IS NULL THEN 'INCOMPLETE: Number of Ears Selected'
                        WHEN LOWER(harvest_method) IN ('plant-specific', 'plant-specific and bulk') AND specific_plant IS NULL THEN 'INCOMPLETE: Specific plant no.'
                        WHEN LOWER(harvest_method) IN ('panicle selection') AND panno_sel IS NULL THEN 'INCOMPLETE: Number of Panicles Selected'
                        WHEN LOWER(harvest_method) IN ('r-line harvest') AND no_of_bags IS NULL THEN 'INCOMPLETE: Number of Bags Selected'
                        WHEN has_b_data_qc_code = TRUE
                            THEN 'BAD_QC_CODE'
                        WHEN harvest_status IN ('NO_HARVEST', 'REVERT_IN_PROGRESS', 'DELETION_IN_PROGRESS', 'UPDATE_IN_PROGRESS', 'BAD_QC_CODE') 
                            OR harvest_status ILIKE 'INCOMPLETE%' OR harvest_status ILIKE 'CONFLICT%'
                            THEN 'READY'
                        ELSE harvest_status
                    END
                FROM 
                    plots,
                    experiment.entry,
                    germplasm.germplasm,
                    tenant.crop
                WHERE
                    germplasm.id = entry.germplasm_id 
                    AND germplasm.is_void = FALSE
                    AND crop.id = germplasm.crop_id
                    AND crop.is_void = FALSE
                    AND entry.id = plot.entry_id
                    AND entry.is_void = FALSE
                    AND plots.plot_id = plot.id
                    AND plot.is_void = FALSE
            `

            await sequelize.query(updateQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })
        }catch(err){
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }
    },

    /**
     * Updates the harvest_status if harvest data is supplied or not for crosses
     * @param {*} req Request parameters
     * @param {*} res Response
     * @param {string} crossIdList string list of cross ids
     * @param {*} transaction Database Transaction
     */
    updateCrossHarvestStatus: async (req, res, crossIdList, transaction) => {
        try{    
            let updateQuery = `
            WITH crossHarvestData AS (
                SELECT
                    *
                FROM
                    CROSSTAB(
                    '
                    SELECT
                        DISTINCT cross_id, 
                        variable_id, 
                        data_value 
                    FROM germplasm.cross_data 
                    WHERE cross_id IN (${crossIdList})
                        AND is_void = false
                    ORDER by cross_id
                    ',
                    '
                    SELECT 
                        id 
                    FROM master.variable 
                    WHERE 
                        abbrev IN (
                            ''DATE_CROSSED'', ''HV_METH_DISC'', 
                            ''NO_OF_SEED'', ''HVDATE_CONT'',
                            ''NO_OF_PLANTS'', ''PANNO_SEL'', 
                            ''SPECIFIC_PLANT'', ''NO_OF_EARS'', ''NO_OF_BAGS''
                        )
                    ORDER BY variable.abbrev
                    '       
                    ) AS (
                        "cross_id" integer,
                        date_crossed text, 
                        harvest_date text, 
                        harvest_method text,
						no_of_bags text,
                        no_of_ears text, 
                        no_of_plants text,
                        no_of_seed text,
                        panno_sel text,
                        specific_plant text
                        )
            ),
			crosses AS (
				SELECT
					crossHarvestData.*,
					(
						SELECT
							id
						FROM
							germplasm.germplasm
						WHERE
							germplasm.designation IN (
								SELECT
									g.designation || '-' || seq.sp_array[seq.s] AS "sp_designation"
								FROM
									(
										SELECT 
											generate_subscripts(string_to_array(crossHarvestData.specific_plant,','), 1) AS s,
											string_to_array(crossHarvestData.specific_plant,',') AS sp_array,
											cross_id
										FROM
											crossHarvestData
									) seq
								WHERE
									crossHarvestData.cross_id = seq.cross_id
							)
							AND germplasm.is_void = FALSE
						LIMIT 1
					) AS child_germplasm,
                    (
                        SELECT EXISTS(
                            SELECT
                                cd.id
                            FROM
                                germplasm.cross_data cd
                            JOIN master.variable v
                                ON v.id = cd.variable_id
                            WHERE
                                cd.cross_id = crossHarvestData.cross_id
                                AND cd.is_void = FALSE
                                AND cd.data_qc_code = 'B'
                                AND v.abbrev IN (
                                    'DATE_CROSSED', 'HV_METH_DISC', 
                                    'NO_OF_SEED', 'HVDATE_CONT',
                                    'NO_OF_PLANTS', 'PANNO_SEL', 
                                    'SPECIFIC_PLANT', 'NO_OF_EARS', 'NO_OF_BAGS')
                            LIMIT 1
                        )
                    ) AS has_b_data_qc_code
				FROM
					crossHarvestData
				LEFT JOIN
					germplasm.cross_parent cp ON cp.cross_id = crossHarvestData.cross_id AND cp.is_void = FALSE
				LEFT JOIN
					germplasm.germplasm g ON g.id = cp.germplasm_id AND g.is_void = FALSE
			),
            withFemale AS (
                SELECT
                    crosses.*,
                    female_crop.crop_code AS female_crop_code,
                    female_germplasm.germplasm_state AS female_germplasm_state,
                    female_germplasm.germplasm_type AS female_germplasm_type
                FROM
                    crosses
				LEFT JOIN LATERAL(
					SELECT
						germplasm_id
					FROM
						germplasm.cross_parent
					WHERE
						cross_id = crosses."cross_id"
						AND parent_role = 'female'
					LIMIT 1
				) female ON true
                LEFT JOIN germplasm.germplasm female_germplasm ON female_germplasm.id = female.germplasm_id AND female_germplasm.is_void = false
                LEFT JOIN tenant.crop female_crop ON female_crop.id = female_germplasm.crop_id AND female_crop.is_void = false
            ),
            withMale AS (
                SELECT
                    withFemale.*,
                    male_crop.crop_code AS male_crop_code,
                    male_germplasm.germplasm_state AS male_germplasm_state,
                    male_germplasm.germplasm_type AS male_germplasm_type
                FROM
                    withFemale
				LEFT JOIN LATERAL(
					SELECT
						germplasm_id
					FROM
						germplasm.cross_parent
					WHERE
						cross_id = withFemale."cross_id"
						AND parent_role = 'male'
					LIMIT 1
				) male ON true
                LEFT JOIN germplasm.germplasm male_germplasm ON male_germplasm.id = male.germplasm_id AND male_germplasm.is_void = false
                LEFT JOIN tenant.crop male_crop ON male_crop.id = male_germplasm.crop_id AND male_crop.is_void = false
            ),
			withSelfing AS (
                SELECT
                    withMale.*,
                    selfing_crop.crop_code AS selfing_crop_code,
                    selfing_germplasm.germplasm_state AS selfing_germplasm_state,
                    selfing_germplasm.germplasm_type AS selfing_germplasm_type
                FROM
                    withMale
				LEFT JOIN LATERAL(
					SELECT
						germplasm_id
					FROM
						germplasm.cross_parent
					WHERE
						cross_id = withMale."cross_id"
						AND parent_role = 'female-and-male'
					LIMIT 1
				) selfing ON true
                LEFT JOIN germplasm.germplasm selfing_germplasm ON selfing_germplasm.id = selfing.germplasm_id AND selfing_germplasm.is_void = false
                LEFT JOIN tenant.crop selfing_crop ON selfing_crop.id = selfing_germplasm.crop_id AND selfing_crop.is_void = false
            )
                
            UPDATE germplasm.cross "cross"
            SET harvest_status = 
                CASE 
                    WHEN cross_method = 'selfing'
                    THEN
                        CASE
                            WHEN harvest_method IS NULL
                                AND harvest_date IS NULL
                                AND no_of_ears IS NULL
                                AND no_of_plants IS NULL
                                AND panno_sel IS NULL
                                AND specific_plant IS NULL
                                THEN 'NO_HARVEST'
                            WHEN selfing_germplasm_state IN ('unknown') THEN 'INVALID_STATE'
                            WHEN
                                (
                                    (harvest_date IS NULL AND selfing_crop_code NOT IN ('PIGEONPEA', 'PEARLMILLET', 'FINGERMILLET', 'SORGHUM', 'GROUNDNUT', 'CHICKPEA'))
                                    OR harvest_method IS NULL
                                )
                                OR (LOWER(harvest_method) IN ('individual spike', 'individual plant', 'single plant', 'single plant selection', 'single plant selection and bulk') AND no_of_plants IS NULL)
                                OR (LOWER(harvest_method) IN ('individual ear', 'dh1 individual ear') AND no_of_ears IS NULL)
                                OR (LOWER(harvest_method) IN ('plant-specific', 'plant-specific and bulk') AND specific_plant IS NULL)
                                OR (LOWER(harvest_method) IN ('panicle selection') AND panno_sel IS NULL)
                            THEN
                                'INCOMPLETE'
                            WHEN selfing_crop_code = 'MAIZE'
                                AND selfing_germplasm_type = 'haploid'
                                AND harvest_method IS NOT NULL
                                AND LOWER(harvest_method) <> 'dh1 individual ear'
                                THEN 'CONFLICT: Haploid cannot be harvested'
                            WHEN selfing_crop_code = 'RICE'
                                AND selfing_germplasm_state = 'fixed' 
                                AND selfing_germplasm_type = 'fixed_line' 
                                AND LOWER(harvest_method) IN ('single plant seed increase')
                                AND no_of_plants IS NULL
                                THEN 'INCOMPLETE: NO OF PLANTS'
                            WHEN has_b_data_qc_code = TRUE
                                THEN 'BAD_QC_CODE'
                            ELSE
                                'READY'
                        END
                    -- rice: when all harvest data are missing, transition to NO_HARVEST
                    WHEN female_crop_code = 'RICE' AND male_crop_code = 'RICE' 
                        AND
                        (
                            harvest_method IS NULL
                            AND harvest_date IS NULL
                            AND date_crossed IS NULL
                            AND no_of_bags IS NULL
                            AND no_of_ears IS NULL
                            AND no_of_plants IS NULL
                            AND no_of_seed IS NULL
                            AND panno_sel IS NULL
                            AND specific_plant IS NULL
                        )
                        THEN 'NO_HARVEST'
                    WHEN harvest_method IS NULL
                        AND harvest_date IS NULL
                        AND date_crossed IS NULL
                        AND no_of_bags IS NULL
                        AND no_of_ears IS NULL
                        AND no_of_plants IS NULL
                        AND no_of_seed IS NULL
                        AND panno_sel IS NULL
                        AND specific_plant IS NULL
                        THEN 'NO_HARVEST'
                    -- rice general rules
                    WHEN female_crop_code = 'RICE' AND male_crop_code = 'RICE' 
                        AND LOWER(cross_method) NOT IN ('transgenesis', 'genome editing')
                        AND
                        (
                            harvest_method IS NULL
                            OR harvest_date IS NULL
                            OR date_crossed IS NULL
                        )
                        THEN
                            'INCOMPLETE: ' ||
                            RIGHT(
                                (CASE WHEN harvest_method IS NOT NULL THEN '' ELSE ', HARV METH' END) ||
                                (CASE WHEN harvest_date IS NOT NULL THEN '' ELSE ', HARVEST DATE' END) ||
                                (CASE WHEN date_crossed IS NOT NULL THEN '' ELSE ', CROSSING DATE' END), 
                            
                                LENGTH(
                                    (CASE WHEN harvest_method IS NOT NULL THEN '' ELSE ', HARV METH' END) ||
                                    (CASE WHEN harvest_date IS NOT NULL THEN '' ELSE ', HARVEST DATE' END) ||
                                    (CASE WHEN date_crossed IS NOT NULL THEN '' ELSE ', CROSSING DATE' END)
                                )-1
                            )
                    -- rice special rules: transgenesis/genome editing
                    WHEN female_crop_code = 'RICE' AND male_crop_code = 'RICE' 
                        AND LOWER(cross_method) IN ('transgenesis', 'genome editing')
                        AND
                        (
                            harvest_method IS NULL
                            OR harvest_date IS NULL
                        )
                        THEN
                            'INCOMPLETE: ' ||
                            RIGHT(
                                (CASE WHEN harvest_method IS NOT NULL THEN '' ELSE ', HARV METH' END) ||
                                (CASE WHEN harvest_date IS NOT NULL THEN '' ELSE ', HARVEST DATE' END), 
                            
                                LENGTH(
                                    (CASE WHEN harvest_method IS NOT NULL THEN '' ELSE ', HARV METH' END) ||
                                    (CASE WHEN harvest_date IS NOT NULL THEN '' ELSE ', HARVEST DATE' END)
                                )-1
                            )
                    -- rice: require no of bags
                    WHEN female_crop_code = 'RICE' AND male_crop_code = 'RICE' 
                        AND LOWER(cross_method) IN ('hybrid formation', 'cms multiplication', 'test cross')
                        AND no_of_bags IS NULL 
                        THEN 'INCOMPLETE: NO OF BAGS'
                    -- rice: require no of seeds
                    WHEN female_crop_code = 'RICE' AND male_crop_code = 'RICE' 
                        AND LOWER(cross_method) IN ('single cross', 'double cross', 'three-way cross', 'complex cross', 'backcross', 'transgenesis', 'genome editing')
                        AND no_of_seed IS NULL 
                        THEN 'INCOMPLETE: NO OF SEED'
                    -- check for haploid parents in maize
                    WHEN female_crop_code = 'MAIZE' AND male_crop_code = 'MAIZE'
                        AND female_germplasm_type = 'haploid'
                        AND male_germplasm_type = 'haploid'
                            THEN 'CONFLICT: Parents are haploids'
                    WHEN female_crop_code = 'MAIZE' AND male_crop_code = 'MAIZE'
                        AND female_germplasm_type = 'haploid'
                            THEN 'CONFLICT: Female parent is a haploid'
                    WHEN female_crop_code = 'MAIZE' AND male_crop_code = 'MAIZE'
                        AND male_germplasm_type = 'haploid'
                            THEN 'CONFLICT: Male parent is a haploid'
                    -- check required variables for wheat and maize
                    WHEN female_crop_code IN ('WHEAT', 'MAIZE', 'COWPEA', 'SOYBEAN', 'CHICKPEA', 'GROUNDNUT', 'PIGEONPEA', 'PEARLMILLET', 'FINGERMILLET', 'SORGHUM')
                        AND male_crop_code IN ('WHEAT', 'MAIZE', 'COWPEA', 'SOYBEAN', 'CHICKPEA', 'GROUNDNUT', 'PIGEONPEA', 'PEARLMILLET', 'FINGERMILLET', 'SORGHUM')
                        AND harvest_method IS NULL 
                        AND harvest_date IS NULL 
                            THEN 'INCOMPLETE: HARVEST DATE, HARV METH'

                    WHEN female_crop_code IN ('WHEAT', 'MAIZE', 'COWPEA', 'SOYBEAN', 'CHICKPEA', 'GROUNDNUT', 'PIGEONPEA', 'PEARLMILLET', 'FINGERMILLET', 'SORGHUM') 
                        AND male_crop_code IN ('WHEAT', 'MAIZE', 'COWPEA', 'SOYBEAN', 'CHICKPEA', 'GROUNDNUT', 'PIGEONPEA', 'PEARLMILLET', 'FINGERMILLET', 'SORGHUM')
                        AND harvest_method IS NULL 
                        AND harvest_date IS NOT NULL 
                            THEN 'INCOMPLETE: HARV METH'
                    WHEN female_crop_code IN ('WHEAT', 'MAIZE') AND male_crop_code IN ('WHEAT', 'MAIZE')
                        AND harvest_method IS NOT NULL 
                        AND harvest_date IS NULL 
                            THEN 'INCOMPLETE: HARVEST DATE'
                    WHEN has_b_data_qc_code = TRUE
                        THEN 'BAD_QC_CODE'
                    WHEN harvest_status IN ('NO_HARVEST', 'REVERT_IN_PROGRESS', 'DELETION_IN_PROGRESS', 'UPDATE_IN_PROGRESS', 'BAD_QC_CODE') 
                        OR harvest_status ILIKE 'INCOMPLETE%' OR harvest_status ILIKE 'CONFLICT%'
                        THEN 'READY'
                    ELSE harvest_status
                END
            FROM 
                withSelfing
            WHERE
                "cross".id = withSelfing.cross_id
        `
            await sequelize.query(updateQuery, {
                type: sequelize.QueryTypes.UPDATE,
                transaction: transaction
            })

        }catch(err){
            let errMsg = await errorBuilder.getError(req.headers.host, 500001)
            res.send(new errors.InternalError(errMsg))
            return
        }

    },
}