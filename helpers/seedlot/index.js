/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let format = require('pg-format')
let germplasmHelper = require('../../helpers/germplasm/index.js')
let errorBuilder = require('../error-builder')
let errors = require('restify-errors')
let escapeRegExp = require('lodash/fp/escapeRegExp');

async function getHarvestConfig(configAbbrev) {

    let harvestConfigConfigQuery = `
        SELECT 
            config.config_value AS "configValue"
        FROM 
            platform.config 
        WHERE 
            config.abbrev = (:configAbbrev)
            AND config.is_void = FALSE 
        `
    // Retrieve seedName config from the database
    let harvestConfig = await sequelize.query(harvestConfigConfigQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            configAbbrev: configAbbrev,
        },
    })
    return await harvestConfig[0]
}

/**
 * Gets experiment ccurrence count
 * @param {integer} experimentDbId experiment identifier
 */
async function getExperimentOccurrenceCount(experimentDbId) {

    try { 
        let experimentOccurrenceQuery = `
            SELECT 
                count(1)
            FROM 
                experiment.occurrence as "occurrence"
            WHERE 
                occurrence.experiment_id = (:experimentDbId)
                AND occurrence.is_void = FALSE 
            `
        // Retrieve seedName config from the database
        let experimentOccurenceCount = await sequelize.query(experimentOccurrenceQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                experimentDbId: experimentDbId,
            },
        })
        return await experimentOccurenceCount[0].count

    } catch (err){
        return null
    }

}

/**
 * Creates seed name
 * @param {string} seedPattern seed pattern
 * @param {object} plotRecord plot record
 * @param {integer} seedCounter seed counter
 * @param {integer} occurrenceCount occurrence count
 */
async function createSeedName(seedPattern, plotRecord, seedCounter, occurrenceCount) {
    try {
        let seedName = ""
        let patternKey = ""

        if (plotRecord.harvestMethod == "bulk") {
            patternKey = "bulk"
        } else if (String(plotRecord.harvestMethod).toLowerCase() == "individual ear") {
            patternKey = "individual_ear"
        }  else if (String(plotRecord.harvestMethod).toLowerCase() == "single plant selection" || String(plotRecord.harvestMethod).toLowerCase() == "individual spike") {
            patternKey = "single_plant"
        }else {
            patternKey = "default"
        }

        seedPattern[patternKey].forEach(async function (val, index) {

            if (val["type"] == "field") {
                
                let nameValue = plotRecord[val["plotInfoField"]]
               
                if(occurrenceCount > 1 ){
                    seedName = seedName.concat(nameValue)
                }else {
                    if(val["plotInfoField"] !==  "occurrenceCode"){
                        seedName = seedName.concat(nameValue)
                    }
                }
            }

            if (val["type"] == "delimeter") {
                seedName = seedName.concat(val["value"])
            }

            if (seedCounter > 0) {
                if (val["type"] == "counter") {
                    seedName = seedName.concat(String(seedCounter))
                }
            }
        })
        return seedName
    } catch (err) {
        return ""
    }
}

/**
 * Creates package name
 * @param {string} packagePattern package pattern
 * @param {object} seedRecord seed record
 * @param {object} plotRecord plot record
 */
async function createPackageName(packagePattern, seedRecord,plotRecord) {
    let packageName = ""
    packagePattern["pattern"].forEach(async function (val, index) {
        if (val["type"] == "seedField") {
            let nameValue = seedRecord[val["seedInfoField"]]
            packageName = packageName.concat(nameValue)
        }

        if (val["type"] == "plotField") {
            let nameValue = plotRecord[val["plotInfoField"]]
            packageName = packageName.concat(nameValue)
        }

        if (val["type"] == "delimeter") {
            packageName = packageName.concat(val["value"])
        }

        if (val["type"] == "free-text") {
            packageName = packageName.concat(val["value"])
        }

        if (val["type"] == "counter") {
            packageName = packageName.concat("001")
        }
    })
    return packageName
}

/**
 * Gets seed
 * @param {string} seedName seed name
 * @param {integer} germplasmDbId germplasm identifier
 * @param {integer} sourceExperimentDbId source experiment identifier
 * @param {integer} sourceEntryDbId source entry identifier
 * @param {integer} sourcePlotDbId source plot identifier
 * @param {integer} sourceOccurrenceDbId source occurrence identifier
 * @param {integer} sourceLocationDbId source location identifier
 */
async function getSeed(seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId) {

    try {

        let seedQuery = `
            SELECT 
                id
            FROM 
                germplasm.seed
            WHERE 
                seed.seed_name = (:seedName)
                AND seed.germplasm_id = (:germplasmDbId)
                AND seed.source_experiment_id = (:sourceExperimentDbId)
                AND seed.source_entry_id = (:sourceEntryDbId)
                AND seed.source_plot_id = (:sourcePlotDbId)
                AND seed.source_occurrence_id = (:sourceOccurrenceDbId)
                AND seed.source_location_id = (:sourceLocationDbId)
                AND seed.is_void = FALSE 
            `
        // Retrieve seedName config from the database
        let seed = await sequelize.query(seedQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                seedName: seedName,
                germplasmDbId: germplasmDbId,
                sourceExperimentDbId: sourceExperimentDbId,
                sourceEntryDbId: sourceEntryDbId,
                sourcePlotDbId: sourcePlotDbId,
                sourceOccurrenceDbId: sourceOccurrenceDbId,
                sourceLocationDbId: sourceLocationDbId
            },
        })

        return await seed[0]

    } catch (err) {
        return null
    }
}

async function getConfigValue(config, key) {
    let value = config[key]

    // if config value for key does not exist
    // get the default
    if (value == undefined || value.length == 0) {
        value = config.default
    }

    return value
}

/**
 * Generate family code from database
 * @param {string} prefix prefix for family code
 * @returns {string} familyCode newly generated family code
 */
async function generateFamilyCode(prefix = 'FAM') {
    try {
        let generateFamilyCodeQuery =
            `
            SELECT
                germplasm.generate_code('family','${prefix}')
            AS familycode 
        `
        let family = await sequelize.query(generateFamilyCodeQuery, {
            type: sequelize.QueryTypes.SELECT,
        })

        let familyCode = family[0].familycode

        return familyCode
    } catch (err) {
        return null
    }
}

/**
 * Generate germplasm code from database
 * @param {string} prefix prefix for germlasm code
 */
 async function generateGermplasmCode(prefix = 'GE') {
    try {
        let generateGermplasmCodeQuery =
            `
            SELECT
                germplasm.generate_code('germplasm','${prefix}')
            AS germplasmcode 
        `
        let germplasm = await sequelize.query(generateGermplasmCodeQuery, {
            type: sequelize.QueryTypes.SELECT,
        })

        let germplasmCode = germplasm[0].germplasmcode

        return germplasmCode
    } catch (err) {
        return null
    }
 }

/**
 * Generate seed code from database
 * @param {string} prefix prefix for seed code
 */
async function generateSeedCode(prefix = 'SEED') {
    try {
        let generateSeedCodeQuery =
            `
            SELECT
                germplasm.generate_code('seed','${prefix}')
            AS seedcode 
        `
        let seed = await sequelize.query(generateSeedCodeQuery, {
            type: sequelize.QueryTypes.SELECT,
        })

        let seedCode = seed[0].seedcode

        return seedCode
    } catch (err) {
        return null
    }
}

/**
 * Generate package code from database
 * @param {string} prefix prefix for package code
 */
async function generatePackageCode(prefix = 'PKG') {
    try {
        let generatePackageCodeQuery = `
            SELECT
                germplasm.generate_code('package','${prefix}')
            AS packagecode `

        let packageCodeResult = await sequelize.query(generatePackageCodeQuery, {
            type: sequelize.QueryTypes.SELECT,
        })

        let packageCode = packageCodeResult[0].packagecode

        return packageCode
    } catch (err) {
        return null
    }
}

/**
 * Perform text normalization
 * @param {*} text text to be normalized
 */
async function normalizeText(text) {
    try {
        let normalizeQuery =
        `
            SELECT
                platform.normalize_text('${text}')
            AS "normalizedText"
        `

        let result = await sequelize.query(normalizeQuery, {
            type: sequelize.QueryTypes.SELECT,
        })

        return result[0].normalizedText

    } catch(err) {
        return null
    }
}

/**
 * Gets the next value in the sequence
 * @param {*} schema schema where the sequence originates from
 * @param {*} sequenceName name of the sequence
 */
async function getNextInSequence(schema, sequenceName) {
    try {
        let sequenceQuery =
        `
            SELECT
                nextval('${schema}.${sequenceName}')
            AS "nextVal";
        `

        let result = await sequelize.query(sequenceQuery, {
            type: sequelize.QueryTypes.SELECT,
        })

        return result[0].nextVal

    } catch(err) {
        return null
    }
}

/**
 * Get new counter value of the given name
 * Ex. If name is IR 12345-4
 * counter value should be 5
 * @param {string} name - the last name in the sequence
 * @param {string} delimiter - delimiter of the name separating the number
 * @param {object} optional - optional values such as counter prefix and suffix
 */
 async function getCounterValue(name, delimiter, optional = {}) {
    let prefix = optional.prefix == null ? '' : optional.prefix
    let suffix = optional.suffix == null ? '' : optional.suffix

    let counter = 1

    if(name == null) {
        return counter
    }

    // split name by the delimiter
    let pieces = name.split(delimiter)

    // get the last piece
    // ex. in "IR 12345-B-2"
    // the last piece is "2"
    let lastPiece = pieces[pieces.length - 1]
    lastPiece = parseInt(lastPiece.replace(prefix,"").replace(suffix,""))

    // if last piece is not an integer, return the counter
    if(lastPiece ==  NaN) {
        return counter
    }
    
    // add 1 to the last piece and set it as counter's value
    counter = lastPiece + 1

    return counter
}

/**
 * Retrieves the last item based on a given sequence.
 * @param {string} schema - database schema name
 * @param {string} table - schema table name
 * @param {string} field - table field name
 * @param {string} startingString - starting string of the sequence
 * @param {string} pattern - regex expression of the pattern to find
 * @param {object} optional - optional values for delimiter, prefix, suffix
 * @param {boolean} strictEnd - whether or not the result should also end in the exact regex
 * @returns {string} - the current last record in the sequence
 */
async function getLastInNumberedSequence(schema, table, field, startingString, pattern, optional = {}, strictEnd = false) {
    let delimiter = optional.delimiter == undefined ? '' : optional.delimiter
    let prefix = optional.prefix == undefined ? '' : optional.prefix
    let suffix = optional.suffix == undefined ? '' : optional.suffix
    // escape special regexp characters
    startingString = escapeRegExp(startingString)
    delimiter = escapeRegExp(delimiter)
    prefix = escapeRegExp(prefix)
    suffix = escapeRegExp(suffix)
    // If strict ending, append $ at the end of the regex
    let strict = strictEnd ? '$' : ''

    try {

        // build query
        let sequenceQuery = `
            SELECT 
                ${field}
            FROM 
                ${schema}.${table}
            WHERE  
                ${field} ~ '^${startingString}${delimiter}${prefix}${pattern}${suffix}${strict}'
                AND ${table}.is_void = FALSE
            ORDER BY id
            DESC LIMIT 1
        `
        
        // Retrieve record from the database
        let records = await sequelize.query(sequenceQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        // return the field value
        return records[0][field]

    } catch (err) {
        return null
    }
}

/**
 * Get HM configs that follow the abbrev format:
 * <identifier>_<crop_code>_<program_code>
 * eg. HM_TAXONOMY_PREFERENCE_RICE_IRSEA
 * 
 * If no config exists for the program, the default is retrieved.
 * eg. HM_TAXONOMY_PREFERENCE_RICE_DEFAULT
 * @param {string} configBaseString config base string pattern
 * @param {string} cropCode crop code for config
 * @param {string} programCode program code for config
 * @returns {object} config object
 */
async function getConfigGeneric(configBaseString, cropCode, programCode) {
    // Check if there exists a config for the crop program
    let programConfigAbbrev = configBaseString + '_' + cropCode + '_' + programCode
    let cropProgramConfig = await getHarvestConfig(programConfigAbbrev)

    if (cropProgramConfig == undefined || cropProgramConfig.length == 0) {
        // if a config for the crop program does not exist, use the default config
        programConfigAbbrev = configBaseString + '_' + cropCode + '_DEFAULT'
        cropProgramConfig = await getHarvestConfig(programConfigAbbrev)
    }

    return cropProgramConfig != null ? cropProgramConfig.configValue : cropProgramConfig
}

/**
 * Retrieves the naming config for the config base string
 * given the entities and optional parameters
 * @param {string} configBaseString - base string of the config
 * @param {*} entitiesArray entities containing info needed for building names
 * @param {*} optional - optional parameters like counter, additionalConditions, etc
 */
async function getNomenclatureConfig(configBaseString, entitiesArray, optional) {
    let cropCode = entitiesArray.record.cropCode
    let additionalCondition = optional.additionalCondition
    let harvestMode = entitiesArray.harvestMode

    // Check if there exists a config for the crop program
    let programCode = entitiesArray.record.programCode
    let programConfigAbbrev = configBaseString + '_' + cropCode + '_' + programCode
    let cropProgramConfig = await getHarvestConfig(programConfigAbbrev)

    if (cropProgramConfig == undefined || cropProgramConfig.length == 0) {
        // if a config for the crop program does not exist, use the default config
        programConfigAbbrev = configBaseString + '_' + cropCode + '_DEFAULT'
        cropProgramConfig = await getHarvestConfig(programConfigAbbrev)
    }

    let cropProgramConfigArray = cropProgramConfig.configValue

    // get config for harvest mode
    let harvestModeConfig = await getConfigValue(cropProgramConfigArray, harvestMode)

    // get config for cross method
    // (if existing, otherwise, default config is returned)
    let crossMethod = String(entitiesArray.record.crossMethod).toLowerCase()
    let crossMethodConfig = await getConfigValue(harvestModeConfig, crossMethod)

    // get config for germplasm state
    // (if existing, otherwise, default config is returned)
    let germplasmState = entitiesArray.record.germplasmState
    let germplasmStateConfig = await getConfigValue(crossMethodConfig, germplasmState)

    // get config for germplasm type
    // (if existing, otherwise, default config is returned)
    let germplasmType = entitiesArray.record.germplasmType
    let germplasmTypeConfig = await getConfigValue(germplasmStateConfig, germplasmType)

    // get pattern for harvest method
    // (if existing, otherwise, default config is returned)
    let harvestMethod = entitiesArray.record.harvestMethod !== null ? entitiesArray.record.harvestMethod.trim().toLowerCase() : null
    let pattern = await getConfigValue(germplasmTypeConfig, harvestMethod)

    // check additional condition
    if(additionalCondition != undefined) {
        let acpattern = await getConfigValue(pattern, additionalCondition)
        if(acpattern != null || acpattern != undefined) pattern = acpattern
    }

    return pattern;
}

/**
 * Generic name builder
 * This is used for generating names for germplasm,
 * seeds, packages, crosses, etc.
 * @param {*} configBaseString base string if the abbrev of the name pattern
 * @param {*} entitiesArray entities containing info needed for building names
 * @param {*} optional - optional parameters like counter, additionalConditions, etc
 */
async function genericNameBuilder(configBaseString, entitiesArray, optional = {}) {
    let name = ''
    let counter = optional.counter
    let special = optional.special == undefined ? [] : optional.special

    // retrive naming pattern
    let pattern = await getNomenclatureConfig(configBaseString, entitiesArray, optional)
    let patternLength = pattern.length
    let lastValue = ''

    for (let i = 0; i < patternLength; i++) {
        let patternItem = pattern[i]
        let patternItemType = patternItem['type']
        let patternItemSpecial = patternItem.special
        let fieldValue = ''

        // if pattern item is special, but special type is not included in special array
        // skip pattern item
        if (patternItemSpecial != undefined && !special.includes(patternItemSpecial)) continue

        if (patternItemType == 'field') {
            // type: field of an entity record
            let entity = patternItem['entity']
            let fieldName = patternItem['field_name']
            fieldValue = entitiesArray[entity][fieldName]
            if (fieldValue == null || fieldValue == '') continue
        }
        else if (patternItemType == 'delimiter' || patternItemType == 'free-text') {
            // type: delimiter or free-text
            fieldValue = patternItem['value']
        }
        else if (patternItemType == 'free-text-repeater') {
            // type: free-text-repeater
            if (counter != undefined) {
                let value = patternItem['value']
                let delimiter = patternItem['delimiter']
                let minimum = patternItem['minimum']

                if (counter >= minimum) {
                    fieldValue = value + delimiter + counter
                }
                else {
                    fieldValue = value
                }

                // Remove previous repeat from name
                let repeatedRegex = "^"
                    + escapeRegExp(value)
                    + "("
                    + escapeRegExp(delimiter)
                    + "[1-9][0-9]*)*$"
                let nameParts = name.split(lastValue)
                nameParts.pop()
                if (nameParts[nameParts.length-1].match(repeatedRegex)) nameParts.pop()
                name = nameParts.join(lastValue).concat(lastValue)
            }
        }
        else if (patternItemType == 'field-repeater') {
            let skip = optional.skip_repeater == undefined ? false : optional.skip_repeater
            if (skip) continue
            // type: field-repeater
            let entity = patternItem['entity']
            let fieldName = patternItem['field_name']
            fieldValue = entitiesArray[entity][fieldName]
            if (fieldValue == null || fieldValue == '') continue

            if (counter != undefined) {
                let value = fieldValue
                let delimiter = patternItem['delimiter']
                let minimum = patternItem['minimum']

                if (counter >= minimum) {
                    fieldValue = value + delimiter + counter
                }
                else {
                    fieldValue = value
                }
            }
        }
        else if (patternItemType == 'counter') {
            // type: counter
            if (counter != undefined && counter != null && counter != '') {
                let counterVal = 0
                let leadingZeros = patternItem['leading_zero']
                
                if(leadingZeros == 'yes') {
                    let maxDigits = patternItem['max_digits']

                    let counterString = counter.toString()
                    while (counterString.length < maxDigits) counterString = "0" + counterString;

                    counterVal = counterString
                }
                else counterVal = counter

                fieldValue = String(counterVal)
            }
        }
        else if (patternItemType == 'db-sequence') {
            // type: db-sequence
            let schema = patternItem['schema']
            let sequenceName = patternItem['sequence_name']
            fieldValue = await getNextInSequence(schema, sequenceName)
        }

        lastValue = fieldValue

        name = name.concat(fieldValue)
    }

    return name
}

/**
 * Retrieve germplasm_relation records where
 * the child_germplasm_id = childGermplasmDbId
 * @param {integer} childGermplasmDbId 
 * @returns {object} - germplasm_relation records
 */
async function getGermplasmParents(childGermplasmDbId) {
    try{
        // Build query for retrieving germplasm parents
        let germplasmRelationQuery = `
        SELECT
            germplasm_relation.id AS "germplasmRelationDbId",
            childGermplasm.id AS "childGermplasmDbId",
            parentGermplasm.id AS "parentGermplasmDbId"
        FROM
            germplasm.germplasm_relation
        LEFT JOIN
            germplasm.germplasm parentGermplasm ON parentGermplasm.id = germplasm_relation.parent_germplasm_id
        LEFT JOIN
            germplasm.germplasm childGermplasm ON childGermplasm.id = germplasm_relation.child_germplasm_id
        WHERE
            germplasm_relation.child_germplasm_id = ${childGermplasmDbId}
            AND germplasm_relation.is_void = FALSE
        ORDER BY
            germplasm_relation.order_number
        `

        // Retrieve germplasm relations from the database
        let germplasmRelations = await sequelize.query(germplasmRelationQuery, {
        type: sequelize.QueryTypes.SELECT
        })

        return germplasmRelations
    }
    catch(error){
        return null
    }
}

/**
 * Retrieve crosses with cross parents that use
 * the provided germplasm ids, excluding the given cross id
 * @param {integer} crossDbId - cross id to exclude
 * @param {integer} firstGermplasmDbId - first of 2 germplasm used as parents
 * @param {integer} secondGermplasmDbId - second of 2 germplasm used as parents
 * @returns {object} crosses - contains crosses retrieved
 */
async function getOtherCrosses(crossDbId, firstGermplasmDbId, secondGermplasmDbId) {
    // build crosses query
    let crossesQuery = `
        SELECT
            "germplasmCross".id AS "crossDbId",
            "germplasmCross".cross_name AS "crossName",
            "germplasmCross".cross_method AS "crossMethod",
            "germplasmCross".experiment_id AS "experimentDbId"
        FROM
            germplasm.cross_parent first_c_parent
        LEFT JOIN
            germplasm.cross_parent second_c_parent ON second_c_parent.cross_id = first_c_parent.cross_id
        LEFT JOIN
            germplasm.cross "germplasmCross" ON "germplasmCross".id = first_c_parent.cross_id
        WHERE
            first_c_parent.germplasm_id = ${firstGermplasmDbId}
            AND second_c_parent.germplasm_id = ${secondGermplasmDbId}
        	AND "germplasmCross".id <> ${crossDbId}
            AND first_c_parent.is_void = FALSE
            AND second_c_parent.is_void = FALSE
            AND "germplasmCross".is_void = FALSE
        `
    
    // Retrieve crosses from the database
    let crosses = await sequelize.query(crossesQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    // return result
    return crosses
}

/**
 * Builds the regex for the left and right sides of the parentage
 * @param {string} recurrentStr - recurrent parent string
 * @param {object} config - the pattern for the left and right sides of the parentage
 * @returns {obect} - leftRegex and rightRegex
 */
async function generateRecurrentRegex(recurrentStr, config) {
    // escape regex characters
    let bcDelimRegEsc = escapeRegExp(config.delimiter_backcross_number)
    let desigRegEsc = escapeRegExp(recurrentStr)

    // build left regex
    let leftPattern = config.pattern[0]
    let lpLen = leftPattern.length
    let cdEncountered = false
    let leftStr = '^'

    for (let i = 0; i < lpLen; i++) {
        let patternItem = leftPattern[i]
        let patternType = patternItem.type
        
        if (patternType == 'field') {
            leftStr += desigRegEsc
        } else if (patternType == 'delimiter') {
            if(!cdEncountered) {
                leftStr += '(' + bcDelimRegEsc
                cdEncountered = true
            }
            else {
                leftStr += bcDelimRegEsc + ')*'
                cdEncountered = false
            }
        } else if (patternType == 'counter') {
            if(!cdEncountered) {
                leftStr += '(\\d+'
                cdEncountered = true
            }
            else {
                leftStr += '\\d+)*'
                cdEncountered = false
            }
        }
    }

    leftStr += '$'
    let leftRegex = new RegExp(leftStr, 'g')

    // build right regex
    let rightPattern = config.pattern[1]
    let rpLen = rightPattern.length
    cdEncountered = false
    let rightStr = '^'

    for (let i = 0; i < rpLen; i++) {
        let patternItem = rightPattern[i]
        let patternType = patternItem.type
        
        if (patternType == 'field') {
            rightStr += desigRegEsc
        } else if (patternType == 'delimiter') {
            if(!cdEncountered) {
                rightStr += '(' + bcDelimRegEsc
                cdEncountered = true
            }
            else {
                rightStr += bcDelimRegEsc + ')*'
                cdEncountered = false
            }
        } else if (patternType == 'counter') {
            if(!cdEncountered) {
                rightStr += '(\\d+'
                cdEncountered = true
            }
            else {
                rightStr += '\\d+)*'
                cdEncountered = false
            }
        }
    }

    rightStr += '$'
    let rightRegex = new RegExp(rightStr, 'g')

    return {
        leftRegex: leftRegex,
        rightRegex: rightRegex
    }
}

/**
 * Retrieves the cross parents of the given cross id
 * @param {*} crossDbId cross identifier
 */
async function getCrossParents(crossDbId) {
    let crossParentQuery = `
        SELECT 
            DISTINCT ON (cross_parent.parent_role)
            cross_parent.cross_id AS "crossDbId",
            cross_parent.parent_role AS "parentRole",
            pi_entry.germplasm_id AS "germplasmDbId",
            pi_entry.seed_id AS "seedDbId",
            pi_entry.package_id AS "packageDbId",
            experiment.id AS "sourceExperimentDbId",
            experiment.experiment_name AS "sourceExperimentName",
            pi_entry.id AS "entryDbId",
            pi_entry.entry_number AS "entryNumber",
            pi_entry.entry_code AS "entryCode",
            program.id AS "programDbId",
            program.program_code AS "programCode"
        FROM 
            germplasm.cross_parent
        LEFT JOIN
            experiment.experiment experiment ON experiment.id = cross_parent.experiment_id
        LEFT JOIN
            experiment.planting_instruction pi_entry ON pi_entry.entry_id = cross_parent.entry_id
        LEFT JOIN
            experiment.plot plot ON plot.id = pi_entry.plot_id
        LEFT JOIN
            tenant.program program ON experiment.program_id = program.id
        WHERE 
            cross_parent.cross_id = (:crossDbId)
            AND cross_parent.is_void = FALSE
            AND plot.occurrence_id = cross_parent.occurrence_id
        ORDER BY cross_parent.parent_role, cross_parent.order_number
        `
    
    // Retrieve cross parents from the database
    let crossParents = await sequelize.query(crossParentQuery, {
        type: sequelize.QueryTypes.SELECT,
        replacements: {
            crossDbId: crossDbId,
        },
    })

    if(crossParents === undefined){
        return null
    }

    if(crossParents.length < 2) {
        return {
            femaleParent: null,
            maleParent: null
        }
    }

    return {
        femaleParent : crossParents[0],
        maleParent : crossParents[1]
    }
}

/**
 * Retrieve cross information
 * @param {integer} crossDbId - cross record indentifier
 * @param {integer} occurrenceDbId - occurrence record indentifier
 */
async function getCrossInformation(crossDbId, occurrenceDbId = null) {
    /**
     * If occurrence id is specified,
     * apply confition for retrieving
     * the correct plot id.
     */
    let plotOccurrenceCondition = ``
    let crossOccurrenceCondition = ``
    if (occurrenceDbId !== null) {
        plotOccurrenceCondition = `AND plot.occurrence_id = ${occurrenceDbId}`
        crossOccurrenceCondition = `AND occurrence.id = ${occurrenceDbId}`
    }

    try{
        let crossQuery = `
            SELECT
                germplasmCross.id AS "crossDbId",
                germplasmCross.cross_name AS "crossName",
                germplasmCross.cross_method AS "crossMethod",
                crop2.id AS "cropDbId",
                crop2.crop_name AS "cropName",
                crop2.crop_code AS "cropCode",
                project.id AS "projectDbId",
                project.project_code AS "projectCode",
                project.project_name AS "projectName",
                program.id AS "programDbId",
                program.program_code AS "programCode",
                taxonomy.id AS "taxonomyDbId",
                experiment.id AS "experimentDbId",
                experiment.experiment_name AS "experimentName",
                experiment.experiment_year AS "experimentYear",
                experiment.experiment_type AS "experimentType",
                SUBSTRING(CAST(experiment.experiment_year AS text),3,2) AS "experimentYearYY",
                season.season_name AS "experimentSeason",
                season.season_code AS "experimentSeasonCode",
                femPipe.id AS "femaleParentPipelineDbId",
                femPipe.pipeline_name AS "femaleParentPipelineName",
                femPipe.pipeline_code AS "femaleParentPipelineCode",
                occurrence.id AS "occurrenceDbId",
                occurrence.occurrence_code AS "occurrenceCode",
                location.id AS "locationDbId",
                site.id AS "geospatialObjectDbId",
                CASE
                    WHEN goa.data_value IS NOT NULL THEN goa.data_value
                    ELSE geospatial_object.geospatial_object_code
                END AS "originSiteCode",
                CASE
                    WHEN fgoa.data_value IS NOT NULL THEN fgoa.data_value
                    WHEN goa.data_value IS NOT NULL THEN goa.data_value
                    ELSE geospatial_object.geospatial_object_code
                END AS "fieldOriginSiteCode",
                germplasm.id AS "germplasmDbId",
                germplasm.germplasm_state AS "germplasmState",
                germplasm.germplasm_type AS "germplasmType",
                germplasm.generation AS "germplasmGeneration",
                germplasm.designation AS "germplasmDesignation",
                germplasm.parentage AS "germplasmParentage",
                germplasm.taxonomy_id AS "germplasmTaxonomyId",
                germplasm.germplasm_name_type AS "germplasmNameType",
                seed.id AS "seedDbId",
                seed.seed_code AS "seedCode",
                seed.seed_name AS "seedName",
                geospatial_object.geospatial_object_code AS "nurserySiteCode",
                harvest_method.data_value AS "harvestMethod",
                harvest_date.data_value AS "harvestDate",
                crossing_date.data_value AS "crossingDate",
                SUBSTRING(CAST(harvest_date.data_value AS text),3,2) AS "harvestYearYY",
                CASE
                    WHEN germplasmCross.cross_method = 'selfing'
                    THEN
                        (
                            SELECT
                                plot.id
                            FROM
                                experiment.plot
                            LEFT JOIN
                                germplasm.cross_parent cp ON cp.entry_id = plot.entry_id
                            WHERE
                                cp.cross_id = germplasmCross.id
                                ${plotOccurrenceCondition}
                            ORDER BY
                                plot.plot_number ASC
                            LIMIT 1
                        )
                END AS "plotDbId",
                (
                    SELECT
                        COUNT(1)
                    FROM
                        germplasm.cross_parent
                    WHERE
                        cross_parent.cross_id = germplasmCross.id
                ) AS "crossParentCount"
            FROM
                germplasm.cross germplasmCross
            LEFT JOIN
                germplasm.cross_parent femaleCrossParent ON femaleCrossParent.cross_id = germplasmCross.id
                AND femaleCrossParent.parent_role = 'female'
                AND femaleCrossParent.is_void = FALSE
            LEFT JOIN
                experiment.occurrence femOcc ON femOcc.id = femaleCrossParent.occurrence_id AND femOcc.is_void = FALSE
            LEFT JOIN
                experiment.experiment femExp ON femExp.id = femOcc.experiment_id AND femExp.is_void = FALSE
            LEFT JOIN
                tenant.pipeline femPipe ON femPipe.id = femExp.pipeline_id AND femPipe.is_void = FALSE
            LEFT JOIN
                germplasm.germplasm germplasm ON germplasm.id = germplasmCross.germplasm_id
            LEFT JOIN
                germplasm.seed seed ON seed.id = germplasmCross.seed_id
            LEFT JOIN
                tenant.crop crop ON crop.id = germplasm.crop_id
            LEFT JOIN
                experiment.experiment experiment on experiment.id = germplasmCross.experiment_id
            LEFT JOIN
                tenant.project project on project.id = experiment.project_id
            LEFT JOIN
                tenant.program program on program.id = experiment.program_id
            LEFT JOIN
                tenant.crop_program crop_program on crop_program.id = program.crop_program_id
            LEFT JOIN
                tenant.crop crop2 on crop2.id = crop_program.crop_id
            LEFT JOIN
                germplasm.taxonomy taxonomy on taxonomy.crop_id = crop2.id
            LEFT JOIN
                tenant.season season ON season.id = experiment.season_id
            LEFT JOIN
                experiment.occurrence occurrence ON occurrence.experiment_id = experiment.id
                ${crossOccurrenceCondition}
            LEFT JOIN
                experiment.location_occurrence_group log ON log.occurrence_id = occurrence.id
            LEFT JOIN
                experiment.location location ON location.id = log.location_id
            LEFT JOIN
                place.geospatial_object site ON site.id = occurrence.site_id
            LEFT JOIN
                place.geospatial_object on geospatial_object.id=occurrence.site_id
            LEFT JOIN place.geospatial_object_attribute goa ON goa.geospatial_object_id = occurrence.site_id
                AND goa.variable_id = (
                    SELECT
                        id
                    FROM
                        master.variable
                    WHERE
                        abbrev = 'ORIGIN_SITE_CODE'
                )  AND goa.is_void = FALSE
            LEFT JOIN place.geospatial_object_attribute fgoa ON fgoa.geospatial_object_id = occurrence.field_id
                AND fgoa.variable_id = (
                    SELECT 
                        id
                    FROM 
                        master.variable
                    WHERE
                        abbrev = 'ORIGIN_SITE_CODE'
                ) AND fgoa.is_void = FALSE
            LEFT JOIN (
                    SELECT
                        cross_data.id,
                        cross_data.cross_id,
                        cross_data.data_value
                    FROM
                        germplasm.cross_data
                    LEFT JOIN
                        master.variable ON variable.id = cross_data.variable_id
                    WHERE
                        cross_data.cross_id = (:crossDbId)
                        AND variable.abbrev = 'HV_METH_DISC'
                    ORDER BY
                        cross_data.id DESC
                    LIMIT 1
                ) harvest_method ON harvest_method.cross_id = germplasmCross.id
            LEFT JOIN
                germplasm.cross_data harvest_date on harvest_date.cross_id = germplasmCross.id
                    AND harvest_date.variable_id = (
                        SELECT
                            id
                        FROM
                            master.variable
                        WHERE
                            abbrev = 'HVDATE_CONT'
                    ) AND harvest_date.is_void = FALSE
            LEFT JOIN
                germplasm.cross_data crossing_date on crossing_date.cross_id = germplasmCross.id
                    AND crossing_date.variable_id = (
                        SELECT
                            id
                        FROM
                            master.variable
                        WHERE
                            abbrev = 'DATE_CROSSED'
                    ) AND crossing_date.is_void = FALSE
            WHERE
                germplasmCross.id = (:crossDbId)
                AND germplasmCross.is_void = FALSE
        `
        // Retrieve plot from the database
        let crosses = await sequelize.query(crossQuery, {
            type: sequelize.QueryTypes.SELECT,
            replacements: {
                crossDbId: crossDbId
            }
        })

        return crosses[0]
    }
    catch(err){
        return null
    }
}

/**
 * Retrieves the family information of the given family id
 * @param {*} familyDbId family identifier
 */
async function getFamilyInfo(familyDbId) {
    let familyQuery = `
        SELECT 
            family.id AS "familyDbId",
            family.family_code AS "familyCode",
            family.family_name AS "familyName",
            family.cross_id AS "crossDbId"
        FROM 
            germplasm.family
        WHERE  
        family.id = '${familyDbId}' AND family.is_void = FALSE
    `
    // Retrieve germplasm from the database
    let family = await sequelize.query(familyQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    if(family === undefined){
        return null
    }

    return family[0]
}

/**
 * Retrieves the germplasm information of the given germplasm id
 * @param {*} germplasmDbId germplasm identifier
 */
async function getGermplasmInfo(germplasmDbId) {
    let germplasmQuery = `
        SELECT 
            germplasm.id AS "germplasmDbId",
            germplasm.designation,
            germplasm.parentage,
            germplasm.generation,
            germplasm.germplasm_state AS "germplasmState",
            germplasm.germplasm_type AS "germplasmType",
            germplasm.crop_id AS "cropDbId",
            germplasm.taxonomy_id AS "taxonomyDbId",
            (
                SELECT 
                    taxonomy.taxonomy_name AS "germplasmTaxonomyName" 
                FROM 
                    germplasm.taxonomy taxonomy 
                WHERE 
                    taxonomy.id = germplasm.taxonomy_id
            ),
            germplasm.germplasm_normalized_name AS "germplasmNormalizedName",
            gn1.name_value AS "backcrossDerivativeName"
        FROM 
            germplasm.germplasm
        LEFT JOIN
            germplasm.germplasm_name gn1 ON gn1.germplasm_id = germplasm.id
            AND gn1.germplasm_name_type = 'backcross_derivative'
            AND gn1.is_void = FALSE
        WHERE  
            germplasm.id = '${germplasmDbId}' AND germplasm.is_void = FALSE
    `
    // Retrieve germplasm from the database
    let germplasm = await sequelize.query(germplasmQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    if(germplasm === undefined){
        return null
    }

    return germplasm[0]
}

/**
 * Retrieves the child germplasm id based on the given parents
 * @param {integer} femaleParentGermplasmDbId female germplasm identifier
 * @param {integer} maleParentGermplasmDbId male germplasm identifier
 * @param {boolean} ordered if follows 1-female and 2-male ordering
 * @param {boolean} includeChildInfo if includes child info
 * @param {array} childConditions conditions for child germplasm
 * @param {boolean} includeCrossInfo if includes cross info
 * @param {boolean} getCompleteInfo if includes complete info
 */
 async function getChildGermplasmId(femaleParentGermplasmDbId, maleParentGermplasmDbId, ordered = true, includeChildInfo = false, childConditions = [], includeCrossInfo = false, getCompleteInfo = false) {
    let selectedFields = `female.child_germplasm_id AS child_germplasm_id`
    let childInfoJoins = ``
    let childConditionsQuery = ``
    let childGermplasmDbId = 0
    let orderCondition = `
        AND female.parent_germplasm_id = ${femaleParentGermplasmDbId}
        AND female.order_number = 1
        AND male.parent_germplasm_id = ${maleParentGermplasmDbId}
        AND male.order_number = 2
    `
    if(!ordered){
        orderCondition = `
            AND (
                (female.parent_germplasm_id = ${femaleParentGermplasmDbId}
                AND female.order_number = 1
                AND male.parent_germplasm_id = ${maleParentGermplasmDbId}
                AND male.order_number = 2)
            OR  (female.parent_germplasm_id = ${femaleParentGermplasmDbId}
                AND female.order_number = 2
                AND male.parent_germplasm_id = ${maleParentGermplasmDbId}
                AND male.order_number = 1)
            )
        `
    }

    if(includeChildInfo && childConditions.length > 0){
        childInfoJoins = `
            INNER JOIN
                germplasm.germplasm child
                ON female.child_germplasm_id = child.id
            INNER JOIN
                germplasm.seed
                ON seed.germplasm_id = child.id
            INNER JOIN
                tenant.program
                ON program.id = seed.program_id
        `
        childConditionsQuery = ' AND ' + childConditions.join(' AND ')

        if (includeCrossInfo) {
            childInfoJoins += `
                INNER JOIN
                    germplasm.cross gc
                    ON gc.id = seed.cross_id
            `
        }
    }

    if (getCompleteInfo) {
        selectedFields = `*`
    }

    let childGermplasmQuery = `
        SELECT
            ${selectedFields}
        FROM   
            germplasm.germplasm_relation female
        INNER JOIN
            germplasm.germplasm_relation male
            ON female.child_germplasm_id = male.child_germplasm_id
            ${orderCondition}
        ${childInfoJoins}
        WHERE
            female.is_void = false
            AND male.is_void = false
            ${childConditionsQuery}
        LIMIT  1
    `

    // Retrieve child germplasm id from the database
    let childGermplasm = await sequelize.query(childGermplasmQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    if(childGermplasm === undefined || childGermplasm.length == 0){
        return (getCompleteInfo ? null : 0)
    }

    if(childGermplasm.length > 0 ){
        if(getCompleteInfo) return childGermplasm[0]
        childGermplasmDbId = childGermplasm[0]['child_germplasm_id']
    }

    return childGermplasmDbId
}

/**
 * Retrieves the crosses count based on the given germplasm id
 * @param {integer} germplasmId germplasm identifier
 * @param {string} crossMethod optional cross method
 */
async function checkExistingCross(germplasmId,crossMethod = ''){
    let germplasmDbId = parseInt(germplasmId)
    let condition = ''
    if(crossMethod !== ''){
        condition = `
            AND cross_method = '${crossMethod}'
        `
    }
    crossCountQuery = `
        SELECT
            COUNT(1)
        FROM
            germplasm.cross
        WHERE  
            id IN (SELECT cross_id
                FROM germplasm.seed
                WHERE germplasm_id = ${germplasmDbId})
            ${condition}
        LIMIT  1 
    `
    // Retrieve cross count from the database
    let crossCount = await sequelize.query(crossCountQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    if(crossCount == undefined) crossCount = null

    return crossCount
}

/**
 * Retrieves the last BCID from germplasm designation given BCID pattern
 * @param {*} BCIDPattern BCID pattern for regex
 */
async function getPreviousGermplasmBCID(BCIDPattern) {
    let germplasmQuery = `
        SELECT
            germplasm.designation AS "BCID"
        FROM
            germplasm.germplasm germplasm
        WHERE
            germplasm.designation ILIKE '${BCIDPattern}'
            AND germplasm.is_void = FALSE
        ORDER BY id DESC LIMIT 1
    `
    // Retrieve germplasm attribute from the database
    let germplasm = await sequelize.query(germplasmQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    return germplasm[0]
}

/**
 * Retrieves the germplasm name of the given name type
 * for the given germplasm id.
 * @param {integer} germplasmDbId germplasm identifier
 * @param {string} nameType germplasm name type to retrieve
 * @returns {object} germplasm name info
 */
async function getGermplasmNameByType(germplasmDbId, nameType) {
    let germplasmNameQuery = `
        SELECT
            germplasm.id AS "germplasmDbId",
            germplasm_name.id AS "germplasmNameDbId",
            germplasm_name.name_value AS "nameValue",
            germplasm_name.germplasm_name_type AS "germplasmNameType",
            germplasm_name.germplasm_name_status AS "germplasmNameStatus",
            germplasm_name.germplasm_normalized_name AS "germplasmNormalizedName",
            germplasm.germplasm_normalized_name AS "germplasmNormalizedName"
        FROM
            germplasm.germplasm_name germplasm_name
        LEFT JOIN
            germplasm.germplasm germplasm ON germplasm.id = germplasm_name.germplasm_id
        WHERE
            germplasm.id = '${germplasmDbId}'
            AND germplasm.is_void = FALSE
            AND germplasm_name.germplasm_name_type = '${nameType}'
            AND germplasm_name.germplasm_name_status IN ('active', 'standard')
    `
    // Retrieve germplasm name from the database
    let germplasmName = await sequelize.query(germplasmNameQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    return germplasmName[0]
}

/**
 * Retrieves the germplasm name information of the given germplasm id
 * @param {*} germplasmDbId germplasm identifier
 */
async function getGermplasmNameInfo(germplasmDbId) {
    let germplasmNameQuery = `
        SELECT
            germplasm.id AS "germplasmDbId",
            germplasm_name.id AS "germplasmNameDbId",
            germplasm_name.name_value AS "nameValue",
            germplasm_name.germplasm_name_type AS "germplasmNameType",
            germplasm_name.germplasm_name_status AS "germplasmNameStatus",
            germplasm_name.germplasm_normalized_name AS "germplasmNormalizedName",
            germplasm.germplasm_normalized_name AS "germplasmNormalizedName"
        FROM
            germplasm.germplasm_name germplasm_name
        LEFT JOIN
            germplasm.germplasm germplasm ON germplasm.id = germplasm_name.germplasm_id
        WHERE
            germplasm.id = '${germplasmDbId}'
            AND germplasm.is_void = FALSE
    `
    // Retrieve germplasm name from the database
    let germplasmName = await sequelize.query(germplasmNameQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    return germplasmName[0]
}

/**
 * Adds modifier-related fields to the update values
 * @param {*} values array containing columns to update and values
 * @param {*} userId user identifier
 */
async function addModifierFields(values, userId) {
    values.push(
        "modification_timestamp = NOW()",
        `modifier_id = ${userId}`
    )

    return values
}

/**
 * Generic function for basic retrieval of database records
 * @param {string} schema where record belongs to
 * @param {string} table where record belongs to
 * @param {array} fields fields to include in the retrieval
 * @param {array} conditions conditions to add to the WHERE clause
 * @param {string} options additional options such as ORDER BY and LIMIT
 * @returns 
 */
async function retrieveRecordsBasic(schema, table, fields = [], conditions = [], options = '') {
    // build fields
    let included = '*'
    if (fields !== undefined && fields.length > 0) included = fields.join(", ")
    // build where clause
    let whereClause = '';
    if (conditions !== undefined && conditions.length > 0) {
        conditionsArray = conditions.join(" AND ")
        whereClause = `
            WHERE
                ${conditionsArray}
        `
    }

    try {
        // Build select query
        let selectQuery = `
            SELECT
                ${included}
            FROM
                ${schema}.${table}
            ${whereClause}
            ${options}
        `

        // retrieve records
        let records = await sequelize.query(selectQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return records
    } catch (err) {
        return null
    }
}

/**
 * Retrieves one record from the database given the schema and table.
 * 
 * @param {string} schema - schema in the database
 * @param {string} table - table in the schema
 * @param {string} whereClause - (optional) select filters
 * @returns 
 */
async function getOneRecord(schema, table, whereClause = '') {
    let selectQuery = `
        SELECT
            *
        FROM
            ${schema}.${table}
        ${whereClause}
        LIMIT 1
    `

    // retrieve records
    let records = await sequelize.query(selectQuery, {
        type: sequelize.QueryTypes.SELECT
    })

    return records[0]
}

/**
 * Insert new record into the database
 * @param {*} schema where record belongs to
 * @param {*} table where record is to be inserted
 * @param {*} columnValueArray insert values
 * @param {*} userId user identifier
 */
async function insertRecord(schema, table, columnValueArray, userId) {
    columnValueArray['creator_id'] = userId
    let columns = Object.keys(columnValueArray).join(", ")
    let values = [Object.values(columnValueArray)]
    let transaction
    try {
        transaction = await sequelize.transaction({ autocommit: false })

        // Create record
        let insertQuery = format(`
            INSERT INTO ${schema}.${table}
                (${columns})
            VALUES 
                %L
            RETURNING id`, values
        )

        let result = await sequelize.query(insertQuery, {
            type: sequelize.QueryTypes.INSERT,
            transaction: transaction
        })

        await transaction.commit()
        return result[0]
    } catch (err) {
        // Rollback transaction (if still open)
        if (transaction.finished !== 'commit') transaction.rollback()
        return null
    }
}

/**
 * Generic function for updating records
 * @param {*} dbId record identifier
 * @param {*} schema where record belongs to
 * @param {*} table where record belongs to
 * @param {*} values array containint columns to update and values
 * @param {*} userId user identifier
 */
async function updateRecordById(dbId, schema, table, values, userId)  {
    values = await addModifierFields(values, userId)
    let valuesString = values.join(", ")
    let transaction

    try {
        transaction = await sequelize.transaction({ autocommit: false })
        
        // Update the germplasm record
        let updateRecordQuery = `
            UPDATE
                ${schema}.${table}
            SET
                ${valuesString}
            WHERE
                id = ${dbId}
        `

        let result = await sequelize.query(updateRecordQuery, {
            type: sequelize.QueryTypes.UPDATE
        })
        await transaction.commit()

        return true

    } catch (err) {
        // Rollback transaction (if still open)
        if (transaction.finished !== 'commit') transaction.rollback()
        return false
    }
}

/**
 * Returns the value of the scale value
 * given its abbrev
 * @param {string} abbrev string abbreviation of the scale value
 * @return {string} value the string value of the scale value
 */
async function getScaleValueByAbbrev(abbrev) {
    try {

        let scaleValueQuery = `
            SELECT 
                scale_value.value
            FROM 
                master.scale_value
            WHERE  
                scale_value.abbrev = '${abbrev}'
                AND scale_value.is_void = FALSE
        `
        // Retrieve plot from the database
        let scaleValue = await sequelize.query(scaleValueQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return scaleValue[0].value

    } catch (err) {
        return null
    }
}

/**
 * Returns information of the scale value
 * given its abbrev
 * @param {string} abbrev string abbreviation of the scale value
 * @return {string} value the string value of the scale value
 */
 async function getScaleValueInfoByAbbrev(abbrev) {
    try {

        let scaleValueQuery = `
            SELECT 
                scale_value.id AS "scaleValueDbId",
                scale_value.abbrev,
                scale_value.value,
                scale_value.display_name AS "displayName",
                scale_value.description
            FROM 
                master.scale_value
            WHERE  
                scale_value.abbrev = '${abbrev}'
                AND scale_value.is_void = FALSE
        `

        // Retrieve plot from the database
        let scaleValue = await sequelize.query(scaleValueQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return scaleValue[0]

    } catch (err) {
        return null
    }
}

/**
 * Retrieve variable info via abbrev
 * @param {string} abbrev - abbbreviation of variable
 * @return {objet} variable - variable information
 */
async function getVariableByAbbrev(abbrev) {
    try {

        let variableQuery = `
            SELECT 
                variable.id AS "variableDbId",
                variable.abbrev AS "variableAbbrev",
                variable.label AS "variableLabel",
                variable.name AS "variableName",
                variable.type AS "variableType",
                variable.status AS "variableStatus",
                variable.display_name AS "variableDisplayName"
            FROM 
                master.variable
            WHERE  
                variable.abbrev = '${abbrev}'
                AND variable.is_void = FALSE
        `
        // Retrieve plot from the database
        let variable = await sequelize.query(variableQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return variable[0]

    } catch (err) {
        return null
    }
}

/**
 * Retrieve the germplasm attribute of the given germplasm id
 * @param {*} germplasmDbId - germplasm identifier
 * @param {*} variableDbId - variable identifier
 * @return {*} germplasmAttribute - germplasm attribute info
 */
async function getGermplasmAttribute(germplasmDbId, variableDbId) {
    try {

        let germplasmAttributeQuery = `
            SELECT 
                germplasm_attribute.id AS "germplasmAttributeDbId",
                germplasm_attribute.germplasm_id AS "germplasmDbId",
                germplasm_attribute.variable_id AS "variableDbId",
                germplasm_attribute.data_value AS "dataValue",
                germplasm_attribute.data_qc_code AS "dataQcCode"
            FROM 
                germplasm.germplasm_attribute
            WHERE  
                germplasm_attribute.germplasm_id = ${germplasmDbId}
                AND germplasm_attribute.variable_id = '${variableDbId}'
                AND germplasm_attribute.is_void = FALSE
        `
        // Retrieve plot from the database
        let germplasmAttribute = await sequelize.query(germplasmAttributeQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        return germplasmAttribute[0]

    } catch (err) {
        return null
    }
}

/**
 * Retrieves the occurrence's harvested package prefix
 * from the occurrence_data table
 * @param {integer} occurrenceDbId - occurrence identifier 
 * @returns the designated package prefix for the occurrence
 */
async function getHarvestedPackagePrefix(occurrenceDbId){
    try {
            // retrieve the variable id of HARVESTED_PACKAGE_PREFIX
            let variable = await getVariableByAbbrev('HARVESTED_PACKAGE_PREFIX')   
            let variableDbId = variable.variableDbId

            // build query
            let occurrenceDataQuery = `
                SELECT 
                    od.data_value AS "harvestedPackagePrefix"
                FROM 
                    experiment.occurrence_data od
                WHERE  
                    od.variable_id = ${variableDbId}
                    AND od.occurrence_id = ${occurrenceDbId}
                    AND od.is_void = false
            `
            // Retrieve harvested package prefix from the database
            let occurrenceData = await sequelize.query(occurrenceDataQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            return occurrenceData[0].harvestedPackagePrefix
        } catch (error) {
            return null
        }
}

/**
 * Facilitate generation of germplasm designation
 * 
 * @param {Array} entitiesArray - entities information
 * @param {Array} optional - optional parameters
 * @param {String} subEntity - subentity
 * 
 * @returns 
 */
async function buildGermplasmDesignation(entitiesArray, optional = {}, subEntity = 'germplasm') {

    // build designation with empty counter
    let optionalEmptyCounter = optional
    optionalEmptyCounter['counter'] = ''
    let counterRegex = '[1-9][0-9]*'

    // get designation counter delimiter from config
    let configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
    if (subEntity === 'germplasm_name') configBaseString = 'HM_NAME_PATTERN_GERMPLASM_BC_DERIV'
    let nameConfig = await getNomenclatureConfig(configBaseString, entitiesArray, optional)

    // reverse the array
    let reverseConfig = nameConfig.reverse()
    let revCLen = reverseConfig.length
    let special = optional.special !== undefined ? optional.special : []
    let delimiter = ''
    let prefix = ''
    let suffix = ''
    // Get prefix, suffix, and counter values from the config
    for (let i = 0; i < revCLen; i++) {
        let item = reverseConfig[i]
        // if counter item found, check next value
        if (item.type == 'counter') {
            // if the next item is a free-text, set next item as prefix
            // and set the item after that as the prefix
            if(reverseConfig[i+1].type == 'free-text') {
                prefix = reverseConfig[i+1].value
                delimiter = reverseConfig[i+2].value
            }
            // if the next item is a delimiter, set it as the delimiter
            else if(reverseConfig[i+1].type == 'delimiter') {
                delimiter = reverseConfig[i+1].value
            }
            // If counter has leading zeros, modify regex
            if (item.leading_zero === 'yes') counterRegex = '[0-9]*'
            break
        }
        // if free-text item found, check if special
        else if (item.type == 'free-text') {
            // if item is special, and special type is included in the special array,
            // OR if the item is NOT special, then add it to the suffix
            if(
                (item.special !== undefined && special.includes(item.special))
                || item.special === undefined
            ) {
                suffix = item.value + suffix
            }
        }
        // if field item found, get field value from the entites array
        // and add it to the suffix
        else if (item.type == 'field') {
            let entity = item.entity
            let fieldName = item.field_name
            suffix = entitiesArray[entity][fieldName] + suffix
        }
    }

    // get designation pattern
    let germplasmDesignationPattern
    if (subEntity === 'germplasm') {
        configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
        germplasmDesignationPattern = await genericNameBuilder(configBaseString, entitiesArray, optionalEmptyCounter)    
    }
    else if (subEntity === 'germplasm_name') {
        configBaseString = 'HM_NAME_PATTERN_GERMPLASM_BC_DERIV'
        germplasmDesignationPattern = await genericNameBuilder(configBaseString,entitiesArray, optionalEmptyCounter)    
    }
    // remove prefix and suffix
    let pcs = germplasmDesignationPattern.split(delimiter)      // split pattern by the delimiter
    let lastPc = pcs[pcs.length-1]                              // get the last part of the pattern
    lastPc = lastPc.replace(prefix,'')                          // remove instances of prefix
    lastPc = lastPc.replace(suffix,'')                          // remove instances of suffix
    pcs[pcs.length-1] = lastPc                                  // insert clean last piece back to pcs array
    germplasmDesignationPattern = pcs.join(delimiter)           // join pcs with the delimiter

    // get last in sequence
    let fieldName = 'designation'
    if (subEntity === 'germplasm_name') fieldName = 'name_value'
    let lastGermplasmDesignation = await getLastInNumberedSequence(
        'germplasm',
        subEntity,
        fieldName,
        germplasmDesignationPattern,
        counterRegex,
        {
            prefix: prefix,
            suffix: suffix
        },
        true
    )

    // get the next counter based on the last designation
    let germplasmCounter = await getCounterValue(
        lastGermplasmDesignation,
        delimiter,
        {
            prefix: prefix,
            suffix: suffix
        },
        germplasmDesignationPattern
    )
    optional.counter = germplasmCounter

    // generate the next germplasm designation and generation
    if (subEntity === 'germplasm') {
        configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
        return await genericNameBuilder(configBaseString, entitiesArray, optional)
    }
    else if (subEntity === 'germplasm_name') {
        configBaseString = 'HM_NAME_PATTERN_GERMPLASM_BC_DERIV'
        return await  genericNameBuilder(configBaseString, entitiesArray, optional)
    }
}

/**
 * Populates the relation table of the given schema for a given entity
 * @param {*} schema schema where entity belongs
 * @param {*} entity entity relation
 * @param {*} parentDbId identifier of parent record
 * @param {*} childDbId identifier of child record
 * @param {*} userId user identifier
 */
async function populateRelationTable(schema, entity, parentDbId, childDbId, userId) {
    let supportedSchemas = ['germplasm']
    let supportedEntities = ['germplasm', 'seed']

    // if not supported, return null
    if(!supportedSchemas.includes(schema) || !supportedEntities.includes(entity)) return null

    let relationInfo = {}
    relationInfo[`parent_${entity}_id`] = parentDbId
    relationInfo[`child_${entity}_id`] = childDbId
    relationInfo['order_number'] = 1

    result = await insertRecord(schema, `${entity}_relation`, relationInfo, userId)
    if(!result) return null
}

/**
 * Specialized package label builder that utilizes
 * the HARVESTED_PACKAGE_PREFIX variable value
 * stored in the occurrence_data table for the current occurrence.
 * Currently used by Maize harvest use cases
 * @param {object} dbRecord - cross or plot information
 * @param {object} entitiesArray - entities information
 * @param {string} harvestSource - plot or cross
 * @param {integer} userId - user identifier
 * @returns {object} - contains the new package label and the updated entities array
 */
async function buildPackageLabel(dbRecord, entitiesArray, harvestSource, userId) {
    let harvestedPackagePrefix = await getHarvestedPackagePrefix(dbRecord.occurrenceDbId)

    // if prefix is null, generate new one
    if(harvestedPackagePrefix == null) {
        harvestedPackagePrefix = await createHarvestedPackagePrefix(dbRecord, '-', userId)
    }

    entitiesArray[harvestSource].harvestedPackagePrefix = harvestedPackagePrefix

    // get last package label for occurrence
    let lastPackageLabel = await getLastInNumberedSequence(
        'germplasm',
        'package',
        'package_label',
        harvestedPackagePrefix,
        '[0-9]*',
        { delimiter : '-' }
    )
    // get counter for next package label
    let packageCounter = await getCounterValue(lastPackageLabel,'-')
    
    // create new package label
    configBaseString = 'HM_LABEL_PATTERN_PACKAGE'
    let newPackageLabel = await genericNameBuilder(configBaseString, entitiesArray, {counter:packageCounter})

    return {
        newPackageLabel : newPackageLabel,
        entitiesArray : entitiesArray
    }
}

/**
 * Builds seed name
 * Currently supports numbered harvests for maize and wheat
 * @param {object} entitiesArray - entities related to the harvest
 * @param {object} optional - optional parameters
 * @returns {string} seedName - new seed name
 */
async function buildSeedName(entitiesArray, optional = {}) {
    // build seed name with empty counter
    let optionalEmptyCounter = optional
    optionalEmptyCounter['counter'] = ''

    // get seed name pattern
    let configBaseString = 'HM_NAME_PATTERN_SEED'
    let seedNamePattern = genericNameBuilder(configBaseString, entitiesArray, optionalEmptyCounter)

    // get last seed name using pattern
    let lastSeedName = await getLastInNumberedSequence(
        'germplasm',
        'seed',
        'seed_name',
        seedNamePattern,
        '[0-9]*'
    )

    // get seed counter delimiter from config
    let nameConfig = await getNomenclatureConfig(configBaseString, entitiesArray, optional)

    // reverse the array
    let reverseConfig = nameConfig.reverse()
    let revCLen = reverseConfig.length
    let delimiter = ''
    for (let i = 0; i < revCLen; i++) {
        let item = reverseConfig[i]
        // if counter item found, get value of next item
        if (item.type == 'counter') {
            delimiter = reverseConfig[i+1].value
            break
        }
    }

    // get the next counter based on the last seed name
    let seedCounter = await getCounterValue(lastSeedName,delimiter)
    optional['counter'] = seedCounter
    
    // generate the new seed name
    return await genericNameBuilder(configBaseString, entitiesArray, optionalEmptyCounter)
}

/**
 * Inserts a new HARVESTED_PACKAGE_PREFIX value
 * in the occurrence_data table for the current occurrence.
 * @param {object} dbRecord - record information (plot/cross)
 * @param {string} delimiter - delimiter used for the package prefix
 * @param {integer} userId - user indentifier
 * @returns 
 */
async function createHarvestedPackagePrefix(dbRecord, delimiter, userId) {
    let starting = ''
        let final = ''

        // get config for harvested package prefix
        let configAbbrev = 'HARVESTED_PACKAGE_PREFIX_PATTERN_' + dbRecord.cropCode + '_DEFAULT'
        let config = await getHarvestConfig(configAbbrev)
        let pattern = config.configValue.pattern
        let patternLength = pattern.length

        // build starting string
        for (let i = 0; i < patternLength; i++) {
            let value = ''
            let patternItem = pattern[i]
            let patternType = patternItem.type

            if (patternType == 'field') {
                value = dbRecord[patternItem.field_name]
            } else if (patternType == 'delimiter' || patternType == 'free-text') {
                value = patternItem.value
            }

            starting = starting.concat(value)
        }
        
        // retrieve last in numbered sequence
        let last = await getLastInNumberedSequence(
            'experiment',
            'occurrence_data',
            'data_value',
            starting,
            '[0-9]*'
        )
        
        // get next counter value
        let counter = String(await getCounterValue(last, delimiter)).padStart(3, '0')
        
        // build next in numbered sequence
        final = starting.concat(delimiter,counter)
        
        // get variable id of HARVESTED_PACKAGE_PREFIX
        let variable = await getVariableByAbbrev('HARVESTED_PACKAGE_PREFIX')   
        let variableDbId = variable.variableDbId

        // insert into occurrence_data table
        let occurrenceDataInfo = {}
        occurrenceDataInfo['occurrence_id'] = dbRecord.occurrenceDbId
        occurrenceDataInfo['variable_id'] = variableDbId
        occurrenceDataInfo['data_value'] = final
        occurrenceDataInfo['data_qc_code'] = 'G'
        occurrenceDataInfo['modifier_id'] = userId
        await insertRecord('experiment', 'occurrence_data', occurrenceDataInfo, userId)
        
        // return next in numbered sequence
        return final
}

/**
 * Check if has exsting seed record
 * @param {Integer} seedDbId ID of seed record
 * @returns 
 */
async function hasSeedRecord(req, res, seedDbId) {
    // Check if seed exists
    let seedQuery = `
        SELECT EXISTS(
            SELECT 1 
            FROM 
                germplasm.seed s
            WHERE 
                s.id = ${seedDbId}
                AND s.is_void= FALSE
        )
    `

    // Retrieve seed record
    let seed = await sequelize.query(seedQuery, {
        type: sequelize.QueryTypes.SELECT
    }).catch(async err => {
        return undefined
    })

    // If seed does not exist, return error
    if (await seed === undefined || await !seed[0]["exists"]) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404044)
        res.send(new errors.NotFoundError(errMsg))
        return
    }

    return seed[0]["exists"];
}

/**
 * Retrieved seed record using provided records
 * @param {String} additionalConditions Additiona conditions to search for records
 * @returns {Array} array of seed records
 */
async function getSeedRecord(req, res, additionalConditions) {
    // Check if seed exists
    let seedQuery = `
        SELECT
            seed.*
        FROM
            germplasm.seed seed
        WHERE
            seed.is_void = FALSE
            ${additionalConditions}
    `

    // Retrieve seed record
    let seed = await sequelize.query(seedQuery, {
        type: sequelize.QueryTypes.SELECT
    }).catch(async err => {
        return undefined
    })

    // If seed does not exist, return error
    if (await seed === undefined) {
        let errMsg = await errorBuilder.getError(req.headers.host, 404044)
        res.send(new errors.NotFoundError(errMsg))
        return
    }

    return seed;
}

module.exports = {

    /**
     * Gets the next value in the database sequence
     * @param {*} schema schema where the sequence originates from
     * @param {*} sequenceName name of the sequence
     */
    getNextInSequence: async(schema, sequenceName) => {
        return await getNextInSequence(schema, sequenceName)
    },
    
    /**
     * Get HM configs that follow the abbrev format:
     * <identifier>_<crop_code>_<program_code>
     * eg. HM_TAXONOMY_PREFERENCE_RICE_IRSEA
     * 
     * If no config exists for the program, the default is retrieved.
     * eg. HM_TAXONOMY_PREFERENCE_RICE_DEFAULT
     * @param {string} configBaseString config base string pattern
     * @param {string} cropCode crop code for config
     * @param {string} programCode program code for config
     * @returns {object} config object
     */
    getConfigGeneric: async(configBaseString, cropCode, programCode) => {
        return await getConfigGeneric(configBaseString, cropCode, programCode)
    },

    getGermplasmNameInfo: async(germplasmDbId) => {
        return await getGermplasmNameInfo(germplasmDbId)
    },

    /**
     * Gets the configuration value
     * @param {*} config
     * @param {*} key
     */
    getConfigValue: async(config, key) => {
        return await getConfigValue(config, key)
    },

    addModifierFields: async (values, userId) => {
        return await  addModifierFields(values, userId)
    },

    getVariableByAbbrev: async(abbrev) => {
        return await getVariableByAbbrev(abbrev)
    },
    
    /**
     * Builds the regex for the left and right sides of the parentage
     * @param {string} recurrentStr - recurrent parent string
     * @param {object} config - the pattern for the left and right sides of the parentage
     * @returns {obect} - leftRegex and rightRegex
     */
    generateRecurrentRegex: async(recurrentStr, config) => {
        return await generateRecurrentRegex(recurrentStr, config)
    },

    getGermplasmAttribute: async(germplasmDbId, variableDbId) => {
        return await getGermplasmAttribute(germplasmDbId, variableDbId)
    },
    
    /**
     * Builds cross name
     * @param {*} entitiesArray array containing entities (eg. germplasm, seed, etc)
     * @param {*} optional - optional parameters like counter, additionalConditions, etc
     */
    crossNameBuilder: async (entitiesArray, optional = {}) => {
        configBaseString = 'HM_NAME_PATTERN_CROSS'
        return await genericNameBuilder(configBaseString, entitiesArray, optional)
    },

    /**
     * Builds germplasm name
     * @param {*} entitiesArray array containing entities (eg. germplasm, seed, etc)
      * @param {*} optional - optional parameters like counter, additionalConditions, etc
     */
    germplasmNameBuilder: async (entitiesArray, optional = {}) => {
        configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
        return await genericNameBuilder(configBaseString, entitiesArray, optional)
    },

    /**
     * Builds family name
     * @param {object} entitiesArray array containing entities (eg. germplasm, seed, etc)
      * @param {object} optional - optional parameters like counter, additionalConditions, etc
     */
    familyNameBuilder: async (entitiesArray, optional = {}) => {
        configBaseString = 'HM_NAME_PATTERN_FAMILY'
        return await genericNameBuilder(configBaseString, entitiesArray, optional)
    },

    /**
     * Retrieves the germplasm name of the given name type
     * for the given germplasm id.
     * @param {integer} germplasmDbId germplasm identifier
     * @param {string} nameType germplasm name type to retrieve
     * @returns {object} germplasm name info
     */
        getGermplasmNameByType: async(germplasmDbId, nameType) => {
            return await getGermplasmNameByType(germplasmDbId, nameType)
        },

    /**
     * Adds the specified germplasm as a member of the given family,
     * applying the correct order_number based on the last number
     * (defaults to 0 if the germplasm is the first family member).
     * @param {integer} familyDbId - family identifier
     * @param {integer} germplasmDbId - germplasm identifier
     * @param {integer} userId - user identifier
     * @param {integer} orderNumber - (optional) starting order number
     */
    addFamilyMember: async (familyDbId, germplasmDbId, userId, orderNumber = 0) => {
        // Check last available order number
        let familyMembersQuery = `
            SELECT
                family_id,
                order_number
            FROM
                germplasm.family_member
            WHERE
                family_id = ${familyDbId}
            ORDER BY
                order_number DESC
            LIMIT 1
        `
        // Retrieve family members from the database
        let familyMembers = await sequelize.query(familyMembersQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        // If the family has members, get the last oder number and increment by 1
        if (familyMembers[0] !== undefined && familyMembers[0] !== null) {
            let lastOrderNumber = familyMembers[0]['order_number']
            orderNumber = parseInt(lastOrderNumber) + 1
        }
        // Insert new family member
        let familyMemberInfo = {}
        familyMemberInfo['family_id'] = familyDbId
        familyMemberInfo['germplasm_id'] = germplasmDbId
        familyMemberInfo['order_number'] = orderNumber
        return await insertRecord('germplasm', 'family_member', familyMemberInfo, userId)
    },

    /**
     * Builds germplasm - backcross derivative name
     * @param {*} entitiesArray array containing entities (eg. germplasm, seed, etc)
      * @param {*} optional - optional parameters like counter, additionalConditions, etc
     */
    backcrossDerivativeNameBuilder: async (entitiesArray, optional = {}) => {
        configBaseString = 'HM_NAME_PATTERN_GERMPLASM_BC_DERIV'
        return await genericNameBuilder(configBaseString, entitiesArray, optional)
    },

    /**
     * Builds seed name
     * @param {*} entitiesArray array containing entities (eg. germplasm, seed, etc)
     * @param {*} optional - optional parameters like counter, additionalConditions, etc
     */
    seedNameBuilder: async (entitiesArray, optional = {}) => {
        configBaseString = 'HM_NAME_PATTERN_SEED'
        return await genericNameBuilder(configBaseString, entitiesArray, optional)
    },

    /**
     * Retrieves one record from the database given the schema and table.
     * 
     * @param {string} schema - schema in the database
     * @param {string} table - table in the schema
     * @param {string} whereClause - (optional) select filters
     * @returns 
     */
    getOneRecord: async (schema, table, whereClause = '') => {
        return await getOneRecord(schema, table, whereClause)
    },

    /**
     * Builds package label
     * @param {*} entitiesArray array containing entities (eg. germplasm, seed, etc)
     * @param {*} optional - optional parameters like counter, additionalConditions, etc
     */
    packageLabelBuilder: async (entitiesArray, optional = {}) => {
        configBaseString = 'HM_LABEL_PATTERN_PACKAGE'
        return await genericNameBuilder(configBaseString, entitiesArray, optional)
    },

    getPlotInformation: async (plotDbId, crossDbId = null) => {
        // If crossDbId is provided, add correct LEFT JOIN for cross parent
        let crossParentLeftJoin = `
            LEFT JOIN germplasm.cross_parent ON cross_parent.entry_id = entry.id 
                AND cross_parent.parent_role = 'female-and-male'
                AND cross_parent.is_void = FALSE
        `
        if (crossDbId !== null) {
            crossParentLeftJoin = `
                LEFT JOIN germplasm.cross_parent ON cross_parent.cross_id = ${crossDbId} 
                    AND cross_parent.parent_role = 'female-and-male'
                    AND cross_parent.is_void = FALSE
            `
        }

        try{
            let plotQuery = `
                SELECT  
                    plot.id "plotDbId",
                    plot.location_id "locationDbId",
                    experiment.id AS "experimentDbId",
                    experiment.experiment_name "experimentName",
                    experiment.experiment_year "experimentYear",
                    experiment.experiment_type "experimentType",
                    crop.id AS "cropDbId",
                    crop.crop_name "cropName",
                    crop.crop_code AS "cropCode",
                    season.season_name "experimentSeason",
                    stage.stage_code AS "stage",
                    entry.id AS "entryDbId",
                    entry.entry_number "entryNumber",
                    pi_entry.entry_code "entryCode",
                    plot.plot_number "plotNumber",
                    plot.plot_code "plotCode",
                    plot.location_id AS "locationDbId",
                    occurrence.id AS "occurrenceDbId",
                    occurrence.occurrence_code AS "occurrenceCode",
                    occurrence.occurrence_name AS "occurrenceName",
                    CASE
                        WHEN goa.data_value IS NULL THEN geospatial_object.geospatial_object_code
                        ELSE goa.data_value
                    END AS "nurserySiteCode",
                    season.season_code AS "experimentSeasonCode",
                    SUBSTRING(CAST(experiment.experiment_year AS text),3,2) AS "experimentYearYY",
                    experiment.experiment_year AS "experimentYear",
                    plot_data.data_value "harvestMethod",
                    germplasm.id AS "germplasmDbId",
                    germplasm.germplasm_state "germplasmState",
                    germplasm.germplasm_type "germplasmType",
                    germplasm.generation AS "germplasmGeneration",
                    germplasm.designation AS "germplasmDesignation",
                    germplasm.parentage AS "germplasmParentage",
                    germplasm.taxonomy_id AS "germplasmTaxonomyId",
                    germplasm.germplasm_name_type AS "germplasmNameType",
                    family.id AS "familyDbId",
                    family.family_code AS "familyCode",
                    family.family_name AS "familyName",
                    seed.id AS "seedDbId",
                    program.id AS "programDbId",
                    program.program_code AS "programCode",
                    germplasm.cross.id AS "crossDbId",
                    germplasm.cross.cross_method as "crossMethod",
                    CASE
                        WHEN goa.data_value IS NOT NULL THEN goa.data_value
                        ELSE geospatial_object.geospatial_object_code
                    END AS "originSiteCode",
                    CASE
                        WHEN fgoa.data_value IS NOT NULL THEN fgoa.data_value
                        WHEN goa.data_value IS NOT NULL THEN goa.data_value
                        ELSE geospatial_object.geospatial_object_code
                    END AS "fieldOriginSiteCode",
                    (
                        SELECT
                            COUNT(1)
                        FROM
                            germplasm.cross_parent
                        WHERE
                            cross_parent.cross_id = germplasm.cross.id
                    ) AS "crossParentCount",
                    CASE
                        WHEN pd_gc.data_value IS NOT NULL THEN pd_gc.data_value
                        ELSE cd_gc.data_value
                    END AS "grainColor",
                    gn1.name_value AS "backcrossDerivativeName"
                FROM 
                    experiment.plot 
                        LEFT JOIN experiment.entry ON entry.id = plot.entry_id AND entry.is_void = FALSE
                        LEFT JOIN experiment.planting_instruction pi_entry ON pi_entry.plot_id = plot.id AND pi_entry.is_void = FALSE
                        ${crossParentLeftJoin}
                        LEFT JOIN germplasm.cross ON germplasm.cross.id = cross_parent.cross_id 
                            AND germplasm.cross.cross_method = 'selfing'
                            AND germplasm.cross.is_void = FALSE
                        LEFT JOIN experiment.occurrence ON occurrence.id = plot.occurrence_id
                        LEFT JOIN experiment.experiment ON occurrence.experiment_id = experiment.id
                        LEFT JOIN tenant.season ON season.id = experiment.season_id
                        LEFT JOIN tenant.stage ON stage.id = experiment.stage_id
                        LEFT JOIN place.geospatial_object on geospatial_object.id=occurrence.site_id
                        LEFT JOIN experiment.planting_instruction ON planting_instruction.plot_id = plot.id AND planting_instruction.is_void = FALSE
                        LEFT JOIN germplasm.germplasm ON germplasm.id = planting_instruction.germplasm_id
                        LEFT JOIN germplasm.germplasm_name gn1 ON germplasm.id = gn1.germplasm_id
                            AND gn1.germplasm_name_type = 'backcross_derivative'
                            AND gn1.is_void = FALSE
                        LEFT JOIN tenant.crop ON crop.id = germplasm.crop_id
                        LEFT JOIN germplasm.seed ON seed.id= planting_instruction.seed_id
                        LEFT JOIN germplasm.family_member fm ON fm.germplasm_id = planting_instruction.germplasm_id
                        LEFT JOIN germplasm.family ON family.id = fm.family_id
                        LEFT JOIN tenant.program on program.id = experiment.program_id
                        LEFT JOIN place.geospatial_object_attribute goa ON goa.geospatial_object_id = occurrence.site_id
                            AND goa.variable_id = (
                                SELECT 
                                    id
                                FROM 
                                    master.variable
                                WHERE
                                    abbrev = 'ORIGIN_SITE_CODE'
                                    AND is_void = FALSE
                            ) AND goa.is_void = FALSE
                        LEFT JOIN place.geospatial_object_attribute fgoa ON fgoa.geospatial_object_id = occurrence.field_id
                            AND fgoa.variable_id = (
                                SELECT 
                                    id
                                FROM 
                                    master.variable
                                WHERE
                                    abbrev = 'ORIGIN_SITE_CODE'
                                    AND is_void = FALSE
                            ) AND fgoa.is_void = FALSE
                        LEFT JOIN (
                            SELECT
                                plot_data.id,
                                plot_data.plot_id,
                                plot_data.data_value
                            FROM
                                experiment.plot_data
                            LEFT JOIN
                                master.variable ON variable.id = plot_data.variable_id
                            WHERE
                                plot_data.plot_id = (:plotDbId)
                                AND variable.abbrev = 'HV_METH_DISC'
                            ORDER BY
                                plot_data.id DESC
                            LIMIT 1
                        ) plot_data ON plot_data.plot_id = plot.id
                        LEFT JOIN experiment.plot_data pd_gc ON pd_gc.plot_id = plot.id
                            AND pd_gc.is_void = FALSE
                            AND pd_gc.variable_id = (
                                SELECT
                                    id
                                FROM
                                    master.variable
                                WHERE
                                    abbrev = 'GRAIN_COLOR'
                                    AND is_void = FALSE
                            )
                        LEFT JOIN germplasm.cross_data cd_gc ON cd_gc.cross_id = germplasm.cross.id
                            AND cd_gc.is_void = FALSE
                            AND cd_gc.variable_id = (
                                SELECT
                                    id
                                FROM
                                    master.variable
                                WHERE
                                    abbrev = 'GRAIN_COLOR'
                                    AND is_void = FALSE
                            )
                WHERE 
                    plot.id = (:plotDbId)
                    AND plot.is_void = FALSE
            `
            // Retrieve plot from the database   
            let plots = await sequelize.query(plotQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    plotDbId: plotDbId
                }
            })

            return plots[0]
        }
        catch(err){
            return null
        }
    },

    /**
     * Retrieve cross information
     * @param {integer} crossDbId - cross record indentifier
     * @param {integer} occurrenceDbId - occurrence record indentifier
     */
    getCrossInformation: async (crossDbId, occurrenceDbId = null) => {
        return await getCrossInformation(crossDbId, occurrenceDbId);
    },

    /**
     * Retrieves the number of crosses belonging
     * to an occurrence given its ID
     * @param {integer} occurrenceDbId ocurrence identifier
     * @returns 
     */
    getOccurrenceCrossCount: async (occurrenceDbId) => {
        try {
            let crossCountQuery = `
                SELECT
                    COUNT(1)
                FROM
                    germplasm.cross "germplasmCross"
                WHERE
                    "germplasmCross".occurrence_id = ${occurrenceDbId}

            `
            // Retrieve count from the database   
            let result = await sequelize.query(crossCountQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            let countObject = result[0] !== undefined ? result[0] : []

            return countObject.count !== undefined && countObject.count !== null ?
                parseInt(countObject.count) : null
        }
        catch (e) {
            return null
        }
    },

    getCrossRecord: async (plotDbId) => {
        try{
            let plotQuery = `
        
            SELECT  
                plot.id "plotDbId",
                experiment.id "experimentDbId",
                crop.crop_name "cropName",
                "cross".id "crossDbId",
                lower("cross".cross_method) "crossMethod",
                cross_parent.parent_role "parent1Role",
                cross_parent.entry_id "parent1EntryDbId",
                germplasm.id "parent1GermplasmDbId",
                germplasm.germplasm_state "parent1GermplasmState",
                germplasm.germplasm_type "parent1GermplasmType",
                germplasm.generation "parent1Generation",
                germplasm.designation "parent1Designation",
                other_parent.parent_role "parent2Role",
                other_parent.entry_id "parent2EntryDbId",
                other_parent_germplasm.id "parent2GermplasmDbId",
                other_parent_germplasm.designation "parent2Designation",
                other_parent_germplasm.germplasm_state "parent2GermplasmState",
                other_parent_germplasm.germplasm_type "parent2GermplasmType",
                other_parent_germplasm.generation "parent2Generation"
            FROM 
                experiment.plot 
                    LEFT JOIN experiment.occurrence ON occurrence.id = plot.occurrence_id
                    LEFT JOIN experiment.experiment ON occurrence.experiment_id = experiment.id
                    LEFT JOIN germplasm.cross_parent ON plot.entry_id = cross_parent.entry_id
                        AND cross_parent.experiment_id = occurrence.experiment_id
                    LEFT JOIN germplasm.germplasm ON germplasm.id = cross_parent.germplasm_id
                    LEFT JOIN germplasm.cross ON "cross".id = cross_parent.cross_id
                    LEFT JOIN germplasm.cross_parent other_parent ON other_parent.cross_id = "cross".id 
                        AND other_parent.is_void = FALSE
                        AND other_parent.experiment_id = occurrence.experiment_id
                        AND other_parent.entry_id <> plot.entry_id
                    LEFT JOIN germplasm.germplasm other_parent_germplasm ON other_parent_germplasm.id = other_parent.germplasm_id
                    LEFT JOIN tenant.crop crop ON crop.id = germplasm.crop_id
            WHERE 
                plot.id = (:plotDbId)
                AND plot.is_void = FALSE       
            `

            // Retrieve plot from the database   
            let crossParents = await sequelize.query(plotQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    plotDbId: plotDbId
                }
            })
            return crossParents
        }
        catch(err){
            return null
        }
    },

    getSeedInformation: async (seedDbId) => {

        try{
            let seedQuery = `
                SELECT 
                    seed.id AS "seedDbId",
                    seed.seed_name AS "seedName"
                FROM 
                    germplasm.seed
                WHERE 
                    seed.id = (:seedDbId)
                    AND seed.is_void = FALSE
                `
            // Retrieve plot from the database
            let seeds = await sequelize.query(seedQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    seedDbId: seedDbId,
                },
            })

            return seeds[0]
        }
        catch(error){
            return null
        }
    },

    getPackageInformation: async (packageDbId) => {

        try{
            let packageQuery = `
                SELECT 
                    package.id AS "packageDbId",
                    package.package_label AS "packageLabel"
                FROM 
                    germplasm.package
                WHERE 
                    package.id = (:packageDbId)
                    AND package.is_void = FALSE
                `
            // Retrieve plot from the database
            let packages = await sequelize.query(packageQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    packageDbId: packageDbId,
                },
            })

            return packages[0]
        }
        catch(error){
            return null
        }
    },

    getPlotHarvestData: async (plotDbId) => {
        try {
            let plotDataQuery = `
            WITH 
                PLOT_RECORD AS(
                    SELECT 
                        plot.id AS plot_id
                    FROM 
                        experiment.plot
                    WHERE
                        plot.id = (:plotDbId)
                        AND plot.is_void = FALSE),
                HARVEST_DATE AS(
                    SELECT 
                        plot_data.data_value AS harvest_date ,
                        plot_data.plot_id AS id
                    FROM 
                        experiment.plot_data 
                            LEFT JOIN master.variable ON plot_data.variable_id= variable.id AND variable.is_void = FALSE
                    WHERE 
                        plot_data.plot_id = (:plotDbId)
                        AND variable.abbrev = 'HVDATE_CONT'
                        AND plot_data.is_void = FALSE
                    ORDER BY
                        plot_data.id DESC
                    LIMIT 1),
                HARVEST_METHOD AS(
                    SELECT 
                        plot_data.data_value AS harvest_method ,
                        plot_data.plot_id AS id
                    FROM 
                        experiment.plot_data 
                            LEFT JOIN master.variable ON plot_data.variable_id= variable.id AND variable.is_void = FALSE
                    WHERE 
                        plot_data.plot_id = (:plotDbId)
                        AND variable.abbrev = 'HV_METH_DISC'
                        AND plot_data.is_void = FALSE
                    ORDER BY
                        plot_data.id DESC
                    LIMIT 1),
                NO_OF_PLANTS AS(
                    SELECT 
                        plot_data.data_value AS no_of_plants ,
                        plot_data.plot_id AS id
                    FROM 
                        experiment.plot_data 
                            LEFT JOIN master.variable ON plot_data.variable_id= variable.id AND variable.is_void = FALSE
                    WHERE 
                        plot_data.plot_id = (:plotDbId)
                        AND variable.abbrev = 'NO_OF_PLANTS'
                        AND plot_data.is_void = FALSE
                    ORDER BY
                        plot_data.id DESC
                    LIMIT 1),
                NO_OF_EARS AS(
                    SELECT 
                        plot_data.data_value AS no_of_ears ,
                        plot_data.plot_id AS id
                    FROM 
                        experiment.plot_data 
                            LEFT JOIN master.variable ON plot_data.variable_id= variable.id AND variable.is_void = FALSE
                    WHERE 
                        plot_data.plot_id = (:plotDbId)
                        AND variable.abbrev = 'NO_OF_EARS'
                        AND plot_data.is_void = FALSE
                    ORDER BY
                        plot_data.id DESC
                    LIMIT 1),
                SPECIFIC_PLANT AS(
                    SELECT 
                        plot_data.data_value AS specific_plant ,
                        plot_data.plot_id AS id
                    FROM 
                        experiment.plot_data 
                            LEFT JOIN master.variable ON plot_data.variable_id= variable.id AND variable.is_void = FALSE
                    WHERE 
                        plot_data.plot_id = (:plotDbId)
                        AND variable.abbrev = 'SPECIFIC_PLANT'
                        AND plot_data.is_void = FALSE
                    ORDER BY
                        plot_data.id DESC
                    LIMIT 1),
                PANNO_SEL AS(
                    SELECT 
                        plot_data.data_value AS panno_sel ,
                        plot_data.plot_id AS id
                    FROM 
                        experiment.plot_data 
                            LEFT JOIN master.variable ON plot_data.variable_id= variable.id AND variable.is_void = FALSE
                    WHERE 
                        plot_data.plot_id = (:plotDbId)
                        AND variable.abbrev = 'PANNO_SEL'
                        AND plot_data.is_void = FALSE
                    ORDER BY
                        plot_data.id DESC
                    LIMIT 1)
            SELECT 
                plot_id,
                harvest_date,
                harvest_method,
                no_of_plants,
                no_of_ears,
                specific_plant,
                panno_sel
            FROM 
                PLOT_RECORD AS t1
            LEFT JOIN
                HARVEST_DATE AS t2 ON t2.id = t1.plot_id
            LEFT JOIN
                HARVEST_METHOD AS t3 ON t3.id = t1.plot_id
            LEFT JOIN
                NO_OF_PLANTS AS t4 ON t4.id = t1.plot_id
            LEFT JOIN
                NO_OF_EARS AS t5 ON t5.id = t1.plot_id
            LEFT JOIN
                SPECIFIC_PLANT AS t6 ON t6.id = t1.plot_id
            LEFT JOIN
                PANNO_SEL AS t7 ON t7.id = t1.plot_id
            `

            let plotHarvestData = await sequelize.query(plotDataQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    plotDbId: plotDbId,
                },
            })
            return plotHarvestData[0]
        }
        catch(error){
            return null
        }
    },

    /**
     * Retrieve the harvest data of the cross
     * @param {*} crossDbId 
     */
    getCrossHarvestData: async (crossDbId) => {
        try{
            let crossHarvestDataQuery = `
                WITH 
                    CROSS_RECORD AS(
                        SELECT 
                            "germplasmCross".id AS cross_id
                        FROM 
                            germplasm.cross AS "germplasmCross"
                        WHERE
                            "germplasmCross".id = (:crossDbId)
                            AND "germplasmCross".is_void = FALSE),
                    CROSSING_DATE AS(
                        SELECT 
                            cross_data.data_value AS crossing_date,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'DATE_CROSSED'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    HARVEST_DATE AS(
                        SELECT 
                            cross_data.data_value AS harvest_date,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'HVDATE_CONT'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    HARVEST_METHOD AS(
                        SELECT 
                            cross_data.data_value AS harvest_method,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'HV_METH_DISC'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    NO_OF_SEED AS(
                        SELECT 
                            cross_data.data_value AS no_of_seed,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'NO_OF_SEED'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    NO_OF_BAGS AS(
                        SELECT 
                            cross_data.data_value AS no_of_bags,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'NO_OF_BAGS'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    NO_OF_PLANTS AS(
                        SELECT 
                            cross_data.data_value AS no_of_plants,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'NO_OF_PLANTS'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    NO_OF_EARS AS(
                        SELECT 
                            cross_data.data_value AS no_of_ears,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'NO_OF_EARS'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    PANNO_SEL AS(
                        SELECT 
                            cross_data.data_value AS panno_sel,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'PANNO_SEL'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1),
                    SPECIFIC_PLANT AS(
                        SELECT 
                            cross_data.data_value AS specific_plant,
                            cross_data.cross_id AS id
                        FROM 
                            germplasm.cross_data 
                        LEFT JOIN
                            master.variable ON cross_data.variable_id= variable.id AND variable.is_void = FALSE
                        WHERE 
                            cross_data.cross_id = (:crossDbId)
                            AND variable.abbrev = 'SPECIFIC_PLANT'
                            AND cross_data.is_void = FALSE
                        ORDER BY
                            cross_data.id DESC
                        LIMIT 1)
                SELECT 
                    cross_id,
                    crossing_date,
                    harvest_date,
                    harvest_method,
                    no_of_seed,
                    no_of_bags,
                    no_of_plants,
                    no_of_ears,
                    panno_sel,
                    specific_plant
                FROM
                    CROSS_RECORD AS t1
                LEFT JOIN
                    CROSSING_DATE AS t2 ON t2.id = t1.cross_id
                LEFT JOIN
                    HARVEST_DATE AS t3 ON t3.id = t1.cross_id
                LEFT JOIN
                    HARVEST_METHOD AS t4 ON t4.id = t1.cross_id
                LEFT JOIN
                    NO_OF_SEED AS t5 ON t5.id = t1.cross_id
                LEFT JOIN
                    NO_OF_BAGS AS t6 ON t6.id = t1.cross_id
                LEFT JOIN
                    NO_OF_PLANTS AS t7 ON t7.id = t1.cross_id
                LEFT JOIN
                    NO_OF_EARS AS t8 ON t8.id = t1.cross_id
                LEFT JOIN
                    PANNO_SEL AS t9 ON t9.id = t1.cross_id
                LEFT JOIN
                    SPECIFIC_PLANT AS t10 ON t10.id = t1.cross_id
                `

            let crossHarvestData = await sequelize.query(crossHarvestDataQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    crossDbId: crossDbId,
                },
            })
            return crossHarvestData[0]
        }
        catch(error){
            return null
        }
    },

    /**
     * Get the germplasm relations of the germplasm
     * @param {*} childGermplasmDbId - dbId of the child germplasm
     */
     getGermplasmParents : async (childGermplasmDbId) => {
        return await getGermplasmParents(childGermplasmDbId)
    },


    /**
     * Get the cross parents of the cross
     * @param {*} crossDbId - dbId of the cross
     */
    getCrossParents : async (crossDbId) => {
        return await getCrossParents(crossDbId);
    },

    /**
     * Retrieves the family information of the given family id
     * @param {*} familyDbId family identifier
     */
    getFamilyInfo : async (familyDbId) => {
        return await getFamilyInfo(familyDbId);
    },

    /**
     * Retrieve germplasm information
     * @param {*} germplasmDbId - dbId of the germplasm
     */
    getGermplasmInfo : async (germplasmDbId) => {
        return await getGermplasmInfo(germplasmDbId);
    },

    /**
     * Retrieves the child germplasm id based on the given parents
     * @param {*} femaleParentGermplasmDbId female germplasm identifier
     * @param {*} maleParentGermplasmDbId male germplasm identifier
     * @param {boolean} ordered if it follows 1-female and 2-male ordering
     * @param {boolean} includeChildInfo if it includes child info
     * @param {array} childConditions conditions for the child
     * @param {boolean} includeCrossInfo if it includes cross info
     * @param {boolean} getCompleteInfo if it includes complete info
     */
    getChildGermplasmId : async (femaleParentGermplasmDbId,maleParentGermplasmDbId,ordered = true, includeChildInfo = false, childConditions = [], includeCrossInfo = false, getCompleteInfo = false) => {
        return await getChildGermplasmId(femaleParentGermplasmDbId,maleParentGermplasmDbId, ordered, includeChildInfo, childConditions, includeCrossInfo, getCompleteInfo)
    },

    /**
     * Checks if one or both parents are OPV, and applies the correct
     * germplasm type for the child
     * @param {integer} femaleParentGermplasmDbId female parent germplasm identifier
     * @param {integer} maleParentGermplasmDbId male parent germplasm identifier
     * @returns {object} containing hasOPV (whether or not there is an OPV parent) and typeAbbrev (germplasm type abbrev for the child)
     */
    checkOPVMaize: async (femaleParentGermplasmDbId,maleParentGermplasmDbId) => {
        let femaleParentGermplasm = await getGermplasmInfo(femaleParentGermplasmDbId)
        let maleParentGermplasm = await getGermplasmInfo(maleParentGermplasmDbId)

        let parentAState = femaleParentGermplasm.germplasmState
        let parentAType = femaleParentGermplasm.germplasmType
        let parentBState = maleParentGermplasm.germplasmState
        let parentBType = maleParentGermplasm.germplasmType

        let hasOPV = false
        let typeAbbrev = ''

        // Case 1: One parent is fixed (any type), and one parent is not_fixed and OPV
        // Child type = top cross hybrid
        if (
            (parentAState === 'fixed' && parentBState === 'not_fixed' && parentBType === 'OPV')
            || (parentAState === 'not_fixed' && parentAType === 'OPV' && parentBState === 'fixed')
        ) {
            hasOPV = true
            typeAbbrev = 'GERMPLASM_TYPE_TOP_CROSS_HYBRID'
        }
        // Case 2: Both parents are not_fixed and OPV
        // Child type = varietal cross hybrid
        else if (
            parentAState === 'not_fixed' && parentAType === 'OPV'
            && parentBState === 'not_fixed' && parentBType === 'OPV'
        ) {
            hasOPV = true
            typeAbbrev = 'GERMPLASM_TYPE_VARIETAL_CROSS_HYBRID'
        }

        return {
            hasOPV: hasOPV,
            typeAbbrev: typeAbbrev
        }

    },

    /**
     * Retrieves the crosses count based on the given germplasm id
     * @param {integer} germplasmId germplasm identifier
     * @param {string} crossMethod optional cross method
     */
    checkExistingCross : async (germplasmId,crossMethod) => {
        return await checkExistingCross(germplasmId,crossMethod)
    },

    /**
     * Generic function for updating records
     * @param {*} dbId record identifier
     * @param {*} schema where record belongs to
     * @param {*} table where record belongs to
     * @param {*} values array containing columns to update and values
     * @param {*} userId user identifier
     */
    updateRecordById : async (dbId, schema, table, values, userId) => {
        return await updateRecordById(dbId, schema, table, values, userId)
    },

    /**
     * Insert record with the given column values
     * into the given schema and table
     * @param {*} schema where record belongs to
     * @param {*} table where record belongs to
     * @param {*} columnValueArray columns to insert and respective values
     * @param {*} userId user identifier
     */
    insertRecord : async (schema, table, columnValueArray, userId) => {
        return await insertRecord(schema, table, columnValueArray, userId)
    },

    /**
     * Normalize text
     * @param {*} text 
     */
    normalizeText : async (text) => {
        return await normalizeText(text)
    },

    /**
     * Generate family code from database
     * @param {string} prefix prefix for family code
     */
     generateFamilyCode : async (prefix = 'FAM') => {
        return await generateFamilyCode(prefix)
    },

    /**
     * Generate germplasm code from database
     * @param {string} prefix prefix for germplasm code
     */
     generateGermplasmCode : async (prefix = 'GE') => {
        return await generateGermplasmCode(prefix)
    },

    /**
     * Generate seed code from database
     * @param {string} prefix prefix for seed code
     */
    generateSeedCode : async (prefix = 'SEED') => {
        return await generateSeedCode(prefix)
    },

    /**
     * Generate package code from database
     * @param {string} prefix prefix for package code
     */
    generatePackageCode : async (prefix) => {
        return await generatePackageCode(prefix)
    },

    /**
     * Germplasm Type Process for Maize and Wheat
     * Determine the germplasm type of the child
     * based on the parents' germplasm state
     * @param {*} femaleParentGermplasm germplasm record of female parent
     * @param {*} maleParentGermplasm germplasm record of male parent
     */
     germplasmTypeProcess : async (femaleParentGermplasm, maleParentGermplasm) => {
        let germplasmType = ""

        if( femaleParentGermplasm == undefined || maleParentGermplasm == undefined ||
            femaleParentGermplasm.germplasmState == undefined || maleParentGermplasm.germplasmState == undefined){
            return null
        }

        // Get state of female parent and male parent
        let femaleParentGermplasmState = femaleParentGermplasm !== null && femaleParentGermplasm.germplasmState !== undefined ? femaleParentGermplasm.germplasmState : ''
        let maleParentGermplasmState = maleParentGermplasm !== null &&  maleParentGermplasm.germplasmState !== undefined ? maleParentGermplasm.germplasmState : ''

        // If both parents' germplasm state is "fixed" then the child gemplasm type is F1F, else F1
        if(femaleParentGermplasmState == "fixed" && maleParentGermplasmState == "fixed") {
            germplasmType = "F1F"
        } else {
            germplasmType = "F1"
        }

        return germplasmType
    },

    /**
     * Determines what the child germplasm taxonomy ID should be
     * based on the parents' taxonomy IDs.
     * @param {object} femaleParentGermplasm germplasm record of female parent
     * @param {object} maleParentGermplasm germplasm record of male parent
     * @param {object} options other options
     */
    determineTaxonomy: async(femaleParentGermplasm, maleParentGermplasm, options = {}) => {
        let childTaxonomyDbId = null
        let taxonomyNameValue = null

        // Get female and male taxonomy records
        // FEMALE
        let femaleTaxonomy = await retrieveRecordsBasic(
            "germplasm",
            "taxonomy",
            [],
            [
                `id = ${femaleParentGermplasm.taxonomyDbId}`,
                'is_void = FALSE'
            ]
        )
        femaleTaxonomy = femaleTaxonomy != null && femaleTaxonomy[0] != null ? femaleTaxonomy[0] : []
        // MALE
        let maleTaxonomy = await retrieveRecordsBasic(
            "germplasm",
            "taxonomy",
            [],
            [
                `id = ${maleParentGermplasm.taxonomyDbId}`,
                'is_void = FALSE'
            ]
        )
        maleTaxonomy = maleTaxonomy != null && maleTaxonomy[0] != null ? maleTaxonomy[0] : []

        // Get taxonomy preference config
        let taxonomyPreferenceConfig = await getConfigGeneric(
            'HM_TAXONOMY_PREFERENCE',
            options.cropCode,
            options.programCode
        )
        let preferredTaxonId = taxonomyPreferenceConfig != null && taxonomyPreferenceConfig.taxon_id != null ? 
            taxonomyPreferenceConfig.taxon_id : null
        let createGermplasmAttribute = taxonomyPreferenceConfig != null && taxonomyPreferenceConfig.add_germplasm_attribute == 'true'

        // Get preferred taxonomy record
        let preferredTaxonomy = null
        if (preferredTaxonId != null) {
            preferredTaxonomy = await retrieveRecordsBasic(
                "germplasm",
                "taxonomy",
                [],
                [
                    `taxon_id = '${preferredTaxonId}'`,
                    'is_void = FALSE'
                ]
            )
            preferredTaxonomy = preferredTaxonomy != null && preferredTaxonomy[0] != null ? preferredTaxonomy[0] : []
        }

        // CASE 1: female and male taxonomy are the same
        // RESULT: use same taxonomy for child
        if (femaleTaxonomy.id == maleTaxonomy.id) {
            return {
                taxonomyDbId: femaleTaxonomy.id,
                taxonomyName: null
            }
        }

        // CASE 2: one parent's taxonomy is preferred (config)
        // RESULT: use preferred taxonomy
        if (preferredTaxonomy != null
            && (
                preferredTaxonomy.id == femaleTaxonomy.id
                || preferredTaxonomy.id == maleTaxonomy.id
            )    
        ) {
            childTaxonomyDbId = preferredTaxonomy.id
        }

        // CASE 3: none of their taxonomies are preferred
        // RESULT: use taxon_id = 'unknown' taxonomy record
        else {
            let unknownTaxonomy = await retrieveRecordsBasic(
                "germplasm",
                "taxonomy",
                [],
                [
                    `(LOWER(taxon_id) = 'unknown' OR LOWER(taxonomy_name) = 'unknown')`,
                    'is_void = FALSE'
                ]
            )
            unknownTaxonomy = unknownTaxonomy != null && unknownTaxonomy[0] != null ? unknownTaxonomy[0] : []
            childTaxonomyDbId = unknownTaxonomy.id
        }

        // Build Taxonomy Name
        if (createGermplasmAttribute || options.includeTaxonomyName) {
            let femaleTaxonomyName = femaleTaxonomy.taxonomy_name
            let maleTaxonomyName = maleTaxonomy.taxonomy_name
            // Taxonomy name = <female taxonomy name> x <male taxonomy name>
            taxonomyNameValue = femaleTaxonomyName + ' x ' + maleTaxonomyName
        }

        return {
            taxonomyDbId: childTaxonomyDbId,
            taxonomyName: taxonomyNameValue
        }
    },

    /**
     * Taxonomy Process for Wheat
     * Determines the taxonomy id of the child
     * based on the parents' taxonomy ids
     * @param {object} femaleParentGermplasm germplasm record of female parent
     * @param {object} maleParentGermplasm germplasm record of male parent
     * @param {boolean} isMHI if the pairing is a maternal haploid induction cross (defaults to false)
     */
    taxonomyProcess: async (femaleParentGermplasm, maleParentGermplasm, isMHI = false) => {
        let newGermplasmTaxonomyId = null
        
        // If isMHI and female taxonomy id is not null
        if(isMHI && femaleParentGermplasm.taxonomyDbId != null) {
            newGermplasmTaxonomyId = femaleParentGermplasm.taxonomyDbId
        }
        // Check parent taxonomy_id match
        else if(femaleParentGermplasm.taxonomyDbId != null && femaleParentGermplasm.taxonomyDbId == maleParentGermplasm.taxonomyDbId){
            // Assign child taxonomy_id
            newGermplasmTaxonomyId = femaleParentGermplasm.taxonomyDbId
        }
        else{
            try {
                let taxonomyQuery = `
                    SELECT 
                        id AS "taxonomyDbId" 
                    FROM 
                        germplasm.taxonomy 
                    WHERE 
                        (
                            LOWER(taxon_id) = 'unknown'
                            OR LOWER(taxonomy_name) = 'unknown'
                        ) 
                        AND is_void = FALSE
                `
                // Retrieve taxonomy_id for taxonomy_name = "Unknown"
                let taxonomyDbId = await sequelize.query(taxonomyQuery, {
                    type: sequelize.QueryTypes.SELECT
                })

                if(taxonomyDbId[0] != null) newGermplasmTaxonomyId = taxonomyDbId[0].taxonomyDbId
            }
            catch (err){
                return false
            }
        }

        return newGermplasmTaxonomyId
    },

    /**
     * Parent Attribute Propagation for Maize
     * Determines the parent attributes to be propagated to generated materials
     * based on the parents' attributes
     * @param {*} crossParents cross record of the parents
     */
    parentAttributePropagation : async (crossParents) => {

        // initialize variables
        let childGrainColorAttribute = null
        let childHetGroupAttribute = null

        // retrieve variable by abbrev
        let hetGroupVariable = await getVariableByAbbrev('HETEROTIC_GROUP')
        let hetGroupVariableDbId = hetGroupVariable != undefined && hetGroupVariable.variableDbId != undefined && hetGroupVariable.variableDbId != null ? hetGroupVariable.variableDbId : null
        let grainColorVariable = await getVariableByAbbrev('GRAIN_COLOR')
        let grainColorVariableDbId = grainColorVariable != undefined && grainColorVariable.variableDbId != undefined && grainColorVariable.variableDbId != null ? grainColorVariable.variableDbId : null

        if(hetGroupVariableDbId != null && grainColorVariableDbId != null){
            // retrieve parent germplasm attribute
            let femaleParentHetGroupAttr = await getGermplasmAttribute(crossParents.femaleParent.germplasmDbId, hetGroupVariableDbId)
            let femaleParentGrainColorAttr = await getGermplasmAttribute(crossParents.femaleParent.germplasmDbId, grainColorVariableDbId)

            let maleParentHetGroupAttr = await getGermplasmAttribute(crossParents.maleParent.germplasmDbId, hetGroupVariableDbId)
            let maleParentGrainColorAttr = await getGermplasmAttribute(crossParents.maleParent.germplasmDbId, grainColorVariableDbId)

            // check if parents has attributes
            let femaleParentHetGroup = femaleParentHetGroupAttr != undefined ? femaleParentHetGroupAttr.dataValue : null
            let maleParentHetGroup = maleParentHetGroupAttr != undefined ? maleParentHetGroupAttr.dataValue : null

            let femaleParentGrainColor = femaleParentGrainColorAttr != undefined ? femaleParentGrainColorAttr.dataValue : null
            let maleParentGrainColor = maleParentGrainColorAttr != undefined ? maleParentGrainColorAttr.dataValue : null

            // If the F and M parents have the same value for the attribute, pass it down to the child germplasm.
            if(femaleParentHetGroup != null && maleParentHetGroup != null){
                if(femaleParentHetGroup == maleParentHetGroup) {
                    childHetGroupAttribute = femaleParentHetGroup
                }
            }

            if(femaleParentGrainColor != null && maleParentGrainColor != null){
                if(femaleParentGrainColor == maleParentGrainColor) {
                    childGrainColorAttribute = femaleParentGrainColor
                }
            }
        }

        // return propagated parent attributes
        return {
            'childGrainColor': childGrainColorAttribute,
            'childHetGroup': childHetGroupAttribute
        }
    },

    /**
     * Identify the growth habit of the child based on the parents' growth habit.
     * If fgh = mgh, then fgh
     * Else, no growth habit
     * @param {Integer} fGermplasmDbId female germplasm identifier
     * @param {Integer} mGermplasmDbId male germplasm identifier
     */
    growthHabitProcess: async(fGermplasmDbId,mGermplasmDbId) => {
        let variableInfo = await getVariableByAbbrev('GROWTH_HABIT')
        let variableDbId = variableInfo.variableDbId !== undefined? variableInfo.variableDbId : 0
        let fGermplasmAttribute = await getGermplasmAttribute(fGermplasmDbId,variableDbId)
        let fGermplasmGrowthHabit = fGermplasmAttribute != undefined && fGermplasmAttribute.dataValue !== undefined ? fGermplasmAttribute.dataValue : ''
        let mGermplasmAttribute = await getGermplasmAttribute(mGermplasmDbId,variableDbId)
        let mGermplasmGrowthHabit = mGermplasmAttribute != undefined && mGermplasmAttribute.dataValue !== undefined ? mGermplasmAttribute.dataValue : ''
        // set defaults
        let growthHabit = '';
        let growthHabitAttributeValue = null;

        // If both parents have growth habit
        if (fGermplasmGrowthHabit != '' && mGermplasmGrowthHabit != '') {
            // Build growth habit for BCID
            growthHabit = fGermplasmGrowthHabit.concat(mGermplasmGrowthHabit)

            // If both parents have the same growth habit,
            // set child's growth habit attribute to the same value
            if (fGermplasmGrowthHabit == mGermplasmGrowthHabit) {
                growthHabitAttributeValue = fGermplasmGrowthHabit
            }
        }

        return {
            growthHabit: growthHabit,
            growthHabitAttributeValue: growthHabitAttributeValue
        }
    },

    /**
     * BCID Process for Wheat
     * Determines the BCID of the child
     * based on the parents' cross records
     * @param {*} crossParents cross record of the parents
     * @param {*} crossHarvestData harvest data of the cross record
     * @param {*} siteCode site code of the cross record
     * @param {*} crossTypeCode type code of the cross record
     */
    BCIDProcess: async (crossParents, crossHarvestData, siteCode, crossTypeCode) => {
        let femaleParentProgramCode = crossParents.femaleParent.programCode
        let variable = await getVariableByAbbrev('GROWTH_HABIT')
        let variableDbId = variable.variableDbId
        let femaleParentAttribute = await getGermplasmAttribute(crossParents.femaleParent.germplasmDbId, variableDbId)
        let femaleParentGrowthHabit = femaleParentAttribute != undefined ? femaleParentAttribute.dataValue : null
        let maleParentAttribute = await getGermplasmAttribute(crossParents.maleParent.germplasmDbId, variableDbId)
        let maleParentGrowthHabit = maleParentAttribute != undefined ? maleParentAttribute.dataValue : null
        let crossOrganizationCode = ''
        let growthHabit = ''
        let growthHabitChild = null
        let year = ''
        let BCIDPattern = ''
        let sequenceNumber = ''
        let BCID = ''

        // Get organization code from female parent's program code
        if(femaleParentProgramCode == 'BW') crossOrganizationCode = 'CM'
        else if(femaleParentProgramCode == 'DW') crossOrganizationCode = 'CD'
        else crossOrganizationCode = femaleParentProgramCode

        // Get growth habit of both parents from germplasm id if both are not null
        if(femaleParentAttribute != undefined
            && maleParentAttribute != undefined
            && femaleParentGrowthHabit != null
            && maleParentGrowthHabit != null) {
            growthHabit = femaleParentGrowthHabit + maleParentGrowthHabit
            
            // if the parents have the same growth habit, use this for the child growth habit
            if(femaleParentGrowthHabit == maleParentGrowthHabit) {
                growthHabitChild = femaleParentGrowthHabit
            }
        }

        // Get 2-digit year from harvest date
        year = crossHarvestData.harvest_date.substring(2,4)

        // Get 5-digit zero-padded sequential number based on crosses made in the same program, growth habit, year, and site
        BCIDPattern = crossOrganizationCode + growthHabit + year + siteCode

        // Get last sequence given the BCID pattern and cross type code
        let sequencePattern = '[0-9]{5}'
        let lastBCID = await getLastInNumberedSequence(
            'germplasm',
            'germplasm',
            'designation',
            BCIDPattern,
            sequencePattern,
            {
                suffix: crossTypeCode
            }
        )
        
        // if the last BCID is null, start sequence at 00001
        if(lastBCID == null) {
            sequenceNumber = '00001'
        }
        // else, parse the last number in the sequence and add one to get the next
        else {
            let sequenceRegex = new RegExp('[0-9]{5}', 'g')
            let matches = lastBCID.match(sequenceRegex)
            let lastInSequence = matches[0]
            sequenceNumber = String(parseInt(lastInSequence) + 1).padStart(5, '0')
        }

        // Generate BCID from organization code, growth habit, year, site code, 5-digit sequence number, and cross type code
        BCID = crossOrganizationCode + growthHabit + year + siteCode + sequenceNumber + crossTypeCode

        return {
            'BCID': BCID,
            'growthHabitChild': growthHabitChild
        }
    },

    /**
     * Parentage process for Rice
     * @param {*} femaleParentGermplasm female germplasm information
     * @param {*} maleParentGermplasm male germplasm information
     * @param {string} crossMethod crossing method used
     * @returns {string} new parentage
     */
    riceParentageProcess : async (femaleParentGermplasm, maleParentGermplasm, crossMethod) => {

        //single cross, complex cross
        let delimiter = '/'
        let female = femaleParentGermplasm.designation
        let male = maleParentGermplasm.designation
        let fourSlahes = false
        let fiveSlahes = false

        if(crossMethod == 'three-way cross' || crossMethod == 'double cross' ){

            female = String(femaleParentGermplasm.generation).includes('F1') ? femaleParentGermplasm.parentage : femaleParentGermplasm.designation
            male = String(maleParentGermplasm.generation).includes('F1') ? maleParentGermplasm.parentage : maleParentGermplasm.designation

            //set delimiter
            if(String(female).includes('////') || String(male).includes('////')){// adding this in preparation of complex crosses
                delimiter = '/////'
                fiveSlahes = true
            }
            else if(fiveSlahes == false && (String(female).includes('///') || String(male).includes('///'))){// this rarely happens
                delimiter = '////'
                fourSlahes = true
            }
            else if((fourSlahes == false && fiveSlahes == false) && (String(female).includes('//') || String(male).includes('//'))){
                delimiter = '///'
            }else{
                delimiter = '//'
            }

        }

        return String(female).concat(delimiter, male)
    },

    /**
     * Determines the cross number of the child 
     * based on the max cross number between the female and male parents.
     * @param {object} crossParents object containing cross parent info
     * @param {boolean} increment whether or not the cross number will be incremented for the child
     * @returns {integer} crossNumber
     */
    getChildCrossNumber : async (crossParents, increment = true) => {
        // get variable id
        let variable = await getVariableByAbbrev('CROSS_NUMBER')
        let variableDbId = variable.variableDbId
        // get female germplasm name record
        let femaleGermplasmName = await getGermplasmNameInfo(crossParents.femaleParent.germplasmDbId)
        let femaleGermplasmNameType = femaleGermplasmName.germplasmNameType
        let femaleGermplasmNameStatus = femaleGermplasmName.germplasmNameStatus
        // get male germplasm name record
        let maleGermplasmName = await getGermplasmNameInfo(crossParents.maleParent.germplasmDbId)
        let maleGermplasmNameType = maleGermplasmName.germplasmNameType
        let maleGermplasmNameStatus = maleGermplasmName.germplasmNameStatus
        // initialize variables
        let femaleCrossNumber = null
        let femaleGermplasmAttribute = null
        let maleCrossNumber = null
        let maleGermplasmAttribute = null
        let maxCrossNumber = null
        // audit variables
        let resultObject = {
            success: true,
            errors: [],
            crossNumber: null
        }

        // For female parent, determine cross number
        // If germplasm name type is either 'cross_abbreviation or 'cross_name', and name status is 'active,
        // set cross number = 0
        if(
            (femaleGermplasmNameType == 'cross_abbreviation' || femaleGermplasmNameType == 'cross_name')
            && femaleGermplasmNameStatus == 'active'
        ) {
            femaleCrossNumber = 0
        }
        // Else, get cross number germplasm attribute
        else {
            femaleGermplasmAttribute = await getGermplasmAttribute(
                crossParents.femaleParent.germplasmDbId, 
                variableDbId
            )
        }

        // For male parent, determine cross number
        // If germplasm name type is either 'cross_abbreviation or 'cross_name', and name status is 'active,
        // set cross number = 0
        if(
            (maleGermplasmNameType == 'cross_abbreviation' || maleGermplasmNameType == 'cross_name')
            && maleGermplasmNameStatus == 'active'
        ) {
            maleCrossNumber = 0
        }
        // Else, get cross number germplasm attribute
        else {
            maleGermplasmAttribute = await getGermplasmAttribute(
                crossParents.maleParent.germplasmDbId, 
                variableDbId
            )
        }

        // If a parent's cross number is missing, add to errors array and set success to FALSE
        if (femaleCrossNumber === null && (femaleGermplasmAttribute === undefined || femaleGermplasmAttribute === null)) {
            resultObject.success = false
            resultObject.errors.push('Missing cross number attribute for female parent germplasm.')
        }
        if (maleCrossNumber === null && (maleGermplasmAttribute === undefined || maleGermplasmAttribute === null)) {
            resultObject.success = false
            resultObject.errors.push('Missing cross number attribute for male parent germplasm.')
        }
        // If success is FALSE, return the result object with errors
        if (!resultObject.success) {
            return resultObject
        }
        // Parse cross numbers
        femaleCrossNumber = femaleCrossNumber === null ? parseInt(femaleGermplasmAttribute.dataValue) : femaleCrossNumber
        maleCrossNumber = maleCrossNumber === null ? parseInt(maleGermplasmAttribute.dataValue) : maleCrossNumber

        // Identify cross number of child
        maxCrossNumber = (femaleCrossNumber > maleCrossNumber)? femaleCrossNumber: maleCrossNumber

        // If increment is true, return maxCrossNumber + 1
        if(increment) maxCrossNumber += 1

        resultObject.crossNumber = maxCrossNumber
        return resultObject
    },

    /**
     * Parentage Process for Wheat
     * Generates the pedigree and cross number of a resulting cross
     * given the information of the cross parents
     * and male parent designation
     * @param {*} crossParents information of cross parents
     */
    wheatParentageProcess : async (crossParents) => {
        let parentage = null
        let variable = await getVariableByAbbrev('CROSS_NUMBER')
        let maxCrossNumber = null
        let crossDelimiter = null
        // female variables
        let femaleGermplasm = await getGermplasmInfo(crossParents.femaleParent.germplasmDbId)
        let femaleGermplasmName = null
        let femaleGermplasmNameType = null
        let femaleCrossParentName = null
        let femaleCrossNumber = null
        let femaleGermplasmAttribute = null
        // male variables
        let maleGermplasm = await getGermplasmInfo(crossParents.maleParent.germplasmDbId)
        let maleGermplasmName = null
        let maleGermplasmNameType = null
        let maleCrossParentName = null
        let maleCrossNumber = null
        let maleGermplasmAttribute = null

        // Check if the female parent has a germplasm_name with name type = cross_abbreviation
        femaleGermplasmName = await getGermplasmNameByType(crossParents.femaleParent.germplasmDbId, 'cross_abbreviation')
        // If cross_abbreviation does not exist, get germplasm_name with name type = cross_name
        if(femaleGermplasmName == undefined) {
            femaleGermplasmName = await getGermplasmNameByType(crossParents.femaleParent.germplasmDbId, 'cross_name')
            // If cross_name does not exist, get standard/active germplasm name
            if(femaleGermplasmName == undefined) {
                femaleGermplasmName = await getGermplasmNameInfo(crossParents.femaleParent.germplasmDbId)
            }
        }
        femaleGermplasmNameType = femaleGermplasmName.germplasmNameType

        // Check if the male parent has a germplasm_name with name type = cross_abbreviation
        maleGermplasmName = await getGermplasmNameByType(crossParents.maleParent.germplasmDbId, 'cross_abbreviation')
        // If cross_abbreviation does not exist, get germplasm_name with name type = cross_name
        if(maleGermplasmName == undefined) {
            maleGermplasmName = await getGermplasmNameByType(crossParents.maleParent.germplasmDbId, 'cross_name')
            // If cross_name does not exist, get standard/active germplasm name
            if(maleGermplasmName == undefined) {
                maleGermplasmName = await getGermplasmNameInfo(crossParents.maleParent.germplasmDbId)
            }
        }
        maleGermplasmNameType = maleGermplasmName.germplasmNameType

        // For female parent, determine cross parent name and cross parent number
        if(femaleGermplasmNameType == 'cross_abbreviation' || femaleGermplasmNameType == 'cross_name') {
            femaleCrossParentName = femaleGermplasmName.nameValue
            femaleCrossNumber = 0
        } else {
            femaleCrossParentName = femaleGermplasm.parentage
            femaleGermplasmAttribute = await getGermplasmAttribute(crossParents.femaleParent.germplasmDbId, variable.variableDbId)
            femaleCrossNumber = parseInt(femaleGermplasmAttribute !== undefined ? femaleGermplasmAttribute.dataValue : '0')
        }

        // For male parent, determine cross parent name and cross parent number
        if(maleGermplasmNameType == 'cross_abbreviation' || maleGermplasmNameType == 'cross_name') {
            maleCrossParentName = maleGermplasmName.nameValue
            maleCrossNumber = 0
        } else {
            maleCrossParentName = maleGermplasm.parentage
            maleGermplasmAttribute = await getGermplasmAttribute(crossParents.maleParent.germplasmDbId, variable.variableDbId)
            maleCrossNumber = parseInt(maleGermplasmAttribute !== undefined ? maleGermplasmAttribute.dataValue : '0')
        }

        // Identify cross number of child
        maxCrossNumber = (femaleCrossNumber > maleCrossNumber)? femaleCrossNumber: maleCrossNumber

        // Get cross delimiter for parentage
        if(maxCrossNumber == 0) crossDelimiter = '/'
        else if(maxCrossNumber == 1) crossDelimiter = '//'
        else crossDelimiter = '/'+(++maxCrossNumber)+'/'

        // Create parentage
        parentage = femaleCrossParentName + crossDelimiter + maleCrossParentName

        return {
            'parentage': parentage,
            'crossNumber': maxCrossNumber
        }
    },

    /**
     * Pedigree Process for Maize
     * Generates the pedigree of a resulting cross
     * given the designation of the female parent designation
     * and male parent designation
     * @param {*} femaleParentGermplasm germplasm record of female parent
     * @param {*} maleParentGermplasm germplasm record of male parent
     * @param {string} delimiter use slash (/) when non hybrid, (&) when hybrid 
     */
    pedigreeProcess : async (femaleParentGermplasm, maleParentGermplasm, delimiter = '/') => {
        // Get designation of female parent and male parent
        let femaleParentDesignation = femaleParentGermplasm.designation
        let maleParentDesignation = maleParentGermplasm.designation

        // Build pedigree:
        // <female_parent_designation>/<male_parent_designation>
        let pedigree = femaleParentDesignation.concat(delimiter,maleParentDesignation)

        return pedigree
    },

    createGermplasmRecord : async (germplasmData, userId) => {
        let transaction
        let germplasmValuesArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })

            let tempArray = [
                germplasmData.designation,
                germplasmData.parentage,
                germplasmData.generation,
                germplasmData.germplasmState,
                germplasmData.germplasmNameType,
                germplasmData.germplasmType,
                germplasmData.cropId,
                germplasmData.taxonomyId,
                userId
            ]

            germplasmValuesArray.push(tempArray)

            // Create germplasm
            let germplasmQuery = format(`
                INSERT INTO germplasm.germplasm 
                    (designation, parentage, generation, germplasm_state, germplasm_name_type, germplasm_type, crop_id, taxonomy_id, creator_id)
                VALUES 
                    %L
                RETURNING id`, germplasmValuesArray
            )

            let germplasm = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            await transaction.commit()
            return germplasm[0]

        } catch (err) {
            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            return false
        }
    },

    createGermplasm: async (germplasmData, userDbId, plotRecord) => {

        let transaction
        let germplasmValuesArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            //generate seedCode
            let designation = germplasmData.designation
            let parentage = germplasmData.parentage
            let generation = germplasmData.generation
            let state = germplasmData.germplasmState
            let nameType = germplasmData.germplasmNameType
            let normalizedName = await germplasmHelper.normalizeText(germplasmData.germplasmNormalizedName)
            let cropId = germplasmData.cropId
            let taxonomyId = germplasmData.taxonomyId
            let productProfileId = null
            let germplasmType = null

            if(String(plotRecord['crossMethod']).toLowerCase() == "selfing" && String(plotRecord['germplasmState']).toLowerCase() == "not_fixed"){
                germplasmType = "segregating"   
            }else if (String(plotRecord['crossMethod']).toLowerCase() == 'selfing' && String(plotRecord['germplasmState']).toLowerCase() == "fixed"){
                germplasmType = "inbred_line"   
            }

            let tempArray = [
                designation,
                parentage,
                generation,
                state,
                nameType,
                germplasmType,
                normalizedName,
                cropId,
                taxonomyId,
                productProfileId,
                userDbId
            ]

            germplasmValuesArray.push(tempArray)

            // Create germplasm
            let germplasmQuery = format(`
                INSERT INTO germplasm.germplasm 
                    (designation, parentage, generation, germplasm_state, germplasm_name_type, germplasm_type,germplasm_normalized_name, crop_id, taxonomy_id,
                    product_profile_id, creator_id)
                VALUES 
                    %L
                RETURNING id`, germplasmValuesArray
            )

            let germplasm = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction
            })

            await transaction.commit()
            return germplasm[0]

        } catch (err) {
            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            return false
        }
    },

    createGermplasmName: async (germplasmData, plotRecord, userDbId) => {

        let transaction
        let germplasmNameValuesArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            //generate seedCode
            let nameValue = germplasmData.designation
            let germplasmNameType = germplasmData.germplasmNameType
            let germplasmNameStatus = "standard"
            let germplasmNormalizedName = germplasmData.germplasmNormalizedName
            let germplasmDbId = germplasmData.germplasmDbId
            let creatorDbId = userDbId

            let tempArray = [
                germplasmDbId,
                nameValue,
                germplasmNameType,
                germplasmNameStatus,
                germplasmNormalizedName,
                creatorDbId
            ]

            germplasmNameValuesArray.push(tempArray)

            // Create germplasmName record/s
            let germplasmNameQuery = format(`
                INSERT INTO
                  germplasm.germplasm_name (
                    germplasm_id, name_value, germplasm_name_type,germplasm_name_status, germplasm_normalized_name,creator_id
                  )
                VALUES 
                  %L
                RETURNING id`, germplasmNameValuesArray
            )

            let germplasmNames = await sequelize.query(germplasmNameQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction,
            })

            await transaction.commit()

            return germplasmNames[0]

        } catch (err) {
            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            return false
        }
    },

    createSeedRecord: async (seedData, userId) => {
        let transaction
        let seedsValuesArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            //generate seedCode
            let generateSeedCodeQuery =
                `
                SELECT
                    germplasm.generate_code('seed')
                AS seedcode 
            `
            let seed = await sequelize.query(generateSeedCodeQuery, {
                type: sequelize.QueryTypes.SELECT,
            })

            let seedCode = seed[0].seedcode
            let creatorId = userId

            let tempArray = [
                seedCode,
                seedData.seedName,
                seedData.harvestDate,
                seedData.harvestMethod,
                seedData.germplasmDbId,
                seedData.programDbId,
                seedData.sourceExperimentDbId,
                seedData.sourceEntryDbId,
                seedData.sourcePlotDbId,
                seedData.sourceOccurrenceDbId,
                seedData.sourceLocationDbId,
                seedData.crossDbId,
                seedData.harvestSource,
                creatorId
            ]

            seedsValuesArray.push(tempArray)

            let insertSeedsQuery = format(`
                INSERT INTO
                    germplasm.seed (
                    seed_code,seed_name, harvest_date, harvest_method,
                    germplasm_id,program_id, source_experiment_id,
                    source_entry_id, source_plot_id, source_occurrence_id, source_location_id, cross_id, harvest_source,
                    creator_id) 
                VALUES 
                    %L RETURNING id`, seedsValuesArray
            )

            let seeds = await sequelize.query(insertSeedsQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction,
            })
            await transaction.commit()
            return seeds[0]

        } catch (err) {
            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            return false
        }
    },

    createSeed: async (newGermplasmDbId, plotRecord, harvestData, userId, seedCounter) => {

        let transaction
        let seedsValuesArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            //generate seedCode
            let generateSeedCodeQuery =
                `
                SELECT
                    germplasm.generate_code('seed')
                AS seedcode 
            `
            let seed = await sequelize.query(generateSeedCodeQuery, {
                type: sequelize.QueryTypes.SELECT,
            })

            let seedCode = seed[0].seedcode
            // generate seedName
            let crop = String(plotRecord.cropCode).toUpperCase()
            let programCode = String(plotRecord.programCode).toUpperCase()
            let seedNameConfig = null

            let seedNameAbbrevProgram = "HM_SEED_NAME_PATTERN_"
            seedNameAbbrevProgram = seedNameAbbrevProgram.concat(crop)
            seedNameAbbrevProgram = seedNameAbbrevProgram.concat("_")
            seedNameAbbrevProgram = seedNameAbbrevProgram.concat(programCode)

            seedNameConfig = await getHarvestConfig(seedNameAbbrevProgram)

            if (seedNameConfig == undefined || seedNameConfig.length == 0) {

                let seedNameAbbrevDefault = "HM_SEED_NAME_PATTERN_"
                seedNameAbbrevDefault = seedNameAbbrevDefault.concat(crop)
                seedNameAbbrevDefault = seedNameAbbrevDefault.concat("_DEFAULT")
                seedNameConfig = await getHarvestConfig(seedNameAbbrevDefault)
            }
            let occurrenceCount = await getExperimentOccurrenceCount(plotRecord.experimentDbId)

            let seedName = await createSeedName(seedNameConfig.configValue, plotRecord, seedCounter,occurrenceCount)

            //other seed fields
            let germplasmDbId = newGermplasmDbId
            let harvestDate = harvestData.harvest_date
            let harvestMethod = harvestData.harvest_method
            let programDbId = plotRecord.seedProgramId
            let sourceExperimentDbId = plotRecord.experimentDbId
            let sourceEntryDbId = plotRecord.entryDbId
            let sourcePlotDbId = plotRecord.plotDbId
            let sourceOccurrenceDbId = plotRecord.occurrenceDbId
            let sourceLocationDbId = plotRecord.locationDbId
            let description = null
            let notes = null
            let harvestSource = 'plot'
            let creatorId = userId

            let tempArray = [
                seedCode,
                seedName,
                harvestDate,
                harvestMethod,
                germplasmDbId,
                programDbId,
                sourceExperimentDbId,
                sourceEntryDbId,
                sourcePlotDbId,
                sourceOccurrenceDbId,
                sourceLocationDbId,
                description,
                creatorId,
                notes,
                harvestSource
            ]

            seedsValuesArray.push(tempArray)
            //check seed duplicate before inserting
            let seedObject = await getSeed(seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId)

            if (seedObject == undefined) {

                let insertSeedsQuery = format(`
                    INSERT INTO
                        germplasm.seed (
                        seed_code,seed_name, harvest_date,harvest_method,
                        germplasm_id,program_id,source_experiment_id,
                        source_entry_id,source_plot_id,source_occurrence_id,source_location_id,
                        description,creator_id,notes,harvest_source) 
                    VALUES 
                        %L RETURNING id`, seedsValuesArray
                )

                let seeds = await sequelize.query(insertSeedsQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                })
                await transaction.commit()
                return seeds[0]

            } else {
                await transaction.commit()
                return seedObject[0]
            }
            
        } catch (err) {
            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            return false
        }
    },

    createPackageRecord: async (packageData, userId) => {
        let transaction
        let packageValuesArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            //generate packageCode
            let generatePackageCodeQuery = `
                SELECT
                    germplasm.generate_code('package')
                AS packagecode `

            let packageRecord = await sequelize.query(generatePackageCodeQuery, {
                type: sequelize.QueryTypes.SELECT,
            })

            let packageCode = packageRecord[0].packagecode

            //other seed fields
            let packageQuantity = 0
            let packageUnit = ""
            let packageStatus = "active"
            
            let geospatialObjectDbId = null
            let facilityDbId = null
            let creatorId = userId

            // Get values
            let tempArray = [
                packageCode,
                packageData.packageLabel,
                packageQuantity,
                packageUnit,
                packageStatus,
                packageData.seedDbId,
                packageData.programDbId,
                geospatialObjectDbId,
                facilityDbId,
                creatorId,
            ]

            packageValuesArray.push(tempArray)

            let packagesQuery = format(`
                INSERT INTO
                    germplasm.package (
                    package_code, package_label, package_quantity, package_unit,
                    package_status, seed_id, program_id, geospatial_object_id,
                    facility_id, creator_id
                )VALUES 
                %L RETURNING id`, packageValuesArray)

            let packages = await sequelize.query(packagesQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction,
            })

            await transaction.commit()
            return packages[0]

        } catch (err) {

            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            return false
        }
    },

    createPackage: async (seedRecord, plotRecord, userId) => {
        let transaction
        let packageValuesArray = []

        try {
            // Get transaction
            transaction = await sequelize.transaction({ autocommit: false })
            //generate seedCode
            let generatePackageCodeQuery = `
                SELECT
                    germplasm.generate_code('package')
                AS packagecode `

            let packageRecord = await sequelize.query(generatePackageCodeQuery, {
                type: sequelize.QueryTypes.SELECT,
            })

            let packageCode = packageRecord[0].packagecode

            // generate packageName
            let crop = String(plotRecord.cropCode).toUpperCase()
            let programCode = String(plotRecord.programCode).toUpperCase()
            let packageNameConfig = null

            let packageNameAbbrevProgram = "HM_PACKAGE_NAME_PATTERN_"
            packageNameAbbrevProgram = packageNameAbbrevProgram.concat(crop)
            packageNameAbbrevProgram = packageNameAbbrevProgram.concat("_")
            packageNameAbbrevProgram = packageNameAbbrevProgram.concat(programCode)

            packageNameConfig = await getHarvestConfig(packageNameAbbrevProgram)

            if (packageNameConfig == undefined || packageNameConfig.length == 0) {
                let packageNameAbbrevDefault = "HM_PACKAGE_NAME_PATTERN_"
                packageNameAbbrevDefault = packageNameAbbrevDefault.concat(crop)
                packageNameAbbrevDefault = packageNameAbbrevDefault.concat("_DEFAULT")
                packageNameConfig = await getHarvestConfig(packageNameAbbrevDefault)
            }

            let packageLabel = await createPackageName(packageNameConfig.configValue, seedRecord,plotRecord)

            //other seed fields
            let packageQuantity = 0
            let packageUnit = ""
            let packageStatus = "active"

            let seedDbId = seedRecord.seedDbId
            let programDbId = plotRecord.seedProgramId
            let geospatialObjectDbId = null
            let facilityDbId = null
            let creatorId = userId

            // Get values
            let tempArray = [
                packageCode,
                packageLabel,
                packageQuantity,
                packageUnit,
                packageStatus,
                seedDbId,
                programDbId,
                geospatialObjectDbId,
                facilityDbId,
                creatorId,
            ]

            packageValuesArray.push(tempArray)

            let packagesQuery = format(`
                INSERT INTO
                    germplasm.package (
                    package_code, package_label, package_quantity, package_unit,
                    package_status, seed_id, program_id, geospatial_object_id,
                    facility_id, creator_id
                )VALUES 
                %L RETURNING id`, packageValuesArray)

            let packages = await sequelize.query(packagesQuery, {
                type: sequelize.QueryTypes.INSERT,
                transaction: transaction,
            })

            await transaction.commit()
            return packages[0]

        } catch (err) {

            // Rollback transaction (if still open)
            if (transaction.finished !== 'commit') transaction.rollback()
            return false
        }
    },

    getNewGermplasmDesignationForBulk: async (plotRecord, harvestData) => {

        try {
            let germplasmGeneration = plotRecord.germplasmGeneration
            let designation = String(plotRecord.germplasmDesignation)
            let newDesignation = ''
            let cropCode = String(plotRecord.cropCode).toLocaleLowerCase()
            // get Harvest code 
            let harvestCode = await getHarvestConfig("HM_CROP_BULK_HARVEST_CODE")
            let germplasmNameSuffix = await getHarvestConfig("HM_GERMPLASM_NAME_SUFFIX")
            let harvestCodeValue = null
            let numberPlant = harvestData.numeric_variable

            if (cropCode == "maize") {
                harvestCodeValue = String(harvestCode.configValue.maize.default)

                if ((germplasmGeneration == "F1" || String(germplasmGeneration).includes("BC"))) {

                    if (String(germplasmGeneration).charAt(0) != "(") {
                        newDesignation = newDesignation.concat("(")
                        newDesignation = newDesignation.concat(designation)
                        newDesignation = newDesignation.concat(")")
                        newDesignation = newDesignation.concat("-")
                        newDesignation = newDesignation.concat(harvestCodeValue)
                    } else {
                        newDesignation = newDesignation.concat(designation)
                        newDesignation = newDesignation.concat("-")
                        newDesignation = newDesignation.concat(harvestCodeValue)
                    }
                } else {

                    let designationLastChar = designation.charAt(designation.length - 1)

                    if (designationLastChar == harvestCodeValue) {
                        newDesignation = newDesignation.concat(designation)
                        newDesignation = newDesignation.concat('*2')

                    } else if (designation.includes(harvestCodeValue.concat("*"))) {
                        let temp = designation.split("*")
                        let actionRepeatNo = parseInt(temp[1]) + 1
                        newDesignation = temp[0].concat("*")
                        newDesignation = newDesignation.concat(String(actionRepeatNo))
                    } else {
                        newDesignation = newDesignation.concat(designation)
                        newDesignation = newDesignation.concat("-")
                        newDesignation = newDesignation.concat(harvestCodeValue)
                    }
                }

            } else if (cropCode == "wheat") {

                if (String(plotRecord.harvestMethod).toLowerCase() == "selected bulk" || String(plotRecord.harvestMethod).toLowerCase() == "bulk") {

                    newDesignation = newDesignation.concat(designation)
                    newDesignation = newDesignation.concat("-")
                    newDesignation = newDesignation.concat(harvestCode.configValue.wheat.default)

                    if(String(plotRecord.harvestMethod).toLowerCase() == "selected bulk"){

                        if (numberPlant == null) {
                            newDesignation = newDesignation.concat(harvestCode.configValue.wheat.no_plant_default)
                        } else {
                            newDesignation = newDesignation.concat(numberPlant)
                        }
                    }

                    if (germplasmGeneration == "F1TOP") {
                        newDesignation = newDesignation.concat(harvestCode.configValue.wheat.F1TOP)
                    }
                }
                let fieldValue = germplasmNameSuffix.configValue.wheat.plotInfoField
                newDesignation = newDesignation.concat(plotRecord[fieldValue])

            }

            return newDesignation

        } catch (err) {
            return null
        }
    },

    /**
     * Checks if the given germplasm generation is a backcross generation
     * with the format BCmFn
     * @param {string} germplasmGeneration - the generation of the germplasm
     * @returns {boolean} - wether or not the generation is a backcross generation
     */
    isBackcrossGeneration: async(germplasmGeneration) => {
        if(germplasmGeneration.match(/^BC\d+F\d+$/g)) return true
        return false
    },

    /**
     * Advance the given germplasm F generation
     * @param {string} germplasmGeneration - the F generation to be advanced
     * @return {string} newGeneration - advanced F generation
     */
    advanceGermplasmGeneration: async(germplasmGeneration) => {
        let newGeneration = ''

        // if normal generation (Fn, Tn, Mn, etc)
        if (germplasmGeneration.match(/^[A-Za-z]+\d+$/g)) {
            let genType = germplasmGeneration.match(/[A-Za-z]+/g)[0]
            let generationNumber = parseInt(germplasmGeneration.match(/\d+/g)[0])
            newGeneration = newGeneration.concat(genType,(generationNumber+1))
        }
        // if back cross generation (BCm<>n)
        else if(germplasmGeneration.match(/^BC\d+[A-Za-z]+\d+$/g)) {
            let genType = germplasmGeneration.match(/[A-Za-z]+\d+$/g)[0]
            let bcGeneration = germplasmGeneration.match(/^BC\d+[A-Za-z]+/g)[0]
            let generationNumber = parseInt(genType.match(/\d+/g)[0])
            newGeneration = newGeneration.concat(bcGeneration,(generationNumber+1))
        }
        // if F1TOP or F1F generation
        else if(germplasmGeneration.match(/^F1(TOP|F)$/g)) {
            newGeneration = 'F2'
        }
        // if unsupported, return input
        else {
            newGeneration = germplasmGeneration
        }

        return newGeneration
    },

    /**
     * Advance the given germplasm BC generation
     * @param {string} germplasmGeneration - the BC generation to be advanced
     * @return {string} newGeneration - advanced BC generation
     */
     advanceGermplasmBCGeneration: async(germplasmGeneration) => {
        let newGeneration = ''

        // If BC generation, increase the BC generation
        if (germplasmGeneration.match(/^BC\d+F\d+$/g)) {
            let bcGeneration = germplasmGeneration.match(/BC\d+/g)[0]
            let generationNumber = parseInt(bcGeneration.match(/\d+/g)[0])
            newGeneration = newGeneration.concat('BC',(generationNumber+1),'F1')
        }
        // // TEMPORARILY DISABLED. To be discussed with RAs if still needed
        // // If F generation, initialize BC generation (BC1Fn)
        // else if (germplasmGeneration.match(/^F\d+$/g)) {
        //     let fGeneration = germplasmGeneration.match(/F\d+/g)[0]
        //     newGeneration = 'BC1' + fGeneration
        // }
        // Else, default to BC1F1
        else {
            newGeneration = 'BC1F1'
        }

        return newGeneration
    },

    /**
     * Retrieves the occurrence's harvested package prefix
     * from the occurrence_data table
     * @param {integer} occurrenceDbId - occurrence identifier 
     * @returns the designated package prefix for the occurrence
     */
    getHarvestedPackagePrefix: async (occurrenceDbId) => {
        return await getHarvestedPackagePrefix(occurrenceDbId)
    },

    /**
     * Inserts a new HARVESTED_PACKAGE_PREFIX value
     * in the occurrence_data table for the current occurrence.
     * @param {object} dbRecord - record information (plot/cross)
     * @param {string} delimiter - delimiter used for the package prefix
     * @param {integer} userId - user indentifier
     * @returns 
     */
    createHarvestedPackagePrefix: async (dbRecord, delimiter, userId) => {
        return await createHarvestedPackagePrefix(dbRecord, delimiter, userId)
    },

    /**
     * Retrieves the last item based on a given sequence.
     * @param {string} schema - database schema name
     * @param {string} table - schema table name
     * @param {string} field - table field name
     * @param {string} startingString - starting string of the sequence
     * @param {string} pattern - regex expression of the pattern to find
     * @param {object} optional - optional values for delimiter, prefix, suffix
     * @param {boolean} strictEnd - whether or not the result should also end in the exact rege
     * @returns {string} - the current last record in the sequence
     */
    getLastInNumberedSequence: async (schema, table, field, startingString, pattern, optional = {}, strictEnd = false) => {
        return await getLastInNumberedSequence(schema, table, field, startingString, pattern, optional, strictEnd)
    },

    /**
     * Retrieves the last available designation
     * from a sequence of desinations that
     * begin with a specified starting designation
     * @param {string} startingDesignation - the base designation for the sequence of designation
     * @param {string} pattern - the pattern to look for after the starting designation
     * @param {object} optional - optional string contents such as delimiter, pattern prefix and pattern suffix
     * @return {string} last designation in the sequence
     */
    getLastGermplasmDesignation: async (startingDesignation, pattern, optional) => {
        startingDesignation = escapeRegExp(startingDesignation)

        let delimiter = optional.delimiter == undefined ? '' : optional.delimiter
        let prefix = optional.prefix == undefined ? '' : optional.prefix
        let suffix = optional.suffix == undefined ? '' : optional.suffix

        try {

            let germplasmQuery = `
                SELECT 
                    designation AS designation 
                FROM 
                    germplasm.germplasm 
                WHERE  
                designation ~ '^${startingDesignation}${delimiter}${prefix}${pattern}${suffix}' AND germplasm.is_void = FALSE ORDER BY id DESC LIMIT 1
            `

            // Retrieve plot from the database
            let germplasmDesignation = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    startingDesignation: startingDesignation,
                },
            })

            return germplasmDesignation[0]['designation']

        } catch (err) {
            return null
        }
    },

    getLastPlantDesignation: async (designation) => {
        try {

            let germplasmQuery = `
                SELECT 
                    designation AS designation 
                FROM 
                    germplasm.germplasm 
                WHERE  
                    designation ILIKE '${designation}__' or designation ILIKE '${designation}___' or designation ILIKE '${designation}___%'  AND germplasm.is_void = FALSE order by id desc limit 1
            `
            // Retrieve plot from the database
            let germplasmDesignation = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    designation: designation,
                },
            })

            return germplasmDesignation[0]

        } catch (err) {
            return null
        }
    },

    getNumberOfSeedlotCreated: async (plotId) => {

        try {

            let seedQuery = `
                SELECT 
                    count(1) 
                FROM 
                    germplasm.seed 
                WHERE  
                    seed.source_plot_id = (:plotId) AND seed.is_void = FALSE
            `
            // Retrieve plot from the database
            let seedCount = await sequelize.query(seedQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    plotId: plotId,
                },
            })

            return seedCount[0].count

        } catch (err) {
            return null
        }
    },

    getGermplasmRecord: async (germplasmDbId) => {
        try {
            let germplasmQuery = `
                SELECT 
                    germplasm.id AS "germplasmDbId",
                    germplasm.designation,
                    germplasm.parentage,
                    germplasm.generation,
                    germplasm.germplasm_state as "germplasmState",
                    germplasm.germplasm_type as "germplasmType",
                    germplasm.germplasm_name_type as "germplasmNameType",
                    germplasm.crop_id as "cropDbId",
                    germplasm.taxonomy_id as "taxonomyDbId",
                    germplasm.germplasm_normalized_name as "germplasmNormalizedName"
                FROM 
                    germplasm.germplasm 
                WHERE  
                    id = '${germplasmDbId}' AND germplasm.is_void = FALSE
            `
            // Retrieve plot from the database
            let germplasm = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            return germplasm[0]

        } catch (err) {
            return null
        }
    },

    getF1FGermplasmChild: async (femaleParentGermplasmDbId, maleParentGermplasmDbId) => {
        try {
            let germplasmRelationQuery = `
                (SELECT
                    germplasm_relation.child_germplasm_id AS "childGermplasmDbId"
                FROM
                    germplasm.germplasm_relation germplasm_relation
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = germplasm_relation.child_germplasm_id
                WHERE
                    germplasm_relation.parent_germplasm_id = ${femaleParentGermplasmDbId}
                    AND germplasm.germplasm_type = 'F1F'
                    AND germplasm_relation.is_void = FALSE
                    AND germplasm.is_void = FALSE
                ORDER BY germplasm_relation.id ASC)
                INTERSECT
                (SELECT
                    germplasm_relation.child_germplasm_id AS "childGermplasmDbId"
                FROM
                    germplasm.germplasm_relation germplasm_relation
                LEFT JOIN
                    germplasm.germplasm germplasm ON germplasm.id = germplasm_relation.child_germplasm_id
                WHERE
                    germplasm_relation.parent_germplasm_id = ${maleParentGermplasmDbId}
                    AND germplasm.germplasm_type = 'F1F'
                    AND germplasm_relation.is_void = FALSE
                    AND germplasm.is_void = FALSE
                ORDER BY germplasm_relation.id ASC)
                LIMIT 1
            `
            // Retrieve plot from the database
            let germplasmRelation = await sequelize.query(germplasmRelationQuery, {
                type: sequelize.QueryTypes.SELECT
            })

            return germplasmRelation[0]

        } catch (err) {
            return null
        }
    },

    getGermplasm: async (designation) => {

        try {
            let germplasmQuery = `
                SELECT 
                    id 
                FROM 
                    germplasm.germplasm 
                WHERE  
                    designation ='${designation}' AND germplasm.is_void = FALSE
            `
            // Retrieve plot from the database
            let germplasm = await sequelize.query(germplasmQuery, {
                type: sequelize.QueryTypes.SELECT,
                replacements: {
                    designation: designation,
                },
            })

            return germplasm[0]

        } catch (err) {
            return null
        }

    },

    getHarvestConfigValue: async (configAbbrev) => {

        return getHarvestConfig(configAbbrev)

    },

    /**
     * Create a cross record and a cross parent record
     * for a plot if it does not have one yet
     * @param {object} plot the plot record containing the info needed to fill the cross
     * @param {integer} userId id of the user creating the records
     * @return {integer} newCrossDbId id of the created cross record 
     */
    createSelfCross: async (plot, userId) => {
        // create cross record
        let crossInfo = []
        crossInfo['cross_name'] = plot.germplasmDesignation.concat('|',plot.germplasmDesignation)
        crossInfo['cross_method'] = await getScaleValueByAbbrev('CROSS_METHOD_SELFING')
        crossInfo['experiment_id'] = plot.experimentDbId
        crossInfo['is_method_autofilled'] = true;
        let result = await insertRecord('germplasm', 'cross', crossInfo, userId)
        let newCrossDbId = result[0].id

        // create cross parent record
        let crossParentInfo = []
        crossParentInfo['experiment_id'] = plot.experimentDbId
        crossParentInfo['germplasm_id'] = plot.germplasmDbId
        crossParentInfo['seed_id'] = plot.seedDbId
        crossParentInfo['entry_id'] = plot.entryDbId
        crossParentInfo['parent_role'] = 'female-and-male'
        crossParentInfo['order_number'] = 1
        crossParentInfo['cross_id'] = newCrossDbId
        result = await insertRecord('germplasm', 'cross_parent', crossParentInfo, userId)

        return newCrossDbId
    },

    /**
     * Returns the value of the scale value
     * given its abbrev
     * @param {string} abbrev string abbreviation of the scale value
     * @return {string} value the string value of the scale value
     */
    getScaleValueByAbbrev: async (abbrev) => {
        return await getScaleValueByAbbrev(abbrev)   
    },

    /**
     * Returns information of the scale value
     * given its abbrev
     * @param {string} abbrev string abbreviation of the scale value
     * @return {string} value the string value of the scale value
     */
     getScaleValueInfoByAbbrev: async (abbrev) => {
        return await getScaleValueInfoByAbbrev(abbrev)   
    },
    
    /**
     * Populates the child germplasm attribute with the same value as the parent's.
     * If the parent does not have the attribute, child attribute will not be populated.
     * @param {integer} parentGermplasmDbId - gerplasm identifier
     * @param {integer} childGermplasmDbId - gerplasm identifier
     * @param {string} attributeAbbrev - abbreviation of germplasm attribute variable
     * @return {object} germplasmAttribute - germplasm attribute information
     */
    populateGermplasmAttribute: async (parentGermplasmDbId, childGermplasmDbId, attributeAbbrev, userId) => {
        let variable = await getVariableByAbbrev(attributeAbbrev)   
        if (variable == undefined) return
        let variableDbId = variable.variableDbId

        let parentAttribute = await getGermplasmAttribute(parentGermplasmDbId, variableDbId)

        // if parent attribute exists, populate child attribute with same dataValue
        if (parentAttribute != undefined) {
            germplasmAttributeInfo = {}
            germplasmAttributeInfo["germplasm_id"] = childGermplasmDbId
            germplasmAttributeInfo["variable_id"] = parentAttribute.variableDbId
            germplasmAttributeInfo["data_qc_code"] = parentAttribute.dataQcCode
            germplasmAttributeInfo["data_value"] = parentAttribute.dataValue

            await insertRecord('germplasm', 'germplasm_attribute', germplasmAttributeInfo, userId)
        }
    },

    /**
     * Insert a new germplasm attribute record
     * @param {integer} germplasmDbId - germplasm identifier
     * @param {string} variableAbbrev - abbrev of the variable to insert
     * @param {string} dataValue - value of the variable
     * @param {integer} userId - user identifier
     */
    insertGermplasmAttribute: async (germplasmDbId, variableAbbrev, dataValue, userId) => {
        let variable = await getVariableByAbbrev(variableAbbrev)
        let variableDbId = variable.variableDbId

        germplasmAttributeInfo = {}
        germplasmAttributeInfo["germplasm_id"] = germplasmDbId
        germplasmAttributeInfo["variable_id"] = variableDbId
        germplasmAttributeInfo["data_qc_code"] = 'G'
        germplasmAttributeInfo["data_value"] = dataValue

        await insertRecord('germplasm', 'germplasm_attribute', germplasmAttributeInfo, userId)
    },

    /**
     * Inserts the child germplasm cross number attribute.
     * @param {integer} germplasmDbId - gerplasm identifier
     * @param {string} crossNumber - cross number of germplasm
     * @param {integer} userId - user identifier
     */
     insertGermplasmAttributeCrossNumber: async (germplasmDbId, crossNumber, userId) => {
        let variable = await getVariableByAbbrev('CROSS_NUMBER')
        let variableDbId = variable.variableDbId

        germplasmAttributeInfo = {}
        germplasmAttributeInfo["germplasm_id"] = germplasmDbId
        germplasmAttributeInfo["variable_id"] = variableDbId
        germplasmAttributeInfo["data_qc_code"] = 'G'
        germplasmAttributeInfo["data_value"] = crossNumber

        await insertRecord('germplasm', 'germplasm_attribute', germplasmAttributeInfo, userId)
    },

    /**
     * Retrieve the number of occurrences an experiment has
     * @param {integer} experimentDbId - experiment identifier
     * @return {integer} occurrenceCount - number of occurrences and experiment has
     */
    getOccurrenceCountOfExperiment: async (experimentDbId) => {
        try {

            let occurrenceCountQuery = `
                SELECT 
                    COUNT(1) AS "occurrenceCount"
                FROM 
                    experiment.occurrence
                WHERE  
                    occurrence.experiment_id = ${experimentDbId}
                    AND occurrence.is_void = FALSE
            `
            // Retrieve plot from the database
            let occurrenceCount = await sequelize.query(occurrenceCountQuery, {
                type: sequelize.QueryTypes.SELECT
            })
    
            return occurrenceCount[0]["occurrenceCount"]
    
        } catch (err) {
            return null
        }
    },

    /**
     * Identify which parent is recurrent
     * and which is non-recurrent.
     * @param {integer} femaleGermplasmDbId - female parent germplasm identifier
     * @param {integer} maleGermplasmDbId - male parent germplasm identifier
     * @returns {object} - contains the recurrent and non recurrent parent. null if no recurrent parent
     */
    findBackcrossRecurrentParent: async (femaleGermplasmDbId, maleGermplasmDbId) => {
        let parentGermplasmDbId = 0
        let parentCount = 0
        let FGenRegex = /F[2-9][0-9]*/g

        // get parents of female
        let femaleGermplasm = await getGermplasmInfo(femaleGermplasmDbId)
        let femaleGeneration = femaleGermplasm !== undefined && femaleGermplasm.generation !== undefined ?
            femaleGermplasm.generation : ''
        let parentsOfFemale = []
        let femaleGID = femaleGermplasmDbId
        // If the female parent's generation contains F2+, traverse upward to find the F1 parent
        while (femaleGeneration.match(FGenRegex)) {
            parentsOfFemale = await getGermplasmParents(femaleGID)
            if (parentsOfFemale !== undefined && parentsOfFemale != null && parentsOfFemale.length == 1) {
                currentParent = parentsOfFemale[0]
                currentParentGermplasm = await getGermplasmInfo(currentParent.parentGermplasmDbId)
                femaleGeneration = currentParentGermplasm.generation
                femaleGID = currentParentGermplasm.germplasmDbId
            }
            else break
        }
        parentsOfFemale = await getGermplasmParents(femaleGID)

        // check if male is recurrent
        parentCount = parentsOfFemale.length
        for (let i = 0; i < parentCount; i++) {
            parentGermplasmDbId = parentsOfFemale[i].parentGermplasmDbId
            if (maleGermplasmDbId == parentGermplasmDbId) {
                let recurrentParentGermplasm = await getGermplasmInfo(maleGermplasmDbId)
                recurrentParentGermplasm.germplasmParentRole = 'male'

                let nonRecurrentParentGermplasm = await getGermplasmInfo(femaleGermplasmDbId)
                nonRecurrentParentGermplasm.germplasmParentRole = 'female'

                return {
                    recurrentParentGermplasm: recurrentParentGermplasm,
                    nonRecurrentParentGermplasm: nonRecurrentParentGermplasm
                }
            }
        }

        // get parents of male
        let maleGermplasm = await getGermplasmInfo(maleGermplasmDbId)
        let maleGeneration = maleGermplasm !== undefined && maleGermplasm.generation !== undefined ?
            maleGermplasm.generation : ''
        let parentsOfMale = []
        let maleGID = maleGermplasmDbId
        // If the male parent's generation contains F2+, traverse upward to find the F1 parent
        while (maleGeneration.match(FGenRegex)) {
            parentsOfMale = await getGermplasmParents(maleGID)
            if (parentsOfMale !== undefined && parentsOfMale != null && parentsOfMale.length == 1) {
                currentParent = parentsOfMale[0]
                currentParentGermplasm = await getGermplasmInfo(currentParent.parentGermplasmDbId)
                maleGeneration = currentParentGermplasm.generation
                maleGID = currentParentGermplasm.germplasmDbId
            }
            else break
        }
        parentsOfMale = await getGermplasmParents(maleGID)

        // check if female is recurrent
        parentCount = parentsOfMale.length
        for (let i = 0; i < parentCount; i++) {
            parentGermplasmDbId = parentsOfMale[i].parentGermplasmDbId
            if (femaleGermplasmDbId == parentGermplasmDbId) {
                let recurrentParentGermplasm = await getGermplasmInfo(femaleGermplasmDbId)
                recurrentParentGermplasm.germplasmParentRole = 'female'
                
                let nonRecurrentParentGermplasm = await getGermplasmInfo(maleGermplasmDbId)
                nonRecurrentParentGermplasm.germplasmParentRole = 'male'

                return {
                    recurrentParentGermplasm: recurrentParentGermplasm,
                    nonRecurrentParentGermplasm: nonRecurrentParentGermplasm
                }
            }
        }

        // if no recurrent parent, return null
        return null
    },

    /**
     * Build new backcross parentage
     * @param {object} entitiesArray - entities required for building the parentage
     * @param {string} recurrentField - field to use for recurrent increment
     * @param {boolean} checkOtherNames - whether or not to check other names (cross_name, cross_abbreviation), defaults to true
     * @returns {string} - new backcross parentage
     */
    backcrossParentageProcess: async (entitiesArray, recurrentField = 'designation', checkOtherNames = true) => {
        // get cross and recurrent parent germplasm entites
        let cross = entitiesArray.cross
        let recurrentParentGermplasm = entitiesArray.recurrentParentGermplasm
        let nonRecurrentParentGermplasm = entitiesArray.nonRecurrentParentGermplasm
        // get non-recurrent parentage
        let parentage = nonRecurrentParentGermplasm.parentage

        // get recurrent value
        let recurrentValue = null
        // check if recurrent parent has cross_abbreviation or cross_name
        let rpGermplasmDbId = recurrentParentGermplasm.germplasmDbId
        let rpGermplasmName = await getGermplasmNameByType(rpGermplasmDbId, 'cross_abbreviation')
        // If cross_abbreviation does not exist, get germplasm_name with name type = cross_name
        if(rpGermplasmName == undefined) {
            rpGermplasmName = await getGermplasmNameByType(rpGermplasmDbId, 'cross_name')
            // If cross_name does not exist, get standard/active germplasm name
            if(rpGermplasmName == undefined) {
                rpGermplasmName = await getGermplasmNameInfo(rpGermplasmDbId)
            }
        }
        // If cross_abbreviation or cross_name exists, use it as recurrent value
        let rpGermplasmNameType = rpGermplasmName.germplasmNameType
        let hasAbbrevOrName = false
        if(checkOtherNames && (rpGermplasmNameType == 'cross_abbreviation' || rpGermplasmNameType == 'cross_name')) {
            recurrentValue = rpGermplasmName.nameValue
            hasAbbrevOrName = true
        }
        // Else, use the recurrent field
        else {
            recurrentValue = recurrentParentGermplasm[recurrentField]
        }

        // get configuration for recurrent parent
        let recurrentParentConfigAbbrev = 'HM_RECURRENT_PARENT_PATTERN_BACKCROSS_' + cross.cropCode + '_DEFAULT'
        let recurrentParentConfig = await getHarvestConfig(recurrentParentConfigAbbrev)
        recurrentParentConfig = recurrentParentConfig.configValue
        let parentageDelimiter = recurrentParentConfig.delimiter_parentage

        // build regex for finding recurrent value in non-recurrent parentage
        let regexes = await generateRecurrentRegex(recurrentValue, recurrentParentConfig)   
        let leftRegex = regexes.leftRegex
        let rightRegex = regexes.rightRegex
        // split non-recurrent parentage by parentage delimiter from config
        let parentageParts = parentage.split(parentageDelimiter)
        let parentagePartsLength = parentageParts.length
        // locate the recurrent value in the non-recurrent's parentage
        let delimiterCount = (recurrentValue.match(/\//g) || []).length;
        let interval = delimiterCount + 1
        let side = ''
        let recurrentPart = ''
        let nonRecurrentPart = ''

        for (let i = 0; i < parentagePartsLength; i++) {
            let currentPiece = parentageParts.slice(i,i+interval).join(parentageDelimiter)
            
            // if matching string is found
            if (
                currentPiece.match(leftRegex) ||
                currentPiece.match(rightRegex)
            ) {
                recurrentPart = currentPiece

                // if interval + current index is greater than or equal to the number of parts,
                // set side = right
                if (interval + i >= parentagePartsLength) {
                    side = 'right'
                    nonRecurrentPart = parentageParts.slice(0,i).join(parentageDelimiter)
                }
                // if interval + current index is less than the number of parts,
                // set side = left
                else {
                    side = 'left'
                    nonRecurrentPart = parentageParts.slice(i+interval,parentagePartsLength).join(parentageDelimiter)
                }
                break
            }
        }

        // get next number for backcross
        let backcrossDelimiter = recurrentParentConfig.delimiter_backcross_number
        let rPParts = recurrentPart.split(backcrossDelimiter)
        let rPPartsLength = rPParts.length
        let counter = 2
        for (let i = 0; i < rPPartsLength; i++) {
            let curr = rPParts[i];
            // if curr matches regex for integers, the current number is found
            if (curr.match(/^\d+$/g)) {
                curr = parseInt(curr)       // parse curr as integer
                counter = curr + 1          // set counter as curr + 1
                break
            }
        }

        // build recurrent parent string
        let patternIdx = side == 'left' ? 0 : 1
        let pattern = recurrentParentConfig.pattern[patternIdx]
        let patternLength = pattern.length
        let rpString = ''
        for (let i = 0; i < patternLength; i++) {
            let patternItem = pattern[i]
            let patternType = patternItem.type
            let value = ''

            if (patternType == 'field') {
                // If recurrent parent has cross_abbreviation or cross name, use it as base value
                if (hasAbbrevOrName) {
                    value = value.concat(recurrentValue)
                }
                // Else, use the field
                else {
                    let entity = patternItem.entity
                    let fieldName = patternItem.field_name
                    value = value.concat(entitiesArray[entity][fieldName])
                }
                    
            } else if (patternType == 'delimiter') {
                value = value.concat(patternItem.value)
            } else if (patternType == 'counter') {
                value = value.concat(counter)
            }

            rpString = rpString.concat(value)
        }

        // build final parentage string parentageDelimiter
        if (side == 'left') {
            return {
                bcParentage: `${rpString}${parentageDelimiter}${nonRecurrentPart}`,
                recurrentRole: 'femaleRecurrent'
            }
        } else if (side == 'right') {
            return {
                bcParentage: `${nonRecurrentPart}${parentageDelimiter}${rpString}`,
                recurrentRole: 'maleRecurrent'
            }
        }
        else {
            return {
                bcParentage: 'unknown',
                recurrentRole: null
            }
        }
    },

    /**
     * Retrieves the naming config for the config base string
     * given the entities and optional parameters
     * @param {string} configBaseString - base string of the config
     * @param {*} entitiesArray entities containing info needed for building names
     * @param {*} optional - optional parameters like counter, additionalConditions, etc
     */
    getNomenclatureConfig: async (configBaseString, entitiesArray, optional) => {
        return await getNomenclatureConfig(configBaseString, entitiesArray, optional)
    },

    /**
     * Checks if the given germplasm ids have been backcrossed before.
     * Supports maize and wheat backcrosses.
     * @param {integer} crossDbId - cross identifier
     * @param {integer} femaleParentGermplasmDbId - female germplasm identifier
     * @param {integer} maleParentGermplasmDbId - male germplasm identifier
     * @returns {object} - object containing hasPreviousBackcross (boolean), notTrueBackcross (boolean), and childGermplasmId (integer)
     */
    checkBackcross: async (crossDbId, femaleParentGermplasmDbId, maleParentGermplasmDbId) => {
        // Retrieve germplasm_relation record
        let childGermplasmId = await getChildGermplasmId(femaleParentGermplasmDbId, maleParentGermplasmDbId, false)

        // if no child yet, allow to create new germplasm
        if (childGermplasmId == 0) {
            return {
                hasPreviousBackcross: false,
                childGermplasmId: childGermplasmId
            }
        }

        // return the germplasm id of the existing child
        return {
            hasPreviousBackcross: true,
            childGermplasmId: childGermplasmId
        }
    },
    
    /**
     * Performs double haploid process. If the germplasm attribute for
     * DH_FIRST_INCREASE_COMPLETED is FALSE, then 4 things are done:
     * - germplasm.designation is appended with -B
     * - existing germplasm.germplasm_name record's status is set to 'active'
     * - new germplasm.germplasm_name record is created for the DH designation
     * - germplasm.germplasm_attribute for DH_FIRST_INCREASE_COMPLETED is set to TRUE
     * @param {integer} germplasmDbId germplasm identifier
     * @param {array} entitiesArray contains entities for name building
     * @param {integer} userId user identifier
     * @returns 
     */
    doubleHaploidProcess: async (germplasmDbId, entitiesArray, userId) => {
        // Get variable id of DH_FIRST_INCREASE_COMPLETED germplasm attribute
        let variable = await getVariableByAbbrev('DH_FIRST_INCREASE_COMPLETED')
        let variableDbId = variable.variableDbId
        // Retrieve germplasm attribute
        let germplasmAttribute = await getGermplasmAttribute(germplasmDbId, variableDbId)
        // If no attribute found, return false
        if(germplasmAttribute === undefined) return false
        // Get attribute id and value
        let germplasmAttributeDbId = germplasmAttribute.germplasmAttributeDbId
        let dhFirstIncreaseCompleted = germplasmAttribute.dataValue.toLowerCase() === "true"

        // If dhFirstIncreaseCompleted = false, start DH process
        if(!dhFirstIncreaseCompleted) { 
            // Generate DH germplasm designation
            let configBaseString = 'HM_NAME_PATTERN_GERMPLASM'
            let newDesignation = await genericNameBuilder(configBaseString, entitiesArray)

            // 1. Rename germplasm
            let values = [
                `designation = '${newDesignation}'`
            ]
            result = await updateRecordById(germplasmDbId, 'germplasm', 'germplasm', values, userId)
            // If there was an error while updating, return false
            if(!result) return false

            // Get old germplasm_name with status = standard
            let germplasmNames = await retrieveRecordsBasic(
                'germplasm',
                'germplasm_name',
                [
                    'germplasm_name.id AS "germplasmNameDbId"',
                    'germplasm_name.germplasm_name_status AS "germplasmNameStatus"'
                ],
                [
                    `germplasm_name.germplasm_id = ${germplasmDbId}`,
                    `germplasm_name.germplasm_name_status = 'standard'`,
                ],
                'ORDER BY ID DESC LIMIT 1'
            )
            let oldGermplasmName = germplasmNames!== null && germplasmNames[0] !== undefined ? germplasmNames[0] : [];
            let oldGermplasmNameDbId = oldGermplasmName.germplasmNameDbId !== undefined ? oldGermplasmName.germplasmNameDbId : 0;

            // 2. Change old germplasm_name status to 'active'
            values = [
                `germplasm_name_status = 'active'`
            ]
            result = await updateRecordById(oldGermplasmNameDbId, 'germplasm', 'germplasm_name', values, userId)
            if(!result) return false

            // 3. Create new germplasm_name
            let germplasmNameInfo = {}
            germplasmNameInfo['germplasm_id'] = germplasmDbId
            germplasmNameInfo['name_value'] = newDesignation
            germplasmNameInfo['germplasm_name_type'] = 'pedigree'
            germplasmNameInfo['germplasm_name_status'] = 'standard'

            result = await insertRecord('germplasm', 'germplasm_name', germplasmNameInfo, userId)
            if(!result) return false

            // 4. Update DH_FIRST_INCREASE_COMPLETED germplasm attribute
            // Set value to TRUE
            values = [
                `data_value = 'true'`
            ]
            result = await updateRecordById(germplasmAttributeDbId, 'germplasm', 'germplasm_attribute', values, userId)
            if(!result) return false
        }

        return true
    },

    /**
     * Enclose the designation of the germplasm
     * if it has 2 parents, OR if its generation is F1
     * @param {object} germplasmRecord contains germplasm information
     * @returns final designation
     */
    encloseDesignationInParentheses: async (germplasmRecord) => {
        // Check if designation needs to be in parentheses
        let pDesignation = germplasmRecord.designation
        let germplasmParents = await getGermplasmParents(germplasmRecord.germplasmDbId)
        let germplasmParentCount = germplasmParents.length
        let isF1 = germplasmRecord.generation.includes('F1')
        if (germplasmParentCount == 2 || isF1) {
            pDesignation = `(${pDesignation})`
        }

        return pDesignation
    },

    /**
     * Retrieve the child germplasm given the parent
     * and the conditions for the child
     * @param {integer} parentGermplasmId parent germplasm identifier
     * @param {array} childConditions contains query conditions for the child germplasm
     * @returns {integer} the child germplasm's id
     */
    retrieveChildGermplasm: async (parentGermplasmId, childConditions = []) => {
        let childConditionsString = ``
        if (childConditions.length > 0) {
            for (let i = 0; i < childConditions.length; i++) {
                childConditionsString += 'AND ' + childConditions[i] + '\n'
            }
        }

        let childGermplasmQuery = `
            SELECT
                parent.id AS "parentGermplasmDbId",
                child.id AS "childGermplasmDbId"
            FROM
                germplasm.germplasm_relation gr
            LEFT JOIN
                germplasm.germplasm parent ON parent.id = gr.parent_germplasm_id AND parent.is_void = FALSE
            LEFT JOIN
                germplasm.germplasm child ON child.id = gr.child_germplasm_id AND child.is_void = FALSE
            WHERE
                gr.parent_germplasm_id = ${parentGermplasmId}
                AND gr.is_void = false
                ${childConditionsString}
        `
        // Retrieve child germplasm id from the database
        let childGermplasm = await sequelize.query(childGermplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let childGermplasmDbId = null
        if(childGermplasm[0] != undefined && childGermplasm[0]['childGermplasmDbId'] != undefined) {
            childGermplasmDbId = childGermplasm[0]['childGermplasmDbId']
        }

        return childGermplasmDbId
    },

    /**
     * Retrieve the child germplasm given the parent
     * and the conditions for the child
     * @param {integer} femaleGermplasmId female parent germplasm identifier
     * @param {integer} maleGermplasmId male parent germplasm identifier
     * @param {array} childConditions contains query conditions for the child germplasm
     * @returns {integer} the child germplasm's id
     */
    retrieveCommonChildGermplasm: async (femaleGermplasmId, maleGermplasmId, childConditions = []) => {
        let childConditionsString = ``
        if (childConditions.length > 0) {
            for (let i = 0; i < childConditions.length; i++) {
                childConditionsString += 'AND ' + childConditions[i] + '\n'
            }
        }

        let childGermplasmQuery = `
        SELECT
            child.id as "childGermplasmDbId"
        FROM 
            germplasm.germplasm AS child
        LEFT JOIN
            germplasm.germplasm_relation female ON female.child_germplasm_id = child.id AND female.is_void = false
        LEFT JOIN
            germplasm.germplasm_relation male ON male.child_germplasm_id = child.id AND male.is_void = false
        WHERE
            child.is_void = FALSE
            AND female.parent_germplasm_id = ${femaleGermplasmId}
            AND male.parent_germplasm_id = ${maleGermplasmId}
            ${childConditionsString}
        `
        // Retrieve child germplasm id from the database
        let childGermplasm = await sequelize.query(childGermplasmQuery, {
            type: sequelize.QueryTypes.SELECT
        })

        let childGermplasmDbId = null
        if(childGermplasm[0] != undefined && childGermplasm[0]['childGermplasmDbId'] != undefined) {
            childGermplasmDbId = childGermplasm[0]['childGermplasmDbId']
        }

        return childGermplasmDbId
    },

    /**
     * Creates germplasm year for the germplasm
     * @param {integer} germplasmDbId germplasm identifier
     * @param {object} cross contains the crossing date
     * @param {integer} userId user identifier
     */
    createGermplasmYear: async (germplasmDbId, cross, userId) => {
        let crossingDate = cross.crossingDate
        let yearValue = crossingDate.split('-')[0]

        let variable = await getVariableByAbbrev('GERMPLASM_YEAR')
        let variableDbId = variable.variableDbId

       let germplasmAttributeInfo = {}
        germplasmAttributeInfo["germplasm_id"] = germplasmDbId
        germplasmAttributeInfo["variable_id"] = variableDbId
        germplasmAttributeInfo["data_qc_code"] = 'G'
        germplasmAttributeInfo["data_value"] = yearValue

        await insertRecord('germplasm', 'germplasm_attribute', germplasmAttributeInfo, userId)
    },

    /**
     * Creates seed name
     * @param {string} seedPattern seed pattern
     * @param {object} plotRecord plot record
     * @param {integer} seedCounter seed counter
     * @param {integer} occurrenceCount occurrence count
     */
    createSeedName: async (seedPattern, plotRecord, seedCounter, occurrenceCount) => {
        return createSeedName(seedPattern, plotRecord, seedCounter, occurrenceCount)
    },

    /**
     * Creates package name
     * @param {string} packagePattern package pattern
     * @param {object} seedRecord seed record
     * @param {object} plotRecord plot record
     */
    createPackageName: async (packagePattern, seedRecord,plotRecord) => {
        return createPackageName(packagePattern, seedRecord,plotRecord)
    },

    /**
     * Gets experiment ccurrence count
     * @param {integer} experimentDbId experiment identifier
     */
    getExperimentOccurrenceCount: async (experimentDbId) => {
        return getExperimentOccurrenceCount(experimentDbId)
    },

    /**
     * Gets seed
     * @param {string} seedName seed name
     * @param {integer} germplasmDbId germplasm identifier
     * @param {integer} sourceExperimentDbId source experiment identifier
     * @param {integer} sourceEntryDbId source entry identifier
     * @param {integer} sourcePlotDbId source plot identifier
     * @param {integer} sourceOccurrenceDbId source occurrence identifier
     * @param {integer} sourceLocationDbId source location identifier
     */
    getSeed: async (seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId) => {
        return getSeed(seedName, germplasmDbId, sourceExperimentDbId, sourceEntryDbId, sourcePlotDbId, sourceOccurrenceDbId, sourceLocationDbId)
    },

    createGermplasmOrigin: async (germplasmDbId, cross, userId) => {
        let origin
        // Get occurrence record
        let occurrenceDbId = cross.occurrenceDbId

        let occurrence = await getOneRecord(
            'experiment',
            'occurrence',
            `WHERE occurrence.id = ${occurrenceDbId}`
        )

        // Get site
        let siteDbId = occurrence.site_id

        let site = await getOneRecord(
            'place',
            'geospatial_object',
            `WHERE geospatial_object.id = ${siteDbId}`
        )

        // Get parent site
        let parentSiteDbId = site.parent_geospatial_object_id

        let parentSite = await getOneRecord(
            'place',
            'geospatial_object',
            `WHERE geospatial_object.id = ${parentSiteDbId}`
        )

        if (parentSite != null
            && parentSite.geospatial_object_type != null
            && parentSite.geospatial_object_type.toLowerCase() == 'country'
            && parentSite.geospatial_object_name != null) {
            origin = parentSite.geospatial_object_name
        }
        else return

        // Check prefix config (if any) and append it to origin
        let prefixConfigSearch = await getOneRecord(
            'platform',
            'config',
            `WHERE abbrev = 'HM_PREFIX_GERMPLASM_ATTRIBUTE_ORIGIN'`
        )
        let prefixConfig = prefixConfigSearch != null && prefixConfigSearch.config_value != null ? prefixConfigSearch.config_value : null
        let prefix = prefixConfig != null && prefixConfig.prefix != null ? prefixConfig.prefix : ''
        let delimiter = prefixConfig != null && prefixConfig.delimiter != null ? prefixConfig.delimiter : ''
        origin = prefix + delimiter + origin

        // Insert origin attribute
        let variable = await getVariableByAbbrev('ORIGIN')
        let variableDbId = variable.variableDbId

        let germplasmAttributeInfo = {}
        germplasmAttributeInfo["germplasm_id"] = germplasmDbId
        germplasmAttributeInfo["variable_id"] = variableDbId
        germplasmAttributeInfo["data_qc_code"] = 'G'
        germplasmAttributeInfo["data_value"] = origin

        await insertRecord('germplasm', 'germplasm_attribute', germplasmAttributeInfo, userId)
    },
    
    /**
     * Retrieves plot data records given the plot id and variable abbrev.
     * 
     * @param {integer} plotDbId plot identifier
     * @param {string} variableAbbrev variable abbreviation
     * @returns {object} variable object
     */
    getPlotData: async (plotDbId, variableAbbrev) => {
        // Get variable abbrev
        let variable = await getVariableByAbbrev(variableAbbrev)
        if (variable == null || variable.variableDbId == null) throw Error("Variable not found.")
        let variableDbId = variable.variableDbId

        // Get plot data
        result = await retrieveRecordsBasic(
            "experiment",
            "plot_data",
            [],
            [ 
                `plot_id = ${plotDbId}`,
                `variable_id = ${variableDbId}` 
            ]
        )

        return result[0] != null ? result[0] : null
    },

    /**
     * Identify which variables to use for COWPEA harvest
     * @param {string} generation germplasm generation
     */
    findVariablesByGeneration: async (generation) => {
        let cowpeaVariables = {}

         // if normal generation (Fn, Tn, Mn, etc)
        if (generation.match(/^[A-Za-z]+\d+$/g)) {
            if (generation.match(/F1$/g)) {
                cowpeaVariables['germplasmType'] = 'GERMPLASM_TYPE_SEGREGATING'
                cowpeaVariables['germplasmState'] = 'not_fixed'
                cowpeaVariables['germplasmNameType'] = 'GERMPLASM_NAME_TYPE_CROSS_CODE'
            } else if (generation.match(/F3$|F4$|F5$|F6$/g)) {
                cowpeaVariables['germplasmType'] = 'GERMPLASM_TYPE_FIXED_LINE'
                cowpeaVariables['germplasmState'] = 'fixed'
                cowpeaVariables['germplasmNameType'] = 'GERMPLASM_NAME_TYPE_INBRED_LINE'
            }
        } 
        // // if back cross generation (BCm<>n)
        else if(generation.match(/^BC\d+[A-Za-z]+\d+$/g)) {
            if (generation.match(/F1$/g)) {
                cowpeaVariables['germplasmType'] = 'GERMPLASM_TYPE_SEGREGATING'
                cowpeaVariables['germplasmState'] = 'not_fixed'
                cowpeaVariables['germplasmNameType'] = 'GERMPLASM_NAME_TYPE_CROSS_CODE'
            } else if (generation.match(/F3$|F4$|F5$|F6$/g)) {
                cowpeaVariables['germplasmType'] = 'GERMPLASM_TYPE_INBRED_LINE'
                cowpeaVariables['germplasmState'] = 'fixed'
                cowpeaVariables['germplasmNameType'] = 'GERMPLASM_NAME_TYPE_CROSS_CODE'
            }
        }

        return cowpeaVariables
    },

    /**
     * Generates cross record records without cross records
     * @param {Object} plotRecord 
     * @param {Integer} userId 
     * 
     * @returns mixed
     */
    populateCrossRecord: async (plotRecord,userId) => {

        if(plotRecord.crossDbId == null) {
            let crossDbId = await createSelfCross(plotRecord, userId)
            plotRecord.crossDbId = crossDbId
            plotRecord.crossParentCount = 1
        }

        return plotRecord
    },

    /**
     * 
     */
    buildGermplasmDesignation: async (entitiesArray) => {
        return await buildGermplasmDesignation(entitiesArray);
    },

    populateRelationTable: async (schema, entity, parentDbId, childDbId, userId) => {
        return await populateRelationTable(schema, entity, parentDbId, childDbId, userId);
    },

    buildPackageLabel: async (dbRecord, entitiesArray, harvestSource, userId) => {
        return await buildPackageLabel(dbRecord, entitiesArray, harvestSource, userId);
    },

    buildSeedName: async (entitiesArray, optional = {}) => {
        return await buildSeedName(entitiesArray, optional);
    },

    retrieveRecordsBasic: async (schema, table, fields = [], conditions = [], options = '') => {
        return await retrieveRecordsBasic(schema, table, fields, conditions, options);
    },

    /**
     * Get new counter value of the given name
     * Ex. If name is IR 12345-4
     * counter value should be 5
     * @param {string} name - the last name in the sequence
     * @param {string} delimiter - delimiter of the name separating the number
     * @param {object} optional - optional values such as counter prefix and suffix
     */
    getCounterValue: async(name, delimiter, optional = {}) => {
        return await getCounterValue(name, delimiter, optional = {})
    },

    hasSeedRecord: async (req, res, seedDbId) => {
        return hasSeedRecord(req, res, seedDbId);
    },

    getSeedRecord: async (req, res, additionalConditions) => {
        return getSeedRecord(req, res, additionalConditions);
    }
}