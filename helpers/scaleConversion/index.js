/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
const scaleConversionsSearch = require('../../routes/v3/scale-conversions-search')

module.exports = {
    /**
     * Retrieves conversion rules based on given crop ID (or lack thereof)
     * 
     * @param {String} cropDbId crop identifier (optional)
     * 
     * @return {Array} List of conversion rules
     */
    getScaleConversionValues: async (cropDbId = null) => {
        let cropDbIdConditionString = ''

        // Build condition string for cropDbId
        if (cropDbId != null) {
            cropDbIdConditionString = `id = ${cropDbId} AND
			id = scale_conversion.crop_id AND`
        } else {
            cropDbIdConditionString = `scale_conversion.crop_id IS NULL AND`
        }

        let scaleConversionQuery = `
            SELECT
                scale_conversion.id AS "scaleConversionDbId",
                scale_conversion.source_unit_id AS "sourceUnitDbId",
                source_scale_value.value AS "sourceUnit",
                scale_conversion.target_unit_id AS "targetUnitDbId",
                target_scale_value.value AS "targetUnit",
                scale_conversion.conversion_value AS "conversionValue"
            FROM
                master.scale_conversion scale_conversion
            LEFT JOIN
                master.scale_value source_scale_value
            ON
                source_scale_value.id = scale_conversion.source_unit_id
            LEFT JOIN
                master.scale_value target_scale_value
            ON
                target_scale_value.id = scale_conversion.target_unit_id
            WHERE
                EXISTS (
                    SELECT 
                        1
                    FROM
                        tenant.crop
                    WHERE
                        ${cropDbIdConditionString}
                        is_void = FALSE
                )
        `

        // Retrieve scale conversion values from the database
        let scaleConversions = await sequelize.query(scaleConversionQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
            .catch(async err => {
                await logger.logFailingQuery(endpoint, 'SELECT', err)
                let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                res.send(new errors.InternalError(errMsg))
                return
            })

        // If no conversion rules were retrieved from a Crop...
        // ...retrieve default conversion rules
        if (scaleConversions.length === 0) {
            scaleConversionQuery = `
                SELECT
                    scale_conversion.id AS "scaleConversionDbId",
                    scale_conversion.source_unit_id AS "sourceUnitDbId",
                    source_scale_value.value AS "sourceUnit",
                    scale_conversion.target_unit_id AS "targetUnitDbId",
                    target_scale_value.value AS "targetUnit",
                    scale_conversion.conversion_value AS "conversionValue"
                FROM
                    master.scale_conversion scale_conversion
                LEFT JOIN
                    master.scale_value source_scale_value
                ON
                    source_scale_value.id = scale_conversion.source_unit_id
                LEFT JOIN
                    master.scale_value target_scale_value
                ON
                    target_scale_value.id = scale_conversion.target_unit_id
                WHERE
                    EXISTS (
                        SELECT 
                            1
                        FROM
                            tenant.crop
                        WHERE
                            scale_conversion.crop_id IS NULL AND
                            is_void = FALSE
                    )
            `

            // Retrieve scale conversion values from the database
            scaleConversions = await sequelize.query(scaleConversionQuery, {
                type: sequelize.QueryTypes.SELECT,
            })
                .catch(async err => {
                    await logger.logFailingQuery(endpoint, 'SELECT', err)
                    let errMsg = await errorBuilder.getError(req.headers.host, 500004)
                    res.send(new errors.InternalError(errMsg))
                    return
                })
        }

        return scaleConversions
    },
}