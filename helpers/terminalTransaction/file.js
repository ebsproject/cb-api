/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')

module.exports = {

    /**
     * Builds query to retrieve terminal transaction_dataset records
     * 
     * @param integer transactionDbId Terminal transaction identifier
     * @param string condition Additional SQL condition
     */
    getDataset: async (transactionDbId, condition) => {
        let datasetQuery = `
            SELECT
                dataset.id::text AS "datasetDbId",
                dataset.transaction_id::text AS "transactionDbId",
                dataset.variable_id::text AS "variableDbId",
                dataset.value,
                dataset.status,
                dataset.is_suppressed AS "isSuppressed",
                dataset.suppress_remarks AS "suppressRemarks",
                dataset.is_generated AS "isComputed",
                dataset.entity,
                dataset.entity_id::text AS "entityDbId",
                dataset.data_unit AS "dataUnit",
                dataset.remarks,
                dataset.notes,
                dataset.is_void AS "isVoid",
                dataset.collection_timestamp AS "collectionTimestamp",
                dataset.creation_timestamp AS "creationTimestamp",
                creator.id AS "creatorDbId",
                creator.person_name AS creator,
                dataset.modification_timestamp AS "modificationTimestamp",
                modifier.id AS "modifierDbId",
                modifier.person_name AS modifier
            FROM
                data_terminal.transaction_dataset dataset
                    LEFT JOIN
                    tenant.person creator ON dataset.creator_id = creator.id
                    LEFT JOIN
                    tenant.person modifier ON dataset.modifier_id = modifier.id
            WHERE
                dataset.transaction_id=${transactionDbId}
                ${condition}
        `
        return await sequelize.query(datasetQuery, {
            type: sequelize.QueryTypes.SELECT,
        })
    },

    /**
     * Inserts transaction file record
     * 
     * @param integer transactionDbId Terminal transaction identifier 
     * @param string status status of the transaction file(committed plot data, 
     * committed cross data, invalid plot data, invalid cross data, 
     * voided plot data, voided cross data)
     * @param json jsonData Dataset records in JSON format
     * @param integer userId User identifier
     * @param {*} transaction Database transaction
     */
    create: async (transactionDbId, status, jsonData, userId, transaction) => {
        let datasetQuery = `
            INSERT INTO
                data_terminal.transaction_file (
                status, transaction_id, dataset, creator_id, is_void)
            SELECT 
                '${status}', 
                ${transactionDbId}, 
                '${jsonData}', 
                ${userId}, 
                FALSE
            RETURNING
                id
        `
        return await sequelize.query(datasetQuery, {
            type: sequelize.QueryTypes.SELECT,
            transaction: transaction
        })
    },

}