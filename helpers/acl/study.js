/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let {sequelize} = require('../../config/sequelize')

module.exports = {

    isStudyAccessible: async (entity,entityId,requestor,requestorId) => {

        let checkAccessQuery = `
            SELECT * FROM z_admin.check_access(
                in_entity := $$` + entity +`$$,
                in_entity_id := $$` + entityId +`$$,
                in_requestor := $$` + requestor +`$$,
                in_requestor_id := $$` + requestorId +`$$
            );
        `

        // Retrieve programs from the database   
        let acl = await sequelize.query(checkAccessQuery,{
            type: sequelize.QueryTypes.SELECT
        })

        if(acl[0].success == true){
            return true
        }
        else{
            return false
        }
    },

    getAccessibleStudies: async(entity,entityId, role = null) => {
        
        let getAccessibleStudies = ``

        if(role == null){
            getAccessibleStudies = `
                SELECT * FROM z_admin.get_accessible_studies(?,?);
            `
        } else {
            let roleAbbrevString = ''

            if(role != null){

                for (role of role.split(',')) {
                    roleAbbrevString += (roleAbbrevString == '') ? "$$" + (role.toUpperCase()).trim() + "$$" : ",$$" + (role.toUpperCase()).trim() + "$$"
                }
            }

            getAccessibleStudies = `
                SELECT * FROM z_admin.get_accessible_studies(?,?,array[` + roleAbbrevString + `])
                WHERE data_role_id IN (
                    SELECT id
                    FROM master.role
                    WHERE abbrev in (
                        ` + roleAbbrevString + `
                    )
                );
            `
        }
        
        // Retrieve list of study IDs that the entity has access to
        let studyList = await sequelize.query(getAccessibleStudies,{
            type: sequelize.QueryTypes.SELECT,
            replacements: [entity,entityId,role]
        })

        return studyList
    }
}