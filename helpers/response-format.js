/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


function ResponseDataFormat (rows = null, count = null) {

    this.rows = rows;
    this.count = count;

}


class PaginationFormat {
    constructor(paginationObject = {
        pageSize: null,
        totalCount: null,
        currentPage: null,
        totalPages: null
    }){
        this.pageSize = paginationObject.pageSize
        this.totalCount = paginationObject.totalCount
        this.currentPage = paginationObject.currentPage
        this.totalPages = paginationObject.totalPages
    }
}

class ResponseFormat {

    constructor(){

        this.metadata = {
            pagination: new PaginationFormat(),
            status: [{
                message: "",
                messageType: ""
            }],
            datafiles: []
        };

        this.result = {
            data: []
        };

    }

    setPagination(pagination) {
        if(pagination instanceof Object){
            this.metadata.pagination = pagination
        }else{
            throw new Error("Pagination must be of type object")
        }
    }

    setData(data){
        if(data instanceof Array){
            this.result.data = data
        }else{
            this.result.data.push(data)
        }
    }

    setStatus(status){
        this.metadata.status = [status]
    }

    addStatus(status){
        this.metadata.status.push(status)
    }

}

module.exports = {
    ResponseDataFormat,
    ResponseFormat,
    PaginationFormat
};