/**
 * Copyright (C) 2024 Enterprise Breeding System
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

let { sequelize } = require('../../config/sequelize')
let format = require('pg-format')
let seedlotHelper = require('../seedlot/index')
let logger = require('../logger/index.js')
const endpoint = 'helpers/package'

module.exports = {

    /**
     * Creates a new package log record for the given package ID.
     * 
     * @param {Integer} packageDbId package identifier
     * @param {Double} packageQuantity package quantity value
     * @param {String} packageUnit package unit value
     * @param {Integer} creatorDbId creator identifier
     * @param {String} packageTransactionType package transaction type (default: deposit; supports: deposit, withdraw, reserve)
     */
    createPackageLog: async (packageDbId, packageQuantity, packageUnit, creatorDbId, packageTransactionType = 'deposit') => {
        try {

            // Build values array
            let values = [
                [
                    packageDbId,
                    packageQuantity,
                    packageUnit,
                    creatorDbId,
                    packageTransactionType
                ]
            ]

            // Build insert query
            let insertQuery = format(`
                INSERT INTO germplasm.package_log
                    (package_id, package_quantity, package_unit, creator_id, package_transaction_type)
                VALUES
                    %L
                RETURNING id`, values)

            // Insert package log
            let packageLog = await sequelize.transaction(async transaction => {
                return await sequelize.query(insertQuery, {
                    type: sequelize.QueryTypes.INSERT,
                    transaction: transaction,
                    raw: true
                }).catch(async err => {
                    logger.logFailingQuery(endpoint+"::createPackageLog()", 'INSERT', err)
                    throw new Error(err)
                })
            })

            return packageLog[0]['id']
        }
        catch (err) {
            logger.logMessage(__filename, err.stack, 'error')
            return null
        }
    },

    /**
     * Determines the package transaction type and package log quantity
     * based on the current quantity and the new quantity.
     * 
     * CASE 1:
     * When the current quantity is greater than the new quantity,
     * the transaction type is "withdraw",
     * and the package log quantity is the current quantity MINUS the new quantity.
     * 
     * CASE 2:
     * When the current quantity is less than the new quantity,
     * the transaction type is "deposit",
     * and the package log quantity is the new quantity MINUS the current quantity.
     * 
     * CASE 3:
     * When the current quantity is equal to the new quantity, returns null.
     * 
     * @param {Integer} currentQuantity current quantity of package.package_quantity
     * @param {Integer} newQuantity new quanityt of package.package_quantity
     * @returns {Object|null} object containing the transaction type and pqValue
     */
    getPackageLogTransactionType: async (currentQuantity, newQuantity) => {
        // If the current quantity is greater than the new quantity,
        // set transaction type to 'withdraw'.
        // The package_quantity value of the package log record (pqValue)
        // is the currentQuantity MINUS the newQuantity
        if (currentQuantity > newQuantity) {
            return {
                transactionType: await seedlotHelper.getScaleValueByAbbrev('PACKAGE_TRANSACTION_TYPE_WITHDRAW'),
                pqValue: currentQuantity - newQuantity
            }
        }
        // If the new quantity is greater than the current quantity,
        // set transaction type to 'deposit'.
        // The package_quantity value of the package log record (pqValue)
        // is the newQuantity MINUS the currentQuantity
        else if (newQuantity > currentQuantity) {
            return {
                transactionType: await seedlotHelper.getScaleValueByAbbrev('PACKAGE_TRANSACTION_TYPE_DEPOSIT'),
                pqValue: newQuantity - currentQuantity
            }
        }

        // Else, return null. If the current quantity is equal to the new quantity,
        // the package_quantity is not updated, and therefore, no package log is created.
        return null
    },

    /**
     * Converts a package quantity from current package unit to another.
     * 
     * @param {Double} packageQuantity package quantity value
     * @param {String} currentPackageUnit current package unit value
     * @param {String} desiredPackageUnit desired package unit value
     * @param {Array} conversionArray list of conversion rules
     * 
     * @return {Integer|Double} Converted package quantity
     */
    convertPackageQuantity: async (packageQuantity, currentPackageUnit, desiredPackageUnit, conversionArray) => {
        // Convert packageQuantity if there is a formula for the given package units
        for (const conversion of conversionArray) {
            const conversionValue = conversion['conversionValue']

            if (conversion['sourceUnit'] == desiredPackageUnit && conversion['targetUnit'] == currentPackageUnit) { // e.g. g to kg
                return packageQuantity /= conversionValue
            } else if (conversion['sourceUnit'] == currentPackageUnit && conversion['targetUnit'] == desiredPackageUnit) { // e.g. kg to g
                return packageQuantity *= conversionValue
            }
        }
    },
}